#include "undistort_node.h"
#include <limits>
#include <iostream>
#include <fstream>
#include <thread> 

#include <boost/filesystem.hpp>


#include "ros/package.h"
#include <cv_bridge/cv_bridge.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/Image.h>
#include "tf2_eigen/tf2_eigen.h"
#include <tf_conversions/tf_eigen.h>

#include "gflags/gflags.h"
#include "glog/logging.h"

#include "undistort_service/node_constants.h"

#include "camera.h"


using namespace std;
using namespace cv;

namespace undistort_service {
constexpr double kTfBufferCacheTimeInSeconds = 3.;

namespace {
constexpr int kLatestOnlyPublisherQueueSize = 1;
static string kIRFrame = "infrared_frame";
static string kRGBFrame = "visible_frame";

// Subscribes to the 'topic' for 'trajectory_id' using the 'node_handle' and
// calls 'handler' on the 'node' to handle messages. Returns the subscriber.
template <typename MessageType>
::ros::Subscriber SubscribeWithHandler(
    void (UndistortNode::*handler)(const std::string&,
                                    const typename MessageType::ConstPtr&),
      const std::string& topic,
    ::ros::NodeHandle* const node_handle, UndistortNode* const node) {
  return node_handle->subscribe<MessageType>(
      topic, kLatestOnlyPublisherQueueSize,
      boost::function<void(const typename MessageType::ConstPtr&)>(
          [node, handler,
           topic](const typename MessageType::ConstPtr& msg) {
            (node->*handler)(topic, msg);
          }));
}
}

UndistortNode::UndistortNode() :
  buffer_(),
  listener_(buffer_),
  state_(UndistortState::IDLE),
  calib_file_updated_(false)
 {
  GetParameters();

  // camera_ = make_shared<Camera>();
  // camera_->LoadCalibrationFile(calibration_filepath_);
  cameras_["rgb"] = make_shared<Camera>();
  cameras_["rgb"]->LoadCalibrationFile(calibration_filepath_, "rgb");

  cameras_["ir"] = make_shared<Camera>();
  cameras_["ir"]->LoadCalibrationFile(calibration_filepath_, "ir");

  // robot_pose_subscriber_ = SubscribeWithHandler<nav_msgs::Odometry>(
  //         &UndistortNode::HandleRobotPose, robot_pose_topic_, &node_handle_,
  //         this);

  ptz_subscriber_ =  SubscribeWithHandler<nav_msgs::Odometry>(
            &UndistortNode::HandlePtzMessage, ptz_topic_, &node_handle_,
            this);

  // temp_image_subscriber_ =  SubscribeWithHandler<sensor_msgs::Image>(
  //           &UndistortNode::HandleTempImage, raw_topic_, &node_handle_,
  //           this);

  undistort_result_publisher_= node_handle_.advertise<undistort_service_msgs::PosedImage>(
        undistort_topic_, 1);

  device_pose_publisher_= node_handle_.advertise<undistort_service_msgs::DevicePosed>(
        device_pose_topic_, 1);

  service_servers_.push_back(node_handle_.advertiseService(
      kStartUndistortServiceName, &UndistortNode::HandleStartUndistortService, this));

  service_servers_.push_back(node_handle_.advertiseService(
      kStopUndistortServiceName, &UndistortNode::HandleStopUndistortService, this));

  service_servers_.push_back(node_handle_.advertiseService(
      kUpdateCalibParamsServiceName, &UndistortNode::HandleUpdateCalibParamsService, this));

  LaunchSynchronizer();

  state_ = UndistortState::UNDISTORTING;
}

UndistortNode::~UndistortNode() { }

void UndistortNode::GetParameters() {
  
  ::ros::NodeHandle private_handle("~");

  private_handle.param<int>("/undistort_service/device_id", device_id_, 0);
  private_handle.param<std::string>("/undistort_service/robot_pose_topic", robot_pose_topic_, "/robot_pose");
  private_handle.param<std::string>("/undistort_service/ptz_topic", ptz_topic_, "/ptz/position");
  private_handle.param<std::string>("/undistort_service/raw_topic", raw_topic_, "/fixed/infrared/raw");
  private_handle.param<std::string>("/undistort_service/arm_topic", arm_topic_, "/yida/robot/arm/data");

  private_handle.param<std::string>("/undistort_service/map_frame", map_frame_, "map");
  private_handle.param<std::string>("/undistort_service/lidar_frame", lidar_frame_, "lidar_pose");
  private_handle.param<std::string>("/undistort_service/robot_frame", robot_frame_, "robot_pose");

  private_handle.param<std::string>("/undistort_service/undistort_topic", undistort_topic_, "/infrared/undistort");

  private_handle.param<std::string>("/undistort_service/device_pose_topic", device_pose_topic_, "/device/pose");

  ros::param::get("~calib_filepath", calibration_filepath_);
}

bool UndistortNode::HandleStartUndistortService(
        undistort_service_msgs::StartUndistort::Request& request,
        undistort_service_msgs::StartUndistort::Response& response) {

  LOG(INFO) << "Enter HandleStartUndistortService . " ;
  if (state_ == UndistortState::UNDISTORTING) {
      response.status.code = UndistortResult::SUCCESS;
      response.status.message = "undistorting.";
      LOG(WARNING) << "undistorting! " ;
      return true;  
  }
  ptz_subscriber_ =  SubscribeWithHandler<nav_msgs::Odometry>(
            &UndistortNode::HandlePtzMessage, ptz_topic_, &node_handle_,
            this);

  // temp_image_subscriber_ = SubscribeWithHandler<sensor_msgs::Image>(
  //           &UndistortNode::HandleTempImage, raw_topic_, &node_handle_,
  //           this);

  response.status.code = UndistortResult::SUCCESS;
  state_ = UndistortState::UNDISTORTING;
  LOG(INFO) << "Exit HandleStartUndistortService . " ;
  return true;
}

bool UndistortNode::HandleStopUndistortService(
        undistort_service_msgs::StopUndistort::Request& request,
        undistort_service_msgs::StopUndistort::Response& response) {
  LOG(INFO) << "Enter HandleUpdateCalibParamsService . " ;

  if (state_ == UndistortState::IDLE) {
      response.status.code = UndistortResult::SUCCESS;
      response.status.message = "not undistorting.";
      LOG(WARNING) << "not undistorting! " ;
      return true;  
  }

  temp_image_subscriber_.shutdown();
  ptz_subscriber_.shutdown();
  std::this_thread::sleep_for(std::chrono::milliseconds(50));

  response.status.code = UndistortResult::SUCCESS;
  state_ = UndistortState::IDLE;

  LOG(INFO) << "Exit HandleStopUndistortService . " ;
  return true;
      
}

bool UndistortNode::HandleUpdateCalibParamsService(
        undistort_service_msgs::UpdateCalibParams::Request& request,
        undistort_service_msgs::UpdateCalibParams::Response& response) {
  LOG(INFO) << "Enter HandleUpdateCalibParamsService . " ;

  ofstream calib_file(calibration_filepath_);
  calib_file << request.calib_params[0];

  calib_file_updated_ = true;

  response.status.code = UndistortResult::SUCCESS;

  LOG(INFO) << "Exit HandleUpdateCalibParamsService . " ;
  return true;
}

void UndistortNode::HandleRobotPose( const std::string& topic, const nav_msgs::Odometry::ConstPtr& pose_msg) {
  // LOG(INFO) << "Enter HandleRobotPose ." ;
  LOG_EVERY_N(INFO, 100)  << topic << " Enter HandleRobotPose .";

  // Eigen::Affine3d robot_to_map;
  // Eigen::fromMsg(pose_msg->pose.pose, robot_to_map);
  // geometry_msgs::TransformStamped robot_transform_msg = tf2::eigenToTransform(robot_to_map);
  // robot_transform_msg.header.stamp = pose_msg->header.stamp;
  // robot_transform_msg.header.frame_id = map_frame_;
  // robot_transform_msg.child_frame_id = robot_frame_;
  // // broadcaster_.sendTransform(robot_transform_msg);
  // buffer_.setTransform(robot_transform_msg,"");
}

void UndistortNode::HandlePtzMessage(const std::string& topic, const nav_msgs::Odometry::ConstPtr& msg) {
  // LOG(INFO) << "Enter HandlePtzMessage ." ;
  LOG_EVERY_N(INFO, 100)  << topic << " Enter HandlePtzMessage .";

  float pitch = 0.0;
  if( msg->pose.pose.position.z >= 0 &&  msg->pose.pose.position.z <= 18000)
  {
	  pitch = (-msg->pose.pose.position.z) / 100.0;
  }
  else
  {
	  pitch = (36000 - msg->pose.pose.position.z) / 100.0;
  }

  // rgb
  Eigen::Affine3d rgb_camera_to_lidar = cameras_["rgb"]->GetRelativePose({msg->pose.pose.position.x / 100, pitch}); 
  // rgb_camera_to_lidar.linear().normalize();
  Eigen::Quaterniond rgb_q(rgb_camera_to_lidar.linear());  // assuming that upper 3x3 matrix is orthonormal
  rgb_q.normalize();
  geometry_msgs::TransformStamped rgb_transform_msg = tf2::eigenToTransform(rgb_camera_to_lidar);
  rgb_transform_msg.header.stamp = msg->header.stamp;
  rgb_transform_msg.header.frame_id = lidar_frame_;
  rgb_transform_msg.child_frame_id = kRGBFrame;
  rgb_transform_msg.transform.rotation.x = rgb_q.x();
  rgb_transform_msg.transform.rotation.y = rgb_q.y();
  rgb_transform_msg.transform.rotation.z = rgb_q.z();
  rgb_transform_msg.transform.rotation.w = rgb_q.w();
  // broadcaster_.sendTransform(rgb_transform_msg);
  buffer_.setTransform(rgb_transform_msg,"");

  // ir
  Eigen::Affine3d ir_camera_to_lidar = cameras_["ir"]->GetRelativePose({msg->pose.pose.position.x / 100, pitch});
  // ir_camera_to_lidar.linear().normalize();
  Eigen::Quaterniond ir_q(ir_camera_to_lidar.linear());  // assuming that upper 3x3 matrix is orthonormal
  ir_q.normalize();
  geometry_msgs::TransformStamped ir_ransform_msg = tf2::eigenToTransform(ir_camera_to_lidar);
  ir_ransform_msg.header.stamp = msg->header.stamp;
  ir_ransform_msg.header.frame_id = lidar_frame_;
  ir_ransform_msg.child_frame_id = kIRFrame;
  ir_ransform_msg.transform.rotation.x = ir_q.x();
  ir_ransform_msg.transform.rotation.y = ir_q.y();
  ir_ransform_msg.transform.rotation.z = ir_q.z();
  ir_ransform_msg.transform.rotation.w = ir_q.w();
  // broadcaster_.sendTransform(ir_ransform_msg);
  buffer_.setTransform(ir_ransform_msg,"");
}

void UndistortNode::HandleTempImage( const std::string& topic, const sensor_msgs::Image::ConstPtr& img_msg) {
  // LOG(INFO) << "Enter HandleTempImage .";
  LOG_EVERY_N(INFO, 100)  << topic << " Enter HandleTempImage .";

  if (calib_file_updated_) {
    cameras_["ir"]->LoadCalibrationFile(calibration_filepath_, "ir");
    calib_file_updated_ = false;
  }

  geometry_msgs::TransformStamped camera_to_map_transform;
  try{
    camera_to_map_transform = buffer_.lookupTransform(
          map_frame_, kIRFrame, img_msg->header.stamp, ::ros::Duration(kTfBufferCacheTimeInSeconds));
  } catch (tf2::TransformException &ex) {
    LOG(WARNING)  << topic << ": Could NOT transform " << kIRFrame << " to " << map_frame_<< " : " <<  ex.what();
    return;
  }
  LOG_EVERY_N(INFO, 100)  << topic << ": lookupTransform from " << kIRFrame << " to " << map_frame_<< " ok.";

  Eigen::Affine3d camera_to_map =  tf2::transformToEigen (camera_to_map_transform);

  cv_bridge::CvImageConstPtr cv_ptr= cv_bridge::toCvCopy(*img_msg, sensor_msgs::image_encodings::TYPE_32FC1);

  cv_bridge::CvImage heat_cvimg(img_msg->header,
                            sensor_msgs::image_encodings::TYPE_32FC1, cv_ptr->image);

  undistort_service_msgs::PosedImage posed_image;
  heat_cvimg.toImageMsg(posed_image.image);
  posed_image.image.header = img_msg->header;
  posed_image.camera = Eigen::toMsg(camera_to_map);

  undistort_result_publisher_.publish(posed_image);
  // LOG(INFO) << "Exit HandleTempImage . " ;
}

void UndistortNode::LaunchSynchronizer() {
  robot_sync_sub_ = nullptr;
  ptz_sync_sub_ = nullptr;
  temp_sync_sub_ = nullptr;
  arm_sync_sub_ = nullptr;
  robot_ptz_arm_sync_ = nullptr;
  robot_ptz_temp_sync_ = nullptr;
  robot_sync_sub_ = new message_filters::Subscriber<nav_msgs::Odometry>(node_handle_, robot_pose_topic_, 1);
  ptz_sync_sub_ = new message_filters::Subscriber<nav_msgs::Odometry>(node_handle_, ptz_topic_, 1);
  temp_sync_sub_ = new message_filters::Subscriber<sensor_msgs::Image>(node_handle_, raw_topic_, 1);
  arm_sync_sub_ = new message_filters::Subscriber<yidamsg::ArmDataStamped>(node_handle_, arm_topic_, 1);
  robot_ptz_arm_sync_ = new message_filters::Synchronizer<robot_ptz_arm_SyncPolicy>(robot_ptz_arm_SyncPolicy(3), *robot_sync_sub_, *ptz_sync_sub_, *arm_sync_sub_);
  robot_ptz_arm_sync_->registerCallback(boost::bind(&UndistortNode::robot_ptz_arm_SyncCallback, this, _1, _2, _3));
  robot_ptz_temp_sync_ = new message_filters::Synchronizer<robot_ptz_temp_SyncPolicy>(robot_ptz_temp_SyncPolicy(5), *robot_sync_sub_, *ptz_sync_sub_, *temp_sync_sub_);
  robot_ptz_temp_sync_->registerCallback(boost::bind(&UndistortNode::robot_ptz_temp_SyncCallback, this, _1, _2, _3));
}

void UndistortNode::robot_ptz_arm_SyncCallback(const nav_msgs::Odometry::ConstPtr &robot_msg, const nav_msgs::Odometry::ConstPtr &ptz_msg, const yidamsg::ArmDataStamped::ConstPtr &arm_msg) {
  // LOG(INFO) << "Enter robot_ptz_arm_SyncCallback . " ;

  // msg
  undistort_service_msgs::DevicePosed device_pose;
  device_pose.device_id = device_id_;

  // robot
  double robot_roll, robot_pitch, robot_yaw;
  tf2::Quaternion robot_quaternion;
	tf2::convert(robot_msg->pose.pose.orientation, robot_quaternion);
	tf2::Matrix3x3(robot_quaternion).getRPY(robot_roll, robot_pitch, robot_yaw);

  device_pose.robot.position = robot_msg->pose.pose.position;
  device_pose.robot.orientation = robot_msg->pose.pose.orientation;
  device_pose.robot.vector.x = robot_roll;
  device_pose.robot.vector.y = robot_pitch;
  device_pose.robot.vector.z = robot_yaw;

  // // robot
  // try {
  //   geometry_msgs::TransformStamped robot_to_map_transform;
  //   robot_to_map_transform = buffer_.lookupTransform(
  //     map_frame_, robot_frame_, robot_msg->header.stamp, ::ros::Duration(1));
  //   LOG_EVERY_N(INFO, 100) << "robot_ptz_SyncCallback: lookupTransform from " << robot_frame_ << " to " << map_frame_<< "  ok. ";
  //   Eigen::Affine3d robot_to_map =  tf2::transformToEigen (robot_to_map_transform);
  //   geometry_msgs::Pose robot_pose = Eigen::toMsg(robot_to_map);
  //   double robot_roll, robot_pitch, robot_yaw;
  //   tf2::Quaternion robot_quaternion;
	//   tf2::convert(robot_pose.orientation, robot_quaternion);
	//   tf2::Matrix3x3(robot_quaternion).getRPY(robot_roll, robot_pitch, robot_yaw);

  //   device_pose.robot.position = robot_pose.position;
  //   device_pose.robot.orientation = robot_pose.orientation;
  //   device_pose.robot.vector.x = robot_roll;
  //   device_pose.robot.vector.y = robot_pitch;
  //   device_pose.robot.vector.z = robot_yaw;
  // } catch (tf2::TransformException &ex) {
  //   LOG(WARNING) <<  "robot_ptz_SyncCallback Could NOT transform " << robot_frame_ << " to " << map_frame_<< " : " <<  ex.what();
  // }

  // rgb
  try{
    geometry_msgs::TransformStamped rgb_camera_to_map_transform;
    rgb_camera_to_map_transform = buffer_.lookupTransform(
      map_frame_, kRGBFrame, ptz_msg->header.stamp, ::ros::Duration(1));
    LOG_EVERY_N(INFO, 100) << "robot_ptz_SyncCallback: lookupTransform from " << kRGBFrame << " to " << map_frame_<< " ok.";
    Eigen::Affine3d rgb_camera_to_map =  tf2::transformToEigen (rgb_camera_to_map_transform);
    // Eigen::Matrix4d rgb_rep_to_cv;
    // rgb_rep_to_cv << 0, 0, 1, 0,
    //                  0,-1, 0, 0,
    //                  1, 0, 0, 0,
    //                  0, 0, 0, 1;
    // Eigen::Affine3d rgb_cam_rep_to_world(rgb_camera_to_map.matrix() * rgb_rep_to_cv);
    geometry_msgs::Pose rgb_pose = Eigen::toMsg(rgb_camera_to_map);

    double rgb_roll, rgb_pitch, rgb_yaw;
    tf2::Quaternion rgb_quaternion;
    tf2::convert(rgb_pose.orientation, rgb_quaternion);
    tf2::Matrix3x3(rgb_quaternion).getRPY(rgb_roll, rgb_pitch, rgb_yaw); //进行转换
    double rgb_horizontal_fov, rgb_vertical_fov;
    cameras_["rgb"]->GetCameraFov(rgb_horizontal_fov, rgb_vertical_fov);
    cv::Size rgb_size;
    cameras_["rgb"]->GetCameraSize(rgb_size);

    device_pose.visible.position = rgb_pose.position;
    device_pose.visible.orientation = rgb_pose.orientation;
    device_pose.visible.vector.x = rgb_roll;
    device_pose.visible.vector.y = rgb_pitch;
    device_pose.visible.vector.z = rgb_yaw;
    device_pose.visible.width = rgb_size.width;
    device_pose.visible.height = rgb_size.height;
    device_pose.visible.fov_h = rgb_horizontal_fov;
    device_pose.visible.fov_v = rgb_vertical_fov;
  } catch (tf2::TransformException &ex) {
    LOG(WARNING) <<  "robot_ptz_SyncCallback Could NOT transform " << kRGBFrame << " to " << map_frame_<< " : " <<  ex.what();
  }
  
  // ir
  try{
    geometry_msgs::TransformStamped ir_camera_to_map_transform;
    ir_camera_to_map_transform = buffer_.lookupTransform(
      map_frame_, kIRFrame, ptz_msg->header.stamp, ::ros::Duration(1));
    LOG_EVERY_N(INFO, 100) << "robot_ptz_SyncCallback: lookupTransform from " << kIRFrame << " to " << map_frame_<< " ok.";
    Eigen::Affine3d ir_camera_to_map =  tf2::transformToEigen (ir_camera_to_map_transform);
    // Eigen::Matrix4d ir_rep_to_cv;
    // ir_rep_to_cv << 0, 0, 1, 0,
    //                 0,-1, 0, 0,
    //                 1, 0, 0, 0,
    //                 0, 0, 0, 1;
    // Eigen::Affine3d ir_cam_rep_to_world(ir_camera_to_map.matrix() * ir_rep_to_cv);
    geometry_msgs::Pose ir_pose = Eigen::toMsg(ir_camera_to_map);

    double ir_roll, ir_pitch, ir_yaw;
    tf2::Quaternion ir_quaternion;
    tf2::convert(ir_pose.orientation, ir_quaternion);
    tf2::Matrix3x3(ir_quaternion).getRPY(ir_roll, ir_pitch, ir_yaw); //进行转换
    double ir_horizontal_fov, ir_vertical_fov;
    cameras_["ir"]->GetCameraFov(ir_horizontal_fov, ir_vertical_fov);
    cv::Size ir_size;
    cameras_["ir"]->GetCameraSize(ir_size);

    device_pose.infrared.position = ir_pose.position;
    device_pose.infrared.orientation = ir_pose.orientation;
    device_pose.infrared.vector.x = ir_roll;
    device_pose.infrared.vector.y = ir_pitch;
    device_pose.infrared.vector.z = ir_yaw;
    device_pose.infrared.width = ir_size.width;
    device_pose.infrared.height = ir_size.height;
    device_pose.infrared.fov_h = ir_horizontal_fov;
    device_pose.infrared.fov_v = ir_vertical_fov;
  } catch (tf2::TransformException &ex) {
    LOG(WARNING) <<  "robot_ptz_SyncCallback Could NOT transform " << kIRFrame << " to " << map_frame_<< " : " <<  ex.what();
  }

  // ptz
  try
  {
    device_pose.ptz.horizontal = ptz_msg->pose.pose.position.x;
    device_pose.ptz.vertical = ptz_msg->pose.pose.position.z;
  }
  catch(const std::exception& e)
  {
    std::cerr << e.what() << '\n';
  }

  // arm
  try{
    device_pose.arm.min_motor_angle1 = arm_msg->arm.min_motor_angle1;
    device_pose.arm.min_motor_angle2 = arm_msg->arm.min_motor_angle2;
    device_pose.arm.min_motor_angle3 = arm_msg->arm.min_motor_angle3;
    device_pose.arm.min_arm_length = arm_msg->arm.min_arm_length;
    device_pose.arm.max_motor_angle1 = arm_msg->arm.max_motor_angle1;
    device_pose.arm.max_motor_angle2 = arm_msg->arm.max_motor_angle2;
    device_pose.arm.max_motor_angle3 = arm_msg->arm.max_motor_angle3;
    device_pose.arm.max_arm_height = arm_msg->arm.max_arm_height;
    // device_pose.arm.min_arm_origin = arm_msg->arm.min_arm_origin;
    device_pose.arm.min_arm_touch = arm_msg->arm.min_arm_touch;
    device_pose.arm.min_arm_end = arm_msg->arm.min_arm_end;
  }
  catch(const std::exception& e)
  {
    std::cerr << e.what() << '\n';
  }

  device_pose_publisher_.publish(device_pose);
}

void UndistortNode::robot_ptz_temp_SyncCallback(const nav_msgs::Odometry::ConstPtr &robot_msg, const nav_msgs::Odometry::ConstPtr &ptz_msg, const sensor_msgs::Image::ConstPtr &temp_msg) {
  LOG(INFO) << "Enter robot_ptz_temp_SyncCallback . " ;

  if (calib_file_updated_) {
    cameras_["ir"]->LoadCalibrationFile(calibration_filepath_, "ir");
    calib_file_updated_ = false;
  }

  geometry_msgs::TransformStamped camera_to_map_transform;
  try{
    camera_to_map_transform = buffer_.lookupTransform(
          map_frame_, kIRFrame, temp_msg->header.stamp, ::ros::Duration(kTfBufferCacheTimeInSeconds));
  } catch (tf2::TransformException &ex) {
    LOG(WARNING) <<  "Could NOT transform " << kIRFrame << " to " << map_frame_<< " : " <<  ex.what();
    return;
  }
  LOG_EVERY_N(INFO, 100) << "robot_ptz_temp_SyncCallback: lookupTransform from " << kIRFrame << " to " << map_frame_<< " ok.";
  
  Eigen::Affine3d camera_to_map =  tf2::transformToEigen (camera_to_map_transform);

  cv_bridge::CvImageConstPtr cv_ptr= cv_bridge::toCvCopy(*temp_msg, sensor_msgs::image_encodings::TYPE_32FC1);

  cv_bridge::CvImage heat_cvimg(temp_msg->header,
                            sensor_msgs::image_encodings::TYPE_32FC1, cv_ptr->image);

  undistort_service_msgs::PosedImage posed_image;
  heat_cvimg.toImageMsg(posed_image.image);
  posed_image.image.header =  temp_msg->header;
  posed_image.camera = Eigen::toMsg (camera_to_map);
  posed_image.robot = robot_msg->pose.pose;
  posed_image.ptz.horizontal = ptz_msg->pose.pose.position.x;
  posed_image.ptz.vertical = ptz_msg->pose.pose.position.z;

  undistort_result_publisher_.publish(posed_image);
  // LOG(INFO) << "Exit robot_ptz_temp_SyncCallback . " ;
}

}

