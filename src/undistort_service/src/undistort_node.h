#pragma once

#include <string>
#include <memory>
#include <vector>

#include "ros/ros.h"
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/Image.h>
#include "tf2_ros/transform_listener.h"
#include "tf2_ros/transform_broadcaster.h"
#include "tf2_geometry_msgs/tf2_geometry_msgs.h"

#include "undistort_service_msgs/PosedImage.h"
#include "undistort_service_msgs/DevicePosed.h"

#include "undistort_service_msgs/UpdateCalibParams.h"
#include "undistort_service_msgs/StartUndistort.h"
#include "undistort_service_msgs/StopUndistort.h"

#include "yidamsg/ArmDataStamped.h"

#include <message_filters/subscriber.h>
#include <message_filters/sync_policies/exact_time.h>
#include <message_filters/sync_policies/approximate_time.h>


namespace  undistort_service{
class Camera;

class UndistortNode {
 public:
    enum UndistortResult {
        SUCCESS = 0,
        DEVICE_ERROR = -1,
        IO_ERROR = -2,
        INVALID_PARAM = -4,
        FAILED = -5
    };
  enum UndistortAction {
        START = 0,
        STOP = 1,
  };
  UndistortNode();
  ~UndistortNode();

  UndistortNode(const UndistortNode&) = delete;
  UndistortNode& operator=(const UndistortNode&) = delete;

  private:
    enum UndistortState {
        IDLE = 0,
        UNDISTORTING = 1,
    };

    void GetParameters();
    
    bool HandleStartUndistortService(
        undistort_service_msgs::StartUndistort::Request& request,
        undistort_service_msgs::StartUndistort::Response& response);
    
    bool HandleStopUndistortService(
        undistort_service_msgs::StopUndistort::Request& request,
        undistort_service_msgs::StopUndistort::Response& response);

    bool HandleUpdateCalibParamsService(
        undistort_service_msgs::UpdateCalibParams::Request& request,
        undistort_service_msgs::UpdateCalibParams::Response& response);

    void HandleRobotPose( const std::string& topic, const nav_msgs::Odometry::ConstPtr& pose_msg);
    void HandlePtzMessage(const std::string& topic, const nav_msgs::Odometry::ConstPtr& msg);
    void HandleTempImage( const std::string& topic, const sensor_msgs::Image::ConstPtr& image_msg);

    ::ros::NodeHandle node_handle_;
    ::ros::Publisher undistort_result_publisher_;
    ::ros::Publisher device_pose_publisher_;
    std::vector<::ros::ServiceServer> service_servers_;

    ::ros::Subscriber temp_image_subscriber_;
    ::ros::Subscriber ptz_subscriber_;
    ::ros::Subscriber robot_pose_subscriber_;

    int state_ = UndistortState::IDLE;
    std::string calibration_filepath_;
    
    int device_id_;

    std::string robot_pose_topic_;
    std::string ptz_topic_;
    std::string raw_topic_;
    std::string arm_topic_;
    std::string undistort_topic_;
    std::string device_pose_topic_;

    std::string map_frame_;
    std::string lidar_frame_;
    std::string robot_frame_;

    // std::shared_ptr<Camera> camera_;
    std::map<std::string, std::shared_ptr<Camera>> cameras_;
    tf2_ros::Buffer buffer_;
    tf2_ros::TransformListener listener_;
    tf2_ros::TransformBroadcaster broadcaster_;

    bool calib_file_updated_ = false;

  private:
    void LaunchSynchronizer();
    typedef message_filters::sync_policies::ApproximateTime<nav_msgs::Odometry, nav_msgs::Odometry, yidamsg::ArmDataStamped> robot_ptz_arm_SyncPolicy;
    typedef message_filters::sync_policies::ApproximateTime<nav_msgs::Odometry, nav_msgs::Odometry, sensor_msgs::Image> robot_ptz_temp_SyncPolicy;
    message_filters::Subscriber<nav_msgs::Odometry> *robot_sync_sub_;
    message_filters::Subscriber<nav_msgs::Odometry> *ptz_sync_sub_;
    message_filters::Subscriber<sensor_msgs::Image> *temp_sync_sub_;
    message_filters::Subscriber<yidamsg::ArmDataStamped> *arm_sync_sub_;
    message_filters::Synchronizer<robot_ptz_arm_SyncPolicy> *robot_ptz_arm_sync_;
    message_filters::Synchronizer<robot_ptz_temp_SyncPolicy> *robot_ptz_temp_sync_;
    void robot_ptz_arm_SyncCallback(const nav_msgs::Odometry::ConstPtr &robot_msg, const nav_msgs::Odometry::ConstPtr &ptz_msg, const yidamsg::ArmDataStamped::ConstPtr &arm_msg);
    void robot_ptz_temp_SyncCallback(const nav_msgs::Odometry::ConstPtr &robot_msg, const nav_msgs::Odometry::ConstPtr &ptz_msg, const sensor_msgs::Image::ConstPtr &temp_msg);
  };
  
}
