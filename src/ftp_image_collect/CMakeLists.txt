cmake_minimum_required(VERSION 3.0.2)
project(ftp_image_collect)


# Compile as C++11, supported in ROS Kinetic and newer
# add_compile_options(-std=c++11)
set(CMAKE_CXX_STANDARD 14)
add_definitions(-DLINUX)

# Find catkin macros and libraries if COMPONENTS list like find_package(catkin
# REQUIRED COMPONENTS xyz) is used, also find other catkin packages
find_package(
  catkin REQUIRED
  COMPONENTS cv_bridge
  yidamsg
  geometry_msgs
  roscpp
  rospy
  std_msgs
  param_server)
find_package(Eigen3 REQUIRED)

find_package(CURL REQUIRED)
if(CURL_FOUND)
  message(STATUS "CURL_INCLUDE_DIRS = ${CURL_INCLUDE_DIRS}.")
  message(STATUS "CURL_VERSION_STRING = ${CURL_VERSION_STRING}.")
  include_directories(${CURL_INCLUDE_DIRS})
endif(CURL_FOUND)

find_package(OpenCV REQUIRED)

# ##############################################################################
# catkin specific configuration ##
# ##############################################################################
# The catkin_package macro generates cmake config files for your package Declare
# things to be passed to dependent projects INCLUDE_DIRS: uncomment this if your
# package contains header files LIBRARIES: libraries you create in this project
# that dependent projects also need CATKIN_DEPENDS: catkin_packages dependent
# projects also need DEPENDS: system dependencies of this project that dependent
# projects also need
catkin_package(
  # INCLUDE_DIRS include LIBRARIES ftp_image_collect CATKIN_DEPENDS cv_bridge
  # yidamsg geometry_msgs roscpp rospy std_msgs DEPENDS system_lib
)

# set(FFMPET_PATH /home/nvidia/third_party/ffmpeg-4.4.1/install)
SET(AVCODEC_LIBRARY libavcodec.so)
SET(AVDEVICE_LIBRARY libavdevice.so)
SET(AVUTIL_LIBRARY libavutil.so)
SET(AVFORMAT_LIBRARY libavformat.so)
SET(SWSCALE_LIBRARY libswscale.so)
set(SWRESAMPLE_LIBRARY libswresample.so)
SET(AVFILTER_LIBRARY libavfilter.so)

# ##############################################################################
# Build ##
# ##############################################################################

# common
set(common_dir ${CMAKE_CURRENT_SOURCE_DIR}/src/common/src)
aux_source_directory(${common_dir} common_src)

# Specify additional locations of header files Your package locations should be
# listed before other locations
include_directories(
  include
  ${catkin_INCLUDE_DIRS}
  ${EIGEN3_INCLUDE_DIR}
  ${OpenCV_INCLUDE_DIRS}
  ${common_dir}
  # ${FFMPET_PATH}/include
  )

link_directories(
  ${OpenCV_LIBRARY_DIRS}
  # ${FFMPET_PATH}/lib
)

add_executable(
  ftp_image_collect_node
  src/ftp_image_collect_node.cpp src/ftp_image_collect.cpp
  src/ftp/CurlHandle.cpp src/ftp/FTPClient.cpp src/H264Decoder.cpp
  ${common_src})
target_link_libraries(
  ftp_image_collect_node
  ${catkin_LIBRARIES}
  ${OpenCV_LIBS}
  crypto
  ssl
  yaml-cpp
  ${CURL_LIBRARIES}
  ${AVCODEC_LIBRARY}
  ${AVDEVICE_LIBRARY}
  ${AVUTIL_LIBRARY}
  ${AVFORMAT_LIBRARY}
  ${SWRESAMPLE_LIBRARY}
  ${SWSCALE_LIBRARY}
  )

# ##############################################################################
# Install ##
# ##############################################################################

# all install targets should use catkin DESTINATION variables See
# http://ros.org/doc/api/catkin/html/adv_user_guide/variables.html

# Mark executable scripts (Python etc.) for installation in contrast to
# setup.py, you can choose the destination catkin_install_python(PROGRAMS
# scripts/my_python_script DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION} )

# Mark executables for installation See
# http://docs.ros.org/melodic/api/catkin/html/howto/format1/building_executables.html
# install(TARGETS ${PROJECT_NAME}_node RUNTIME DESTINATION
# ${CATKIN_PACKAGE_BIN_DESTINATION} )

# Mark libraries for installation See
# http://docs.ros.org/melodic/api/catkin/html/howto/format1/building_libraries.html
# install(TARGETS ${PROJECT_NAME} ARCHIVE DESTINATION
# ${CATKIN_PACKAGE_LIB_DESTINATION} LIBRARY DESTINATION
# ${CATKIN_PACKAGE_LIB_DESTINATION} RUNTIME DESTINATION
# ${CATKIN_GLOBAL_BIN_DESTINATION} )

# Mark cpp header files for installation install(DIRECTORY
# include/${PROJECT_NAME}/ DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
# FILES_MATCHING PATTERN "*.h" PATTERN ".svn" EXCLUDE )

# Mark other files for installation (e.g. launch and bag files, etc.)
# install(FILES # myfile1 # myfile2 DESTINATION
# ${CATKIN_PACKAGE_SHARE_DESTINATION} )

install(
  TARGETS ftp_image_collect_node
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION})
install(DIRECTORY launch cfg image
        DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION})

# ##############################################################################
# Testing ##
# ##############################################################################

# Add gtest based cpp test target and link libraries
# catkin_add_gtest(${PROJECT_NAME}-test test/test_ftp_image_collect.cpp)
# if(TARGET ${PROJECT_NAME}-test) target_link_libraries(${PROJECT_NAME}-test
# ${PROJECT_NAME}) endif()

# Add folders to be run by python nosetests catkin_add_nosetests(test)
