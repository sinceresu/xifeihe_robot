#ifndef __ftp_image_collect_HPP
#define __ftp_image_collect_HPP

#include "ros/ros.h"
#include <iostream>
#include <stdio.h>
#include <iostream>
#include <stdio.h>
#include <mutex>
#include <thread>
#include <queue>
#include <std_msgs/Int32.h>
#include <std_msgs/String.h>
#include "sensor_msgs/Image.h"
#include "param_server/server.h"
#include <yidamsg/InspectedResult.h>
#include "cv_bridge/cv_bridge.h"
#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include "Common.hpp"

#include "H264Decoder.h"

#define PRINT_LOG [](const std::string &strLogMsg) { std::cout << strLogMsg << std::endl; }

class CImageCollect
{
public:
    typedef struct _NodeOptions
    {
        std::string station_id;
        int camera_id;
        // msgs
        std::string collect_visible_msg;
        std::string collect_infrared_msg;
        std::string readyimage_msg;
        std::string visible_image_msg;
        std::string visible_rtsp;
        std::string infrared_image_msg;
        std::string infrared_rtsp;
        std::string result_up_msg;
        std::string ftp_host;
        int ftp_port;
        std::string ftp_username;
        std::string ftp_pwd;
        std::string ftp_path;
        std::string http_up;
        std::string local_path;
    } NodeOptions;

private:
    ros::NodeHandle nh_;
    ros::Subscriber upload_test_sub, collect_visible_sub, collect_infrared_sub;
    ros::Publisher readyimage_pub, result_up_pub, upload_result_pub;

    _NodeOptions node_param;

    bool collect_visible_do_task;
    yidamsg::InspectedResult visible_result;
    bool collect_infrared_do_task;
    yidamsg::InspectedResult infrared_result;

public:
    param_server::Server *pserver;
    int device_type, watch_counter;

    H264Decoder h264_decoder;

private:
    void init();
    static int ftp_callback(void *owner, double dTotalToDownload, double dNowDownloaded, double dTotalToUpload, double dNowUploaded);
    void upload_file_handle(std_msgs::String &_msg, void *_p);
    void colect_visible_handle(yidamsg::InspectedResult &_msg, void *_p);
    void colect_infrared_handle(yidamsg::InspectedResult &_msg, void *_p);
    void pub_monitor_result(int _robot_id, int _task_id, int _object_id, int _point_id, std::string _v_url, std::string _i_url);

public:
    CImageCollect(const ros::NodeHandle &nh = ros::NodeHandle("~"));
    ~CImageCollect();
    void upload_file_callback(const std_msgs::String &msg);
    void colect_visible_callback(const yidamsg::InspectedResult &msg);
    void colect_infrared_callback(const yidamsg::InspectedResult &msg);
    void update();
    void reset();
    void callback(param_server::SimpleType &config)
    {
        for (auto &kv : config)
        {
            ROS_INFO("callback key:%s value:%s", kv.first.c_str(), kv.second.c_str());
        }
        readConfig();
    }
    void readConfig()
    {
    }
};

#endif