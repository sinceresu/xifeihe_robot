#ifndef Global_HPP
#define Global_HPP

#pragma once
#include <string>
#include <vector>

bool CreateDirs(const std::string &dirPath);

int md5_encoded(const char *input, char *output);

char *md5_hex2c(unsigned char *in_md5_c, char *out_md5_c);

int string_match_len(const char *pattern, int patternLen, const char *string, int stringLen, int nocase);
int string_match(const char *pattern, const char *string, int nocase);
//
int match_string(std::string m_str, std::string w_str);

template <typename _ForwardIterator, typename _Tp>
std::vector<_Tp> vector_remove_ptr(std::vector<_Tp> &__vector, _ForwardIterator __first, _ForwardIterator __last, const _Tp &__value)
{
    __first = __vector.begin();
    while (__first != __vector.end() && __vector.size() > 0)
    {
        if ((*(*__first) == *__value))
        {
            __first = __vector.erase(__first);
        }
        else
        {
            ++__first;
        }
    }

    return __vector;
}

#endif
