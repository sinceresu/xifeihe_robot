#include "ftp_image_collect/ftp_image_collect.hpp"
#include "ftp/FTPClient.h"
#include <opencv2/opencv.hpp>

CImageCollect::CImageCollect(const ros::NodeHandle &nh) : nh_(nh)
{
    nh_.param<std::string>("station_id", node_param.station_id, "");
    nh_.param<int>("camera_id", node_param.camera_id, 1);
    nh_.param<std::string>("collect_visible_msg", node_param.collect_visible_msg, "");
    nh_.param<std::string>("collect_infrared_msg", node_param.collect_infrared_msg, "");
    nh_.param<std::string>("readyimage_msg", node_param.readyimage_msg, "");
    nh_.param<std::string>("visible_image_msg", node_param.visible_image_msg, "");
    nh_.param<std::string>("visible_rtsp", node_param.visible_rtsp, "");
    nh_.param<std::string>("infrared_image_msg", node_param.infrared_image_msg, "");
    nh_.param<std::string>("infrared_rtsp", node_param.infrared_rtsp, "");
    nh_.param<std::string>("result_up_msg", node_param.result_up_msg, "");
    nh_.param<std::string>("ftp_host", node_param.ftp_host, "");
    nh_.param<int>("ftp_port", node_param.ftp_port, 21);
    nh_.param<std::string>("ftp_username", node_param.ftp_username, "");
    nh_.param<std::string>("ftp_pwd", node_param.ftp_pwd, "");
    nh_.param<std::string>("ftp_path", node_param.ftp_path, "");
    nh_.param<std::string>("http_up", node_param.http_up, "");
    nh_.param<std::string>("local_path", node_param.local_path, "");
    //
    upload_test_sub = nh_.subscribe("/ftp/upload/file", 1, &CImageCollect::upload_file_callback, this);
    // collect_visible_sub = nh_.subscribe(node_param.collect_visible_msg, 1, &CImageCollect::colect_visible_callback, this);
    collect_infrared_sub = nh_.subscribe(node_param.collect_infrared_msg, 1, &CImageCollect::colect_infrared_callback, this);
    readyimage_pub = nh_.advertise<std_msgs::String>(node_param.readyimage_msg, 1);
    result_up_pub = nh_.advertise<yidamsg::InspectedResult>(node_param.result_up_msg, 1);
    upload_result_pub = nh_.advertise<std_msgs::String>("/ftp/upload/result", 1);
    // std::unique_ptr<param_server::Server> ptr(new param_server::Server("ftp_image_collect","cfg/config.yml"));
    // pserver = std::move(ptr);
    pserver = new param_server::Server("ftp_image_collect", "cfg/config.yml");
    param_server::CallbackType f = boost::bind(&CImageCollect::callback, this, _1); //绑定回调函数
    pserver->setCallback(f);
    readConfig();
    //
    init();
}

CImageCollect::~CImageCollect()
{
    if (pserver != NULL)
        delete pserver;
}

void CImageCollect::init()
{
    collect_visible_do_task = false;
    collect_infrared_do_task = false;
    //
    // test
    // embeddedmz::test();

    // std::string local_file = "/home/lihao/Workspace/test_data/test.wav";
    // std::string target_file = "/home/ftpsuser/test_data/test.wav";
    // std::shared_ptr<embeddedmz::CFTPClient> ftp_client;
    // ftp_client.reset(new embeddedmz::CFTPClient(PRINT_LOG));
    // bool is_init = ftp_client->InitSession(node_param.ftp_host, node_param.ftp_port, node_param.ftp_username, node_param.ftp_pwd,
    //                                         embeddedmz::CFTPClient::FTP_PROTOCOL::FTPS, embeddedmz::CFTPClient::SettingsFlag::ALL_FLAGS);
    // ftp_client->SetInsecure(true);
    // if (is_init)
    // {
    //     ftp_client->SetActive(true);
    //     ftp_client->SetProgressFnCallback(this, &CImageCollect::ftp_callback);
    //     bool ok = ftp_client->UploadFile(local_file, target_file, true);
    //     printf("UploadFile %s.\r\n", ok ? "true" : "fasle");
    // }
}

int CImageCollect::ftp_callback(void *owner, double dTotalToDownload, double dNowDownloaded, double dTotalToUpload, double dNowUploaded)
{
    printf("progress dTotalToDownload:%f , dNowDownloaded:%f , dTotalToUpload:%f , dNowUploaded:%f .\r\n",
           dTotalToDownload, dNowDownloaded, dTotalToUpload, dNowUploaded);
    return 0;
}

void CImageCollect::upload_file_handle(std_msgs::String &_msg, void *_p)
{
    std::vector<std::string> msg_list;
    msg_list.clear();
    SplitString(_msg.data, ";", msg_list);
    if (msg_list.size() < 2)
    {
        printf("msg error.\r\n");
        return;
    }
    std::string local_file = msg_list[0];
    std::string target_file = node_param.ftp_path + "/" + msg_list[1];
    try
    {
        std::shared_ptr<embeddedmz::CFTPClient> ftp_client;
        ftp_client.reset(new embeddedmz::CFTPClient(PRINT_LOG));
        bool is_init = ftp_client->InitSession(node_param.ftp_host, node_param.ftp_port, node_param.ftp_username, node_param.ftp_pwd,
                                               embeddedmz::CFTPClient::FTP_PROTOCOL::FTPS, embeddedmz::CFTPClient::SettingsFlag::ALL_FLAGS);
        ftp_client->SetInsecure(true);
        if (is_init)
        {
            ftp_client->SetActive(true);
            ftp_client->SetProgressFnCallback(this, &CImageCollect::ftp_callback);
            bool ok = ftp_client->UploadFile(local_file, target_file, true);

            sleep(1);
            std::string ret = (ok ? "true:" : "fasle:") + target_file;
            std_msgs::String do_msg;
            do_msg.data = ret;
            upload_result_pub.publish(do_msg);
            printf("UploadFile %s.\r\n", ok ? "true" : "fasle");
        }
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << '\n';
    }
}

void CImageCollect::colect_visible_handle(yidamsg::InspectedResult &_msg, void *_p)
{
    std::vector<std::string> equipid_list;
    equipid_list.clear();
    SplitString(_msg.equipid, "/", equipid_list);
    int mLen = equipid_list.size();
    std::vector<std::string> point_info_list;
    point_info_list.clear();
    SplitString(equipid_list[0], ":", point_info_list);
    // 年 / 月 / 日 / 巡视任务编码 / FIR / 巡检点位ID_机器人编码_时间.jpg
    std::string name = point_info_list[0] + "_" + equipid_list[mLen - 2] + "_" + std::to_string(GetTimeStamp()) + ".jpg";
    std::string local_file = node_param.local_path + "/" + name;
    std::string area_path = GetCurrentDate("%Y") + "/" + GetCurrentDate("%m") + "/" + GetCurrentDate("%d") + "/" + equipid_list[mLen - 1] + "/CCD/";
    std::string target_file = node_param.ftp_path + "/" + area_path + "/" + name;

    try
    {
        bool b_done = false;
        ros::Time last_time = ros::Time::now();
        ros::Time attemp_end = last_time + ros::Duration(10.0);
        ros::Rate rate(1);
        while (ros::ok() && !b_done)
        {
            // boost::shared_ptr<sensor_msgs::Image const> visible_image_sub;
            // visible_image_sub = ros::topic::waitForMessage<sensor_msgs::Image>(node_param.visible_image_msg, ros::Duration(1.0));
            // if (visible_image_sub != nullptr)
            // {
            //     std::vector<unsigned char> vc = visible_image_sub->data;
            //     unsigned char *pBuffer = &vc.at(0);
            //     int dwBufSize = vc.size();
            //     // if (pBuffer[4] == 0x67)
            //     if (h264_decoder.decode(pBuffer, dwBufSize))
            //     {
            //         cv::Mat g_pic = h264_decoder.getMat();
            //         cv::imwrite(local_file, g_pic);

            //         std::shared_ptr<embeddedmz::CFTPClient> ftp_client;
            //         ftp_client.reset(new embeddedmz::CFTPClient(PRINT_LOG));
            //         bool is_init = ftp_client->InitSession(node_param.ftp_host, node_param.ftp_port, node_param.ftp_username, node_param.ftp_pwd,
            //                                                embeddedmz::CFTPClient::FTP_PROTOCOL::FTPS, embeddedmz::CFTPClient::SettingsFlag::ALL_FLAGS);
            //         ftp_client->SetInsecure(true);
            //         if (is_init)
            //         {
            //             ftp_client->SetActive(true);
            //             ftp_client->SetProgressFnCallback(this, &CImageCollect::ftp_callback);
            //             bool ok = ftp_client->UploadFile(local_file, target_file, true);
            //             printf("UploadFile %s.\r\n", ok ? "true" : "fasle");
            //         }
            //         // std::string file_url = "ftp://" + node_param.ftp_host + ":" + std::to_string(node_param.ftp_port) + target_file;
            //         std::string file_url = area_path + "/" + name;
            //         // pub_monitor_result(atoi(equipid_list[mLen - 2].c_str()), atoi(equipid_list[mLen - 1].c_str()), 0, atoi(point_info_list[0].c_str()), file_url, "");
            //         break;
            //     }
            // }
            //
            // ------
            cv::VideoCapture capture;
            capture.open(node_param.visible_rtsp);
            while (capture.isOpened() && !b_done)
            {
                cv::Mat frame;
                capture >> frame;
                if (!frame.empty())
                {
                    cv::imwrite(local_file, frame);

                    std::shared_ptr<embeddedmz::CFTPClient> ftp_client;
                    ftp_client.reset(new embeddedmz::CFTPClient(PRINT_LOG));
                    bool is_init = ftp_client->InitSession(node_param.ftp_host, node_param.ftp_port, node_param.ftp_username, node_param.ftp_pwd,
                                                           embeddedmz::CFTPClient::FTP_PROTOCOL::FTPS, embeddedmz::CFTPClient::SettingsFlag::ALL_FLAGS);
                    ftp_client->SetInsecure(true);
                    if (is_init)
                    {
                        ftp_client->SetActive(true);
                        ftp_client->SetProgressFnCallback(this, &CImageCollect::ftp_callback);
                        bool ok = ftp_client->UploadFile(local_file, target_file, true);
                        printf("UploadFile %s.\r\n", ok ? "true" : "fasle");
                    }
                    // std::string file_url = "ftp://" + node_param.ftp_host + ":" + std::to_string(node_param.ftp_port) + target_file;
                    std::string file_url = area_path + "/" + name;
                    // pub_monitor_result(atoi(equipid_list[mLen - 2].c_str()), atoi(equipid_list[mLen - 1].c_str()), 0, atoi(point_info_list[0].c_str()), file_url, "");
                    b_done = true;
                    break;
                }
                if (ros::Time::now() > attemp_end)
                {
                    b_done = true;
                    break;
                }
            }
            if (capture.isOpened())
            {
                capture.release();
            }
            // ------
            if (ros::Time::now() > attemp_end)
            {
                b_done = true;
                break;
            }

            ros::spinOnce();
            rate.sleep();
        }
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << '\n';
    }

    sleep(3);
    std_msgs::String do_msg;
    readyimage_pub.publish(do_msg);
}

void CImageCollect::colect_infrared_handle(yidamsg::InspectedResult &_msg, void *_p)
{
    std::vector<std::string> equipid_list;
    equipid_list.clear();
    SplitString(_msg.equipid, "/", equipid_list);
    int mLen = equipid_list.size();
    std::vector<std::string> point_info_list;
    point_info_list.clear();
    SplitString(equipid_list[0], ":", point_info_list);
    // 年 / 月 / 日 / 巡视任务编码 / FIR / 巡检点位ID_机器人编码_时间.jpg
    std::string name = point_info_list[0] + "_" + equipid_list[mLen - 2] + "_" + std::to_string(GetTimeStamp()) + ".jpg";
    std::string local_file = node_param.local_path + "/" + name;
    std::string area_path = GetCurrentDate("%Y") + "/" + GetCurrentDate("%m") + "/" + GetCurrentDate("%d") + "/" + equipid_list[mLen - 1] + "/FIR/";
    std::string target_file = node_param.ftp_path + "/" + area_path + "/" + name;

    try
    {
        bool b_done = false;
        ros::Time last_time = ros::Time::now();
        ros::Time attemp_end = last_time + ros::Duration(10.0);
        ros::Rate rate(1);
        while (ros::ok() && !b_done)
        {
            // boost::shared_ptr<sensor_msgs::Image const> infrared_image_sub;
            // infrared_image_sub = ros::topic::waitForMessage<sensor_msgs::Image>(node_param.infrared_image_msg, ros::Duration(1.0));
            // if (infrared_image_sub != nullptr)
            // {
            //     std::vector<unsigned char> vc = infrared_image_sub->data;
            //     unsigned char *pBuffer = &vc.at(0);
            //     int dwBufSize = vc.size();
            //     // if (pBuffer[4] == 0x67)
            //     if (h264_decoder.decode(pBuffer, dwBufSize))
            //     {
            //         cv::Mat g_pic = h264_decoder.getMat();
            //         cv::imwrite(local_file, g_pic);

            //         std::shared_ptr<embeddedmz::CFTPClient> ftp_client;
            //         ftp_client.reset(new embeddedmz::CFTPClient(PRINT_LOG));
            //         bool is_init = ftp_client->InitSession(node_param.ftp_host, node_param.ftp_port, node_param.ftp_username, node_param.ftp_pwd,
            //                                                embeddedmz::CFTPClient::FTP_PROTOCOL::FTPS, embeddedmz::CFTPClient::SettingsFlag::ALL_FLAGS);
            //         ftp_client->SetInsecure(true);
            //         if (is_init)
            //         {
            //             ftp_client->SetActive(true);
            //             ftp_client->SetProgressFnCallback(this, &CImageCollect::ftp_callback);
            //             bool ok = ftp_client->UploadFile(local_file, target_file, true);
            //             printf("UploadFile %s.\r\n", ok ? "true" : "fasle");
            //         }
            //         // std::string file_url = "ftp://" + node_param.ftp_host + ":" + std::to_string(node_param.ftp_port) + target_file;
            //         std::string file_url = area_path + "/" + name;
            //         // pub_monitor_result(atoi(equipid_list[mLen - 2].c_str()), atoi(equipid_list[mLen - 1].c_str()), 0, atoi(point_info_list[0].c_str()), "", file_url);
            //         b_done = true;
            //         break;
            //     }
            // }
            // ------
            cv::VideoCapture capture;
            capture.open(node_param.infrared_rtsp);
            while (capture.isOpened() && !b_done)
            {
                cv::Mat frame;
                capture >> frame;
                if (!frame.empty())
                {
                    cv::imwrite(local_file, frame);
                    std::shared_ptr<embeddedmz::CFTPClient> ftp_client;
                    ftp_client.reset(new embeddedmz::CFTPClient(PRINT_LOG));
                    bool is_init = ftp_client->InitSession(node_param.ftp_host, node_param.ftp_port, node_param.ftp_username, node_param.ftp_pwd,
                                                           embeddedmz::CFTPClient::FTP_PROTOCOL::FTPS, embeddedmz::CFTPClient::SettingsFlag::ALL_FLAGS);
                    ftp_client->SetInsecure(true);
                    if (is_init)
                    {
                        ftp_client->SetActive(true);
                        ftp_client->SetProgressFnCallback(this, &CImageCollect::ftp_callback);
                        bool ok = ftp_client->UploadFile(local_file, target_file, true);
                        printf("UploadFile %s.\r\n", ok ? "true" : "fasle");
                    }
                    // std::string file_url = "ftp://" + node_param.ftp_host + ":" + std::to_string(node_param.ftp_port) + target_file;
                    std::string file_url = area_path + "/" + name;
                    // pub_monitor_result(atoi(equipid_list[mLen - 2].c_str()), atoi(equipid_list[mLen - 1].c_str()), 0, atoi(point_info_list[0].c_str()), "", file_url);
                    b_done = true;
                    break;
                }
                if (ros::Time::now() > attemp_end)
                {
                    b_done = true;
                    break;
                }
            }
            if (capture.isOpened())
            {
                capture.release();
            }
            // ------
            if (ros::Time::now() > attemp_end)
            {
                b_done = true;
                break;
            }

            ros::spinOnce();
            rate.sleep();
        }
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << '\n';
    }

    sleep(3);
    std_msgs::String do_msg;
    readyimage_pub.publish(do_msg);
}
void CImageCollect::pub_monitor_result(int _robot_id, int _task_id, int _object_id, int _point_id,
                                       std::string _v_url, std::string _i_url)
{
    yidamsg::InspectedResult up_msg;
    // up_msg.camid;
    // up_msg.picid;
    // up_msg.x;
    // up_msg.y;
    // up_msg.z;
    // up_msg.equipimage;
    // up_msg.nameplates;
    // up_msg.equipid;
    // up_msg.result;
    // up_msg.success;
    result_up_pub.publish(up_msg);
}

void CImageCollect::update()
{
}

void CImageCollect::upload_file_callback(const std_msgs::String &msg)
{
    std::thread test_th = std::thread(std::bind(&CImageCollect::upload_file_handle, this, msg, nullptr));
    test_th.detach();
}

void CImageCollect::colect_visible_callback(const yidamsg::InspectedResult &msg)
{
    collect_visible_do_task = true;
    visible_result = msg;

    std::thread visible_th = std::thread(std::bind(&CImageCollect::colect_visible_handle, this, msg, nullptr));
    visible_th.detach();
}

void CImageCollect::colect_infrared_callback(const yidamsg::InspectedResult &msg)
{
    collect_infrared_do_task = true;
    infrared_result = msg;

    std::thread infrared_th = std::thread(std::bind(&CImageCollect::colect_infrared_handle, this, msg, nullptr));
    infrared_th.detach();
}

void CImageCollect::reset()
{
}