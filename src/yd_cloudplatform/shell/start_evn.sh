hik_sdk="/libs"
tianboir_sdk="/libs/tianboir"

work_path=$(dirname $(readlink -f $0))
root_path=$(dirname ${work_path})

hik_sdk_dir=${root_path}${hik_sdk}
tianboir_sdk_dir=${root_path}${tianboir_sdk}
echo ${hik_sdk_dir}
echo ${tianboir_sdk_dir}

export LD_LIBRARY_PATH=${hik_sdk_dir}:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=${tianboir_sdk_dir}:$LD_LIBRARY_PATH

roslaunch yd_cloudplatform tianboir_sdk_control.launch
