#include "yd_cloudplatform/camera.h"
#include <thread>
#include <chrono>
#include <algorithm>
#include <math.h>

#include <Eigen/Core>
#include <opencv2/opencv.hpp>
#include<opencv2/core/eigen.hpp>


using namespace std;
namespace {
Eigen::Matrix4d GetRefMats(const vector<Eigen::Matrix4d> &refs, const vector<double> &rads)
{
    assert(refs.size() == rads.size());
    int num = refs.size();
    Eigen::Matrix4d refinseries;
    refinseries.setIdentity();
    Eigen::Matrix4d motionframes;
    for (int i = 0; i < num; i++)
    {
        motionframes = Eigen::Matrix4d::Identity();
        motionframes.block<3, 3>(0, 0) = (Eigen::AngleAxisd(rads[i]* M_PI / 180.0, Eigen::Vector3d::UnitZ())).matrix();
        refinseries = refinseries * refs[i] * motionframes;

        // motionframes = Eigen::Matrix4d::Identity();
	    // motionframes.block<3, 3>(0, 0) = (Eigen::AngleAxisd(rads[1] * M_PI / 180.0, Eigen::Vector3d::UnitX())).matrix();
	    // refinseries = refinseries * refs[1] * motionframes;
    }
    return refinseries;
}

Eigen::Matrix4d ForwardKinematics(const vector<Eigen::Matrix4d> &refs, const vector<double> &rads, Eigen::Matrix4d &tool)
{
    return GetRefMats(refs, rads) * tool;
}
}

Camera::Camera() 
{
} 

int Camera::LoadCalibrationFile(const std::string& calibration_filepath, std::string cam_type) {
  extrinsic_mats_.clear();
	cv::FileStorage fs;

  fs.open(calibration_filepath, cv::FileStorage::READ);
  if(!fs.isOpened()){
      // LOG(FATAL) << "can't open calibration file " << calibration_filepath <<  "." ;
      return -5;
  }
  int num = fs["num"];
  for (int i = 0; i < num; ++i)
  {
      cv::Mat cv_RT;
      string index("Matrix_");
      index = index + to_string(i);
      fs[index] >> cv_RT;
      Eigen::Matrix4d eigen_RT;
      cv::cv2eigen(cv_RT, eigen_RT);
      // cout << eigen_RT << endl;
      extrinsic_mats_.push_back(eigen_RT);

  }
  cv::Mat cv_RT;
  fs["Tool"] >> cv_RT;
  cv::cv2eigen(cv_RT, tool_mat_);
  // cout << tool << endl;

  cv::Mat cam_extrinsic;
  if(cam_type == "rgb")
  {
    fs["CameraExtrinsicMat"] >> cam_extrinsic;
    cv::cv2eigen(cam_extrinsic, cam_extrinsic_mat_);
    
    fs["CameraMat"] >> intrinsic_mat_;
    fs["DistCoeff"] >> distortion_coeffs_;
    fs["ImageSize"] >> image_size_; 
  }
  if(cam_type == "ir")
  {
    fs["IRColorCameraExtrinsicMat"] >> cam_extrinsic;
    cv::cv2eigen(cam_extrinsic, cam_extrinsic_mat_);
    
    fs["IRCameraMat"] >> intrinsic_mat_;
    fs["IRDistCoeff"] >> distortion_coeffs_;
    fs["IRImageSize"] >> image_size_;
  }

  // centered_intrinsic_mat_ =cv::getDefaultNewCameraMatrix ( intrinsic_mat_, image_size_, true);
  // cv::initUndistortRectifyMap(intrinsic_mat_, distortion_coeffs_, cv::Mat(), centered_intrinsic_mat_, image_size_, CV_32FC1, map_x_, map_y_);

  CalculateFov();

  return 0;

 }
 
void Camera::CalculateFov() {
  horizontal_fov_ = 2 * atan2(image_size_.width/2, intrinsic_mat_.at<double>(0, 0)) * 180 / M_PI;
  vertical_fov_ = 2 * atan2(image_size_.height/2, intrinsic_mat_.at<double>(1, 1))* 180 / M_PI;
}

Eigen::Affine3d Camera::GetRelativePose(const std::vector<double> &rads) {
  auto cam_pose = ForwardKinematics(extrinsic_mats_, rads, tool_mat_);
  // LOG(INFO) << "cam_pose: " <<  std::endl <<  cam_pose ;
  return Eigen::Affine3d(cam_pose * cam_extrinsic_mat_);
};

int Camera::GetCameraFov(double &horizontal_fov, double& vertical_fov) {
  if (intrinsic_mat_.empty()) {
    return -4;
  }
  horizontal_fov = horizontal_fov_;
  vertical_fov = vertical_fov_;
  return 0;
}

int Camera::GetCameraSize(cv::Size &size) {
  size = image_size_;
  return 0;
}

