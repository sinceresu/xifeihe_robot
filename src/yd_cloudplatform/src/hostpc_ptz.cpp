#include <ros/ros.h>
#include <pthread.h>
#include <string>
#include <math.h>

#include "yd_cloudplatform/serial_comm.hpp"
#include "yd_cloudplatform/hostpc_ptz.hpp"
#include "yd_cloudplatform/common.hpp"


void hostpc_ptz::control_mode_step(int cloudplatform_id, string direction, int step)
{
    int diff_value = 0;
    int val_panpos = 0;
    int val_tiltpos = 0;

	if(step == 1)
		diff_value = _step_1;
	else if(step == 2)
		diff_value = _step_2;
	else if(step == 3)
		diff_value = _step_3;

    yd_cloudplatform::CloudPlatControl cloudplat_control;

    /*cloudplat_control.request.id = _cloudplatform_id;
    cloudplat_control.request.action = 0;
    cloudplat_control.request.type = 0;
    cloudplat_control.request.value = 0;
    if(cloudplatform_client.call(cloudplat_control))
    {
        val_panpos = cloudplat_control.response.result;
        std::cout << "get xy degree success!" << std::endl;
    }
    else
        std::cout << "get xy degree failed!" << std::endl;
    sleep(0.5);

    cloudplat_control.request.id = _cloudplatform_id;
    cloudplat_control.request.action = 0;
    cloudplat_control.request.type = 1;
    cloudplat_control.request.value = 0;
    if(cloudplatform_client.call(cloudplat_control))
    {
        val_tiltpos = cloudplat_control.response.result;
        std::cout << "get z degree success!" << std::endl;
    }
    else
        std::cout << "get z degree failed!" << std::endl;
    sleep(0.5);*/

	if(direction == "right")
    {
		val_panpos = _now_xyposition + diff_value;
		if(val_panpos > 35999)
			val_panpos = val_panpos - 35999;
    }
	if(direction == "left")
    {
		val_panpos = _now_xyposition - diff_value;
		if(val_panpos < 0)
			val_panpos = 35999 + val_panpos;
    }
    if((direction == "right") || (direction == "left"))
    {
        cloudplat_control.request.id = _cloudplatform_id;
        cloudplat_control.request.action = 1;
        cloudplat_control.request.type = 0;
        cloudplat_control.request.value = val_panpos;
        if(cloudplatform_client.call(cloudplat_control))
        {
            std::cout << "set xy degree success!" << std::endl;
        }
        else
            std::cout << "set xy degree failed!" << std::endl;
        sleep(0.5);
    }

	//0=horizontally 90=straight down 270=straight up
	//range 0-9000 and 27000-35999		
	if(direction == "up")
    {
		val_tiltpos = _now_zposition - diff_value;
        std::cout << "val_tiltpos_result: " << val_tiltpos << " , diff_value: " << diff_value << std::endl;
		if((val_tiltpos >= _z_min_degree) && (val_tiltpos < _z_max_degree))
			val_tiltpos = _z_max_degree;
		else if(val_tiltpos < 0)
			val_tiltpos = _z_origin_degree + val_tiltpos;
        std::cout << "val_tiltpos_set: " << val_tiltpos << std::endl;
    }
	if(direction == "down")
    {
		val_tiltpos = _now_zposition + diff_value;
        std::cout << "val_tiltpos_result: " << val_tiltpos << " , diff_value: " << diff_value << std::endl;
		if ((val_tiltpos > _z_min_degree) && (val_tiltpos <= _z_max_degree))
			val_tiltpos = _z_min_degree;
		else if(val_tiltpos > _z_origin_degree)
			val_tiltpos = val_tiltpos - _z_origin_degree;
        std::cout << "val_tiltpos_set: " << val_tiltpos << std::endl;
    }
    if((direction == "up") || (direction == "down"))
    {
        cloudplat_control.request.id = _cloudplatform_id;
        cloudplat_control.request.action = 1;
        cloudplat_control.request.type = 1;
        cloudplat_control.request.value = val_tiltpos;
        if(cloudplatform_client.call(cloudplat_control))
        {
            std::cout << "set z degree success!" << std::endl;
        }
        else
            std::cout << "set z degree failed!" << std::endl;
        sleep(0.5);
    }
    //sleep(2);
}

void hostpc_ptz::control_mode_value(int cloudplatform_id, int horizontal, int vertical, int focus)
{
	// if(focus < 0)
	// 	focus = 0;
	// else if(focus > 29)
	// 	focus = 29;
	// int val_zoom = _camera_focus_value[focus];

	yd_cloudplatform::CloudPlatControl cloudplat_control;
	
    if(_use_hk_cloudplatform)
    {
        yd_cloudplatform::CloudPlatControl cloudplat_control;
        cloudplat_control.request.id = _cloudplatform_id;
        cloudplat_control.request.action = 1;
        cloudplat_control.request.type = 4;
        cloudplat_control.request.value = 0;
        vector<unsigned int> value;
        value.push_back((int)horizontal);
        value.push_back((int)vertical);
        // value.push_back(val_zoom * 5 + 5);
        value.push_back(focus);
        cloudplat_control.request.allvalue = value;
        if(cloudplatform_client.call(cloudplat_control))
            std::cout << "set xyz_zoom degree success!" << std::endl;
        else
            std::cout << "set xyz_zoom degree failed!" << std::endl;
        sleep(0.4);
    }
    else{
        if((horizontal <= 36000) && (horizontal >= 0))
        {

            cloudplat_control.request.id = _cloudplatform_id;
            cloudplat_control.request.action = 1;
            cloudplat_control.request.type = 0;
            cloudplat_control.request.value = horizontal;
            if(cloudplatform_client.call(cloudplat_control))
            {
                std::cout << "set xy degree success!" << std::endl;
            }
            else
                std::cout << "set xy degree failed!" << std::endl;
            sleep(0.5);
        }
        else
        {
            level = 1;
            message = "got a wrong horizontal value!";
        }
            
        if(((vertical >= 0) && (vertical <= 9000)) || ((vertical >= 27000) && (vertical <= 36000)))
        {
            cloudplat_control.request.id = _cloudplatform_id;
            cloudplat_control.request.action = 1;
            cloudplat_control.request.type = 1;
            cloudplat_control.request.value = vertical;
            if(cloudplatform_client.call(cloudplat_control))
            {
                std::cout << "set z degree success!" << std::endl;
            }
            else
                std::cout << "set z degree failed!" << std::endl;
            sleep(0.5);
        }
        else
        {
            level = 1;
            message = "got a wrong vertical value!";
        }
        
        cloudplat_control.request.id = _cloudplatform_id;
        cloudplat_control.request.action = 1;
        cloudplat_control.request.type = 2;
        // cloudplat_control.request.value = val_zoom;
        cloudplat_control.request.value = focus;
        if(cloudplatform_client.call(cloudplat_control))
        {
            std::cout << "set z degree success!" << std::endl;
        }
        else
            std::cout << "set z degree failed!" << std::endl;
        sleep(1);
    }
}

void hostpc_ptz::control_mode_run(int cloudplatform_id, string direction)
{
    yd_cloudplatform::CloudPlatControl cloudplat_control;
    cloudplat_control.request.id = _cloudplatform_id;
	cloudplat_control.request.action = 2;
	cloudplat_control.request.type = 0;
	cloudplat_control.request.value = 0;

	if(direction == "right")
		cloudplat_control.request.type = 0;
	else if(direction == "left")
		cloudplat_control.request.type = 1;
	else if(direction == "up")
		cloudplat_control.request.type = 2;
	else if(direction == "down")
		cloudplat_control.request.type = 3;
    else if(direction == "forward")
		cloudplat_control.request.type = 4;
    else if(direction == "backward")
		cloudplat_control.request.type = 5;
    else
        std::cout << "direction type is errpor!" << std::endl;

    if(cloudplatform_client.call(cloudplat_control))
    {
        std::cout << "set cmd success!" << std::endl;
    }
    else
        std::cout << "set cmd failed!" << std::endl;
    sleep(0.5);
}

void hostpc_ptz::control_mode_stop(int cloudplatform_id)
{
    yd_cloudplatform::CloudPlatControl cloudplat_control;
    cloudplat_control.request.id = _cloudplatform_id;
    cloudplat_control.request.action = 3;
    cloudplat_control.request.type = 0;
    cloudplat_control.request.value = 0;
    if(cloudplatform_client.call(cloudplat_control))
    {
        std::cout << "set cmd success!" << std::endl;
    }
    else
        std::cout << "set cmd failed!" << std::endl;
}

void hostpc_ptz::nowposition_callback(const nav_msgs::Odometry& msg)
{
    _now_xyposition = msg.pose.pose.position.x;
    _now_zposition = msg.pose.pose.position.z;
}

void hostpc_ptz::yuntaictrl_callback(const yidamsg::manualControlParameters& msg)
{

    if(msg.control_type == 1)
    {
		if(msg.action == 1)
			control_mode_step(_cloudplatform_id, msg.direction, msg.step);
		else if(msg.action == 2)
			control_mode_value(_cloudplatform_id, msg.horizontal, msg.vertical, msg.focus);
		else if(msg.action == 3)
			control_mode_run(_cloudplatform_id, msg.direction);
		else if(msg.action == 4)
			control_mode_stop(_cloudplatform_id);
    }
	else
    {
        std::cout << "get msg is not control cloudplatform." << std::endl;
    }

    /*yd_cloudplatform::CloudPlatControl cloudplat_control;
    cloudplat_control.request.id = _cloudplatform_id;
    cloudplat_control.request.action = 1;
    cloudplat_control.request.type = 0;
    cloudplat_control.request.value = int(finalx_set);
    if(cloudplatform_client.call(cloudplat_control))
        std::cout << "set xy degree success!" << std::endl;
    else
        std::cout << "set xy degree failed!" << std::endl;
    
    cloudplat_control.request.id = _cloudplatform_id;
    cloudplat_control.request.action = 1;
    cloudplat_control.request.type = 1;
    cloudplat_control.request.value = int(finalz_set);
    if(cloudplatform_client.call(cloudplat_control))
        std::cout << "set z degree success!" << std::endl;
    else
        std::cout << "set z degree failed!" << std::endl;

    cloudplat_control.request.id = _cloudplatform_id;
    cloudplat_control.request.action = 1;
    cloudplat_control.request.type = 2;
    cloudplat_control.request.value = zoom_set;
    if(cloudplatform_client.call(cloudplat_control))
        std::cout << "set zoom degree success!" << std::endl;
    else
        std::cout << "set zoom degree failed!" << std::endl;*/
}

hostpc_ptz::hostpc_ptz(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle)
    :node_handle_(node_handle), private_node_handle_(private_node_handle)
{
    std::string camera_focus_value_str, xyz_degree_str, heartbeat_topic_str;
    private_node_handle_.param<int>("use_hk_cloudplatform", _use_hk_cloudplatform, 1);
    private_node_handle_.param<int>("cloudplatform_id", _cloudplatform_id, 1);
    private_node_handle_.param<int>("step_1", _step_1, 100);
    private_node_handle_.param<int>("step_2", _step_2, 1000);
    private_node_handle_.param<int>("step_3", _step_3, 5000);
    private_node_handle_.param<std::string>("camera_focus_value", camera_focus_value_str, "0x0, 0x10b9");
    private_node_handle_.param<std::string>("xyz_degree", xyz_degree_str, "0,35999,35999,27000,9000");
    private_node_handle_.param<std::string>("heartbeat_topic_string", heartbeat_topic_str, "/yida/heartbeat");

    vector<std::string> degree_value_strv, camera_focus_value_strv;
    SplitString(camera_focus_value_str, camera_focus_value_strv, ",");
    for(int i = 0; i < camera_focus_value_strv.size(); i++)
        _camera_focus_value.push_back(atoi(camera_focus_value_strv[i].c_str()));
    SplitString(xyz_degree_str, degree_value_strv, ",");
    _x_min_degree = atoi(degree_value_strv[0].c_str());
    _x_max_degree = atoi(degree_value_strv[1].c_str());
    _z_origin_degree = atoi(degree_value_strv[2].c_str());
    _z_max_degree = atoi(degree_value_strv[3].c_str());
    _z_min_degree = atoi(degree_value_strv[4].c_str());

    level = 0;
    message = "cloutplatform is ok!";

    heartbeat_pub_ = node_handle_.advertise<diagnostic_msgs::DiagnosticArray>(heartbeat_topic_str, 1);
    manual_control_sub_ = node_handle_.subscribe("/ydserver/manual_control", 1, &hostpc_ptz::yuntaictrl_callback, this);
    now_xyzposition_sub_ = node_handle_.subscribe("/yida/yuntai/position", 1, &hostpc_ptz::nowposition_callback, this);
	cloudplatform_client = node_handle_.serviceClient<yd_cloudplatform::CloudPlatControl>("/yida/internal/platform_cmd");
}
    
hostpc_ptz::~hostpc_ptz()
{

}

void hostpc_ptz::update()
{
    pub_heartbeat(level, message);
}

void hostpc_ptz::pub_heartbeat(int level, string message)
{
    diagnostic_msgs::DiagnosticArray log;
    log.header.stamp = ros::Time::now();

    diagnostic_msgs::DiagnosticStatus s;
    s.name = __app_name__;             // 这里写节点的名字
    s.level = level;                   // 0 = OK, 1 = Warn, 2 = Error
    if (!message.empty())
    {
        s.message = message;           // 问题描述
    }
    s.hardware_id = "cloutplatform";   // 硬件信息
    log.status.push_back(s);
    sleep(0.1);
    heartbeat_pub_.publish(log);
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, __app_name__);
    ros::NodeHandle node_handle;
    ros::NodeHandle private_node_handle("~");
    hostpc_ptz visible_ptz_obj(node_handle, private_node_handle);

    ros::Rate rate(20);
    while(ros::ok())
    {
        visible_ptz_obj.update();
        ros::spinOnce();
        rate.sleep();
    }

    return 0;
}
