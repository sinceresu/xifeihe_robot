#include <ros/ros.h>
#include <pthread.h>
#include <string>
#include <math.h>
#include <thread>

#include "yd_cloudplatform/serial_comm.hpp"
#include "yd_cloudplatform/visible_ptz.hpp"
#include "yd_cloudplatform/common.hpp"

void *sub_run(void* args)
{
    visible_ptz * _this = (visible_ptz *)args;
    _this->image_sub_ = _this->node_handle_.subscribe("/yida/internal/visible/image_proc/compressed", 1, &visible_ptz::image_callback, _this);
    ros::spin();
	return nullptr;
}

int visible_ptz::get_zoomset(vector<float> camera_position, vector<float> device_position, int device_width, int device_height, int zoom_scale)
{
    int finalzoom_set = 8;

	float x_distance = device_position[0] - camera_position[0];
	float y_distance = device_position[1] - camera_position[1];
	float z_distance = device_position[2] - camera_position[2];
	float zoom_distancevalue = sqrt(pow(x_distance, 2) + pow(y_distance, 2) + pow(z_distance,2));

	if((device_width == 0) || (device_height == 0))
    {
		finalzoom_set = 8;
		std::cout << "device size is 00000000000000000000000 !" << std::endl;
    }
	else
    {
		float f_width = _camera_cmos_width * zoom_distancevalue * 1000 / device_width / zoom_scale;
		float f_height = _camera_cmos_height * zoom_distancevalue * 1000 / device_height / zoom_scale;
        std::cout << "f_width result is: " << f_width << ", f_height is: " << f_height << "." << std::endl;

		int w_finalzoom_set = finalzoom_set;
		int h_finalzoom_set = finalzoom_set;
		double w_zoom_focus_mm = _zoom_focus_mm;
		double h_zoom_focus_mm = _zoom_focus_mm;
		// 
		for(int i = 0; i < _camera_focus_number- 2; i++)
        {
			if ((f_width >= _camera_focus_mm[i]) && (f_width <= _camera_focus_mm[i+1]))
            {
				w_finalzoom_set = _camera_focus_value[i];
				w_zoom_focus_mm = _camera_focus_mm[i];
            }
			else if (f_width >= _camera_focus_mm[_camera_focus_number - 2])
            {
				w_finalzoom_set = _camera_focus_value[_camera_focus_number - 1];
				w_zoom_focus_mm = _camera_focus_mm[_camera_focus_number - 1];
            }
        }
		// 
        for(int i = 0; i < _camera_focus_number- 2; i++)
        {
			if ((f_height >= _camera_focus_mm[i]) && (f_height <= _camera_focus_mm[i+1]))
            {
				h_finalzoom_set = _camera_focus_value[i];
				h_zoom_focus_mm = _camera_focus_mm[i];
            }
			else if (f_height >= _camera_focus_mm[_camera_focus_number - 2])
            {
				h_finalzoom_set = _camera_focus_value[_camera_focus_number - 1];
				h_zoom_focus_mm = _camera_focus_mm[_camera_focus_number - 1];
            }
        }
		// 
		finalzoom_set = w_finalzoom_set >= h_finalzoom_set ? h_finalzoom_set : w_finalzoom_set;
		_zoom_focus_mm = w_zoom_focus_mm >= h_zoom_focus_mm ? h_zoom_focus_mm : w_zoom_focus_mm;
    }

	if(finalzoom_set < 8)
		finalzoom_set = 8;
	std::cout << "=====get_zoomset result is: " << finalzoom_set << "." << std::endl;
	return finalzoom_set;
}

float visible_ptz::get_xydegree(vector<float> camera_position, vector<float> device_position, vector<float> cloudplatform_position)
{
    float xydegree = 0.0;
	float vector_camera_x = camera_position[0] - cloudplatform_position[0];
	float vector_camera_y = camera_position[1] - cloudplatform_position[1];
	float vector_device_x = device_position[0] - cloudplatform_position[0];
	float vector_device_y = device_position[1] - cloudplatform_position[1];

	float center_camera_distance = sqrt(pow(vector_camera_x, 2) + pow(vector_camera_y, 2));
	float center_device_distance = sqrt(pow(vector_device_x, 2) + pow(vector_device_y, 2));

	float circle_x = cloudplatform_position[0];
	float circle_y = cloudplatform_position[1];
	float circle_r = sqrt(pow(vector_camera_x, 2) + pow(vector_camera_y, 2));
	//the  distance between o and device is center_device_distance
	if(center_device_distance <= circle_r)
    {
		std::cout << "the point is in circle!" << std::endl;
		return 0;
    }
	else if(center_device_distance > circle_r)
    {
		//点到切点距离
		float qiedian_device_distance = sqrt(pow(center_device_distance, 2) - pow(circle_r, 2));
		//计算切线与点心连线的夹角
		float f1 = acos(circle_r / center_device_distance)* 180 / PI;
		float f2 = atan2(vector_camera_y, vector_camera_x)* 180 / PI;
		float f3 = atan2(vector_device_y, vector_device_x)* 180 / PI;
		float f4 = 0;
        std::cout << "f1 = " << f1 << ", f2 = " << f2 << ", f3 = " << f3 << std::endl;

		/*
		if(f2 > 0){
            f4 = 360 - (f2-f3) - f1;
        }
		else{
			f4 = f3 - f2 - f1;
        }
        xydegree = 360 - f4;
		if(xydegree > 360){
			xydegree = -f4;
        }*/
         if(f3 > 0){
		    f4 = f1 + f3 - f2;
        }
        else{
            f4 = f1 + f3 - f2;
        }
		xydegree = 360 - f4;
		if (xydegree > 360){
			xydegree = -f4;
        }
    }
	xydegree = xydegree + _handlexy;
    std::cout << "xydegree in **************************" << xydegree << " ." << std::endl;
	return xydegree;
}

vector<float> visible_ptz::get_newpoint(vector<float> camera_position, vector<float> device_position, vector<float> cloudplatform_position, int xydegree)
{
    float vector_camera_x = camera_position[0] - cloudplatform_position[0];
	float vector_camera_y = camera_position[1] - cloudplatform_position[1];
	float vector_device_x = device_position[0] - cloudplatform_position[0];
	float vector_device_y = device_position[1] - cloudplatform_position[1];

	float circle_x = float(cloudplatform_position[0]);
	float circle_y = float(cloudplatform_position[1]);
	float circle_r = sqrt(pow(vector_camera_x, 2) + pow(vector_camera_y, 2));

	float x_diff = circle_r * cos(xydegree);
	float y_diff = circle_r * sin(xydegree);
	std::cout << "the x_diff is " << x_diff << ", the y_diff is " << y_diff << std::endl;

	float x_point_new = cloudplatform_position[0] + x_diff;
	float y_point_new = cloudplatform_position[1] + y_diff;

	vector<float> result;
	result.push_back(x_point_new);
	result.push_back(y_point_new);
	return result;	
}

float visible_ptz::get_zdegree(float x_newpoint, float y_newpoint, float z_camera, vector<float> device_position, float angley)
{
	float x_distance = device_position[0] - x_newpoint;
	float y_distance = device_position[1] - y_newpoint;
	float z_distance = device_position[2] - z_camera;
	float xy_distance = sqrt(pow(x_distance, 2) + pow(y_distance, 2));
	float zxy_distance = sqrt(pow(z_distance, 2) + pow(xy_distance, 2));

	if(xy_distance == 0.0)
	{
		xy_distance = 0.00001;
	}
	float zdegree = atan(z_distance / xy_distance);
	float zdegree_o = zdegree * 180 / PI;
	std::cout << "get_zdegree result is " << zdegree_o << std::endl;

	float l = 0.05;
	if(zxy_distance == 0.0)
	{
		zxy_distance = 0.00001;
	}
	float theao_device_center = acos(l / zxy_distance);
	float theao_devicecenter_degree = theao_device_center * 180 / PI;
	std::cout << "the camera and device end radian is " << theao_device_center << ", degree is " << theao_devicecenter_degree << std::endl;

	float zdegree_final = 0.0;
	if(z_distance >= 0)
		zdegree_final = zdegree_o + theao_devicecenter_degree - 90;
	else if(z_distance <= 0)
		zdegree_final = zdegree_o + theao_devicecenter_degree - 90;
	std::cout << "the zdegree_final degree is " << zdegree_final << std::endl;

	return zdegree_final + _handlez + _angley * 180 / PI;
}

void visible_ptz::set_position_first()
{
	if(_use_hk_cloudplatform){
		yd_cloudplatform::CloudPlatControl cloudplat_control;
		cloudplat_control.request.id = _camera_id;
		cloudplat_control.request.action = 1;
		cloudplat_control.request.type = 4;
		cloudplat_control.request.value = 0;
		vector<unsigned int> value;
		value.push_back(0);
		value.push_back(0);
		value.push_back(0);
		cloudplat_control.request.allvalue = value;
		if(cloudplatform_client.call(cloudplat_control))
			std::cout << "set xyz_zoom degree success!" << std::endl;
		else
			std::cout << "set xyz_zoom degree failed!" << std::endl;
		sleep(0.5);
	}
	else{
		yd_cloudplatform::CloudPlatControl cloudplat_control;
		cloudplat_control.request.id = _camera_id;
		cloudplat_control.request.action = 1;
		cloudplat_control.request.type = 0;
		cloudplat_control.request.value = 0;
		if(cloudplatform_client.call(cloudplat_control))
			std::cout << "set xy degree success!" << std::endl;
		else
			std::cout << "set xy degree failed!" << std::endl;
		sleep(0.5);
		
		cloudplat_control.request.id = _camera_id;
		cloudplat_control.request.action = 1;
		cloudplat_control.request.type = 1;
		cloudplat_control.request.value = 0;
		if(cloudplatform_client.call(cloudplat_control))
			std::cout << "set z degree success!" << std::endl;
		else
			std::cout << "set z degree failed!" << std::endl;
		sleep(0.5);

		cloudplat_control.request.id = _camera_id;
		cloudplat_control.request.action = 1;
		cloudplat_control.request.type = 2;
		cloudplat_control.request.value = 0;
		if(cloudplatform_client.call(cloudplat_control))
			std::cout << "set zoom degree success!" << std::endl;
		else
			std::cout << "set zoom degree failed!" << std::endl;
		sleep(0.5);
	}
}

void visible_ptz::points_callback(const nav_msgs::Odometry& msg)
{
	 //创建StampedTransform对象存储变换结果数据
    tf::StampedTransform transform_camera;
    tf::StampedTransform transform_cloudplatform;
    
    //监听包装在一个try-catch块中以捕获可能的异常
    try{
      listener.lookupTransform("/map", "/visible_link", ros::Time(0), transform_camera);
      listener.lookupTransform("/map", "/cloudplatform_link", ros::Time(0), transform_cloudplatform);
    }
    catch (tf::TransformException &ex) {
      ROS_ERROR("%s",ex.what());
    }

    _x_camera = transform_camera.getOrigin().x();
	_y_camera = transform_camera.getOrigin().y();
	_z_camera = transform_camera.getOrigin().z();

	_x_cloudplatform = transform_cloudplatform.getOrigin().x();
	_y_cloudplatform = transform_cloudplatform.getOrigin().y();
	_z_cloudplatform = transform_cloudplatform.getOrigin().z();

	_angley = msg.twist.twist.angular.y;
	_anglez = msg.twist.twist.angular.z;
}

void visible_ptz::transfer_callback(const yidamsg::transfer& msg)
{
	std::thread transfer_Thread = std::thread(std::bind(&visible_ptz::transfer_handle_fun, this, msg, nullptr));
	transfer_Thread.detach();

    // yidamsg::InspectedResult imagezoom_msg;
	// yd_cloudplatform::CloudPlatControl cloudplat_control_client;
	// if(transferpub_msg.size() > 0)
	// {
	// 	transferpub_msg.clear();
	// }
 
	// if(msg.flag == 0)
	// {
	// 	std::string str_devicepoint = msg.data;   
    // 	SplitString(str_devicepoint, transferpub_msg, "/");
    
	// 	//device_id = transferpub_msg[2];
	// 	if(transferpub_msg[3] == "1")
	// 	{
	// 		vector<std::string> device_point;
	// 		SplitString(transferpub_msg[4], device_point, ",");
	// 		float x_device = atof(device_point[0].c_str());
	// 		float y_device = atof(device_point[1].c_str());
	// 		float z_device = atof(device_point[2].c_str());
	// 		int device_width = atoi(device_point[3].c_str());
	// 		int device_height = atoi(device_point[4].c_str());
	// 		_device_type = atoi(device_point[5].c_str());
	// 		std::cout << "=====x_device, y_device, z_device value is: " << x_device << " " << y_device << " " << z_device << " .=====" << std::endl;
	// 		std::cout << "=====x_camera, y_camera, z_camera value is: " << _x_camera << " " << _y_camera << " " << _z_camera << " .=====" << std::endl;
	// 		std::cout << "=====x_cloudplatform, y_cloudplatform, z_cloudplatform value is: " << _x_cloudplatform << " " << _y_cloudplatform << " " << _z_cloudplatform << " .=====" << std::endl;

	// 		float dif_height = (z_device - _z_camera) * 1000;
	// 		dif_height = dif_height < 0 ? 0 : dif_height;
	// 		dif_height = dif_height > 1000 ? 1000 : dif_height;
	// 		yidamsg::ArmControl arm_msg;
	// 		arm_msg.request.type = 1;
	// 		arm_msg.request.action = 1;
	// 		arm_msg.request.values.push_back(dif_height);
	// 		if(robot_arm_client.call(arm_msg))
	// 		{
	// 			ros::Rate rate(2);
	// 			while(ros::ok())
	// 			{
	// 				ros::spinOnce();
	// 				rate.sleep();
	// 			}
	// 			std::cout << "set arm arm height degree success!" << std::endl;
	// 		}
	// 		else
	// 			std::cout << "set arm height degree failed!" << std::endl;

	// 		vector<float> camera_position;
	// 		camera_position.push_back(_x_camera);
	// 		camera_position.push_back(_y_camera);
	// 		camera_position.push_back(_z_camera);

	// 		vector<float> cloudplatform_position;
	// 		cloudplatform_position.push_back(_x_cloudplatform);
	// 		cloudplatform_position.push_back(_y_cloudplatform);
	// 		cloudplatform_position.push_back(_z_cloudplatform);

	// 		vector<float> device_position;
	// 		device_position.push_back(x_device);
	// 		device_position.push_back(y_device);
	// 		device_position.push_back(z_device);

	// 		int zoom_scale = 5;
	// 		int zoom_set = get_zoomset(camera_position, device_position, device_width, device_height, zoom_scale);
	// 		float xydegree = get_xydegree(camera_position, device_position, cloudplatform_position);
	// 		vector<float> new_position = get_newpoint(camera_position, device_position, cloudplatform_position, xydegree);
	// 		float zdegree = get_zdegree(new_position[0], new_position[1], _z_camera, device_position, _angley);
		
	// 		float finalx_set = xydegree;
	// 		int val_panpos = 0;
	// 		std::cout << "finalx_set 11111 is " << finalx_set << std::endl;
	// 		if(finalx_set > 0)
	// 		{
	// 			finalx_set = val_panpos + abs(finalx_set * 100);
	// 			std::cout << "finalx_set 22222 is " << finalx_set << std::endl;
	// 			if(finalx_set > 35999)
	// 				finalx_set = finalx_set - 35999;
	// 		}
	// 		else if(finalx_set < 0)
	// 		{
	// 			finalx_set = val_panpos - abs(finalx_set * 100);
	// 			std::cout << "finalx_set 33333 is " << finalx_set << std::endl;
	// 			if(finalx_set < 0)
	// 				finalx_set = 35999 + finalx_set;
	// 		}

	// 		float finalz_set = 0.0;
	// 		int val_tiltpos = 0;
	// 		if(_z_max_degree > _z_min_degree)
	// 		{
	// 			finalz_set = val_tiltpos - zdegree * 100;
	// 			if(z_device >= _z_camera)
	// 			{
	// 				if((finalz_set >= _z_min_degree) && (finalz_set < _z_max_degree))
	// 					finalz_set = _z_max_degree;
	// 				else if(finalz_set < 0)
	// 					finalz_set = _z_origin_degree + finalz_set;
	// 			}
	// 			if(z_device < _z_camera)
	// 				if((finalz_set > _z_min_degree) && (finalz_set <= _z_max_degree))
	// 					finalz_set = _z_min_degree;
	// 				else if(finalz_set > _z_origin_degree)
	// 					finalz_set = finalz_set - _z_origin_degree;
	// 		}
	// 		else if(_z_max_degree < _z_min_degree)
	// 		{
	// 			finalz_set = val_tiltpos + zdegree * 100;
	// 			if(z_device >= _z_camera)
	// 			{
	// 				if(finalz_set > _z_origin_degree)
	// 					finalz_set = finalz_set - _z_origin_degree;
	// 			}
	// 			if(z_device < _z_camera)
	// 			{
	// 				if(finalz_set > _z_origin_degree)
	// 					finalz_set = finalz_set - _z_origin_degree;
	// 			}
	// 		}

	// 		_first_finalx = int(finalx_set);
	// 		_first_finalz = int(finalz_set);

	// 		std::cout << "finalx_set: " << int(finalx_set) << ", finalz_set: " << int(finalz_set) << ", zoom_set: " << int(zoom_set) << std::endl;

	// 		if(_use_hk_cloudplatform){
	// 			yd_cloudplatform::CloudPlatControl cloudplat_control;
	// 			cloudplat_control.request.id = _camera_id;
	// 			cloudplat_control.request.action = 1;
	// 			cloudplat_control.request.type = 4;
	// 			cloudplat_control.request.value = 0;
	// 			vector<unsigned int> value;
	// 			value.push_back(int(finalx_set));
	// 			value.push_back(int(finalz_set));
	// 			//value.push_back(_zoom_focus_mm * 100);
	// 			value.push_back(int(zoom_set));
	// 			cloudplat_control.request.allvalue = value;
	// 			if(cloudplatform_client.call(cloudplat_control))
	// 				std::cout << "set xyz_zoom degree success!" << std::endl;
	// 			else
	// 				std::cout << "set xyz_zoom degree failed!" << std::endl;
	// 			sleep(0.5);
	// 		}
	// 		else{
	// 			yd_cloudplatform::CloudPlatControl cloudplat_control;
	// 			cloudplat_control.request.id = _camera_id;
	// 			cloudplat_control.request.action = 1;
	// 			cloudplat_control.request.type = 0;
	// 			cloudplat_control.request.value = int(finalx_set);
	// 			if(cloudplatform_client.call(cloudplat_control))
	// 				std::cout << "set xy degree success!" << std::endl;
	// 			else
	// 				std::cout << "set xy degree failed!" << std::endl;
	// 			sleep(0.5);
				
	// 			cloudplat_control.request.id = _camera_id;
	// 			cloudplat_control.request.action = 1;
	// 			cloudplat_control.request.type = 1;
	// 			cloudplat_control.request.value = int(finalz_set);
	// 			if(cloudplatform_client.call(cloudplat_control))
	// 				std::cout << "set z degree success!" << std::endl;
	// 			else
	// 				std::cout << "set z degree failed!" << std::endl;
	// 			sleep(0.5);

	// 			cloudplat_control.request.id = _camera_id;
	// 			cloudplat_control.request.action = 1;
	// 			cloudplat_control.request.type = 2;
	// 			cloudplat_control.request.value = zoom_set;
	// 			if(cloudplatform_client.call(cloudplat_control))
	// 				std::cout << "set zoom degree success!" << std::endl;
	// 			else
	// 				std::cout << "set zoom degree failed!" << std::endl;
	// 			sleep(0.5);
	// 		}

	// 		visible_survey_parm_id = 1;
	// 		_cloudplatform_status_ready = true;
	// 	}
	// }
	// else if(msg.flag == 1)
	// {
	// 	set_position_first();
	// }
}

void visible_ptz::transfer_handle_fun(const yidamsg::transfer& msg, void *p)
{
	yidamsg::InspectedResult imagezoom_msg;
	yd_cloudplatform::CloudPlatControl cloudplat_control_client;
	if(transferpub_msg.size() > 0)
	{
		transferpub_msg.clear();
	}
 
	if(msg.flag == 0)
	{
		std::string str_devicepoint = msg.data;   
    	SplitString(str_devicepoint, transferpub_msg, "/");
    
		//device_id = transferpub_msg[2];
		// if(transferpub_msg[3] == "1")
		if(transferpub_msg[3] == "1" || transferpub_msg[3] == "9")
		{
			vector<std::string> device_point;
			SplitString(transferpub_msg[4], device_point, ",");
			float x_device = atof(device_point[0].c_str());
			float y_device = atof(device_point[1].c_str());
			float z_device = atof(device_point[2].c_str());
			int device_width = atoi(device_point[3].c_str());
			int device_height = atoi(device_point[4].c_str());
			_device_type = atoi(device_point[5].c_str());
			std::cout << "=====x_device, y_device, z_device value is: " << x_device << " " << y_device << " " << z_device << " .=====" << std::endl;
			std::cout << "=====x_camera, y_camera, z_camera value is: " << _x_camera << " " << _y_camera << " " << _z_camera << " .=====" << std::endl;
			std::cout << "=====x_cloudplatform, y_cloudplatform, z_cloudplatform value is: " << _x_cloudplatform << " " << _y_cloudplatform << " " << _z_cloudplatform << " .=====" << std::endl;

			// // visible_origin_pose
			// try
			// {
			// 	geometry_msgs::TransformStamped visible_camera_to_map_transform;
			// 	visible_camera_to_map_transform = buffer_.lookupTransform("map", "visible_origin_pose", ros::Time(0), ::ros::Duration(1));
			// 	std::cout << "lookupTransform from visible_origin_pose to map ok." << std::endl;
			// 	Eigen::Affine3d visible_camera_to_map =  tf2::transformToEigen (visible_camera_to_map_transform);
			// 	// Eigen::Matrix4d visible_rep_to_cv;
			// 	// visible_rep_to_cv << 0, 0, 1, 0,
			// 	//                      0,-1, 0, 0,
			// 	//                      1, 0, 0, 0,
			// 	//                      0, 0, 0, 1;
			// 	// Eigen::Affine3d visible_cam_rep_to_world(visible_camera_to_map.matrix() * visible_rep_to_cv);
			// 	geometry_msgs::Pose visible_pose = Eigen::toMsg(visible_camera_to_map);

			// 	double visible_roll, visible_pitch, visible_yaw;
			// 	tf2::Quaternion visible_quaternion;
			// 	tf2::convert(visible_pose.orientation, visible_quaternion);
			// 	tf2::Matrix3x3(visible_quaternion).getRPY(visible_roll, visible_pitch, visible_yaw); //进行转换
			// 	std::cout << "visible_pose is: " << visible_pose.position.x << " " << visible_pose.position.y << " " << visible_pose.position.z << std::endl;
			// }
			// catch(const std::exception& e)
			// {
			// 	std::cerr << e.what() << '\n';
			// }
			// // visible_origin_pose --

			float current_x_camera = _x_camera;
			float current_y_camera = _y_camera;
			float current_z_camera = _z_camera;
			float current_x_cloudplatform = _x_cloudplatform;
			float current_y_cloudplatform = _y_cloudplatform;
			float current_z_cloudplatform = _z_cloudplatform;
			if (_use_arm == 1)
			{
				float dif_height = (z_device - current_z_camera) * 1000;
				dif_height = dif_height < 0 ? 0 : dif_height;
				dif_height = dif_height > 1000 ? 1000 : dif_height;
				yidamsg::ArmControl arm_msg;
				arm_msg.request.type = 1;
				arm_msg.request.action = 1;
				arm_msg.request.values.push_back(dif_height);
				if(robot_arm_client.call(arm_msg))
				{
					float camera_height = current_z_camera;
					float cloudplatform_height = current_z_cloudplatform;
					int arm_time = 0;
					ros::Rate rate(1);
					while(ros::ok())
					{
						boost::shared_ptr<yidamsg::ArmDataStamped const> arm_data_sub;
						arm_data_sub = ros::topic::waitForMessage<yidamsg::ArmDataStamped>("/yida/robot/arm/data", ros::Duration(3.0));
						if (arm_data_sub != nullptr)
						{
							current_z_camera = camera_height + (arm_data_sub->arm.max_arm_height * 0.001);
							current_z_cloudplatform = cloudplatform_height + (arm_data_sub->arm.max_arm_height * 0.001);
							if (abs(arm_data_sub->arm.max_arm_height - dif_height) < 10)
							{
								current_z_camera = camera_height + (dif_height * 0.001);
								current_z_cloudplatform = cloudplatform_height + (dif_height * 0.001);
								ROS_INFO("transfer_handle_fun arm height done.");
								break;
							}
						}
						arm_time++;
						if (arm_time >= (30 * 1))
						{
							ROS_WARN("transfer_handle_fun arm height timeout.");
							break;
						}
								
						ros::spinOnce();
						rate.sleep();
					}
					std::cout << "set arm height degree success!" << std::endl;
				}
				else
					std::cout << "set arm height degree failed!" << std::endl;
			}
			vector<float> camera_position;
			camera_position.push_back(current_x_camera);
			camera_position.push_back(current_y_camera);
			camera_position.push_back(current_z_camera);

			vector<float> cloudplatform_position;
			cloudplatform_position.push_back(current_x_cloudplatform);
			cloudplatform_position.push_back(current_y_cloudplatform);
			cloudplatform_position.push_back(current_z_cloudplatform);

			vector<float> device_position;
			device_position.push_back(x_device);
			device_position.push_back(y_device);
			device_position.push_back(z_device);

			int zoom_scale = 5;
			int zoom_set = get_zoomset(camera_position, device_position, device_width, device_height, zoom_scale);
			float xydegree = get_xydegree(camera_position, device_position, cloudplatform_position);
			vector<float> new_position = get_newpoint(camera_position, device_position, cloudplatform_position, xydegree);
			float zdegree = get_zdegree(new_position[0], new_position[1], current_z_camera, device_position, _angley);
		
			float finalx_set = xydegree;
			int val_panpos = 0;
			std::cout << "finalx_set 11111 is " << finalx_set << std::endl;
			if(finalx_set > 0)
			{
				finalx_set = val_panpos + abs(finalx_set * 100);
				std::cout << "finalx_set 22222 is " << finalx_set << std::endl;
				if(finalx_set > 35999)
					finalx_set = finalx_set - 35999;
			}
			else if(finalx_set < 0)
			{
				finalx_set = val_panpos - abs(finalx_set * 100);
				std::cout << "finalx_set 33333 is " << finalx_set << std::endl;
				if(finalx_set < 0)
					finalx_set = 35999 + finalx_set;
			}

			float finalz_set = 0.0;
			int val_tiltpos = 0;
			if(_z_max_degree > _z_min_degree)
			{
				finalz_set = val_tiltpos - zdegree * 100;
				if(z_device >= current_z_camera)
				{
					if((finalz_set >= _z_min_degree) && (finalz_set < _z_max_degree))
						finalz_set = _z_max_degree;
					else if(finalz_set < 0)
						finalz_set = _z_origin_degree + finalz_set;
				}
				if(z_device < current_z_camera)
					if((finalz_set > _z_min_degree) && (finalz_set <= _z_max_degree))
						finalz_set = _z_min_degree;
					else if(finalz_set > _z_origin_degree)
						finalz_set = finalz_set - _z_origin_degree;
			}
			else if(_z_max_degree < _z_min_degree)
			{
				finalz_set = val_tiltpos + zdegree * 100;
				if(z_device >= current_z_camera)
				{
					if(finalz_set > _z_origin_degree)
						finalz_set = finalz_set - _z_origin_degree;
				}
				if(z_device < current_z_camera)
				{
					if(finalz_set > _z_origin_degree)
						finalz_set = finalz_set - _z_origin_degree;
				}
			}

			_first_finalx = int(finalx_set);
			_first_finalz = int(finalz_set);

			std::cout << "finalx_set: " << int(finalx_set) << ", finalz_set: " << int(finalz_set) << ", zoom_set: " << int(zoom_set) << std::endl;

			if(_use_hk_cloudplatform){
				yd_cloudplatform::CloudPlatControl cloudplat_control;
				cloudplat_control.request.id = _camera_id;
				cloudplat_control.request.action = 1;
				cloudplat_control.request.type = 4;
				cloudplat_control.request.value = 0;
				vector<unsigned int> value;
				value.push_back(int(finalx_set));
				value.push_back(int(finalz_set));
				//value.push_back(_zoom_focus_mm * 100);
				value.push_back(int(zoom_set));
				cloudplat_control.request.allvalue = value;
				if(cloudplatform_client.call(cloudplat_control))
					std::cout << "set xyz_zoom degree success!" << std::endl;
				else
					std::cout << "set xyz_zoom degree failed!" << std::endl;
				sleep(0.5);
			}
			else{
				yd_cloudplatform::CloudPlatControl cloudplat_control;
				cloudplat_control.request.id = _camera_id;
				cloudplat_control.request.action = 1;
				cloudplat_control.request.type = 0;
				cloudplat_control.request.value = int(finalx_set);
				if(cloudplatform_client.call(cloudplat_control))
					std::cout << "set xy degree success!" << std::endl;
				else
					std::cout << "set xy degree failed!" << std::endl;
				sleep(0.5);
				
				cloudplat_control.request.id = _camera_id;
				cloudplat_control.request.action = 1;
				cloudplat_control.request.type = 1;
				cloudplat_control.request.value = int(finalz_set);
				if(cloudplatform_client.call(cloudplat_control))
					std::cout << "set z degree success!" << std::endl;
				else
					std::cout << "set z degree failed!" << std::endl;
				sleep(0.5);

				cloudplat_control.request.id = _camera_id;
				cloudplat_control.request.action = 1;
				cloudplat_control.request.type = 2;
				cloudplat_control.request.value = zoom_set;
				if(cloudplatform_client.call(cloudplat_control))
					std::cout << "set zoom degree success!" << std::endl;
				else
					std::cout << "set zoom degree failed!" << std::endl;
				sleep(0.5);
			}

			visible_survey_parm_id = 1;
			_cloudplatform_status_ready = true;
		}
	}
	else if(msg.flag == 1)
	{
		set_position_first();
	}
}

void visible_ptz::detect_rect_callback(const yidamsg::Detect_Result& msg)
{
	std::cout << "=====*********get in detectresult*********.=====" << std::endl;
	vector<std::string> device_point;
    SplitString(transferpub_msg[4], device_point, ",");
	float x_device = atof(device_point[0].c_str());
	float y_device = atof(device_point[1].c_str());
	float z_device = atof(device_point[2].c_str());
	int device_width = atoi(device_point[3].c_str());
	int device_height = atoi(device_point[4].c_str());
	_device_type = atoi(device_point[5].c_str());

	vector<float> camera_position;
	camera_position.push_back(_x_camera);
	camera_position.push_back(_y_camera);
	camera_position.push_back(_z_camera);

	vector<float> device_position;
	device_position.push_back(x_device);
	device_position.push_back(y_device);
	device_position.push_back(z_device);
	
	//图像大小
	int captureimage_width = _camera_image_width;
	int captureimage_height = _camera_image_height;
	//计算检测框的中心坐标
	int framecentral_x = msg.xmin + ((msg.xmax - msg.xmin) / 2);
	int framecentral_y = msg.ymin + ((msg.ymax - msg.ymin) / 2);
	//（与中心坐标的）偏移量
	int offset_x = framecentral_x - (captureimage_width / 2);
	int offset_y = framecentral_y - (captureimage_height / 2);
	//计算焦距对应像素点个数
	float focus_set = _zoom_focus_mm / _camera_pixel_size * 1000;
	if(focus_set == 0)
		focus_set = 0.001;
	//计算水平、垂直偏移量（换算成云台对应的度制）
	float horizontal_x = ((atan(offset_x / focus_set) / PI) * 180);
	float vertical_y = ((atan(offset_y / focus_set) / PI) * 180);
	int finalx_set = horizontal_x * 100;
	int finalz_set = vertical_y * 100;

	finalx_set = _first_finalx + finalx_set;
	if(finalx_set > 35999)
		finalx_set = finalx_set - 35999;
	else if(finalx_set < 0)
		finalx_set = 35999 + finalx_set;

	finalz_set = _first_finalz + finalz_set;
	if(_z_max_degree > _z_min_degree)
	{
		if(z_device >= _z_camera)
		{
			if((finalz_set >= _z_min_degree) && (finalz_set < _z_max_degree))
				finalz_set = _z_max_degree;
			else if(finalz_set < 0)
				finalz_set = _z_origin_degree + finalz_set;
		}
		if(z_device < _z_camera)
			if((finalz_set > _z_min_degree) && (finalz_set <= _z_max_degree))
				finalz_set = _z_min_degree;
			else if(finalz_set > _z_origin_degree)
				finalz_set = finalz_set - _z_origin_degree;
	}
	else if(_z_max_degree < _z_min_degree)
	{
		if(z_device >= _z_camera)
		{
			if(finalz_set > _z_origin_degree)
				finalz_set = finalz_set - _z_origin_degree;
		}
		if(z_device < _z_camera)
		{
			if(finalz_set > _z_origin_degree)
				finalz_set = finalz_set - _z_origin_degree;
		}
	}

	int zoom_scale = 1.6;
	int zoom_set = get_zoomset(camera_position, device_position, device_width, device_height, zoom_scale);

	if(_use_hk_cloudplatform != 1)
	{
		yd_cloudplatform::CloudPlatControl cloudplat_control;
		cloudplat_control.request.id = _camera_id;
		cloudplat_control.request.action = 1;
		cloudplat_control.request.type = 0;
		cloudplat_control.request.value = int(finalx_set);
		if(cloudplatform_client.call(cloudplat_control))
			std::cout << "set xy degree success!" << std::endl;
		else
			std::cout << "set xy degree failed!" << std::endl;
		sleep(0.5);
		
		cloudplat_control.request.id = _camera_id;
		cloudplat_control.request.action = 1;
		cloudplat_control.request.type = 1;
		cloudplat_control.request.value = int(finalz_set);
		if(cloudplatform_client.call(cloudplat_control))
			std::cout << "set z degree success!" << std::endl;
		else
			std::cout << "set z degree failed!" << std::endl;
		sleep(0.5);

		cloudplat_control.request.id = _camera_id;
		cloudplat_control.request.action = 1;
		cloudplat_control.request.type = 2;
		cloudplat_control.request.value = zoom_set;
		if(cloudplatform_client.call(cloudplat_control))
			std::cout << "set zoom degree success!" << std::endl;
		else
			std::cout << "set zoom degree failed!" << std::endl;
		sleep(0.5);

		visible_survey_parm_id = 2;
		_cloudplatform_status_ready = true;
	}
	else
	{
		sleep(3);
		yidamsg::InspectedResult imagezoom_msg;
		imagezoom_msg.camid = 2;
		imagezoom_msg.picid = _device_type;
		imagezoom_msg.equipimage = _image_char;
		std::stringstream ss;
		ss.clear();
		ss.str("");
		ss << transferpub_msg[2] << "," << transferpub_msg[4] << "/" << transferpub_msg[5] << "/" << transferpub_msg[6];
		imagezoom_msg.equipid = ss.str();
		// readyimage_pub_.publish(imagezoom_msg);
		if(transferpub_msg[3] == "9")
		{
			readyimage_pub_.publish(imagezoom_msg);
			// readyimage_yiwu_pub_.publish(imagezoom_msg);
		}
		else
		{
			readyimage_pub_.publish(imagezoom_msg);
		}
		time_t currtime = time(NULL);
		tm* p = localtime(&currtime);
		char filename[100] = {0};
    	sprintf(filename,"visible_%d%02d%02d%02d%02d%02d.jpg",p->tm_year+1900,p->tm_mon+1,p->tm_mday,p->tm_hour,p->tm_min,p->tm_sec);
		//_image = cv::imdecode(cv::Mat(_image_char),1);
		//cv::imwrite(filename, _image);
	}
}

void visible_ptz::image_callback(const sensor_msgs::CompressedImagePtr& msg)
{
	if(_image_char.size() > 0)
	{
		_image_char.clear();
	}
	_image_char = msg->data;
	//_image = cv::imdecode(cv::Mat(msg->data),1); //convert compressed image data to cv::Mat
}

void visible_ptz::isreach_callback(const std_msgs::Int32& msg)
{
	if((msg.data == 1) && (_cloudplatform_status_ready == true))
	{
		sleep(5);

		_cloudplatform_status_ready = false;

		yidamsg::InspectedResult imagezoom_msg;
		// imagezoom_msg.camid = 1;
		imagezoom_msg.camid = visible_survey_parm_id;
		imagezoom_msg.picid = _device_type;
		//imagezoom_msg.equipimage = _image_char;
		std::stringstream ss;
		ss.clear();
		ss.str("");
		ss << transferpub_msg[2] << "," << transferpub_msg[4] << "/" << transferpub_msg[5] << "/" << transferpub_msg[6];
		imagezoom_msg.equipid = ss.str();
		// readyimage_pub_.publish(imagezoom_msg);
		if(transferpub_msg[3] == "9")
		{
			readyimage_pub_.publish(imagezoom_msg);
			// readyimage_yiwu_pub_.publish(imagezoom_msg);
		}
		else
		{
			readyimage_pub_.publish(imagezoom_msg);
		}
		time_t currtime = time(NULL);
		tm* p = localtime(&currtime);
		char filename[100] = {0};
    	sprintf(filename,"visible_%d%02d%02d%02d%02d%02d.jpg",p->tm_year+1900,p->tm_mon+1,p->tm_mday,p->tm_hour,p->tm_min,p->tm_sec);
		//_image = cv::imdecode(cv::Mat(_image_char),1);
		//cv::imwrite(filename, _image);
	}
}

void visible_ptz::ptz_pose_callback(const nav_msgs::Odometry& msg)
{
	// // rgb
	// Eigen::Affine3d rgb_camera_to_lidar = cameras_ptr_->GetRelativePose({0.0, 0.0}); 
	// // rgb_camera_to_lidar.linear().normalize();
	// Eigen::Quaterniond rgb_q(rgb_camera_to_lidar.linear());  // assuming that upper 3x3 matrix is orthonormal
	// rgb_q.normalize();
	// geometry_msgs::TransformStamped rgb_transform_msg = tf2::eigenToTransform(rgb_camera_to_lidar);
	// rgb_transform_msg.header.stamp = msg.header.stamp;
	// rgb_transform_msg.header.frame_id = "lidar_pose";
	// rgb_transform_msg.child_frame_id = "visible_origin_pose";
	// rgb_transform_msg.transform.rotation.x = rgb_q.x();
	// rgb_transform_msg.transform.rotation.y = rgb_q.y();
	// rgb_transform_msg.transform.rotation.z = rgb_q.z();
	// rgb_transform_msg.transform.rotation.w = rgb_q.w();
	// // broadcaster_.sendTransform(rgb_transform_msg);
	// buffer_.setTransform(rgb_transform_msg,"");
}

void visible_ptz::robot_arm_data_callback(const yidamsg::ArmDataStamped& msg)
{
	if (_robot_arm_status_ready == true)
	{
		_robot_arm_status_ready = false;
	}
}

visible_ptz::visible_ptz(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle)
    :node_handle_(node_handle), private_node_handle_(private_node_handle),buffer_(),listener_(buffer_)
{
    std::string camera_focus_mm_str, camera_focus_value_str, xyz_degree_str;
	private_node_handle_.param<int>("use_hk_cloudplatform", _use_hk_cloudplatform, 1);
    private_node_handle_.param<double>("handleset_xy", _handlexy, 0.0);
    private_node_handle_.param<double>("handleset_z", _handlez, 2.0);
    private_node_handle_.param<int>("camera_id", _camera_id, 1);
    private_node_handle_.param<std::string>("camera_ip", _camera_ip, "192.168.1.62");
    private_node_handle_.param<std::string>("camera_username", _camera_username, "admin");
    private_node_handle_.param<std::string>("camera_password", _camera_password, "123qweasd");
    private_node_handle_.param<int>("camera_image_width", _camera_image_width, 1920);
    private_node_handle_.param<int>("camera_image_height", _camera_image_height, 1080);
    private_node_handle_.param<double>("camera_cmos_width", _camera_cmos_width, 5.28);
    private_node_handle_.param<double>("camera_cmos_height", _camera_cmos_height, 2.97);
    private_node_handle_.param<double>("camera_pixel_size", _camera_pixel_size, 3.75);
    private_node_handle_.param<int>("camera_focus_number", _camera_focus_number, 30);
    private_node_handle_.param<std::string>("camera_focus_mm", camera_focus_mm_str, "4.3, 8.6");
    private_node_handle_.param<std::string>("camera_focus_value", camera_focus_value_str, "0x0, 0x10b9");
    private_node_handle_.param<std::string>("xyz_degree", xyz_degree_str, "0,35999,35999,27000,9000");
    private_node_handle_.param<int>("offset_xy", _offset_xy, 0);
    private_node_handle_.param<std::string>("heartbeat_topic_string", heartbeat_topic_str, "/yida/heartbeat");
	private_node_handle_.param<int>("use_arm", _use_arm, 0);
	std::string calibration_filepath;
	private_node_handle_.param<std::string>("calib_filepath", calibration_filepath, "");

	// cameras_ptr_ = make_shared<Camera>();
	// cameras_ptr_->LoadCalibrationFile(calibration_filepath, "rgb");

    vector<std::string> degree_value_strv, camera_focus_mm_strv, camera_focus_value_strv;
    SplitString(camera_focus_mm_str, camera_focus_mm_strv, ",");
    for(int i = 0; i < camera_focus_mm_strv.size(); i++)
        _camera_focus_mm.push_back(atof(camera_focus_mm_strv[i].c_str()));
    SplitString(camera_focus_value_str, camera_focus_value_strv, ",");
    for(int i = 0; i < camera_focus_value_strv.size(); i++)
        _camera_focus_value.push_back(atoi(camera_focus_value_strv[i].c_str()));
    
    SplitString(xyz_degree_str, degree_value_strv, ",");
    _x_min_degree = atoi(degree_value_strv[0].c_str());
    _x_max_degree = atoi(degree_value_strv[1].c_str());
    _z_origin_degree = atoi(degree_value_strv[2].c_str());
    _z_max_degree = atoi(degree_value_strv[3].c_str());
    _z_min_degree = atoi(degree_value_strv[4].c_str());

	_x_camera = 0.0;
	_y_camera = 0.0; 
	_z_camera = 0.0;

    level = 0;
    message = "cloutplatform is ok!";

	visible_survey_parm_id = 1;
	_cloudplatform_status_ready = false;
	_robot_arm_status_ready = false;

	for(int i = 0; i < 10; i++)
	{
		transferpub_msg.push_back("init value");
	}

    heartbeat_pub_ = node_handle_.advertise<diagnostic_msgs::DiagnosticArray>(heartbeat_topic_str, 1);
    readyimage_pub_ = node_handle_.advertise<yidamsg::InspectedResult>("/visible_survey_parm", 1);
	readyimage_yiwu_pub_ = node_handle_.advertise<yidamsg::InspectedResult>("/visible_yiwu_survey_parm", 1);

    robot_pose_sub_ = node_handle_.subscribe("/robot_pose", 1, &visible_ptz::points_callback, this);
    transfer_sub_ = node_handle_.subscribe("/transfer_pub", 1, &visible_ptz::transfer_callback, this);
    detectresult_sub_ = node_handle_.subscribe("/detect_rect", 1, &visible_ptz::detect_rect_callback, this);
    cloudplatform_isreach_sub_ = node_handle_.subscribe("/yida/platform_isreach", 1, &visible_ptz::isreach_callback, this);
	ptz_sub_ = node_handle_.subscribe("/yida/yuntai/position", 1, &visible_ptz::ptz_pose_callback, this);
	robot_arm_sub_ = node_handle_.subscribe("/yida/robot/arm/data", 1, &visible_ptz::robot_arm_data_callback, this);

	cloudplatform_client = node_handle_.serviceClient<yd_cloudplatform::CloudPlatControl>("/yida/internal/platform_cmd");
	robot_arm_client = node_handle_.serviceClient<yidamsg::ArmControl>("/yida/robot/arm");
	
	pthread_t ros_thread = 0; 
	pthread_create(&ros_thread,NULL,sub_run,(void*)this);
}
    
visible_ptz::~visible_ptz()
{

}

void visible_ptz::update()
{
    pub_heartbeat(level, message);
}

void visible_ptz::pub_heartbeat(int level, string message)
{
    diagnostic_msgs::DiagnosticArray log;
    log.header.stamp = ros::Time::now();

    diagnostic_msgs::DiagnosticStatus s;
    s.name = __app_name__;             // 这里写节点的名字
    s.level = level;                   // 0 = OK, 1 = Warn, 2 = Error
    if (!message.empty())
    {
        s.message = message;           // 问题描述
    }
    s.hardware_id = "cloutplatform";   // 硬件信息
    log.status.push_back(s);
    sleep(0.1);
    heartbeat_pub_.publish(log);
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, __app_name__);
    ros::NodeHandle node_handle;
    ros::NodeHandle private_node_handle("~");
    visible_ptz visible_ptz_obj(node_handle, private_node_handle);

    ros::Rate rate(20);
    while(ros::ok())
    {
        visible_ptz_obj.update();
        ros::spinOnce();
        rate.sleep();
    }

    return 0;
}
