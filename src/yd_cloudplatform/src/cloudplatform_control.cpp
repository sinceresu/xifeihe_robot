#include <ros/ros.h>
#include <pthread.h>
#include <string.h>
#include <string>

#include "yd_cloudplatform/serial_comm.hpp"
#include "yd_cloudplatform/cloudplatform_control.hpp"
#include "yd_cloudplatform/common.hpp"

unsigned char g_cmd_cloudplat_getxy[LENGTH] = {0};
unsigned char g_cmd_cloudplat_getz[LENGTH] = {0};
unsigned char g_cmd_cloudplat[LENGTH];
unsigned int g_get_info_flag = 1;
unsigned int g_now_xyposition = 0;
unsigned int g_now_zposition = 0;
unsigned int g_action = 0;
unsigned int g_control_type = 0;
unsigned int g_xy_goal = 0;
unsigned int g_z_goal = 0;
unsigned int g_reach_flag = 0;
unsigned int g_xy_reach_flag = 0;
unsigned int g_z_reach_flag = 0;

void *get_degree(void* args)
{
    int z_diff_val = 0, xy_diff_val = 0;
    cloudplatform_control * _this = (cloudplatform_control *)args;

    while(true)
    {
        sleep(0.1);
        if(_this->_cmd_read_queue.size() < 10)
        {
            pthread_mutex_lock(&_this->_mutex_read);
            std::string get_xydegree = _this->device_id + "/0/0/0";
            _this->_cmd_read_queue.push_back(get_xydegree);
            sleep(0.2);
            std::string get_zdegree = _this->device_id + "/0/1/0";
            _this->_cmd_read_queue.push_back(get_zdegree);
            sleep(0.2);
            pthread_mutex_unlock(&_this->_mutex_read);
        }

        if((g_action == 1) &&   ((g_control_type == 0) || (g_control_type == 1)))
        {
			xy_diff_val = g_xy_goal - g_now_xyposition;
            //std::cout << "g_xy_goal: " << g_xy_goal << std::endl;
            //std::cout << "g_now_xyposition: " << g_now_xyposition << std::endl;
            //std::cout << "xy_val: " << xy_diff_val << std::endl;
			if((xy_diff_val > 35900) || (xy_diff_val < -35900))
				xy_diff_val = 0;
            z_diff_val = g_z_goal - g_now_zposition;
            //std::cout << "g_z_goal: " << g_z_goal << std::endl;
            //std::cout << "g_now_zposition: " << g_now_zposition << std::endl;
            //std::cout << "z_val: " << z_diff_val << std::endl;
			if((z_diff_val > 35900) || (z_diff_val < -35900))
				z_diff_val = 0;
			if(((z_diff_val < 600) && (z_diff_val > -600)) && ((xy_diff_val < 600) && (xy_diff_val > -600)))
            {
				g_action = -1;
                g_xy_reach_flag = 1;
                g_z_reach_flag = 1;
            }
        }
    }
}

void *cmd_work(void* args)
{
    while(true)
    {
        sleep(0.1);
        std::string work_cmd = "";
        cloudplatform_control * _this = (cloudplatform_control *)args;
        if(_this->_cmd_control_queue.size() > 0)
        {
            pthread_mutex_lock(&_this->_mutex_control);
            std::cout << "before execute, _cmd_control_queue size is : " << _this->_cmd_control_queue.size() << std::endl;
            work_cmd = _this->_cmd_control_queue[0];
            _this->_cmd_control_queue.pop_front();
            std::cout << "after execute, _cmd_control_queue size is : " << _this->_cmd_control_queue.size() << std::endl;
            pthread_mutex_unlock(&_this->_mutex_control);

            if(work_cmd != "")
            {
                vector<std::string> cmd_value_strv;
                SplitString(work_cmd, cmd_value_strv, "/");

                int id = atoi(cmd_value_strv[0].c_str());
                int action = atoi(cmd_value_strv[1].c_str());
                int type = atoi(cmd_value_strv[2].c_str());
                int value = atoi(cmd_value_strv[3].c_str());
                //std::cout << "id: " << id << ", action: " << action << ", type: " << type << ", value: " << value << std::endl;

                g_action = action;
                if((type == 0) || (type == 1))
                {
                    g_control_type = type;
                    if(g_control_type == 0)
                        g_xy_goal = value;
                    else if(g_control_type == 1)
                        g_z_goal = value;
                }
            }
        }
        else if(_this->_cmd_read_queue.size() > 0)
        {
            pthread_mutex_lock(&_this->_mutex_read);
            //std::cout << "before execute, _cmd_read_queue size is : " << _this->_cmd_read_queue.size() << std::endl;
            work_cmd = _this->_cmd_read_queue[0];
            _this->_cmd_read_queue.pop_front();
            //std::cout << "after execute, _cmd_read_queue size is : " << _this->_cmd_read_queue.size() << std::endl;
            pthread_mutex_unlock(&_this->_mutex_read);
        }
        if(work_cmd != "")
        {
            unsigned int result_value = 0;
            vector<std::string> cmd_value_strv;
            SplitString(work_cmd, cmd_value_strv, "/");

            int id = atoi(cmd_value_strv[0].c_str());
            int action = atoi(cmd_value_strv[1].c_str());
            int type = atoi(cmd_value_strv[2].c_str());
            int value = atoi(cmd_value_strv[3].c_str());
            //std::cout << "id: " << id << ", action: " << action << ", type: " << type << ", value: " << value << std::endl;

            if(id == 0)
            {
                if ((action == 0) && (type == 0))
                {
                    //std::cout << "---获取水平绝对角度---" << std::endl;
                    g_cmd_cloudplat[2] = 0x47;
                    g_cmd_cloudplat[3] = 0x00;
                }
                else if ((action == 0) && (type == 1))
                {
                    //std::cout << "---获取垂直绝对角度---" << std::endl;
                    g_cmd_cloudplat[2] = 0x49;
                    g_cmd_cloudplat[3] = 0x00;
                }
                else if ((action == 0) && (type == 2))
                {
                    std::cout << "---获取变倍绝对值---" << std::endl;
                    g_cmd_cloudplat[2] = 0x00;
                    g_cmd_cloudplat[3] = 0x55;
                }
                else if ((action == 0) && (type == 3))
                {
                    std::cout << "---转到预置位---" << std::endl;
                    g_cmd_cloudplat[2] = 0x00;
                    g_cmd_cloudplat[3] = 0x07;
                }
                else if ((action == 0) && (type == 4))
                {
                    std::cout << "---获取聚焦位置---" << std::endl;
                    g_cmd_cloudplat[2] = 0x00;
                    g_cmd_cloudplat[3] = 0x57;
                }
                else if ((action == 1) && (type == 0))
                {
                    std::cout << "---设置水平绝对角度---" << std::endl;
                    g_cmd_cloudplat[2] = 0x41;
                    g_cmd_cloudplat[3] = 0x00;
                }
                else if ((action == 1) && (type == 1))
                {
                    std::cout << "---设置垂直绝对角度---" << std::endl;
                    g_cmd_cloudplat[2] = 0x42;
                    g_cmd_cloudplat[3] = 0x00;
                }
                else if ((action == 1) && (type == 2))
                {
                    std::cout << "---设置变倍绝对值---" << std::endl;
                    g_cmd_cloudplat[2] = 0x00;
                    g_cmd_cloudplat[3] = 0x5D;
                }
                else if ((action == 1) && (type == 3))
                {
                    std::cout << "---设置预置位---" << std::endl;
                    g_cmd_cloudplat[2] = 0x00;
                    g_cmd_cloudplat[3] = 0x03;
                }
                else if ((action == 1) && (type == 4))
                {
                    std::cout << "---设置聚焦位置---" << std::endl;
                    g_cmd_cloudplat[2] = 0x00;
                    g_cmd_cloudplat[3] = 0x5F;
                }
                else if ((action == 1) && (type == 5))
                {
                    std::cout << "---设置o---" << std::endl;
                    g_cmd_cloudplat[2] = 0x00;
                    g_cmd_cloudplat[3] = 0x9B;
                }
                else
                    std::cout << "Action or type error!" << std::endl;
                g_cmd_cloudplat[4] = value / 256;
                g_cmd_cloudplat[5] = value % 256;
                g_cmd_cloudplat[6] = _this->get_sumcs(g_cmd_cloudplat);
                result_value = _this->cmd_serialcontrol(g_cmd_cloudplat, LENGTH, action, type);
            }
            else if((id == 1) || (id == 2))
            {
                if ((action == 0) && (type == 0))
                {
                    //std::cout << "---获取水平绝对角度---" << std::endl;
                    g_cmd_cloudplat[2] = 0x00;
                    g_cmd_cloudplat[3] = 0x51;
                }
                else if ((action == 0) && (type == 1))
                {
                    //std::cout  << "---获取垂直绝对角度---" << std::endl;
                    g_cmd_cloudplat[2] = 0x00;
                    g_cmd_cloudplat[3] = 0x53;
                }
                else if ((action == 0) && (type == 2))
                {
                    std::cout  << "---获取变倍绝对值---" << std::endl;
                    g_cmd_cloudplat[2] = 0x00;
                    g_cmd_cloudplat[3] = 0x55;
                }
                else if ((action == 0) && (type == 3))
                {
                    std::cout  << "---转到预置位---" << std::endl;
                    g_cmd_cloudplat[2] = 0x00;
                    g_cmd_cloudplat[3] = 0x07;
                }
                else if ((action == 1) && (type == 0))
                {
                    std::cout  << "---设置水平绝对角度---" << std::endl;
                    g_cmd_cloudplat[2] = 0x00;
                    g_cmd_cloudplat[3] = 0x4B;
                }
                else if ((action == 1) && (type == 1))
                {
                    std::cout  << "---设置垂直绝对角度---" << std::endl;
                    g_cmd_cloudplat[2] = 0x00;
                    g_cmd_cloudplat[3] = 0x4D;
                }
                else if ((action == 1) && (type == 2))
                {
                    std::cout  << "---设置变倍绝对值---" << std::endl;
                    g_cmd_cloudplat[2] = 0x00;
                    g_cmd_cloudplat[3] = 0x4F;
                }
                else if ((action == 1) && (type == 3))
                {
                    std::cout  << "---设置预置位---" << std::endl;
                    g_cmd_cloudplat[2] = 0x00;
                    g_cmd_cloudplat[3] = 0x03;
                }
                g_cmd_cloudplat[4] = value / 256;
                g_cmd_cloudplat[5] = value % 256;
                if((action == 2) && (type == 0))
                {
                    std::cout  << "---设置连续向右旋转---" << std::endl;
                    g_cmd_cloudplat[2] = 0x00;
                    g_cmd_cloudplat[3] = 0x02;
                    g_cmd_cloudplat[4] = value;
                    g_cmd_cloudplat[5] = 0x00;
                }
                else if((action == 2) && (type == 1))
                {
                    std::cout  << "---设置连续向左旋转---" << std::endl;
                    g_cmd_cloudplat[2] = 0x00;
                    g_cmd_cloudplat[3] = 0x04;
                    g_cmd_cloudplat[4] = value;
                    g_cmd_cloudplat[5] = 0x00;
                }
                else if((action == 2) && (type == 2))
                {
                    std::cout  << "---设置连续向上旋转---" << std::endl;
                    g_cmd_cloudplat[2] = 0x00;
                    g_cmd_cloudplat[3] = 0x08;
                    g_cmd_cloudplat[4] = 0x00;
                    g_cmd_cloudplat[5] = value;
                }
                else if((action == 2) && (type == 3))
                {
                    std::cout  << "---设置连续向下旋转---" << std::endl;
                    g_cmd_cloudplat[2] = 0x00;
                    g_cmd_cloudplat[3] = 0x10;
                    g_cmd_cloudplat[4] = 0x00;
                    g_cmd_cloudplat[5] = value;
                }
                else if((action == 3) && (type == 0))
                {
                    std::cout  << "---设置停止旋转---" << std::endl;
                    g_cmd_cloudplat[2] = 0x00;
                    g_cmd_cloudplat[3] = 0x00;
                    g_cmd_cloudplat[4] = 0x00;
                    g_cmd_cloudplat[5] = 0x00;
                }
                if((action > 3) || (type > 3))
                    std::cout  << "Action or type error!" << std::endl;
                g_cmd_cloudplat[6] = _this->get_sumcs(g_cmd_cloudplat);
                result_value = _this->cmd_serialcontrol(g_cmd_cloudplat, LENGTH, action, type);
            }
            if((action == 0) && (type == 0))
            {
                g_now_xyposition = result_value;
            }
            else if((action == 0) && (type == 1))
            {
                g_now_zposition = result_value;
            }
        }
    }
}

cloudplatform_control::cloudplatform_control(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle)
    :node_handle_(node_handle), private_node_handle_(private_node_handle)
{
    private_node_handle_.param<std::string>("platform1_str", platform1_str, "/dev/ttyS2");
    private_node_handle_.param<std::string>("platform2_str", platform2_str, "/dev/ttyS2");
    private_node_handle_.param<std::string>("platform3_str", platform3_str, "/dev/ttyS2");
    private_node_handle_.param<std::string>("device_id", device_id, "1");
    private_node_handle_.param<std::string>("band_rate", band_rate, "9600");
    private_node_handle_.param<std::string>("heartbeat_topic_string", heartbeat_topic_str, "/yida/heartbeat");

    std::cout << "platform1_str: " << platform1_str << std::endl;
    std::cout << "platform2_str: " << platform2_str << std::endl;
    std::cout << "platform3_str: " << platform3_str << std::endl;
    std::cout << "device_id: " << device_id << std::endl;
    
    cloudplatform_server = node_handle_.advertiseService("/yida/internal/platform_cmd", &cloudplatform_control::handle_cloudplatform, this);

    std::string device_name = "";
    if(device_id == "1")
    {
        device_name = platform1_str;
        unsigned char temp_arr_xy[7] = {0xFF, 0x01, 0x00, 0x51, 0x00, 0x00, 0x52};
        memcpy(g_cmd_cloudplat_getxy,temp_arr_xy,sizeof(temp_arr_xy));
        //std::cout << sizeof(temp_arr_xy) << std::endl;
        unsigned char temp_arr_z[7] = {0xFF, 0x01, 0x00, 0x53, 0x00, 0x00, 0x54};
        memcpy(g_cmd_cloudplat_getz,temp_arr_z,sizeof(temp_arr_z));
        unsigned char temp_arr[7] = {0xFF, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00};
        memcpy(g_cmd_cloudplat, temp_arr, sizeof(temp_arr));
    }
    else if(device_id == "2")
    {
        device_name = platform2_str;
        unsigned char temp_arr_xy[7] = {0xFF, 0x01, 0x00, 0x51, 0x00, 0x00, 0x52};
        memcpy(g_cmd_cloudplat_getxy,temp_arr_xy,sizeof(temp_arr_xy));
        unsigned char temp_arr_z[7] = {0xFF, 0x01, 0x00, 0x53, 0x00, 0x00, 0x54};
        memcpy(g_cmd_cloudplat_getz,temp_arr_z,sizeof(temp_arr_z));
        unsigned char temp_arr[7] = {0xFF, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00};
        memcpy(g_cmd_cloudplat, temp_arr, sizeof(temp_arr));
    }
    else if(device_id == "3")
    {
        device_name = platform3_str;
        unsigned char temp_arr_xy[7] = {0xFF, 0x01, 0x00, 0x51, 0x00, 0x00, 0x52};
        memcpy(g_cmd_cloudplat_getxy,temp_arr_xy,sizeof(temp_arr_xy));
        unsigned char temp_arr_z[7] = {0xFF, 0x01, 0x00, 0x53, 0x00, 0x00, 0x54};
        memcpy(g_cmd_cloudplat_getz,temp_arr_z,sizeof(temp_arr_z));
        unsigned char temp_arr[7] = {0xFF, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00};
        memcpy(g_cmd_cloudplat, temp_arr, sizeof(temp_arr));
    }

    level = 0;
    message = "cloutplatform is ok!";
    pthread_mutex_init(&_mutex_read, NULL);
    pthread_mutex_init(&_mutex_control, NULL);

    heartbeat_pub_ = node_handle_.advertise<diagnostic_msgs::DiagnosticArray>(heartbeat_topic_str, 1);
    cloudplatform_pub_ = node_handle_.advertise<diagnostic_msgs::DiagnosticArray>("/yida/internal/platform_status", 1);
    isreach_pub_ = node_handle_.advertise<std_msgs::Int32>("/yida/platform_isreach", 1);
    yuntaiposition_pub_ = node_handle_.advertise<nav_msgs::Odometry>("/yida/yuntai/position", 1);

	int n_res = m_obj_cloudplat.connect(device_name, atoi(band_rate.c_str()));
    if(OPEN_FAILED == n_res)
	{
		std::cout << "Open device faild!" << std::endl;
	}

    create_getdegree_thread();
    create_work_thread();
}
    
cloudplatform_control::~cloudplatform_control()
{

}

bool cloudplatform_control::handle_cloudplatform(yd_cloudplatform::CloudPlatControl::Request &req, yd_cloudplatform::CloudPlatControl::Response &res)
{
    pthread_mutex_lock(&_mutex_control);
    std::stringstream ss;
	ss.clear();
	ss.str("");
	ss << req.id << "/" << req.action << "/" << req.type << "/" << req.value;
    std::string s_cmd = ss.str();
    _cmd_control_queue.push_back(s_cmd);
    pthread_mutex_unlock(&_mutex_control);
	return true;
}

unsigned int cloudplatform_control::get_sumcs(unsigned char cmdin[])
{
    unsigned int result = (cmdin[1] + cmdin[2] + cmdin[3] + cmdin[4] + cmdin[5]) % 256;
	return result;
}

unsigned int cloudplatform_control::cmd_serialcontrol(unsigned char cmd_cloudplat[], unsigned int size, unsigned int action, unsigned int type)
{
    sleep(0.2);
    int res = m_obj_cloudplat.serial_write(cmd_cloudplat, size);
    if(res == -1)
    {
        level = 2;
        message = "IO is error!";
        return 0;
    }
	//sleep(0.1);
    unsigned char *p_getdata;
	p_getdata = m_obj_cloudplat.serial_read(size);
    //for (int n=0;n<size;n++)
	//	printf("  0x%x\t%02x\n",p_getdata+n,p_getdata[n]);

    if ((action == 0) and (type != 3))
		return p_getdata[4] * 256 + p_getdata[5];
    else
		return 1;
}

void cloudplatform_control::create_getdegree_thread()
{
    pthread_t ros_thread = 0; 
    pthread_create(&ros_thread,NULL,get_degree,(void*)this);
}

void cloudplatform_control::create_work_thread()
{
    pthread_t ros_thread = 1; 
    pthread_create(&ros_thread,NULL,cmd_work,(void*)this);
}

void cloudplatform_control::update()
{
    pub_heartbeat(level, message);

    nav_msgs::Odometry msg_pos;
	msg_pos.header.stamp = ros::Time::now();
	msg_pos.pose.pose.position.x = g_now_xyposition;
	msg_pos.pose.pose.position.z = g_now_zposition;
	yuntaiposition_pub_.publish(msg_pos);

    if((g_xy_reach_flag == 1) && (g_z_reach_flag == 1))
    {
        g_xy_reach_flag = 0;
        g_z_reach_flag = 0;
        g_reach_flag = 1;
        std_msgs::Int32 res_msg;
		res_msg.data = 1;
		isreach_pub_.publish(res_msg);
    }

    diagnostic_msgs::DiagnosticArray status_msg;
    status_msg.header.stamp = ros::Time::now();
    diagnostic_msgs::DiagnosticStatus status_diagnostic;
    status_diagnostic.name = "platform_angle";
    status_diagnostic.message = std::to_string(g_now_xyposition) + " " + std::to_string(g_now_zposition);
    status_msg.status.push_back(status_diagnostic);
    cloudplatform_pub_.publish(status_msg);
}

void cloudplatform_control::pub_heartbeat(int level, string message)
{
    diagnostic_msgs::DiagnosticArray log;
    log.header.stamp = ros::Time::now();

    diagnostic_msgs::DiagnosticStatus s;
    s.name = __app_name__;             // 这里写节点的名字
    s.level = level;                   // 0 = OK, 1 = Warn, 2 = Error
    if (!message.empty())
    {
        s.message = message;           // 问题描述
    }
    s.hardware_id = "cloutplatform";   // 硬件信息
    log.status.push_back(s);
    sleep(0.1);
    heartbeat_pub_.publish(log);
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, __app_name__);
    ros::NodeHandle node_handle;
    ros::NodeHandle private_node_handle("~");
    cloudplatform_control control_obj(node_handle, private_node_handle);

    ros::Rate rate(20);
    while(ros::ok())
    {
        control_obj.update();
        ros::spinOnce();
        rate.sleep();
    }

    return 0;
}
