#include <ros/ros.h>
#include <pthread.h>
#include <string.h>
#include <string>
#include <math.h>
#include <sstream>
#include "jsoncpp/json/json.h"
#include "jsoncpp/json/value.h"
#include "jsoncpp/json/writer.h"

#include "yd_cloudplatform/serial_comm.hpp"
#include "yd_cloudplatform/infrared_ptz.hpp"
#include "yd_cloudplatform/common.hpp"

void *sub_run(void* args)
{
    infrared_ptz * _this = (infrared_ptz *)args;
    _this->infrared_image_sub_ = _this->node_handle_.subscribe("/yida/internal/infrared/image_proc/compressed", 1, &infrared_ptz::infrared_image_callback, _this);
    _this->visible_image_sub_ = _this->node_handle_.subscribe("/yida/internal/visible/image_proc/compressed", 1, &infrared_ptz::visible_image_callback, _this);
    ros::spin();

	return nullptr;
}

float infrared_ptz::get_xydegree(vector<float> camera_position, vector<float> device_position, vector<float> cloudplatform_position)
{
    float xydegree = 0.0;
	float vector_camera_x = camera_position[0] - cloudplatform_position[0];
	float vector_camera_y = camera_position[1] - cloudplatform_position[1];
	float vector_device_x = device_position[0] - cloudplatform_position[0];
	float vector_device_y = device_position[1] - cloudplatform_position[1];

	float center_camera_distance = sqrt(pow(vector_camera_x, 2) + pow(vector_camera_y, 2));
	float center_device_distance = sqrt(pow(vector_device_x, 2) + pow(vector_device_y, 2));

	float circle_x = cloudplatform_position[0];
	float circle_y = cloudplatform_position[1];
	float circle_r = sqrt(pow(vector_camera_x, 2) + pow(vector_camera_y, 2));
	//the  distance between o and device is center_device_distance
	if(center_device_distance <= circle_r)
    {
		std::cout << "the point is in circle!" << std::endl;
		return 0;
    }
	else if(center_device_distance > circle_r)
    {
		//点到切点距离
		float qiedian_device_distance = sqrt(pow(center_device_distance, 2) - pow(circle_r, 2));
		//计算切线与点心连线的夹角
		float f1 = acos(circle_r / center_device_distance)* 180 / PI;
		float f2 = atan2(vector_camera_y, vector_camera_x)* 180 / PI;
		float f3 = atan2(vector_device_y, vector_device_x)* 180 / PI;
		float f4 = 0;
        std::cout << "f1 = " << f1 << ", f2 = " << f2 << ", f3 = " << f3 << std::endl;

		/*
        if(f3 > 0){
		    f4 = f1 + f3 - f2;
        }
        else{
            f4 = f1 + f3 - f2;
        }
		xydegree = 360 - f4;
		if (xydegree > 360){
			xydegree = -f4;
        }*/
        if(f2 > 0){
            f4 = 360 - (f2-f3) - f1;
        }
		else{
			f4 = f3 - f2 - f1;
        }
        xydegree = 360 - f4;
		if(xydegree > 360){
			xydegree = -f4;
        }
    }
	xydegree = xydegree + _handlexy;
    std::cout << "xydegree in **************************" << xydegree << " ." << std::endl;
	return xydegree;
}

vector<float> infrared_ptz::get_newpoint(vector<float> camera_position, vector<float> device_position, vector<float> cloudplatform_position, int xydegree)
{
    float vector_camera_x = camera_position[0] - cloudplatform_position[0];
	float vector_camera_y = camera_position[1] - cloudplatform_position[1];
	float vector_device_x = device_position[0] - cloudplatform_position[0];
	float vector_device_y = device_position[1] - cloudplatform_position[1];

	float circle_x = float(cloudplatform_position[0]);
	float circle_y = float(cloudplatform_position[1]);
	float circle_r = sqrt(pow(vector_camera_x, 2) + pow(vector_camera_y, 2));

	float x_diff = circle_r * cos(xydegree);
	float y_diff = circle_r * sin(xydegree);
	std::cout << "the x_diff is " << x_diff << ", the y_diff is " << y_diff << std::endl;

	float x_point_new = cloudplatform_position[0] + x_diff;
	float y_point_new = cloudplatform_position[1] + y_diff;

	vector<float> result;
	result.push_back(x_point_new);
	result.push_back(y_point_new);
	return result;	
}

float infrared_ptz::get_zdegree(float x_newpoint, float y_newpoint, float z_camera, vector<float> device_position, float angley)
{
	float x_distance = device_position[0] - x_newpoint;
	float y_distance = device_position[1] - y_newpoint;
	float z_distance = device_position[2] - z_camera;
	float xy_distance = sqrt(pow(x_distance, 2) + pow(y_distance, 2));
	float zxy_distance = sqrt(pow(z_distance, 2) + pow(xy_distance, 2));

	if(xy_distance == 0.0)
	{
		xy_distance = 0.00001;
	}
	float zdegree = atan(z_distance / xy_distance);
	float zdegree_o = zdegree * 180 / PI;
	std::cout << "get_zdegree result is " << zdegree_o << std::endl;

	float l = 0.05;
	if(zxy_distance == 0.0)
	{
		zxy_distance = 0.00001;
	}
	float theao_device_center = acos(l / zxy_distance);
	float theao_devicecenter_degree = theao_device_center * 180 / PI;
	std::cout << "the camera and device end radian is " << theao_device_center << ", degree is " << theao_devicecenter_degree << std::endl;

	float zdegree_final = 0.0;
	if(z_distance >= 0)
		zdegree_final = zdegree_o + theao_devicecenter_degree - 90;
	else if(z_distance <= 0)
		zdegree_final = zdegree_o + theao_devicecenter_degree - 90;
	std::cout << "the zdegree_final degree is " << zdegree_final << std::endl;

	return zdegree_final + _handlez + _angley * 180 / PI;
}

void infrared_ptz::set_position_first()
{
	if(_use_hk_cloudplatform){
		yd_cloudplatform::CloudPlatControl cloudplat_control;
		cloudplat_control.request.id = _camera_id;
		cloudplat_control.request.action = 1;
		cloudplat_control.request.type = 4;
		cloudplat_control.request.value = 0;
		vector<unsigned int> value;
		value.push_back(0);
		value.push_back(0);
		value.push_back(0);
		cloudplat_control.request.allvalue = value;
		if(cloudplatform_client.call(cloudplat_control))
			std::cout << "set xyz degree success!" << std::endl;
		else
			std::cout << "set xyz degree failed!" << std::endl;
		sleep(0.5);
	}
	else{
		yd_cloudplatform::CloudPlatControl cloudplat_control;
		cloudplat_control.request.id = _camera_id;
		cloudplat_control.request.action = 1;
		cloudplat_control.request.type = 0;
		cloudplat_control.request.value = 0;
		if(cloudplatform_client.call(cloudplat_control))
			std::cout << "set xy degree success!" << std::endl;
		else
			std::cout << "set xy degree failed!" << std::endl;
		sleep(0.5);
		
		cloudplat_control.request.id = _camera_id;
		cloudplat_control.request.action = 1;
		cloudplat_control.request.type = 1;
		cloudplat_control.request.value = 0;
		if(cloudplatform_client.call(cloudplat_control))
			std::cout << "set z degree success!" << std::endl;
		else
			std::cout << "set z degree failed!" << std::endl;
		sleep(0.5);

		cloudplat_control.request.id = _camera_id;
		cloudplat_control.request.action = 1;
		cloudplat_control.request.type = 2;
		cloudplat_control.request.value = 0;
		if(cloudplatform_client.call(cloudplat_control))
			std::cout << "set zoom degree success!" << std::endl;
		else
			std::cout << "set zoom degree failed!" << std::endl;
		sleep(0.5);
	}
}

void infrared_ptz::points_callback(const nav_msgs::Odometry& msg)
{
	 //创建StampedTransform对象存储变换结果数据
    tf::StampedTransform transform_camera;
    tf::StampedTransform transform_cloudplatform;
    
    //监听包装在一个try-catch块中以捕获可能的异常
    try{
      listener.lookupTransform("/map", "/infrared_link", ros::Time(0), transform_camera);
      listener.lookupTransform("/map", "/cloudplatform_link", ros::Time(0), transform_cloudplatform);
    }
    catch (tf::TransformException &ex) {
      ROS_ERROR("%s",ex.what());
    }

    _x_camera = transform_camera.getOrigin().x();
	_y_camera = transform_camera.getOrigin().y();
	_z_camera = transform_camera.getOrigin().z();

	_x_cloudplatform = transform_cloudplatform.getOrigin().x();
	_y_cloudplatform = transform_cloudplatform.getOrigin().y();
	_z_cloudplatform = transform_cloudplatform.getOrigin().z();

	_angley = msg.twist.twist.angular.y;
	_anglez = msg.twist.twist.angular.z;
}

void infrared_ptz::transfer_callback(const yidamsg::transfer& msg)
{
    yidamsg::InspectedResult imagezoom_msg;
	yd_cloudplatform::CloudPlatControl cloudplat_control_client;
	if(transferpub_msg.size() > 0)
	{
		transferpub_msg.clear();
	}

	if(msg.flag == 0)
	{
		std::string str_devicepoint = msg.data;   
		SplitString(str_devicepoint, transferpub_msg, "/");
		
		//device_id = transferpub_msg[2];
		if((transferpub_msg[3] == "2") || (transferpub_msg[3] == "5"))
		{
			vector<std::string> device_point;
			SplitString(transferpub_msg[4], device_point, ",");
			float x_device = atof(device_point[0].c_str());
			float y_device = atof(device_point[1].c_str());
			float z_device = atof(device_point[2].c_str());
			std::cout << "=====x_device, y_device, z_device value is: " << x_device << " " << y_device << " " << z_device << " .=====" << std::endl;
			std::cout << "=====x_camera, y_camera, z_camera value is: " << _x_camera << " " << _y_camera << " " << _z_camera << " .=====" << std::endl;
			std::cout << "=====x_cloudplatform, y_cloudplatform, z_cloudplatform value is: " << _x_cloudplatform << " " << _y_cloudplatform << " " << _z_cloudplatform << " .=====" << std::endl;

			vector<float> camera_position;
			camera_position.push_back(_x_camera);
			camera_position.push_back(_y_camera);
			camera_position.push_back(_z_camera);

			vector<float> cloudplatform_position;
			cloudplatform_position.push_back(_x_cloudplatform);
			cloudplatform_position.push_back(_y_cloudplatform);
			cloudplatform_position.push_back(_z_cloudplatform);

			vector<float> device_position;
			device_position.push_back(x_device);
			device_position.push_back(y_device);
			device_position.push_back(z_device);

			float xydegree = get_xydegree(camera_position, device_position, cloudplatform_position);
			vector<float> new_position = get_newpoint(camera_position, device_position, cloudplatform_position, xydegree);
			float zdegree = get_zdegree(new_position[0], new_position[1], _z_camera, device_position, _angley);
		
			float finalx_set = xydegree;
			int val_panpos = 0;
			std::cout << "finalx_set 11111 is " << finalx_set << std::endl;
			if(finalx_set > 0)
			{
				finalx_set = val_panpos + abs(finalx_set * 100);
				std::cout << "finalx_set 22222 is " << finalx_set << std::endl;
				if(finalx_set > 35999)
					finalx_set = finalx_set - 35999;
			}
			else if(finalx_set < 0)
			{
				finalx_set = val_panpos - abs(finalx_set * 100);
				std::cout << "finalx_set 33333 is " << finalx_set << std::endl;
				if(finalx_set < 0)
					finalx_set = 35999 + finalx_set;
			}

			float finalz_set = 0.0;
			int val_tiltpos = 0;
			if(_z_max_degree > _z_min_degree)
			{
				finalz_set = val_tiltpos - zdegree * 100;
				if(z_device >= _z_camera)
				{
					if((finalz_set >= _z_min_degree) && (finalz_set < _z_max_degree))
						finalz_set = _z_max_degree;
					else if(finalz_set < 0)
						finalz_set = _z_origin_degree + finalz_set;
				}
				if(z_device < _z_camera)
					if((finalz_set > _z_min_degree) && (finalz_set <= _z_max_degree))
						finalz_set = _z_min_degree;
					else if(finalz_set > _z_origin_degree)
						finalz_set = finalz_set - _z_origin_degree;
			}
			else if(_z_max_degree < _z_min_degree)
			{
				finalz_set = val_tiltpos + zdegree * 100;
				if(z_device >= _z_camera)
				{
					if(finalz_set > _z_origin_degree)
						finalz_set = finalz_set - _z_origin_degree;
				}
				if(z_device < _z_camera)
				{
					if(finalz_set > _z_origin_degree)
						finalz_set = finalz_set - _z_origin_degree;
				}
			}

			std::cout << "finalx_set: " << int(finalx_set) << ", finalz_set: " << int(finalz_set) << std::endl;

			if(_use_hk_cloudplatform){
				yd_cloudplatform::CloudPlatControl cloudplat_control;
				cloudplat_control.request.id = _camera_id;
				cloudplat_control.request.action = 1;
				cloudplat_control.request.type = 3;
				cloudplat_control.request.value = 0;
				vector<unsigned int> value;
            	value.push_back(int(finalx_set));
            	value.push_back(int(finalz_set));
				cloudplat_control.request.allvalue = value;
				if(cloudplatform_client.call(cloudplat_control))
					std::cout << "set xyz degree success!" << std::endl;
				else
					std::cout << "set xyz degree failed!" << std::endl;
				sleep(0.5);
			}
			else{
				yd_cloudplatform::CloudPlatControl cloudplat_control;
				cloudplat_control.request.id = _camera_id;
				cloudplat_control.request.action = 1;
				cloudplat_control.request.type = 0;
				cloudplat_control.request.value = int(finalx_set);
				if(cloudplatform_client.call(cloudplat_control))
					std::cout << "set xy degree success!" << std::endl;
				else
					std::cout << "set xy degree failed!" << std::endl;
				sleep(0.5);
				
				cloudplat_control.request.id = _camera_id;
				cloudplat_control.request.action = 1;
				cloudplat_control.request.type = 1;
				cloudplat_control.request.value = int(finalz_set);
				if(cloudplatform_client.call(cloudplat_control))
					std::cout << "set z degree success!" << std::endl;
				else
					std::cout << "set z degree failed!" << std::endl;
				sleep(0.5);
			}

			_cloudplatform_status_ready = true;
		}
		else if(transferpub_msg[3] == "6")
		{
			/////////////////////////////////////////////////////
			yd_cloudplatform::CloudPlatControl cloudplat_control;
			cloudplat_control.request.id = _camera_id;
			cloudplat_control.request.action = 2;
			cloudplat_control.request.type = 0;
			cloudplat_control.request.value = _scan_speed;
			if(cloudplatform_client.call(cloudplat_control))
			{
				std::cout << "set cmd success!" << std::endl;
			}
			else
			{
				std::cout << "set cmd failed!" << std::endl;
			}
			sleep(_scan_time);

			cloudplat_control.request.id = _camera_id;
			cloudplat_control.request.action = 3;
			cloudplat_control.request.type = 0;
			cloudplat_control.request.value = 0;
			if(cloudplatform_client.call(cloudplat_control))
			{
				std::cout << "set cmd success!" << std::endl;
			}
			else
			{
				std::cout << "set cmd failed!" << std::endl;
			}
			sleep(1);

			cloudplat_control.request.id = _camera_id;
			cloudplat_control.request.action = 1;
			cloudplat_control.request.type = 1;
			cloudplat_control.request.value = 34500;
			if(cloudplatform_client.call(cloudplat_control))
			{
				std::cout << "set xy degree success!" << std::endl;
			}
			else
			{
				std::cout << "set xy degree failed!" << std::endl;
			}
			sleep(1);

			///////////////////////////////////////////////////
			cloudplat_control.request.id = _camera_id;
			cloudplat_control.request.action = 2;
			cloudplat_control.request.type = 0;
			cloudplat_control.request.value = _scan_speed;
			if(cloudplatform_client.call(cloudplat_control))
			{
				std::cout << "set cmd success!" << std::endl;
			}
			else
			{
				std::cout << "set cmd failed!" << std::endl;
			}
			sleep(_scan_time);

			cloudplat_control.request.id = _camera_id;
			cloudplat_control.request.action = 3;
			cloudplat_control.request.type = 0;
			cloudplat_control.request.value = 0;
			if(cloudplatform_client.call(cloudplat_control))
			{
				std::cout << "set cmd success!" << std::endl;
			}
			else
			{
				std::cout << "set cmd failed!" << std::endl;
			}
			sleep(1);

			cloudplat_control.request.id = _camera_id;
			cloudplat_control.request.action = 1;
			cloudplat_control.request.type = 1;
			cloudplat_control.request.value = 33000;
			if(cloudplatform_client.call(cloudplat_control))
			{
				std::cout << "set xy degree success!" << std::endl;
			}
			else
			{
				std::cout << "set xy degree failed!" << std::endl;
			}
			sleep(1);

			////////////////////////////////////////////////////////
			cloudplat_control.request.id = _camera_id;
			cloudplat_control.request.action = 2;
			cloudplat_control.request.type = 0;
			cloudplat_control.request.value = _scan_speed;
			if(cloudplatform_client.call(cloudplat_control))
			{
				std::cout << "set cmd success!" << std::endl;
			}
			else
			{
				std::cout << "set cmd failed!" << std::endl;
			}
			sleep(_scan_time);

			cloudplat_control.request.id = _camera_id;
			cloudplat_control.request.action = 3;
			cloudplat_control.request.type = 0;
			cloudplat_control.request.value = 0;
			if(cloudplatform_client.call(cloudplat_control))
			{
				std::cout << "set cmd success!" << std::endl;
			}
			else
			{
				std::cout << "set cmd failed!" << std::endl;
			}
			sleep(1);

			set_position_first();

			std_msgs::String meterflag_msg;
			std::stringstream mss;
			mss.clear();
			mss.str("");
			mss << transferpub_msg[2] << "/2";
			meterflag_msg.data = mss.str();
			meterflag_pub_.publish(meterflag_msg);
			std::cout <<  "***************inspected_msg-eterflag_msg*************" << std::endl;
		}
	}
	else if(msg.flag == 1)
	{
		set_position_first();

		Json::Value json_obj;
		json_obj["motion_state"] = 1;
		std::string stream_data = json_obj.toStyledString();
		std_msgs::String append_data_msg;
		append_data_msg.data = stream_data;
		append_data_pub_.publish(append_data_msg);
	}
}

void infrared_ptz::infrared_image_callback(const sensor_msgs::CompressedImagePtr& msg)
{
	if(_infrared_image_char.size() > 0)
	{
		_infrared_image_char.clear();
	}
	_infrared_image_char = msg->data;
	//_infrared_image = cv::imdecode(cv::Mat(msg->data),1); //convert compressed image data to cv::Mat
}

void infrared_ptz::visible_image_callback(const sensor_msgs::CompressedImagePtr& msg)
{
	if(_visible_image_char.size() > 0)
	{
		_visible_image_char.clear();
	}
	_visible_image_char = msg->data;
	//_visible_image = cv::imdecode(cv::Mat(msg->data),1); //convert compressed image data to cv::Mat
}

void infrared_ptz::isreach_callback(const std_msgs::Int32& msg)
{
	if((msg.data == 1) && (_cloudplatform_status_ready == true) && (transferpub_msg[3] == "2" || transferpub_msg[3] == "5"))
	{
		sleep(5);

		_cloudplatform_status_ready = false;

        /*if(transferpub_msg[3] == "2")
        {
            yd_cloudplatform::GetTemperature gettemper;
            vector<unsigned short> x;
            x.push_back(130);
            x.push_back(190);
            vector<unsigned short> y;
            y.push_back(100);
            y.push_back(140);
			gettemper.request.spot_x = x;
			gettemper.request.spot_y = y;
            if(gettemperature_client.call(gettemper))
			    _get_temperature = gettemper.response.temperature_c;
            else
                _get_temperature = 0.0;
        }*/

		yidamsg::InspectedResult inspected_msg;
		inspected_msg.camid = _camera_id;
		//inspected_msg.equipimage = _infrared_image_char;
        //inspected_msg.nameplates = _visible_image_char;
		std::stringstream ss;
		ss.clear();
		ss.str("");
		ss << transferpub_msg[2] << ":" << transferpub_msg[3] << "/" << transferpub_msg[4] << "/" << transferpub_msg[5] << "/" << transferpub_msg[6] << "/" << transferpub_msg[7];
		inspected_msg.equipid = ss.str();
        std::stringstream oss;
        oss.clear();
		oss.str("");
        oss << _get_temperature;
		//inspected_msg.result = oss.str();
        inspected_msg.success = true;
		infrared_result_pub_.publish(inspected_msg);

		/*std_msgs::String meterflag_msg;
		std::stringstream mss;
        mss.clear();
		mss.str("");
        mss << transferpub_msg[2] << "/2";
		meterflag_msg.data = mss.str();
		meterflag_pub_.publish(meterflag_msg);
		std::cout <<  "***************inspected_msg-eterflag_msg*************" << std::endl;*/

		Json::Value json_obj;
		json_obj["motion_state"] = 0;
		json_obj["point_id"] = transferpub_msg[2];
		json_obj["point_type"] = transferpub_msg[3];
		json_obj["position"] = transferpub_msg[4];
		json_obj["object_id"] = transferpub_msg[5];
		json_obj["device_id"] = transferpub_msg[6];
		json_obj["task_id"] = transferpub_msg[7];
		std::string stream_data = json_obj.toStyledString();
		std_msgs::String append_data_msg;
		append_data_msg.data = stream_data;
		append_data_pub_.publish(append_data_msg);
	}
}

void infrared_ptz::meterflag_callback(const std_msgs::String& msg) {
	Json::Value json_obj;
	json_obj["motion_state"] = 1;
	std::string stream_data = json_obj.toStyledString();
	std_msgs::String append_data_msg;
	append_data_msg.data = stream_data;
	append_data_pub_.publish(append_data_msg);
}

infrared_ptz::infrared_ptz(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle)
    :node_handle_(node_handle), private_node_handle_(private_node_handle)
{
    std::string camera_focus_mm_str, camera_focus_value_str, xyz_degree_str;
	private_node_handle_.param<int>("use_hk_cloudplatform", _use_hk_cloudplatform, 1);
    private_node_handle_.param<double>("handleset_xy", _handlexy, 0.0);
    private_node_handle_.param<double>("handleset_z", _handlez, 2.0);
    private_node_handle_.param<int>("camera_id", _camera_id, 1);
    private_node_handle_.param<std::string>("xyz_degree", xyz_degree_str, "0,35999,35999,27000,9000");
    private_node_handle_.param<int>("offset_xy", _offset_xy, 0);
	private_node_handle_.param<int>("scan_speed", _scan_speed, 0);
	private_node_handle_.param<int>("scan_time", _scan_time, 0);
    private_node_handle_.param<std::string>("heartbeat_topic_string", heartbeat_topic_str, "/yida/heartbeat");

    vector<std::string> degree_value_strv;
    SplitString(xyz_degree_str, degree_value_strv, ",");
    _x_min_degree = atoi(degree_value_strv[0].c_str());
    _x_max_degree = atoi(degree_value_strv[1].c_str());
    _z_origin_degree = atoi(degree_value_strv[2].c_str());
    _z_max_degree = atoi(degree_value_strv[3].c_str());
    _z_min_degree = atoi(degree_value_strv[4].c_str());

	_x_camera = 0.0;
	_y_camera = 0.0; 
	_z_camera = 0.0;

    level = 0;
    message = "cloutplatform is ok!";
	_cloudplatform_status_ready = false;

	for(int i = 0; i < 10; i++)
	{
		transferpub_msg.push_back("init value");
	}

    heartbeat_pub_ = node_handle_.advertise<diagnostic_msgs::DiagnosticArray>(heartbeat_topic_str, 1);
    infrared_result_pub_ = node_handle_.advertise<yidamsg::InspectedResult>("/infrared_survey_parm", 1);
    meterflag_pub_ = node_handle_.advertise<std_msgs::String>("/meter_flag", 1);
	append_data_pub_ = node_handle_.advertise<std_msgs::String>("/streaming_service/append_data", 1);

    robot_pose_sub_ = node_handle_.subscribe("/robot_pose", 1, &infrared_ptz::points_callback, this);
    transfer_sub_ = node_handle_.subscribe("/transfer_pub", 1, &infrared_ptz::transfer_callback, this);
    isreach_sub_ = node_handle_.subscribe("/yida/platform_isreach", 1, &infrared_ptz::isreach_callback, this);
	meterflag_sub_ = node_handle_.subscribe("/meter_flag", 1, &infrared_ptz::meterflag_callback, this);

	cloudplatform_client = node_handle_.serviceClient<yd_cloudplatform::CloudPlatControl>("/yida/internal/platform_cmd");
    gettemperature_client = node_handle_.serviceClient<yd_cloudplatform::GetTemperature>("/yida/internal/get_temperature");
    
    pthread_t ros_thread = 0; 
	pthread_create(&ros_thread,NULL,sub_run,(void*)this);
}
    
infrared_ptz::~infrared_ptz()
{

}

void infrared_ptz::update()
{
    pub_heartbeat(level, message);
}

void infrared_ptz::pub_heartbeat(int level, string message)
{
    diagnostic_msgs::DiagnosticArray log;
    log.header.stamp = ros::Time::now();

    diagnostic_msgs::DiagnosticStatus s;
    s.name = __app_name__;             // 这里写节点的名字
    s.level = level;                   // 0 = OK, 1 = Warn, 2 = Error
    if (!message.empty())
    {
        s.message = message;           // 问题描述
    }
    s.hardware_id = "cloutplatform";   // 硬件信息
    log.status.push_back(s);
    sleep(0.1);
    heartbeat_pub_.publish(log);
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, __app_name__);
    ros::NodeHandle node_handle;
    ros::NodeHandle private_node_handle("~");
    infrared_ptz visible_ptz_obj(node_handle, private_node_handle);

    ros::Rate rate(20);
    while(ros::ok())
    {
        visible_ptz_obj.update();
        ros::spinOnce();
        rate.sleep();
    }

    return 0;
}
