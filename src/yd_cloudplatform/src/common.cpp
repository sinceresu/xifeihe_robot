#include "yd_cloudplatform/common.hpp"

using namespace std;

//字符串分割函数  
void SplitString(const string& s, vector<string>& v, const string& c)
{
    string::size_type pos1, pos2;
    pos2 = s.find(c);
    pos1 = 0;
    while(string::npos != pos2)
    {
        v.push_back(s.substr(pos1, pos2-pos1));

        pos1 = pos2 + c.size();
        pos2 = s.find(c, pos1);
    }

    if(pos1 != s.length())
    {
        v.push_back(s.substr(pos1));
    }
}

//字符串拼接函数
char* SplicingString(const char *a, const char *b)
{
    char* dest = new char[strlen(a)+strlen(b)+1];
    strcpy(dest,a);
    strcat(dest,b);
    return dest;
}
