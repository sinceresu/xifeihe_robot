#include <lz4.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <termios.h>

#include "tianboir_sdk/tianboir_sdk_control.hpp"

bool bSaveH = false;
bool bSaveT = false;

std::string GetCurrentDate(const std::string &_format)
{
    time_t nowtime;
    nowtime = time(NULL);
    char tmp[64];
    strftime(tmp, sizeof(tmp), _format.c_str(), localtime(&nowtime));

    return tmp;
}

tianboir_sdk_control *tianboir_sdk_control::p_this = nullptr;

tianboir_sdk_control::tianboir_sdk_control()
{
    p_this = this;
    //
    ros::NodeHandle private_nh("~");
    ros::NodeHandle nh;
    //
    std::string hik_sdk_dir;
    private_nh.param<std::string>("hik_sdk_dir", hik_sdk_dir, "/home/lh/catkin_ws/src/InspectionRobot/yd_cloudplatform/libs");
    private_nh.param<std::string>("hik_device_ip", hik_device_ip, "192.168.1.66");
    private_nh.param<std::string>("hik_device_port", hik_device_port, "8000");
    private_nh.param<std::string>("hik_username", hik_username, "admin");
    private_nh.param<std::string>("hik_password", hik_password, "abcd1234");
    private_nh.param<std::string>("hik_width", hik_width, "1920");
    private_nh.param<std::string>("hik_height", hik_height, "1080");
    private_nh.param<std::string>("tianboir_device_ip", tianboir_device_ip, "192.168.1.169");
    private_nh.param<std::string>("heartbeat_topic", heartbeat_topic, "/yd/heartbeat");
    private_nh.param<double>("ptz_time_difference", ptz_time_difference, 0.0);
    private_nh.param<double>("temp_time_difference", temp_time_difference, 0.0);
    // set param server
    ros_param_server = new param_server::Server("yd_cloudplatform", "config/config.yml");
    param_server::CallbackType call_back = boost::bind(&tianboir_sdk_control::param_Callback, this, _1);
    ros_param_server->setCallback(call_back);
    read_param();
    //
    level = 0;
    message = "cloutplatform is ok!";
    heartbeat_pub = nh.advertise<diagnostic_msgs::DiagnosticArray>(heartbeat_topic, 1);
    buffer_pub = nh.advertise<sensor_msgs::Image>("/tianboir/image_proc", 1);
    position_pub = nh.advertise<nav_msgs::Odometry>("/yida/yuntai/position", 1);
    cloudplatform_pub = nh.advertise<diagnostic_msgs::DiagnosticArray>("/yida/internal/platform_status", 1);
    inPlace_pub = nh.advertise<std_msgs::Int32>("/yida/platform_isreach", 1);
    temperature_pub = nh.advertise<sensor_msgs::Image>("/yida/internal/temperature", 1);
    detect_rect_sub = nh.subscribe("/detect_rect", 1, &tianboir_sdk_control::detect_rect_Callback, this);
    cloudplatform_server = nh.advertiseService("/yida/internal/platform_cmd", &tianboir_sdk_control::cloudplatform_server_Callback, this);
    temperature_server = nh.advertiseService("/yida/internal/get_temperature", &tianboir_sdk_control::temperature_server_Callback, this);

    if (init_tianboir_connect(tianboir_device_ip))
    {
        open_tianboir_capture();
    }

    NET_DVR_LOCAL_SDK_PATH struLocalSdkPath = {0};
    strcpy((char *)struLocalSdkPath.sPath, hik_sdk_dir.c_str());
    NET_DVR_SetSDKInitCfg(NET_SDK_INIT_CFG_SDK_PATH, &struLocalSdkPath);

    if (!init_hik_connect(hik_device_ip, hik_device_port, hik_username, hik_password))
    {
        printf("init_hik_connect failed, address:%s, port:%s ...", hik_device_ip.c_str(), hik_device_port.c_str());
        // return;
    }

    if (!SerialStart())
    {
        printf("SerialStart failed, address:%s, port:%s ...", hik_device_ip.c_str(), hik_device_port.c_str());
        // return;
    }

    // if (!open_hik_capture())
    // {
    //     printf("open_hik_capture failed, address:%s, port:%s ...", hik_device_ip.c_str(), hik_device_port.c_str());
    //     // return;
    // }

    // 云台姿态
    std::thread Position_thread(std::bind(&tianboir_sdk_control::PTZ_Position, this, nullptr));
    Position_thread.detach();

    // std::thread hik_image_thread(std::bind(&tianboir_sdk_control::hik_image, this, nullptr));
    // hik_image_thread.detach();
    //
    // std::thread tianboir_image_thread(std::bind(&tianboir_sdk_control::tianboir_image, this, nullptr));
    // tianboir_image_thread.detach();
}

tianboir_sdk_control::~tianboir_sdk_control()
{
    Destroy();
}

bool tianboir_sdk_control::Destroy()
{
    tianboir_disconnect();

    SerialStop();

    hik_disconnect();

    printf("Destroy \r\n");

    return true;
}

void tianboir_sdk_control::param_Callback(param_server::SimpleType &_config)
{
    ROS_INFO("param_Callback ...");

    read_param();
}

void tianboir_sdk_control::read_param()
{
    if (ros_param_server->exist("ptz_time_difference"))
    {
        ros_param_server->get("ptz_time_difference", ptz_time_difference);
        printf("ptz_time_difference data:%f \r\n", ptz_time_difference);
    }
    if (ros_param_server->exist("temp_time_difference"))
    {
        ros_param_server->get("temp_time_difference", temp_time_difference);
        printf("temp_time_difference data:%f \r\n", temp_time_difference);
    }
}

void tianboir_sdk_control::pub_heartbeat(int _level, std::string _message)
{
    diagnostic_msgs::DiagnosticArray msg;
    msg.header.stamp = ros::Time::now();

    diagnostic_msgs::DiagnosticStatus status;
    status.name = __app_name__;
    status.level = _level;                // 0 = OK, 1 = Warn, 2 = Error
    status.message = _message;            // 问题描述
    status.hardware_id = "cloutplatform"; // 硬件信息
    msg.status.push_back(status);

    heartbeat_pub.publish(msg);
}

void tianboir_sdk_control::pub_buffer(std::vector<unsigned char> _buff)
{
    sensor_msgs::Image msg;
    msg.header.stamp = ros::Time::now();
    // msg.width;
    // msg.height;
    // msg.encoding;
    // msg.is_bigendian;
    // msg.step;
    msg.data = _buff;
    buffer_pub.publish(msg);
}

void tianboir_sdk_control::pub_position(unsigned int _x, unsigned int _y, unsigned int _z)
{
    nav_msgs::Odometry msg;
    msg.header.stamp = ros::Time::now();
    msg.pose.pose.position.x = _x;
    msg.pose.pose.position.y = _y;
    msg.pose.pose.position.z = _z;
    msg.header.stamp += ros::Duration(ptz_time_difference);
    position_pub.publish(msg);
}

void tianboir_sdk_control::pub_cloudplatform(unsigned int _x, unsigned int _y, unsigned int _z)
{
    diagnostic_msgs::DiagnosticArray msg;
    msg.header.stamp = ros::Time::now();
    diagnostic_msgs::DiagnosticStatus status;
    status.name = "platform_angle";
    status.message = std::to_string(_x) + " " + std::to_string(_z);
    msg.status.push_back(status);
    cloudplatform_pub.publish(msg);
}

void tianboir_sdk_control::pub_inPlace(int _data)
{
    std_msgs::Int32 msg;
    msg.data = _data;
    inPlace_pub.publish(msg);
}

void tianboir_sdk_control::pub_temperature(int _width, int _height, unsigned char *_buf, int _size)
{
    sensor_msgs::Image msg;
    msg.header.stamp = ros::Time::now();
    msg.width = _width;
    msg.height = _height;
    // msg.encoding;
    // msg.is_bigendian;
    vector<unsigned char> data(_buf, _buf + _size);
    msg.step = _size;
    msg.data = data;
    msg.header.stamp += ros::Duration(ptz_time_difference);
    temperature_pub.publish(msg);
}

void tianboir_sdk_control::detect_rect_Callback(const yidamsg::Detect_Result::ConstPtr &msg)
{
    ROS_INFO_STREAM("detect_rect_Callback data:\n"
                    << *msg);
}

bool tianboir_sdk_control::cloudplatform_server_Callback(yd_cloudplatform::CloudPlatControl::Request &req,
                                                         yd_cloudplatform::CloudPlatControl::Response &res)
{
    ROS_INFO_STREAM("cloudplatform_server_Callback data:\n"
                    << req);

    control_goal.id = req.id;
    control_goal.action = req.action;
    if (req.type != 2)
    {
        control_goal.type = req.type;
    }
    //
    int id = req.id;
    int action = req.action;
    int type = req.type;
    int value = req.value;
    std::vector<unsigned int> allvalue = req.allvalue;
    // 获取
    if (action == 0)
    {
        if (type == 0)
        {
            // 获取水平
            unsigned char panBuf[7] = {0xFF, 0x01, 0x00, 0x51, 0x00, 0x00, 0x52};
            bool bP = SerialSend((char *)panBuf, 7);
        }
        if (type == 1)
        {
            // 获取垂直
            unsigned char tiltBuf[7] = {0xFF, 0x01, 0x00, 0x53, 0x00, 0x00, 0x54};
            bool bT = SerialSend((char *)tiltBuf, 7);
        }
        if (type == 2)
        {
            // 获取变倍
            unsigned char zoomBuf[7] = {0xFF, 0x01, 0x00, 0x55, 0x00, 0x00, 0x56};
            bool bZ = SerialSend((char *)zoomBuf, 7);
        }
        if (type == 3)
        {
            // 获取水平,垂直
        }
        if (type == 4)
        {
            // 获取水平,垂直,变倍
        }
    }
    // 设置
    if (action == 1)
    {
        int x = 0;
        int y = 0;
        int z = 0;
        int zoom = 0;
        //
        switch (type)
        {
        case 0: // 设置水平
            x = control_goal.x = value;
            break;
        case 1: // 设置垂直
            // z = control_goal.z = 36000 - value;
            z = control_goal.z = value;
            break;
        case 2: // 设置变倍
            zoom = control_goal.zoom = value;
            break;
        case 3: // 设置水平、垂直
            x = control_goal.x = allvalue[0];
            // z = control_goal.z = 36000 - allvalue[1];
            z = control_goal.z = allvalue[1];
            break;
        case 4: // 设置水平、垂直、变倍
            x = control_goal.x = allvalue[0];
            // z = control_goal.z = 36000 - allvalue[1];
            z = control_goal.z = allvalue[1];
            zoom = control_goal.zoom = allvalue[2];
            break;

        default:
            break;
        }
        //
        if ((type == 0 || type == 3 || type == 4) && (0 <= x <= 36000))
        {
            // 设置水平角度命令
            int panSize = 0;
            char panBuf[7] = {0};
            CmdBuf(0x4B, x, panBuf, panSize);
            bool bP = SerialSend((char *)panBuf, panSize);
        }
        if ((type == 1 || type == 3 || type == 4) && ((0 <= z <= 9000) || (27000 <= z <= 35999)))
        {
            // 设置垂直角度命令
            int tiltSize = 0;
            char tiltBuf[7] = {0};
            CmdBuf(0x4D, z, tiltBuf, tiltSize);
            bool bP = SerialSend((char *)tiltBuf, tiltSize);
        }
        if ((type == 2 || type == 4) && zoom > 0)
        {
            // 设置变倍命令
            int zoomSize = 0;
            char zoomBuf[7] = {0};
            CmdBuf(0x4F, zoom, zoomBuf, zoomSize);
            bool bP = SerialSend((char *)zoomBuf, zoomSize);
        }
    }
    // 转动
    if (action == 2)
    {
        if (type == 0)
        {
            // 云台右转
            unsigned char rightBuf[7] = {0xFF, 0x01, 0x00, 0x02, 0x2F, 0x00, 0x32};
            bool bR = SerialSend((char *)rightBuf, 7);
        }
        if (type == 1)
        {
            // 云台左转
            unsigned char leftBuf[7] = {0xFF, 0x01, 0x00, 0x04, 0x2F, 0x00, 0x34};
            bool bL = SerialSend((char *)leftBuf, 7);
        }
        if (type == 2)
        {
            // 云台上转
            unsigned char upBuf[7] = {0xFF, 0x01, 0x00, 0x08, 0x2F, 0x00, 0x38};
            bool bU = SerialSend((char *)upBuf, 7);
        }
        if (type == 3)
        {
            // 云台下转
            unsigned char downBuf[7] = {0xFF, 0x01, 0x00, 0x10, 0x2F, 0x00, 0x40};
            bool bD = SerialSend((char *)downBuf, 7);
        }
    }
    // 停止
    if (action == 3)
    {
        // 停止转动
        unsigned char stopBuf[7] = {0xFF, 0x01, 0x00, 0x00, 0x00, 0x00, 0x01};
        bool bS = SerialSend((char *)stopBuf, 7);
    }

    return true;
}

bool tianboir_sdk_control::temperature_server_Callback(yd_cloudplatform::GetTemperature::Request &req,
                                                       yd_cloudplatform::GetTemperature::Response &res)
{
    ROS_INFO_STREAM("temperature_server_Callback data:\n"
                    << req);

    return true;
}

// hik
bool tianboir_sdk_control::init_hik_connect(std::string _ip, std::string _port, std::string _username, std::string _password)
{
    // 初始化
    if (!NET_DVR_Init())
    {
        printf("NET_DVR_Init failed, error code: %d ...", NET_DVR_GetLastError());
        NET_DVR_Cleanup();
        return false;
    }
    // 设置连接时间与重连时间
    NET_DVR_SetConnectTime(6000, 1);
    // NET_DVR_SetRecvTimeOut(6000);
    NET_DVR_SetReconnect(6000, true);

    // // 注册设备
    // NET_DVR_DEVICEINFO_V30 struDeviceInfo30;
    // lUserID = NET_DVR_Login_V30((char *)_ip.c_str(), atoi(_port.c_str()), (char *)_username.c_str(), (char *)_password.c_str(), &struDeviceInfo30);
    // if (lUserID < 0)
    // {
    //     printf("NET_DVR_Login_V30 failed, error code: %d; address:%s, port:%s ...", NET_DVR_GetLastError(), _ip.c_str(), _port.c_str());
    //     NET_DVR_Cleanup();
    //     return false;
    // }
    // 注册设备
    NET_DVR_USER_LOGIN_INFO struLoginInfo = {0};
    NET_DVR_DEVICEINFO_V40 struDeviceInfo = {0};
    strcpy((char *)struLoginInfo.sDeviceAddress, _ip.c_str());  // 设备 IP 地址
    struLoginInfo.wPort = atoi(_port.c_str());                  // 设备端口
    strcpy((char *)struLoginInfo.sUserName, _username.c_str()); // 设备登录用户名
    strcpy((char *)struLoginInfo.sPassword, _password.c_str()); // 设备登录密码
    struLoginInfo.bUseAsynLogin = 0;                            // 同步登录,登录接口返回成功即登录成功
    //
    lUserID = NET_DVR_Login_V40(&struLoginInfo, &struDeviceInfo);
    if (lUserID < 0)
    {
        printf("NET_DVR_Login_V40 failed, error code: %d; address:%s, port:%s ...", NET_DVR_GetLastError(), _ip.c_str(), _port.c_str());
        return false;
    }

    return true;
}

bool tianboir_sdk_control::hik_disconnect()
{
    // 注销用户
    if (lUserID >= 0)
    {
        if (!NET_DVR_Logout(lUserID))
        {
            printf("NET_DVR_Logout error, %d\n", NET_DVR_GetLastError());
            return false;
        }
        lUserID = -1;
    }
    // 释放 SDK 资源
    if (!NET_DVR_Cleanup())
    {
        printf("NET_DVR_Cleanup error, %d\n", NET_DVR_GetLastError());
        return false;
    }

    return true;
}

bool tianboir_sdk_control::open_hik_capture()
{
    //---------------------------------------
    //启动预览
    //获取窗口句柄
    NET_DVR_PREVIEWINFO struPlayInfo = {0};
    struPlayInfo.hPlayWnd = 0;     //需要 SDK 解码时句柄设为有效值,仅取流不解码时可设为空
    struPlayInfo.lChannel = 1;     //预览通道号
    struPlayInfo.dwStreamType = 0; //0-主码流,1-子码流,2-码流 3,3-码流 4,以此类推
    struPlayInfo.dwLinkMode = 0;   //0- TCP 方式,1- UDP 方式,2- 多播方式,3- RTP 方式,4-RTP/RTSP,5-RSTP/HTTP
    struPlayInfo.bBlocked = 1;     //0- 非阻塞取流,1- 阻塞取流
    lRealPlayHandle = NET_DVR_RealPlay_V40(lUserID, &struPlayInfo, tianboir_sdk_control::g_RealDataCallBack_V30, this);
    if (lRealPlayHandle < 0)
    {
        printf("NET_DVR_RealPlay_V40 failed, error code: %d\n", NET_DVR_GetLastError());
        return false;
    }

    return true;
}

bool tianboir_sdk_control::close_hik_capture()
{
    //关闭预览
    if (lRealPlayHandle >= 0)
    {
        if (!NET_DVR_StopRealPlay(lRealPlayHandle))
        {
            printf("NET_DVR_StopRealPlay error, %d\n", NET_DVR_GetLastError());
            return false;
        }
        lRealPlayHandle = -1;
    }

    return true;
}

void CALLBACK tianboir_sdk_control::g_RealDataCallBack_V30(LONG lRealHandle, DWORD dwDataType, BYTE *pBuffer, DWORD dwBufSize, void *dwUser)
{
    tianboir_sdk_control *_this = (tianboir_sdk_control *)dwUser;

    switch (dwDataType)
    {
    case NET_DVR_SYSHEAD: //系统头
        if (!PlayM4_GetPort(&_this->lPort))
        {
            break;
            //获取播放库未使用的通道号
        }
        //m_iPort = lPort;
        //第一次回调的是系统头,将获取的播放库 port 号赋值给全局 port,下次回调数据时即使用此 port 号播放
        if (dwBufSize > 0)
        {
            if (!PlayM4_SetStreamOpenMode(_this->lPort, STREAME_REALTIME))
            //设置实时流播放模式
            {
                break;
            }
            if (!PlayM4_OpenStream(_this->lPort, pBuffer, dwBufSize, 1024 * 1024)) //打开流接口
            {
                break;
            }
            if (!PlayM4_Play(_this->lPort, 0)) //播放开始
            {
                break;
            }
            if (!PlayM4_SetDecCallBackEx(_this->lPort, tianboir_sdk_control::DecCBFun, 0, 0))
            {
                break;
            }
        }
        break;
    case NET_DVR_STREAMDATA:
        //码流数据
        if (dwBufSize > 0 && _this->lPort != -1)
        {
            if (!PlayM4_InputData(_this->lPort, pBuffer, dwBufSize))
            {
                break;
            }
        }
        break;
    default:
        //其他数据
        if (dwBufSize > 0 && _this->lPort != -1)
        {
            if (!PlayM4_InputData(_this->lPort, pBuffer, dwBufSize))
            {
                break;
            }
        }
        break;
    }
}

void CALLBACK tianboir_sdk_control::DecCBFun(int nPort, char *pBuf, int nSize, FRAME_INFO *pFrameInfo, int nReserved1, int nReserved2)
{
    if (pFrameInfo->nType == T_AUDIO16)
    {
    }
    if (pFrameInfo->nType == T_AUDIO8)
    {
    }
    if (pFrameInfo->nType == T_UYVY)
    {
    }
    if (pFrameInfo->nType == T_YV12)
    {
    }
    if (pFrameInfo->nType == T_RGB32)
    {
    }
}

void tianboir_sdk_control::hik_image(void *pData)
{
    string filename = "rtsp://admin:abcd1234@192.168.1.66:554/h264/ch1/main/av_stream";
    cv::Mat src_frame;
    cv::VideoCapture cap;
    cap.open(filename);
    while (cap.isOpened())
    {
        cap.read(src_frame);
        // check if we succeeded
        if (src_frame.empty())
        {
            cerr << "hik ERROR! blank src_frame grabbed\n";
            continue;
        }
        //灰度转换
        cv::Mat dst_frame;
        cv::cvtColor(src_frame, dst_frame, cv::COLOR_RGB2GRAY);
        // check if we succeeded
        if (dst_frame.empty())
        {
            cerr << "hik ERROR! blank dst_frame grabbed\n";
            continue;
        }
        //
        if (bSaveH)
        {
            bSaveH = false;
            string date = GetCurrentDate();
            std::vector<unsigned char> jpgBuff;
            std::vector<int> param = std::vector<int>(2);
            // param[0] = CV_IMWRITE_JPEG_QUALITY;
            param[0] = 1;
            param[1] = 100;
            cv::imencode(".jpg", src_frame, jpgBuff, param);
            int size = jpgBuff.size();
            if (size > 0)
            {
                unsigned char buff[size];
                memset(buff, 0x00, size);
                for (int i = 0; i < size; i++)
                {
                    buff[i] = jpgBuff[i];
                }
                std::string file = date + "-hik_src.jpg";
                int fd = open(file.c_str(), O_RDWR | O_CREAT | O_APPEND, 0666);
                if (fd == -1)
                {
                    //
                }
                else
                {
                    write(fd, buff, size);
                    close(fd);
                }
            }
            jpgBuff.clear();
            cv::imencode(".jpg", dst_frame, jpgBuff, param);
            size = jpgBuff.size();
            if (size > 0)
            {
                unsigned char buff[size];
                memset(buff, 0x00, size);
                for (int i = 0; i < size; i++)
                {
                    buff[i] = jpgBuff[i];
                }
                std::string file = date + "-hik_dst.jpg";
                int fd = open(file.c_str(), O_RDWR | O_CREAT | O_APPEND, 0666);
                if (fd == -1)
                {
                    //
                }
                else
                {
                    write(fd, buff, size);
                    close(fd);
                }
            }
        }
        // show live and wait for a key with timeout long enough to show images
        cv::imshow("hik-src", src_frame);
        cv::imshow("hik-dst", dst_frame);
        cv::waitKey(1);
    }
}

// hik_serial
bool tianboir_sdk_control::SerialStart()
{
    // //设置 232 为透明通道模式(使用 232 透明通道时调用,485 不需要)
    // DWORD dwReturned = 0;
    // NET_DVR_RS232CFG_V30 struRS232Cfg;
    // memset(&struRS232Cfg, 0, sizeof(NET_DVR_RS232CFG_V30));
    // if (!NET_DVR_GetDVRConfig(0, NET_DVR_GET_RS232CFG_V30, 0, &struRS232Cfg, sizeof(NET_DVR_RS232CFG_V30), &dwReturned))
    // {
    //     printf("NET_DVR_GET_RS232CFG_V30 error, %d\n", NET_DVR_GetLastError());
    //     return false;
    // }
    // // struRS232Cfg.struRs232.dwWorkMode = 2;
    // for (size_t i = 0; i < MAX_SERIAL_PORT; i++)
    // {
    //     struRS232Cfg.struRs232[i].dwWorkMode = 2;
    // }
    // //设置 232 为透明通道模式;0:窄带传输,1:控制台,2:透明通道
    // if (!NET_DVR_SetDVRConfig(0, NET_DVR_SET_RS232CFG_V30, 0, &(struRS232Cfg), sizeof(NET_DVR_RS232CFG)))
    // {
    //     printf("NET_DVR_SET_RS232CFG_V30 error, %d\n", NET_DVR_GetLastError());
    //     return false;
    // }

    // 建立透明通道
    iSelSerialIndex = 2; // 1:232 串口;2:485 串口
    h_lTranHandle = NET_DVR_SerialStart(lUserID, iSelSerialIndex, tianboir_sdk_control::g_fSerialDataCallBack, lUserID);
    if (h_lTranHandle < 0)
    {
        printf("NET_DVR_SerialStart error, %d\n", NET_DVR_GetLastError());
        return false;
    }

    return true;
}

bool tianboir_sdk_control::SerialStop()
{
    usleep(1000 * 500);
    // 断开透明通道
    if (h_lTranHandle >= 0)
    {
        if (!NET_DVR_SerialStop(h_lTranHandle))
        {
            printf("NET_DVR_SerialStop error, %d\n", NET_DVR_GetLastError());
            return false;
        }
        h_lTranHandle = -1;
    }

    return true;
}

bool tianboir_sdk_control::SerialSend(char *_buf, int _size)
{
    bool bRet = true;

    seria_mtx.lock();
    try
    {
        if (h_lTranHandle < 0)
        {
            bRet = false;
        }
        else
        {
            // 通过透明通道发送数据
            lSerialChan = 1; // 使用 485 时该值有效,从 1 开始;232 时设置为 0
            if (iSelSerialIndex == 1)
            {
                lSerialChan = 0;
            }
            if (!NET_DVR_SerialSend(h_lTranHandle, lSerialChan, _buf, _size)) // buf 为发送数据的缓冲区
            {
                bRet = false;
                printf("NET_DVR_SerialSend error, %d\n", NET_DVR_GetLastError());
            }
            //
            // if (!NET_DVR_SendToSerialPort(lUserID, iSelSerialIndex, lSerialChan, _buf, _size))
            // {
            //     bRet = false;
            //     printf("NET_DVR_SendToSerialPort error, %d\n", NET_DVR_GetLastError());
            // }
            usleep(1000 * 8);
        }
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << '\n';
    }
    seria_mtx.unlock();

    return bRet;
}

bool tianboir_sdk_control::CmdBuf(char _type, int _value, char *_buf, int &_size)
{
    _buf[0] = 0xFF;
    _buf[1] = 0x01;
    _buf[2] = 0x00;
    _buf[3] = _type;
    _buf[4] = _value / 256;
    _buf[5] = _value % 256;
    _buf[6] = (_buf[1] + _buf[2] + _buf[3] + _buf[4] + _buf[5]) % 256;

    _size = 7;

    return true;
}

unsigned int position_x = 0, position_y = 0, position_z = 0, position_zoom = 0;
// 回调透传数据函数的外部实现
void CALLBACK tianboir_sdk_control::g_fSerialDataCallBack(LONG lSerialHandle, char *pRecvDataBuffer, DWORD dwBufSize, DWORD dwUser)
{
    // ...... 处理接收到的透传数据,pRecvDataBuffer 中存放接收到的数据

    // printf("recv data:");
    // for (int i = 0; i < (int)dwBufSize; i++)
    // {
    //     printf("0x%x ", pRecvDataBuffer[i]);
    // }
    // printf("\r\n");

    unsigned char *data = (unsigned char *)pRecvDataBuffer;
    // 水平
    if (data[3] == 0x59)
    {
        tianboir_sdk_control::p_this->position.x = position_x = ((data[4] << 8) + data[5]);
    }
    //
    {
        tianboir_sdk_control::p_this->position.y = position_y = 0;
    }
    // 垂直
    if (data[3] == 0x5b)
    {
        tianboir_sdk_control::p_this->position.z = position_z = ((data[4] << 8) + data[5]);
    }
    // 变倍
    if (data[3] == 0x5d)
    {
        tianboir_sdk_control::p_this->position.zoom = position_zoom = ((data[4] << 8) + data[5]);
    }

    // pub云台角度
    tianboir_sdk_control::p_this->pub_position(position_x, position_y, position_z);
    tianboir_sdk_control::p_this->pub_cloudplatform(position_x, position_y, position_z);
}

void tianboir_sdk_control::PTZ_Position(void *pData)
{
    ros::Rate loop_rate(7);
    while (ros::ok())
    {
        PTZ_Query();

        if (PTZ_InPlace())
        {
            pub_inPlace(1);
        }

        // ros::spin();
        ros::spinOnce();
        loop_rate.sleep();
    }

    // double hz = 20.0;
    // double max_query_hz = 7.0;
    // int query_num = ceil(hz / max_query_hz);
    // int loop_count = 0;

    // ros::Rate loop_rate(hz);
    // while (ros::ok())
    // {
    //     loop_count++;
    //     if (loop_count >= query_num)
    //     {
    //         loop_count = 0;
    //         //
    //         PTZ_Query();
    //     }
    //     if (PTZ_InPlace())
    //     {
    //         pub_inPlace(1);
    //     }

    //     // ros::spin();
    //     ros::spinOnce();
    //     loop_rate.sleep();
    // }
}

bool tianboir_sdk_control::PTZ_Query()
{
    // 获取水平角度命令
    unsigned char panBuf[7] = {0xFF, 0x01, 0x00, 0x51, 0x00, 0x00, 0x52};
    bool bP = SerialSend((char *)panBuf, 7);

    // 获取垂直角度命令
    unsigned char tiltBuf[7] = {0xFF, 0x01, 0x00, 0x53, 0x00, 0x00, 0x54};
    bool bT = SerialSend((char *)tiltBuf, 7);

    // // 获取变倍命令
    // unsigned char zoomBuf[7] = {0xFF, 0x01, 0x00, 0x55, 0x00, 0x00, 0x56};
    // bool bZ = SerialSend((char *)zoomBuf, 7);

    if (bP && bT)
    {
        return true;
    }

    return false;
}

bool tianboir_sdk_control::PTZ_InPlace()
{
    bool b_inPlace_flag = false;

    int x_diff_val = 0;
    int y_diff_val = 0;
    int z_diff_val = 0;

    if (control_goal.action == 1)
    {
        if (control_goal.type == 0)
        {
            /* code */
        }
        if (control_goal.type == 1)
        {
            /* code */
        }
        if (control_goal.type == 3 || control_goal.type == 4)
        {
            /* code */
        }

        if (control_goal.type == 0 || control_goal.type == 1 || control_goal.type == 3 || control_goal.type == 4)
        {
            x_diff_val = control_goal.x - position.x;
            if ((x_diff_val > 35900) || (x_diff_val < -35900))
                x_diff_val = 0;
            //
            y_diff_val = 0;
            //
            z_diff_val = control_goal.z - position.z;
            if ((z_diff_val > 35900) || (z_diff_val < -35900))
                z_diff_val = 0;
            //
            if (((x_diff_val < 100) && (x_diff_val > -100)) && ((z_diff_val < 100) && (z_diff_val > -100)))
            {
                b_inPlace_flag = true;
                //
                control_goal.id = -1;
                control_goal.action = -1;
                control_goal.type = -1;
                control_goal.x = 0;
                control_goal.y = 0;
                control_goal.z = 0;
                control_goal.zoom = 0;
            }
        }
    }

    return b_inPlace_flag;
}

// tianboir
bool tianboir_sdk_control::init_tianboir_connect(string _device_ip)
{
    b_active_disconnect = false;

    // if (!p_nd_init(mh_handle))
    // {
    //     printf("init failed \r\n");
    //     return false;
    // }

    if (!p_nd_connect(mh_handle, _device_ip))
    {
        printf("p_nd_connect failed \r\n");
        return false;
    }

    if (!p_nd_set_cb_on_disconnected(mh_handle, tianboir_sdk_control::on_disconnected, this))
    {
        printf("p_nd_set_cb_on_disconnected failed \r\n");
        return false;
    }

    string sys_version;
    if (!p_nd_get_machine_sys_version(mh_handle, sys_version))
    {
        printf("get_machine_sys_version failed \r\n");
    }
    else
    {
        printf("get_machine_sys_version %s \r\n", sys_version.c_str());
    }

    string hardware_version;
    if (!p_nd_get_machine_hardware_version(mh_handle, hardware_version))
    {
        printf("get_machine_hardware_version failed \r\n");
    }
    else
    {
        printf("get_machine_hardware_version %s \r\n", hardware_version.c_str());
    }

    string software_version;
    if (!p_nd_get_machine_software_version(mh_handle, software_version))
    {
        printf("get_machine_software_version failed \r\n");
    }
    else
    {
        printf("get_machine_software_version %s \r\n", software_version.c_str());
    }

    string sn;
    if (!p_nd_get_machine_sn(mh_handle, sn))
    {
        printf("get_machine_sn failed \r\n");
    }
    else
    {
        printf("get_machine_sn %s \r\n", sn.c_str());
    }

    unsigned short w_width, w_height;
    if (!p_ir_get_resolution(mh_handle, w_width, w_height))
    {
        printf("get_resolution failed \r\n");
    }
    else
    {
        tianboir_width = w_width;
        tianboir_height = w_height;
        printf("get_resolution w_width:%hu, w_height:%hu \r\n", w_width, w_height);
    }

    return true;
}

bool tianboir_sdk_control::tianboir_disconnect()
{
    b_active_disconnect = true;

    if (!p_nd_disconnect(mh_handle))
    {
        printf("p_nd_disconnect failed \r\n");
        return false;
    }
    printf("p_nd_disconnect \r\n");

    if (!p_nd_uninit(mh_handle))
    {
        printf("p_nd_uninit failed \r\n");
    }
    printf("p_nd_disconnect \r\n");

    return true;
}

bool tianboir_sdk_control::open_tianboir_capture()
{
    if (!p_nd_set_cb_one_frame_received(mh_handle, tianboir_sdk_control::one_frame_received, this))
    {
        printf("set_cb_one_frame_received failed \r\n");
        return false;
    }

    return true;
}

void tianboir_sdk_control::on_disconnected(void *p_user_data)
{
    printf("on_disconnected tianboir ... \r\n");

    tianboir_sdk_control *_this = (tianboir_sdk_control *)p_user_data;

    if (!_this->b_active_disconnect)
    {
        printf("Reconnection tianboir ... \r\n");
        // if (!p_nd_connect(_this->mh_handle, _this->tianboir_device_ip))
        if (!p_nd_connect(mh_handle, _this->tianboir_device_ip))
        {
            printf("p_nd_connect failed \r\n");
        }
    }
}

int received_count = 0;
void tianboir_sdk_control::one_frame_received(void *p_user_data, unsigned short *pw_ad, unsigned short w_width, unsigned short w_height)
{
    received_count++;
    if (received_count < 4)
    {
        return;
    }
    received_count = 0;
    //
    tianboir_sdk_control *_this = (tianboir_sdk_control *)p_user_data;
    //
    // unsigned char *mpuc_rgb32_buffer = nullptr;
    // mpuc_rgb32_buffer = new unsigned char[w_width * w_height * 4];
    // bool b_get_bits = false;
    // if (mpuc_rgb32_buffer && p_nd_capture_one_frame(mh_handle))
    // {
    //     b_get_bits = p_ir_get_bits(mh_handle, mpuc_rgb32_buffer);
    //     if (b_get_bits)
    //     {
    //         cv::Mat g_frame = cv::Mat(w_height, w_width, CV_8UC4, mpuc_rgb32_buffer);
    //         sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), sensor_msgs::image_encodings::TYPE_8UC4, g_frame).toImageMsg();
    //         _this->buffer_pub.publish(msg);
    //     }
    //     //
    //     float *pf_temp = new float[w_width * w_height];
    //     p_ir_get_all_pixel_temperature(mh_handle, pf_temp);

    //     unsigned int bufLen = w_width * w_height * sizeof(float);
    //     unsigned char *tempBuff = new unsigned char[bufLen];
    //     memset(tempBuff, 0x00, bufLen);
    //     memcpy(tempBuff, pf_temp, bufLen);

    //     char *outBuff = NULL;
    //     int outLen = 0;
    //     if (_this->zipBuff((char *)tempBuff, bufLen, &outBuff, outLen))
    //     {
    //         int retLen = outLen + 4;
    //         char *retBuff = new char[retLen];
    //         memset(retBuff, 0x00, retLen);
    //         memcpy(retBuff, &bufLen, 4);
    //         memcpy(retBuff + 4, outBuff, outLen);

    //         _this->pub_temperature(w_width, w_height, (unsigned char *)retBuff, retLen);

    //         delete[] outBuff;
    //         delete retBuff;
    //     }
    //     delete[] pf_temp;
    //     delete tempBuff;
    // }
    // if (mpuc_rgb32_buffer)
    // {
    //     delete[] mpuc_rgb32_buffer;
    //     mpuc_rgb32_buffer = nullptr;
    // }
    //
    // ******************************
    int w_range = w_width * w_height;
    unsigned int pcLen = 0;
    unsigned int adLen = w_range * sizeof(unsigned short);

    unsigned char *adBuff = new unsigned char[adLen];
    memset(adBuff, 0x00, adLen);
    memcpy(adBuff, pw_ad, adLen);

    unsigned char *pcBuf = new unsigned char[w_range * 4];
    // if (!p_ir_get_temperature_snap_buffer(_this->mh_handle, pcBuf, pcLen))
    if (!p_ir_get_temperature_snap_buffer(mh_handle, pcBuf, pcLen))
    {
        delete adBuff;
        delete pcBuf;
        printf("p_ir_get_temperature_snap_buffer failed \r\n");
        return;
    }

    InfraredData ifd;
    ifd.time = ros::Time::now();
    ifd.width = w_width;
    ifd.height = w_height;
    ifd.ad_len = adLen;
    ifd.ad_buf = std::vector<unsigned char>(adBuff, adBuff + adLen);
    ifd.pc_len = pcLen;
    ifd.pc_buf = std::vector<unsigned char>(pcBuf, pcBuf + pcLen);
    _this->ifds.push(ifd);

    while (_this->ifds.size() > 1)
        _this->ifds.pop();

    unsigned int dataLen = 8 + pcLen + adLen;
    unsigned char *dataBuff = new unsigned char[dataLen];
    memset(dataBuff, 0x00, dataLen);
    memcpy(dataBuff, &pcLen, 4);
    memcpy(dataBuff + 4, &adLen, 4);
    memcpy(dataBuff + 8, pcBuf, pcLen);
    memcpy(dataBuff + 8 + pcLen, pw_ad, adLen);
    //
    // _this->pub_temperature(w_width, w_height, dataBuff, dataLen);
    //
    char *outBuff = NULL;
    int outLen = 0;
    if (_this->zipBuff((char *)dataBuff, dataLen, &outBuff, outLen))
    {
        int retLen = outLen + 4;
        char *retBuff = new char[retLen];
        memset(retBuff, 0x00, retLen);
        memcpy(retBuff, &dataLen, 4);
        memcpy(retBuff + 4, outBuff, outLen);

        _this->pub_temperature(w_width, w_height, (unsigned char *)retBuff, retLen);

        delete[] outBuff;
        delete retBuff;
    }
    //
    delete adBuff;
    delete pcBuf;
    delete dataBuff;
}

void tianboir_sdk_control::tianboir_image(void *pData)
{
    ros::Rate loop_rate(20);
    while (ros::ok())
    {
        if (p_nd_capture_one_frame(mh_handle))
        {
            // image
            unsigned char *mpuc_rgb32_buffer = new unsigned char[tianboir_width * tianboir_height * 4];
            bool b_get_bits = p_ir_get_bits(mh_handle, mpuc_rgb32_buffer);
            if (b_get_bits)
            {
                cv::Mat g_frame = cv::Mat(tianboir_width, tianboir_height, CV_8UC4, mpuc_rgb32_buffer);
                sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), sensor_msgs::image_encodings::TYPE_8UC4, g_frame).toImageMsg();
                buffer_pub.publish(msg);
            }
            delete mpuc_rgb32_buffer;
            // ******************************
            // temperature
            int len = tianboir_width * tianboir_height;
            float *pf_temp = new float[tianboir_width * tianboir_height];
            p_ir_get_all_pixel_temperature(mh_handle, pf_temp);
            //
            unsigned int bufLen = tianboir_width * tianboir_height * sizeof(float);
            unsigned char *tempBuff = new unsigned char[bufLen];
            memset(tempBuff, 0x00, bufLen);
            memcpy(tempBuff, pf_temp, bufLen);
            char *outBuff = NULL;
            int outLen = 0;
            if (zipBuff((char *)tempBuff, bufLen, &outBuff, outLen))
            {
                int retLen = outLen + 4;
                char *retBuff = new char[retLen];
                memset(retBuff, 0x00, retLen);
                memcpy(retBuff, &bufLen, 4);
                memcpy(retBuff + 4, outBuff, outLen);

                pub_temperature(tianboir_width, tianboir_height, (unsigned char *)retBuff, retLen);

                delete[] outBuff;
                delete retBuff;
            }
            delete[] pf_temp;
            delete tempBuff;
        }

        // if (!ifds.empty() && ifds.size() > 0)
        // {
        //     InfraredData ifd = ifds.front();

        //     unsigned int dataLen = 8 + ifd.pc_len + ifd.ad_len;
        //     unsigned char *dataBuff = new unsigned char[dataLen];
        //     memset(dataBuff, 0x00, dataLen);
        //     memcpy(dataBuff, &ifd.pc_len, 4);
        //     memcpy(dataBuff + 4, &ifd.ad_len, 4);
        //     memcpy(dataBuff + 8, (unsigned char *)&(ifd.pc_buf), ifd.pc_len);
        //     memcpy(dataBuff + 8 + ifd.pc_len, (unsigned char *)&(ifd.ad_buf), ifd.ad_len);

        //     char *outBuff = NULL;
        //     int outLen = 0;
        //     if (zipBuff((char *)dataBuff, dataLen, &outBuff, outLen))
        //     {
        //         int retLen = outLen + 4;
        //         char *retBuff = new char[retLen];
        //         memset(retBuff, 0x00, retLen);
        //         memcpy(retBuff, &dataLen, 4);
        //         memcpy(retBuff + 4, outBuff, outLen);

        //         pub_temperature(ifd.width, ifd.height, (unsigned char *)retBuff, retLen);

        //         delete[] outBuff;
        //         delete retBuff;
        //     }
        //     delete dataBuff;

        //     ifds.pop();
        // }

        // ros::spin();
        ros::spinOnce();
        loop_rate.sleep();
    }

    // while (true)
    // {
    //     try
    //     {
    //         bool bImage = true;
    //         cv::Mat src_frame = cv::Mat(tianboir_width, tianboir_height, CV_8UC4, rgb_data);
    //         if (src_frame.empty())
    //         {
    //             bImage = false;
    //             cerr << "tianboir ERROR! blank src_frame grabbed\n";
    //         }
    //         //灰度转换
    //         cv::Mat dst_frame;
    //         cv::cvtColor(src_frame, dst_frame, cv::COLOR_RGB2GRAY);
    //         if (dst_frame.empty())
    //         {
    //             bImage = false;
    //             cerr << "tianboir ERROR! blank dst_frame grabbed\n";
    //         }
    //         //
    //         if (bImage)
    //         {
    //             if (bSaveT)
    //             {
    //                 bSaveT = false;
    //                 string date = GetCurrentDate();
    //                 std::string srcFile = date + "-tianboir_src.jpg";
    //                 cv::imwrite(srcFile, src_frame);
    //                 std::string dstFile = date + "-tianboir_dst.jpg";
    //                 cv::imwrite(dstFile, dst_frame);
    //             }
    //             cv::imshow("tianboir-src", src_frame);
    //             cv::imshow("tianboir-dst", dst_frame);
    //             cv::waitKey(1);
    //         }
    //     }
    //     catch (const std::exception &e)
    //     {
    //         std::cerr << e.what() << '\n';
    //     }

    //     usleep(1000 * 10);
    // }
}

bool tianboir_sdk_control::zoom_motor_forward()
{
    if (!p_nd_motor_forward(mh_handle))
    {
        printf("zoom_motor_forward failed \r\n");
        return false;
    }

    return true;
}

bool tianboir_sdk_control::zoom_motor_reverse()
{
    if (!p_nd_motor_reverse(mh_handle))
    {
        printf("zoom_motor_reverse failed \r\n");
        return false;
    }

    return true;
}

bool tianboir_sdk_control::zoom_motor_stop()
{
    if (!p_nd_motor_stop(mh_handle))
    {
        printf("zoom_motor_stop failed \r\n");
        return false;
    }

    return true;
}

bool tianboir_sdk_control::zipBuff(const char *_buff, int _len, char **_outBuff, int &_outLen)
{
    _outLen = 0;
    *_outBuff = NULL;
    if (_buff == NULL || _len == 0)
    {
        return false;
    }
    unsigned int maxOutLen = LZ4_compressBound(_len);
    *_outBuff = new char[maxOutLen];
    unsigned int zipBufSize = LZ4_compress_fast(_buff, *_outBuff, _len, maxOutLen, 3);
    if (zipBufSize <= 0)
    {
        delete[](*_outBuff);
        *_outBuff = NULL;
        return false;
    }
    _outLen = zipBufSize;

    return true;
}

bool tianboir_sdk_control::unZipBuff(const char *_buff, int _len, char *_outBuff, int _outLen)
{
    // int decompressSize = LZ4_decompress_safe(_buff, _outBuff, _len, _outLen);
    // //int decompressSize = LZ4_decompress_fast( _buff, _len, _outBuff, _outLen);
    // return decompressSize == _outLen;

    return true;
}

void tianboir_sdk_control::update()
{
    pub_heartbeat(level, message);
}

//
void key_capture(void *pData)
{
    bool done = false;
    while (!done)
    {
        int in;
        struct termios new_settings;
        struct termios stored_settings;
        tcgetattr(0, &stored_settings);
        new_settings = stored_settings;
        new_settings.c_lflag &= (~ICANON);
        new_settings.c_cc[VTIME] = 0;
        tcgetattr(0, &stored_settings);
        new_settings.c_cc[VMIN] = 1;
        tcsetattr(0, TCSANOW, &new_settings);
        in = getchar();
        tcsetattr(0, TCSANOW, &stored_settings);

        // printf("%d\r\n", in);

        if (in == 104) // h
        {
            bSaveH = true;
        }
        if (in == 116) // t
        {
            bSaveT = true;
        }
        //
        if (in == 119) // w
        {
            tianboir_sdk_control::p_this->zoom_motor_forward();
        }
        if (in == 120) // x
        {
            tianboir_sdk_control::p_this->zoom_motor_reverse();
        }
        if (in == 97) // a
        {
        }
        if (in == 100) // d
        {
        }
        if (in == 115) // s
        {
            tianboir_sdk_control::p_this->zoom_motor_stop();
        }
    }
}

void cmd_capture(void *pData)
{
    bool done = false;
    string input;
    while (!done)
    {
        std::cout << "Enter Command:\r\n";
        std::getline(std::cin, input);

        if (input == "help")
        {
            std::cout << "\nCommand List:\n"
                      << "help: Display this help text\n"
                      << "quit: Exit the program\n"
                      << std::endl;
        }
        else if (input == "quit")
        {
            tianboir_sdk_control::p_this->Destroy();
            done = true;
            exit(0);
        }
        else
        {
            std::cout << "> Unrecognized Command" << std::endl;
        }
    }
}

void sig_handler(int sig)
{
    if (sig == SIGINT)
    {
        printf("Terminated by Ctrl+C signal. \r\n");
        tianboir_sdk_control::p_this->Destroy();
        // exit(0);
    }
}

int main(int argc, char **argv)
{
    signal(SIGINT, sig_handler);
    // key
    std::thread key_thread(key_capture, nullptr);
    key_thread.detach();
    // cmd
    // std::thread cmd_thread(cmd_capture, nullptr);
    // cmd_thread.detach();

    ros::init(argc, argv, __app_name__);

    if (!p_nd_init(mh_handle))
    {
        printf("init failed \r\n");
        return false;
    }
    //
    tianboir_sdk_control tianboir;

    ros::Rate rate(10);
    while (ros::ok())
    {
        tianboir.update();

        ros::spinOnce();
        rate.sleep();
    }

    return 0;
}
