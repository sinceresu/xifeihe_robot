# yd_cloudplatform

云台控制节点，接收任务调度节点的控制请求，计算不同任务下需要云台旋转的角度，根据请求控制云

台转动，并实时发布一体化云台中相机的网络通信状态

## Depends
- arm  
pip install requests  

- x86_64  
hksdk

## Compile
[1] CMakeLists.txt 根据自己平台设置 ARM_BUILD  
[2] cd ~/workspace && catkin_make

## Start Run

roslaunch yd_cloudplatform cloudplatform_correlation.launch

**每个节点的参数配置见下面相关节点说明**



# yd_cloudplatform(cloudplat_control)

云台控制节点，接收任务调度节点的控制请求，根据请求控制云台转动

### Subscribed Topics

None

### Published Topics

1、/yida/hearbeat (diagnostic_msgs::DiagnosticArray)  1hz      

​      发布给监控节点

2、/yida/internal/platform_status (diagnostic_msgs::DiagnosticArray)  1hz      

​      发布给设备状态管理节点

3、/yida/platform_isreach (std_msgs::Int32)     

​      发布给任务调度节点或红外、可见光检测计算云台转动角度节点

### Server Topic

1、/yida/internal/platform_cmd(yd_cloudplatform::CloudPlatControl)    

​      提供服务给任务调度节点或红外、可见光检测计算云台转动角度节点

### Client Topic

None

### Params

**platform1_str: **云台类型1或云台序号1的端口名称

**platform2_str: **云台类型2或云台序号2的端口名称

**platform3_str: **云台类型3或云台序号3的端口名称

**device_id:**设备id

**baud_rate:**串口波特率

**heartbeat_topic_string:**心跳topic名称

###  Msg and Srv

```shell
yd_cloudplatform/CloudPlatControl.srv

int32 id         #云台id 0\1\2
int32 action     #获取0 设置1
int32 type       #水平0 垂直1 变倍2 预置位3
int32 value      #设置值
---
int32 result     #返回结果
```





# yd_cloudplatform(tf发布节点)

相机与激光的tf关系





# yd_cloudplatform(visible_cloudplat)

可见光检测计算云台转动角度节点，接收任务调度节点的任务信息，将图片发送给表计检测模块

### Subscribed Topics

1、/robot_pose (nav_msgs::Odometry) 

​      接收于定位节点

2、/transfer_pub (yidamsg::transfer) 

​      接收于任务调度节点

3、/detect_rect (yidamsg::Detect_Result) 

​      接收于表计检测节点

4、/get_setvalue (std_msgs::String)

​      接收于之前上色节点

5、/yida/internal/visible/image_proc/compressed (sensor_msgs::CompressedImage) 

​      接收于相机图像流采集节点

### Published Topics

1、/yida/hearbeat (diagnostic_msgs::DiagnosticArray)  1hz

​      发布给监控节点

2、/xy_position (std_msgs::Int32) 

3、/z_position (std_msgs::Int32) 

4、/zoom_position (std_msgs::Int32)

5、/stop_receive_lase (std_msgs::Bool) 

​      发布给定位节点

6、/oukeimage_zoom (yidamsg::InspectedResult)

​      发布给表计检测节点

### Server Topic

None

### Client Topic

1、/yida/internal/platform_cmd(yd_cloudplatform::CloudPlatControl)

​      请求云台控制节点服务

### Params

**handleset_xy: **云台水平方向微调

**handleset_z: **云台垂直方向微调

**camera_id: **云台类型或云台序号

**camera_ip: **相机ip地址

**camera_username: **相机用户名

**camera_password: **相机i密码

**camera_image_width: **图像宽度

**camera_image_height: **图像高度

**camera_cmos_width: **图像传感器宽度

**camera_cmos_height: **图像传感器高度

**camera_pixel_size: **像素物理大小

**camera_focus_number: **相机可变倍倍数

**camera_focus_mm: **各变倍倍数物理焦距

**camera_focus_value: **各变倍倍数设置绝对值

**xyz_degree:**云台角度绝对值范围

**offset_xy:**云台校正值

###  Msg and Srv

```shell
yidamsg/transfer
int32 flag      #类型，目前为保留字段
string data     #任务信息

yidamsg/Detect_Result
int32 xmin      #框的左上角水平方向像素坐标
int32 ymin      #框的左上角垂直方向像素坐标
int32 xmax      #框的右下角水平方向像素坐标
int32 ymax      #框的右下角垂直方向像素坐标
string name     #名称

yidamsg/InspectedResult
int32 camid
int32 picid
float32 x
float32 y
float32 z
uint8[] equipimage
uint8[] nameplates
string equipid
string result
bool success

yd_cloudplatform/CloudPlatControl.srv
int32 id        #云台id 0\1\2
int32 action    #获取0 设置1
int32 type      #水平0 垂直1 变倍2 预置位3
int32 value     #设置值
---
int32 result    #返回结果
```





# yd_cloudplatform(infrared_cloudplat)

红外检测计算云台转动角度节点，接收任务调度节点的任务信息，将图片发送给上位机

### Subscribed Topics

1、/robot_pose (nav_msgs::Odometry) 

​      接收于定位节点

2、/transfer_pub (yidamsg::transfer) 

​      接收于任务调度节点

3、/yida/internal/infrared/image_proc/compressed (sensor_msgs::CompressedImage) 

​       接收于相机图像流采集节点

4、/yida/internal/visible/image_proc/compressed (sensor_msgs::CompressedImage) 

​       接收于相机图像流采集节点

### Published Topics

1、/yida/hearbeat (diagnostic_msgs::DiagnosticArray)  1hz

​      发布给监控节点

2、/meter_flag (std_msgs::String) 

​      发布给任务调度节点

3、/detect_result (yidamsg::InspectedResult) 

​      发布给表计检测节点

4、/xy_position (std_msgs::Int32) 

5、/z_position (std_msgs::Int32) 

6、/stop_receive_lase (std_msgs::Bool) 

​      发布给定位节点

### Server Topic

None

### Client Topic

1、/yida/internal/platform_cmd (yd_cloudplatform::CloudPlatControl)

​      请求云台控制节点服务

2、/thermal/get_temperature (yd_cloudplatform::GetTemperature)

​      请求获取温度

### Params

**handleset_xy: **云台水平方向微调

**handleset_z: **云台垂直方向微调

**camera_id: **云台类型或云台序号

**xyz_degree:**云台角度绝对值范围

**offset_xy:**云台校正值

###  Msg and Srv

```shell
yidamsg/transfer
int32 flag      #类型，目前为保留字段
string data     #任务信息

yidamsg/InspectedResult
int32 camid
int32 picid
float32 x
float32 y
float32 z
uint8[] equipimage
uint8[] nameplates
string equipid
string result
bool success

yd_cloudplatform/CloudPlatControl.srv
int32 id         #云台id 0\1\2
int32 action     #获取0 设置1
int32 type       #水平0 垂直1 变倍2 预置位3
int32 value      #设置值
---
int32 result     #返回结果

yd_cloudplatform/GetTemperature.srv
uint16[] spot_x  #单点，单点的x像素坐标；若是框，为左上角和右下角的像素点的x像素坐标
uint16[] spot_y  #单点，单点的y像素坐标；若是框，为左上角和右下角的像素点的y像素坐标
---
float32 temperature_c #结果
```





# yd_cloudplatform(hostcp_cloudplat)

接收上位机控制topic，控制云台

### Subscribed Topics

1、/ydserver/manual_control(yidamsg::manualControlParameters) 

​      接收于上位机

### Published Topics

1、/yida/hearbeat (diagnostic_msgs::DiagnosticArray)  1hz

​      发布给监控节点

### Server Topic

None

### Client Topic

1、/yida/internal/platform_cmd (yd_cloudplatform::CloudPlatControl)

​      请求云台控制节点服务

### Params

**step_1: **步进步长等级1

**step_2: **步进步长等级2

**step_3: **步进步长等级3

**cloudplatform_id: **云台类型或云台序号

**xyz_degree:**云台角度绝对值范围

**camera_focus_value:**相机变倍值

###  Msg and Srv

```shell
manualControlParameters.msg

uint8 control_type
int32 cloudplatform_id
int32 action
string direction
int32 step
int32 horizontal
int32 vertical
int32 focus
float32 linear_speed
float32 angular_speed
```





# yd_cloudplatform(camera_status_node)

可见光和红外相机设备状态发布节点

### Subscribed Topics

None

### Published Topics

1、/yida/internal/camera_status  (diagnostic_msgs::DiagnosticArray)   

​      发布给状态管理节点

2、/yida/hearbeat (diagnostic_msgs::DiagnosticArray)  

​      发布给监控节点

### Params

**camera_status_topic: **相机设备状态topic名称

**heartbeat_topic_string: **节点心跳topic名称

**visible_ip: **可见光相机ip

**infrared_ip: **红外相机ip地址

通过ping可见光/红外相机的ip的状态，来获取相机设备当前的状态。

### 说明

关于camera_status节点发送给状态管理节点的说明（**摘自状态管理节点的约定文档**）

三种状态：

[1]不可ping

[2]正常

[3]延迟高

\-------------------------------------------------------------------------------------------------------

消息：

topic: /yida/internal/camera_status

message: diagnostic_msgs/DiagnosticArray.msg

说明：

[1]Header部分

​     stamp: 当前时间戳

​     frame_id: visual/thermal 分别对应“可见光相机/红外相机”

[2]DiagnosticStatus部分

​      level: ERROR/OK/WARN 分别对应“三种状态”

​     name: 可见光相机ip/红外相机ip

​     message: 不可ping/正常/延迟 三种状态描述

​     hardware_id: 可填相机设备id，无则不填

​     values: 可填当前相机的焦距、光圈大小等相关设备参数，无则不填