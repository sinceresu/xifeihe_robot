#ifndef __CLOUDPLATFORM_SDK_CONTROL_TEST__
#define __CLOUDPLATFORM_SDK_CONTROL_TEST__

#include <iostream>
#include <stdio.h>
#include <mutex>
#include <csignal>
#include <thread>
#include <vector>
#include <queue>

#include <opencv2/opencv.hpp>

#include "haikang_sdk/HCNetSDK.h"
#include "haikang_sdk/LinuxPlayM4.h"
#include "haikang_sdk/convert.h"

#include "tianboir_sdk/p-ir.h"
#include "tianboir_sdk/p-net.h"
#include "tianboir_sdk/p-net-device.h"
#include "tianboir_sdk/p-image.h"

#include "ros/ros.h"
#include <std_msgs/Int32.h>
#include <nav_msgs/Odometry.h>
#include <diagnostic_msgs/DiagnosticArray.h>
#include <sensor_msgs/Image.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include "yd_cloudplatform/CloudPlatControl.h"
#include "yd_cloudplatform/GetTemperature.h"
#include "yidamsg/Detect_Result.h"
#include "param_server/server.h"
// test
#include "yd_cloudplatform/common.hpp"
#include "yidamsg/ControlMode.h"
#include <tf/transform_listener.h>
#include <std_msgs/String.h>
#include <std_msgs/Bool.h>
#include <sensor_msgs/CompressedImage.h>
#include <yidamsg/transfer.h>
#include <yidamsg/InspectedResult.h>

std::string GetCurrentDate(const std::string &_format = "%Y-%m-%d %H:%M:%S");

#define __app_name__ "tianboir_sdk_control"

// 获取水平角度命令
unsigned char gPanBuf[7] = {0xFF, 0x01, 0x00, 0x51, 0x00, 0x00, 0x52};
// 获取垂直角度命令
unsigned char gTiltBuf[7] = {0xFF, 0x01, 0x00, 0x53, 0x00, 0x00, 0x54};
// 获取变倍命令
unsigned char gZoomBuf[7] = {0xFF, 0x01, 0x00, 0x55, 0x00, 0x00, 0x56};
// 设置水平角度命令
unsigned char sPanBuf[7] = {0xFF, 0x01, 0x00, 0x4B, 0 / 256, 0 % 256, 0x52};
// 设置垂直角度命令
unsigned char sTiltBuf[7] = {0xFF, 0x01, 0x00, 0x4D, 0 / 256, 0 % 256, 0x54};
// 设置变倍命令
unsigned char sZoomBuf[7] = {0xFF, 0x01, 0x00, 0x4F, 0 / 256, 0 % 256, 0x56};
// 云台上转
unsigned char upBuf[7] = {0xFF, 0x01, 0x00, 0x08, 0x2F, 0x00, 0x38};
// 云台下转
unsigned char downBuf[7] = {0xFF, 0x01, 0x00, 0x10, 0x2F, 0x00, 0x40};
// 云台左转
unsigned char leftBuf[7] = {0xFF, 0x01, 0x00, 0x04, 0x2F, 0x00, 0x34};
// 云台右转
unsigned char rightBuf[7] = {0xFF, 0x01, 0x00, 0x02, 0x2F, 0x00, 0x32};
// 停止转动
unsigned char stopBuf[7] = {0xFF, 0x01, 0x00, 0x00, 0x00, 0x00, 0x01};
// 远程复位 无效
unsigned char resetBuf[7] = {0xFF, 0x01, 0x00, 0x0F, 0x00, 0x00, 0x10};
// 回到零位 有效
unsigned char zeroBuf[7] = {0xFF, 0x01, 0x00, 0x07, 0x00, 0x22, 0x2A};

typedef struct ControlGoal
{
    int id;
    int action;
    int type;
    unsigned int x;
    unsigned int y;
    unsigned int z;
    unsigned int zoom;
    ControlGoal() : id(-1), action(-1), type(-1), x(0), y(0), z(0), zoom(0) {}
} SControlGoal, *PControlGoal;

typedef struct ControlPosition
{
    ros::Time time;
    unsigned int x;
    unsigned int y;
    unsigned int z;
    unsigned int zoom;
    ControlPosition() : x(0), y(0), z(0), zoom(0) {}
} SControlPosition, *PControlPosition;

typedef struct InfraredData
{
    ros::Time time;
    unsigned int width;
    unsigned int height;
    unsigned int rgb_len;
    // unsigned char *rgb_buf;
    std::vector<unsigned char> rgb_buf;
    unsigned int ad_len;
    // unsigned char *ad_buf;
    std::vector<unsigned char> ad_buf;
    unsigned int pc_len;
    // unsigned char *pc_buf;
    std::vector<unsigned char> pc_buf;

} SInfraredData, *PInfraredData;

THANDLE mh_handle;

class tianboir_sdk_control
{
public:
    tianboir_sdk_control();
    ~tianboir_sdk_control();

    static tianboir_sdk_control *p_this;

    bool Destroy();

private:
    param_server::Server *ros_param_server;
    ros::NodeHandle nh, private_nh;
    ros::Publisher heartbeat_pub;
    ros::Publisher buffer_pub;
    ros::Publisher position_pub;
    ros::Publisher cloudplatform_pub;
    ros::Publisher inPlace_pub;
    ros::Publisher temperature_pub;

    ros::Subscriber detect_rect_sub;
    ros::ServiceServer cloudplatform_server;
    ros::ServiceServer temperature_server;

    std::string heartbeat_topic;
    int level;
    std::string message;

    std::string image_save_dir;
    double ptz_time_difference;
    double temp_time_difference;

    void param_Callback(param_server::SimpleType &_config);
    void read_param();

    void pub_heartbeat(int _level, std::string _message);
    void pub_buffer(std::vector<unsigned char> _buff);
    void pub_position(unsigned int _x, unsigned int _y, unsigned int _z);
    void pub_cloudplatform(unsigned int _x, unsigned int _y, unsigned int _z);
    void pub_inPlace(int _data);
    void pub_temperature(int _width, int _height, unsigned char *_buf, int _size);
    void detect_rect_Callback(const yidamsg::Detect_Result::ConstPtr &msg);
    bool cloudplatform_server_Callback(yd_cloudplatform::CloudPlatControl::Request &req,
                                       yd_cloudplatform::CloudPlatControl::Response &res);
    bool temperature_server_Callback(yd_cloudplatform::GetTemperature::Request &req,
                                     yd_cloudplatform::GetTemperature::Response &res);

private:
    std::string hik_device_ip;
    std::string hik_device_port;
    std::string hik_username;
    std::string hik_password;
    std::string hik_width;
    std::string hik_height;
    LONG lUserID;
    LONG lRealPlayHandle;
    LONG lPort;
    LONG h_lTranHandle;
    int iSelSerialIndex;
    LONG lSerialChan;
    std::mutex seria_mtx;
    ControlGoal control_goal;
    ControlPosition position;

public:
    bool hik_show;
    bool hik_save;
    bool init_hik_connect(std::string _ip, std::string _port, std::string _username, std::string _password);
    bool hik_disconnect();

    bool open_hik_capture();
    bool close_hik_capture();
    static void CALLBACK g_RealDataCallBack_V30(LONG lRealHandle, DWORD dwDataType, BYTE *pBuffer, DWORD dwBufSize, void *dwUser);
    static void CALLBACK DecCBFun(int nPort, char *pBuf, int nSize, FRAME_INFO *pFrameInfo, int nReserved1, int nReserved2);
    bool YV12ToBGR24_Table(unsigned char *pYUV, unsigned char *pBGR24, int width, int height);
    void hik_image(void *pData);

    bool SerialStart();
    bool SerialStop();
    bool SerialSend(char *_buf, int _size);
    bool CmdBuf(char _type, int _value, char *_buf, int &_size);
    static void CALLBACK g_fSerialDataCallBack(LONG lSerialHandle, char *pRecvDataBuffer, DWORD dwBufSize, DWORD dwUser);

    void PTZ_Position(void *pData);
    bool PTZ_Query();
    bool PTZ_InPlace();

private:
    std::string tianboir_device_ip;
    // THANDLE mh_handle;
    unsigned short tianboir_width;
    unsigned short tianboir_height;
    int tianboir_freq;
    std::mutex temperature_mtx;
    bool b_active_disconnect;
    std::queue<InfraredData> ifds;

public:
    bool tianboir_show;
    bool tianboir_save;
    bool zipBuff(const char *_buff, int _len, char **_outBuff, int &_outLen);
    bool unZipBuff(const char *_buff, int _len, char *_outBuff, int _outLen);
    //
    bool init_tianboir_connect(string _device_ip);
    bool tianboir_disconnect();
    bool open_tianboir_capture();
    static void on_disconnected(void *p_user_data);
    static void one_frame_received(void *p_user_data, unsigned short *pw_ad, unsigned short w_width, unsigned short w_height);
    void tianboir_image(void *pData);

    bool zoom_motor_forward();
    bool zoom_motor_reverse();
    bool zoom_motor_stop();

public:
    void update();

    // test
private:
    ros::Publisher control_mode_pub;
    void send_control_mode(int _robot_id, int _mode);
    ros::Subscriber control_mode_sub;
    void control_mode_Callback(const yidamsg::ControlMode::ConstPtr &msg);
    //
    ros::Publisher transfer_pub_;
    bool send_transfer_flag(int _robot_id, std::string _data);
    ros::Subscriber transfer_sub_;
    void transfer_callback(const std_msgs::String &msg);
    //
    void run_detection(std::string data, void *ud);

    int robot_id;
    int device_mode;
    std::string image_dir;
    int is_inPlace;

public:
    bool save_image(std::string _dir, std::string _file_name);
};

#endif
