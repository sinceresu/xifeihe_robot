﻿#ifndef _P_IR_HEADER_
#define _P_IR_HEADER_

#include "common-def.h"
#include <string>
#include <vector>

#if defined(_WIN32) || defined(_WIN64)

#ifdef PIR_EXPORTS
#define P_IR_API __declspec(dllexport) bool __stdcall
#else
#define P_IR_API __declspec(dllimport) bool __stdcall
#endif

#else
#define P_IR_API bool
#endif

#ifdef __cplusplus
extern "C"
{
#endif

P_IR_API p_ir_append_temperature_to_file(THANDLE h, FILE *p_file, unsigned int dw_sound_length, unsigned char *pby_sound_data,
    unsigned int dw_user_data_length, void *p_user_data);
P_IR_API p_ir_append_temperature_to_national_grid_file(THANDLE h, FILE *p_file,
                                           unsigned char *pc_label_data, unsigned int ui_label_bytes);
P_IR_API p_ir_get_temperature_snap_buffer(THANDLE h, unsigned char *pc_buf, unsigned int &ui_buf_length);

P_IR_API p_ir_start_record(THANDLE h, FILE* p_file, THANDLE &h_record);
P_IR_API p_ir_record_one_frame(THANDLE h, THANDLE h_record, unsigned short w_mark_size, void *p_mark_info);
P_IR_API p_ir_stop_record(THANDLE h, THANDLE &h_record, unsigned int dw_user_data_bytes, void *p_user_data);

P_IR_API p_ir_get_productor(THANDLE h, std::string &str_productor);
P_IR_API p_ir_get_mac_type(THANDLE h, std::string &str_mac_type);
P_IR_API p_ir_get_mac_sn(THANDLE h, std::string &str_mac_sn);
P_IR_API p_ir_get_detector_sn(THANDLE h, std::string &str_detector_sn);
P_IR_API p_ir_get_product_date_time(THANDLE h, std::string &str_product_date_time);

P_IR_API p_ir_get_supported_lens(THANDLE h, std::vector<LenType> &vct_lens);
P_IR_API p_ir_get_len_sn(THANDLE h, LenType len_type, std::string &str);
P_IR_API p_ir_get_len_des(THANDLE h, LenType len_type, std::string &str);
P_IR_API p_ir_support_len_type(THANDLE h, LenType len_type, bool &b_support);
P_IR_API p_ir_support_meas_range(THANDLE h, LenType len_type, MeasRange meas_range, bool &b_support);
P_IR_API p_ir_get_len_type_meas_range(THANDLE h, LenType &type, MeasRange &range);
P_IR_API p_ir_get_temperature_range(THANDLE h, LenType type, MeasRange range, short &w_low, short &w_high);

P_IR_API p_ir_set_emiss(THANDLE h, float ft);
P_IR_API p_ir_set_dist(THANDLE h, float ft);
P_IR_API p_ir_set_env_temp(THANDLE h, float ft);
P_IR_API p_ir_set_rel_hum(THANDLE h, float ft);
P_IR_API p_ir_set_modify_temp(THANDLE h, float ft);
P_IR_API p_ir_set_temperature_unit(THANDLE h, TemperatureInit unit);
P_IR_API p_ir_set_temperature_para(THANDLE h, TemperatureInit unit, float f_emiss, float f_dist, float f_env_temp, float f_rel_hum);
P_IR_API p_ir_set_enviroment_reference_temperature(THANDLE h, float f_enviroment_reference_temperature);

P_IR_API p_ir_get_emiss(THANDLE h, float &ft);
P_IR_API p_ir_get_dist(THANDLE h, float &ft);
P_IR_API p_ir_get_env_temp(THANDLE h, float &ft);
P_IR_API p_ir_get_rel_hum(THANDLE h, float &ft);
P_IR_API p_ir_get_modify_temp(THANDLE h, float &ft);
P_IR_API p_ir_get_temperature_unit(THANDLE h, TemperatureInit &unit);
P_IR_API p_ir_get_temperature_para(THANDLE h, TemperatureInit &unit, float &f_emiss, float &f_dist, float &f_env_temp, float &f_rel_hum);
P_IR_API p_ir_get_enviroment_reference_temperature(THANDLE h, float &f_enviroment_reference_temperature);

P_IR_API p_ir_use_default_temperature_para(THANDLE h, bool b_default);
P_IR_API p_ir_is_using_default_temperature_para(THANDLE h, bool &b_default);

P_IR_API p_ir_get_temperature(THANDLE h, unsigned short *pw_ad, unsigned int dw_ad_count, float *pf_temp);
P_IR_API p_ir_get_temperature_ex(THANDLE h, unsigned short *pw_ad, unsigned int dw_ad_count, float f_emiss, float f_dist, float *pf_temp);
P_IR_API p_ir_get_temperature_with_self_para(THANDLE h, unsigned short *pw_ad, unsigned int dw_ad_count, float *pf_emiss, float *pf_dist, float *pf_temp);
P_IR_API p_ir_get_temperature_from_ad(THANDLE h, unsigned short w_ad, float &f_temp);
P_IR_API p_ir_get_temperature_from_ad_ex(THANDLE h, unsigned short w_ad, float f_emiss, float f_dist, float &f_temp);
P_IR_API p_ir_get_temperature_from_position(THANDLE h, unsigned short w_x, unsigned short w_y, float &f_temp);
P_IR_API p_ir_get_temperature_from_position_ex(THANDLE h, unsigned short w_x, unsigned short w_y, float f_emiss, float f_dist, float &f_temp);
P_IR_API p_ir_get_all_pixel_temperature(THANDLE h, float *pf_temp);
P_IR_API p_ir_get_ad_from_temperature(THANDLE h, float f_temp, unsigned short &w_ad);
P_IR_API p_ir_get_temperature_and_ad_from_position(THANDLE h, unsigned short w_x, unsigned short w_y, unsigned short &w_ad, float &f_temp);

//< interface about temperature, bits and anas
P_IR_API p_ir_get_resolution(THANDLE h, unsigned short &w_width, unsigned short &w_height);

P_IR_API p_ir_get_max_temperature(THANDLE h, unsigned short &w_x, unsigned short &w_y, unsigned short &w_ad, float &f_temp);
P_IR_API p_ir_get_min_temperature(THANDLE h, unsigned short &w_x, unsigned short &w_y, unsigned short &w_ad, float &f_temp);
P_IR_API p_ir_get_avg_temperature(THANDLE h, unsigned short &w_avg, float &f_temp);
P_IR_API p_ir_get_bits(THANDLE h, unsigned char *pby_bits);
P_IR_API p_ir_get_bits_ex(THANDLE h, unsigned char *pby_bits, unsigned short w_width, unsigned short w_height,
                          unsigned short w_left, unsigned short w_top, unsigned short w_right, unsigned short w_bottom);

P_IR_API p_ir_use_default_imaging_para(THANDLE h, bool b_default);
P_IR_API p_ir_is_using_default_imaging_para(THANDLE h, bool &b_default);
P_IR_API p_ir_set_palette(THANDLE h, PALETTE pi);
P_IR_API p_ir_get_palette(THANDLE h, PALETTE &pi);
P_IR_API p_ir_auto_imaging(THANDLE h);
P_IR_API p_ir_set_imaging_range(THANDLE h, unsigned short w_low, unsigned short w_high);
P_IR_API p_ir_get_imaging_range(THANDLE h, unsigned short &w_low, unsigned short &w_high);
P_IR_API p_ir_set_low_of_imaging_range(THANDLE h, unsigned short w_low);
P_IR_API p_ir_get_low_of_imaging_range(THANDLE h, unsigned short &w_low);
P_IR_API p_ir_set_high_of_imaging_range(THANDLE h, unsigned short w_low);
P_IR_API p_ir_get_high_of_imaging_range(THANDLE h, unsigned short &w_low);
P_IR_API p_ir_set_set_apply_blur(THANDLE h, bool b_apply);

P_IR_API p_ir_use_default_isotherm(THANDLE h, bool b_default);
P_IR_API p_ir_is_using_default_isotherm(THANDLE h, bool &b_default);
P_IR_API p_ir_add_isotherm(THANDLE h, THANDLE &h_isotherm, float f_low, float f_high, unsigned int dw_clr, bool b_transparent);
P_IR_API p_ir_delete_isotherm(THANDLE h, THANDLE h_isotherm);
P_IR_API p_ir_delete_all_isotherm(THANDLE h);
P_IR_API p_ir_update_isotherm(THANDLE h, THANDLE h_isotherm, float f_low, float f_high, unsigned int dw_clr, bool b_transparent);
P_IR_API p_ir_get_isotherm_count(THANDLE h, unsigned char &by_count);
P_IR_API p_ir_get_isotherm(THANDLE h, unsigned char by_index, THANDLE &h_isotherm, float &f_low, float &f_high, unsigned int &dw_clr, bool &b_transparent);

P_IR_API p_ir_set_zoom_factor(THANDLE h, ImageZoomFactor zoom_factor);
P_IR_API p_ir_get_zoom_factor(THANDLE h, ImageZoomFactor &zoom_factor);
P_IR_API p_ir_set_rotation_angle(THANDLE h, RotationAngle angle);
P_IR_API p_ir_get_rotation_angle(THANDLE h, RotationAngle &angle);

//< ana
P_IR_API p_ir_add_anas(THANDLE h, const std::string &str_anas);
P_IR_API p_ir_update_ana_size(THANDLE h, const std::string &str_old, const std::string &str_new);
P_IR_API p_ir_update_ana_size_ex(THANDLE h, const std::string &str_ana, const std::vector<S_POINT> &vct_nodes);
P_IR_API p_ir_delete_anas(THANDLE h, const std::string &str_anas);
P_IR_API p_ir_delete_all_ana(THANDLE h);
P_IR_API p_ir_remove_anas_saved_in_file(THANDLE h);
P_IR_API p_ir_add_anas_saved_in_file(THANDLE h);
P_IR_API p_ir_get_str_of_anas(THANDLE h, AnaType ana_type, std::string &str_anas);
P_IR_API p_ir_get_str_of_anas_saved_in_file(THANDLE h, AnaType ana_type, std::string &str_anas);
P_IR_API p_ir_get_ana_count(THANDLE h, AnaType ana_type, unsigned int &dw_count);

P_IR_API p_ir_get_hit_ana(THANDLE h, unsigned short w_x, unsigned short w_y, AnaType &ana_type, std::string &str_ana);
P_IR_API p_ir_set_hit_range(THANDLE h, AnaType ana_type, unsigned char by_hit_range);

P_IR_API p_ir_ana_get_nodes(THANDLE h, const std::string &str_ana, std::vector<S_POINT> &vct_nodes);
P_IR_API p_ir_ana_get_pixel_count(THANDLE h, const std::string &str_ana, unsigned int &dw_count);
P_IR_API p_ir_ana_get_temperature(THANDLE h, const std::string &str_ana, unsigned int &dw_count, float *pf_temp);
P_IR_API p_ir_ana_get_all_pos(THANDLE h, const std::string &str_ana, unsigned int &dw_count, S_POINT *p_pos);

P_IR_API p_ir_ana_get_max_temperature(THANDLE h, const std::string &str_ana, unsigned short &w_x, unsigned short &w_y, unsigned short &w_ad, float &f_temp);
P_IR_API p_ir_ana_get_min_temperature(THANDLE h, const std::string &str_ana, unsigned short &w_x, unsigned short &w_y, unsigned short &w_ad, float &f_temp);
P_IR_API p_ir_ana_get_avg_temperature(THANDLE h, const std::string &str_ana, unsigned short &w_avg, float &f_temp);

P_IR_API p_ir_ana_show_max_point(THANDLE h, const std::string &str_ana, bool b_show);
P_IR_API p_ir_ana_show_min_point(THANDLE h, const std::string &str_ana, bool b_show);
P_IR_API p_ir_ana_is_show_max_point(THANDLE h, const std::string &str_ana, bool &b_show);
P_IR_API p_ir_ana_is_show_min_point(THANDLE h, const std::string &str_ana, bool &b_show);
P_IR_API p_ir_ana_set_show_text_type(THANDLE h, const std::string &str_ana, ShowTextType type);
P_IR_API p_ir_ana_get_show_text_type(THANDLE h, const std::string &str_ana, ShowTextType &type);
P_IR_API p_ir_ana_set_text_location(THANDLE h, const std::string &str_ana, AnaTextLocation location);
P_IR_API p_ir_ana_get_text_location(THANDLE h, const std::string &str_ana, AnaTextLocation &location);

P_IR_API p_ir_ana_show_alias(THANDLE h, const std::string &str_ana, bool b_show);
P_IR_API p_ir_ana_is_show_alias(THANDLE h, const std::string &str_ana, bool &b_show);
P_IR_API p_ir_ana_set_alias(THANDLE h, const std::string &str_ana, const std::string &str_alias);
P_IR_API p_ir_ana_get_alias(THANDLE h, const std::string &str_ana, std::string &str_alias);

P_IR_API p_ir_ana_set_visible(THANDLE h, const std::string &str_ana, bool b_visible);
P_IR_API p_ir_ana_is_visible(THANDLE h, const std::string &str_ana, bool &b_visible);
P_IR_API p_ir_ana_set_can_move(THANDLE h, const std::string &str_ana, bool b_move);
P_IR_API p_ir_ana_is_can_move(THANDLE h, const std::string &str_ana, bool &b_move);

P_IR_API p_ir_ana_show_des(THANDLE h, const std::string &str_ana, bool b_show);
P_IR_API p_ir_ana_is_show_des(THANDLE h, const std::string &str_ana, bool &b_show);
P_IR_API p_ir_ana_set_des(THANDLE h, const std::string &str_ana, const std::string &str_des);
P_IR_API p_ir_ana_get_dea(THANDLE h, const std::string &str_ana, std::string &str_des);

P_IR_API p_ir_ana_set_line_color(THANDLE h, const std::string &str_ana, unsigned int dw_color);
P_IR_API p_ir_ana_get_line_color(THANDLE h, const std::string &str_ana, unsigned int &dw_color);

P_IR_API p_ir_ana_set_self_para(THANDLE h, const std::string &str_ana, bool b_self_para, float f_emiss, float f_dist);
P_IR_API p_ir_ana_get_self_para(THANDLE h, const std::string &str_ana, bool &b_self_para, float &f_emiss, float &f_dist);

P_IR_API p_ir_ana_has_cursor(THANDLE h, const std::string &str_ana, bool &b_has_cursor);
P_IR_API p_ir_ana_show_cursor(THANDLE h, const std::string &str_ana, bool b_show);
P_IR_API p_ir_ana_show_temp_of_cursor(THANDLE h, const std::string &str_ana, bool b_show);
P_IR_API p_ir_ana_is_show_cursor(THANDLE h, const std::string &str_ana, bool &b_show);
P_IR_API p_ir_ana_is_show_temp_of_cursor(THANDLE h, const std::string &str_ana, bool &b_show);
P_IR_API p_ir_ana_set_scale_of_cursor(THANDLE h, const std::string &str_ana, float f_scale);
P_IR_API p_ir_ana_get_scale_of_cursor(THANDLE h, const std::string &str_ana, float &f_scale);
P_IR_API p_ir_ana_get_temperature_of_cursor(THANDLE h, const std::string &str_ana, unsigned short &w_x, unsigned short &w_y, float &f_temp);

P_IR_API p_ir_ana_apply_self_palette(THANDLE h, const std::string &str_ana, bool b_apply);
P_IR_API p_ir_ana_is_use_self_palette(THANDLE h, const std::string &str_ana, bool &b_apply);
P_IR_API p_ir_ana_set_palette(THANDLE h, const std::string &str_ana, PALETTE pi);
P_IR_API p_ir_ana_auto_imaging(THANDLE h, const std::string &str_ana);
P_IR_API p_ir_ana_set_imaging_range(THANDLE h, const std::string &str_ana, unsigned short w_low, unsigned short w_high);
P_IR_API p_ir_ana_set_low_of_imaging_range(THANDLE h, const std::string &str_ana, unsigned short w_low);
P_IR_API p_ir_ana_set_high_of_imaging_range(THANDLE h, const std::string &str_ana, unsigned short w_high);
P_IR_API p_ir_ana_get_palette(THANDLE h, const std::string &str_ana, PALETTE &pi);
P_IR_API p_ir_ana_get_imaging_range(THANDLE h, const std::string &str_ana, unsigned short &w_low, unsigned short &w_high);
P_IR_API p_ir_ana_get_low_of_imaging_range(THANDLE h, const std::string &str_ana, unsigned short &w_low);
P_IR_API p_ir_ana_get_high_of_imaging_range(THANDLE h, const std::string &str_ana, unsigned short &w_high);

P_IR_API p_ir_repair_pt(THANDLE h, unsigned short w_x, unsigned short w_y, unsigned short w_ad);
P_IR_API p_ir_reapply(THANDLE h);
P_IR_API p_ir_set_human_mode(THANDLE h, bool b_human_mode);
#ifdef __cplusplus
}
#endif

#endif //< _P_IR_HEADER_
