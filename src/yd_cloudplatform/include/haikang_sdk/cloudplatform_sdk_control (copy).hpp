#ifndef __CLOUDPLATFORM_SDK_CONTROL__
#define __CLOUDPLATFORM_SDK_CONTROL__

#include <iostream>
#include <stdio.h>
#include <iostream>
#include <stdio.h>

#include <diagnostic_msgs/DiagnosticArray.h>
#include <std_msgs/Int32.h>
#include <nav_msgs/Odometry.h>

#include "yidamsg/Detect_Result.h"
#include "yd_cloudplatform/CloudPlatControl.h"
#include "yd_cloudplatform/GetTemperature.h"

#include "ros/ros.h"
#include "image_transport/image_transport.h"
#include "cv_bridge/cv_bridge.h"
#include "sensor_msgs/image_encodings.h"
#include "sensor_msgs/Image.h"
#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

#define __app_name__ "cloudplat_sdk_control"

#define MAX_DEGREE_DEC 3600
#define MAX_DEGREE_HEX 0x3600

#define ISAPI_OUT_LEN	3 * 1024 * 1024
#define ISAPI_STATUS_LEN  8*1024
 
class cloudplatform_sdk_control
{
private:
    ros::NodeHandle node_handle_, private_node_handle_;
    int level;
    string message;

    std::string visible_topic_str, heartbeat_topic_str, sdkcom_dir_str;
    std::string m_device_ip, m_device_port, m_device_username, m_device_password;
    std::string m_image_width, m_image_height;

    ros::Publisher heartbeat_pub_;
    ros::Publisher cloudplatform_pub_;
    ros::Publisher isreach_pub_;
    ros::Publisher yuntaiposition_pub_;

    ros::ServiceServer cloudplatform_server;
    ros::ServiceServer gettemperature_server;

    ros::Subscriber detectresult_sub_;

public:
    string device_id;
    deque<string> _cmd_control_queue, _cmd_read_queue;
    pthread_mutex_t _mutex_read, _mutex_control;
    ros::Publisher temperature_buffer_pub_;

private:
    void create_getdegree_thread();
    void create_work_thread();
    void create_gettemperature_thread();
    
public:
    cloudplatform_sdk_control();
    ~cloudplatform_sdk_control();
    bool set_action(int id, int type, int value, int xy_value, int z_value, int zoom_value);
    int get_action(int id, int type);
    void update();
    void pub_heartbeat(int level, string message);

    void detect_rect_callback(const yidamsg::Detect_Result& msg);

    bool handle_cloudplatform(yd_cloudplatform::CloudPlatControl::Request &req, yd_cloudplatform::CloudPlatControl::Response &res);
    bool handle_gettemperature(yd_cloudplatform::GetTemperature::Request &req, yd_cloudplatform::GetTemperature::Response &res);
};

#endif