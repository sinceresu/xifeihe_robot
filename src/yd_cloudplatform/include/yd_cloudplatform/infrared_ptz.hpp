#ifndef __VISIBLE_PTZ_H__
#define __VISIBLE_PTZ_H__

#include <iostream>
#include <stdio.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>

#include <tf/transform_listener.h>
#include <diagnostic_msgs/DiagnosticArray.h>
#include <std_msgs/Int32.h>
#include <std_msgs/String.h>
#include <std_msgs/Bool.h>
#include <sensor_msgs/CompressedImage.h>
#include <nav_msgs/Odometry.h>

#include <yd_cloudplatform/CloudPlatControl.h>
#include <yd_cloudplatform/GetTemperature.h>
#include <yidamsg/transfer.h>
#include <yidamsg/InspectedResult.h>
#include <yidamsg/Detect_Result.h>


#define __app_name__ "infrared_cloudplat"
#define PI 3.141592654

using namespace std;

class infrared_ptz
{
private:
    ros::NodeHandle private_node_handle_;

    cv::Mat _infrared_image, _visible_image;
    std::vector<unsigned char> _infrared_image_char, _visible_image_char;

    std::string heartbeat_topic_str;
    int _camera_id, _use_hk_cloudplatform;
    double _handlexy, _handlez;
    int _x_min_degree, _x_max_degree, _z_origin_degree, _z_max_degree, _z_min_degree, _offset_xy;
    bool _cloudplatform_status_ready;
    int _scan_speed, _scan_time;

    vector<std::string> transferpub_msg;

    float _x_camera, _y_camera, _z_camera;
    float _x_cloudplatform, _y_cloudplatform, _z_cloudplatform;
	float _angley, _anglez;
    float _get_temperature;

    int level;
    string message;

    tf::TransformListener listener;

    ros::Publisher heartbeat_pub_;
    ros::Publisher stoplaser_pub_;
    ros::Publisher infrared_result_pub_;
    ros::Publisher meterflag_pub_;
    ros::Publisher append_data_pub_;
    ros::ServiceClient cloudplatform_client;
    ros::ServiceClient gettemperature_client;

    ros::Subscriber robot_pose_sub_;
	ros::Subscriber transfer_sub_;
    ros::Subscriber isreach_sub_;
    ros::Subscriber meterflag_sub_;

public:
	ros::NodeHandle node_handle_;
    ros::Subscriber infrared_image_sub_;
    ros::Subscriber visible_image_sub_;

private:
	float get_xydegree(vector<float> camera_position, vector<float> device_position, vector<float> cloudplatform_position);
	vector<float> get_newpoint(vector<float> camera_position, vector<float> device_position, vector<float> cloudplatform_position, int xydegree);
	float get_zdegree(float x_newpoint, float y_newpoint, float z_camera, vector<float> device_position, float angley);

    void set_position_first();

    void points_callback(const nav_msgs::Odometry& msg);
    void transfer_callback(const yidamsg::transfer& msg);
    void isreach_callback(const std_msgs::Int32& msg);
    void meterflag_callback(const std_msgs::String& msg);

public:
    infrared_ptz(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle);
    ~infrared_ptz();
    void update();
    void pub_heartbeat(int level, string message);
    void infrared_image_callback(const sensor_msgs::CompressedImagePtr& msg);
    void visible_image_callback(const sensor_msgs::CompressedImagePtr& msg);
};

#endif
