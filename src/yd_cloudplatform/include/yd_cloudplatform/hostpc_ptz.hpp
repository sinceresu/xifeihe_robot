#ifndef __VISIBLE_PTZ_H__
#define __VISIBLE_PTZ_H__

#include <iostream>
#include <stdio.h>

#include <diagnostic_msgs/DiagnosticArray.h>
#include <nav_msgs/Odometry.h>
#include <std_msgs/Int32.h>
#include <std_msgs/String.h>
#include <std_msgs/Bool.h>

#include <yd_cloudplatform/CloudPlatControl.h>
#include <yidamsg/manualControlParameters.h>


#define __app_name__ "hostpc_cloudplat"

using namespace std;

class hostpc_ptz
{
private:
    ros::NodeHandle node_handle_, private_node_handle_;

    std::string heartbeat_topic_str;
    int _cloudplatform_id, _use_hk_cloudplatform;
    vector<unsigned int> _camera_focus_value;
    int _x_min_degree, _x_max_degree, _z_origin_degree, _z_max_degree, _z_min_degree;
    int _step_1, _step_2, _step_3;
    int _now_xyposition, _now_zposition;

    int level;
    string message;

    ros::Publisher heartbeat_pub_;
    ros::Subscriber manual_control_sub_;
    ros::Subscriber now_xyzposition_sub_;
    ros::ServiceClient cloudplatform_client;

private:
    void control_mode_step(int cloudplatform_id, string direction, int step);
    void control_mode_value(int cloudplatform_id, int horizontal, int vertical, int focus);
    void control_mode_run(int cloudplatform_id, string direction);
    void control_mode_stop(int cloudplatform_id);
    void yuntaictrl_callback(const yidamsg::manualControlParameters& msg);
    void nowposition_callback(const nav_msgs::Odometry& msg);

public:
    hostpc_ptz(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle);
    ~hostpc_ptz();
    void update();
    void pub_heartbeat(int level, string message);
};

#endif