#ifndef __VISIBLE_PTZ_H__
#define __VISIBLE_PTZ_H__

#include <iostream>
#include <stdio.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>

#include <tf/transform_listener.h>
#include <diagnostic_msgs/DiagnosticArray.h>
#include <std_msgs/Int32.h>
#include <std_msgs/String.h>
#include <std_msgs/Bool.h>
#include <sensor_msgs/CompressedImage.h>
#include <nav_msgs/Odometry.h>

#include "tf2_ros/transform_listener.h"
#include "tf2_ros/transform_broadcaster.h"
#include "tf2_geometry_msgs/tf2_geometry_msgs.h"
#include "tf2_eigen/tf2_eigen.h"

#include <yd_cloudplatform/CloudPlatControl.h>
#include <yidamsg/ArmControl.h>
#include <yidamsg/ArmDataStamped.h>
#include <yidamsg/transfer.h>
#include <yidamsg/InspectedResult.h>
#include <yidamsg/Detect_Result.h>

#include "camera.h"


#define __app_name__ "visible_cloudplat"
#define PI 3.141592654

using namespace std;

class visible_ptz
{
private:
    ros::NodeHandle private_node_handle_;

    cv::Mat _image;
    std::vector<unsigned char> _image_char;

    std::string heartbeat_topic_str;
    int _use_hk_cloudplatform, _camera_id, _camera_image_width, _camera_image_height, _camera_focus_number, _device_type, _use_arm;
    double _handlexy, _handlez, _camera_cmos_width, _camera_cmos_height, _camera_pixel_size, _zoom_focus_mm;
    std::string  _camera_ip, _camera_username, _camera_password;
	vector<float> _camera_focus_mm;
    vector<unsigned int> _camera_focus_value;
    int _x_min_degree, _x_max_degree, _z_origin_degree, _z_max_degree, _z_min_degree, _offset_xy;
    float _first_finalx, _first_finalz;
    int visible_survey_parm_id;
    bool _cloudplatform_status_ready;
    bool _robot_arm_status_ready;

    vector<std::string> transferpub_msg;

    float _x_camera, _y_camera, _z_camera;
    float _x_cloudplatform, _y_cloudplatform, _z_cloudplatform;
	float _angley, _anglez;

    int level;
    string message;

    tf::TransformListener listener;
    
    tf2_ros::Buffer buffer_;
    tf2_ros::TransformListener listener_;
    tf2_ros::TransformBroadcaster broadcaster_;
    std::shared_ptr<Camera> cameras_ptr_;

    ros::Publisher heartbeat_pub_;
    ros::Publisher readyimage_pub_;
    ros::Publisher readyimage_yiwu_pub_;
    ros::Publisher stoplaser_pub_;
    ros::ServiceClient cloudplatform_client;
    ros::ServiceClient robot_arm_client;

    ros::Subscriber robot_pose_sub_;
	ros::Subscriber transfer_sub_;
	ros::Subscriber detectresult_sub_;
    ros::Subscriber cloudplatform_isreach_sub_;
    ros::Subscriber ptz_sub_;
    ros::Subscriber robot_arm_sub_;

public:
	ros::NodeHandle node_handle_;
    ros::Subscriber image_sub_;

private:
    int get_zoomset(vector<float> camera_position, vector<float> device_position, int device_width, int device_height, int zoom_scale);
	float get_xydegree(vector<float> camera_position, vector<float> device_position, vector<float> cloudplatform_position);
	vector<float> get_newpoint(vector<float> camera_position, vector<float> device_position, vector<float> cloudplatform_position, int xydegree);
	float get_zdegree(float x_newpoint, float y_newpoint, float z_camera, vector<float> device_position, float angley);

    void set_position_first();

    void points_callback(const nav_msgs::Odometry& msg);
    void transfer_callback(const yidamsg::transfer& msg);
    void transfer_handle_fun(const yidamsg::transfer& msg, void *p);
    void detect_rect_callback(const yidamsg::Detect_Result& msg);
    void isreach_callback(const std_msgs::Int32& msg);
    void ptz_pose_callback(const nav_msgs::Odometry & msg);
    void robot_arm_data_callback(const yidamsg::ArmDataStamped& msg);

public:
    visible_ptz(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle);
    ~visible_ptz();
    void update();
    void pub_heartbeat(int level, string message);
    void image_callback(const sensor_msgs::CompressedImagePtr& msg);
};

#endif
