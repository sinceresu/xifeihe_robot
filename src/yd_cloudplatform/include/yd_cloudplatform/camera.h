#ifndef _COLORINT_SERVICE_CAMERA_H
#define _COLORINT_SERVICE_CAMERA_H

#include <vector>
#include <memory>
#include <thread>
#include <Eigen/Core>
#include <Eigen/Geometry>

#include "opencv2/opencv.hpp"

class Camera 
{
public:
  Camera();
  ~Camera(){};

   int LoadCalibrationFile(const std::string& calibration_filepath, std::string cam_type);
   void CalculateFov() ;
   Eigen::Affine3d GetRelativePose(const std::vector<double> &rads);
   int GetCameraFov(double &horizontal_fov, double& vertical_fov);
   int GetCameraSize(cv::Size &size);
   int GetRads(const Eigen::Affine3d pose, std::vector<double> &rads);

private:
  // expressed in the camera frame.
  cv::Mat intrinsic_mat_;  
  std::vector<Eigen::Matrix4d> extrinsic_mats_;
  // cv::Mat centered_intrinsic_mat_;  
  Eigen::Matrix4d tool_mat_;
  cv::Mat distortion_coeffs_;
  cv::Size image_size_;
  Eigen::Affine3d cam_to_ref_transform_;

  Eigen::Matrix4d cam_extrinsic_mat_;

  float horizontal_fov_;
  float vertical_fov_;
};

#endif  
