#include "decoder_h264/double_queue.hpp"


double_queue::double_queue(/* args */)
{
    m_free_queue = (CommonQueue *)malloc(sizeof(struct CommonQueue));
    m_work_queue = (CommonQueue *)malloc(sizeof(struct CommonQueue));

    init_queue(m_free_queue, Free_Queue);
    init_queue(m_work_queue, Work_Queue);

    for(int i = 0; i < QUEUE_SIZE; i++)
    {
        CommonQueueNode *node = (CommonQueueNode *)malloc(sizeof(struct CommonQueueNode));
        node->data = 0;
        node->size = 0;
        node->index = 0;
        push_queue(m_free_queue, node);
    }

    pthread_mutex_init(&m_free_queue_mutex, nullptr);
    pthread_mutex_init(&m_work_queue_mutex, nullptr);
}

double_queue::~double_queue()
{

}

void double_queue::init_queue(CommonQueue *queue, QueueType type)
{
    if(nullptr != queue)
    {
        queue->type = type;
        queue->size = 0;
        queue->front = 0;
        queue->rear = 0;
    }    
}

void double_queue::push_queue(CommonQueue *queue, CommonQueueNode *node)
{
    if(nullptr == queue)
    {
        std::cout << "in push_queue function, queue is null!" << std::endl;
        return;
    }
    if(nullptr == node)
    {   
        std::cout << "in push_queue function, node is null!" << std::endl;
        return;
    }

    node->next = nullptr;
    if(Free_Queue == queue->type)
    {
        pthread_mutex_lock(&m_free_queue_mutex);
        if(queue->front == nullptr)
        {
            queue->front = node;
            queue->rear = node;
        }
        else
        {
            //head in, head out
            node->next = queue->front;
            queue->front = node;
        }
        queue->size += 1;
        pthread_mutex_unlock(&m_free_queue_mutex);
    }

    if(Work_Queue == queue->type)
    {
        pthread_mutex_lock(&m_work_queue_mutex);
        static unsigned int node_index = 0;
        node->index = (++node_index);
        if(node_index > 500)
            node_index = 0;
        if(queue->front == nullptr)
        {
            queue->front = node;
            queue->rear = node;
        }
        else
        {
            //tail in, head out
            queue->rear->next = node;
            queue->rear = node;
        }
        queue->size += 1;
        pthread_mutex_unlock(&m_work_queue_mutex);
    }
}

CommonQueueNode *double_queue::pop_queue(CommonQueue *queue)
{
    if(nullptr == queue)
    {
        std::cout << "in pop queue function, queue is null!" << std::endl;
        return nullptr;
    }

    const std::string type = (queue->type == Work_Queue) ? "work_queue" : "free_queue";
    //std::cout << "queue type is " << type << std::endl;
    pthread_mutex_t queue_mutex = (queue->type == Work_Queue) ? m_work_queue_mutex : m_free_queue_mutex;

    CommonQueueNode *element = nullptr;
    pthread_mutex_lock(&queue_mutex);
    element = queue->front;
    if(nullptr == element)
    {
        pthread_mutex_unlock(&queue_mutex);
        std::cout << "in pop queue function, node is null!" << std::endl;
        return nullptr;
    }

    queue->front = queue->front->next;
    queue->size -= 1;
    pthread_mutex_unlock(&queue_mutex);
    return element;
}

void double_queue::clear_queue(CommonQueue *queue)
{
    while (queue->size) {
        CommonQueueNode *node = pop_queue(queue);
        free_node(node);
    }
}

void double_queue::free_node(CommonQueueNode *node)
{
    if(nullptr != node){
        free(node->data);
        free(node);
    }
}

void double_queue::reset_free_queue(CommonQueue *free_queue, CommonQueue *work_queue)
{
    if (nullptr == free_queue) {
        std::cout << "in reset free queue function, free queue is null!" << std::endl;
        return;
    }
    
    if (nullptr == work_queue) {
        std::cout << "in reset free queue function, work queue is null!" << std::endl;
        return;
    }
    
    int workqueue_size = work_queue->size;
    if (workqueue_size > 0) {
        for (int i = 0; i < workqueue_size; i++) {
            CommonQueueNode *node = pop_queue(work_queue);
            if(nullptr != node)
            {
                free(node->data);
                node->data = nullptr;
                push_queue(free_queue, node);
            }
        }
    }
}

