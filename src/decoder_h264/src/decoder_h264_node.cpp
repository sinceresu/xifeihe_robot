#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <string>
#include <sstream>
#include <vector>

#include "ros/ros.h"
#include "image_transport/image_transport.h"
#include "cv_bridge/cv_bridge.h"
#include "sensor_msgs/image_encodings.h"
#include "sensor_msgs/Image.h"
#include <opencv2/opencv.hpp>

#include <decoder_h264/decoder_h264_node.hpp>
#include <decoder_h264/H264Decoder.h>

using namespace std;


Mat g_frame;
int g_lPort = 0;
char* g_rgbBuffer = NULL;
unsigned char* g_yuvBuffer = NULL;
unsigned int g_len = 0;
std::string g_error_info_str;

long lUserID;
long lRealPlayHandle;

int flag = 0;
cv::Mat g_result_pic;
int g_get_image = 0;
H264Decoder h264_decoder;

void *display_image(void* args)
{
    while(true)
    {
        if(g_get_image == 1)
        {
            try{
                g_get_image = 0;
                // cv::imshow("img", g_result_pic);
                // cv::waitKey(30);
            }
            catch(const std::exception& e){
                std::cerr << e.what() << '\n';
            }
        }
    }
}

 void decoder_h264_node::getstream_callback(const sensor_msgs::Image& msg)
 {
    vector<unsigned char> vc;
    vc = msg.data;
    //for (int n=0;n<4;n++)
	//	            printf("  0x%02x\t%02x\n",vc[n],vc[n]);
    //std::cout << "strlen(pBuf):" << vc.size() << std::endl;

    unsigned char* pBuffer = &vc.at(0);
    int dwBufSize = vc.size();

    //for (int n=0;n<4;n++)
	//	printf("  0x%02x\t%02x\n",vc[n],vc[n]);
    //std::cout << "strlen(pBuf):" << vc.size() << std::endl;
    // for (int n=0;n<4;n++)
    //     //printf("  %02x ", pBuffer[n]);
    //     printf("  0x%02x\t%02x\n",(pBuffer+n),pBuffer[n]);
    std::cout << "strlen(pBuffer):" << dwBufSize << std::endl;

    try
    {
        if(pBuffer[4] == 0x67)
        {
            h264_decoder.decode(pBuffer, dwBufSize);
            g_result_pic = h264_decoder.getMat();
            g_get_image = 1;
        }
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }
 }

decoder_h264_node::decoder_h264_node()
{ 
    ros::NodeHandle private_node_handle("~");
    ros::NodeHandle node_handle_;

    //private_node_handle.param<std::string>("visible_topic_name", visible_topic_str, "rtsp://admin:123qweasd@192.168.1.64:554/h264/ch1/main/av_stream");
    //private_node_handle.param<std::string>("heartbeat_topic_string", heartbeat_topic_str, "/yida/heartbeat");

    heartbeat_pub_ = node_handle_.advertise<diagnostic_msgs::DiagnosticArray>("/yida/heartbeat", 1);
    getstream_sub_ = node_handle_.subscribe("/yida/visible/image_proc", 20, &decoder_h264_node::getstream_callback, this, ros::TransportHints().tcpNoDelay(true));
    image_transport::ImageTransport it(node_handle_);
    h264decodeframe_pub = it.advertise("/yida/internal/visible/image_proc", 1);
    for (int i = 0; i < MAX_DEV+1; i++)
	{
		if(i < MAX_DEV)
		{
			for(int nI=0; nI < MAX_FRAME_LENTH; nI++)
			{
				memset(&m_pFrameCache[nI], 0x00, sizeof(CacheBuffder));
			}
			m_mFrameCacheLenth = 0;
		}
	}

    //create_display_thread();
}

decoder_h264_node::~decoder_h264_node()
{

}

void decoder_h264_node::create_display_thread()
{
    pthread_t ros_thread_display = 0;
    pthread_create(&ros_thread_display,NULL,display_image,NULL);
}

bool decoder_h264_node::get_h246_fromps(BYTE* pBuffer, int nBufLenth, BYTE** pH264, int& nH264Lenth, bool& bVideo)
{
	return true;
}

void decoder_h264_node::update()
{
    if(g_get_image == 1)
    {
        try
        {
            if(!g_result_pic.empty())
            {
            sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", g_result_pic).toImageMsg();
            msg->header.stamp = ros::Time::now();
            msg->header.frame_id = "visible_picture";
            h264decodeframe_pub.publish(msg);
	    std::cout << "pub msg!" << std::endl;
	    g_get_image = 0;
            }
        }
        catch(const std::exception& e)
        {
            std::cerr << e.what() << '\n';
        } 
    }
}

void decoder_h264_node::pub_heartbeat(int level, string message, string hardware_id)
{
    diagnostic_msgs::DiagnosticArray log;
    log.header.stamp = ros::Time::now();
    
    diagnostic_msgs::DiagnosticStatus s;
    s.name = __app_name__;         // 这里写节点的名字
    s.level = level;               // 0 = OK, 1 = Warn, 2 = Error
    if (!message.empty())
    {
        s.message = message;       // 问题描述
    }
    s.hardware_id = hardware_id;   // 硬件信息
    log.status.push_back(s);

    heartbeat_pub_.publish(log);
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, __app_name__);
    decoder_h264_node hsdkcap;
    ros::Rate rate(40);

    while (ros::ok())
    {
        hsdkcap.update();
        ros::spinOnce();
        rate.sleep();
    }

    return 0;
}
