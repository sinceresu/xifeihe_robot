#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <string>
#include <sstream>
#include <vector>

#include "ros/ros.h"
#include "image_transport/image_transport.h"
#include "cv_bridge/cv_bridge.h"
#include "sensor_msgs/image_encodings.h"
#include "sensor_msgs/Image.h"
#include <opencv2/opencv.hpp>
#include <image_transport/image_transport.h>

#include <decoder_h264/doghouse_decoder_rgb.hpp>
#include <decoder_h264/H264Decoder.h>

using namespace std;


Mat g_frame;
int g_lPort = 0;
char* g_rgbBuffer = NULL;
unsigned char* g_yuvBuffer = NULL;
unsigned int g_len = 0;
std::string g_error_info_str;

long lUserID;
long lRealPlayHandle;

int flag = 0;
cv::Mat g_result_pic;
int g_get_image = 0;
H264Decoder h264_decoder;

void *display_image(void* args)
{
    std::cout << "image is normal operation ." << std::endl;
/*     while(true)
    {
        if(g_get_image == 1)
        {
            try{
                g_get_image = 0;
                cv::imshow("img", g_result_pic);
		        cv::imwrite("img.jpg", g_result_pic);
                cv::waitKey(30);
            }
            catch(const std::exception& e){
                std::cerr << e.what() << '\n';
            }
        }
    } */
}

 void decoder_h264_node::getstream_callback(const sensor_msgs::Image& msg)
 {
    vector<unsigned char> vc;
    secs = msg.header.stamp.sec;
    n_secs = msg.header.stamp.nsec;
    vc = msg.data;
    //for (int n=0;n<4;n++)
	//	            printf("  0x%02x\t%02x\n",vc[n],vc[n]);
    //std::cout << "strlen(pBuf):" << vc.size() << std::endl;

    unsigned char* pBuffer = &vc.at(0);
    int dwBufSize = vc.size();

    //for (int n=0;n<4;n++)
	//	printf("  0x%02x\t%02x\n",vc[n],vc[n]);
    //std::cout << "strlen(pBuf):" << vc.size() << std::endl;
/*     for (int n=0;n<5;n++)
        //printf("  %02x ", pBuffer[n]);
        printf("  0x%02x\t%02x\n",pBuffer+n,pBuffer[n]);
    std::cout << "strlen(pBuffer):" << dwBufSize << std::endl; */

    try
    {
        if(pBuffer[4] == 0x67)
        {
            h264_decoder.decode(pBuffer, dwBufSize);
            g_result_pic = h264_decoder.getMat();
            // sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", g_result_pic).toImageMsg();
            sensor_msgs::ImagePtr msg1 = cv_bridge::CvImage(std_msgs::Header(), "rgb8", g_result_pic).toImageMsg();
            msg1->header.stamp.sec = secs;
            msg1->header.stamp.nsec = n_secs;
            image_pub_.publish(msg1);
            g_get_image = 1;
        }
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }
 }

decoder_h264_node::decoder_h264_node()
{ 
    ros::NodeHandle private_node_handle("~");
    ros::NodeHandle node_handle_;

    private_node_handle.param<std::string>("visible_topic_name", visible_topic_str, "/yida/visible/image_proc");
    private_node_handle.param<std::string>("heartbeat_topic_string", heartbeat_topic_str, "/yida/heartbeat");
    image_transport::ImageTransport it(node_handle_);
    image_pub_ = it.advertise("/yida/yt_image", 1);
    heartbeat_pub_ = node_handle_.advertise<diagnostic_msgs::DiagnosticArray>(heartbeat_topic_str, 1);
    getstream_sub_ = node_handle_.subscribe(visible_topic_str, 20, &decoder_h264_node::getstream_callback, this, ros::TransportHints().tcpNoDelay(true));

    for (int i = 0; i < MAX_DEV+1; i++)
	{
		if(i < MAX_DEV)
		{
			for(int nI=0; nI < MAX_FRAME_LENTH; nI++)
			{
				memset(&m_pFrameCache[nI], 0x00, sizeof(CacheBuffder));
			}
			m_mFrameCacheLenth = 0;
		}
	}

    //create_display_thread();
}

decoder_h264_node::~decoder_h264_node()
{

}

void decoder_h264_node::create_display_thread()
{
    pthread_t ros_thread_display = 0;
    pthread_create(&ros_thread_display,NULL,display_image,NULL);
}

bool decoder_h264_node::get_h246_fromps(BYTE* pBuffer, int nBufLenth, BYTE** pH264, int& nH264Lenth, bool& bVideo)
{
	return true;
}

void decoder_h264_node::update()
{
    //if(g_frame.empty())
    /*if(g_len == 0)
    {
        int level = 2;
        std::string message = g_error_info_str;
        std::string hardware_id = "visible_camera";
        pub_heartbeat(level, message, hardware_id);
    }
    else
    {
        int level = 0;
        std::string message = "visible camera is ok!";
        std::string hardware_id = "visible_camera_getstream";
        pub_heartbeat(level, message, hardware_id);
    }*/
}




void decoder_h264_node::pub_heartbeat(int level, string message, string hardware_id)
{
    diagnostic_msgs::DiagnosticArray log;
    log.header.stamp = ros::Time::now();
    
    diagnostic_msgs::DiagnosticStatus s;
    s.name = __app_name__;         // 这里写节点的名字
    s.level = level;               // 0 = OK, 1 = Warn, 2 = Error
    if (!message.empty())
    {
        s.message = message;       // 问题描述
    }
    s.hardware_id = hardware_id;   // 硬件信息
    log.status.push_back(s);

    heartbeat_pub_.publish(log);
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, __app_name__);
    decoder_h264_node hsdkcap;
    ros::Rate rate(20);

    while (ros::ok())
    {
        hsdkcap.update();
        ros::spinOnce();
        rate.sleep();
    }

    return 0;
}
