# cmake needs this line
cmake_minimum_required(VERSION 2.8.3)

# Define project name
project(common)

set(PACKAGE_DEPENDENCIES
)

find_package(catkin REQUIRED COMPONENTS ${PACKAGE_DEPENDENCIES})

include_directories(
  include
  ${catkin_INCLUDE_DIRS}
)

catkin_package(
  CATKIN_DEPENDS
  ${PACKAGE_DEPENDENCIES}
  DEPENDS
  EIGEN3
  INCLUDE_DIRS "./include"
  LIBRARIES ${PROJECT_NAME}
)

file(GLOB_RECURSE ALL_SRCS "src/*.cc" "src/*.h")
 
add_library(${PROJECT_NAME} STATIC
  ${ALL_SRCS}
)
add_subdirectory("./src")

target_link_libraries(${PROJECT_NAME} PUBLIC 
)

# Catkin
target_include_directories(${PROJECT_NAME} SYSTEM PUBLIC ${catkin_INCLUDE_DIRS})
target_link_libraries(${PROJECT_NAME} PUBLIC ${catkin_LIBRARIES})
add_dependencies(${PROJECT_NAME} ${catkin_EXPORTED_TARGETS})

# Add the binary directory first, so that port.h is included after it has
# been generated.
target_include_directories(${PROJECT_NAME} PUBLIC
    $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
    $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>
    $<INSTALL_INTERFACE:include>
)


install(TARGETS ${PROJECT_NAME}
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_GLOBAL_BIN_DESTINATION}
)

# Install source headers.
# file(GLOB_RECURSE HDRS "common/*.h")
# foreach(HDR ${HDRS})
#   file(RELATIVE_PATH REL_FIL ${PROJECT_SOURCE_DIR} ${HDR})
#   get_filename_component(INSTALL_DIR ${REL_FIL} DIRECTORY)
#   install(
#     FILES
#       ${HDR}
#     DESTINATION
#       include/${INSTALL_DIR}
#   )
# endforeach()

