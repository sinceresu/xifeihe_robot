#ifndef YD_FRAMWORK_HMI_HPP
#define YD_FRAMWORK_HMI_HPP
#include <ros/ros.h>
#include "std_srvs/SetBool.h"
#include <std_msgs/String.h>
#include <diagnostic_msgs/DiagnosticArray.h>
#include <Uart.h>

#include <vector>
#include <queue>
#include <map>

using namespace std;

struct MonNodeS
{
    std::string mon;
    std::string node;
    std::string state;
    std::string res;
    std::string cpu;
    std::string mem;
};

class HmiUI
{
public:
    HmiUI():page(-1),pageSub(0),sys0(0)
    {
        openDevice();
        //register 
        nh.param<std::string>("/MonitoringCenter/shellPath", shellPath, "");
        nh.param<std::string>("/MonitoringCenter/serviceName", serviceName, "");
        nh.param<std::string>("/MonitoringCenter/hmiName", hmiName, "/dev/ttyUSB3");//hmiName
        client = nh.serviceClient<std_srvs::SetBool>(serviceName);
        //init
        hws["yd"] = hwMap();
        hws["nvidia"] = hwMap();
        reset();
    }
    ~HmiUI()
    {
        ut.close();
    }

    void reset(){
        string cmd = "";
        vector<string> array;
        array.push_back("sys0=0");
        array.push_back("page main");
        comCmd(cmd, array);
        sendCmd(cmd);
    }

    void openDevice()
    {
        isOpen = ut.open(hmiName, 9600);
        if (!isOpen)
        {
            ROS_ERROR("open serial failed!");
        }else{
            ROS_INFO("open serial success!");
        }
    }

    void reopen(){
        ut.close();
        sleep(0.5);
        openDevice();
    }
    void updateData(diagnostic_msgs::DiagnosticArray &diagnostics){
        robotData = diagnostics;
        double diff = ros::Time::now().toSec() - timestamps;
        if(diff>0.5){
            freshUI();
            timestamps = ros::Time::now().toSec();
        }
    }
    //ui logic
    int getPage();
    bool openStatus();
    void freshPageDevice();
    void freshPageLog();
    void freshPageNode();
    void freshPageOther();
    void freshUI();
    //deal string
    void comCmd(string &cmd, vector<string> vec);
    bool sendCmd(string &cmd);
    void deleteAllMark(string &s, const string &mark);

    void restartAll();
    bool restartXavier();

private:
    diagnostic_msgs::DiagnosticArray robotData;
    typedef std::map<std::string, diagnostic_msgs::DiagnosticStatus> hwMap;
    std::map<std::string, hwMap> hws;
    vector<MonNodeS> monStatus;
    vector<diagnostic_msgs::DiagnosticStatus> logStatus;
    bool isOpen;
    wincomm::Uart ut;
    int page,pageSub,sys0;
    int notDataTime;
    vector<vector<char>> hmiBack;
    double timestamps;
    //ros
    ros::NodeHandle nh;
    ros::ServiceClient client;
    string shellPath,serviceName,hmiName;
};

#endif
