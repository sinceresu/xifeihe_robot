cmake_minimum_required(VERSION 2.8.3)
set(CMAKE_CXX_STANDARD 11)
project(yd_framework)

## Compile as C++11, supported in ROS Kinetic and newer
# add_compile_options(-std=c++11)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
  roscpp rospy message_generation
  std_msgs std_srvs yidamsg
)

## System dependencies are found with CMake's conventions
# find_package(Boost REQUIRED COMPONENTS system)


## Uncomment this if the package has a setup.py. This macro ensures
## modules and global scripts declared therein get installed
## See http://ros.org/doc/api/catkin/html/user_guide/setup_dot_py.html
# catkin_python_setup()

################################################
## Declare ROS messages, services and actions ##
################################################

## To declare and build messages, services or actions from within this
## package, follow these steps:
## * Let MSG_DEP_SET be the set of packages whose message types you use in
##   your messages/services/actions (e.g. std_msgs, actionlib_msgs, ...).
## * In the file package.xml:
##   * add a build_depend tag for "message_generation"
##   * add a build_depend and a exec_depend tag for each package in MSG_DEP_SET
##   * If MSG_DEP_SET isn't empty the following dependency has been pulled in
##     but can be declared for certainty nonetheless:
##     * add a exec_depend tag for "message_runtime"
## * In this file (CMakeLists.txt):
##   * add "message_generation" and every package in MSG_DEP_SET to
##     find_package(catkin REQUIRED COMPONENTS ...)
##   * add "message_runtime" and every package in MSG_DEP_SET to
##     catkin_package(CATKIN_DEPENDS ...)
##   * uncomment the add_*_files sections below as needed
##     and list every .msg/.srv/.action file to be processed
##   * uncomment the generate_messages entry below
##   * add every package in MSG_DEP_SET to generate_messages(DEPENDENCIES ...)

## Generate messages in the 'msg' folder
# add_message_files(
#   FILES
#   Message1.msg
#   Message2.msg
# )

## Generate services in the 'srv' folder
add_service_files(
  FILES
  Settings.srv
  Debug.srv
)

## Generate actions in the 'action' folder
# add_action_files(
#   FILES
#   Action1.action
#   Action2.action
# )

## Generate added messages and services with any dependencies listed here
generate_messages(
  DEPENDENCIES
  std_msgs
)

################################################
## Declare ROS dynamic reconfigure parameters ##
################################################

## To declare and build dynamic reconfigure parameters within this
## package, follow these steps:
## * In the file package.xml:
##   * add a build_depend and a exec_depend tag for "dynamic_reconfigure"
## * In this file (CMakeLists.txt):
##   * add "dynamic_reconfigure" to
##     find_package(catkin REQUIRED COMPONENTS ...)
##   * uncomment the "generate_dynamic_reconfigure_options" section below
##     and list every .cfg file to be processed

## Generate dynamic reconfigure parameters in the 'cfg' folder
# generate_dynamic_reconfigure_options(
#   cfg/DynReconf1.cfg
#   cfg/DynReconf2.cfg
# )

###################################
## catkin specific configuration ##
###################################
## The catkin_package macro generates cmake config files for your package
## Declare things to be passed to dependent projects
## INCLUDE_DIRS: uncomment this if your package contains header files
## LIBRARIES: libraries you create in this project that dependent projects also need
## CATKIN_DEPENDS: catkin_packages dependent projects also need
## DEPENDS: system dependencies of this project that dependent projects also need
catkin_package(
 INCLUDE_DIRS include
 LIBRARIES yd_framework
 CATKIN_DEPENDS roscpp rospy std_msgs message_runtime
#  DEPENDS system_lib
)

###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations
include_directories(
  include
  src/bridge
  src/bridge/yasio
  src/bridge/libusb
  src/bridge/libusb/config_posix
  src/bridge/lynmax4d
  src/debugging
  /usr/include/libdwarf
  ${catkin_INCLUDE_DIRS}
)

add_definitions(-DASIO_STANDALONE -DHAS_SOCKLEN_T#-D_WEBSOCKETPP_CPP11_INTERNAL_
                -DUSE_SYSTEM_LOGGING_FACILITY -DMG_ENABLE_CALLBACK_USERDATA)
set(BRIDGE_SOURCES
    src/bridge/bridge_client.cpp
    src/bridge/bridge_server.cpp
    src/bridge/bridge_enet.cpp

    src/bridge/simple_client.cpp
    src/bridge/simple_enet.cpp
    src/bridge/simple_uv.cpp
    src/bridge/log4z.cpp
    
    src/bridge/lz4/lz4.c
    src/bridge/lz4/lz4frame.c
    src/bridge/lz4/lz4hc.c
    src/bridge/lz4/xxhash.c
    
    src/bridge/enet/callbacks.c
    src/bridge/enet/compress.c
    src/bridge/enet/host.c
    src/bridge/enet/list.c
    src/bridge/enet/packet.c
    src/bridge/enet/peer.c
    src/bridge/enet/protocol.c
    src/bridge/enet/unix.c
    src/bridge/enet/win32.c
    
    src/bridge/libusb/core.c 
    src/bridge/libusb/descriptor.c
    src/bridge/libusb/hotplug.c
    src/bridge/libusb/io.c
    src/bridge/libusb/strerror.c
    src/bridge/libusb/sync.c
    src/bridge/libusb/os/linux_udev.c
    src/bridge/libusb/os/linux_usbfs.c
    src/bridge/libusb/os/poll_posix.c
    src/bridge/libusb/os/threads_posix.c
    
    src/bridge/lynmax4d/arlink.cpp
    src/bridge/lynmax4d/simplefifo.cpp
    src/bridge/lynmax4d/commands.cpp
    src/bridge/lynmax4d/h264bitstream/h264_stream.c
    src/bridge/lynmax4d/h264bitstream/h264_sei.c
    src/bridge/lynmax4d/h264bitstream/h264_nal.c
)

set(DEBUGGING_SOURCES
    src/debugging/daemon.cpp
    
    src/debugging/mongoose.c
    src/debugging/frozen.c
    src/debugging/process.cpp
    src/debugging/process_unix.cpp
    src/debugging/serial/serial.cc
    src/debugging/serial/impl/unix.cc
    src/debugging/serial/impl/list_ports/list_ports_linux.cc
    src/debugging/Uart.cpp
)

## Add cmake target dependencies of the library
## as an example, code may need to be generated before libraries
## either from message generation or dynamic reconfigure
# add_dependencies(${PROJECT_NAME} ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

## Declare a C++ executable
## With catkin_make all packages are built within a single CMake context
## The recommended prefix ensures that target names across packages don't collide
add_executable(${PROJECT_NAME}_resource src/res_center.cpp ${BRIDGE_SOURCES})
add_executable(${PROJECT_NAME}_monitor src/mon_center.cpp ${DEBUGGING_SOURCES}  src/hmi_ui.cpp)

## Rename C++ executable without prefix
## The above recommended prefix causes long target names, the following renames the
## target back to the shorter version for ease of user use
## e.g. "rosrun someones_pkg node" instead of "rosrun someones_pkg someones_pkg_node"
set_target_properties(${PROJECT_NAME}_resource PROPERTIES OUTPUT_NAME resource PREFIX "")
set_target_properties(${PROJECT_NAME}_monitor PROPERTIES OUTPUT_NAME monitor PREFIX "")

## Add cmake target dependencies of the executable
## same as for the library above
add_dependencies(${PROJECT_NAME}_resource
    ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
add_dependencies(${PROJECT_NAME}_monitor
    ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

## Specify libraries to link a library or executable target against
target_link_libraries(${PROJECT_NAME}_resource ${catkin_LIBRARIES} pthread udev dw crypto)
target_link_libraries(${PROJECT_NAME}_monitor ${catkin_LIBRARIES} udev dw)

#############
## Install ##
#############

# all install targets should use catkin DESTINATION variables
# See http://ros.org/doc/api/catkin/html/adv_user_guide/variables.html

## Mark executable scripts (Python etc.) for installation
## in contrast to setup.py, you can choose the destination
# install(PROGRAMS
#   scripts/my_python_script
#   DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
# )

## Mark executables for installation
## See http://docs.ros.org/melodic/api/catkin/html/howto/format1/building_executables.html
# install(TARGETS ${PROJECT_NAME}_node
#   RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
# )

## Mark libraries for installation
## See http://docs.ros.org/melodic/api/catkin/html/howto/format1/building_libraries.html
# install(TARGETS ${PROJECT_NAME}
#   ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
#   LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
#   RUNTIME DESTINATION ${CATKIN_GLOBAL_BIN_DESTINATION}
# )

## Mark cpp header files for installation
# install(DIRECTORY include/${PROJECT_NAME}/
#   DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
#   FILES_MATCHING PATTERN "*.h"
#   PATTERN ".svn" EXCLUDE
# )

## Mark other files for installation (e.g. launch and bag files, etc.)
# install(FILES
#   # myfile1
#   # myfile2
#   DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
# )

install(TARGETS ${PROJECT_NAME}_resource ${PROJECT_NAME}_monitor
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

install(DIRECTORY launch
  DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
)

#############
## Testing ##
#############

## Add gtest based cpp test target and link libraries
# catkin_add_gtest(${PROJECT_NAME}-test test/test_yd_framework.cpp)
# if(TARGET ${PROJECT_NAME}-test)
#   target_link_libraries(${PROJECT_NAME}-test ${PROJECT_NAME})
# endif()

## Add folders to be run by python nosetests
# catkin_add_nosetests(test)
