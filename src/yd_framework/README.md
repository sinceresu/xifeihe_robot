# yd_framework
* resource_center/资源中心     
** 通过websocket把ros内的消息转发给上位机，充当ros和上位机之间的桥
![图片标题](/api/file/getImage?fileId=5ff6c2534374b84124000070)
* monitor_center/监控中心    
** 配合[rosmon](https://github.com/xqms/rosmon)把ros下的节点监控起来
** 配合[glances](https://github.com/nicolargo/glances)把系统工作的计算单元监控起来，包括内存/CPU/网络

#  Depend

* ubuntu(18.04/16.04)
* ros(melodic/kinetic)
* [rosmon](https://github.com/xqms/rosmon)
* [glances](https://github.com/nicolargo/glances)

#  Install﻿
* install rosmon    
**  sudo apt-get install ros-melodic-rosmon    
*  install glances    
** sudo apt-get install glances    

#  Params
```c++
<launch>
  <node pkg="yd_framework" name="monitor_center" type="monitor" output="screen" respawn ="true">
    <param name="shellPath" value="$(find yd_framework)/launch/reboot.sh" />
    <param name="serviceName" value="/yida/xavier/restart" />
    <param name="jsonPath" value="$(find yd_framework)/launch/ydrobot.json" />
    <param name="nvidiaIp" value="192.168.1.250" />
    <param name="hmiName" value="/dev/ttyUSB3" />
  </node>

  <node pkg="yd_framework" name="resource_center" type="resource" output="screen" respawn ="true">
    <param name="remoteIp" value="192.168.1.136" />
  </node>
  
</launch>
```
 - remoteIp    
上位机IP，与resource_center通信    
 - nvidiaIp     
XavierIP，monitor_center除了监控本机性能还监控nvidia的性能
 - hmiName     
机器人上外接触摸屏的串口名（触摸屏直接接在工控机上），如果没有可以不配
 - jsonPath
```json
{
    "resource": {
        "/yida/status_updates": {
            "type": "diagnostic_msgs/DiagnosticArray",
            "queue_size": 16,
            "timeout": 4
        },
        "/robot_pose": {
            "type": "nav_msgs/Odometry",
            "queue_size": 256,
            "timeout": 0.5
        }
    },
    "scene": {
        "/AuxLocator": {
            "heartbeat": true
        },
	    "/resource_center": {
            "heartbeat": true
        },
        "/yd_status_manager": {
            "heartbeat": true
        },
	    "/yd_bg_task": {
            "heartbeat": true
        },
	    "/yd_motion_control": {
            "heartbeat": true
        },
	    "/rtsp_to_h264_node": {
            "heartbeat": true
        },
	    "/infrared_cloudplat": {
            "heartbeat": true
        },
	    "/visible_cloudplat": {
            "heartbeat": true
        },
	    "/cloudplat_sdk_control": {
            "heartbeat": true
        },
	    "/hostpc_cloudplat": {
            "heartbeat": true
        },
	    "/camera_status_node": {
            "heartbeat": true
        },
        "/watch_detect": {
            "heartbeat": true
        },
        "/mon_motion_control": {
            "supervising": true
        },
        "/mon_websocket": {
            "supervising": true
        },
	    "/mon_velodyne": {
            "supervising": true
        }
    }
}

```
-- scene 中配置需要监听节点的rosmon 节点   
supervising true 收集rosmon节点的信息，然后通过 /yd/node_status 发布出去    
"heartbeat": true 收集/yd/heartbeat中对应节点的信息，如果有信息通过 /yd/node_status 发布出去，如果超过一定时间没有消息认为该节点已死，发送"Heartbeat has stopped for a while"
#  Use
### resource_center    
 - 保证机器人中节点正常运行,必须包含rosbridge_server
 - 配置好remoteIp之后，运行程序会与上位机（remoteIp）传输消息
 - 启动上位机的桥接程序，上位机软件就可以通过9090端口获取机器人端的数据

### monitor_center  
 - /yd/node_status  包含三部分信息    
 1.系统运行状况，包含本机和nvidiaIp的主机    
 -- /hw/* 硬件xinxi
2.rosmon 监控信息    
 -- num_nodes rosmon 中信息    
3.节点心跳信息    
 -- heartbeat /yd/heartbeat 中的信息
 
 - /yd/system_state 系统启动情况
 根据jsonPath中scene的配置决定是否启动
 判断依据：supervising为true的节点是否有rosmon的监控信息，heartbeat为true的节点是否有心跳发出
 判断逻辑：开机后进入init模式，如果不满足上述任一条件则进入WAITTING，直到满足所有条件切换为FINISH
 -- 1 INIT 开机
 -- 2 WAITTING 开机等待所有节点启动
 -- 3 FINISH 所有节点启动
#  Run
rosrun param_client client
