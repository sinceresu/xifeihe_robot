#ifndef SIMPLE_UV_HPP
#define SIMPLE_UV_HPP

#include <websocketpp/config/asio_no_tls_client.hpp>
#include <websocketpp/client.hpp>
#include "simple_client.hpp"
#include "yd_framework/timer.hpp"
#include <set>
#include <deque>

#include "rapidjson/rapidjson.h"
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

using namespace std;
using namespace rapidjson;

typedef websocketpp::client<websocketpp::config::asio_client> ClientType;
typedef websocketpp::connection_hdl ConnectionType;
typedef std::map<int,ConnectionType> ConList;

class SimpleUV : public SimpleClient
{
public:
    SimpleUV(const std::string& addr, int udpPort);
    ~SimpleUV();
    virtual void runOnce();
    
    void stop();
    void connect();
    bool _serverStatus;
    bool write(std::string& data);

protected:
    void onFailed(ConnectionType hdl);
    void onOpen(ConnectionType hdl);
    void onClose(ConnectionType hdl);
    void onMessage(ConnectionType hdl, ClientType::message_ptr msg);
    void onTimeout(ConnectionType hdl);
    bool onPing(ConnectionType hdl,std::string msg);

    virtual void pollTimer();
    virtual void handleMessageWS(std::string& msg);
    void clear();
    
    ClientType _ppClient;
    ConnectionType _ppConnection;
    std::string url;

    websocketpp::lib::shared_ptr<websocketpp::lib::thread> m_thread;
    ConList m_connection_list;
    int m_next_id,m_count;
    //timeout
    Timer _timer;
    std::deque<int64_t> _timeout_queue;
};

#endif
