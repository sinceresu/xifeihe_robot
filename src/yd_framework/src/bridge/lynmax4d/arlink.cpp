#include "simplefifo.h"
#include "arlink.h"
#include <sstream>
#include <iostream>
#include <stdarg.h>
#include <libusb.h>
using namespace lynmax4d;

ArlinkThread::ArlinkThread(SimpleFifo* fifoV, SimpleFifo* fifoA, SimpleFifo* fifoU)
    : ThreadBase(), _videoFifo(fifoV), _audioFifo(fifoA), _userFifo(fifoU)
{
    _epControlOut = 0; _epControlIn = 0; _epVideoOut = 0; _epVideoIn = 0; _epAudioOut = 0; _epAudioIn = 0;
    _epUserOut = 0; _epUserIn = 0; _replyRequired = 0; _handle = NULL; _device = NULL;

    int status = libusb_init(&_context);
    if (status != 0)
        printUserLog(LV_WARNING, LYNMAX_APP_NAME " Failed to initialize: %d", status);
}

bool ArlinkThread::initialize()
{
    if (_handle != NULL) return true;
    if (!open(ARLINK_VID, ARLINK_PID)) close();
    return true;
}

void ArlinkThread::destroy()
{
    close();
    libusb_exit(_context);
}

void ArlinkThread::execute(unsigned long& microsecondsToSleep)
{
    static const int s_bufSize = 1024 * 8;
    uint8_t buf[s_bufSize] = "";
    if (!_handle) return;

    static const int s_timeout = 100;
    if (_replyRequired > 0)
    {
        // Command to control endpoint
        static const int s_timeoutCmd = 20;
        int readCmdSize = hidRead(_handle, _epControlIn, (char*)buf, 512, s_timeoutCmd);
        if (readCmdSize > 0) receiveControlData((char*)buf, readCmdSize);
        if (_replyRequired > 0) _replyRequired--;
    }

    // Handle user & audio reading in async mode
    if (_epVideoIn > 0)
    {
        // Handle video reading in sync mode
        int readDataSize = hidRead(_handle, _epVideoIn, (char*)buf, s_bufSize, s_timeout);
        if (readDataSize > 0)
        {
            if (_videoFifo) _videoFifo->write(buf, readDataSize);
        }
        else
            microsecondsToSleep = 0;
    }
    microsecondsToSleep = 1000;
}

bool ArlinkThread::sendRawCommand(uint8_t* cmd, int size, int reqNum)
{
    int actualSize = size + (4 - (size % 4));
    std::vector<char> commandBuffer; commandBuffer.resize(actualSize, 0);
    memcpy(&(commandBuffer[0]), cmd, size);

    if (!_replyRequired && _epControlOut != 0)
    {
        static const int s_timeoutCmd = 10;
        if (hidReadWrite(_handle, _epControlOut, &(commandBuffer[0]), actualSize, s_timeoutCmd) >= 0)
        { _replyRequired = reqNum; return true; }
    }
    return false;
}

const std::vector<uint8_t>& ArlinkThread::getLastRawReply()
{
    while (_replyRequired > 0) { std::this_thread::sleep_for(std::chrono::microseconds(10000)); }
    std::unique_lock<std::mutex> lock(_commonMutex);
    _storedReplyData.swap(_replyData);
    return _storedReplyData;
}

bool ArlinkThread::sendRawVideoData(uint8_t* buffer, int size)
{
    if (buffer && _epVideoOut != 0)
    {
        static const int s_timeoutCmd = 10;
        if (hidReadWrite(_handle, _epVideoOut, (char*)buffer, size, s_timeoutCmd) >= 0)
            return true;
    }
    return false;
}

bool ArlinkThread::prepare(const char* devPath)
{
    if (_handle != NULL) close();
    _device = libusb_get_device2(_context, devPath);
    return _device != NULL;
}

bool ArlinkThread::open(int descriptor)
{
    if (_handle != NULL) close();
    if (_device == NULL) return false;

    int status = libusb_open2(_device, &_handle, descriptor);
    if (status != 0)
        printUserLog(LV_WARNING, LYNMAX_APP_NAME " Failed to open descriptor: %d", status);
    return openDevice();
}

bool ArlinkThread::open(int vid, int pid)
{
    if (_handle != NULL) close();
    _handle = libusb_open_device_with_vid_pid(_context, vid, pid);
    return openDevice();
}

void ArlinkThread::close()
{
    if (_handle != NULL)
    {
        libusb_device* dev = libusb_get_device(_handle);
        struct libusb_config_descriptor* config = NULL;

        int status = libusb_get_active_config_descriptor(dev, &config);
        if (status < 0)
            printUserLog(LV_WARNING, LYNMAX_APP_NAME " Failed to get config descriptor");
        else
        {
            for (int i = 0; i < config->bNumInterfaces; i++)
            {
                if (config->interface[i].num_altsetting == 0) continue;
                libusb_release_interface(_handle, config->interface[i].altsetting[0].bInterfaceNumber);
            }
            libusb_free_config_descriptor(config);
        }
        libusb_close(_handle);
    }

    _handle = NULL; _device = NULL;
    _interfaces.clear();
}

bool ArlinkThread::openDevice()
{
    if (_handle != NULL)
    {
        struct libusb_config_descriptor* config = NULL;
        struct libusb_device_descriptor desc;
        _device = libusb_get_device(_handle);

        int status = libusb_get_device_descriptor(_device, &desc);
        if (status < 0)
        {
            printUserLog(LV_WARNING, LYNMAX_APP_NAME " Failed to get descriptor");
            return false;
        }

        status = libusb_get_active_config_descriptor(_device, &config);
        if (status < 0)
        {
            printUserLog(LV_WARNING, LYNMAX_APP_NAME " Failed to get config descriptor");
            return false;
        }

        status = libusb_set_configuration(_handle, config->bConfigurationValue);
        if (status < 0)
        {
            printUserLog(LV_WARNING, LYNMAX_APP_NAME " Failed to set configuration");
            return false;
        }

        unsigned char interfaceName[256];
        for (int i = 0; i < config->bNumInterfaces; i++)
        {
            const struct libusb_interface_descriptor* interfaceDesc = config->interface[i].altsetting;
            if (config->interface[i].num_altsetting == 0) continue;

            libusb_get_string_descriptor_ascii(_handle, interfaceDesc[0].iInterface, interfaceName, 256);
            status = libusb_claim_interface(_handle, interfaceDesc[0].bInterfaceNumber);
            if (status < 0)
            {
                printUserLog(LV_WARNING, LYNMAX_APP_NAME " Failed to claim interface %d: %s", i, interfaceName);
                continue;
            }

            InterfaceData data{ interfaceDesc[0].bInterfaceNumber, interfaceDesc[0].bInterfaceClass };
            for (int e = 0; e < interfaceDesc[0].bNumEndpoints; ++e)
            {
                int ep = interfaceDesc[0].endpoint[e].bEndpointAddress;
                TransferType t = (TransferType)(interfaceDesc[0].endpoint[e].bmAttributes & 0x3);
                data.endpoints[t] = ep; _endpoints[ep] = t;
            }

            _interfaces[(char*)interfaceName] = data;
            printUserLog(LV_DEBUG, LYNMAX_APP_NAME " Created interface %d: %s; Out = 0x%x, In = 0x%x",
                         i, interfaceName, data.endpoints[TYPE_INTERRUPT], data.endpoints[TYPE_BULK]);
        }
        libusb_free_config_descriptor(config);
    }

    if (!_handle)
        printUserLog(LV_WARNING, LYNMAX_APP_NAME " Failed to open device");
    else
    {
        _epControlOut = _interfaces["ArtosynComm"].endpoints[TYPE_INTERRUPT];
        _epControlIn = _interfaces["ArtosynComm"].endpoints[TYPE_BULK];
        _epVideoOut = _interfaces["ArtosynVideo0"].endpoints[TYPE_INTERRUPT];
        _epVideoIn = _interfaces["ArtosynVideo0"].endpoints[TYPE_BULK];
        _epAudioOut = _interfaces["ArtosynAudio"].endpoints[TYPE_INTERRUPT];
        _epAudioIn = _interfaces["ArtosynAudio"].endpoints[TYPE_BULK];
        _epUserOut = _interfaces["ArtosynCustomer"].endpoints[TYPE_INTERRUPT];
        _epUserIn = _interfaces["ArtosynCustomer"].endpoints[TYPE_BULK];
    }
    return _handle != NULL;
}

bool ArlinkThread::receiveControlData(char* buf, int size)
{
    if (buf[0] == (char)0xFF && buf[1] == (char)0x5A)
    {
        std::unique_lock<std::mutex> lock(_commonMutex);
        _replyRequired = 0; _replyData.resize(size);
        memcpy((char*)&(_replyData[0]), buf, size);
        return true;
    }
    else
    {
        std::stringstream ss;
        for (int i = 0; i < size; ++i) ss << std::hex << (int)((unsigned char)buf[i]) << " ";
        printUserLog(LV_WARNING, LYNMAX_APP_NAME " Unknown received data: %s", ss.str().c_str());
        ss << std::dec; _replyRequired--;
    }
    return false;
}

int ArlinkThread::hidRead(libusb_device_handle* dev, int ep, char* bytes, int size, int timeout)
{
    int result = 0, status = 0;
    if (_endpoints[ep] == TYPE_BULK)
    {
        status = libusb_bulk_transfer(dev, ep, (unsigned char*)bytes, size, &result, timeout);
        if (status < 0 && status != LIBUSB_ERROR_TIMEOUT)
            printUserLog(LV_WARNING, LYNMAX_APP_NAME " Bulk read (%x): error = %d", ep, status);
    }
    else
        printUserLog(LV_WARNING, LYNMAX_APP_NAME " Endpoint (%x) is not a bulk endpoint", ep);
    return result;
}

int ArlinkThread::hidWrite(libusb_device_handle* dev, int ep, char* bytes, int size, int timeout)
{
    int result = 0, status = 0;
    if (_endpoints[ep] == TYPE_INTERRUPT)
    {
        status = libusb_interrupt_transfer(dev, ep, (unsigned char*)bytes, size, &result, timeout);
        if (status < 0) printUserLog(LV_WARNING, LYNMAX_APP_NAME " Interrupt write (%x): error = %d", ep, status);
    }
    else
    {
        status = libusb_bulk_transfer(dev, ep, (unsigned char*)bytes, size, &result, timeout);
        if (status < 0 && status != LIBUSB_ERROR_TIMEOUT)
            printUserLog(LV_WARNING, LYNMAX_APP_NAME " Bulk write (%x): error = %d", ep, status);
    }
    return result;
}

int ArlinkThread::hidReadWrite(libusb_device_handle* dev, int ep, char* bytes, int size, int timeout)
{
    struct libusb_transfer* transfer = libusb_alloc_transfer(0);
    if (_endpoints[ep] == TYPE_INTERRUPT)
    {
        libusb_fill_interrupt_transfer(transfer, dev, ep, (unsigned char*)bytes, size,
                                       &ArlinkThread::stateActivateCallback, this, timeout);
    }
    else
    {
        libusb_fill_bulk_transfer(transfer, dev, ep, (unsigned char*)bytes, size,
                                  &ArlinkThread::stateActivateCallback, this, timeout);
    }

    int status = libusb_submit_transfer(transfer);
    if (status < 0 && status != LIBUSB_ERROR_TIMEOUT)
        printUserLog(LV_WARNING, LYNMAX_APP_NAME " Transfer submit (%x): error = %d", ep, status);
    return status;
}

void ArlinkThread::stateActivateCallback(struct libusb_transfer* transfer)
{
    ArlinkThread* arlink = (ArlinkThread*)transfer->user_data;
    if (arlink->_epAudioIn == transfer->endpoint)
    {
        if (arlink->_audioFifo && transfer->status == LIBUSB_TRANSFER_COMPLETED)
            arlink->_audioFifo->write(transfer->buffer, transfer->actual_length);
    }
    else if (arlink->_epUserIn == transfer->endpoint)
    {
        if (arlink->_userFifo && transfer->status == LIBUSB_TRANSFER_COMPLETED)
            arlink->_userFifo->write(transfer->buffer, transfer->actual_length);
    }
    libusb_free_transfer(transfer);
}
