#ifndef LYNMAX4D_THREADBASE_HPP
#define LYNMAX4D_THREADBASE_HPP

#include <thread>
#include <mutex>
#include <chrono>

namespace lynmax4d
{
    class ThreadBase
    {
    public:
        ThreadBase() : _done(true), _quiting(false) {}
        bool isDone() const { return _done; }

        virtual ~ThreadBase()
        {
            if (!_done) quit(true);
        }

        /** Core threading worker function */
        static void run(ThreadBase* t)
        {
            if (t->initialize())
            {
                do
                {
                    unsigned long microsecondsToSleep = 150000L;
                    t->execute(microsecondsToSleep);
                    if (microsecondsToSleep > 0) std::this_thread::sleep_for(std::chrono::microseconds(microsecondsToSleep));
                    else if (microsecondsToSleep == 0) std::this_thread::yield();
                } while (!t->isDone() && !t->isQuiting());
            }
            t->destroy();
            t->resetFlags();
        }

        /** Start the thread */
        void start()
        {
            _done = false; _quiting = false;
            _thread = std::thread(ThreadBase::run, this);
        }

        /** Quit the thread */
        void quit(bool waitForThreadToExit = true)
        {
            _quiting = true;
            if (waitForThreadToExit)
            {
                while (!isDone()) std::this_thread::yield();
            }
            _thread.join();
        }

        /** Re-implement this for work in initializing/destroying process */
        virtual bool initialize() { return true; }
        virtual void destroy() {}

        /** Re-implement this for every loop's work while the thread is running */
        virtual void execute(unsigned long& microsecondsToSleep) = 0;

    protected:
        void resetFlags() { _done = true; _quiting = false; }
        bool isQuiting() const { return _quiting; }

        std::thread _thread;
        bool _done, _quiting;
    };
}

#endif
