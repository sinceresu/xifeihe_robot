#include "h264bitstream/h264_stream.h"
#include "simplefifo.h"
#include <sstream>
#include <iostream>
#include <stdarg.h>

#ifdef __ANDROID__
#include <android/log.h>
#endif
using namespace lynmax4d;

#if _WIN32
extern "C"
{
    FILE _iob[3] = { *stdin, *stdout, *stderr };
}
#endif

void printUserLog(int level, const char* format, ...)
{
    char buffer[512] = { 0 };
    va_list arg;
    va_start(arg, format);
    vsnprintf(buffer, 511, format, arg);
    va_end(arg);

#ifdef __ANDROID__
    if (level == LV_FATAL) __android_log_print(ANDROID_LOG_ERROR, "Lynmax4D", "%s", buffer);
    else if (level == LV_WARNING) __android_log_print(ANDROID_LOG_WARN, "Lynmax4D", "%s", buffer);
    else __android_log_print(ANDROID_LOG_INFO, "Lynmax4D", "%s", buffer);
#else
    printf("Log-%d: %s\n", level, buffer);
#endif
}

bool SimpleFifo::findNextNAL(unsigned char* buf, int size, int& start, int& end)
{
    if (buf == NULL || size <= 0) return false;
    return find_nal_unit((uint8_t*)buf, size, &start, &end) > 0;
}
