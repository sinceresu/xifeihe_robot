/*
 * @Descripttion: 
 * @version: 
 * @Author: li
 * @Date: 2021-01-19 15:29:11
 * @LastEditors: li
 * @LastEditTime: 2021-03-03 18:20:01
 */
#ifndef SIMPLE_CLIENT_HPP
#define SIMPLE_CLIENT_HPP

#include "simple_websocket/client_ws.hpp"
#include <iostream>
#include <assert.h>
#include <stdio.h>
#include <string>
#include <condition_variable>
#include <mutex>
#include <log4z.h>
#include <thread>

#include "rapidjson/rapidjson.h"
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
using namespace rapidjson;


#define OUTPUT_MESSAGE 0
#define OUTPUT_HEADER 0
#define USE_LZ4 false

using WsClient = SimpleWeb::SocketClient<SimpleWeb::WS>;
using namespace zsummer::log4z;
using namespace std;

class SimpleClient
{
public:
    SimpleClient();
    void process();
    void poll();

    void start(const std::string &addr = "localhost", int port = 9090);
    void send(const std::string& message);

    virtual void pollTimer(){}
    virtual void handleMessageWS(std::string& msg) {}
    virtual void runOnce(){}
    virtual void stop();
    bool _clientStatus;

protected:
    WsClient* _client;
    std::shared_ptr<WsClient::Connection> _connection;
    std::thread* _wsThread;
    std::atomic<int> _messageCount;
    std::atomic<bool> _isReceive;

    bool _isProcessing;
    std::string _addr;
    int _port;
    std::vector<std::string> _messages;

    std::mutex _actionLock;  
    std::condition_variable _actionCondition;

    std::mutex _sendingMutex;
    std::vector<std::string> _sendingList;
};

#endif
