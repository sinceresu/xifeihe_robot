#include "bridge_client.hpp"
using websocketpp::lib::placeholders::_1;
using websocketpp::lib::placeholders::_2;

BridgeClient::BridgeClient() : _isProcessing(false)
{
    _client.set_access_channels(websocketpp::log::alevel::all);
    //_client.clear_access_channels(websocketpp::log::alevel::frame_payload);
    _client.clear_access_channels(websocketpp::log::alevel::all);
    _client.clear_error_channels(websocketpp::log::alevel::all);
    _client.init_asio();
    _client.set_open_handler(websocketpp::lib::bind(&BridgeClient::onOpen, this, ::_1));
    _client.set_close_handler(websocketpp::lib::bind(&BridgeClient::onClose, this, ::_1));
    _client.set_message_handler(websocketpp::lib::bind(&BridgeClient::onMessage, this, ::_1, ::_2));
}

void BridgeClient::process()
{
    _isProcessing = true;
    while (_isProcessing)
    {
        websocketpp::lib::unique_lock<websocketpp::lib::mutex> lock(_actionLock);
        while (_messages.empty() && _isProcessing) _actionCondition.wait(lock);

        std::vector<ClientType::message_ptr> localMsgs;
        localMsgs.swap(_messages);
        lock.unlock();

        for (std::vector<ClientType::message_ptr>::iterator itr = localMsgs.begin();
                itr != localMsgs.end(); ++itr)
        { handleMessageWS(_connection, *itr); }
    }
}

void BridgeClient::start(const std::string& addr, uint16_t port)
{
    std::string uri = addr;
    if (port > 0) uri += ":" + std::to_string(port);

    websocketpp::lib::error_code ec;
    ClientType::connection_ptr c = _client.get_connection(uri, ec);
    if (ec)
        std::cout << "could not connect: " << ec.message() << std::endl;
    else
        _client.connect(c);
}

void BridgeClient::stop()
{
    _isProcessing = false;
    _actionCondition.notify_all();
    _client.stop();
}

void BridgeClient::runOnce()
{
    _client.poll_one();
}

void BridgeClient::onOpen(ConnectionType hdl)
{
    websocketpp::lib::lock_guard<websocketpp::lib::mutex> guard(_actionLock);
    _connection = hdl;
}

void BridgeClient::onClose(ConnectionType hdl)
{
    websocketpp::lib::lock_guard<websocketpp::lib::mutex> guard(_actionLock);
    _connection = hdl;
}

void BridgeClient::onMessage(ConnectionType hdl, ClientType::message_ptr msg)
{
    std::cout << "[BridgeClient] get message from ROS: " << msg->get_payload() << std::endl;
    // Queue message up for sending by processing thread
    {
        websocketpp::lib::lock_guard<websocketpp::lib::mutex> guard(_actionLock);
        _messages.push_back(msg);
    }
    _actionCondition.notify_one();
}

#if 0
int main(int argc, char** argv)
{
    BridgeClient client;
    try
    {
        std::thread thread(websocketpp::lib::bind(&BridgeClient::process, &client));
        client.start("ws://localhost", 9090);
        while (running)
        {
            client.runOnce();
            sleep(20);
        }
        client.stop();
        thread.join();
    }
    catch (websocketpp::exception const & e)
    {
        std::cout << e.what() << std::endl;
    }
    catch (...)
    {
        std::cout << "other exception" << std::endl;
    }
    return 0;
}
#endif
