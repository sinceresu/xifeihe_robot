#include "bridge_message.hpp"
#include "bridge_enet.hpp"

using websocketpp::lib::placeholders::_1;
using websocketpp::lib::placeholders::_2;

BridgeServerEnet::BridgeServerEnet(int udpPort)
:   BridgeServer(), _enetServer(NULL)
{
    if (enet_initialize() == 0)
    {
        ENetAddress address;
        address.host = ENET_HOST_ANY;
        address.port = udpPort;
        
        // Allows 8 connections... is it necessary?
        _enetServer = enet_host_create(&address, 8, 1, 0, 0);
        if (!_enetServer) enet_deinitialize();
    }
    
    if (!_enetServer)
        std::cout << "[BridgeServerEnet] Failed to initialize" << std::endl;
}

BridgeServerEnet::~BridgeServerEnet()
{
    enet_deinitialize();
}

void BridgeServerEnet::runOnce()
{
    std::vector<std::string> localSendingList;
    {
        _sendingMutex.lock();
        localSendingList.swap(_sendingList);
        _sendingMutex.unlock();
    }
    for (unsigned int i = 0; i < localSendingList.size(); ++i)
        write(localSendingList[i], false);
    enet_host_flush(_enetServer);
    
    static char addressName[256];
    ENetEvent event;
    while(enet_host_service(_enetServer, &event, 50) > 0)
    {
        switch (event.type)
        {
        case ENET_EVENT_TYPE_CONNECT:
            {
                ENetAddress remoteAddr = event.peer->address;
                enet_address_get_host_ip(&remoteAddr, addressName, 256);
                _peers.insert(event.peer);
                std::cout << "[BridgeServerEnet] New client: " << addressName
                          << ", total peers = " << _peers.size() << std::endl;
            }
            break;
        case ENET_EVENT_TYPE_RECEIVE:
            {
                char* ptr = (char*)event.packet->data;
                std::vector<char> buffer(ptr, ptr + event.packet->dataLength);
                handleMessageEnet(event, buffer);
                enet_packet_destroy(event.packet);
            }
            break;
        case ENET_EVENT_TYPE_DISCONNECT:
            {
                ENetAddress remoteAddr = event.peer->address;
                enet_address_get_host_ip(&remoteAddr, addressName, 256);
                _peers.erase(event.peer);
                std::cout << "[BridgeServerEnet] Lost client: " << addressName << std::endl;
            }
            break;
        default: break;
        }
    }
    BridgeServer::runOnce();
}

bool BridgeServerEnet::write(const std::string& data, bool toFulsh)
{
    if (!_peers.empty() && _enetServer)
    {
        ENetPacket* packet = enet_packet_create(data.c_str(), data.size(), ENET_PACKET_FLAG_RELIABLE);
        for (std::set<ENetPeer*>::iterator itr = _peers.begin(); itr != _peers.end(); ++itr)
        {
            enet_peer_send(*itr, 0, packet);
        }
        
        if (toFulsh) enet_host_flush(_enetServer);
        return true;
    }
    return false;
}

void BridgeServerEnet::handleMessageEnet(ENetEvent& event, const std::vector<char>& buf)
{
    websocketpp::lib::error_code ec;
    MessageType::ptr msg = BridgeMessager::deserializedData(buf, true);
    websocketpp::lib::lock_guard<websocketpp::lib::mutex> guard(_connectionLock);
    for (std::set<ConnectionType, std::owner_less<ConnectionType> >::iterator itr = _connections.begin();
         itr != _connections.end(); ++itr)
    {
        _server.send(*itr, msg, ec);
        if (ec) std::cout << "[BridgeServerEnet] Send failed: " << ec.message() << std::endl;
    }
}

void BridgeServerEnet::handleMessageWS(ConnectionType& hdl, ServerType::message_ptr& msg)
{
    std::string data = BridgeMessager::serializedData(msg, true);
    _sendingMutex.lock();
    _sendingList.push_back(data);
    _sendingMutex.unlock();
}

BridgeClientEnet::BridgeClientEnet(const std::string& addr, int udpPort)
:   BridgeClient(), _enetClient(NULL), _serverPeer(NULL)
{
    if (enet_initialize() == 0)
    {
        // Supports 1 channel only
        _enetClient = enet_host_create(NULL, 1, 1, 0, 0);
        if (_enetClient)
        {
            enet_address_set_host(&_serverAddress, addr.c_str());
            _serverAddress.port = udpPort;
            reconnect(1000);
        }
        else
            enet_deinitialize();
    }
    
    if (!_enetClient)
        std::cout << "[BridgeClientEnet] Failed to initialize" << std::endl;
}

BridgeClientEnet::~BridgeClientEnet()
{
    if (_serverPeer)
        enet_peer_disconnect(_serverPeer, 0);
    enet_deinitialize();
}

void BridgeClientEnet::reconnect(int confirmTimeout)
{
    if (_enetClient == NULL) return;
    if (_serverPeer != NULL) enet_peer_disconnect(_serverPeer, 0);

    // Connect with 1 channel
    _serverPeer = enet_host_connect(_enetClient, &_serverAddress, 1, 0);
    if (_serverPeer)
    {
        ENetEvent event;
        if (enet_host_service(_enetClient, &event, confirmTimeout) > 0
            && event.type == ENET_EVENT_TYPE_CONNECT)
        {
            std::cout << "[BridgeClientEnet] Connected to server" << std::endl;
        }
        else
        {
            std::cout << "[BridgeClientEnet] Failed to connect to server" << std::endl;
            enet_peer_reset(_serverPeer); _serverPeer = NULL;
        }
    }
    else
        std::cout << "[BridgeClientEnet] Failed to connect to server" << std::endl;
}

void BridgeClientEnet::runOnce()
{
    BridgeClient::runOnce();
    if (!_serverPeer)
    {
        reconnect(1000);
        if (!_serverPeer) return;
    }
    
    std::vector<std::string> localSendingList;
    {
        _sendingMutex.lock();
        localSendingList.swap(_sendingList);
        _sendingMutex.unlock();
    }
    std::cout << "[BridgeClientEnet] send size:" << localSendingList.size() << std::endl;
    for (unsigned int i = 0; i < localSendingList.size(); ++i)
        {
            write(localSendingList[i], false);
        }
    enet_host_flush(_enetClient);
    
    ENetEvent event;
    while(enet_host_service(_enetClient, &event, 50) > 0)
    {
        switch (event.type)
        {
        case ENET_EVENT_TYPE_RECEIVE:
            {
                char* ptr = (char*)event.packet->data;
                std::vector<char> buffer(ptr, ptr + event.packet->dataLength);
                handleMessageEnet(event, buffer);
                enet_packet_destroy(event.packet);
            }
            break;
        case ENET_EVENT_TYPE_DISCONNECT:
            std::cout << "[BridgeClientEnet] Lost server" << std::endl;
            _serverPeer = NULL;
            break;
        default: break;
        }
    }
}

bool BridgeClientEnet::write(const std::string& data, bool toFulsh)
{
    if (_serverPeer && _enetClient)
    {
        ENetPacket* packet = enet_packet_create(data.c_str(), data.size(), ENET_PACKET_FLAG_RELIABLE);
        enet_peer_send(_serverPeer, 0, packet);
        if (toFulsh) enet_host_flush(_enetClient);
        return true;
    }
    return false;
}

void BridgeClientEnet::handleMessageEnet(ENetEvent& event, const std::vector<char>& buf)
{
    websocketpp::lib::error_code ec;
    MessageType::ptr msg = BridgeMessager::deserializedData(buf, true);
    std::cout << "[BridgeClientEnet] New message (" << buf.size() << ") to ROS: " << msg->get_payload() << std::endl;
    _client.send(_connection, msg, ec);
    if (ec) std::cout << "[BridgeClientEnet] Send failed: " << ec.message() << std::endl;
}

void BridgeClientEnet::handleMessageWS(ConnectionType& hdl, ServerType::message_ptr& msg)
{
    std::string data = BridgeMessager::serializedData(msg, true);
    _sendingMutex.lock();
    _sendingList.push_back(data);
    _sendingMutex.unlock();
}
