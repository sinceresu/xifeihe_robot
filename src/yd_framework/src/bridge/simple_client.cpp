#include <simple_client.hpp>
#include <functional>

SimpleClient::SimpleClient():_isProcessing(true)
{
    //start log4z
    ILog4zManager::getRef().start();
    LOGI("stream input *** " << "SimpleClient start" << " *** ");
}

void SimpleClient::send(const std::string &message)
{
    if(_connection==nullptr) return;
    _connection->send(message);
}

void SimpleClient::process()
{
    //_isProcessing = true;
    while (_isProcessing)
    {
        std::unique_lock<std::mutex> lock(_actionLock);
        while (_messages.empty() && _isProcessing)
            _actionCondition.wait(lock);

        std::vector<std::string> localMsgs;
        localMsgs.swap(_messages);
        lock.unlock();

        for (std::vector<std::string>::iterator itr = localMsgs.begin();
             itr != localMsgs.end(); ++itr)
        {
            handleMessageWS(*itr);
        }
    }
}


void SimpleClient::poll()
{
    int _resetCcount = 0;
    sleep(10);
    while (_isProcessing)
    {
        if(!_isReceive && _messageCount==0){
            LOGI("[SimpleClient] 12001 no established.");
        }else if(_isReceive && _messageCount==0){
            LOGI("[SimpleClient] get message 0 from ros per second. total time:"<< _resetCcount);
            _resetCcount++;
        }else{
            LOGD("[SimpleClient] get message "<< _messageCount <<" from ros per second ");
            _messageCount = 0;
            _resetCcount = 0;
        }
        if(!_clientStatus){
            LOGI("[SimpleClient] rosbridge reconnect ... ");
            _client->stop();
            _wsThread->detach();
            _wsThread = NULL;
            start(_addr,_port);
            sleep(5);
        }
        sleep(1);
        /*
        if(!_isReceive && _messageCount==0){
            LOGI("[SimpleClient] no message from ros");
        }else if(_isReceive && _messageCount==0){
            LOGI("[SimpleClient] get message "<< _messageCount <<" from ros per second ");
            _resetCcount++;
            if(_resetCcount>60){
                LOGI("[SimpleClient] no message from ros, reset connect!");
                _connection==nullptr;
                _clientStatus = false;
                _resetCcount = 0;
            }
        }else{
            LOGD("[SimpleClient] get message "<< _messageCount <<" from ros per second ");
            _messageCount = 0;
            _resetCcount = 0;
        }
        if(!_clientStatus){
            LOGI("[SimpleClient] rosbridge reconnect ... ");
            _client->stop();
            _wsThread->detach();
            _wsThread = NULL;
            sleep(5);
            start(_addr,_port);
        }
        //pollTimer();
        sleep(1);
        */
    }
}

void SimpleClient::start(const std::string &addr, int port)
{
    _isReceive = false;
    _messageCount = 0;
    _addr = addr;
    _port = port;
    _client = new WsClient(_addr + ":" + std::to_string(_port));

    _client->on_message = [this](std::shared_ptr<WsClient::Connection> connection, std::shared_ptr<WsClient::InMessage> message) {
        std::string msg = message->string();
        _messageCount++;
        //std::cout << "[SimpleClient] Message received: \"" << msg << "\"" << std::endl;
        Document document;
        ParseResult result = document.Parse(msg.c_str());
        if (result.IsError())
        {
            std::cout << "JSON parse error:" << result.Code() << " offset:" << result.Offset() << std::endl;
            return;
        }
        if (!document.IsObject())
        {
            std::cout << "message is not object " << msg << std::endl;
            return;
        }
        #if OUTPUT_HEADER
        Value::ConstMemberIterator itr = document.FindMember("op");
        if (itr != document.MemberEnd()){
            if (strcmp(itr->value.GetString(), "publish") == 0)
            {
                std::string topic = document["topic"].GetString();
                if(strcmp(topic.c_str(),"/detect_result")==0){
                    LOGD("get message from ros " << msg);
                }
                /*
                if (document.HasMember("msg"))
                {
                    Value &v = document["msg"];
                    if (v.HasMember("header") && v["header"].IsObject())
                    {
                        auto header = v["header"].GetObject();
                        if (header["stamp"].IsObject())
                        {
                            auto stamp = header["stamp"].GetObject();
                            if (stamp["secs"].IsNumber())
                            {
                                LOGD("get ros message secs " << stamp["secs"].GetUint64());
                            }
                        }
                    }
                }
                */
            }
        }
        #endif
        {
            std::unique_lock<std::mutex> lock(_actionLock);
            _messages.push_back(msg);
        }
        _actionCondition.notify_one();
    };

    _client->on_open = [this](std::shared_ptr<WsClient::Connection> connection) {
        _connection = connection;
        _clientStatus = true;
        LOGI("[SimpleClient] Opened connection");
        //std::cout << "[SimpleClient] Opened connection" << std::endl;
    };

    _client->on_close = [this](std::shared_ptr<WsClient::Connection> /*connection*/, int status, const std::string & /*reason*/) {
        _connection = nullptr;
        _clientStatus = false; 
        LOGI("[SimpleClient] Closed connection with status code "<< status);
        //std::cout << "[SimpleClient] Closed connection with status code " << status << std::endl;
    };

    _client->on_error = [this](std::shared_ptr<WsClient::Connection> /*connection*/, const SimpleWeb::error_code &ec) {
        _connection = nullptr;
        _clientStatus = false;  
        LOGE("[SimpleClient] Error: " << ec.value() << ", error message: " << ec.message());
    };

    //_client->start();
    _wsThread = new std::thread(std::bind(&WsClient::start, _client));
}

void SimpleClient::stop()
{
    LOGI("[SimpleClient] stop");
    _isProcessing = false;
    _client->stop();
    delete _client;
    _wsThread->join();
    delete _wsThread;
}