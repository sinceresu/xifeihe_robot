#ifndef HTTP_CLIENT_HPP
#define HTTP_CLIENT_HPP

extern "C"
{
    #include "mongoose.h"
    #include "frozen.h"
}
#include <iostream>
#include <sstream>
#include <fstream>
#include <map>

struct HttpGetClient
{
    static struct mg_mgr requester;
    static std::string resultData;
    static int finished;
    
    static void callback(struct mg_connection* connection, int ev, void* p, void* user)
    {
        struct http_message* message = (struct http_message*)p;
        if (ev == MG_EV_CONNECT && *(int*)message != 0) finished = 1;
        else if (ev == MG_EV_CLOSE) finished = 1;
        else if (ev == MG_EV_HTTP_REPLY)
        {
            resultData = std::string(message->body.p, message->body.len);
            connection->flags |= MG_F_CLOSE_IMMEDIATELY;
            finished = 1;
        }
    }
    
    static void get(const char* address, bool restful)
    {
        if (finished > 0) mg_mgr_free(&requester);
        mg_mgr_init(&requester, NULL);
        resultData = ""; finished = 0;
        
        struct mg_connection* nc = mg_connect_http(&requester, callback, NULL, address, NULL, NULL);
        if (restful) mg_set_protocol_http_websocket(nc);
    }
    
    static void post(const char* address, const char* type, const char* postData)
    {
        if (finished > 0) mg_mgr_free(&requester);
        mg_mgr_init(&requester, NULL);
        resultData = ""; finished = 0;
        
        struct mg_connection* nc = mg_connect_http(
            &requester, callback, NULL, address,
            (type ? (std::string("Content-Type: ") + type + std::string("\r\n")).c_str() : NULL), postData);
    }
    
    static bool frame()
    {
        if (!finished)
        {
            mg_mgr_poll(&requester, 1000);
            return false;
        }
        return true;
    }
};

#endif

