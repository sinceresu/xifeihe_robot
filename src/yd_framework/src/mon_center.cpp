#include "debugging/http_client.hpp"
#include <yd_framework/mon_center.hpp>
#include <yd_framework/hmi_ui.hpp>
#include <fstream>
#include <iostream>

///////// Backward tracing begin /////////
#define BACKWARD_HAS_DW 1
#include <backward.hpp>
backward::SignalHandling sh;
static void trace()
{
    backward::StackTrace st;
    st.load_here(32);
    backward::Printer p;
    p.print(st);
}
///////// Backward tracing end /////////

static void addDiagnosticKeyValue(diagnostic_msgs::DiagnosticStatus &s,
                                  const std::string &k, const std::string &v)
{
    diagnostic_msgs::KeyValue kv;
    kv.key = k;
    kv.value = v;
    s.values.push_back(kv);
}

template <typename T>
void addKeyValueFromJson(MonitoringCenter::KeyValueList &kvList, rapidjson::Value &json,
                         const std::string &name, const std::string &keyName)
{
    if (!json.HasMember(name.c_str()))
        return;
    rapidjson::Value &member = json[name.c_str()];

    if (member.Is<T>())
    {
        diagnostic_msgs::KeyValue kv;
        kv.key = keyName;
        kv.value = std::to_string(member.Get<T>());
        kvList.push_back(kv);
    }
}

void MonitoringCenter::diagnosticCallback(const diagnostic_msgs::DiagnosticArrayConstPtr &ptr)
{
    if (!ros::ok())
        return;
    if (ptr->status.empty())
        return;
    const diagnostic_msgs::DiagnosticStatus &ds = ptr->status[0];
    //deal /topic topic
    std::string nodeName = ds.name;
    std::string head("/");
    bool isHead = nodeName.compare(0, head.size(), head) == 0;
    if(!isHead) nodeName = "/"+nodeName;

    std::map<std::string, NodeState>::iterator itr = _regNodes.find(nodeName);
    if (itr == _regNodes.end())
    {
        ROS_WARN("Unknown heartbeat from %s", nodeName.c_str());
        return;
    }

    NodeState &state = itr->second;
    if (state.needHeartbeat)
    {
        state.timestamps.push(ptr->header.stamp.toSec());
        while (state.timestamps.size() > 10)
            state.timestamps.pop();
    }
    state.log = *ptr;
}

void MonitoringCenter::supervisingCallback(const rosmon_msgs::StateConstPtr &ptr, const std::string &m)
{
    std::map<std::string, NodeState>::iterator itr = _regNodes.find(m);
    if (itr == _regNodes.end())
    {
        ROS_WARN("Unknown monitoring state from %s", m.c_str());
        return;
    }

    NodeState &state = itr->second;
    state.log.header.stamp = ptr->header.stamp;
    state.log.status.clear();

    diagnostic_msgs::DiagnosticStatus s;
    s.name = m;
    s.level = 0;
    s.values.clear();
    addDiagnosticKeyValue(s, "num_nodes", std::to_string(ptr->nodes.size()));
    for (unsigned int i = 0; i < ptr->nodes.size(); ++i)
    {
        const rosmon_msgs::NodeState &ns = ptr->nodes[i];
        if (ns.state == 0)
            s.level = 1;
        else if (ns.state != 1)
            s.level = 2;

        std::string postfix = std::to_string(i);
        addDiagnosticKeyValue(s, "node" + postfix, ns.name);
        addDiagnosticKeyValue(s, "state" + postfix, std::to_string(ns.state));
        addDiagnosticKeyValue(s, "restarted" + postfix, std::to_string(ns.restart_count));
        addDiagnosticKeyValue(s, "cpu_system" + postfix, std::to_string(ns.system_load));
        addDiagnosticKeyValue(s, "cpu_user" + postfix, std::to_string(ns.user_load));
        addDiagnosticKeyValue(s, "memory" + postfix, std::to_string(ns.memory));
    }
    state.log.status.push_back(s);
}

void MonitoringCenter::glanceTimerCallback(const ros::TimerEvent &event)
{
    if (_glanceCurrentKey.empty())
    {
        // Start new glance
        _glanceCurrentKey = _glanceResults.begin()->first;
        _glanceRequesting = true;
        //HttpGetClient::get((_glanceUriPrefix + _glanceCurrentKey).c_str(), true);
        std::map<std::string, std::string>::iterator itr = _glanceIpKey.find(_glanceCurrentKey);
        if (itr == _glanceIpKey.end())
        {
            ROS_WARN("Unknown key from %s", _glanceCurrentKey.c_str());
        }else{
            string url = itr->second;
            HttpGetClient::get(url.c_str(), true);
        }
    }
    else
    {
        if (!_glanceRequesting)
        {
            // http-get already returns, get the value
            std::map<std::string, KeyValueList>::iterator itr = _glanceResults.begin();
            for (; itr != _glanceResults.end(); ++itr)
            {
                if (itr->first == _glanceCurrentKey)
                {
                    std::string result = HttpGetClient::resultData;
                    if (result.empty())
                        break;

                    rapidjson::Document doc;
                    if (doc.Parse(result.c_str(), result.size()).HasParseError())
                    {
                        ROS_WARN("Failed to parse json data: %s:", result.c_str());
                        if (result.find("404") != std::string::npos)
                            _glanceUriPrefix = "http://127.0.0.1:61208/api/3/";
                        break;
                    }

                    KeyValueList &keyValues = itr->second;
                    keyValues.clear();
                    string::size_type cpu_idx = _glanceCurrentKey.find("cpu");
                    string::size_type mem_idx = _glanceCurrentKey.find("mem");
                    string::size_type net_idx = _glanceCurrentKey.find("network");
                    if(cpu_idx!=string::npos){
                        addKeyValueFromJson<int>(keyValues, doc, "interrupts", "irq");
                        addKeyValueFromJson<int>(keyValues, doc, "soft_interrupts", "softirq");
                        addKeyValueFromJson<int>(keyValues, doc, "ctx_switches", "ctx_switches");
                        addKeyValueFromJson<int>(keyValues, doc, "syscalls", "syscalls");
                        addKeyValueFromJson<double>(keyValues, doc, "system", "cpu_system");
                        addKeyValueFromJson<double>(keyValues, doc, "user", "cpu_user");
                        addKeyValueFromJson<double>(keyValues, doc, "total", "total");
                    }else if(mem_idx!=string::npos){
                        addKeyValueFromJson<long>(keyValues, doc, "used", "mem_used");
                        addKeyValueFromJson<long>(keyValues, doc, "total", "mem_total");
                        addKeyValueFromJson<double>(keyValues, doc, "percent", "percent");
                    }else if(net_idx!=string::npos){
                        diagnostic_msgs::KeyValue kv;
                        kv.key = "num_interfaces";
                        kv.value = std::to_string(doc.Size());
                        keyValues.push_back(kv);

                        for (unsigned int i = 0; i < doc.Size(); i++)
                        {
                            std::string postfix = std::to_string(i);
                            if (doc[i].HasMember("interface_name"))
                            {
                                kv.key = "interface" + postfix;
                                kv.value = doc[i]["interface_name"].GetString();
                                keyValues.push_back(kv);
                            }
                            addKeyValueFromJson<int>(keyValues, doc[i], "rx", "rx" + postfix);
                            addKeyValueFromJson<int>(keyValues, doc[i], "tx", "tx" + postfix);
                        }
                    }
                    /*
                    if (_glanceCurrentKey == "cpu")
                    {
                        addKeyValueFromJson<int>(keyValues, doc, "interrupts", "irq");
                        addKeyValueFromJson<int>(keyValues, doc, "soft_interrupts", "softirq");
                        addKeyValueFromJson<int>(keyValues, doc, "ctx_switches", "ctx_switches");
                        addKeyValueFromJson<int>(keyValues, doc, "syscalls", "syscalls");
                        addKeyValueFromJson<double>(keyValues, doc, "system", "cpu_system");
                        addKeyValueFromJson<double>(keyValues, doc, "user", "cpu_user");
                    }
                    else if (_glanceCurrentKey == "mem")
                    {
                        addKeyValueFromJson<int>(keyValues, doc, "used", "mem_used");
                        addKeyValueFromJson<int>(keyValues, doc, "total", "mem_total");
                    }
                    else if (_glanceCurrentKey == "network" && doc.IsArray())
                    {
                        diagnostic_msgs::KeyValue kv;
                        kv.key = "num_interfaces";
                        kv.value = std::to_string(doc.Size());
                        keyValues.push_back(kv);

                        for (unsigned int i = 0; i < doc.Size(); i++)
                        {
                            std::string postfix = std::to_string(i);
                            if (doc[i].HasMember("interface_name"))
                            {
                                kv.key = "interface" + postfix;
                                kv.value = doc[i]["interface_name"].GetString();
                                keyValues.push_back(kv);
                            }
                            addKeyValueFromJson<bool>(keyValues, doc[i],
                                                      "is_up", "running" + postfix);
                            addKeyValueFromJson<int>(keyValues, doc[i],
                                                     "speed", "speed" + postfix);
                            addKeyValueFromJson<int>(keyValues, doc[i], "rx", "rx" + postfix);
                            addKeyValueFromJson<int>(keyValues, doc[i], "tx", "tx" + postfix);
                        }
                    }
                    */
                    break;
                }
            }

            // Start new glance
            if (itr != _glanceResults.end())
                ++itr;
            if (itr != _glanceResults.end())
                _glanceCurrentKey = itr->first;
            else
                _glanceCurrentKey = _glanceResults.begin()->first;
            _glanceRequesting = true;
            //HttpGetClient::get((_glanceUriPrefix + _glanceCurrentKey).c_str(), true);
            std::map<std::string, std::string>::iterator ipItr = _glanceIpKey.find(_glanceCurrentKey);
            if (ipItr == _glanceIpKey.end())
            {
                ROS_WARN("Unknown key from %s", _glanceCurrentKey.c_str());
            }else{
                string url = ipItr->second;
                HttpGetClient::get(url.c_str(), true);
            }
        }
        else
        {
        } // Wait...
    }
}

bool MonitoringCenter::debugCallback(yd_framework::DebugRequest &req,
                                     yd_framework::DebugResponse &response)
{
    // TODO
    return true;
}

bool MonitoringCenter::loadResource(const std::string &jsonFile)
{
    double current = ros::Time::now().toSec() - 10;
    _regNodes.clear();

    // Always include the resource center
    // NodeState resStateData;
    // resStateData.name = "/resource_center";
    // resStateData.timestamps.push(current);
    // resStateData.needHeartbeat = true;
    // _regNodes[resStateData.name] = resStateData;

    // Start reading JSON configure file
    std::ifstream in(jsonFile.c_str());
    if (!in)
    {
        ROS_WARN("Failed to open %s", jsonFile.c_str());
        return false;
    }

    rapidjson::IStreamWrapper isw(in);
    rapidjson::EncodedInputStream<rapidjson::UTF8<>, rapidjson::IStreamWrapper> eis(isw);
    rapidjson::Document doc;
    if (doc.ParseStream(eis).HasParseError())
    {
        ROS_ERROR("Failed to parse json in %s:", jsonFile.c_str());
        return false;
    }

    if (!doc.HasMember("scene"))
    {
        ROS_WARN("No 'scene' field in %s:", jsonFile.c_str());
        return false;
    }

    const rapidjson::Value &scene = doc["scene"];
    if (!scene.IsObject())
    {
        ROS_WARN("Invalid 'scene' field in %s:", jsonFile.c_str());
        return false;
    }

    // Traverse 'scene' field and register all nodes
    for (rapidjson::Value::ConstMemberIterator itr = scene.MemberBegin();
         itr != scene.MemberEnd(); ++itr)
    {
        //deal /topic topic
        std::string nodeName = itr->name.GetString();
        std::string head("/");
        bool isHead = nodeName.compare(0, head.size(), head) == 0;
        if(!isHead) nodeName = "/"+nodeName;

        NodeState stateData;
        stateData.name = nodeName;
        stateData.timestamps.push(current);

        const rapidjson::Value &msg = itr->value;
        if (!msg.IsObject())
        {
            ROS_WARN("Invalid node field: %s:", stateData.name.c_str());
            continue;
        }

        for (rapidjson::Value::ConstMemberIterator itr2 = msg.MemberBegin();
             itr2 != msg.MemberEnd(); ++itr2)
        {
            std::string key = itr2->name.GetString();
            if (key == "heartbeat")
            {
                if (!itr2->value.IsBool())
                    continue;
                else
                    stateData.needHeartbeat = itr2->value.GetBool();
            }
            else if (key == "supervising")
            {
                if (!itr2->value.IsBool())
                    continue;
                else
                    stateData.supervising = itr2->value.GetBool();
            }
            else if (key == "restartType")
            {
                
                if (!itr2->value.IsInt()) continue;
                else{
                    stateData.restartType = itr2->value.GetInt();
                }
            }
        }
        if (stateData.supervising)
        {
            // Subscribe to rosmon's state message for supervising
            stateData.supervisingState = _node.subscribe<rosmon_msgs::State>(
                stateData.name + "/state", 8, boost::bind(&MonitoringCenter::supervisingCallback, this, _1, stateData.name));
        }
        _regNodes[nodeName] = stateData;
        ROS_INFO("Monitoring node %s", stateData.name.c_str());
    }

    in.close();
    return true;
}

void MonitoringCenter::update()
{
    // Wait for any HTTP Get response from Glances
    if (HttpGetClient::frame())
        _glanceRequesting = false;
    // Check system status
    statusUpdate();
    // Check node heartbeats
    ros::Time current = ros::Time::now();
    for (std::map<std::string, NodeState>::iterator itr = _regNodes.begin();
         itr != _regNodes.end(); ++itr)
    {
        NodeState &state = itr->second;
        if (!state.needHeartbeat || state.timestamps.empty())
            continue;

        double diff = current.toSec() - state.timestamps.back();
        if (diff > 2.0)
        {
            state.log.header.stamp = current;
            if (state.log.status.size() != 1)
            {
                state.log.status.clear();
                state.log.status.push_back(diagnostic_msgs::DiagnosticStatus());
            }

            diagnostic_msgs::DiagnosticStatus &s = state.log.status[0];
            s.name = itr->first;
            s.level = 1; // WARN
            s.message = "Heartbeat has stopped for a while";
            s.values.clear();
            addDiagnosticKeyValue(s, "stopped_time", std::to_string(diff));
            // TODO: check rosmon and try to restart, ...
            if(current.toSec()-_last_restart_time > 120){
                if(state.restartType==1){
                    //restart all 
                    hmi.restartAll();
                    _last_restart_time = current.toSec();
                }else if(state.restartType==2){
                    //restart xavier
                    hmi.restartXavier();
                    _last_restart_time = current.toSec();
                }
            }
        }
    }

    // Update information to display and publisher
    diagnostic_msgs::DiagnosticArray diagnostics;
    diagnostics.header.stamp = current;
    {
        // Record glances status
        for (std::map<std::string, KeyValueList>::const_iterator itr = _glanceResults.begin();
             itr != _glanceResults.end(); ++itr)
        {
            diagnostic_msgs::DiagnosticStatus s;
            s.name = "/hw/" + itr->first;
            s.level = 0; // TODO: check...

            const KeyValueList &keyValues = itr->second;
            for (unsigned int i = 0; i < keyValues.size(); ++i)
                s.values.push_back(keyValues[i]);
            diagnostics.status.push_back(s);
        }

        // Record rosmon states & node heartbeats
        for (std::map<std::string, NodeState>::const_iterator itr = _regNodes.begin();
             itr != _regNodes.end(); ++itr)
        {
            const NodeState &state = itr->second;
            if (!state.log.status.empty())
                diagnostics.status.push_back(state.log.status[0]);
        }
    }
    //update hmi status
    {
        diagnostic_msgs::DiagnosticStatus s;
        s.name = "monitor_center";
        s.hardware_id = "1";
        diagnostic_msgs::KeyValue dv;
        if(hmi.openStatus()){
            s.level = 0;
            s.message = "ok";
            dv.key = "error_code";
            dv.value = "10300";
        }else{
            s.level = 2;
            s.message = "hmi open failed";
            dv.key = "error_code";
            dv.value = "10303";
        }
        s.values.push_back(dv);
        diagnostics.status.push_back(s);
    }

    _nodeStatus.publish(diagnostics);
    hmi.updateData(diagnostics);
}

void MonitoringCenter::statusUpdate(){
    // 1.检查心跳      /yd/heartbeat
    // 2.检查监控节点   mon_XXX
    diagnostic_msgs::DiagnosticStatus systemStatus;
    if(_status==SystemState::FINISH){
        systemStatus.level = _status;
        _systemStatus.publish(systemStatus);
        return;
    }
    ros::Time current = ros::Time::now();
    bool isFinish = true;
    _status = SystemState::WAITTING;
    //std::cout << "node num:"<< _regNodes.size() << std::endl;
    for (std::map<std::string, NodeState>::iterator itr = _regNodes.begin();
         itr != _regNodes.end(); ++itr)
    {
        NodeState &state = itr->second;
        diagnostic_msgs::DiagnosticStatus &ds = systemStatus;
        if(state.supervising && state.log.status.empty()){
            isFinish = false;
            addDiagnosticKeyValue(ds, itr->first, "rosmon no message");
        }
        if (!state.needHeartbeat || state.timestamps.empty())
            continue;
        double diff = current.toSec() - state.timestamps.back();
        if (diff > 2.0)
        {
            isFinish = false;
            addDiagnosticKeyValue(ds, itr->first, "Heartbeat has stopped for a while");
        }
    }
    if(isFinish)
    {
        _status = SystemState::FINISH;
        ros::param::set("/yd/system_state",(int)_status);
    }
    systemStatus.level = _status;
    _systemStatus.publish(systemStatus);
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "monitor_center");
    MonitoringCenter monCenter;
    //read from launch
    monCenter.loadResource();
    //monCenter.loadResource((argc > 1) ? argv[1] : "config.json");

    MonitoringCenter::SupporterMap publishers, subscribers, services;
    monCenter.systemState(publishers, subscribers, services);

    for (MonitoringCenter::SupporterMap::iterator itr = publishers.begin();
         itr != publishers.end(); ++itr)
    {
        printf("Published topic: %s\n", itr->first.c_str());
        for (unsigned int i = 0; i < itr->second.size(); ++i)
            printf("  * %s\n", itr->second[i].c_str());
    }
    for (MonitoringCenter::SupporterMap::iterator itr = subscribers.begin();
         itr != subscribers.end(); ++itr)
    {
        printf("Subscribed topic: %s\n", itr->first.c_str());
        for (unsigned int i = 0; i < itr->second.size(); ++i)
            printf("  * %s\n", itr->second[i].c_str());
    }
    for (MonitoringCenter::SupporterMap::iterator itr = services.begin();
         itr != services.end(); ++itr)
    {
        printf("Published service: %s\n", itr->first.c_str());
        for (unsigned int i = 0; i < itr->second.size(); ++i)
            printf("  * %s\n", itr->second[i].c_str());
    }

    ros::Rate rate(10);
    while (ros::ok())
    {
        monCenter.update();
        ros::spinOnce();
        rate.sleep();
    }
    return 0;
}
