#ifndef Characters_HPP
#define Characters_HPP

#pragma once
#include <iostream>
#include <string>
#include <vector>

namespace utils_common_libs
{

    std::string &TrimString(std::string &src);

    std::string &ReplaceString(std::string &strSource, const std::string &strOld, const std::string &strNew);

    std::vector<std::string> &SplitString(const std::string &src, const std::string &separator, std::vector<std::string> &dest);

}

#endif