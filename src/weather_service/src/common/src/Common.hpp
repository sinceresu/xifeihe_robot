#ifndef Common_HPP
#define Common_HPP

#pragma once
#include "Characters.hpp"
#include "DateTime.hpp"
#include "Global.hpp"
#include "Base64.hpp"
#include "Utils.hpp"
#include "EnumString.h"

namespace utils_common_libs
{
    void init();
    void shutdown();
    bool is_ok();
}

#endif