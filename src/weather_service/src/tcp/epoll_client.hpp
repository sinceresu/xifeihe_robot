#ifndef epoll_client_HPP
#define epoll_client_HPP

#pragma once
#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <stdio.h>
#include <cstring>
#include <stdlib.h>
#include <sys/epoll.h>
#include <sys/socket.h>
#include <unistd.h>
#include <cassert>
#include <string>
#include <iostream>
#include <memory>
#include <functional>
#include <thread>
#include <mutex>

namespace epoll_client
{
    static const uint32_t kEpollWaitTime = 30; // epoll wait timeout 20 ms
    static const uint32_t kMaxEvents = 1;      // epoll wait max size

    typedef struct _msg_Packet_t
    {
    public:
        _msg_Packet_t()
            : msg(""), size(0) {}
        _msg_Packet_t(const std::string &_msg, const int &_size)
            : msg(_msg), size(_size) {}
        _msg_Packet_t(int _fd, const std::string &_msg, const int &_size)
            : fd(_fd), msg(_msg), size{_size} {}

        int fd{-1};      // meaning socket
        std::string msg; // real binary content
        int size;        // real binary size
    } msg_Packet_t;

    typedef std::function<void(const msg_Packet_t &_data)> RecvCallback;

    class CEpollClient
    {
    public:
        CEpollClient(const std::string &_server_ip, const uint16_t &_server_port);
        ~CEpollClient();

    private:
        std::string server_ip_{""};                     // tcp server ip
        uint16_t server_port_{0};                       // tcp server port
        int32_t socket_fd_{-1};                         // socket fd
        int32_t epoll_fd_{-1};                          // epoll fd
        bool b_open_{false};                            // connect status
        bool loop_flag_{true};                          // if loop_flag is false, then exit the epoll loop
        std::shared_ptr<std::thread> th_loop_{nullptr}; // one loop per thread(call epoll_wait in loop)
        RecvCallback recv_callback_{nullptr};           // callback when received
        std::mutex read_mtx_;
        std::mutex write_mtx_;

    protected:
        // create epoll instance using epoll_create and return a fd of epoll
        int32_t CreateEpoll();
        // add/modify/remove a item(socket/fd) in epoll instance(rbtree), for this example, just add a socket to epoll rbtree
        int32_t UpdateEpoll(int _e_fd, int _op, int _s_fd, int _events);
        // one loop per thread, call epoll_wait and return ready socket(readable,writeable,error...)
        void LoopEpoll();
        // create a socket fd using api socket()
        int32_t CreateSocket();
        // connect to server
        int32_t ConnectSocket(int32_t _s_fd);
        // handle tcp socket readable event(read())
        int32_t OnSocketRead(int32_t _s_fd, msg_Packet_t &_data);
        // handle tcp socket writeable event(write())
        int32_t OnSocketWrite(int32_t _s_fd, const msg_Packet_t &_data);

    public:
        // start tcp client
        bool Start();
        // stop tcp client
        bool Stop();
        bool IsOpen();
        // read packet
        int32_t ReadData(msg_Packet_t &_data);
        // write packet
        int32_t WriteData(const msg_Packet_t &_data);
        // register a callback when packet received
        void RegisterOnRecvCallback(RecvCallback _callback);
        void UnRegisterOnRecvCallback();
    };

}

#endif