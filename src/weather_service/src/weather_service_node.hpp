#ifndef weather_service_node_HPP
#define weather_service_node_HPP

#pragma once
#include <queue>
#include <mutex>
#include "ros/ros.h"
#include "ros/package.h"
#include "std_msgs/String.h"
#include "weather_service_msgs/KeyValue.h"
#include "weather_service_msgs/WeatherData.h"
#include "yidamsg/weather.h"
#include "weather_service.hpp"

namespace weather_service
{
    class CWeatherServiceNode
    {
    public:
        typedef struct _NodeOptions
        {
            int robot_id;
        } NodeOptions;

    public:
        CWeatherServiceNode();
        ~CWeatherServiceNode();

    private:
        NodeOptions node_options_;

    private:
        ros::NodeHandle nh_;
        ros::NodeHandle private_nh_;

        void LaunchParam();
        void LaunchPublishers();

        ros::Publisher weather_publisher_;
        ros::Publisher weather_pub;

    public:
        void Run();

    private:
        CWeatherService weather_service;
        CWeatherService::ParamOptions_t weather_param;
        void data_handle(void *p);
    };
}

#endif