#ifndef weather_service_HPP
#define weather_service_HPP

#pragma once
#include <iostream>
#include <vector>
#include <map>
#include <queue>
#include <thread>
#include <functional>
#include <math.h>
#include <mutex>
// #include "Common.hpp"
#include "common/src/Common.hpp"
#include "tcp/socket_client.hpp"

namespace weather_service
{

    typedef struct _Key_Value_t
    {
        std::string key;
        std::string value;
        _Key_Value_t(std::string _key, std::string _value) : key(_key), value(_value) {}
    } Key_Value_t;

    class CWeatherService
    {
    public:
        typedef struct _ParamOptions_t
        {
            std::string service_ip;
            int service_port;
            int device_slave;
        } ParamOptions_t;

    public:
        CWeatherService();
        ~CWeatherService();

    private:
        ParamOptions_t param_;
        std::shared_ptr<socket_client::CSocketClient> modbus_ptr_;
        int slave_;

        std::mutex cmd_mtx;

    public:
        void SetParam(ParamOptions_t _param);
        int Connect(std::string _ip, int _port, int _slave);
        int Disconnect();
        void Run();
        bool QueryData(std::vector<Key_Value_t> &_data);
    };
}

#endif
