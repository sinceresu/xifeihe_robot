#include "weather_service_node.hpp"

namespace weather_service
{

#define weather_data_pub "/weather/data"
#define weather_info_pub "/weather_info"

    CWeatherServiceNode::CWeatherServiceNode()
    {
        LaunchParam();
        LaunchPublishers();
    }

    CWeatherServiceNode::~CWeatherServiceNode()
    {
    }

    void CWeatherServiceNode::LaunchParam()
    {
        ros::param::get("~robot_id", node_options_.robot_id);

        ros::param::get("~service_ip", weather_param.service_ip);
        ros::param::get("~service_port", weather_param.service_port);
        ros::param::get("~device_slave", weather_param.device_slave);
    }

    void CWeatherServiceNode::LaunchPublishers()
    {
        weather_publisher_ = nh_.advertise<weather_service_msgs::WeatherData>(weather_data_pub, 1);
        weather_pub = nh_.advertise<yidamsg::weather>(weather_info_pub, 1);
    }

    void CWeatherServiceNode::Run()
    {
        weather_service.Connect(weather_param.service_ip, weather_param.service_port, weather_param.device_slave);
        std::thread data_Thread = std::thread(std::bind(&CWeatherServiceNode::data_handle, this, nullptr));
        data_Thread.detach();
    }

    void CWeatherServiceNode::data_handle(void *p)
    {
        ros::Rate r(1);
        while (ros::ok())
        {
            std::vector<Key_Value_t> weather_datas;
            if (weather_service.QueryData(weather_datas))
            {
                weather_service_msgs::WeatherData msg;
                msg.header.frame_id = "weather_" + std::to_string(node_options_.robot_id);
                msg.header.stamp = ros::Time::now();
                for (std::vector<Key_Value_t>::iterator data = weather_datas.begin(); data != weather_datas.end(); data++)
                {
                    weather_service_msgs::KeyValue item;
                    item.key = data->key;
                    item.value = data->value;
                    msg.values.push_back(item);
                }
                weather_publisher_.publish(msg);
                //
                yidamsg::weather msg_weather;
                msg_weather.robot_id = node_options_.robot_id;
                for (std::vector<Key_Value_t>::iterator data = weather_datas.begin(); data != weather_datas.end(); data++)
                {
                    if (data->key == "humidity")
                    {
                        msg_weather.humidity = atof(data->value.c_str());
                    }
                    if (data->key == "temperature")
                    {
                        msg_weather.temperature = atof(data->value.c_str());
                    }
                }
                weather_pub.publish(msg_weather);
            }

            ros::spinOnce();
            r.sleep();
        }
    }

}