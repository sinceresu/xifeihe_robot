#ifndef base64_HPP
#define base64_HPP

#pragma once
#include <string>
#include <iostream>
#include <string>
#include <sstream>

#include <boost/archive/iterators/base64_from_binary.hpp>
#include <boost/archive/iterators/binary_from_base64.hpp>
#include <boost/archive/iterators/transform_width.hpp>

using namespace boost::archive::iterators;

std::string base64_encode(unsigned char const *, unsigned int len);
std::string base64_decode(std::string const &s);

bool Base64Encode(const std::string &inPut, std::string *outPut);
bool Base64Decode(const std::string &inPut, std::string *outPut);

std::string Encode(const char *Data, int DataByte);
std::string Decode(const char *Data, int DataByte, int &OutByte);

#endif