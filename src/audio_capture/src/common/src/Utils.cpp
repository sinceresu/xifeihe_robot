#include "Utils.hpp"

CRate::CRate(float _hz)
    : hz(_hz), max_hz(1000000), status(true)
{
    begin_time_stamp = get_time_stamp();
    end_time_stamp = get_time_stamp();
    //
    b_exit_sig = false;
    if (load_counter == 0)
    {
        /* code */
        signal(SIGINT, sig_handler);
    }
    load_counter++;
}

CRate::~CRate()
{
    status = false;
    //
    load_counter--;
    if (load_counter == 0)
    {
        /* code */
    }
}

unsigned int CRate::load_counter = 0;
bool CRate::b_exit_sig = false;
void CRate::sig_handler(int sig)
{
    if (sig == SIGINT)
    {
        b_exit_sig = true;
        printf("Interrupt signal (\"%d\") received. \r\n", sig);
    }
}

time_t CRate::get_time_stamp()
{
    /* std::chrono::milliseconds ms = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch());
    std::time_t timeStamp = ms.count(); */
    //
    std::chrono::time_point<std::chrono::system_clock, std::chrono::milliseconds> tp = std::chrono::time_point_cast<std::chrono::milliseconds>(std::chrono::system_clock::now());
    time_t timeStamp = tp.time_since_epoch().count();
    return timeStamp;
}

bool CRate::is_ok()
{
    // return status;
    if (!b_exit_sig && status)
    {
        return true;
    }
    return false;
}

void CRate::sleep_wait()
{
    // struct timespec tn;
    // clock_gettime(CLOCK_REALTIME, &tn);

    end_time_stamp = get_time_stamp();
    long diff_time_stamp = end_time_stamp - begin_time_stamp;
    long wait_time = (max_hz / hz) - diff_time_stamp;
    wait_time = wait_time > 0 ? wait_time : 1;
    usleep(wait_time);
    begin_time_stamp = get_time_stamp();
}