#include <stdio.h>
#include <csignal>
#include "glog/logging.h"
#include "audio_capture_node.hpp"

void sig_handler(int sig)
{
    if (sig == SIGINT)
    {
        exit(0);
    }
}

namespace audio_capture
{
    void run()
    {
        CAudioCaptureNode::NodeOptions node_options;
        ros::param::get("~audio_device", node_options.audio_device);

        ros::param::get("~audio_capture_pub", node_options.audio_capture_pub);
        ros::param::get("~audio_capture_service", node_options.audio_capture_service);

        CAudioCaptureNode audio_capture_node(node_options);

        ROS_INFO("audio_capture node started.");

        ros::spin();
        // ros::Rate loop_rate(1);
        // while (ros::ok())
        // {
        //     ros::spinOnce();
        //     loop_rate.sleep();
        // }
    }
}

int main(int argc, char **argv)
{
    signal(SIGINT, sig_handler);

    setlocale(LC_CTYPE, "zh_CN.utf8");
    setlocale(LC_ALL, "");

    std::string run_path = "";
    char *buffer;
    if ((buffer = getcwd(NULL, 0)) == NULL)
    {
        run_path = ".";
    }
    else
    {
        run_path = buffer;
        free(buffer);
    }

    std::string pack_path = "";
    pack_path = ros::package::getPath("audio_capture");

    ros::init(argc, argv, "audio_capture");
    ros::Time::init();

    audio_capture::run();

    ros::shutdown();

    return 0;
}
