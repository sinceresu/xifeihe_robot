#ifndef audio_capture_HPP
#define audio_capture_HPP

#pragma once
#include <queue>
#include <mutex>
#include <thread>
#include <vector>
#include <map>
#include <functional>
#include <signal.h>
#include <stdio.h>
#include <malloc.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <fcntl.h>
#include <ctype.h>
#include <errno.h>
#include <limits.h>
#include <time.h>
#include <locale.h>
#include <assert.h>
#include <termios.h>
#include <poll.h>
#include <sys/uio.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <endian.h>
#include "audio/gettext.h"
#include "audio/formats.h"
#include "audio/version.h"
#include <alsa/asoundlib.h>
#include "common/src/Common.hpp"

namespace audio_capture
{

    typedef struct _audio_option_t
    {
        char *opt;
        char *value;
        int v_flag;
    } audio_option_t;

#define ABS(a) (a) < 0 ? -(a) : (a)

#ifdef SND_CHMAP_API_VERSION
#define CONFIG_SUPPORT_CHMAP 1
#endif

#ifndef LLONG_MAX
#define LLONG_MAX 9223372036854775807LL
#endif

#ifndef le16toh
#include <asm/byteorder.h>
#define le16toh(x) __le16_to_cpu(x)
#define be16toh(x) __be16_to_cpu(x)
#define le32toh(x) __le32_to_cpu(x)
#define be32toh(x) __be32_to_cpu(x)
#endif

#if __GNUC__ > 2 || (__GNUC__ == 2 && __GNUC_MINOR__ >= 95)
#define error(...)                                                           \
    do                                                                       \
    {                                                                        \
        fprintf(stderr, "%s: %s:%d: ", command.c_str(), __func__, __LINE__); \
        fprintf(stderr, __VA_ARGS__);                                        \
        putc('\n', stderr);                                                  \
    } while (0)
#else
#define error(args...)                                               \
    do                                                               \
    {                                                                \
        fprintf(stderr, "%s: %s:%d: ", command, __func__, __LINE__); \
        fprintf(stderr, ##args);                                     \
        putc('\n', stderr);                                          \
    } while (0)
#endif

#ifndef timersub
#define timersub(a, b, result)                           \
    do                                                   \
    {                                                    \
        (result)->tv_sec = (a)->tv_sec - (b)->tv_sec;    \
        (result)->tv_usec = (a)->tv_usec - (b)->tv_usec; \
        if ((result)->tv_usec < 0)                       \
        {                                                \
            --(result)->tv_sec;                          \
            (result)->tv_usec += 1000000;                \
        }                                                \
    } while (0)
#endif

#ifndef timermsub
#define timermsub(a, b, result)                          \
    do                                                   \
    {                                                    \
        (result)->tv_sec = (a)->tv_sec - (b)->tv_sec;    \
        (result)->tv_nsec = (a)->tv_nsec - (b)->tv_nsec; \
        if ((result)->tv_nsec < 0)                       \
        {                                                \
            --(result)->tv_sec;                          \
            (result)->tv_nsec += 1000000000L;            \
        }                                                \
    } while (0)
#endif

#define DEFAULT_FORMAT SND_PCM_FORMAT_U8
#define DEFAULT_SPEED 8000

#define FORMAT_DEFAULT -1
#define FORMAT_RAW 0
#define FORMAT_VOC 1
#define FORMAT_WAVE 2
#define FORMAT_AU 3

    enum
    {
        VUMETER_NONE,
        VUMETER_MONO,
        VUMETER_STEREO
    };

    enum
    {
        OPT_VERSION = 1,
        OPT_PERIOD_SIZE,
        OPT_BUFFER_SIZE,
        OPT_DISABLE_RESAMPLE,
        OPT_DISABLE_CHANNELS,
        OPT_DISABLE_FORMAT,
        OPT_DISABLE_SOFTVOL,
        OPT_TEST_POSITION,
        OPT_TEST_COEF,
        OPT_TEST_NOWAIT,
        OPT_MAX_FILE_TIME,
        OPT_PROCESS_ID_FILE,
        OPT_USE_STRFTIME,
        OPT_DUMP_HWPARAMS,
        OPT_FATAL_ERRORS,
    };

    typedef struct _hw_params_t
    {
        snd_pcm_format_t format;
        unsigned int channels;
        unsigned int rate;
    } hw_params_t;

    typedef struct _fmt_capture_t
    {
        // std::function<void(int fd, size_t count)> start;
        // std::function<void(int fd)> end;
        std::function<bool(int fd, size_t count)> start;
        std::function<bool(int fd)> end;
        std::string what;
        long long max_filesize;
    } fmt_capture_t;

    static snd_pcm_sframes_t (*readi_func_)(snd_pcm_t *handle, void *buffer, snd_pcm_uframes_t size);
    static snd_pcm_sframes_t (*writei_func_)(snd_pcm_t *handle, const void *buffer, snd_pcm_uframes_t size);
    static snd_pcm_sframes_t (*readn_func_)(snd_pcm_t *handle, void **bufs, snd_pcm_uframes_t size);
    static snd_pcm_sframes_t (*writen_func_)(snd_pcm_t *handle, void **bufs, snd_pcm_uframes_t size);

    class CAudioCapture
    {
    public:
        CAudioCapture();
        ~CAudioCapture();

    private:
        bool b_run = false;
        bool b_stop = false;
        int argc_n = 0;
        char **argv_p = NULL;
        // std::vector<char *> dicts;
        std::map<std::string, audio_option_t> params_dict;
        // std::string command = "aplay";
        std::string command = "arecord";
        std::vector<std::string> files;
        snd_pcm_t *handle = NULL;
        hw_params_t hwparams, rhwparams;
        int timelimit = 10;
        int sampleslimit = 0;
        int quiet_mode = 0;
        int file_type = FORMAT_DEFAULT;
        int open_mode = 0;
        // snd_pcm_stream_t stream = SND_PCM_STREAM_PLAYBACK;
        snd_pcm_stream_t stream = SND_PCM_STREAM_CAPTURE;
        int mmap_flag = 0;
        int interleaved = 1;
        int nonblock = 0;
        volatile sig_atomic_t in_aborting = 0;
        u_char *audiobuf = NULL;
        snd_pcm_uframes_t chunk_size = 0;
        unsigned period_time = 0;
        unsigned buffer_time = 0;
        snd_pcm_uframes_t period_frames = 0;
        snd_pcm_uframes_t buffer_frames = 0;
        int avail_min = -1;
        int start_delay = 0;
        int stop_delay = 0;
        int monotonic = 0;
        int interactive = 0;
        int can_pause = 0;
        int fatal_errors = 0;
        int verbose = 0;
        int vumeter = VUMETER_NONE;
        int buffer_pos = 0;
        size_t significant_bits_per_sample, bits_per_sample, bits_per_frame;
        size_t chunk_bytes;
        int test_position = 0;
        int test_coef = 8;
        int test_nowait = 0;
        snd_output_t *log;
        long long max_file_size = 0;
        int max_file_time = 0;
        int use_strftime = 0;
        volatile int recycle_capture_file = 0;
        long term_c_lflag = -1;
        int dump_hw_params = 0;

        int fd = -1;
        off64_t pbrec_count = LLONG_MAX, fdcount;
        int vocmajor, vocminor;

        char *pidfile_name = NULL;
        FILE *pidf = NULL;
        int pidfile_written = 0;

#ifdef CONFIG_SUPPORT_CHMAP
        snd_pcm_chmap_t *channel_map = NULL; /* chmap to override */
        unsigned int *hw_map = NULL;         /* chmap to follow */
#endif

        fmt_capture_t fmt_rec_table[4] = {
            {NULL, NULL, ("raw data"), LLONG_MAX},
            {std::bind(&CAudioCapture::begin_voc, this, std::placeholders::_1, std::placeholders::_2), std::bind(&CAudioCapture::end_voc, this, std::placeholders::_1), ("VOC"), 16000000LL},
            /* FIXME: can WAV handle exactly 2GB or less than it? */
            {std::bind(&CAudioCapture::begin_wave, this, std::placeholders::_1, std::placeholders::_2), std::bind(&CAudioCapture::end_wave, this, std::placeholders::_1), ("WAVE"), 2147483648LL},
            {std::bind(&CAudioCapture::begin_au, this, std::placeholders::_1, std::placeholders::_2), std::bind(&CAudioCapture::end_au, this, std::placeholders::_1), ("Sparc Audio"), LLONG_MAX}};

    private:
        void usage(char *command);
        void version(void);
        void device_list(void);
        void pcm_list(void);

        // void prg_exit(int code);
        int prg_exit(int code);
        ssize_t xwrite(int fd, const void *buf, size_t count);
        long parse_long(const char *str, int *err);
        // int init(int argc, char *argv[]);
        int init();
        int unint();
        ssize_t safe_read(int fd, void *buf, size_t count);
        int test_vocfile(void *buffer);
        size_t test_wavefile_read(int fd, u_char *buffer, size_t *size, size_t reqsize, int line);
        ssize_t test_wavefile(int fd, u_char *_buffer, size_t size);
        int test_au(int fd, void *buffer);
        void show_available_sample_formats(snd_pcm_hw_params_t *params);
#ifdef CONFIG_SUPPORT_CHMAP
        int setup_chmap(void);
#else
#define setup_chmap() 0
#endif
        // void set_params(void);
        bool set_params(void);
        void init_stdin(void);
        void done_stdin(void);
        char wait_for_input(void);
        // void do_pause(void);
        int do_pause(void);
        // void check_stdin(void);
        int check_stdin(void);
        // void xrun(void);
        bool xrun(void);
        // void suspend(void);
        bool suspend(void);
        void print_vu_meter_mono(int perc, int maxperc);
        void print_vu_meter_stereo(int *perc, int *maxperc);
        void print_vu_meter(signed int *perc, signed int *maxperc);
        void compute_max_peak(u_char *data, size_t samples);
        void do_test_position(void);
#ifdef CONFIG_SUPPORT_CHMAP
        // u_char *remap_data(u_char *data, size_t count);
        u_char *remap_data(u_char *data, size_t count, bool &b_exit);
        // u_char **remap_datav(u_char **data, size_t count);
        u_char **remap_datav(u_char **data, size_t count, bool &b_exit);
#else
// #define remap_data(data, count) (data)
// #define remap_datav(data, count) (data)
#define remap_data(data, count, b_exit) (data)
#define remap_datav(data, count, b_exit) (data)
#endif

        ssize_t pcm_write(u_char *data, size_t count);
        ssize_t pcm_writev(u_char **data, unsigned int channels, size_t count);
        ssize_t pcm_read(u_char *data, size_t rcount);
        ssize_t pcm_readv(u_char **data, unsigned int channels, size_t rcount);
        ssize_t voc_pcm_write(u_char *data, size_t count);
        // void voc_write_silence(unsigned x);
        int voc_write_silence(unsigned x);
        // void voc_pcm_flush(void);
        int voc_pcm_flush(void);
        // void voc_play(int fd, int ofs, char *name);
        int voc_play(int fd, int ofs, char *name);
        void init_raw_data(void);
        off64_t calc_count(void);
        // void begin_voc(int fd, size_t cnt);
        bool begin_voc(int fd, size_t cnt);
        // void begin_wave(int fd, size_t cnt);
        bool begin_wave(int fd, size_t cnt);
        // void begin_au(int fd, size_t cnt);
        bool begin_au(int fd, size_t cnt);
        // void end_voc(int fd);
        bool end_voc(int fd);
        // void end_wave(int fd);
        bool end_wave(int fd);
        // void end_au(int fd);
        bool end_au(int fd);
        void header(int rtype, char *name);
        // void playback_go(int fd, size_t loaded, off64_t count, int rtype, char *name);
        int playback_go(int fd, size_t loaded, off64_t count, int rtype, char *name);

        int read_header(int *loaded, int header_size);
        int playback_au(char *name, int *loaded);
        int playback_voc(char *name, int *loaded);
        int playback_wave(char *name, int *loaded);
        int playback_raw(char *name, int *loaded);
        // void playback(char *name);
        bool playback(char *name);
        size_t mystrftime(char *s, size_t max, const char *userformat,
                          const struct tm *tm, const int filenumber);
        int new_capture_file(char *name, char *namebuf, size_t namelen,
                             int filecount);
        int create_path(const char *path);
        int safe_open(const char *name);
        // void capture(char *orig_name);
        bool capture(char *orig_name);
        // void playbackv_go(int *fds, unsigned int channels, size_t loaded, off64_t count, int rtype, char **names);
        int playbackv_go(int *fds, unsigned int channels, size_t loaded, off64_t count, int rtype, char **names);
        // void capturev_go(int *fds, unsigned int channels, off64_t count, int rtype, char **names);
        int capturev_go(int *fds, unsigned int channels, off64_t count, int rtype, char **names);
        // void playbackv(char **names, unsigned int count);
        bool playbackv(char **names, unsigned int count);
        // void capturev(char **names, unsigned int count);
        bool capturev(char **names, unsigned int count);

    public:
        void audio_setopt_dict(const char *opt, const char *value, const int v_flag = 0);
        bool is_run();
        int start();
        int stop();
    };

}

#endif