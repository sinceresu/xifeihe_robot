#ifndef audio_capture_node_HPP
#define audio_capture_node_HPP

#pragma once
#include <queue>
#include <mutex>
#include "ros/ros.h"
#include "ros/package.h"
#include "std_msgs/String.h"
#include "audio_capture_msgs/CaptureResult.h"
#include "audio_capture_msgs/AudioCapture.h"
#include "audio_capture.hpp"
#include "mp3/audio_mp3_capture.hpp"

namespace audio_capture
{

    class CAudioCaptureNode
    {
    public:
        typedef struct _NodeOptions
        {
            std::string audio_device;

            std::string audio_capture_pub;
            std::string audio_capture_service;
        } NodeOptions;

    public:
        CAudioCaptureNode(NodeOptions _node_options);
        ~CAudioCaptureNode();

    private:
        NodeOptions node_options;

        ros::NodeHandle nh;
        ros::NodeHandle private_nh;

        void LaunchPublishers();
        void LaunchService();

        ros::Publisher audio_capture_publisher;
        void publisher_audio_capture(std::string _file, int _result, std::string _msg);

        ros::ServiceServer audio_capture_service;
        bool audio_capture_service_Callback(audio_capture_msgs::AudioCapture::Request &req,
                                            audio_capture_msgs::AudioCapture::Response &res);

    private:
        CAudioCapture audio_capture;
        int audio_arecord(std::string _file, std::string _file_type, unsigned int _time);
        void audio_arecord_fun(void *p, std::string _file, std::string _file_type, unsigned int _time);
    };

}

#endif