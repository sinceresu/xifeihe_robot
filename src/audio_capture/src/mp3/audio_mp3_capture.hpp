/*
 *  https://www.alsa-project.org/alsa-doc/alsa-lib/_2test_2latency_8c-example.html#a30
 */

#ifndef audio_mp3_capture_HPP
#define audio_mp3_capture_HPP

#pragma once
#include <queue>
#include <mutex>
#include <thread>
#include <functional>
#include <condition_variable>
#include <alsa/asoundlib.h>
#include <lame/lame.h>

namespace audio_capture
{

    typedef std::function<int(int, std::string)> CaptureResultCallback;

    class CAudioMp3Capture
    {
    public:
        CAudioMp3Capture(std::string _device);
        ~CAudioMp3Capture();

    private:
        snd_pcm_t *capture_handle;
        //
        std::string cdevice = "default"; /* default, hw:0,0, plughw:0,0 */
        int block = 1;                   /* block mode */
        // stream
        snd_pcm_format_t format = SND_PCM_FORMAT_S16_LE;
        unsigned int resample = 1;
        int channels = 2;
        unsigned int rate = 44100;
        int periods = 2;
        // bufsize
        int buffer_size = 0;    /* auto */
        int period_size = 0;    /* auto */
        int latency_min = 32;   /* in frames / 2 */
        int latency_max = 2048; /* in frames / 2 */
        int latency = 0;
        //
        int loop_sec = 10; /* seconds */
        int use_poll = 0;
        unsigned long loop_limit;
        //
        snd_output_t *output = NULL;
        //
        char *buffer = NULL;
        short *pcm_buffer = NULL;
        //
        int b_loop_capture = false;

        int alsa_setparams_stream(snd_pcm_t *handle, snd_pcm_hw_params_t *params, const char *id);
        int alsa_setparams_bufsize(snd_pcm_t *handle, snd_pcm_hw_params_t *params, snd_pcm_hw_params_t *tparams, snd_pcm_uframes_t bufsize, const char *id);
        int alsa_setparams_set(snd_pcm_t *handle, snd_pcm_hw_params_t *params, snd_pcm_sw_params_t *swparams, const char *id);
        int alsa_setparams(snd_pcm_t *handle, int *bufsize);
        void showstat(snd_pcm_t *handle, size_t frames);
        void showlatency(size_t latency);
        void showinmax(size_t in_max);
        void gettimestamp(snd_pcm_t *handle, snd_timestamp_t *timestamp);
        void setscheduler(void);
        long timediff(snd_timestamp_t t1, snd_timestamp_t t2);
        // long readbuf(snd_pcm_t *handle, char *buf, long len, size_t *frames, size_t *max);
        long readbuf(snd_pcm_t *handle, short *buf, long len, size_t *frames, size_t *max);
        void capture_loop(void *p);
        int alsa_open(std::string _device);
        int alsa_close();

    private:
        lame_global_flags *gfp;
        int mp3_size;
        unsigned char *mp3_buffer = NULL;

        int lame_load();
        int lame_release();

    private:
        static bool b_initialize;
        CaptureResultCallback result_callback;
        FILE *pFile;

        int init();
        int unInit();

    public:
        void RegisterOnResultCallback(CaptureResultCallback _callback);
        int start_capture(std::string _file, int _time);
        int stop_capture();
    };

}

#endif