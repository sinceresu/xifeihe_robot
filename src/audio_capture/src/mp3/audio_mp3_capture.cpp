#include "audio_mp3_capture.hpp"

namespace audio_capture
{

    CAudioMp3Capture::CAudioMp3Capture(std::string _device)
        : cdevice(_device)
    {
    }

    CAudioMp3Capture::~CAudioMp3Capture()
    {
    }

    bool CAudioMp3Capture::b_initialize = false;

    int CAudioMp3Capture::alsa_setparams_stream(snd_pcm_t *handle, snd_pcm_hw_params_t *params, const char *id)
    {
        int err;
        unsigned int rrate;

        err = snd_pcm_hw_params_any(handle, params);
        if (err < 0)
        {
            printf("Broken configuration for %s PCM: no configurations available: %s\n", snd_strerror(err), id);
            return err;
        }
        err = snd_pcm_hw_params_set_rate_resample(handle, params, resample);
        if (err < 0)
        {
            printf("Resample setup failed for %s (val %i): %s\n", id, resample, snd_strerror(err));
            return err;
        }
        err = snd_pcm_hw_params_set_access(handle, params, SND_PCM_ACCESS_RW_INTERLEAVED);
        if (err < 0)
        {
            printf("Access type not available for %s: %s\n", id, snd_strerror(err));
            return err;
        }
        err = snd_pcm_hw_params_set_format(handle, params, format);
        if (err < 0)
        {
            printf("Sample format not available for %s: %s\n", id, snd_strerror(err));
            return err;
        }
        err = snd_pcm_hw_params_set_channels(handle, params, channels);
        if (err < 0)
        {
            printf("Channels count (%i) not available for %s: %s\n", channels, id, snd_strerror(err));
            return err;
        }
        rrate = rate;
        err = snd_pcm_hw_params_set_rate_near(handle, params, &rrate, 0);
        if (err < 0)
        {
            printf("Rate %iHz not available for %s: %s\n", rate, id, snd_strerror(err));
            return err;
        }
        if ((int)rrate != rate)
        {
            printf("Rate doesn't match (requested %iHz, get %iHz)\n", rate, err);
            return -EINVAL;
        }
        return 0;
    }

    int CAudioMp3Capture::alsa_setparams_bufsize(snd_pcm_t *handle, snd_pcm_hw_params_t *params, snd_pcm_hw_params_t *tparams, snd_pcm_uframes_t bufsize, const char *id)
    {
        int err;
        snd_pcm_uframes_t periodsize;

        snd_pcm_hw_params_copy(params, tparams);
        periodsize = bufsize * 2;
        err = snd_pcm_hw_params_set_buffer_size_near(handle, params, &periodsize);
        if (err < 0)
        {
            printf("Unable to set buffer size %li for %s: %s\n", bufsize * 2, id, snd_strerror(err));
            return err;
        }
        if (period_size > 0)
            periodsize = period_size;
        else
            periodsize /= 2;
        err = snd_pcm_hw_params_set_period_size_near(handle, params, &periodsize, 0);
        if (err < 0)
        {
            printf("Unable to set period size %li for %s: %s\n", periodsize, id, snd_strerror(err));
            return err;
        }
        return 0;
    }

    int CAudioMp3Capture::alsa_setparams_set(snd_pcm_t *handle, snd_pcm_hw_params_t *params, snd_pcm_sw_params_t *swparams, const char *id)
    {
        int err;
        snd_pcm_uframes_t val;

        err = snd_pcm_hw_params(handle, params);
        if (err < 0)
        {
            printf("Unable to set hw params for %s: %s\n", id, snd_strerror(err));
            return err;
        }
        err = snd_pcm_sw_params_current(handle, swparams);
        if (err < 0)
        {
            printf("Unable to determine current swparams for %s: %s\n", id, snd_strerror(err));
            return err;
        }
        err = snd_pcm_sw_params_set_start_threshold(handle, swparams, 0x7fffffff);
        if (err < 0)
        {
            printf("Unable to set start threshold mode for %s: %s\n", id, snd_strerror(err));
            return err;
        }
        if (!block)
            val = 4;
        else
            snd_pcm_hw_params_get_period_size(params, &val, NULL);
        err = snd_pcm_sw_params_set_avail_min(handle, swparams, val);
        if (err < 0)
        {
            printf("Unable to set avail min for %s: %s\n", id, snd_strerror(err));
            return err;
        }
        err = snd_pcm_sw_params(handle, swparams);
        if (err < 0)
        {
            printf("Unable to set sw params for %s: %s\n", id, snd_strerror(err));
            return err;
        }
        return 0;
    }

    int CAudioMp3Capture::alsa_setparams(snd_pcm_t *handle, int *bufsize)
    {
        int err, last_bufsize = *bufsize;
        snd_pcm_hw_params_t *ct_params; /* templates with rate, format and channels */
        snd_pcm_hw_params_t *c_params;
        snd_pcm_sw_params_t *c_swparams;
        snd_pcm_uframes_t c_size, c_psize;
        unsigned int c_time;
        unsigned int val;

        snd_pcm_hw_params_alloca(&ct_params);
        snd_pcm_hw_params_alloca(&c_params);
        snd_pcm_sw_params_alloca(&c_swparams);

        // if ((err = snd_pcm_hw_params_malloc(&ct_params)) < 0)
        // {
        //     fprintf(stderr, "cannot allocate hardware parameter structure (%s)\n",
        //             snd_strerror(err));
        //     return -1;
        // }
        // if ((err = snd_pcm_hw_params_malloc(&c_params)) < 0)
        // {
        //     fprintf(stderr, "cannot allocate hardware parameter structure (%s)\n",
        //             snd_strerror(err));
        //     return -1;
        // }
        // if ((err = snd_pcm_sw_params_malloc(&c_swparams)) < 0)
        // {
        //     fprintf(stderr, "cannot allocate hardware parameter structure (%s)\n",
        //             snd_strerror(err));
        //     return -1;
        // }

        if ((err = alsa_setparams_stream(handle, ct_params, "capture")) < 0)
        {
            // snd_pcm_hw_params_free(ct_params);
            printf("Unable to set parameters for playback stream: %s\n", snd_strerror(err));
            return -1;
        }

        if (buffer_size > 0)
        {
            *bufsize = buffer_size;
            goto __set_it;
        }

    __again:
        if (buffer_size > 0)
            return -1;
        if (last_bufsize == *bufsize)
            *bufsize += 4;
        last_bufsize = *bufsize;
        if (*bufsize > latency_max)
            return -1;

    __set_it:
        if ((err = alsa_setparams_bufsize(handle, c_params, ct_params, *bufsize, "capture")) < 0)
        {
            // snd_pcm_hw_params_free(c_params);
            printf("Unable to set parameters for playback stream: %s\n", snd_strerror(err));
            return -1;
        }

        // snd_pcm_hw_params_get_period_size(c_params, &c_psize, NULL);
        // if (c_psize > (unsigned int)*bufsize)
        //     *bufsize = c_psize;
        // snd_pcm_hw_params_get_period_time(c_params, &c_time, NULL);

        // snd_pcm_hw_params_get_buffer_size(c_params, &c_size);
        // if (c_psize * 2 < c_size)
        // {
        //     snd_pcm_hw_params_get_periods_min(c_params, &val, NULL);
        //     if (val > 2)
        //     {
        //         printf("capture device does not support 2 periods per buffer\n");
        //         return -1;
        //     }
        //     goto __again;
        // }

        if ((err = alsa_setparams_set(handle, c_params, c_swparams, "capture")) < 0)
        {
            // snd_pcm_sw_params_free(c_swparams);
            printf("Unable to set sw parameters for playback stream: %s\n", snd_strerror(err));
            return -1;
        }

        // free
        // snd_pcm_hw_params_free(ct_params);
        // snd_pcm_hw_params_free(c_params);
        // snd_pcm_sw_params_free(c_swparams);

        if ((err = snd_pcm_prepare(handle)) < 0)
        {
            printf("Prepare error: %s\n", snd_strerror(err));
            return -1;
        }

        snd_pcm_dump(handle, output);
        fflush(stdout);
        return 0;
    }

    void CAudioMp3Capture::showstat(snd_pcm_t *handle, size_t frames)
    {
        int err;
        snd_pcm_status_t *status;

        snd_pcm_status_alloca(&status);
        if ((err = snd_pcm_status(handle, status)) < 0)
        {
            printf("Stream status error: %s\n", snd_strerror(err));
            return;
        }
        printf("*** frames = %li ***\n", (long)frames);
        snd_pcm_status_dump(status, output);
    }

    void CAudioMp3Capture::showlatency(size_t latency)
    {
        double d;
        latency *= 2;
        d = (double)latency / (double)rate;
        printf("Trying latency %li frames, %.3fus, %.6fms (%.4fHz)\n", (long)latency, d * 1000000, d * 1000, (double)1 / d);
    }

    void CAudioMp3Capture::showinmax(size_t in_max)
    {
        double d;

        printf("Maximum read: %li frames\n", (long)in_max);
        d = (double)in_max / (double)rate;
        printf("Maximum read latency: %.3fus, %.6fms (%.4fHz)\n", d * 1000000, d * 1000, (double)1 / d);
    }

    void CAudioMp3Capture::gettimestamp(snd_pcm_t *handle, snd_timestamp_t *timestamp)
    {
        int err;
        snd_pcm_status_t *status;

        snd_pcm_status_alloca(&status);
        if ((err = snd_pcm_status(handle, status)) < 0)
        {
            printf("Stream status error: %s\n", snd_strerror(err));
            return;
        }
        snd_pcm_status_get_trigger_tstamp(status, timestamp);
    }

    void CAudioMp3Capture::setscheduler(void)
    {
        struct sched_param sched_param;

        if (sched_getparam(0, &sched_param) < 0)
        {
            printf("Scheduler getparam failed...\n");
            return;
        }
        sched_param.sched_priority = sched_get_priority_max(SCHED_RR);
        if (!sched_setscheduler(0, SCHED_RR, &sched_param))
        {
            printf("Scheduler set to Round Robin with priority %i...\n", sched_param.sched_priority);
            fflush(stdout);
            return;
        }
        printf("!!!Scheduler set to Round Robin with priority %i FAILED!!!\n", sched_param.sched_priority);
    }

    long CAudioMp3Capture::timediff(snd_timestamp_t t1, snd_timestamp_t t2)
    {
        signed long l;

        t1.tv_sec -= t2.tv_sec;
        l = (signed long)t1.tv_usec - (signed long)t2.tv_usec;
        if (l < 0)
        {
            t1.tv_sec--;
            l = 1000000 + l;
            l %= 1000000;
        }
        return (t1.tv_sec * 1000000) + l;
    }

    // long CAudioMp3Capture::readbuf(snd_pcm_t *handle, char *buf, long len, size_t *frames, size_t *max)
    long CAudioMp3Capture::readbuf(snd_pcm_t *handle, short *buf, long len, size_t *frames, size_t *max)
    {
        long r;

        if (!block)
        {
            do
            {
                r = snd_pcm_readi(handle, buf, len);
            } while (r == -EAGAIN);
            if (r > 0)
            {
                *frames += r;
                if ((long)*max < r)
                    *max = r;
            }
            // printf("read = %li\n", r);
        }
        else
        {
            int frame_bytes = (snd_pcm_format_width(format) / 8) * channels;
            do
            {
                r = snd_pcm_readi(handle, buf, len);
                if (r > 0)
                {
                    buf += r * frame_bytes;
                    len -= r;
                    *frames += r;
                    if ((long)*max < r)
                        *max = r;
                }
                // printf("r = %li, len = %li\n", r, len);
            } while (r >= 1 && len > 0);
        }
        // showstat(handle, 0);
        return r;
    }

    void CAudioMp3Capture::capture_loop(void *p)
    {
        snd_timestamp_t c_tstamp;
        ssize_t r;
        int ok = 1;
        size_t frames_in, in_max = 0;
        int b_size;
        //
        gettimestamp(capture_handle, &c_tstamp);
        printf("Capture:\n");
        showstat(capture_handle, frames_in);
        //
        // while (b_loop_capture && ok && frames_in < loop_limit)
        while (b_loop_capture && ok && ((loop_sec < 0) ? 1 : (frames_in < loop_limit)))
        {
            if (use_poll)
            {
                /* use poll to wait for next event */
                snd_pcm_wait(capture_handle, 1000);
            }
            // if ((r = readbuf(capture_handle, buffer, latency, &frames_in, &in_max)) < 0)
            if ((r = readbuf(capture_handle, pcm_buffer, latency, &frames_in, &in_max)) < 0)
                ok = 0;
            else
            {
                b_size = lame_encode_buffer_interleaved(gfp, pcm_buffer, r / 4, mp3_buffer, mp3_size);

                // write to stdout
                int n = fwrite(mp3_buffer, sizeof(char), b_size, pFile);
                if (n != b_size)
                {
                    fprintf(stderr, "short write: wrote %d bytes\n", n);
                }
            }
        }
        // memset(mp3_buffer, 0, latency);
        b_size = lame_encode_flush(gfp, mp3_buffer, sizeof(mp3_buffer));
        if (b_size > 0)
        {
            fwrite(mp3_buffer, sizeof(char), b_size, pFile);
        }
        b_size = lame_get_lametag_frame(gfp, mp3_buffer, sizeof(mp3_buffer));
        if (b_size > 0)
        {
            fwrite(mp3_buffer, sizeof(char), b_size, pFile);
        }

        printf("Capture:\n");
        showstat(capture_handle, frames_in);
        showinmax(in_max);

        unInit();

        if (ok)
            printf("Success\n");
        else
            printf("Failure\n");

        if (b_loop_capture)
        {
            /* code */
        }
        else
        {
        }
    }

    int CAudioMp3Capture::alsa_open(std::string _device)
    {
        int err;
        err = snd_output_stdio_attach(&output, stdout, 0);
        if (err < 0)
        {
            printf("Output failed: %s\n", snd_strerror(err));
            return -1;
        }

        loop_limit = loop_sec * rate;
        latency = latency_min - 4;
        buffer = (char *)malloc((latency_max * snd_pcm_format_width(format) / 8) * 2);
        pcm_buffer = (short *)malloc((latency_max * snd_pcm_format_width(format) / 8) * 2);

        setscheduler();

        printf("Capture device is %s\n", _device.c_str());
        printf("Parameters are %iHz, %s, %i channels, %s mode\n", rate, snd_pcm_format_name(format), channels, block ? "blocking" : "non-blocking");
        printf("Poll mode: %s\n", use_poll ? "yes" : "no");
        printf("Loop limit is %lu frames, minimum latency = %i, maximum latency = %i\n", loop_limit, latency_min * 2, latency_max * 2);

        // Open PCM device for recording(capture)
        if ((err = snd_pcm_open(&capture_handle, _device.c_str(), SND_PCM_STREAM_CAPTURE, block ? 0 : SND_PCM_NONBLOCK)) < 0)
        {
            fprintf(stderr, "cannot open audio device %s (%s)\n",
                    _device.c_str(),
                    snd_strerror(err));
            return -1;
        }

        if (alsa_setparams(capture_handle, &latency) != 0)
        {
            // return -1;
            goto open_fail;
        }

        showlatency(latency);

        // if (snd_pcm_format_set_silence(format, buffer, latency * channels) < 0)
        if (snd_pcm_format_set_silence(format, pcm_buffer, latency * channels) < 0)
        {
            fprintf(stderr, "silence error\n");
            // return -1;
            goto open_fail;
        }

        if ((err = snd_pcm_start(capture_handle)) < 0)
        {
            printf("Go error: %s\n", snd_strerror(err));
            // return -1;
            goto open_fail;
        }

        return 0;

    open_fail:
        snd_pcm_close(capture_handle);
        return -1;
    }

    int CAudioMp3Capture::alsa_close()
    {
        snd_pcm_drop(capture_handle);
        // snd_pcm_drain(capture_handle);
        snd_pcm_hw_free(capture_handle);
        snd_pcm_close(capture_handle);
        if (buffer)
        {
            free(buffer);
            buffer = NULL;
        }
        if (pcm_buffer)
        {
            free(pcm_buffer);
            pcm_buffer = NULL;
        }

        return 0;
    }

    int CAudioMp3Capture::lame_load()
    {
        gfp = lame_init();
        if (gfp == NULL)
        {
            return -1;
        }

        int err = lame_init_params(gfp);
        if (err < 0)
        {
            lame_close(gfp);
            return -1;
        }

        mp3_size = (1.25 * latency) + 7200;
        mp3_buffer = (unsigned char *)malloc(mp3_size);

        return 0;
    }

    int CAudioMp3Capture::lame_release()
    {
        lame_close(gfp);
        if (mp3_buffer)
        {
            free(mp3_buffer);
            mp3_buffer = NULL;
        }

        return 0;
    }

    int CAudioMp3Capture::init()
    {
        if (alsa_open(cdevice) != 0)
        {
            return -1;
        }

        if (lame_load() != 0)
        {
            return -1;
        }

        b_initialize = true;

        return 0;
    }

    int CAudioMp3Capture::unInit()
    {
        lame_release();
        alsa_close();
        if (pFile)
        {
            fclose(pFile);
        }
        result_callback = nullptr;
        b_initialize = false;

        return 0;
    }

    void CAudioMp3Capture::RegisterOnResultCallback(CaptureResultCallback _callback)
    {
        result_callback = _callback;
    }

    int CAudioMp3Capture::start_capture(std::string _file, int _time)
    {
        loop_sec = _time;
        if (init() != 0)
        {
            return -1;
        }

        pFile = fopen(_file.c_str(), "wb");
        if (pFile == NULL)
        {
            unInit();
            return -1;
        }

        b_loop_capture = true;
        std::thread record_Thread = std::thread(std::bind(&CAudioMp3Capture::capture_loop, this, nullptr));
        record_Thread.detach();

        return 0;
    }

    int CAudioMp3Capture::stop_capture()
    {
        b_loop_capture = false;

        return 0;
    }
}