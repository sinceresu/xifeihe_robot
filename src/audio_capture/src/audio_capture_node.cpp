#include "audio_capture_node.hpp"

namespace audio_capture
{
    CAudioCaptureNode::CAudioCaptureNode(NodeOptions _node_options)
        : node_options(_node_options)
    {
        LaunchPublishers();
        LaunchService();
    }

    CAudioCaptureNode::~CAudioCaptureNode()
    {
    }

    void CAudioCaptureNode::LaunchPublishers()
    {
        audio_capture_publisher = nh.advertise<audio_capture_msgs::CaptureResult>(node_options.audio_capture_pub, 1);
    }

    void CAudioCaptureNode::LaunchService()
    {
        audio_capture_service = nh.advertiseService(node_options.audio_capture_service,
                                                    &CAudioCaptureNode::audio_capture_service_Callback, this);
    }

    void CAudioCaptureNode::publisher_audio_capture(std::string _file, int _result, std::string _msg)
    {
        audio_capture_msgs::CaptureResult msg;
        msg.file = _file;
        msg.result = _result;
        msg.message = _msg;
        audio_capture_publisher.publish(msg);
    }

    bool CAudioCaptureNode::audio_capture_service_Callback(audio_capture_msgs::AudioCapture::Request &req,
                                                           audio_capture_msgs::AudioCapture::Response &res)
    {
        ROS_INFO_STREAM("ptz_control_service_Callback req:\n"
                        << req);

        int ret = 0;

        if (req.type == 0)
        {
            audio_capture.stop();
        }
        if (req.type == 1)
        {
            if (audio_capture.is_run())
            {
                ret = 1;
                goto __end;
            }
            audio_arecord(req.file, req.format, req.time);
        }

    __end:
        if (ret == 1)
        {
            res.result = 1;
            res.message = "runing";
            return false;
        }
        res.result = 0;
        res.message = "success";

        return true;
    }

    int CAudioCaptureNode::audio_arecord(std::string _file, std::string _file_type, unsigned int _time)
    {
        std::thread record_Thread = std::thread(std::bind(&CAudioCaptureNode::audio_arecord_fun, this, nullptr, _file, _file_type, _time));
        record_Thread.detach();

        return 0;
    }

    void CAudioCaptureNode::audio_arecord_fun(void *p, std::string _file, std::string _file_type, unsigned int _time)
    {
        audio_capture.audio_setopt_dict("--command", "arecord", 0);
        audio_capture.audio_setopt_dict("--names", _file.c_str(), 1);
        audio_capture.audio_setopt_dict("--device", "default", 1);
        audio_capture.audio_setopt_dict("--file-type", _file_type.c_str(), 1);
        audio_capture.audio_setopt_dict("--duration", std::to_string(_time).c_str(), 1);
        // audio_capture.audio_setopt_dict("--channels", "2", 1);
        // audio_capture.audio_setopt_dict("--format", "cd", 1);
        // audio_capture.audio_setopt_dict("--rate", "44100", 1);

        int ret = audio_capture.start();
        if (ret == 0)
        {
            publisher_audio_capture(_file, 0, "finish");
            ROS_INFO("audio arecord finish");
        }
        else if (ret == 1)
        {
            publisher_audio_capture(_file, 1, "run");
        }
        else
        {
            publisher_audio_capture(_file, -1, "fail");
            ROS_INFO("audio arecord fail");
        }
    }

}