#include <iostream>
#include <stdio.h>
#include <pthread.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>

#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CompressedImage.h>
#include <sensor_msgs/image_encodings.h>
#include <diagnostic_msgs/DiagnosticArray.h>

#include <yd_camerastream_capture/infrared_stream_capture.hpp>

Mat g_frame;
std::string g_infrared_rtsp_str;
std::string g_error_info_str;

void *get_image(void* args)
{
    VideoCapture rtsp_cap;
    rtsp_cap.open(g_infrared_rtsp_str);

    while(true)
    {
        if(!rtsp_cap.isOpened())
        {
            ROS_INFO("camera stream open fail, open again!");
            g_error_info_str = "infrared camera open faild!";
            sleep(1);
            rtsp_cap.release();
            rtsp_cap.open(g_infrared_rtsp_str);
        }
        else
        {        
            rtsp_cap.read(g_frame);
            if(g_frame.empty())
            {
                ROS_INFO("Error! blank frame grabbed!");
                g_error_info_str = "infrared camera image is empty!";
                sleep(1);
                rtsp_cap.release();
                rtsp_cap.open(g_infrared_rtsp_str);
            }
        }
    }
}

infrared_stream_capture::infrared_stream_capture()
{ 
    ros::NodeHandle private_node_handle("~");
    ros::NodeHandle node_handle_;

    private_node_handle.param<std::string>("infrared_topic_name", infrared_topic_str, "/infrared/image_proc");
    private_node_handle.param<std::string>("infrared_rtsp_string", infrared_rtsp_str, "rtsp://admin:123qweasd@192.168.1.62:554/h264/ch1/main/av_stream");
    private_node_handle.param<std::string>("heartbeat_topic_string", heartbeat_topic_str, "/yida/heartbeat");
	g_infrared_rtsp_str = infrared_rtsp_str;

    image_transport::ImageTransport it(node_handle_);
    publisher_image_ = it.advertise(infrared_topic_str, 1);
    heartbeat_pub_ = node_handle_.advertise<diagnostic_msgs::DiagnosticArray>("heartbeat_topic_str", 1);

    create_thread();
}

infrared_stream_capture::~infrared_stream_capture()
{

}

void infrared_stream_capture::create_thread()
{
    pthread_t ros_thread = 0; 
    pthread_create(&ros_thread,NULL,get_image,NULL);
}

void infrared_stream_capture::update()
{
    if(g_frame.empty())
    {
        int level = 2;
        std::string message = g_error_info_str;
        std::string hardware_id = "infrared_camera";
        pub_heartbeat(level, message, hardware_id);
    }
    else
    {
        sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", g_frame).toImageMsg();
        msg->header.stamp = ros::Time::now();
        msg->header.frame_id = "infrared_picture";
        publisher_image_.publish(msg);
        int level = 0;
        std::string message = "infrared camera is ok!";
        std::string hardware_id = "infrared_camera";
        pub_heartbeat(level, message, hardware_id);
    }
}

void infrared_stream_capture::pub_heartbeat(int level, string message, string hardware_id)
{
    diagnostic_msgs::DiagnosticArray log;
    log.header.stamp = ros::Time::now();

    diagnostic_msgs::DiagnosticStatus s;
    s.name = __app_name__;         // 这里写节点的名字
    s.level = level;               // 0 = OK, 1 = Warn, 2 = Error
    if (!message.empty())
    {
        s.message = message;       // 问题描述
    }
    s.hardware_id = hardware_id;   // 硬件信息
    log.status.push_back(s);

    heartbeat_pub_.publish(log);
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, __app_name__);
    infrared_stream_capture camcap;
    ros::Rate rate(15);

    while (ros::ok())
    {
        camcap.update();
        ros::spinOnce();
        rate.sleep();
    }

    return 0;
}

