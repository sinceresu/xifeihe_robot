# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/li/yd_framework/ros_base/yd_camerastream_capture/src/infrared_stream_capture.cpp" "/home/li/yd_framework/ros_base/yd_camerastream_capture/obj-x86_64-linux-gnu/CMakeFiles/infrared_stream_capture_node.dir/src/infrared_stream_capture.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"yd_camerastream_capture\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  "/opt/ros/melodic/include"
  "/opt/ros/melodic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  "/usr/include/opencv"
  "../if"
  "../("
  "../ARM_BUILD"
  "../)"
  "/usr/local/ffmpeg/include/libavcodec"
  "/usr/local/ffmpeg/include/libavformat"
  "/usr/local/ffmpeg/include/libswscale"
  "../else"
  "/usr/include/x86_64-linux-gnu/libavcodec"
  "/usr/include/x86_64-linux-gnu/libavformat"
  "/usr/include/x86_64-linux-gnu/libswscale"
  "../endif"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
