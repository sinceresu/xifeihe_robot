/*
 * @Descripttion: 
 * @version: 
 * @Author: li
 * @Date: 2021-02-19 14:32:48
 * @LastEditors: li
 * @LastEditTime: 2021-03-04 16:13:19
 */
#include "ros/ros.h"
#include <image_transport/image_transport.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CompressedImage.h>
#include <sensor_msgs/image_encodings.h>
#include <vector>
#include <diagnostic_msgs/DiagnosticArray.h>

using namespace std;
class rtsp_capture{
public:
    std::string error_code,error_info;
    std::atomic<bool> receive_error;
    pthread_t tid = 0;
    std::string topic_str, rtsp_str;
    bool use_tcp;
    ros::Publisher image_pub,heartbeat_pub;
    rtsp_capture();
    ~rtsp_capture();
    void update();
    void pub_heartbeat(int level, string e_message,string e_code);
};