#include <iostream>
#include <stdio.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>

#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CompressedImage.h>
#include <sensor_msgs/image_encodings.h>
#include <diagnostic_msgs/DiagnosticArray.h>

using namespace cv;
using namespace std;

#define __app_name__ "infrared_stream_node"
 
class infrared_stream_capture
{
private:
    std::string infrared_topic_str, infrared_rtsp_str, heartbeat_topic_str;
    image_transport::Publisher publisher_image_;
    ros::Publisher heartbeat_pub_;
private:
    void create_thread();
public:
    infrared_stream_capture();
    ~infrared_stream_capture();
    void update();
    void pub_heartbeat(int level, string message, string hardware_id);
};
