/**
 * @file front_end.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-1-19
 * @brief front end
 */

#ifndef YD_FUSION_LOCALIZATION_FRONTEND_HPP_
#define YD_FUSION_LOCALIZATION_FRONTEND_HPP_

#include "yd_fusion_localization/subscriber/initialpose_subscriber.hpp"
#include "yd_fusion_localization/sensor_bridge/predictor.hpp"
#include "yd_fusion_localization/sensor_bridge/historypose.hpp"
#include "yd_fusion_localization/tf/tf_broadcaster.hpp"

namespace yd_fusion_localization
{
    class FrontEnd
    {
    public:
        FrontEnd(ros::NodeHandle &nh, const YAML::Node &yaml_node, const std::string &map_frame, const std::string &robot_frame);
        ~FrontEnd() {}
        FrontEnd(const FrontEnd &frontend) = delete;
        FrontEnd &operator=(const FrontEnd &frontend) = delete;

        bool HandleMessage(const MessagePointer &msg_ptr, const std::string &topic);
        void ClearData();
        void Reset();
        bool IfStop();
        bool IfRelocalization();
        bool IfReceivedInitialpose();
        void GetInitialpose(std::deque<Sophus::SE3d> &poses);
        void GetHistorypose(std::deque<Sophus::SE3d> &poses);
        void PublishStatus(DiagnosticType type);
        void SetStatus(bool status);
        bool Status();
        void SetState(const State &state);
        State GetState();
        void SetUpdateState(const State &state);
        State GetUpdateState() const;
        void SetInliers(const std::vector<int> &inliers);
        std::vector<int> GetInliers();
        bool HasData(double start_stamp, double end_stamp, std::deque<std::pair<bool, PredictDataPtr>> &datas);
        std::deque<std::pair<bool, PredictDataPtr>> NewData() const;
        bool GetVelocity(double stamp, Eigen::Vector3d &linear_velocity, Eigen::Vector3d &angular_velocity);
        bool GetVelocity(const State &state, Eigen::Vector3d &linear_velocity, Eigen::Vector3d &angular_velocity);

        bool Update();

    private:
        void Publish(const State &state);
        void Publish(double stamp);
        int DeadReckoning(const State &state, double ustamp, State &result_state);
        bool DeadReckoning(const State &state, const PredictDataPtr &data, State &result_state);
        bool CheckCovariance(const Eigen::MatrixXd &covariance);
        void ConfigurePredictors(ros::NodeHandle &nh, const YAML::Node &yaml_node, const std::string &robot_frame);
        void ConfigureDiagnosticPublisher(ros::NodeHandle &nh, const YAML::Node &yaml_node);

        double update_interval_time_;
        double update_delay_time_;
        double max_vel_;
        double max_gyro_;
        std::vector<double> max_covariance_;
        bool inlier_check_;

        std::shared_ptr<InitialposeSubscriber> initialpose_subscriber_;
        Sophus::SE3d initpose_;
        std::shared_ptr<Historypose> historypose_;
        std::deque<std::shared_ptr<Predictor>> predictors_;
        std::deque<PredictDataPtr> pdata_;

        bool localization_status_;
        std::mutex mutex_status_;

        std::shared_ptr<DiagnosticPublisher> diagnostic_publisher_;
        std::mutex mutex_diag_;

        std::pair<std::shared_ptr<Publisher>, PublishDataPtr> pose_publisher_;
        // std::pair<std::shared_ptr<Publisher>, PublishDataPtr> lidar_pose_publisher_;
        std::shared_ptr<TFBroadCaster> tf_broadcaster_;

        State state_;
        std::mutex mutex_state_;
        State update_state_;

        std::vector<int> inliers_;
        std::mutex mutex_inliers_;
        
    };
} // namespace yd_fusion_localization

#endif
