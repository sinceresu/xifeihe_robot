/**
 * @file lidar_mapping.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-6-17
 * @brief lidar mapping
 */

#ifndef YD_FUSION_LOCALIZATION_LIDARMAPPING_HPP_
#define YD_FUSION_LOCALIZATION_LIDARMAPPING_HPP_

#include <g2o/stuff/macros.h>
#include <g2o/core/factory.h>
#include <g2o/core/block_solver.h>
#include <g2o/core/linear_solver.h>
#include <g2o/core/sparse_optimizer.h>
#include <g2o/core/robust_kernel_factory.h>
#include <g2o/core/optimization_algorithm_factory.h>
#include <g2o/solvers/pcg/linear_solver_pcg.h>
#include <g2o/solvers/eigen/linear_solver_eigen.h>
#include <g2o/core/optimization_algorithm_levenberg.h>

#include "yd_fusion_localization/models/graph_optimizer/g2o/edge/edge_pl.hpp"
#include "yd_fusion_localization/models/graph_optimizer/g2o/edge/edge_ql.hpp"
#include "yd_fusion_localization/models/graph_optimizer/g2o/edge/edge_posel.hpp"
#include "yd_fusion_localization/models/graph_optimizer/g2o/edge/edge_odom.hpp"
#include "yd_fusion_localization/models/graph_optimizer/g2o/edge/edge_plane3d.hpp"

#include "yd_fusion_localization/models/cloud_filter/cloud_filter.hpp"
#include "yd_fusion_localization/mapping/mapping.hpp"
/*
    * A point cloud type that has 6D pose info ([x,y,z,roll,pitch,yaw] intensity is time stamp)
    */

namespace yd_fusion_localization
{
    class LidarMapping : public Mapping
    {
    public:
        LidarMapping(const YAML::Node &yaml_node);
        virtual ~LidarMapping();
        virtual void Reset() override;
        virtual bool Initialize(const std::deque<LocalizerDataPtr> &localizer_data, FrameDataPtr &frame) override;
        virtual bool Handle(const std::deque<LocalizerDataPtr> &localizer_data, FrameDataPtr &frame) override;
        virtual bool Handle(const LoopDataPtr &loop_detect, LoopDataPtr &loop_result) override;
        virtual bool Optimize() override;
        virtual bool SaveMap(const std::string& map_directory) override;
        virtual std::deque<std::pair<bool, Sophus::SE3d>> GetOptimizedPose() override;
        virtual std::deque<FrameDataPtr> GetKeyFrames() override;
        virtual pcl::PointCloud<pcl::PointXYZI>::Ptr GetGlobalMap();
        virtual Sophus::SE3d GetOdom2Map() const;
        virtual bool GetOptimizedPose(int index, Sophus::SE3d &op) const;

    private:
        bool LoopMatching(const LoopDataPtr &loop_detect, LoopDataPtr &loop_result);
        bool LoopFindKey(LoopDataPtr &loop_result);
        pcl::PointCloud<pcl::PointXYZI>::Ptr LoopFindNearKeyframes(const int &key, const int &searchNum);
        void AddRobustKernel(g2o::OptimizableGraph::Edge *edge);
        void AddEdgeLocalizer(int frame_index, const LocalizerDataPtr &data);
        void RemoveEdgeOutlier();
        void RemoveEdgeRobustKernel();

        int max_iterations_num_ = 512;

        double outlier_chi2_;
        std::string map_path_;
        std::shared_ptr<CloudFilter<pcl::PointXYZI>> full_filter_;
        g2o::RobustKernelFactory *robust_kernel_factory_;
        std::string robust_kernel_name_;
        double robust_kernel_delta_;
        std::unique_ptr<g2o::SparseOptimizer> graph_ptr_;
        std::deque<FrameDataPtr> key_frame_;
        std::deque<std::pair<bool, Sophus::SE3d>> optimized_pose_;
        std::deque<double> optimized_pose_time_;

        int edge_id_;
        std::vector<g2o::OptimizableGraph::Edge *> robust_edges_;
        std::shared_ptr<CloudFilter<pcl::PointXYZI>> loop_filter_;
        int loop_near_key_num_;
        double loop_search_radius_;
        double loop_fitness_score_;
        Sophus::SE3d odom2map_;
    };
} // namespace yd_fusion_localization

#endif