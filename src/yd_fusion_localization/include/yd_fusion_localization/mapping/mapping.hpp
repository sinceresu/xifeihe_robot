/**
 * @file mapping.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-6-17
 * @brief mapping
 */

#ifndef YD_FUSION_LOCALIZATION_MAPPING_HPP_
#define YD_FUSION_LOCALIZATION_MAPPING_HPP_

#include "yd_fusion_localization/utilities/data_conversions.hpp"

namespace yd_fusion_localization
{
    class Mapping
    {
    public:
        Mapping(const YAML::Node &yaml_node);
        virtual ~Mapping() {}
        virtual void Reset() = 0;
        virtual bool Initialize(const std::deque<LocalizerDataPtr> &localizer_data, FrameDataPtr &frame) = 0;
        virtual bool Handle(const std::deque<LocalizerDataPtr> &localizer_data, FrameDataPtr &frame) = 0;
        virtual bool Handle(const LoopDataPtr &loop_detect, LoopDataPtr &loop_result) = 0;
        virtual bool Optimize() = 0;
        virtual bool SaveMap(const std::string & map_directory = "") = 0;
        virtual std::deque<std::pair<bool, Sophus::SE3d>> GetOptimizedPose() = 0;
        virtual std::deque<FrameDataPtr> GetKeyFrames() = 0;

    protected:
    };
} // namespace yd_fusion_localization

#endif