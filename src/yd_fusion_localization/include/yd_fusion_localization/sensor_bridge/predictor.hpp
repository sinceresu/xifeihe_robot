/**
 * @file predictor.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-1-13
 * @brief predictor
 */

#ifndef YD_FUSION_LOCALIZATION_PREDICTOR_HPP_
#define YD_FUSION_LOCALIZATION_PREDICTOR_HPP_

/// predict subscriber
#include "yd_fusion_localization/subscriber/odometry_subscriber.hpp"
#include "yd_fusion_localization/subscriber/twist_subscriber.hpp"
#include "yd_fusion_localization/subscriber/imu_subscriber.hpp"
#include "yd_fusion_localization/subscriber/vg_subscriber.hpp"
#include "yd_fusion_localization/tf/tf_static_broadcaster.hpp"
#include "yd_fusion_localization/sensor_bridge/configuration.hpp"

namespace yd_fusion_localization
{
    class Predictor
    {
    public:
        Predictor(ros::NodeHandle &nh, const YAML::Node &yaml_node, const std::string &robot_frame);
        ~Predictor() {}
        Predictor(const Predictor &other) = delete;
        Predictor &operator=(const Predictor &other) = delete;

        bool HandleMessage(const MessagePointer &msg_ptr, const std::string &topic);
        void ClearData();
        PredictType GetType() const;
        PredictDataPtr NewData() const;
        bool HasData(double start_stamp, double end_stamp, PredictDataPtr &data);
        double GetLatestData(double start_stamp, PredictDataPtr &data);
        bool CheckData(const PredictDataPtr &data) const;
        bool CheckDRData(const PredictDataPtr &data) const;
        void Publish(double stamp);

    private:
        PredictType type_;
        std::shared_ptr<PredictSubscriber> subscriber_;
        std::deque<std::pair<std::shared_ptr<TFStaticBroadCaster>, Sophus::SE3d>> tf_broadcasters_;
    };
} // namespace yd_fusion_localization

#endif
