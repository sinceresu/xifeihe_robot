/**
 * @file measurer.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-1-8
 * @brief measurer
 */

#ifndef YD_FUSION_LOCALIZATION_MEASURER_HPP_
#define YD_FUSION_LOCALIZATION_MEASURER_HPP_

#include <chrono>
#include <condition_variable>

/// measure subscriber
#include "yd_fusion_localization/subscriber/cloud_subscriber.hpp"
#include "yd_fusion_localization/subscriber/gnss_subscriber.hpp"
#include "yd_fusion_localization/subscriber/pose_subscriber.hpp"
#include "yd_fusion_localization/subscriber/image_subscriber.hpp"
#include "yd_fusion_localization/subscriber/p_subscriber.hpp"
#include "yd_fusion_localization/subscriber/q_subscriber.hpp"

/// tf broadcaster
#include "yd_fusion_localization/tf/tf_broadcaster.hpp"

/// localizer
#include "yd_fusion_localization/localizer/cloud_localizer.hpp"
#include "yd_fusion_localization/localizer/pose_localizer.hpp"
#include "yd_fusion_localization/localizer/imagelandmark_localizer.hpp"
#include "yd_fusion_localization/localizer/p_localizer.hpp"
#include "yd_fusion_localization/localizer/q_localizer.hpp"

/// publisher
#include "yd_fusion_localization/sensor_bridge/configuration.hpp"
namespace yd_fusion_localization
{
    class Measurer
    {
    public:
        Measurer(ros::NodeHandle &nh, const YAML::Node &yaml_node, const std::string &map_frame, const std::string &robot_frame);
        ~Measurer() {}
        Measurer(const Measurer &other) = delete;
        Measurer &operator=(const Measurer &other) = delete;

        bool HandleMessage(const MessagePointer &msg_ptr, const std::string &topic);
        void ClearData();
        void Reset();
        LocalizerType GetLType() const;
        MeasureDataPtr NewMData() const;
        LocalizerDataPtr NewLData() const;
        bool GetLatestMStamp(double &stamp);
        double GetEarliestMStamp();
        bool HasMData(double start_stamp, double end_stamp, double &stamp);
        bool HasLData(const MState &guess_mstate, LocalizerDataPtr &ldata);
        bool HasLData(double stamp, LocalizerDataPtr &ldata);
        bool CheckPosition(const Sophus::SE3d &pose);
        bool CheckTime(double stamp);
        void LoopDetection(std::deque<Sophus::SE3d> &poses);
        bool Initialize(const std::deque<MState> &poses, LocalizerDataPtr &ldata);
        void Publish(const State &state);
        void Publish(double stamp);
        void PublishTF(double stamp);
        bool Save(const MState &mstate);
        bool Save();

    private:
        void Publish(double stamp, const Sophus::SE3d &pose);

        std::shared_ptr<MeasureSubscriber> subscriber_;
        std::shared_ptr<Localizer> localizer_;
        LocalizerType ltype_;
        std::unordered_map<std::string, std::pair<std::shared_ptr<Publisher>, PublishDataPtr>> publishers_;
        std::deque<std::pair<std::shared_ptr<TFBroadCaster>, Sophus::SE3d>> tf_broadcasters_;

        MeasureDataPtr mdata_;
        LocalizerDataPtr ldata_;
    };
} // namespace yd_fusion_localization

#endif