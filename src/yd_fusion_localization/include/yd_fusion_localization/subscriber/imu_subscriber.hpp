/**
 * @file imu_subscriber.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-15
 * @brief subscribe imu data
 */

#ifndef YD_FUSION_LOCALIZATION_IMU_SUBSCRIBER_HPP_
#define YD_FUSION_LOCALIZATION_IMU_SUBSCRIBER_HPP_

#include "yd_fusion_localization/subscriber/predict_subscriber.hpp"

namespace yd_fusion_localization
{
    class ImuSubscriber : public PredictSubscriber
    {
    public:
        ImuSubscriber(ros::NodeHandle &nh,
                      const YAML::Node &yaml_node);
        ~ImuSubscriber() {}
        bool HandleMessage(const MessagePointer &msg_ptr, const std::string &topic) override;
        void ClearData() override;
        void ClearAllData() override;
        double GetEarliestStamp() override;
        PredictType GetType() const { return PredictType::Imu; }
        bool ValidData(double start_stamp, double end_stamp, PredictDataPtr &data) override;
        double GetLatestData(double start_stamp, PredictDataPtr &data) override;
        bool CheckData(const PredictDataPtr &data) const override;
        bool CheckDRData(const PredictDataPtr &data) const override;

    private:
        void ParseData(PredictDataPtr &data) override;

        double max_time_;
        double max_gyro_;
        double max_acc_;
        Eigen::Vector3d gravity_;
        std::deque<sensor_msgs::ImuConstPtr> data_buffer_;
    };
} // namespace yd_fusion_localization

#endif