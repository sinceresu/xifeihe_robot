/**
 * @file localizer.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-18
 * @brief localizer
 */

#ifndef YD_FUSION_LOCALIZATION_LOCALIZER_HPP_
#define YD_FUSION_LOCALIZATION_LOCALIZER_HPP_

#include "yd_fusion_localization/utilities/data_conversions.hpp"

namespace yd_fusion_localization
{
    class Localizer
    {
    public:
        Localizer(const YAML::Node &yaml_node);
        virtual ~Localizer() {}
        virtual bool CheckPosition(const Sophus::SE3d &pose) = 0;
        virtual bool LoopDetection(const MeasureDataPtr &data, std::deque<Sophus::SE3d> &pose) = 0;
        virtual bool Initialize(const MeasureDataPtr &data, const MState &mstate, LocalizerDataPtr &localizer_data) = 0;
        virtual bool GetPose(const MeasureDataPtr &data, const MState &guess, LocalizerDataPtr &localizer_data) = 0;
        virtual bool GetPose(const MeasureDataPtr &data, LocalizerDataPtr &localizer_data) = 0;
        virtual bool Save(const MState &mstate, const MeasureDataPtr &data) = 0;
        virtual bool Save() = 0;

        Sophus::SE3d local_to_map_; ///< 地图到map_frame的转换
        std::string local_frame_;
    };
} // namespace yd_fusion_localization

#endif