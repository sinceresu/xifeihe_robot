/**
 * @file gnss_localizer.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-21
 * @brief gnss localizer
 */

#ifndef YD_FUSION_LOCALIZATION_GNSS_LOCALIZER_HPP_
#define YD_FUSION_LOCALIZATION_GNSS_LOCALIZER_HPP_

#include "yd_fusion_localization/localizer/localizer.hpp"
#include "Geocentric/LocalCartesian.hpp"

namespace yd_fusion_localization
{
    class GNSSLocalizer : public Localizer
    {
    public:
        GNSSLocalizer(const YAML::Node &yaml_node);
        ~GNSSLocalizer() {}
        bool CheckPosition(const Sophus::SE3d &pose) override;
        bool LoopDetection(const MeasureDataPtr &data, std::deque<Sophus::SE3d> &pose) override;
        bool Initialize(const MeasureDataPtr &data, const MState &mstate, LocalizerDataPtr &localizer_data) override;
        bool GetPose(const MeasureDataPtr &data, const MState &guess, LocalizerDataPtr &localizer_data) override;
        bool GetPose(const MeasureDataPtr &data, LocalizerDataPtr &localizer_data) override;
        bool Save(const MState &mstate, const MeasureDataPtr &data) override;
        bool Save() override;

    private:
        bool GetPose(const MeasureDataPtr &data, Eigen::Vector3d &result_p);
        /**
         * @brief refer to https://en.wikipedia.org/wiki/Geographic_coordinate_conversion#From_geodetic_to_ECEF_coordinates
         */
        Eigen::Vector3d LatLongAltToEcef(const double &latitude, const double &longitude,
                                         const double &altitude);
        Eigen::Vector3d LlaToLocal(const double &latitude, const double &longitude,
                                   const double &altitude);
        Eigen::Vector3d LocalToLla(const Eigen::Vector3d &xyz);

        double height_;
        Eigen::Matrix<double, 6, 1> invalid_ranges_;
        double distance_threshold_;
        GeographicLib::LocalCartesian geo_converter_;
        bool geo_inited_;
    };
} // namespace yd_fusion_localization

#endif