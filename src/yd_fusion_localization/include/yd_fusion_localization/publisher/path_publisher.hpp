/**
 * @file path_publisher.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-7-12
 * @brief path publisher
 */

#ifndef YD_FUSION_LOCALIZATION_PATH_PUBLISHER_HPP_
#define YD_FUSION_LOCALIZATION_PATH_PUBLISHER_HPP_

#include "yd_fusion_localization/publisher/publisher.hpp"

namespace yd_fusion_localization
{
    class PathPublisher : public Publisher
    {
    public:
        PathPublisher(ros::NodeHandle &nh,
                      const std::string &topic_name,
                      const std::string &frame_id,
                      int buff_size,
                      bool latch = false);

        void Publish(const PublishDataPtr &data) override;
    };
} // namespace yd_fusion_localization

#endif