/**
 * @file data_types.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-19
 * @brief data types
 */
#ifndef PCL_NO_PRECOMPILE
#define PCL_NO_PRECOMPILE
#endif

#ifndef YD_FUSION_LOCALIZATION_DATA_TYPES_HPP_
#define YD_FUSION_LOCALIZATION_DATA_TYPES_HPP_

#include <deque>
#include <vector>
#include <unordered_map>
#include <Eigen/Geometry>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include "sophus/so3.hpp"
#include "sophus/interpolate.hpp"
#include "glog/logging.h"
#include <yaml-cpp/yaml.h>
// #include "yd_fusion_localization/global_defination/global_defination.h"

#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/PointCloud2.h>
#include "sensor_msgs/NavSatFix.h"
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/Image.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <nav_msgs/Odometry.h>
#include "geometry_msgs/TwistStamped.h"
#include "geometry_msgs/PoseStamped.h"
#include "nav_msgs/Path.h"
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

namespace yd_fusion_localization
{
    struct PointXYZT
    {
        // PCL_ADD_POINT4D;
        float x;
        float y;
        float z;
        float time;
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    } EIGEN_ALIGN16;

    struct PointPQIndT
    {
        float x;
        float y;
        float z;
        float qw;
        float qx;
        float qy;
        float qz;
        int index;
        double time;
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    } EIGEN_ALIGN16;

    struct PointXYZIRT
    {
        PCL_ADD_POINT4D
        PCL_ADD_INTENSITY;
        uint16_t ring;
        float time;
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        PointXYZIRT &operator=(const PointXYZIRT &rhs)
        {
            x = rhs.x;
            y = rhs.y;
            z = rhs.z;
            intensity = rhs.intensity;
            ring = rhs.ring;
            time = rhs.time;
            return *this;
        }
        PointXYZIRT &operator=(const pcl::PointXYZ &rhs)
        {
            x = rhs.x;
            y = rhs.y;
            z = rhs.z;
            intensity = 0;
            ring = 0;
            time = 0;
            return *this;
        }
        PointXYZIRT &operator=(const pcl::PointXYZI &rhs)
        {
            x = rhs.x;
            y = rhs.y;
            z = rhs.z;
            intensity = rhs.intensity;
            ring = 0;
            time = 0;
            return *this;
        }
        PointXYZIRT &operator=(const PointXYZT &rhs)
        {
            x = rhs.x;
            y = rhs.y;
            z = rhs.z;
            intensity = 0;
            ring = 0;
            time = rhs.time;
            return *this;
        }
    } EIGEN_ALIGN16;
} // namespace yd_fusion_localization

POINT_CLOUD_REGISTER_POINT_STRUCT(
    yd_fusion_localization::PointXYZT,
    (float, x, x)(float, y, y)(float, z, z)(float, time, time))

POINT_CLOUD_REGISTER_POINT_STRUCT(
    yd_fusion_localization::PointPQIndT,
    (float, x, x)(float, y, y)(float, z, z)(float, qw, qw)(float, qx, qx)(float, qy, qy)(float, qz, qz)(int, index, index)(double, time, time))

POINT_CLOUD_REGISTER_POINT_STRUCT(yd_fusion_localization::PointXYZIRT,
                                  (float, x, x)(float, y, y)(float, z, z)(float, intensity, intensity)(uint16_t, ring, ring)(float, time, time))

namespace yd_fusion_localization
{
    enum DiagnosticType
    {
        UpdateOk = 0,
        Stop,
        ReceivedInitialpose,
        Relocalization,
        JumpBackInTime,
        UpdateDelayed,
        WaitingForData,
        DeadReckoningFailed,
        InitWithLoopDetection,
        InitWithHistorypose,
        LoopFailed,
        LoopDetected,
        InitializeFailed1,
        InitializeFailed2,
        InitializeSucceeded1,
        InitializeSucceeded2,
        InitWindowDelayed,
        InitWindowCheckPositionFailed,
        InitWindowAddStateFailed1,
        InitWindowSucceeded,
        InitWindowFailed,
        BackEndFinishedIfFinishTrue,
        BackEndFinished,
        BackEndRunLDelayed,
        BackEndRunPDelayed,
        BackEndIfMeasureTrue,
        GetNStateNegative,
        BackEndMeasureFusionAddStateFalse,
        BackEndMeasureFusionOptimizeFalse,
        BackEndMeasureFusionSucceeded,
        BackEndIfPredictTrue,
        BackEndPredictFusionAddStateFalse,
        BackEndPredictFusionOptimizeFalse,
        BackEndPredictFusionSucceeded,
        FrontEndStatusFalse,
        BackEndIfInitedFalse,
        BackEndIfInitedTrue,
        FrontEndUpdateFalse,
        Finish,
        Finished,
        InitWindowAddStateSucceeded,
        WindowInited,
        InitializeFailed1Succeeded1,
        InitializeFailed1Failed1,
        InitWindowFailedInitIndexError,
        BackEndIfMeasureGetNStateFalse1,
        BackEndIfMeasureGetMStampFalse,
        BackEndIfMeasureGetNStateFalse2,
        BackEndIfMeasureGetMStateFalseWait,
        BackEndIfMeasureGetMStateFalseSetZero,
        BackEndMeasureNoLData,
        BackEndMeasureNoPData,
        BackEndIfPredictGetNStateFalse,
        BackEndIfPredictNoData,
        InitializeNoMeasure1,
        InitializeNoMeasure2,
        InitWindowNoMData1,
        InitWindowNoLData1,
        InitWindowNoPData1,
        InitWindowNoMData2,
        InitWindowNoLData2,
        InitWindowNoPData2,
        InitWindowAddStateFailed2,
    };

    enum StateType
    {
        None = 0,
        Basic,
        Vel,
        Bg,
        VBaBg,
    };

    struct State
    {
        State();
        State(const State &data);
        ~State() {}
        State &operator=(const State &rhs);

        StateType type;
        int index;
        double stamp;
        Eigen::Vector3d p;
        Eigen::Quaterniond q;
        Eigen::Vector3d v;
        Eigen::Vector3d ba;
        Eigen::Vector3d bg;
        Eigen::MatrixXd covariance;
    };

    struct BState
    {
        BState();
        BState(const BState &data);
        ~BState() {}
        BState &operator=(const BState &rhs);

        double stamp;
        Eigen::Vector3f linear_velocity;
        Eigen::Vector3f angular_velocity;
    };

    struct IState
    {
        IState();
        IState(const IState &data);
        ~IState() {}
        IState &operator=(const IState &rhs);

        double stamp;
        Sophus::SE3d pose;
    };

    struct MState
    {
        MState();
        MState(const MState &data);
        ~MState() {}
        MState &operator=(const MState &rhs);

        double stamp;
        Sophus::SE3d pose;
        Eigen::Vector3d linear_velocity;
        Eigen::Vector3d angular_velocity;
    };

    enum PredictType
    {
        Imu = 0,
        Twist,
        Odometry,
        Vg,
    };

    struct PredictData
    {
        PredictData();
        PredictData(const PredictData &data);
        virtual ~PredictData() {}
        PredictData &operator=(const PredictData &rhs);
        virtual PredictType GetType() const = 0;
        virtual void Clear() = 0;

        Sophus::SE3d sensor_to_robot;
        Eigen::VectorXd noise;
        std::deque<double> stamps;
    };
    typedef std::shared_ptr<PredictData> PredictDataPtr;

    struct ImuData : public PredictData
    {
        ImuData();
        ImuData(const ImuData &data);
        ~ImuData() {}
        ImuData &operator=(const ImuData &rhs);
        PredictType GetType() const override;
        void Clear() override;

        Eigen::Vector3d gravity;
        std::deque<Eigen::Vector3d> acc;
        std::deque<Eigen::Vector3d> gyro;
    };
    typedef std::shared_ptr<ImuData> ImuDataPtr;

    struct TwistData : public PredictData
    {
        TwistData();
        TwistData(const TwistData &data);
        ~TwistData() {}
        TwistData &operator=(const TwistData &rhs);
        PredictType GetType() const override;
        void Clear() override;

        std::deque<Eigen::Vector3d> vel;
        std::deque<Eigen::Vector3d> gyro;
    };
    typedef std::shared_ptr<TwistData> TwistDataPtr;

    struct OdometryData : public PredictData
    {
        OdometryData();
        OdometryData(const OdometryData &data);
        ~OdometryData() {}
        OdometryData &operator=(const OdometryData &rhs);
        PredictType GetType() const override;
        void Clear() override;

        std::deque<Sophus::SE3d> pose;
    };
    typedef std::shared_ptr<OdometryData> OdometryDataPtr;

    struct VgData : public PredictData
    {
        VgData();
        VgData(const VgData &data);
        ~VgData() {}
        VgData &operator=(const VgData &data);
        PredictType GetType() const override;
        void Clear() override;

        Sophus::SE3d sensor_to_robot_gyro;
        std::deque<Eigen::Vector3d> vel;
        std::deque<Eigen::Vector3d> gyro;
    };
    typedef std::shared_ptr<VgData> VgDataPtr;

    enum MeasureType
    {
        CloudM = 0,
        GNSSM,
        ImageM,
        PoseM,
        PM,
        QM,
    };

    struct MeasureData
    {
        MeasureData();
        MeasureData(const MeasureData &data);
        virtual ~MeasureData() {}
        MeasureData &operator=(const MeasureData &rhs);
        virtual MeasureType GetType() const = 0;

        Sophus::SE3d sensor_to_robot;
        Eigen::VectorXd noise;
        double stamp;
    };
    typedef std::shared_ptr<MeasureData> MeasureDataPtr;

    enum CloudPointType
    {
        XYZ = 0,
        XYZT,
        XYZI,
        XYZIRT,
    };
    struct CloudData : public MeasureData
    {
        CloudData();
        CloudData(const CloudData &data);
        ~CloudData() {}
        CloudData &operator=(const CloudData &rhs);
        MeasureType GetType() const override;

        CloudPointType point_type;
        pcl::PointCloud<pcl::PointXYZ>::Ptr xyz_ptr;
        pcl::PointCloud<PointXYZT>::Ptr xyzt_ptr;
        pcl::PointCloud<pcl::PointXYZI>::Ptr xyzi_ptr;
        pcl::PointCloud<PointXYZIRT>::Ptr xyzirt_ptr;
    };
    typedef std::shared_ptr<CloudData> CloudDataPtr;

    struct GNSSData : public MeasureData
    {
        GNSSData();
        GNSSData(const GNSSData &data);
        ~GNSSData() {}
        GNSSData &operator=(const GNSSData &rhs);
        MeasureType GetType() const override;

        double latitude;
        double longitude;
        double altitude;
        int status;
        int service;
    };
    typedef std::shared_ptr<GNSSData> GNSSDataPtr;

    struct ImageData : public MeasureData
    {
        ImageData();
        ImageData(const ImageData &data);
        ~ImageData() {}
        ImageData &operator=(const ImageData &rhs);
        MeasureType GetType() const override;

        cv_bridge::CvImageConstPtr image_ptr;
    };
    typedef std::shared_ptr<ImageData> ImageDataPtr;

    struct PoseData : public MeasureData
    {
        PoseData();
        PoseData(const PoseData &data);
        ~PoseData() {}
        PoseData &operator=(const PoseData &rhs);
        MeasureType GetType() const override;

        Sophus::SE3d pose;
    };
    typedef std::shared_ptr<PoseData> PoseDataPtr;

    struct PData : public MeasureData
    {
        PData();
        PData(const PData &data);
        ~PData() {}
        PData &operator=(const PData &rhs);
        MeasureType GetType() const override;

        Eigen::Vector3d p;
    };
    typedef std::shared_ptr<PData> PDataPtr;

    struct QData : public MeasureData
    {
        QData();
        QData(const QData &data);
        ~QData() {}
        QData &operator=(const QData &rhs);
        MeasureType GetType() const override;

        Eigen::Quaterniond q;
    };
    typedef std::shared_ptr<QData> QDataPtr;

    enum PreprocessedType
    {
        CloudXYZ = 0,
        CloudXYZI,
        CloudImage,
        CloudFeature,
        CloudPlane,
    };

    struct PreprocessedData
    {
        PreprocessedData();
        PreprocessedData(const PreprocessedData &data);
        virtual ~PreprocessedData() {}
        PreprocessedData &operator=(const PreprocessedData &rhs);
        virtual PreprocessedType GetType() const = 0;

        Sophus::SE3d sensor_to_robot;
        Eigen::VectorXd noise;
        double stamp;
    };
    typedef std::shared_ptr<PreprocessedData> PreprocessedDataPtr;

    struct CloudXYZData : public PreprocessedData
    {
        CloudXYZData();
        CloudXYZData(const CloudXYZData &data);
        ~CloudXYZData() {}
        CloudXYZData &operator=(const CloudXYZData &rhs);
        PreprocessedType GetType() const override;

        pcl::PointCloud<pcl::PointXYZ>::Ptr xyz_ptr;
    };
    typedef std::shared_ptr<CloudXYZData> CloudXYZDataPtr;

    struct CloudXYZIData : public PreprocessedData
    {
        CloudXYZIData();
        CloudXYZIData(const CloudXYZIData &data);
        ~CloudXYZIData() {}
        CloudXYZIData &operator=(const CloudXYZIData &rhs);
        PreprocessedType GetType() const override;

        pcl::PointCloud<pcl::PointXYZI>::Ptr xyzi_ptr;
    };
    typedef std::shared_ptr<CloudXYZIData> CloudXYZIDataPtr;

    struct CloudImageData : public PreprocessedData
    {
        CloudImageData();
        CloudImageData(const CloudImageData &data);
        ~CloudImageData() {}
        CloudImageData &operator=(const CloudImageData &rhs);
        PreprocessedType GetType() const override;

        pcl::PointCloud<pcl::PointXYZI>::Ptr xyzi_ptr;
        cv::Mat range_matrix;
        float ang_res_x;
        float ang_res_y;
        int rows;
        int cols;
    };
    typedef std::shared_ptr<CloudImageData> CloudImageDataPtr;

    struct CloudFeatureData : public PreprocessedData
    {
        CloudFeatureData();
        CloudFeatureData(const CloudFeatureData &data);
        ~CloudFeatureData() {}
        CloudFeatureData &operator=(const CloudFeatureData &rhs);
        PreprocessedType GetType() const override;

        std::vector<int32_t> startRingIndex;
        std::vector<int32_t> endRingIndex;
        std::vector<int32_t> pointColInd;
        std::vector<float> pointRange;
        pcl::PointCloud<pcl::PointXYZI>::Ptr xyzi_ptr;
        pcl::PointCloud<pcl::PointXYZI>::Ptr corner_ptr;
        pcl::PointCloud<pcl::PointXYZI>::Ptr surface_ptr;
    };
    typedef std::shared_ptr<CloudFeatureData> CloudFeatureDataPtr;

    struct CloudPlaneData : public PreprocessedData
    {
        CloudPlaneData();
        CloudPlaneData(const CloudPlaneData &data);
        ~CloudPlaneData() {}
        CloudPlaneData &operator=(const CloudPlaneData &rhs);
        PreprocessedType GetType() const override;

        pcl::PointCloud<pcl::PointXYZI>::Ptr xyzi_ptr;
        Eigen::Vector4f coeffs;
    };
    typedef std::shared_ptr<CloudPlaneData> CloudPlaneDataPtr;

    enum LandmarkType
    {
        PlaneM = 0,
    };

    struct LandmarkDataInFrame
    {
        LandmarkDataInFrame();
        LandmarkDataInFrame(const LandmarkDataInFrame &data);
        virtual ~LandmarkDataInFrame() {}
        LandmarkDataInFrame &operator=(const LandmarkDataInFrame &rhs);
        virtual LandmarkType GetType() const = 0;

        int index;
        Eigen::VectorXd noise;
    };
    typedef std::shared_ptr<LandmarkDataInFrame> LandmarkDataInFramePtr;

    struct PlaneLandmarkDataInFrame : public LandmarkDataInFrame
    {
        PlaneLandmarkDataInFrame();
        PlaneLandmarkDataInFrame(const PlaneLandmarkDataInFrame &data);
        ~PlaneLandmarkDataInFrame() {}
        PlaneLandmarkDataInFrame &operator=(const PlaneLandmarkDataInFrame &rhs);
        LandmarkType GetType() const override;

        Eigen::Vector4d coeff;
    };
    typedef std::shared_ptr<PlaneLandmarkDataInFrame> PlaneLandmarkDataInFramePtr;

    enum FrameDataType
    {
        Lidar = 0,
        TIO,
        VIO,
    };

    struct FrameData
    {
        FrameData();
        FrameData(const FrameData &data);
        virtual ~FrameData() {}
        FrameData &operator=(const FrameData &rhs);
        virtual FrameDataType GetType() const = 0;

        Sophus::SE3d sensor_to_robot;
        Eigen::VectorXd noise;
        double stamp;
        int index;
        Sophus::SE3d odometry;
    };
    typedef std::shared_ptr<FrameData> FrameDataPtr;

    struct LidarFrameData : public FrameData
    {
        LidarFrameData();
        LidarFrameData(const LidarFrameData &data);
        ~LidarFrameData() {}
        LidarFrameData &operator=(const LidarFrameData &rhs);
        FrameDataType GetType() const;

        pcl::PointCloud<pcl::PointXYZI>::Ptr xyzi_ptr;
        pcl::PointCloud<pcl::PointXYZI>::Ptr corner_ptr;
        pcl::PointCloud<pcl::PointXYZI>::Ptr surface_ptr;
        std::deque<PlaneLandmarkDataInFramePtr> plane;
    };
    typedef std::shared_ptr<LidarFrameData> LidarFrameDataPtr;

    struct TIOFrameData : public FrameData
    {
        TIOFrameData();
        TIOFrameData(const TIOFrameData &data);
        ~TIOFrameData() {}
        TIOFrameData &operator=(const TIOFrameData &rhs);
        FrameDataType GetType() const;

        StateType type;
        Eigen::Vector3d v;
        Eigen::Vector3d ba;
        Eigen::Vector3d bg;
    };
    typedef std::shared_ptr<TIOFrameData> TIOFrameDataPtr;

    struct LandmarkData
    {
        LandmarkData();
        LandmarkData(const LandmarkData &data);
        virtual ~LandmarkData() {}
        LandmarkData &operator=(const LandmarkData &rhs);
        virtual LandmarkType GetType() const = 0;

        int index;
        int frame_index;
        double stamp;
        Sophus::SE3d sensor_to_robot;
        Eigen::VectorXd noise;
    };
    typedef std::shared_ptr<LandmarkData> LandmarkDataPtr;

    struct PlaneLandmarkData : public LandmarkData
    {
        PlaneLandmarkData();
        PlaneLandmarkData(const PlaneLandmarkData &data);
        ~PlaneLandmarkData() {}
        PlaneLandmarkData &operator=(const PlaneLandmarkData &rhs);
        LandmarkType GetType() const override;

        Eigen::Vector4d coeff;
    };
    typedef std::shared_ptr<PlaneLandmarkData> PlaneLandmarkDataPtr;

    enum LoopDataType
    {
        LidarLoop = 0,
        ImageLoop,
    };

    struct LoopData
    {
        LoopData();
        LoopData(const LoopData &data);
        virtual ~LoopData() {}
        LoopData &operator=(const LoopData &rhs);

        Sophus::SE3d sensor_to_robot;
        Eigen::VectorXd noise;
        double stamp_prev;
        double stamp_next;
        int index_prev;
        int index_next;
        Sophus::SE3d relative_pose;
    };
    typedef std::shared_ptr<LoopData> LoopDataPtr;

    enum LocalizerType
    {
        PL = 0,
        QL,
        PoseL,
    };
    struct LocalizerData
    {
        LocalizerData();
        LocalizerData(const LocalizerData &data);
        virtual ~LocalizerData() {}
        LocalizerData &operator=(const LocalizerData &rhs);
        virtual LocalizerType GetType() const = 0;

        Sophus::SE3d sensor_to_robot;
        Sophus::SE3d local_to_map;
        Eigen::VectorXd noise;
        double stamp;
    };
    typedef std::shared_ptr<LocalizerData> LocalizerDataPtr;

    struct PLocalizerData : public LocalizerData
    {
        PLocalizerData();
        PLocalizerData(const PLocalizerData &data);
        ~PLocalizerData() {}
        PLocalizerData &operator=(const PLocalizerData &rhs);
        LocalizerType GetType() const override;

        Eigen::Vector3d p;
    };
    typedef std::shared_ptr<PLocalizerData> PLocalizerDataPtr;

    struct QLocalizerData : public LocalizerData
    {
        QLocalizerData();
        QLocalizerData(const QLocalizerData &data);
        ~QLocalizerData() {}
        QLocalizerData &operator=(const QLocalizerData &rhs);
        LocalizerType GetType() const override;

        Eigen::Quaterniond q;
    };
    typedef std::shared_ptr<QLocalizerData> QLocalizerDataPtr;

    struct PoseLocalizerData : public LocalizerData
    {
        PoseLocalizerData();
        PoseLocalizerData(const PoseLocalizerData &data);
        ~PoseLocalizerData() {}
        PoseLocalizerData &operator=(const PoseLocalizerData &rhs);
        LocalizerType GetType() const override;

        Sophus::SE3d pose;
    };
    typedef std::shared_ptr<PoseLocalizerData> PoseLocalizerDataPtr;

    enum PublishType
    {
        CloudP = 0,
        OdometryP,
        PoseP,
        PathP,
        CloudXYZIP,
        LoopP,
    };

    struct PublishData
    {
        PublishData();
        PublishData(const PublishData &data);
        virtual ~PublishData() {}
        PublishData &operator=(const PublishData &rhs) = delete;
        virtual PublishType GetType() const = 0;

        double stamp;
    };
    typedef std::shared_ptr<PublishData> PublishDataPtr;

    struct CloudPublishData : public PublishData
    {
        CloudPublishData();
        CloudPublishData(const CloudPublishData &data);
        ~CloudPublishData() {}
        CloudPublishData &operator=(const CloudPublishData &rhs) = delete;
        PublishType GetType() const override;

        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_ptr;
    };
    typedef std::shared_ptr<CloudPublishData> CloudPublishDataPtr;

    struct PosePublishData : public PublishData
    {
        PosePublishData();
        PosePublishData(const PosePublishData &data);
        ~PosePublishData() {}
        PosePublishData &operator=(const PosePublishData &rhs) = delete;
        PublishType GetType() const override;

        Sophus::SE3d pose;
        Eigen::Matrix<double, 6, 6> covariance;
    };
    typedef std::shared_ptr<PosePublishData> PosePublishDataPtr;

    struct OdometryPublishData : public PublishData
    {
        OdometryPublishData();
        OdometryPublishData(const OdometryPublishData &data);
        ~OdometryPublishData() {}
        OdometryPublishData &operator=(const OdometryPublishData &rhs) = delete;
        PublishType GetType() const override;

        Sophus::SE3d pose;
        Eigen::Matrix<double, 6, 1> twist;
        Eigen::Matrix<double, 6, 6> posecov;
        Eigen::Matrix<double, 6, 6> twistcov;
    };
    typedef std::shared_ptr<OdometryPublishData> OdometryPublishDataPtr;

    struct PathPublishData : public PublishData
    {
        PathPublishData();
        PathPublishData(const PathPublishData &data);
        ~PathPublishData() {}
        PathPublishData &operator=(const PathPublishData &rhs) = delete;
        PublishType GetType() const override;

        std::deque<std::pair<bool, Sophus::SE3d>> poses;
    };
    typedef std::shared_ptr<PathPublishData> PathPublishDataPtr;

    struct CloudXYZIPublishData : public PublishData
    {
        CloudXYZIPublishData();
        CloudXYZIPublishData(const CloudXYZIPublishData &data);
        ~CloudXYZIPublishData() {}
        CloudXYZIPublishData &operator=(const CloudXYZIPublishData &rhs) = delete;
        PublishType GetType() const override;

        pcl::PointCloud<pcl::PointXYZI>::Ptr xyzi_ptr;
    };
    typedef std::shared_ptr<CloudXYZIPublishData> CloudXYZIPublishDataPtr;

    struct LoopPublishData : public PublishData
    {
        LoopPublishData();
        LoopPublishData(const LoopPublishData &data);
        ~LoopPublishData() {}
        LoopPublishData &operator=(const LoopPublishData &rhs) = delete;
        PublishType GetType() const override;

        Sophus::SE3d key_pose_prev;
        Sophus::SE3d key_pose_next;
    };
    typedef std::shared_ptr<LoopPublishData> LoopPublishDataPtr;

    struct MessagePointer
    {
        sensor_msgs::PointCloud2::ConstPtr cloud_ptr;
        sensor_msgs::NavSatFixConstPtr gnss_ptr;
        sensor_msgs::ImuConstPtr imu_ptr;
        geometry_msgs::PoseWithCovarianceStampedConstPtr pose_ptr;
        nav_msgs::OdometryConstPtr odometry_ptr;
        geometry_msgs::TwistStampedConstPtr twist_ptr;
        sensor_msgs::ImageConstPtr image_ptr;
        geometry_msgs::PoseStampedConstPtr p_ptr;
        sensor_msgs::ImuConstPtr q_ptr;
    };
} // namespace yd_fusion_localization

#endif