/**
 * @file tic_toc.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-3-30
 * @brief tic toc
 */

#ifndef YD_FUSION_LOCALIZATION_TIC_TOC_HPP_
#define YD_FUSION_LOCALIZATION_TIC_TOC_HPP_

#include <ctime>
#include <cstdlib>
#include <chrono>
#include <iostream>
#include <string>

namespace yd_fusion_localization
{
    class TicToc
    {
    public:
        TicToc()
        {
            tic();
        }

        void tic()
        {
            start = std::chrono::system_clock::now();
        }

        double toc()
        {
            end = std::chrono::system_clock::now();
            std::chrono::duration<double> elapsed_seconds = end - start;
            start = std::chrono::system_clock::now();
            return elapsed_seconds.count();
        }

        void toc(const std::string &about_task)
        {
            end = std::chrono::system_clock::now();
            std::chrono::duration<double> elapsed_seconds = end - start;
            double elapsed_ms = elapsed_seconds.count() * 1000;

            std::cout.precision(3); // 10 for sec, 3 for ms
            std::cout << about_task << ": " << elapsed_ms << " msec." << std::endl;
        }

    private:
        std::chrono::time_point<std::chrono::system_clock> start, end;
    };
}
#endif