/**
 * @file fusion_graph.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-1-6
 * @brief fusion graph
 */

#ifndef YD_FUSION_LOCALIZATION_FUSION_GRAPH_HPP_
#define YD_FUSION_LOCALIZATION_FUSION_GRAPH_HPP_

#include "yd_fusion_localization/fusion/fusion_interface.hpp"
#include "yd_fusion_localization/models/graph_optimizer/g2o/graph_optimizer_g2o.hpp"

namespace yd_fusion_localization
{
    class FusionGraph : public FusionInterface
    {
    public:
        FusionGraph(const YAML::Node &yaml_node);
        ~FusionGraph() {}
        void Reset() override;
        bool AddState(std::deque<std::pair<bool, PredictDataPtr>> &predict_data, std::deque<std::pair<bool, LocalizerDataPtr>> &localizer_data) override;
        bool AddState(const State &state, std::deque<std::pair<bool, PredictDataPtr>> &predict_data, std::deque<std::pair<bool, LocalizerDataPtr>> &localizer_data) override;
        bool Optimize(std::vector<int> &predict_inlier, std::vector<int> &localizer_inlier, bool key_frame, State &result_state) override;
        bool WindowInited() const override;
        int GetStatesNum() const override;

    private:
        std::shared_ptr<GraphOptimizerInterface> graph_optimizer_ptr_;
    };
} // namespace yd_fusion_localization

#endif