#pragma once
#include "g2o/core/base_vertex.h"
#include "yd_fusion_localization/models/lidar_feature/plane3d.hpp"

namespace g2o
{
    class VertexPlane3d : public g2o::BaseVertex<3, yd_fusion_localization::Plane3D>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
        VertexPlane3d();
        virtual bool read(std::istream &is);

        virtual bool write(std::ostream &os) const;

        virtual void setToOriginImpl();

        virtual void oplusImpl(const double *update_);

        virtual int estimateDimension() const
        {
            return 3;
        }

        virtual int minimalEstimateDimension() const
        {
            return 3;
        }
    };
} // namespace g2o