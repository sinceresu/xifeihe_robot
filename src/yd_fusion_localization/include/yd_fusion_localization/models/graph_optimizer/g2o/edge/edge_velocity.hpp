#pragma once

#include "g2o/core/base_multi_edge.h"
#include "yd_fusion_localization/models/graph_optimizer/g2o/vertex/vertex_vec.hpp"
#include "yd_fusion_localization/models/graph_optimizer/g2o/vertex/vertex_q.hpp"
#include "yd_fusion_localization/utilities/data_conversions.hpp"

namespace g2o
{
    class EdgeVelocity : public BaseMultiEdge<3, Eigen::Vector3d>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        EdgeVelocity(const yd_fusion_localization::TwistData &data);
        virtual bool read(std::istream &is) { return false; }
        virtual bool write(std::ostream &os) const { return false; }
        void computeError();
        virtual void linearizeOplus();
        virtual void setMeasurement(const Eigen::Vector3d &m);
        std::vector<g2o::MatrixX::MapType, Eigen::aligned_allocator<g2o::MatrixX::MapType>> GetJacobian() const
        {
            return _jacobianOplus;
        }
    };
} // namespace g2o