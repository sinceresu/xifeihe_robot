#pragma once

#include "g2o/core/base_multi_edge.h"
#include "yd_fusion_localization/models/graph_optimizer/g2o/vertex/vertex_vec.hpp"
#include "yd_fusion_localization/models/graph_optimizer/g2o/vertex/vertex_q.hpp"
#include "yd_fusion_localization/models/preintegration/preintegration_vg_ex.hpp"

namespace g2o
{
    class EdgeVg : public BaseMultiEdge<9, std::shared_ptr<yd_fusion_localization::PreIntegrationVgEx>>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        EdgeVg();
        virtual bool read(std::istream &is) { return false; }
        virtual bool write(std::ostream &os) const { return false; }
        void computeError();
        virtual void linearizeOplus();
        virtual void setMeasurement(const std::shared_ptr<yd_fusion_localization::PreIntegrationVgEx> &m);
        std::vector<g2o::MatrixX::MapType, Eigen::aligned_allocator<g2o::MatrixX::MapType>> GetJacobian() const
        {
            return _jacobianOplus;
        }
    };
} // namespace g2o