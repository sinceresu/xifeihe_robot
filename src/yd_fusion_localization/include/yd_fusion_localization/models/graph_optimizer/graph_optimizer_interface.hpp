/**
 * @file graph_optimizer_interface.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-1-4
 * @brief graph optimizer interface
 */

#ifndef YD_FUSION_LOCALIZATION_GRAPH_OPTIMIZER_INTERFACE_HPP_
#define YD_FUSION_LOCALIZATION_GRAPH_OPTIMIZER_INTERFACE_HPP_

#include "yd_fusion_localization/models/graph_optimizer/g2o/edge/edge_pl.hpp"
#include "yd_fusion_localization/models/graph_optimizer/g2o/edge/edge_ql.hpp"
#include "yd_fusion_localization/models/graph_optimizer/g2o/edge/edge_posel.hpp"
#include "yd_fusion_localization/models/graph_optimizer/g2o/edge/edge_imu.hpp"
#include "yd_fusion_localization/models/graph_optimizer/g2o/edge/edge_odom.hpp"
#include "yd_fusion_localization/models/graph_optimizer/g2o/edge/edge_twist.hpp"
#include "yd_fusion_localization/models/graph_optimizer/g2o/edge/edge_vg.hpp"
#include "yd_fusion_localization/models/graph_optimizer/g2o/edge/edge_velocity.hpp"
#include "yd_fusion_localization/models/graph_optimizer/g2o/edge/edge_marginalization.hpp"
#include <Eigen/Dense>
#include <memory>

namespace yd_fusion_localization
{
    class GraphOptimizerInterface
    {
    public:
        GraphOptimizerInterface(const YAML::Node &yaml_node);
        virtual ~GraphOptimizerInterface() {}
        virtual void Reset() = 0;
        /// 优化
        virtual bool Optimize() = 0;
        /// 输入、输出数据
        virtual bool GetOptimizedState(std::deque<State> &optimized_state) = 0;
        virtual int GetNodeNum() const = 0;
        virtual int GetStatesNum() const = 0;
        /// 添加节点、边、鲁棒核
        virtual bool AddState(std::deque<std::pair<bool, PredictDataPtr>> &predict_data, std::deque<std::pair<bool, LocalizerDataPtr>> &localizer_data) = 0;
        virtual bool AddState(const State &state, std::deque<std::pair<bool, PredictDataPtr>> &predict_data, std::deque<std::pair<bool, LocalizerDataPtr>> &localizer_data) = 0;
        virtual bool Optimize(std::vector<int> &predict_inlier, std::vector<int> &localizer_inlier, bool key_frame, State &result_state) = 0;
        virtual bool WindowInited() const;

    protected:
        StateType state_type_;
        unsigned int state_num_;
        int window_size_;
        int max_state_num_;
        int max_node_num_;
        int max_edge_num_;
        int max_iterations_num_ = 512;
        double check_chi2_;
        std::vector<double> check_predictor_;
        std::vector<double> check_localizer_;
        double predictor_outlier_chi2_;
        double localizer_outlier_chi2_;

        std::deque<std::pair<int, double>> keys_;
        int edge_id_;

        int init_window_size_;
        bool marge_info_;
        bool compute_marginals_;
    };
} // namespace yd_fusion_localization
#endif