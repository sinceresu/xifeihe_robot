#pragma once
#include <ceres/ceres.h>
#include <ceres/rotation.h>
#include "yd_fusion_localization/models/lidar_feature/surface_normal_feature_distance.hpp"

namespace ceres
{
    class EdgeSurfNormalFeatureCeres : public ceres::SizedCostFunction<1, 7>
    {
    public:
        EdgeSurfNormalFeatureCeres(const yd_fusion_localization::SurfaceNormalFeatureDistance &surface_normal_feature);
        virtual ~EdgeSurfNormalFeatureCeres() {}
        virtual bool Evaluate(double const *const *parameters, double *residuals, double **jacobians) const;

        yd_fusion_localization::SurfaceNormalFeatureDistance surface_normal_feature_;
    };
}