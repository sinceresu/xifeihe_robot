#pragma once
#include "yd_fusion_localization/utilities/data_conversions.hpp"

namespace yd_fusion_localization
{
    class SurfaceNormalFeatureDistance
    {
    public:
        SurfaceNormalFeatureDistance(const Eigen::Vector3d &curr_point, const Eigen::Vector3d &plane_unit_norm, double negative_OA_dot_norm);
        virtual ~SurfaceNormalFeatureDistance() {}
        virtual double Error(const Eigen::Quaterniond &q_w_curr, const Eigen::Vector3d &t_w_curr) const;
        virtual bool Evaluate(double const *const *parameters, double *residuals, double **jacobians) const;

        Eigen::Vector3d curr_point_;
        Eigen::Vector3d plane_unit_norm_;
        double negative_OA_dot_norm_;
    };
}