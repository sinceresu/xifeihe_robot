/**
 * @file ndt_omp_registration.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-6-3
 * @brief ndt_omp registration
 */

#ifndef YD_FUSION_LOCALIZATION_NDTOMPREGISTRATION_HPP_
#define YD_FUSION_LOCALIZATION_NDTOMPREGISTRATION_HPP_

#include "yd_fusion_localization/models/registration/registration.hpp"
#include <pclomp/ndt_omp.h>

namespace yd_fusion_localization
{
    template<typename PointType>
    class NDTOMPRegistration : public Registration<PointType>
    {
    public:
        NDTOMPRegistration(const YAML::Node &node);
        NDTOMPRegistration(float res, float step_size, float trans_eps, int max_iter, std::string neighborhood_search_method, int num_threads);
        ~NDTOMPRegistration(){};
        bool SetInputTarget(const typename pcl::PointCloud<PointType>::Ptr &input_target) override;
        bool ScanMatch(double stamp,
                       const typename pcl::PointCloud<PointType>::Ptr &input_source,
                       typename pcl::PointCloud<PointType>::Ptr &result_cloud_ptr,
                       const Eigen::Matrix4f &predict_pose,
                       Eigen::Matrix4f &result_pose) override;
        float GetFitnessScore() override;

    private:
        bool SetRegistrationParam(float res, float step_size, float trans_eps, int max_iter, std::string neighborhood_search_method, int num_threads);

    private:
        typename pclomp::NormalDistributionsTransform<PointType, PointType>::Ptr ndt_omp_ptr_;
    };
} // namespace yd_fusion_localization

#endif