/**
 * @file tf_broadcaster.hpp
 * @author Su Jin
 * @version 1.0
 * @date 2022-7-21
 * @brief static tf broadcaster
 */


#ifndef YD_FUSION_LOCALIZATION_TF_STATIC_BROADCASTER_HPP_
#define YD_FUSION_LOCALIZATION_TF_STATIC_BROADCASTER_HPP_

#include "yd_fusion_localization/utilities/data_conversions.hpp"
#include <tf2_ros/static_transform_broadcaster.h>

#include <tf/transform_datatypes.h>
namespace yd_fusion_localization
{
    class TFStaticBroadCaster {
    public:
        TFStaticBroadCaster(std::string frame_id, std::string child_frame_id);
        void SendTransform(double stamp, const Sophus::SE3d& pose);
    protected:
        geometry_msgs::TransformStamped transform_;
        tf2_ros::StaticTransformBroadcaster broadcaster_;
        bool tf_static_published_ = false;
    };
}

#endif