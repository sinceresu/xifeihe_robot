/**
 * @file tf_broadcaster.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-29
 * @brief tf broadcaster
 */


#ifndef YD_FUSION_LOCALIZATION_TF_BROADCASTER_HPP_
#define YD_FUSION_LOCALIZATION_TF_BROADCASTER_HPP_

#include "yd_fusion_localization/utilities/data_conversions.hpp"
#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>
namespace yd_fusion_localization
{
    class TFBroadCaster {
    public:
        TFBroadCaster(std::string frame_id, std::string child_frame_id);
        void SendTransform(double stamp, const Sophus::SE3d& pose);
    protected:
        tf::StampedTransform transform_;
        tf::TransformBroadcaster broadcaster_;
    };
}

#endif