/**
 * @file tf_listener.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-15
 * @brief tf listener
 */

#ifndef YD_FUSION_LOCALIZATION_TF_LISTENER_HPP_
#define YD_FUSION_LOCALIZATION_TF_LISTENER_HPP_

#include <tf/transform_listener.h>

#include "yd_fusion_localization/utilities/data_conversions.hpp"

namespace yd_fusion_localization
{
    class TFListener
    {
    public:
        TFListener() = default;

        bool LookupTransform(const std::string &parent_frame_id, const std::string &child_frame_id, Sophus::SE3d &transform_isometry);

    private:
        tf::TransformListener listener_;
    };
} // namespace yd_fusion_localization

#endif