/**
 * @file preprocessor.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-6-2
 * @brief preprocessor
 */

#ifndef YD_FUSION_LOCALIZATION_PREPROCESSOR_HPP_
#define YD_FUSION_LOCALIZATION_PREPROCESSOR_HPP_

#include "yd_fusion_localization/utilities/data_conversions.hpp"

namespace yd_fusion_localization
{
    class Preprocessor
    {
    public:
        Preprocessor(const YAML::Node &yaml_node);
        virtual ~Preprocessor() {}
        virtual bool Preprocess(const MeasureDataPtr &data, const BState &bstate, PreprocessedDataPtr &preprocessed_data) = 0;
        virtual bool Preprocess(PreprocessedDataPtr &data1, PreprocessedDataPtr &data2) = 0;
    };
} // namespace yd_fusion_localization

#endif