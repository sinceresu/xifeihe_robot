/**
 * @file cloud_image_projection.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-6-2
 * @brief cloud_image_projection
 */

#ifndef YD_FUSION_LOCALIZATION_CLOUDIMAGEPROJECTION_HPP_
#define YD_FUSION_LOCALIZATION_CLOUDIMAGEPROJECTION_HPP_

#include "yd_fusion_localization/preprocessor/preprocessor.hpp"

namespace yd_fusion_localization
{
    class CloudImageProjection : public Preprocessor
    {
    public:
        CloudImageProjection(const YAML::Node &yaml_node);
        ~CloudImageProjection() {}
        bool Preprocess(const MeasureDataPtr &data, const BState &bstate, PreprocessedDataPtr &preprocessed_data) override;
        bool Preprocess(PreprocessedDataPtr &data1, PreprocessedDataPtr &data2) override;

    private:
        bool AddIRTtoPointCloud(const CloudDataPtr &cloud_data);
        void ProjectPointCloud(const CloudDataPtr &cloud_data, const BState &bstate, CloudImageDataPtr &cloud_image);

        template <typename PointT>
        bool RemovePointCloud(const pcl::PointCloud<PointT> &cloud_in, pcl::PointCloud<PointT> &cloud_out, float lower_bound, float upper_bound);

        template <typename PointT>
        bool AddIRTtoPointCloud(CloudPointType type, const pcl::PointCloud<PointT> &cloud_in, pcl::PointCloud<PointXYZIRT> &cloud_out);

        void DeskewPoint(const PointXYZIRT &pi, pcl::PointXYZI &po, const BState &bstate);

        std::vector<float> frame_distance_threshold_;
        int scan_number_;
        int horizon_scan_;
        double scan_period_;
    };
} // namespace yd_fusion_localization

#endif