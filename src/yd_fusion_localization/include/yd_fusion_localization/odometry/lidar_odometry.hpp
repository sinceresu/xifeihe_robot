/**
 * @file lidar_odometry.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-6-10
 * @brief lidar_odometry
 */

#ifndef YD_FUSION_LOCALIZATION_LIDARODOMETRY_HPP_
#define YD_FUSION_LOCALIZATION_LIDARODOMETRY_HPP_

#include "yd_fusion_localization/odometry/odometry.hpp"
#include <pcl/kdtree/kdtree_flann.h>
#include "yd_fusion_localization/models/cloud_filter/voxel_filter.hpp"
#include <unordered_set>
#include "yd_fusion_localization/models/graph_optimizer/ceres/vertex/vertex_se3_ceres.hpp"
#include "yd_fusion_localization/models/graph_optimizer/ceres/edge/edge_corner_feature_ceres.hpp"
#include "yd_fusion_localization/models/graph_optimizer/ceres/edge/edge_surface_normal_feature_ceres.hpp"

namespace yd_fusion_localization
{
    class LidarOdometry : public Odometry
    {
    public:
        LidarOdometry(const YAML::Node &yaml_node);
        ~LidarOdometry();
        void Reset() override;
        bool Initialize(const IState &istate, const std::deque<PredictDataPtr> &predict_data, const std::deque<LocalizerDataPtr> &local_data, const std::deque<PreprocessedDataPtr> &pre_data, FrameDataPtr &result_data) override;
        bool Handle(const IState &istate, const std::deque<PredictDataPtr> &predict_data, const std::deque<LocalizerDataPtr> &local_data, const std::deque<PreprocessedDataPtr> &pre_data, FrameDataPtr &result_data) override;
        bool PredictMState(const std::deque<PredictDataPtr> &predict_data, MState &mstate) override;
        double GetCurrentStamp() const override;
        pcl::PointCloud<pcl::PointXYZI>::Ptr GetCurrentScan() const;
        pcl::PointCloud<pcl::PointXYZI>::Ptr GetCornerLocalMap() const;
        pcl::PointCloud<pcl::PointXYZI>::Ptr GetSurfaceLocalMap() const;
        pcl::PointCloud<pcl::PointXYZI>::Ptr GetLocalMap();

    private:
        void AllocateMemory();
        bool CheckData(const std::deque<PredictDataPtr> &predict_data, const std::deque<LocalizerDataPtr> &local_data, const std::deque<PreprocessedDataPtr> &pre_data);
        void ParseData(const IState &istate, const CloudFeatureDataPtr &data, LidarFrameDataPtr &lidar_frame);
        bool ScanMatching(LidarFrameDataPtr &lidar_frame);
        void RegisterOnLocalMap(LidarFrameDataPtr &lidar_frame);
        bool IfKeyFrame(const LidarFrameDataPtr &lidar_frame);
        void SlideWindow();
        double FramePdistance(const LidarFrameDataPtr &a, const LidarFrameDataPtr &b);
        double FrameQdistance(const LidarFrameDataPtr &a, const LidarFrameDataPtr &b);
		int AddEdgeCorner(const pcl::PointCloud<pcl::PointXYZI>::Ptr& pc_in, const pcl::PointCloud<pcl::PointXYZI>::Ptr& map_in, ceres::Problem& problem, ceres::LossFunction *loss_function);
		int AddEdgeSurfaceNormal(const pcl::PointCloud<pcl::PointXYZI>::Ptr& pc_in, const pcl::PointCloud<pcl::PointXYZI>::Ptr& map_in, ceres::Problem& problem, ceres::LossFunction *loss_function);
        void PointAssociateToMap(pcl::PointXYZI const *const pi, pcl::PointXYZI *const po);

        double key_distance_thresh_;
        double key_angle_thresh_;
        double local_map_range_;
        int edge_min_num_thresh_;
        int surf_min_num_thresh_;
        double odom_trans_thresh_;
        double odom_rot_thresh_;
        int window_size_;
        double translation_iter_thresh_;
        double rotation_iter_thresh_;

        std::shared_ptr<CloudFilter<pcl::PointXYZI>> corner_filter_;
        std::shared_ptr<CloudFilter<pcl::PointXYZI>> surface_filter_;
        std::shared_ptr<CloudFilter<pcl::PointXYZI>> map_filter_;

        std::deque<LidarFrameDataPtr> key_frame_;

        pcl::PointCloud<pcl::PointXYZI>::Ptr corner_local_map_tmp_;
        pcl::PointCloud<pcl::PointXYZI>::Ptr surface_local_map_tmp_;
        pcl::PointCloud<pcl::PointXYZI>::Ptr local_map_tmp_;
        pcl::PointCloud<pcl::PointXYZI>::Ptr corner_local_map_;
        pcl::PointCloud<pcl::PointXYZI>::Ptr surface_local_map_;
        pcl::PointCloud<pcl::PointXYZI>::Ptr local_map_;
        pcl::KdTreeFLANN<pcl::PointXYZI>::Ptr kdtree_corner_local_map_;
        pcl::KdTreeFLANN<pcl::PointXYZI>::Ptr kdtree_surface_local_map_;
        int local_map_threads_num_;

		//optimization variable
		double parameters_[7] = {0, 0, 0, 1, 0, 0, 0};
		Eigen::Map<Eigen::Quaterniond> q_w_curr_ = Eigen::Map<Eigen::Quaterniond>(parameters_);
		Eigen::Map<Eigen::Vector3d> t_w_curr_ = Eigen::Map<Eigen::Vector3d>(parameters_ + 4);
        int iteration_num_;
    };
} // namespace yd_fusion_localization

#endif