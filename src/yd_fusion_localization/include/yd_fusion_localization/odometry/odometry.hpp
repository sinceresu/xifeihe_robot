/**
 * @file odometry.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-6-8
 * @brief odometry
 */

#ifndef YD_FUSION_LOCALIZATION_ODOMETRY_HPP_
#define YD_FUSION_LOCALIZATION_ODOMETRY_HPP_

#include "yd_fusion_localization/utilities/data_conversions.hpp"

namespace yd_fusion_localization
{
    class Odometry
    {
    public:
        Odometry(const YAML::Node &yaml_node);
        virtual ~Odometry() {}
        virtual void Reset() = 0;
        virtual bool Initialize(const IState &istate, const std::deque<PredictDataPtr> &predict_data, const std::deque<LocalizerDataPtr> &local_data, const std::deque<PreprocessedDataPtr> &pre_data, FrameDataPtr &result_data) = 0;
        virtual bool Handle(const IState &istate, const std::deque<PredictDataPtr> &predict_data, const std::deque<LocalizerDataPtr> &local_data, const std::deque<PreprocessedDataPtr> &pre_data, FrameDataPtr &result_data) = 0;
        virtual bool PredictMState(const std::deque<PredictDataPtr> &predict_data, MState &mstate) = 0;
        virtual double GetCurrentStamp() const = 0;
        virtual bool IfInited() const;
        virtual void SetInited(bool inited);
        virtual bool IfWindowInited() const;
        virtual void SetWindowInited(bool window_inited);

    protected:

        bool inited_;
        bool window_inited_;
    };
} // namespace yd_fusion_localization

#endif