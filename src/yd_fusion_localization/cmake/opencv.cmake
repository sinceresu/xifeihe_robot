find_package(OpenCV 3.0 REQUIRED)
include_directories(
  ${OpenCV_INCLUDE_DIRS})
message(STATUS "OpenCV_INCLUDE_DIRS:" ${OpenCV_INCLUDE_DIRS})
# message(STATUS "OpenCV_LIBS:" ${OpenCV_LIBS})
list(APPEND ALL_TARGET_LIBRARIES ${OpenCV_LIBS})
