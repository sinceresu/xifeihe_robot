/**
 * @file predictor.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-1-13
 * @brief predictor
 */

#include "yd_fusion_localization/sensor_bridge/predictor.hpp"

namespace yd_fusion_localization
{
    Predictor::Predictor(ros::NodeHandle &nh, const YAML::Node &yaml_node, const std::string &robot_frame)
    {
        std::string type = yaml_node["type"].as<std::string>();
        if (type == "Odometry")
        {
            type_ = PredictType::Odometry;
            subscriber_ = std::make_shared<OdometrySubscriber>(nh, yaml_node);
        }
        else if (type == "Twist")
        {
            type_ = PredictType::Twist;
            subscriber_ = std::make_shared<TwistSubscriber>(nh, yaml_node);
        }
        else if (type == "Imu")
        {
            type_ = PredictType::Imu;
            subscriber_ = std::make_shared<ImuSubscriber>(nh, yaml_node);
        }
        else if (type == "Vg")
        {
            type_ = PredictType::Vg;
            subscriber_ = std::make_shared<VgSubscriber>(nh, yaml_node);
        }
        else
        {
            LOG(ERROR) << "Predict Subscriber type error: " << type << "! Only support Odometry, Imu, Twist, or Vg!";
            return;
        }

        tf_broadcasters_.clear();
        if (subscriber_->sensor_frame_ != robot_frame && subscriber_->sensor_frame_ != "")
        {
            tf_broadcasters_.emplace_back(std::make_pair(std::make_shared<TFStaticBroadCaster>(robot_frame, subscriber_->sensor_frame_), subscriber_->sensor_to_robot_));

        }
        if (type_ == PredictType::Vg)
        {
            std::shared_ptr<VgSubscriber> sub = std::dynamic_pointer_cast<VgSubscriber>(subscriber_);
            std::string sensor2 = sub->sensor_frame_gyro_;
            if (sensor2 != robot_frame && sensor2 != "")
            {
                tf_broadcasters_.emplace_back(std::make_pair(std::make_shared<TFStaticBroadCaster>(robot_frame, sensor2), sub->sensor_to_robot_gyro_));

            }
        }
    }

    bool Predictor::HandleMessage(const MessagePointer &msg_ptr, const std::string &topic)
    {
        return subscriber_->HandleMessage(msg_ptr, topic);
    }

    void Predictor::ClearData()
    {
        subscriber_->ClearData();
    }

    PredictType Predictor::GetType() const
    {
        return type_;
    }

    PredictDataPtr Predictor::NewData() const
    {
        PredictDataPtr ptr;
        if (type_ == PredictType::Odometry)
        {
            ptr = std::make_shared<OdometryData>();
        }
        else if (type_ == PredictType::Twist)
        {
            ptr = std::make_shared<TwistData>();
        }
        else if (type_ == PredictType::Imu)
        {
            ptr = std::make_shared<ImuData>();
        }
        else if (type_ == PredictType::Vg)
        {
            ptr = std::make_shared<VgData>();
        }
        else
        {
            return nullptr;
        }
        ptr->sensor_to_robot = subscriber_->sensor_to_robot_;
        return ptr;
    }

    bool Predictor::HasData(double start_stamp, double end_stamp, PredictDataPtr &data)
    {
        return subscriber_->ValidData(start_stamp, end_stamp, data);
    }

    double Predictor::GetLatestData(double start_stamp, PredictDataPtr &data)
    {
        return subscriber_->GetLatestData(start_stamp, data);
    }

    bool Predictor::CheckData(const PredictDataPtr &data) const
    {
        return subscriber_->CheckData(data);
    }

    bool Predictor::CheckDRData(const PredictDataPtr &data) const
    {
        return subscriber_->CheckDRData(data);
    }

    void Predictor::Publish(double stamp)
    {
        for (auto &tf : tf_broadcasters_)
        {
            tf.first->SendTransform(stamp, tf.second);
        }
    }
} // namespace yd_fusion_localization
