/**
 * @file measurer.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-1-8
 * @brief measurer
 */

#include "yd_fusion_localization/sensor_bridge/measurer.hpp"
#include "yd_fusion_localization/localizer/gnss_localizer.hpp"

namespace yd_fusion_localization
{
    Measurer::Measurer(ros::NodeHandle &nh, const YAML::Node &yaml_node, const std::string &map_frame, const std::string &robot_frame)
    {
        std::string type = yaml_node["subscriber"]["type"].as<std::string>();
        if (type == "Cloud")
        {
            subscriber_ = std::make_shared<CloudSubscriber>(nh, yaml_node["subscriber"]);
            localizer_ = std::make_shared<CloudLocalizer>(yaml_node["localizer"]);
            ltype_ = LocalizerType::PoseL;
        }
        else if (type == "GNSS")
        {
            subscriber_ = std::make_shared<GNSSSubscriber>(nh, yaml_node["subscriber"]);
            localizer_ = std::make_shared<GNSSLocalizer>(yaml_node["localizer"]);
            ltype_ = LocalizerType::PL;
        }
        else if (type == "Pose")
        {
            subscriber_ = std::make_shared<PoseSubscriber>(nh, yaml_node["subscriber"]);
            localizer_ = std::make_shared<PoseLocalizer>(yaml_node["localizer"]);
            ltype_ = LocalizerType::PoseL;
        }
        else if (type == "Image")
        {
            subscriber_ = std::make_shared<ImageSubscriber>(nh, yaml_node["subscriber"]);
            localizer_ = std::make_shared<ImageLandmarkLocalizer>(yaml_node["localizer"]);
            ltype_ = LocalizerType::PoseL;
        }
        else if (type == "P")
        {
            subscriber_ = std::make_shared<PSubscriber>(nh, yaml_node["subscriber"]);
            localizer_ = std::make_shared<PLocalizer>(yaml_node["localizer"]);
            ltype_ = LocalizerType::PL;
        }
        else if (type == "Q")
        {
            subscriber_ = std::make_shared<QSubscriber>(nh, yaml_node["subscriber"]);
            localizer_ = std::make_shared<QLocalizer>(yaml_node["localizer"]);
            ltype_ = LocalizerType::QL;
        }
        else
        {
            LOG(ERROR) << "Measuremnt Subscriber type error: " << type << "! Only support Cloud, GNSS, Image, or Pose!";
            return;
        }

        ConfigurePublishers(nh, yaml_node["publisher"], localizer_->local_frame_, publishers_);

        tf_broadcasters_.clear();
        if (localizer_->local_frame_ != map_frame && localizer_->local_frame_ != "")
        {
            tf_broadcasters_.emplace_back(std::make_pair(std::make_shared<TFBroadCaster>(map_frame, localizer_->local_frame_), localizer_->local_to_map_));
        }
        if (subscriber_->sensor_frame_ != robot_frame && subscriber_->sensor_frame_ != "")
        {
            tf_broadcasters_.emplace_back(std::make_pair(std::make_shared<TFBroadCaster>(robot_frame, subscriber_->sensor_frame_), subscriber_->sensor_to_robot_));
        }

        mdata_ = NewMData();
        ldata_ = NewLData();
    }

    bool Measurer::HandleMessage(const MessagePointer &msg_ptr, const std::string &topic)
    {
        return subscriber_->HandleMessage(msg_ptr, topic);
    }
    void Measurer::ClearData()
    {
        subscriber_->ClearData();
    }
    void Measurer::Reset()
    {
        mdata_->stamp = -1.0;
        ldata_->stamp = -1.0;
        ldata_->noise[0] = DBL_MAX;
    }
    LocalizerType Measurer::GetLType() const
    {
        return ltype_;
    }
    MeasureDataPtr Measurer::NewMData() const
    {
        MeasureDataPtr data;
        if (subscriber_->GetType() == MeasureType::CloudM)
        {
            data = std::make_shared<CloudData>();
        }
        else if (subscriber_->GetType() == MeasureType::GNSSM)
        {
            data = std::make_shared<GNSSData>();
        }
        else if (subscriber_->GetType() == MeasureType::PoseM)
        {
            data = std::make_shared<PoseData>();
        }
        else if (subscriber_->GetType() == MeasureType::ImageM)
        {
            data = std::make_shared<ImageData>();
        }
        else if (subscriber_->GetType() == MeasureType::PM)
        {
            data = std::make_shared<PData>();
        }
        else if (subscriber_->GetType() == MeasureType::QM)
        {
            data = std::make_shared<QData>();
        }
        else
        {
            return nullptr;
        }
        data->sensor_to_robot = subscriber_->sensor_to_robot_;
        data->stamp = -1.0;
        return data;
    }

    LocalizerDataPtr Measurer::NewLData() const
    {
        LocalizerDataPtr data;
        if (ltype_ == LocalizerType::PoseL)
        {
            data = std::make_shared<PoseLocalizerData>();
            data->noise.resize(6);
        }
        else if (ltype_ == LocalizerType::PL)
        {
            data = std::make_shared<PLocalizerData>();
            data->noise.resize(3);
        }
        else if (ltype_ == LocalizerType::QL)
        {
            data = std::make_shared<QLocalizerData>();
            data->noise.resize(3);
        }
        else
        {
            return nullptr;
        }
        data->sensor_to_robot = subscriber_->sensor_to_robot_;
        data->local_to_map = localizer_->local_to_map_;
        data->noise[0] = DBL_MAX;
        data->stamp = -1.0;
        return data;
    }

    bool Measurer::GetLatestMStamp(double &stamp)
    {
        if (subscriber_->GetLatestData(mdata_))
        {
            stamp = mdata_->stamp;
            return true;
        }
        return false;
    }

    double Measurer::GetEarliestMStamp()
    {
        return subscriber_->GetEarliestStamp();
    }

    bool Measurer::HasMData(double start_stamp, double end_stamp, double &stamp)
    {
        if (std::fabs(mdata_->stamp - end_stamp) < 1e-5)
        {
            stamp = mdata_->stamp;
            return true;
        }
        if (subscriber_->HasData(start_stamp, end_stamp, stamp))
        {
            return true;
        }
        return false;
    }

    bool Measurer::HasLData(const MState &guess_mstate, LocalizerDataPtr &ldata)
    {
        if (std::fabs(mdata_->stamp - guess_mstate.stamp) >= 1e-5)
        {
            if (!subscriber_->ValidData(guess_mstate.stamp, mdata_))
            {
                return false;
            }
        }

        MState gmstate(guess_mstate);
        TransformRM2SL(subscriber_->sensor_to_robot_, localizer_->local_to_map_, gmstate);
        if (!localizer_->GetPose(mdata_, gmstate, ldata))
        {
            return false;
        }

        return true;
    }

    bool Measurer::HasLData(double stamp, LocalizerDataPtr &ldata)
    {
        if (std::fabs(mdata_->stamp - stamp) >= 1e-5)
        {
            if (!subscriber_->ValidData(stamp, mdata_))
            {
                return false;
            }
        }

        if (!localizer_->GetPose(mdata_, ldata))
        {
            return false;
        }

        return true;
    }

    bool Measurer::CheckPosition(const Sophus::SE3d &pose)
    {
        return localizer_->CheckPosition(pose);
    }

    void Measurer::LoopDetection(std::deque<Sophus::SE3d> &poses)
    {
        if (subscriber_->GetLatestData(mdata_))
        {
            std::deque<Sophus::SE3d> temps;
            if (localizer_->LoopDetection(mdata_, temps))
            {
                for (int i = 0; i < temps.size(); ++i)
                {
                    TransformSL2RM(subscriber_->sensor_to_robot_, localizer_->local_to_map_, temps[i]);
                    poses.push_back(temps[i]);
                }
            }
        }
    }

    bool Measurer::Initialize(const std::deque<MState> &mstates, LocalizerDataPtr &ldata)
    {
        if (mstates.empty())
        {
            return false;
        }
        Publish(mstates.front().stamp);
        if (std::fabs(mdata_->stamp - mstates.front().stamp) >= 1e-5)
        {
            if (!subscriber_->ValidData(mstates.front().stamp, mdata_))
            {
                return false;
            }
        }

        ldata->noise[0] = DBL_MAX;
        bool res = false;
        for (unsigned int i = 0; i < mstates.size(); ++i)
        {
            if (localizer_->CheckPosition(mstates[i].pose))
            {
                MState gmstate(mstates[i]);
                TransformRM2SL(subscriber_->sensor_to_robot_, localizer_->local_to_map_, gmstate);
                if (localizer_->Initialize(mdata_, gmstate, ldata_))
                {
                    res = true;
                    if (ldata_->noise[0] < ldata->noise[0])
                    {
                        CopyLocalizerData(ldata_, ldata);
                    }
                }
                Publish(mdata_->stamp, gmstate.pose);
            }
        }
        if (res)
        {
            Publish(mdata_->stamp, std::dynamic_pointer_cast<PoseLocalizerData>(ldata)->pose);
        }
        return res;
    }

    void Measurer::Publish(const State &state)
    {
        Sophus::SE3d pose(state.q, state.p);
        TransformRM2SL(subscriber_->sensor_to_robot_, localizer_->local_to_map_, pose);
        Publish(state.stamp, pose);
    }
    void Measurer::Publish(double stamp)
    {
        // for (auto &tf : tf_broadcasters_)
        // {
            // tf.first->SendTransform(stamp, tf.second);
        // }

        for (auto iterator = publishers_.begin(); iterator != publishers_.end(); iterator++)
        {
            if (iterator->first == "global_map")
            {
                if (iterator->second.first->HasSubscribers())
                {
                    std::shared_ptr<CloudLocalizer> cloud_localizer = std::dynamic_pointer_cast<CloudLocalizer>(localizer_);
                    pcl::PointCloud<pcl::PointXYZ>::Ptr global_map = cloud_localizer->GetGlobalMap();
                    CloudPublishDataPtr publish_data = std::dynamic_pointer_cast<CloudPublishData>(iterator->second.second);
                    publish_data->stamp = stamp;
                    publish_data->cloud_ptr = global_map;
                    iterator->second.first->Publish(iterator->second.second);
                }
            }
            else if (iterator->first == "local_map")
            {
                if (iterator->second.first->HasSubscribers())
                {
                    std::shared_ptr<CloudLocalizer> cloud_localizer = std::dynamic_pointer_cast<CloudLocalizer>(localizer_);
                    pcl::PointCloud<pcl::PointXYZ>::Ptr local_map = cloud_localizer->GetLocalMap();
                    CloudPublishDataPtr publish_data = std::dynamic_pointer_cast<CloudPublishData>(iterator->second.second);
                    publish_data->stamp = stamp;
                    publish_data->cloud_ptr = local_map;
                    iterator->second.first->Publish(iterator->second.second);
                }
            }
        }
    }
    void Measurer::PublishTF(double stamp)
    {
        for (auto &tf : tf_broadcasters_)
        {
            tf.first->SendTransform(stamp, tf.second);
        }
    }
    bool Measurer::Save(const MState &mstate)
    {
        if (std::fabs(mdata_->stamp - mstate.stamp) >= 1e-5)
        {
            if (!subscriber_->ValidData(mstate.stamp, mdata_))
            {
                return false;
            }
        }
        return localizer_->Save(mstate, mdata_);
    }
    bool Measurer::Save()
    {
        return localizer_->Save();
    }
    void Measurer::Publish(double stamp, const Sophus::SE3d &pose)
    {
        if (publishers_.find("current_scan") != publishers_.end())
        {
            std::pair<std::shared_ptr<Publisher>, PublishDataPtr> val = publishers_["current_scan"];
            if (val.first->HasSubscribers())
            {
                std::shared_ptr<CloudLocalizer> cloud_localizer = std::dynamic_pointer_cast<CloudLocalizer>(localizer_);
                pcl::PointCloud<pcl::PointXYZ>::Ptr current_scan = cloud_localizer->GetCurrentScan();
                pcl::PointCloud<pcl::PointXYZ>::Ptr pub_scan = boost::make_shared<pcl::PointCloud<pcl::PointXYZ>>();
                Eigen::Matrix4f trans = pose.matrix().cast<float>();
                pcl::transformPointCloud(*current_scan, *pub_scan, trans);
                CloudPublishDataPtr publish_data = std::dynamic_pointer_cast<CloudPublishData>(val.second);
                publish_data->stamp = stamp;
                publish_data->cloud_ptr = pub_scan;
                val.first->Publish(val.second);
            }
        }
    }
} // namespace yd_fusion_localization