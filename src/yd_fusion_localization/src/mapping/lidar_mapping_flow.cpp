/**
 * @file lidar_mapping_flow.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-7-6
 * @brief lidar mapping flow
 */

#include "yd_fusion_localization/mapping/lidar_mapping_flow.hpp"
#include "yd_fusion_localization/global_defination/global_defination.h"

namespace yd_fusion_localization
{
    LidarMappingFlow::LidarMappingFlow(ros::NodeHandle &nh, const std::string map_directory) :
        map_directory_(map_directory)
    {
        Configure(nh);
    }

    bool LidarMappingFlow::HandleMessage(const MessagePointer &msg_ptr, const std::string &topic)
    {
        bool res = false;
        for (int i = 0; i < predictors_.size(); ++i)
        {
            if (predictors_[i]->HandleMessage(msg_ptr, topic))
            {
                res = true;
                break;
            }
        }
        for (int i = 0; i < measurers_.size(); ++i)
        {
            if (measurers_[i]->HandleMessage(msg_ptr, topic))
            {
                return true;
            }
        }
        if (cloud_subscriber_->HandleMessage(msg_ptr, topic))
        {
            return true;
        }
        return res;
    }

    void LidarMappingFlow::Run()
    {
        ClearData();
        bool use_q = false;
        if (!measurers_.empty() && measurers_[0]->GetLType() == LocalizerType::QL)
        {
            use_q = true;
        }
        if (!IfInited())
        {
            LOG(INFO) << "LidarMappingFlow::Run Init!!!";
            Reset();
            cloud_subscriber_->GetAllData(cloud_datas_);
            while (cloud_datas_.size() > 2)
            {
                cloud_datas_.pop_front();
            }
            if (cloud_datas_.size() < 2)
            {
                return;
            }
            MeasureDataPtr data = cloud_datas_.front();
            cloud_datas_.pop_front();
            double curr_stamp = data->stamp;

            if (use_q)
            {
                if (!measurers_[0]->HasLData(curr_stamp, ldata_[0]))
                {
                    return;
                }
            }

            std::deque<LocalizerDataPtr> ldatas;
            std::vector<int> lhas;
            for (int i = 0; i < measurers_.size(); ++i)
            {
                if (!(use_q && i == 0))
                {
                    if (measurers_[i]->HasLData(curr_stamp, ldata_[i]))
                    {
                        ldatas.push_back(ldata_[i]);
                        lhas.push_back(i);
                    }
                }
            }

            BState bstate;
            bstate.stamp = curr_stamp;
            bstate.linear_velocity.setZero();
            bstate.angular_velocity.setZero();
            IState istate;
            istate.stamp = curr_stamp;
            istate.pose = Sophus::SE3d(Eigen::Quaterniond::Identity(), Eigen::Vector3d::Zero());

            PreprocessedDataPtr cloud_image = nullptr;
            if (!cloud_image_projection_->Preprocess(data, bstate, cloud_image))
            {
                return;
            }
            PreprocessedDataPtr cloud_feature = nullptr;
            if (!cloud_feature_extraction_->Preprocess(cloud_image, cloud_feature))
            {
                return;
            }

            FrameDataPtr frame = nullptr;
            if (use_q)
            {
                if (!lidar_odometry_->Initialize(istate, std::deque<PredictDataPtr>(), std::deque<LocalizerDataPtr>({ldata_[0]}), std::deque<PreprocessedDataPtr>({cloud_feature}), frame))
                {
                    return;
                }
            }
            else
            {
                if (!lidar_odometry_->Initialize(istate, std::deque<PredictDataPtr>(), std::deque<LocalizerDataPtr>(), std::deque<PreprocessedDataPtr>({cloud_feature}), frame))
                {
                    return;
                }
            }

            LidarFrameDataPtr lidar_frame = std::dynamic_pointer_cast<LidarFrameData>(frame);
            PublishLidarOdometry(lidar_frame);

            int ground_detection = -1;
            ros::param::get("/ground_detection", ground_detection);
            if (ground_detection >= 0)
            {
                PreprocessedDataPtr pre_ground = nullptr;
                if (cloud_ground_detection_->Preprocess(cloud_image, pre_ground))
                {
                    CloudPlaneDataPtr cloud_ground2 = std::dynamic_pointer_cast<CloudPlaneData>(pre_ground);
                    PlaneLandmarkDataInFramePtr ground_data = std::make_shared<PlaneLandmarkDataInFrame>();
                    ground_data->index = ground_detection;
                    ground_data->noise = cloud_ground2->noise;
                    ground_data->coeff = cloud_ground2->coeffs.cast<double>();
                    lidar_frame->plane.push_back(ground_data);
                    last_ground_istate_ = istate;
                    PublishGround(lidar_frame, cloud_ground2);
                }
            }

            LocalizerDataPtr odoml = std::make_shared<PoseLocalizerData>();
            PoseLocalizerDataPtr podoml = std::dynamic_pointer_cast<PoseLocalizerData>(odoml);
            podoml->stamp = curr_stamp;
            podoml->local_to_map = Sophus::SE3d(Eigen::Quaterniond::Identity(), Eigen::Vector3d::Zero());
            podoml->sensor_to_robot = frame->sensor_to_robot;
            podoml->noise = frame->noise;
            podoml->pose = frame->odometry;
            FrameDataPtr tio_frame = nullptr;
            if (!tio_odometry_->Initialize(istate, std::deque<PredictDataPtr>(), std::deque<LocalizerDataPtr>({odoml}), std::deque<PreprocessedDataPtr>(), tio_frame))
            {
                return;
            }

            if (lidar_mapping_->Initialize(ldatas, frame))
            {
                last_stamp_ = curr_stamp;
                SetInited(true);
            }
            PublishLidarMapping(lidar_frame);
            last_loop_istate_ = istate;

            if (use_q)
            {
                measurers_[0]->PublishTF(curr_stamp);
            }
            for (auto &index : lhas)
            {
                measurers_[index]->PublishTF(curr_stamp);
                last_measure_istate_[index] = istate;
            }
        }
        else
        {
            cloud_subscriber_->GetAllData(cloud_datas_);
            while (!cloud_datas_.empty())
            {
                MeasureDataPtr data = cloud_datas_.front();
                double curr_stamp = data->stamp;
                double ros_stamp = ros::Time::now().toSec();

                std::deque<PredictDataPtr> pdatas;
                std::vector<int> phas;
                for (int i = 0; i < predictors_.size(); ++i)
                {
                    if (predictors_[i]->HasData(last_stamp_, curr_stamp, pdata_[i]))
                    {
                        pdatas.push_back(pdata_[i]);
                        phas.push_back(i);
                    }
                    else if (ros_stamp < curr_stamp + 0.3)
                    {
                        return;
                    }
                }
                if (pdatas.empty())
                {
                    LOG(INFO) << "pdatas.empty()!!!";
                    // SetInited(false);
                    // return;
                }

                if (use_q)
                {
                    if (!measurers_[0]->HasLData(curr_stamp, ldata_[0]))
                    {
                        if (ros_stamp < curr_stamp + 0.1)
                        {
                            return;
                        }
                        else
                        {
                            LOG(INFO) << "ros_stamp > curr_stamp + 0.1!!!";
                            // SetInited(false);
                            // return;
                        }
                    }
                }

                MState mstate;
                mstate.stamp = curr_stamp;
                if (!tio_odometry_->PredictMState(pdatas, mstate))
                {
                    LOG(INFO) << "!tio_odometry_->PredictMState!!!";
                    // SetInited(false);
                    // return;
                }
                BState bstate;
                bstate.stamp = curr_stamp;
                bstate.linear_velocity = mstate.linear_velocity.cast<float>();
                bstate.angular_velocity = mstate.angular_velocity.cast<float>();
                IState istate;
                istate.stamp = curr_stamp;
                istate.pose = mstate.pose;

                std::deque<LocalizerDataPtr> ldatas;
                std::vector<int> lhas;
                for (int i = 0; i < measurers_.size(); ++i)
                {
                    if (!(use_q && i == 0))
                    {
                        if (IfMeasure(istate, i))
                        {
                            if (measurers_[i]->HasLData(curr_stamp, ldata_[i]))
                            {
                                ldatas.push_back(ldata_[i]);
                                lhas.push_back(i);
                            }
                            else if (ros_stamp < curr_stamp + 0.2)
                            {
                                return;
                            }
                        }
                    }
                }

                PreprocessedDataPtr cloud_image = nullptr;
                if (!cloud_image_projection_->Preprocess(data, bstate, cloud_image))
                {
                    LOG(INFO) << "!cloud_image_projection_!!!";
                    SetInited(false);
                    return;
                }
                PreprocessedDataPtr cloud_feature = nullptr;
                if (!cloud_feature_extraction_->Preprocess(cloud_image, cloud_feature))
                {
                    LOG(INFO) << "!cloud_feature_extraction_!!!";
                    SetInited(false);
                    return;
                }

                FrameDataPtr frame = nullptr;
                if (use_q)
                {
                    if (!lidar_odometry_->Handle(istate, std::deque<PredictDataPtr>(), std::deque<LocalizerDataPtr>({ldata_[0]}), std::deque<PreprocessedDataPtr>({cloud_feature}), frame))
                    {
                        LOG(INFO) << "!lidar_odometry_->Handle!!!";
                        SetInited(false);
                        return;
                    }
                }
                else
                {
                    if (!lidar_odometry_->Handle(istate, std::deque<PredictDataPtr>(), std::deque<LocalizerDataPtr>(), std::deque<PreprocessedDataPtr>({cloud_feature}), frame))
                    {
                        LOG(INFO) << "!lidar_odometry_->Handle!!!";
                        SetInited(false);
                        return;
                    }
                }
                
                cloud_datas_.pop_front();
                LidarFrameDataPtr lidar_frame = std::dynamic_pointer_cast<LidarFrameData>(frame);
                PublishLidarOdometry(lidar_frame);

                bool optimize = false;
                if (IfGroundDetection(istate) && frame->index >= 0)
                {
                    int ground_detection = -1;
                    ros::param::get("/ground_detection", ground_detection);
                    if (ground_detection >= 0)
                    {
                        PreprocessedDataPtr pre_ground = nullptr;
                        if (cloud_ground_detection_->Preprocess(cloud_image, pre_ground))
                        {
                            CloudPlaneDataPtr cloud_ground2 = std::dynamic_pointer_cast<CloudPlaneData>(pre_ground);
                            PlaneLandmarkDataInFramePtr ground_data = std::make_shared<PlaneLandmarkDataInFrame>();
                            ground_data->index = ground_detection;
                            ground_data->noise = cloud_ground2->noise;
                            ground_data->coeff = cloud_ground2->coeffs.cast<double>();
                            lidar_frame->plane.push_back(ground_data);
                            last_ground_istate_ = istate;
                            optimize = true;
                            PublishGround(lidar_frame, cloud_ground2);
                        }
                    }
                }
                if (!lhas.empty())
                {
                    optimize = true;
                }

                LocalizerDataPtr odoml = std::make_shared<PoseLocalizerData>();
                PoseLocalizerDataPtr podoml = std::dynamic_pointer_cast<PoseLocalizerData>(odoml);
                podoml->stamp = curr_stamp;
                podoml->local_to_map = Sophus::SE3d(Eigen::Quaterniond::Identity(), Eigen::Vector3d::Zero());
                podoml->sensor_to_robot = frame->sensor_to_robot;
                podoml->noise = frame->noise;
                podoml->pose = frame->odometry;
                FrameDataPtr tio_frame = nullptr;
                if (!tio_odometry_->Handle(istate, pdatas, std::deque<LocalizerDataPtr>({odoml}), std::deque<PreprocessedDataPtr>(), tio_frame))
                {
                    LOG(INFO) << "!tio_odometry_->Handle!!!";
                    SetInited(false);
                    return;
                }

                if (frame->index >= 0)
                {
                    if (lidar_mapping_->Handle(ldatas, frame))
                    {
                        last_stamp_ = curr_stamp;
                        if (optimize)
                        {
                            lidar_mapping_->Optimize();
                        }
                        bool save_map = false;
                        ros::param::get("/save_map", save_map);
                        if (save_map)
                        {
                            SaveMap();
                            ros::param::set("/save_map", false);
                        }
                    }
                    else
                    {
                        LOG(INFO) << "!lidar_mapping_->Handle!!!";
                        SetInited(false);
                        return;
                    }
                }

                for (auto &index : phas)
                {
                    predictors_[index]->Publish(curr_stamp);
                }
                if (use_q)
                {
                    measurers_[0]->PublishTF(curr_stamp);
                }
                for (auto &index : lhas)
                {
                    measurers_[index]->PublishTF(curr_stamp);
                    last_measure_istate_[index] = istate;
                }

                if (frame->index >= 0 && IfLoop(istate))
                {
                    LoopDataPtr loop_detect = nullptr;
                    if (distance_loop_->Detect(lidar_mapping_->GetKeyFrames(), frame, loop_detect))
                    {
                        LoopDataPtr loop_result = nullptr;
                        if (lidar_mapping_->Handle(loop_detect, loop_result))
                        {
                            if (lidar_mapping_->Optimize())
                            {
                                last_loop_istate_ = istate;
                                PublishLoop(loop_result);
                            }
                            else
                            {
                                LOG(INFO) << "!lidar_mapping_->Optimize!!!";
                                SetInited(false);
                                return;
                            }
                        }
                        else
                        {
                            LOG(INFO) << "!lidar_mapping_->Handle2!!!";
                            // SetInited(false);
                            // return;
                        }
                    }
                }
                PublishLidarMapping(lidar_frame);
            }
        }
    }

    void LidarMappingFlow::Finish()
    {
        SaveMap();
    }

    void LidarMappingFlow::PublishLidarOdometry(const LidarFrameDataPtr &frame)
    {
        if (currentscan_publisher_.first->HasSubscribers())
        {
            CloudXYZIPublishDataPtr publish_data = std::dynamic_pointer_cast<CloudXYZIPublishData>(currentscan_publisher_.second);
            publish_data->stamp = frame->stamp;
            publish_data->xyzi_ptr = lidar_odometry_->GetCurrentScan();
            currentscan_publisher_.first->Publish(currentscan_publisher_.second);
        }
        if (corner_localmap_publisher_.first->HasSubscribers())
        {
            CloudXYZIPublishDataPtr publish_data = std::dynamic_pointer_cast<CloudXYZIPublishData>(corner_localmap_publisher_.second);
            publish_data->stamp = frame->stamp;
            publish_data->xyzi_ptr = lidar_odometry_->GetCornerLocalMap();
            corner_localmap_publisher_.first->Publish(corner_localmap_publisher_.second);
        }
        if (surface_localmap_publisher_.first->HasSubscribers())
        {
            CloudXYZIPublishDataPtr publish_data = std::dynamic_pointer_cast<CloudXYZIPublishData>(surface_localmap_publisher_.second);
            publish_data->stamp = frame->stamp;
            publish_data->xyzi_ptr = lidar_odometry_->GetSurfaceLocalMap();
            surface_localmap_publisher_.first->Publish(surface_localmap_publisher_.second);
        }
        if (localmap_publisher_.first->HasSubscribers())
        {
            CloudXYZIPublishDataPtr publish_data = std::dynamic_pointer_cast<CloudXYZIPublishData>(localmap_publisher_.second);
            publish_data->stamp = frame->stamp;
            publish_data->xyzi_ptr = lidar_odometry_->GetLocalMap();
            localmap_publisher_.first->Publish(localmap_publisher_.second);
        }
        OdometryPublishDataPtr odom_pub = std::dynamic_pointer_cast<OdometryPublishData>(odom_publisher_.second);
        odom_pub->stamp = frame->stamp;
        odom_pub->pose = frame->odometry;
        odom_publisher_.first->Publish(odom_publisher_.second);
        robot2odom_tf_->SendTransform(frame->stamp, frame->odometry * frame->sensor_to_robot.inverse());
        if (sensor2robot_tf_)
        {
            sensor2robot_tf_->SendTransform(frame->stamp, frame->sensor_to_robot);
        }
    }

    void LidarMappingFlow::PublishGround(const LidarFrameDataPtr &frame, const CloudPlaneDataPtr &ground)
    {
        if (ground_publisher_.first->HasSubscribers())
        {
            CloudXYZIPublishDataPtr publish_data = std::dynamic_pointer_cast<CloudXYZIPublishData>(ground_publisher_.second);
            publish_data->stamp = frame->stamp;
            publish_data->xyzi_ptr = boost::make_shared<pcl::PointCloud<pcl::PointXYZI>>();
            pcl::transformPointCloud(*(ground->xyzi_ptr), *(publish_data->xyzi_ptr), frame->odometry.matrix().cast<float>());
            ground_publisher_.first->Publish(ground_publisher_.second);
        }
    }

    void LidarMappingFlow::PublishLidarMapping(const LidarFrameDataPtr &frame)
    {
        if (globalmap_publisher_.first->HasSubscribers())
        {
            CloudXYZIPublishDataPtr publish_data = std::dynamic_pointer_cast<CloudXYZIPublishData>(globalmap_publisher_.second);
            publish_data->stamp = frame->stamp;
            publish_data->xyzi_ptr = lidar_mapping_->GetGlobalMap();
            globalmap_publisher_.first->Publish(globalmap_publisher_.second);
        }
        PathPublishDataPtr publish_data = std::dynamic_pointer_cast<PathPublishData>(keyframe_publisher_.second);
        publish_data->stamp = frame->stamp;
        publish_data->poses = lidar_mapping_->GetOptimizedPose();
        keyframe_publisher_.first->Publish(keyframe_publisher_.second);
        odom2map_tf_->SendTransform(frame->stamp, lidar_mapping_->GetOdom2Map());
    }

    void LidarMappingFlow::PublishLoop(const LoopDataPtr &loop_data)
    {
        if (loop_publisher_.first->HasSubscribers())
        {
            LoopPublishDataPtr publish_data = std::dynamic_pointer_cast<LoopPublishData>(loop_publisher_.second);
            publish_data->stamp = loop_data->stamp_prev;
            Sophus::SE3d pose;
            if (!lidar_mapping_->GetOptimizedPose(loop_data->index_prev, pose))
            {
                return;
            }
            publish_data->key_pose_prev = pose;
            if (!lidar_mapping_->GetOptimizedPose(loop_data->index_next, pose))
            {
                return;
            }
            publish_data->key_pose_next = pose;
            loop_publisher_.first->Publish(loop_publisher_.second);
        }
    }

    void LidarMappingFlow::Reset()
    {
        // cloud_datas_.clear();
        last_stamp_ = -1.0;
        tio_odometry_->Reset();
        lidar_odometry_->Reset();
        lidar_mapping_->Reset();
        for (auto &ld : ldata_)
        {
            ld->stamp = -1.0;
        }
        last_ground_istate_.stamp = -1.0;
        for (auto &mi : last_measure_istate_)
        {
            mi.stamp = -1.0;
        }
        last_loop_istate_.stamp = -1.0;
    }

    void LidarMappingFlow::SetInited(bool inited)
    {
        inited_ = inited;
        tio_odometry_->SetInited(inited);
        lidar_odometry_->SetInited(inited);
    }

    bool LidarMappingFlow::IfInited()
    {
        return inited_;
    }

    void LidarMappingFlow::ClearData()
    {
        for (int i = 0; i < predictors_.size(); ++i)
        {
            predictors_[i]->ClearData();
        }
        for (int i = 0; i < measurers_.size(); ++i)
        {
            measurers_[i]->ClearData();
        }
        // cloud_subscriber_->ClearData();
    }

    void LidarMappingFlow::SaveMap()
    {
        lidar_mapping_->Optimize();
        lidar_mapping_->SaveMap(map_directory_);
    }

    void LidarMappingFlow::Configure(ros::NodeHandle &nh)
    {
        ros::param::set("/ground_detection", -1);
        ros::param::set("/save_map", false);
        std::string yaml_path = WORK_SPACE_PATH + "/config/mapping/lidar_mapping.yaml";
        YAML::Node yaml_node = YAML::LoadFile(yaml_path);
        std::string map_frame = yaml_node["map_frame"].as<std::string>();
        std::string odom_frame = yaml_node["odom_frame"].as<std::string>();
        std::string robot_frame = yaml_node["robot_frame"].as<std::string>();
        std::string sensor_frame = yaml_node["sensor_frame"].as<std::string>();
        ground_detect_distance_ = yaml_node["ground_detect_distance"].as<double>();
        ground_detect_angle_ = yaml_node["ground_detect_angle"].as<double>();
        ground_detect_time_ = yaml_node["ground_detect_time"].as<double>();
        loop_time_ = yaml_node["loop_time"].as<double>();
        ConfigurePublisher(nh, yaml_node["odom_publisher"], odom_frame, odom_publisher_);
        if (robot_frame == sensor_frame)
        {
            sensor2robot_tf_ = nullptr;
        }
        else
        {
            sensor2robot_tf_ = std::make_shared<TFBroadCaster>(robot_frame, sensor_frame);
        }

        robot2odom_tf_ = std::make_shared<TFBroadCaster>(odom_frame, robot_frame);
        odom2map_tf_ = std::make_shared<TFBroadCaster>(map_frame, odom_frame);
        ConfigurePublisher(nh, yaml_node["currentscan_publisher"], odom_frame, currentscan_publisher_);
        ConfigurePublisher(nh, yaml_node["ground_publisher"], odom_frame, ground_publisher_);
        ConfigurePublisher(nh, yaml_node["corner_localmap_publisher"], odom_frame, corner_localmap_publisher_);
        ConfigurePublisher(nh, yaml_node["surface_localmap_publisher"], odom_frame, surface_localmap_publisher_);
        ConfigurePublisher(nh, yaml_node["localmap_publisher"], odom_frame, localmap_publisher_);
        ConfigurePublisher(nh, yaml_node["globalmap_publisher"], map_frame, globalmap_publisher_);
        ConfigurePublisher(nh, yaml_node["keyframe_publisher"], map_frame, keyframe_publisher_);
        ConfigurePublisher(nh, yaml_node["loop_publisher"], map_frame, loop_publisher_);
        ConfigurePredictors(nh, yaml_node["predictors"], robot_frame);
        ConfigureMeasurers(nh, yaml_node["measurers"], map_frame, robot_frame);

        cloud_subscriber_ = std::make_shared<CloudSubscriber>(nh, yaml_node["cloud_subscriber"]);
        cloud_image_projection_ = std::make_shared<CloudImageProjection>(yaml_node["cloud_image_projection"]);
        cloud_feature_extraction_ = std::make_shared<CloudFeatureExtraction>(yaml_node["cloud_feature_extraction"]);
        cloud_ground_detection_ = std::make_shared<CloudGroundDetection>(yaml_node["cloud_ground_detection"]);

        tio_odometry_ = std::make_shared<TioOdometry>(yaml_node["tio_odometry"]);
        lidar_odometry_ = std::make_shared<LidarOdometry>(yaml_node["lidar_odometry"]);
        distance_loop_ = std::make_shared<DistanceLoop>(yaml_node["loop"]);
        lidar_mapping_ = std::make_shared<LidarMapping>(yaml_node["lidar_mapping"]);
        SetInited(false);
        Reset();
    }

    void LidarMappingFlow::ConfigurePredictors(ros::NodeHandle &nh, const YAML::Node &yaml_node, const std::string &robot_frame)
    {
        predictors_.clear();
        pdata_.clear();
        int num = yaml_node["number"].as<int>();
        for (int i = 0; i < num; ++i)
        {
            std::string index("no_");
            index = index + std::to_string(i + 1);
            std::shared_ptr<Predictor> predictor = std::make_shared<Predictor>(nh, yaml_node[index], robot_frame);
            predictors_.push_back(predictor);
            pdata_.push_back(predictor->NewData());
        }
    }

    void LidarMappingFlow::ConfigureMeasurers(ros::NodeHandle &nh, const YAML::Node &yaml_node, const std::string &map_frame, const std::string &robot_frame)
    {
        measurers_.clear();
        ldata_.clear();
        measure_distance_.clear();
        measure_angle_.clear();
        measure_time_.clear();
        last_measure_istate_.clear();
        int num = yaml_node["number"].as<int>();
        for (int i = 0; i < num; ++i)
        {
            std::string index("no_");
            index = index + std::to_string(i + 1);
            std::shared_ptr<Measurer> measurer = std::make_shared<Measurer>(nh, yaml_node[index], map_frame, robot_frame);
            measurers_.push_back(measurer);
            ldata_.push_back(measurer->NewLData());
            measure_distance_.push_back(yaml_node[index]["distance"].as<double>());
            measure_angle_.push_back(yaml_node[index]["angle"].as<double>());
            measure_time_.push_back(yaml_node[index]["time"].as<double>());
            last_measure_istate_.push_back(IState());
        }
    }

    bool LidarMappingFlow::IfGroundDetection(const IState &istate)
    {
        if (istate.stamp - last_ground_istate_.stamp > ground_detect_time_)
        {
            return true;
        }
        if ((istate.pose.translation() - last_ground_istate_.pose.translation()).norm() > ground_detect_distance_)
        {
            return true;
        }
        if (std::acos(std::fabs((last_ground_istate_.pose.unit_quaternion().inverse() * istate.pose.unit_quaternion()).w())) > ground_detect_angle_)
        {
            return true;
        }
        return false;
    }

    bool LidarMappingFlow::IfMeasure(const IState &istate, int index)
    {
        if (istate.stamp - last_measure_istate_[index].stamp > measure_time_[index])
        {
            return true;
        }
        if ((istate.pose.translation() - last_measure_istate_[index].pose.translation()).norm() > measure_distance_[index])
        {
            return true;
        }
        if (std::acos(std::fabs((last_measure_istate_[index].pose.unit_quaternion().inverse() * istate.pose.unit_quaternion()).w())) > measure_angle_[index])
        {
            return true;
        }
        return false;
    }

    bool LidarMappingFlow::IfLoop(const IState &istate)
    {
        if (istate.stamp - last_loop_istate_.stamp > loop_time_)
        {
            return true;
        }
        return false;
    }
}