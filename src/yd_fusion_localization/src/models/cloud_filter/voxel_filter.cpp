/**
 * @file voxel_filter.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-22
 * @brief voxel filter
 */

#include "yd_fusion_localization/models/cloud_filter/voxel_filter.hpp"

namespace yd_fusion_localization
{
    template<typename PointType>
    VoxelFilter<PointType>::VoxelFilter(const YAML::Node &node)
    {
        float leaf_size_x = node["leaf_size"][0].as<float>();
        float leaf_size_y = node["leaf_size"][1].as<float>();
        float leaf_size_z = node["leaf_size"][2].as<float>();

        SetFilterParam(leaf_size_x, leaf_size_y, leaf_size_z);
    }

    template<typename PointType>
    VoxelFilter<PointType>::VoxelFilter(float leaf_size_x, float leaf_size_y, float leaf_size_z)
    {
        SetFilterParam(leaf_size_x, leaf_size_y, leaf_size_z);
    }

    template<typename PointType>
    bool VoxelFilter<PointType>::SetFilterParam(float leaf_size_x, float leaf_size_y, float leaf_size_z)
    {
        voxel_filter_.setLeafSize(leaf_size_x, leaf_size_y, leaf_size_z);

        std::cout << "Voxel Filter 的参数为：" << std::endl
                  << leaf_size_x << ", "
                  << leaf_size_y << ", "
                  << leaf_size_z
                  << std::endl
                  << std::endl;

        return true;
    }

    template<typename PointType>
    bool VoxelFilter<PointType>::Filter(const typename pcl::PointCloud<PointType>::Ptr &input_cloud_ptr, typename pcl::PointCloud<PointType>::Ptr &filtered_cloud_ptr)
    {
        voxel_filter_.setInputCloud(input_cloud_ptr);
        voxel_filter_.filter(*filtered_cloud_ptr);
        return true;
    }
    template class VoxelFilter<pcl::PointXYZ>;
    template class VoxelFilter<pcl::PointXYZI>;
} // namespace yd_fusion_localization