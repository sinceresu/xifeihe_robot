/**
 * @file box_filter.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-22
 * @brief box filter
 */

#include <vector>
#include <iostream>

#include "yd_fusion_localization/models/cloud_filter/box_filter.hpp"

namespace yd_fusion_localization
{
    template<typename PointType>
    BoxFilter<PointType>::BoxFilter(const YAML::Node &node)
    {
        size_.resize(6);
        edge_.resize(6);
        origin_.resize(3);

        for (size_t i = 0; i < size_.size(); i++)
        {
            size_.at(i) = node["size"][i].as<float>();
        }
        SetSize(size_);
    }

    template<typename PointType>
    bool BoxFilter<PointType>::Filter(const typename pcl::PointCloud<PointType>::Ptr &input_cloud_ptr, typename pcl::PointCloud<PointType>::Ptr &filtered_cloud_ptr)
    {
        filtered_cloud_ptr->clear();
        pcl_box_filter_.setMin(Eigen::Vector4f(edge_.at(0), edge_.at(2), edge_.at(4), 1.0e-6));
        pcl_box_filter_.setMax(Eigen::Vector4f(edge_.at(1), edge_.at(3), edge_.at(5), 1.0e6));
        pcl_box_filter_.setInputCloud(input_cloud_ptr);
        pcl_box_filter_.filter(*filtered_cloud_ptr);

        return true;
    }

    template<typename PointType>
    void BoxFilter<PointType>::SetSize(std::vector<float> size)
    {
        size_ = size;
        std::cout << "Box Filter 的尺寸为：" << std::endl
                  << "min_x: " << size.at(0) << ", "
                  << "max_x: " << size.at(1) << ", "
                  << "min_y: " << size.at(2) << ", "
                  << "max_y: " << size.at(3) << ", "
                  << "min_z: " << size.at(4) << ", "
                  << "max_z: " << size.at(5)
                  << std::endl
                  << std::endl;

        CalculateEdge();
    }

    template<typename PointType>
    void BoxFilter<PointType>::SetOrigin(std::vector<float> origin)
    {
        origin_ = origin;
        CalculateEdge();
    }

    template<typename PointType>
    void BoxFilter<PointType>::CalculateEdge()
    {
        for (size_t i = 0; i < origin_.size(); ++i)
        {
            edge_.at(2 * i) = size_.at(2 * i) + origin_.at(i);
            edge_.at(2 * i + 1) = size_.at(2 * i + 1) + origin_.at(i);
        }
    }

    template<typename PointType>
    std::vector<float> BoxFilter<PointType>::GetEdge()
    {
        return edge_;
    }
    template class BoxFilter<pcl::PointXYZ>;
    template class BoxFilter<pcl::PointXYZI>;
} // namespace yd_fusion_localization