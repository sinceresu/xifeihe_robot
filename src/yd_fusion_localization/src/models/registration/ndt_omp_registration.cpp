/**
 * @file ndt_omp_registration.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-6-3
 * @brief ndt_omp registration
 */

#include "yd_fusion_localization/models/registration/ndt_omp_registration.hpp"

namespace yd_fusion_localization
{
    template<typename PointType>
    NDTOMPRegistration<PointType>::NDTOMPRegistration(const YAML::Node &node)
        : ndt_omp_ptr_(new pclomp::NormalDistributionsTransform<PointType, PointType>())
    {

        float res = node["res"].as<float>();
        float step_size = node["step_size"].as<float>();
        float trans_eps = node["trans_eps"].as<float>();
        int max_iter = node["max_iter"].as<int>();
        std::string neighborhood_search_method = node["neighborhood_search_method"].as<std::string>();
        int num_threads = node["num_threads"].as<int>();
        SetRegistrationParam(res, step_size, trans_eps, max_iter, neighborhood_search_method, num_threads);
    }

    template<typename PointType>
    NDTOMPRegistration<PointType>::NDTOMPRegistration(float res, float step_size, float trans_eps, int max_iter, std::string neighborhood_search_method, int num_threads)
        : ndt_omp_ptr_(new pclomp::NormalDistributionsTransform<PointType, PointType>())
    {

        SetRegistrationParam(res, step_size, trans_eps, max_iter, neighborhood_search_method, num_threads);
    }

    template<typename PointType>
    bool NDTOMPRegistration<PointType>::SetRegistrationParam(float res, float step_size, float trans_eps, int max_iter, std::string neighborhood_search_method, int num_threads)
    {
        ndt_omp_ptr_->setResolution(res);
        ndt_omp_ptr_->setStepSize(step_size);
        ndt_omp_ptr_->setTransformationEpsilon(trans_eps);
        ndt_omp_ptr_->setMaximumIterations(max_iter);
        ndt_omp_ptr_->setNumThreads(num_threads);
        if (neighborhood_search_method == "KDTREE")
        {
            ndt_omp_ptr_->setNeighborhoodSearchMethod(pclomp::KDTREE);
        }
        else if (neighborhood_search_method == "DIRECT1")
        {
            ndt_omp_ptr_->setNeighborhoodSearchMethod(pclomp::DIRECT1);
        }
        else
        {
            ndt_omp_ptr_->setNeighborhoodSearchMethod(pclomp::DIRECT7);
        }

        std::cout << "NDT 的匹配参数为：" << std::endl
                  << "res: " << res << ", "
                  << "step_size: " << step_size << ", "
                  << "trans_eps: " << trans_eps << ", "
                  << "max_iter: " << max_iter << ", "
                  << "neighborhood_search_method: " << neighborhood_search_method
                  << std::endl
                  << std::endl;

        return true;
    }

    template<typename PointType>
    bool NDTOMPRegistration<PointType>::SetInputTarget(const typename pcl::PointCloud<PointType>::Ptr &input_target)
    {
        ndt_omp_ptr_->setInputTarget(input_target);

        return true;
    }

    template<typename PointType>
    bool NDTOMPRegistration<PointType>::ScanMatch(double stamp,
                                       const typename pcl::PointCloud<PointType>::Ptr &input_source,
                                       typename pcl::PointCloud<PointType>::Ptr &result_cloud_ptr,
                                       const Eigen::Matrix4f &predict_pose,
                                       Eigen::Matrix4f &result_pose)
    {
        ndt_omp_ptr_->setInputSource(input_source);

        ndt_omp_ptr_->align(*result_cloud_ptr, predict_pose);
        result_pose = ndt_omp_ptr_->getFinalTransformation();

        return true;
    }

    template<typename PointType>
    float NDTOMPRegistration<PointType>::GetFitnessScore()
    {
        return ndt_omp_ptr_->getFitnessScore();
    }
    template class NDTOMPRegistration<pcl::PointXYZ>;
    template class NDTOMPRegistration<pcl::PointXYZI>;
} // namespace yd_fusion_localization