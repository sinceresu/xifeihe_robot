/**
 * @file graph_optimizer_g2o.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-1-4
 * @brief preintegration imu
 */

#include "yd_fusion_localization/models/graph_optimizer/g2o/graph_optimizer_g2o.hpp"
#include "glog/logging.h"
#include "boost/math/distributions/chi_squared.hpp"

namespace yd_fusion_localization
{
    GraphOptimizerG2o::GraphOptimizerG2o(const YAML::Node &yaml_node)
        : GraphOptimizerInterface(yaml_node)
    {
        robust_kernel_factory_ = g2o::RobustKernelFactory::instance();
        robust_kernel_name_ = yaml_node["robust_kernel_name"].as<std::string>();
        robust_kernel_delta_ = yaml_node["robust_kernel_delta"].as<double>();

        graph_ptr_.reset(new g2o::SparseOptimizer());
        std::string solver_type = yaml_node["solver_type"].as<std::string>();
        g2o::OptimizationAlgorithmFactory *solver_factory = g2o::OptimizationAlgorithmFactory::instance();
        g2o::OptimizationAlgorithmProperty solver_property;
        g2o::OptimizationAlgorithm *solver = solver_factory->construct(solver_type, solver_property);
        // solver_factory->listSolvers(std::cout);
        // g2o::OptimizationAlgorithm* solver = new g2o::OptimizationAlgorithmLevenberg(g2o::make_unique<g2o::BlockSolverX>(g2o::make_unique<g2o::LinearSolverCholmod<g2o::BlockSolverX::PoseMatrixType>>()));
        graph_ptr_->setAlgorithm(solver);
        if (!graph_ptr_->solver())
        {
            LOG(ERROR) << "G2O 优化器创建失败！";
            solver_factory->listSolvers(std::cout);
        }
        robust_predict_edges_.clear();
        robust_localizer_edges_.clear();
    }

    void GraphOptimizerG2o::Reset()
    {
        graph_ptr_->clear();
        keys_.clear();
        edge_id_ = 0;
        robust_predict_edges_.clear();
        robust_localizer_edges_.clear();
        margedge_ = nullptr;
    }

    bool GraphOptimizerG2o::Optimize()
    {
        if (graph_ptr_->edges().size() < 1)
        {
            return false;
        }

        graph_ptr_->initializeOptimization();
        graph_ptr_->computeInitialGuess();
        graph_ptr_->computeActiveErrors();
        graph_ptr_->setVerbose(false);
        graph_ptr_->optimize(max_iterations_num_);

        // double chi2 = graph_ptr_->chi2();
        // int iterations = graph_ptr_->optimize(max_iterations_num_);
        // double chi22 = graph_ptr_->chi2();
        // assert(!std::isnan(chi2));
        // assert(!std::isnan(chi22));
        // for (auto &it : graph_ptr_->edges())
        // {
        //     std::cout << dynamic_cast<g2o::OptimizableGraph::Edge *>(it)->chi2() << std::endl;
        // }

        // LOG(INFO) << std::endl
        //           << "------ 后端优化 -------" << std::endl
        //           << "顶点数：" << graph_ptr_->vertices().size() << ", 边数： " << graph_ptr_->edges().size() << std::endl
        //           << "迭代次数： " << iterations << "/" << max_iterations_num_ << std::endl
        //           << "优化前后误差变化：" << chi2 << "--->" << chi22
        //           << std::endl;

        // for (auto &it : graph_ptr_->edges())
        // {
        //     dynamic_cast<g2o::OptimizableGraph::Edge *>(it)->computeError();
        //     std::cout << dynamic_cast<g2o::OptimizableGraph::Edge *>(it)->id() << ", " << dynamic_cast<g2o::OptimizableGraph::Edge *>(it)->chi2() << std::endl;
        //     if (dynamic_cast<g2o::EdgeTwist *>(it))
        //     {
        //         std::cout << "EdgeTwist " << dynamic_cast<g2o::EdgeTwist *>(it)->error() << std::endl;
        //         std::cout << dynamic_cast<g2o::EdgeTwist *>(it)->information() << std::endl;
        //     }
        //     else if (dynamic_cast<g2o::EdgePoseL *>(it))
        //     {
        //         std::cout << "EdgePoseL " << dynamic_cast<g2o::EdgePoseL *>(it)->error() << std::endl;
        //         std::cout << dynamic_cast<g2o::EdgePoseL *>(it)->information() << std::endl;
        //     }
        //     else if (dynamic_cast<g2o::EdgeImu *>(it))
        //     {
        //         std::cout << "EdgeImu " << dynamic_cast<g2o::EdgeImu *>(it)->error() << std::endl;
        //         std::cout << dynamic_cast<g2o::EdgeImu *>(it)->information() << std::endl;
        //     }
        //     else if (dynamic_cast<g2o::EdgeOdom *>(it))
        //     {
        //         std::cout << "EdgeOdom " << dynamic_cast<g2o::EdgeOdom *>(it)->error() << std::endl;
        //         std::cout << dynamic_cast<g2o::EdgeOdom *>(it)->information() << std::endl;
        //     }
        //     else if (dynamic_cast<g2o::EdgeVg *>(it))
        //     {
        //         std::cout << "EdgeVg " << dynamic_cast<g2o::EdgeVg *>(it)->error() << std::endl;
        //         std::cout << dynamic_cast<g2o::EdgeVg *>(it)->information() << std::endl;
        //     }
        //     else if (dynamic_cast<g2o::EdgeMarginalization *>(it))
        //     {
        //         std::cout << "EdgeMarginalization " << dynamic_cast<g2o::EdgeMarginalization *>(it)->error() << std::endl;
        //         std::cout << dynamic_cast<g2o::EdgeMarginalization *>(it)->information() << std::endl;
        //     }
        //     else
        //     {
        //         std::cout << "Edge NoType " << std::endl;
        //     }
        // }
        return true;
    }

    bool GraphOptimizerG2o::GetOptimizedState(std::deque<State> &optimized_state)
    {
        optimized_state.clear();
        for (unsigned int i = 0; i < keys_.size(); i++)
        {
            State state;
            state.type = state_type_;
            state.index = keys_[i].first;
            state.stamp = keys_[i].second;
            int ind = (state.index * state_num_) % max_node_num_;
            g2o::VertexVec *vp = dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(ind));
            state.p = vp->estimate();
            ind = (ind + 1) % max_node_num_;
            g2o::VertexQ *vq = dynamic_cast<g2o::VertexQ *>(graph_ptr_->vertex(ind));
            state.q = vq->estimate();

            if (state_type_ == StateType::VBaBg)
            {
                ind = (ind + 1) % max_node_num_;
                Eigen::Vector3d v = (dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(ind)))->estimate();
                ind = (ind + 1) % max_node_num_;
                Eigen::Vector3d ba = (dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(ind)))->estimate();
                ind = (ind + 1) % max_node_num_;
                Eigen::Vector3d bg = (dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(ind)))->estimate();
                state.v = v;
                state.ba = ba;
                state.bg = bg;
            }
            else if (state_type_ == StateType::Basic)
            {
                // nothing
            }
            else if (state_type_ == StateType::Bg)
            {
                ind = (ind + 1) % max_node_num_;
                Eigen::Vector3d bg = (dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(ind)))->estimate();
                state.bg = bg;
            }
            else if (state_type_ == StateType::Vel)
            {
                ind = (ind + 1) % max_node_num_;
                Eigen::Vector3d v = (dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(ind)))->estimate();
                state.v = v;
            }
            optimized_state.push_back(state);
        }
        return true;
    }

    int GraphOptimizerG2o::GetNodeNum() const
    {
        return graph_ptr_->vertices().size();
    }

    int GraphOptimizerG2o::GetStatesNum() const
    {
        return keys_.size();
    }

    bool GraphOptimizerG2o::AddState(std::deque<std::pair<bool, PredictDataPtr>> &predict_data, std::deque<std::pair<bool, LocalizerDataPtr>> &localizer_data)
    {
        double stamp;
        Eigen::Vector3d p;
        Eigen::Quaterniond q;
        LOG(INFO) << "GraphOptimizerG2o::AddState1";
        for (int i = 0; i < predict_data.size(); ++i)
        {
            LOG(INFO) << "predict_data " << i << " is " << predict_data[i].first;
        }
        for (int i = 0; i < localizer_data.size(); ++i)
        {
            LOG(INFO) << "localizer_data " << i << " is " << localizer_data[i].first;
        }
        std::deque<std::pair<bool, LocalizerDataPtr>>::iterator fi = std::find_if(localizer_data.begin(), localizer_data.end(), [](const std::pair<bool, LocalizerDataPtr> &v) {
            if (!v.first)
            {
                return false;
            }
            return (v.second->GetType() == LocalizerType::PoseL);
        });
        if (fi == localizer_data.end())
        {
            LOG(INFO) << "GraphOptimizerG2o::AddState1 init localizer_data no posel!!!";
            fi = std::find_if(localizer_data.begin(), localizer_data.end(), [](const std::pair<bool, LocalizerDataPtr> &v) {
                if (!v.first)
                {
                    return false;
                }
                return (v.second->GetType() == LocalizerType::PL);
            });
            if (fi == localizer_data.end())
            {
                LOG(WARNING) << "GraphOptimizerG2o::AddState1 init localizer_data no pl!!!";
                return false;
            }
            else
            {
                PLocalizerDataPtr pl_data = std::dynamic_pointer_cast<PLocalizerData>(fi->second);
                p = pl_data->p;
            }
            fi = std::find_if(localizer_data.begin(), localizer_data.end(), [](const std::pair<bool, LocalizerDataPtr> &v) {
                if (!v.first)
                {
                    return false;
                }
                return (v.second->GetType() == LocalizerType::QL);
            });
            if (fi == localizer_data.end())
            {
                LOG(WARNING) << "GraphOptimizerG2o::AddState1 init localizer_data no ql!!!";
                return false;
            }
            else
            {
                stamp = fi->second->stamp;
                QLocalizerDataPtr ql_data = std::dynamic_pointer_cast<QLocalizerData>(fi->second);
                q = ql_data->q;
            }
        }
        else
        {
            stamp = fi->second->stamp;
            PoseLocalizerDataPtr posel_data = std::dynamic_pointer_cast<PoseLocalizerData>(fi->second);
            p = posel_data->pose.translation();
            q = posel_data->pose.so3().unit_quaternion();
        }

        if (!keys_.empty())
        {
            std::deque<std::pair<bool, PredictDataPtr>>::iterator fi2 = std::find_if(predict_data.begin(), predict_data.end(), [](const std::pair<bool, PredictDataPtr> &v) {
                return v.first;
            });
            if (fi2 == predict_data.end())
            {
                LOG(WARNING) << "GraphOptimizerG2o::AddState1 init no predict_data!!!";
                return false;
            }
        }

        State state;
        state.type = state_type_;
        state.stamp = stamp;
        state.p = p;
        state.q = q;
        state.v = Eigen::Vector3d::Zero();
        state.ba = Eigen::Vector3d::Zero();
        state.bg = Eigen::Vector3d::Zero();

        if (!AddNode(state, false))
        {
            return false;
        }

        AddEdges(predict_data, localizer_data);

        return true;
    }

    bool GraphOptimizerG2o::AddState(const State &state, std::deque<std::pair<bool, PredictDataPtr>> &predict_data, std::deque<std::pair<bool, LocalizerDataPtr>> &localizer_data)
    {
        LOG(INFO) << "GraphOptimizerG2o::AddState2";
        for (int i = 0; i < predict_data.size(); ++i)
        {
            LOG(INFO) << "predict_data " << i << " is " << predict_data[i].first;
        }
        for (int i = 0; i < localizer_data.size(); ++i)
        {
            LOG(INFO) << "localizer_data " << i << " is " << localizer_data[i].first;
        }
        std::deque<std::pair<bool, PredictDataPtr>>::iterator fi = std::find_if(predict_data.begin(), predict_data.end(), [](const std::pair<bool, PredictDataPtr> &v) { return v.first; });
        if (fi == predict_data.end())
        {
            LOG(WARNING) << "GraphOptimizerG2o::AddState2 predict_data empty!!!";
            return false;
        }
        if (!AddNode(state, false))
        {
            return false;
        }

        AddEdges(predict_data, localizer_data);

        // check outliers
        std::vector<int> inlier;
        RemoveEdgeOutlier(robust_predict_edges_.back(), inlier, check_predictor_);
        if (inlier.empty())
        {
            return false;
        }
        RemoveEdgeOutlier(robust_localizer_edges_.back(), inlier, check_localizer_);

        return true;
    }

    bool GraphOptimizerG2o::Optimize(std::vector<int> &predict_inlier, std::vector<int> &localizer_inlier, bool key_frame, State &result_state)
    {
        if (!Optimize())
        {
            return false;
        }

        for (int i = 0; i < robust_predict_edges_.size(); ++i)
        {
            RemoveEdgeOutlier(robust_predict_edges_[i], predict_inlier, predictor_outlier_chi2_);
            // if (i + 1 == robust_predict_edges_.size())
            // {
            //     if (predict_inlier.empty())
            //     {
            //         return false;
            //     }
            // }

            RemoveEdgeOutlier(robust_localizer_edges_[i], localizer_inlier, localizer_outlier_chi2_);

            RemoveEdgeRobustKernel(robust_predict_edges_[i]);
            RemoveEdgeRobustKernel(robust_localizer_edges_[i]);
        }
        robust_predict_edges_.clear();
        robust_localizer_edges_.clear();

        if (!Optimize())
        {
            return false;
        }

        if (!CheckChi2())
        {
            return false;
        }

        if (!GetStateWithCovariance(keys_.size() - 1, result_state))
        {
            return false;
        }

        if (!key_frame)
        {
            DeleteNewestState();
        }

        Marginalize();
        assert((result_state.type == StateType::Basic && result_state.covariance.cols() == 6) || (result_state.type == StateType::VBaBg && result_state.covariance.cols() == 15) || (result_state.type == StateType::Bg && result_state.covariance.cols() == 9) || (result_state.type == StateType::Vel && result_state.covariance.cols() == 9));

        return true;
    }

    void GraphOptimizerG2o::AddEdges(const std::deque<std::pair<bool, PredictDataPtr>> &predict_data, const std::deque<std::pair<bool, LocalizerDataPtr>> &localizer_data)
    {
        std::vector<std::vector<g2o::OptimizableGraph::Edge *>> predict_edges(predict_data.size());
        for (int i = 0; i < predict_data.size(); ++i)
        {
            if (predict_data[i].first)
            {
                AddEdgePredict(keys_[keys_.size() - 2].first, keys_.back().first, predict_data[i].second, true, predict_edges[i]);
            }
        }
        robust_predict_edges_.push_back(predict_edges);

        std::vector<std::vector<g2o::OptimizableGraph::Edge *>> localizer_edges(localizer_data.size());
        for (int i = 0; i < localizer_data.size(); ++i)
        {
            if (localizer_data[i].first)
            {
                AddEdgeLocalizer(keys_.back().first, localizer_data[i].second, true, localizer_edges[i]);
            }
        }
        robust_localizer_edges_.push_back(localizer_edges);
    }

    bool GraphOptimizerG2o::CheckChi2()
    {
        double chi2 = graph_ptr_->chi2();
        if (margedge_)
        {
            chi2 -= margedge_->chi2();
        }
        return chi2 < check_chi2_;
    }

    bool GraphOptimizerG2o::GetStateWithCovariance(int key_index, State &state)
    {
        state = GetState(key_index);
        if (!GetCovariance(key_index, state))
        {
            return false;
        }
        assert((state.type == StateType::Basic && state.covariance.cols() == 6) || (state.type == StateType::VBaBg && state.covariance.cols() == 15) || (state.type == StateType::Bg && state.covariance.cols() == 9) || (state.type == StateType::Vel && state.covariance.cols() == 9));
        return true;
    }

    State GraphOptimizerG2o::GetState(int key_index)
    {
        int state_index = keys_[key_index].first;
        int node_index = (state_index * state_num_) % max_node_num_;
        State state;
        state.index = state_index;
        state.stamp = keys_[key_index].second;
        state.type = state_type_;
        state.p = (dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(node_index)))->estimate();
        node_index = (node_index + 1) % max_node_num_;
        state.q = (dynamic_cast<g2o::VertexQ *>(graph_ptr_->vertex(node_index)))->estimate();

        if (state_type_ == StateType::VBaBg)
        {
            node_index = (node_index + 1) % max_node_num_;
            Eigen::Vector3d v = (dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(node_index)))->estimate();
            node_index = (node_index + 1) % max_node_num_;
            Eigen::Vector3d ba = (dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(node_index)))->estimate();
            node_index = (node_index + 1) % max_node_num_;
            Eigen::Vector3d bg = (dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(node_index)))->estimate();
            state.v = v;
            state.ba = ba;
            state.bg = bg;
        }
        else if (state_type_ == StateType::Basic)
        {
            // do nothing
        }
        else if (state_type_ == StateType::Bg)
        {
            node_index = (node_index + 1) % max_node_num_;
            Eigen::Vector3d bg = (dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(node_index)))->estimate();
            state.bg = bg;
        }
        else if (state_type_ == StateType::Vel)
        {
            node_index = (node_index + 1) % max_node_num_;
            Eigen::Vector3d v = (dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(node_index)))->estimate();
            state.v = v;
        }
        else
        {
            LOG(ERROR) << "GetState: type " << state_type_ << " not supported!!!";
        }
        return state;
    }

    bool GraphOptimizerG2o::GetCovariance(int key_index, State &state)
    {
        int state_index = keys_[key_index].first;
        int node_index = state_index * state_num_;
        std::vector<g2o::OptimizableGraph::Vertex *> vertices;
        g2o::SparseBlockMatrix<g2o::MatrixX> cov;
        int size = 0;
        std::vector<int> inds;
        std::vector<int> outs;
        for (int i = 0; i < state_num_; ++i)
        {
            g2o::OptimizableGraph::Vertex *v = graph_ptr_->vertex((node_index + i) % max_node_num_);
            int hi = v->hessianIndex();
            if (hi != -1)
            {
                inds.emplace_back(hi);
                vertices.emplace_back(v);
                size += v->dimension();
            }
            else
            {
                if (i == 0 || i == 1)
                {
                    return false;
                }
                else
                {
                    outs.emplace_back(i);
                }
            }
        }
        // for (auto &it : graph_ptr_->vertices())
        // {
        //     std::cout << it.second->id() << ", " << dynamic_cast<g2o::OptimizableGraph::Vertex *>(it.second)->hessianIndex() << std::endl;
        // }

        if (state_type_ == StateType::VBaBg)
        {
            if (outs.empty())
            {
                state.type = StateType::VBaBg;
            }
            else
            {
                auto fi = std::find(outs.begin(), outs.end(), 4);
                if (fi != outs.end())
                {
                    fi = std::find(outs.begin(), outs.end(), 2);
                    if (fi != outs.end())
                    {
                        state.type = StateType::Basic;
                    }
                    else
                    {
                        state.type = StateType::Vel;
                    }
                }
                else
                {
                    state.type = StateType::Bg;
                }
            }
        }
        else if (state_type_ == StateType::Vel)
        {
            if (outs.empty())
            {
                state.type = StateType::Vel;
            }
            else
            {
                assert(outs.size() == 1);
                assert(outs.front() == 2);
                state.type = StateType::Basic;
            }
        }
        else if (state_type_ == StateType::Bg)
        {
            if (outs.empty())
            {
                state.type = StateType::Bg;
            }
            else
            {
                assert(outs.size() == 1);
                assert(outs.front() == 2);
                state.type = StateType::Basic;
            }
        }

        state.covariance = Eigen::MatrixXd::Zero(size, size);
        if (compute_marginals_)
        {
            std::vector<std::pair<int, int>> indices;
            for (int i = 0; i < inds.size(); ++i)
            {
                for (int j = i; j < inds.size(); ++j)
                {
                    indices.emplace_back(std::make_pair(inds[i], inds[j]));
                    if (j != i)
                    {
                        indices.emplace_back(std::make_pair(inds[j], inds[i]));
                    }
                }
            }
            if (!graph_ptr_->computeMarginals(cov, indices))
            {
                state.type = StateType::Basic;
                state.covariance = Eigen::MatrixXd::Identity(6, 6) * 0.5;
                LOG(WARNING) << "GraphOptimizerG2o computeMarginals False!";
                return true;
                // return false;
            }
            for (int i = 0; i < vertices.size(); ++i)
            {
                for (int j = 0; j < vertices.size(); ++j)
                {
                    state.covariance.block(i * 3, j * 3, 3, 3) = *(cov.block(vertices[i]->hessianIndex(), vertices[j]->hessianIndex()));
                }
            }
        }
        // std::cout << "GraphOptimizerG2o::GetCovariance " << state.covariance(0, 0) << std::endl;
        // std::cout << "GraphOptimizerG2o::GetCovariance " << state.covariance << std::endl;
        return true;
    }

    bool GraphOptimizerG2o::AddNode(const State &state, bool need_fix)
    {
        int state_index;
        if (keys_.empty())
        {
            state_index = 0;
        }
        else
        {
            state_index = (keys_.back().first + 1) % max_state_num_;
            if (state.stamp - keys_.back().second < 1e-2)
            {
                LOG(ERROR) << "GraphOptimizerG2o::AddNode False! new state time diff is " << state.stamp - keys_.back().second;
                return false;
            }
        }
        keys_.push_back(std::make_pair(state_index, state.stamp));

        int ind = (state_index * state_num_) % max_node_num_;
        g2o::VertexVec *vp(new g2o::VertexVec());
        vp->setId(ind);
        vp->setEstimate(state.p);
        if (need_fix)
        {
            vp->setFixed(true);
        }
        graph_ptr_->addVertex(vp);

        ind = (ind + 1) % max_node_num_;
        g2o::VertexQ *vq(new g2o::VertexQ());
        vq->setId(ind);
        vq->setEstimate(state.q);
        if (need_fix)
        {
            vq->setFixed(true);
        }
        graph_ptr_->addVertex(vq);

        if (state_type_ == StateType::VBaBg)
        {
            ind = (ind + 1) % max_node_num_;
            g2o::VertexVec *vv(new g2o::VertexVec());
            vv->setId(ind);
            vv->setEstimate(state.v);
            graph_ptr_->addVertex(vv);

            ind = (ind + 1) % max_node_num_;
            g2o::VertexVec *vba(new g2o::VertexVec());
            vba->setId(ind);
            vba->setEstimate(state.ba);
            graph_ptr_->addVertex(vba);

            ind = (ind + 1) % max_node_num_;
            g2o::VertexVec *vbg(new g2o::VertexVec());
            vbg->setId(ind);
            vbg->setEstimate(state.bg);
            graph_ptr_->addVertex(vbg);
        }
        else if (state_type_ == StateType::Basic)
        {
            // nothing
        }
        else if (state_type_ == StateType::Bg)
        {
            ind = (ind + 1) % max_node_num_;
            g2o::VertexVec *vbg(new g2o::VertexVec());
            vbg->setId(ind);
            vbg->setEstimate(state.bg);
            graph_ptr_->addVertex(vbg);
        }
        else if (state_type_ == StateType::Vel)
        {
            ind = (ind + 1) % max_node_num_;
            g2o::VertexVec *vv(new g2o::VertexVec());
            vv->setId(ind);
            vv->setEstimate(state.v);
            graph_ptr_->addVertex(vv);
        }
        else
        {
            return false;
        }
        return true;
    }

    void GraphOptimizerG2o::AddEdgeLocalizer(int state_index, const LocalizerDataPtr &data, bool addrobustkernel, std::vector<g2o::OptimizableGraph::Edge *> &edges)
    {
        if (data->GetType() == LocalizerType::PL)
        {
            int node_index = (state_index * state_num_) % max_node_num_;
            g2o::VertexVec *vp = dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(node_index));
            node_index = (node_index + 1) % max_node_num_;
            g2o::VertexQ *vq = dynamic_cast<g2o::VertexQ *>(graph_ptr_->vertex(node_index));

            g2o::EdgePL *edge(new g2o::EdgePL());
            PLocalizerDataPtr pl_data = std::dynamic_pointer_cast<PLocalizerData>(data);
            edge->setMeasurement(*pl_data);
            edge->vertices()[0] = vp;
            edge->vertices()[1] = vq;

            edge->setId(edge_id_);
            edge_id_ = (edge_id_ + 1) % max_edge_num_;
            if (addrobustkernel)
            {
                AddRobustKernel(edge, robust_kernel_name_, robust_kernel_delta_);
            }
            graph_ptr_->addEdge(edge);
            edges.push_back(dynamic_cast<g2o::OptimizableGraph::Edge *>(edge));
        }
        else if (data->GetType() == LocalizerType::QL)
        {
            int node_index = (state_index * state_num_) % max_node_num_;
            node_index = (node_index + 1) % max_node_num_;
            g2o::VertexQ *vq = dynamic_cast<g2o::VertexQ *>(graph_ptr_->vertex(node_index));

            g2o::EdgeQL *edge(new g2o::EdgeQL());
            QLocalizerDataPtr ql_data = std::dynamic_pointer_cast<QLocalizerData>(data);
            edge->setMeasurement(*ql_data);
            edge->vertices()[0] = vq;

            edge->setId(edge_id_);
            edge_id_ = (edge_id_ + 1) % max_edge_num_;
            if (addrobustkernel)
            {
                AddRobustKernel(edge, robust_kernel_name_, robust_kernel_delta_);
            }
            graph_ptr_->addEdge(edge);
            edges.push_back(dynamic_cast<g2o::OptimizableGraph::Edge *>(edge));
        }
        else if (data->GetType() == LocalizerType::PoseL)
        {
            int node_index = (state_index * state_num_) % max_node_num_;
            g2o::VertexVec *vp = dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(node_index));
            node_index = (node_index + 1) % max_node_num_;
            g2o::VertexQ *vq = dynamic_cast<g2o::VertexQ *>(graph_ptr_->vertex(node_index));

            g2o::EdgePoseL *edge(new g2o::EdgePoseL());
            PoseLocalizerDataPtr posel_data = std::dynamic_pointer_cast<PoseLocalizerData>(data);
            edge->setMeasurement(*posel_data);
            edge->vertices()[0] = vp;
            edge->vertices()[1] = vq;

            edge->setId(edge_id_);
            edge_id_ = (edge_id_ + 1) % max_edge_num_;
            if (addrobustkernel)
            {
                AddRobustKernel(edge, robust_kernel_name_, robust_kernel_delta_);
            }
            graph_ptr_->addEdge(edge);
            edges.push_back(dynamic_cast<g2o::OptimizableGraph::Edge *>(edge));
        }
        else
        {
            LOG(ERROR) << "AddEdgeLocalizer fail! Wrong LocalizerType " << data->GetType();
            return;
        }
    }
    void GraphOptimizerG2o::AddEdgePredict(int state_index1, int state_index2, const PredictDataPtr &data, bool addrobustkernel, std::vector<g2o::OptimizableGraph::Edge *> &edges)
    {
        int node_index1 = (state_index1 * state_num_) % max_node_num_;
        g2o::VertexVec *vp1 = dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(node_index1));
        node_index1 = (node_index1 + 1) % max_node_num_;
        g2o::VertexQ *vq1 = dynamic_cast<g2o::VertexQ *>(graph_ptr_->vertex(node_index1));

        int node_index2 = (state_index2 * state_num_) % max_node_num_;
        g2o::VertexVec *vp2 = dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(node_index2));
        node_index2 = (node_index2 + 1) % max_node_num_;
        g2o::VertexQ *vq2 = dynamic_cast<g2o::VertexQ *>(graph_ptr_->vertex(node_index2));

        if (data->GetType() == PredictType::Odometry)
        {
            g2o::EdgeOdom *edge(new g2o::EdgeOdom());
            OdometryDataPtr odom_data = std::dynamic_pointer_cast<OdometryData>(data);
            std::shared_ptr<PreIntegrationOdomEx> pre = std::make_shared<PreIntegrationOdomEx>(*odom_data);
            edge->setMeasurement(pre);
            edge->vertices()[0] = vp1;
            edge->vertices()[1] = vq1;
            edge->vertices()[2] = vp2;
            edge->vertices()[3] = vq2;

            edge->setId(edge_id_);
            edge_id_ = (edge_id_ + 1) % max_edge_num_;
            if (addrobustkernel)
            {
                AddRobustKernel(edge, robust_kernel_name_, robust_kernel_delta_);
            }
            graph_ptr_->addEdge(edge);
            edges.push_back(dynamic_cast<g2o::OptimizableGraph::Edge *>(edge));
        }
        else if (data->GetType() == PredictType::Imu)
        {
            node_index1 = (node_index1 + 1) % max_node_num_;
            g2o::VertexVec *vv1 = dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(node_index1));
            node_index1 = (node_index1 + 1) % max_node_num_;
            g2o::VertexVec *vba1 = dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(node_index1));
            node_index1 = (node_index1 + 1) % max_node_num_;
            g2o::VertexVec *vbg1 = dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(node_index1));

            node_index2 = (node_index2 + 1) % max_node_num_;
            g2o::VertexVec *vv2 = dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(node_index2));
            node_index2 = (node_index2 + 1) % max_node_num_;
            g2o::VertexVec *vba2 = dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(node_index2));
            node_index2 = (node_index2 + 1) % max_node_num_;
            g2o::VertexVec *vbg2 = dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(node_index2));

            g2o::EdgeImu *edge(new g2o::EdgeImu());
            ImuDataPtr imu_data = std::dynamic_pointer_cast<ImuData>(data);
            std::shared_ptr<PreIntegrationImuEx> pre = std::make_shared<PreIntegrationImuEx>(*imu_data, vba1->estimate(), vbg1->estimate());
            edge->setMeasurement(pre);
            edge->vertices()[0] = vp1;
            edge->vertices()[1] = vq1;
            edge->vertices()[2] = vv1;
            edge->vertices()[3] = vba1;
            edge->vertices()[4] = vbg1;
            edge->vertices()[5] = vp2;
            edge->vertices()[6] = vq2;
            edge->vertices()[7] = vv2;
            edge->vertices()[8] = vba2;
            edge->vertices()[9] = vbg2;

            edge->setId(edge_id_);
            edge_id_ = (edge_id_ + 1) % max_edge_num_;
            if (addrobustkernel)
            {
                AddRobustKernel(edge, robust_kernel_name_, robust_kernel_delta_);
            }
            graph_ptr_->addEdge(edge);
            edges.push_back(dynamic_cast<g2o::OptimizableGraph::Edge *>(edge));
        }
        else if (data->GetType() == PredictType::Twist)
        {
            g2o::EdgeTwist *edge(new g2o::EdgeTwist());
            TwistDataPtr twist_data = std::dynamic_pointer_cast<TwistData>(data);
            std::shared_ptr<PreIntegrationTwistEx> pre = std::make_shared<PreIntegrationTwistEx>(*twist_data);
            edge->setMeasurement(pre);
            edge->vertices()[0] = vp1;
            edge->vertices()[1] = vq1;
            edge->vertices()[2] = vp2;
            edge->vertices()[3] = vq2;

            edge->setId(edge_id_);
            edge_id_ = (edge_id_ + 1) % max_edge_num_;
            if (addrobustkernel)
            {
                AddRobustKernel(edge, robust_kernel_name_, robust_kernel_delta_);
            }
            graph_ptr_->addEdge(edge);
            edges.push_back(dynamic_cast<g2o::OptimizableGraph::Edge *>(edge));

            if (state_type_ == StateType::VBaBg || state_type_ == StateType::Vel)
            {
                node_index2 = (node_index2 + 1) % max_node_num_;
                g2o::VertexVec *vv2 = dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(node_index2));
                g2o::EdgeVelocity *edge2(new g2o::EdgeVelocity(*twist_data));
                edge2->vertices()[0] = vq2;
                edge2->vertices()[1] = vv2;

                edge2->setId(edge_id_);
                edge_id_ = (edge_id_ + 1) % max_edge_num_;
                if (addrobustkernel)
                {
                    AddRobustKernel(edge2, robust_kernel_name_, robust_kernel_delta_);
                }
                graph_ptr_->addEdge(edge2);
                edges.push_back(dynamic_cast<g2o::OptimizableGraph::Edge *>(edge2));
            }
        }
        else if (data->GetType() == PredictType::Vg)
        {
            g2o::VertexVec *vbg1;
            g2o::VertexVec *vbg2;
            if (state_type_ == StateType::VBaBg)
            {
                node_index1 = (node_index1 + 3) % max_node_num_;
                vbg1 = dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(node_index1));

                node_index2 = (node_index2 + 3) % max_node_num_;
                vbg2 = dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(node_index2));
            }
            else if (state_type_ == StateType::Bg)
            {
                node_index1 = (node_index1 + 1) % max_node_num_;
                vbg1 = dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(node_index1));

                node_index2 = (node_index2 + 1) % max_node_num_;
                vbg2 = dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(node_index2));
            }
            else
            {
                LOG(ERROR) << "AddEdgePredict PredictType::Vg fail! Wrong StateType " << state_type_;
                return;
            }

            g2o::EdgeVg *edge(new g2o::EdgeVg());
            VgDataPtr vg_data = std::dynamic_pointer_cast<VgData>(data);
            std::shared_ptr<PreIntegrationVgEx> pre = std::make_shared<PreIntegrationVgEx>(*vg_data, vbg1->estimate());
            edge->setMeasurement(pre);
            edge->vertices()[0] = vp1;
            edge->vertices()[1] = vq1;
            edge->vertices()[2] = vbg1;
            edge->vertices()[3] = vp2;
            edge->vertices()[4] = vq2;
            edge->vertices()[5] = vbg2;

            edge->setId(edge_id_);
            edge_id_ = (edge_id_ + 1) % max_edge_num_;
            if (addrobustkernel)
            {
                AddRobustKernel(edge, robust_kernel_name_, robust_kernel_delta_);
            }
            graph_ptr_->addEdge(edge);
            edges.push_back(dynamic_cast<g2o::OptimizableGraph::Edge *>(edge));
        }
    }

    void GraphOptimizerG2o::DeleteNewestState()
    {
        if (keys_.empty())
        {
            return;
        }
        int node_index = keys_.back().first * state_num_;
        for (unsigned int i = 0; i < state_num_; ++i)
        {
            g2o::OptimizableGraph::Vertex *v = graph_ptr_->vertex((node_index + i) % max_node_num_);
            graph_ptr_->removeVertex(v, false);
        }
        keys_.pop_back();
    }

    void GraphOptimizerG2o::Marginalize()
    {
        if (keys_.size() <= window_size_)
        {
            return;
        }
        int node_index = keys_.front().first * state_num_;
        if (!marge_info_)
        {
            for (int i = 0; i < state_num_; ++i)
            {
                int ind = (node_index + i) % max_node_num_;
                g2o::HyperGraph::Vertex *v = graph_ptr_->vertex(ind);
                /// DeleteOldestState
                graph_ptr_->removeVertex(v, false);
            }
            keys_.pop_front();
            return;
        }
        std::vector<g2o::HyperGraph::Vertex *> margvertices;
        std::vector<g2o::HyperGraph::Edge *> margedges;
        /// find all the marge edges
        for (int i = 0; i < state_num_; ++i)
        {
            int ind = (node_index + i) % max_node_num_;
            g2o::HyperGraph::Vertex *v = graph_ptr_->vertex(ind);
            margvertices.push_back(v);
            for (auto &it : v->edges())
            {
                auto ei = std::find_if(margedges.begin(), margedges.end(), [it](const std::vector<g2o::HyperGraph::Edge *>::value_type &e) { return e->id() == it->id(); });
                if (ei == margedges.end())
                {
                    margedges.push_back(it);
                }
            }
        }
        /// find all the marge vertices
        for (auto &eit : margedges)
        {
            for (auto vit : eit->vertices())
            {
                auto vi = std::find_if(margvertices.begin(), margvertices.end(), [vit](const std::vector<g2o::HyperGraph::Vertex *>::value_type &v) { return v->id() == vit->id(); });
                if (vi == margvertices.end())
                {
                    margvertices.push_back(vit);
                }
            }
        }

        int m = state_num_ * 3;
        int pos = margvertices.size() * 3;
        int n = pos - m;
        if (n > 0)
        {
            Eigen::MatrixXd A(pos, pos);
            Eigen::VectorXd b(pos);
            A.setZero();
            b.setZero();
            for (auto it = margedges.begin(); it != margedges.end(); ++it)
            {
                std::vector<g2o::MatrixX::MapType, Eigen::aligned_allocator<g2o::MatrixX::MapType>> jacs;
                Eigen::MatrixXd info;
                Eigen::VectorXd error;
                if (dynamic_cast<g2o::EdgePL *>(*it))
                {
                    (dynamic_cast<g2o::EdgePL *>(*it))->computeError();
                    (dynamic_cast<g2o::EdgePL *>(*it))->linearizeOplus();
                    jacs = (dynamic_cast<g2o::EdgePL *>(*it))->GetJacobian();
                    info = (dynamic_cast<g2o::EdgePL *>(*it))->information();
                    error = (dynamic_cast<g2o::EdgePL *>(*it))->error();
                }
                else if (dynamic_cast<g2o::EdgeQL *>(*it))
                {
                    (dynamic_cast<g2o::EdgeQL *>(*it))->computeError();
                    (dynamic_cast<g2o::EdgeQL *>(*it))->linearizeOplus();
                    jacs = (dynamic_cast<g2o::EdgeQL *>(*it))->GetJacobian();
                    info = (dynamic_cast<g2o::EdgeQL *>(*it))->information();
                    error = (dynamic_cast<g2o::EdgeQL *>(*it))->error();
                }
                else if (dynamic_cast<g2o::EdgePoseL *>(*it))
                {
                    (dynamic_cast<g2o::EdgePoseL *>(*it))->computeError();
                    (dynamic_cast<g2o::EdgePoseL *>(*it))->linearizeOplus();
                    jacs = (dynamic_cast<g2o::EdgePoseL *>(*it))->GetJacobian();
                    info = (dynamic_cast<g2o::EdgePoseL *>(*it))->information();
                    error = (dynamic_cast<g2o::EdgePoseL *>(*it))->error();
                }
                else if (dynamic_cast<g2o::EdgeImu *>(*it))
                {
                    (dynamic_cast<g2o::EdgeImu *>(*it))->computeError();
                    (dynamic_cast<g2o::EdgeImu *>(*it))->linearizeOplus();
                    jacs = (dynamic_cast<g2o::EdgeImu *>(*it))->GetJacobian();
                    info = (dynamic_cast<g2o::EdgeImu *>(*it))->information();
                    error = (dynamic_cast<g2o::EdgeImu *>(*it))->error();
                }
                else if (dynamic_cast<g2o::EdgeOdom *>(*it))
                {
                    (dynamic_cast<g2o::EdgeOdom *>(*it))->computeError();
                    (dynamic_cast<g2o::EdgeOdom *>(*it))->linearizeOplus();
                    jacs = (dynamic_cast<g2o::EdgeOdom *>(*it))->GetJacobian();
                    info = (dynamic_cast<g2o::EdgeOdom *>(*it))->information();
                    error = (dynamic_cast<g2o::EdgeOdom *>(*it))->error();
                }
                else if (dynamic_cast<g2o::EdgeTwist *>(*it))
                {
                    (dynamic_cast<g2o::EdgeTwist *>(*it))->computeError();
                    (dynamic_cast<g2o::EdgeTwist *>(*it))->linearizeOplus();
                    jacs = (dynamic_cast<g2o::EdgeTwist *>(*it))->GetJacobian();
                    info = (dynamic_cast<g2o::EdgeTwist *>(*it))->information();
                    error = (dynamic_cast<g2o::EdgeTwist *>(*it))->error();
                }
                else if (dynamic_cast<g2o::EdgeVg *>(*it))
                {
                    (dynamic_cast<g2o::EdgeVg *>(*it))->computeError();
                    (dynamic_cast<g2o::EdgeVg *>(*it))->linearizeOplus();
                    jacs = (dynamic_cast<g2o::EdgeVg *>(*it))->GetJacobian();
                    info = (dynamic_cast<g2o::EdgeVg *>(*it))->information();
                    error = (dynamic_cast<g2o::EdgeVg *>(*it))->error();
                }
                else if (dynamic_cast<g2o::EdgeVelocity *>(*it))
                {
                    (dynamic_cast<g2o::EdgeVelocity *>(*it))->computeError();
                    (dynamic_cast<g2o::EdgeVelocity *>(*it))->linearizeOplus();
                    jacs = (dynamic_cast<g2o::EdgeVelocity *>(*it))->GetJacobian();
                    info = (dynamic_cast<g2o::EdgeVelocity *>(*it))->information();
                    error = (dynamic_cast<g2o::EdgeVelocity *>(*it))->error();
                }
                else if (dynamic_cast<g2o::EdgeMarginalization *>(*it))
                {
                    (dynamic_cast<g2o::EdgeMarginalization *>(*it))->computeError();
                    (dynamic_cast<g2o::EdgeMarginalization *>(*it))->linearizeOplus();
                    jacs = (dynamic_cast<g2o::EdgeMarginalization *>(*it))->GetJacobian();
                    info = (dynamic_cast<g2o::EdgeMarginalization *>(*it))->information();
                    error = (dynamic_cast<g2o::EdgeMarginalization *>(*it))->error();
                }
                else
                {
                    std::cerr << "Edge Error!!!" << std::endl;
                    return;
                }

                std::vector<g2o::HyperGraph::Vertex *> vertices((*it)->vertices());
                for (unsigned int i = 0; i < vertices.size(); ++i)
                {
                    int vid = vertices[i]->id();
                    auto vit = std::find_if(margvertices.begin(), margvertices.end(), [vid](const std::vector<g2o::HyperGraph::Vertex *>::value_type &v) { return v->id() == vid; });
                    if (vit == margvertices.end())
                    {
                        std::cerr << "vit error!!!" << std::endl;
                        return;
                    }
                    int index_i = std::distance(margvertices.begin(), vit);
                    for (unsigned int j = i; j < vertices.size(); ++j)
                    {
                        vid = vertices[j]->id();
                        vit = std::find_if(margvertices.begin(), margvertices.end(), [vid](const std::vector<g2o::HyperGraph::Vertex *>::value_type &v) { return v->id() == vid; });
                        if (vit == margvertices.end())
                        {
                            std::cerr << "vit error!!!" << std::endl;
                            return;
                        }
                        int index_j = std::distance(margvertices.begin(), vit);
                        if (i == j)
                        {
                            A.block(index_i * 3, index_j * 3, 3, 3) += jacs[i].transpose() * info * jacs[j];
                        }
                        else
                        {
                            A.block(index_i * 3, index_j * 3, 3, 3) += jacs[i].transpose() * info * jacs[j];
                            A.block(index_j * 3, index_i * 3, 3, 3) = A.block(index_i * 3, index_j * 3, 3, 3).transpose();
                        }
                    }
                    b.segment(index_i * 3, 3) += jacs[i].transpose() * info * error;
                }
            }

            Eigen::MatrixXd Amm = 0.5 * (A.block(0, 0, m, m) + A.block(0, 0, m, m).transpose());
            Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> saes(Amm);
            double eps = 1e-8;
            Eigen::MatrixXd Amm_inv = saes.eigenvectors() * Eigen::VectorXd((saes.eigenvalues().array() > eps).select(saes.eigenvalues().array().inverse(), 0)).asDiagonal() * saes.eigenvectors().transpose();

            Eigen::VectorXd bmm = b.segment(0, m);
            Eigen::MatrixXd Amr = A.block(0, m, m, n);
            Eigen::MatrixXd Arm = A.block(m, 0, n, m);
            Eigen::MatrixXd Arr = A.block(m, m, n, n);
            Eigen::VectorXd brr = b.segment(m, n);
            A = Arr - Arm * Amm_inv * Amr;
            b = brr - Arm * Amm_inv * bmm;

            Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> saes2(A);
            Eigen::MatrixXd A_inv = saes2.eigenvectors() * Eigen::VectorXd((saes2.eigenvalues().array() > eps).select(saes2.eigenvalues().array().inverse(), 0)).asDiagonal() * saes2.eigenvectors().transpose();

            Eigen::VectorXd minus_mu = A_inv * b;

            g2o::EdgeMarginalization *edge(new g2o::EdgeMarginalization());
            edge->setSize(n / 3);
            edge->setMeasurement(minus_mu);
            edge->setInformation(A);
            for (unsigned int i = state_num_; i < margvertices.size(); ++i)
            {
                if (dynamic_cast<g2o::VertexQ *>(margvertices[i]))
                {
                    edge->push_back(i - state_num_, (dynamic_cast<g2o::VertexQ *>(margvertices[i]))->estimate());
                }
                else if (dynamic_cast<g2o::VertexVec *>(margvertices[i]))
                {
                    edge->push_back(i - state_num_, (dynamic_cast<g2o::VertexVec *>(margvertices[i]))->estimate());
                }
                else
                {
                    std::cerr << "Vertex Error!!!" << std::endl;
                    return;
                }
                edge->vertices()[i - state_num_] = margvertices[i];
            }
            edge->setId(edge_id_);
            edge_id_ = (edge_id_ + 1) % max_edge_num_;
            graph_ptr_->addEdge(edge);
            margedge_ = edge;
        }
        /// DeleteOldestState
        for (unsigned int i = 0; i < state_num_; ++i)
        {
            graph_ptr_->removeVertex(margvertices[i], false);
        }
        keys_.pop_front();
    }

    void GraphOptimizerG2o::AddRobustKernel(g2o::OptimizableGraph::Edge *edge, const std::string &kernel_type, double kernel_delta)
    {
        if (kernel_type == "NONE")
        {
            return;
        }

        g2o::RobustKernel *kernel = robust_kernel_factory_->construct(kernel_type);
        if (kernel == nullptr)
        {
            std::cerr << "warning : invalid robust kernel type: " << kernel_type << std::endl;
            return;
        }

        kernel->setDelta(kernel_delta);
        edge->setRobustKernel(kernel);
    }

    void GraphOptimizerG2o::RemoveEdgeOutlier(std::vector<std::vector<g2o::OptimizableGraph::Edge *>> &edges, std::vector<int> &inlier, double outlier_chi2)
    {
        inlier.clear();
        for (int i = 0; i < edges.size(); ++i)
        {
            bool outlier = false;
            for (int j = 0; j < edges[i].size(); ++j)
            {
                if (edges[i][j] == nullptr)
                {
                    outlier = true;
                    continue;
                }
                // boost::math::chi_squared mydist(edges[i][j]->dimension());
                // double quantile = boost::math::quantile(mydist, prob);
                edges[i][j]->computeError();
                double chi2 = edges[i][j]->chi2();
                if (chi2 > outlier_chi2)
                {
                    if (dynamic_cast<g2o::EdgeVg *>(edges[i][j]))
                    {
                        LOG(INFO) << "EdgeVg " << chi2 << std::endl
                                  << dynamic_cast<g2o::EdgeVg *>(edges[i][j])->error().transpose() << std::endl;
                    }
                    else if (dynamic_cast<g2o::EdgeOdom *>(edges[i][j]))
                    {
                        LOG(INFO) << "EdgeOdom " << chi2 << std::endl
                                  << dynamic_cast<g2o::EdgeOdom *>(edges[i][j])->error().transpose() << std::endl;
                    }
                    else if (dynamic_cast<g2o::EdgeTwist *>(edges[i][j]))
                    {
                        LOG(INFO) << "EdgeTwist " << chi2 << std::endl
                                  << dynamic_cast<g2o::EdgeTwist *>(edges[i][j])->error().transpose() << std::endl;
                    }
                    else if (dynamic_cast<g2o::EdgeVelocity *>(edges[i][j]))
                    {
                        LOG(INFO) << "EdgeVelocity " << chi2 << std::endl
                                  << dynamic_cast<g2o::EdgeVelocity *>(edges[i][j])->error().transpose() << std::endl;
                    }
                    else if (dynamic_cast<g2o::EdgeImu *>(edges[i][j]))
                    {
                        LOG(INFO) << "EdgeImu " << chi2 << std::endl
                                  << dynamic_cast<g2o::EdgeImu *>(edges[i][j])->error().transpose() << std::endl;
                    }
                    else if (dynamic_cast<g2o::EdgePoseL *>(edges[i][j]))
                    {
                        LOG(INFO) << "EdgePoseL " << chi2 << std::endl
                                  << dynamic_cast<g2o::EdgePoseL *>(edges[i][j])->error().transpose() << std::endl;
                    }
                    else if (dynamic_cast<g2o::EdgePL *>(edges[i][j]))
                    {
                        LOG(INFO) << "EdgePL " << chi2 << std::endl
                                  << dynamic_cast<g2o::EdgePL *>(edges[i][j])->error().transpose() << std::endl;
                    }
                    else if (dynamic_cast<g2o::EdgeQL *>(edges[i][j]))
                    {
                        LOG(INFO) << "EdgeQL " << chi2 << std::endl
                                  << dynamic_cast<g2o::EdgeQL *>(edges[i][j])->error().transpose() << std::endl;
                    }
                    outlier = true;
                    graph_ptr_->removeEdge(dynamic_cast<g2o::HyperGraph::Edge *>(edges[i][j]));
                    edges[i][j] = nullptr;
                }
            }
            if (!outlier && !(edges[i].empty()))
            {
                inlier.push_back(i);
            }
        }
    }

    void GraphOptimizerG2o::RemoveEdgeOutlier(std::vector<std::vector<g2o::OptimizableGraph::Edge *>> &edges, std::vector<int> &inlier, const std::vector<double> &residuals)
    {
        inlier.clear();
        for (int i = 0; i < edges.size(); ++i)
        {
            bool outlier = false;
            for (int j = 0; j < edges[i].size(); ++j)
            {
                if (edges[i][j] == nullptr)
                {
                    outlier = true;
                    continue;
                }
                edges[i][j]->computeError();
                Eigen::VectorXd error;
                if (dynamic_cast<g2o::EdgeOdom *>(edges[i][j]))
                {
                    error = dynamic_cast<g2o::EdgeOdom *>(edges[i][j])->error();
                }
                else if (dynamic_cast<g2o::EdgeVg *>(edges[i][j]))
                {
                    error = dynamic_cast<g2o::EdgeVg *>(edges[i][j])->error();
                }
                else if (dynamic_cast<g2o::EdgeImu *>(edges[i][j]))
                {
                    error = dynamic_cast<g2o::EdgeImu *>(edges[i][j])->error();
                }
                else if (dynamic_cast<g2o::EdgeTwist *>(edges[i][j]))
                {
                    error = dynamic_cast<g2o::EdgeTwist *>(edges[i][j])->error();
                }
                else if (dynamic_cast<g2o::EdgeVelocity *>(edges[i][j]))
                {
                    error = Eigen::VectorXd::Zero(9);
                    error.tail(3) = dynamic_cast<g2o::EdgeVelocity *>(edges[i][j])->error();
                }
                else if (dynamic_cast<g2o::EdgePoseL *>(edges[i][j]))
                {
                    error = dynamic_cast<g2o::EdgePoseL *>(edges[i][j])->error();
                }
                else if (dynamic_cast<g2o::EdgePL *>(edges[i][j]))
                {
                    error = dynamic_cast<g2o::EdgePL *>(edges[i][j])->error();
                }
                else if (dynamic_cast<g2o::EdgeQL *>(edges[i][j]))
                {
                    error = dynamic_cast<g2o::EdgeQL *>(edges[i][j])->error();
                }
                else
                {
                    continue;
                }
                int size = std::min(int(error.rows()), int(residuals.size()));
                for (int k = 0; k < size; ++k)
                {
                    if (std::fabs(error[k]) > residuals[k])
                    {
                        if (dynamic_cast<g2o::EdgeVg *>(edges[i][j]))
                        {
                            LOG(INFO) << "EdgeVg " << dynamic_cast<g2o::EdgeVg *>(edges[i][j])->error().transpose() << std::endl;
                        }
                        else if (dynamic_cast<g2o::EdgeOdom *>(edges[i][j]))
                        {
                            LOG(INFO) << "EdgeOdom " << dynamic_cast<g2o::EdgeOdom *>(edges[i][j])->error().transpose() << std::endl;
                        }
                        else if (dynamic_cast<g2o::EdgeTwist *>(edges[i][j]))
                        {
                            LOG(INFO) << "EdgeTwist " << dynamic_cast<g2o::EdgeTwist *>(edges[i][j])->error().transpose() << std::endl;
                        }
                        else if (dynamic_cast<g2o::EdgeVelocity *>(edges[i][j]))
                        {
                            LOG(INFO) << "EdgeVelocity " << dynamic_cast<g2o::EdgeVelocity *>(edges[i][j])->error().transpose() << std::endl;
                        }
                        else if (dynamic_cast<g2o::EdgeImu *>(edges[i][j]))
                        {
                            LOG(INFO) << "EdgeImu " << dynamic_cast<g2o::EdgeImu *>(edges[i][j])->error().transpose() << std::endl;
                        }
                        else if (dynamic_cast<g2o::EdgePoseL *>(edges[i][j]))
                        {
                            LOG(INFO) << "EdgePoseL " << dynamic_cast<g2o::EdgePoseL *>(edges[i][j])->error().transpose() << std::endl;
                        }
                        else if (dynamic_cast<g2o::EdgePL *>(edges[i][j]))
                        {
                            LOG(INFO) << "EdgePL " << dynamic_cast<g2o::EdgePL *>(edges[i][j])->error().transpose() << std::endl;
                        }
                        else if (dynamic_cast<g2o::EdgeQL *>(edges[i][j]))
                        {
                            LOG(INFO) << "EdgeQL " << dynamic_cast<g2o::EdgeQL *>(edges[i][j])->error().transpose() << std::endl;
                        }
                        outlier = true;
                        graph_ptr_->removeEdge(dynamic_cast<g2o::HyperGraph::Edge *>(edges[i][j]));
                        edges[i][j] = nullptr;
                    }
                }
            }
            if (!outlier && !(edges[i].empty()))
            {
                inlier.push_back(i);
            }
        }
    }

    void GraphOptimizerG2o::RemoveEdgeRobustKernel(std::vector<std::vector<g2o::OptimizableGraph::Edge *>> &edges)
    {
        for (int i = 0; i < edges.size(); ++i)
        {
            for (int j = 0; j < edges[i].size(); ++j)
            {
                if (edges[i][j])
                {
                    edges[i][j]->setRobustKernel(nullptr);
                }
            }
        }
    }
} // namespace yd_fusion_localization