#include "yd_fusion_localization/models/graph_optimizer/g2o/edge/edge_ql.hpp"

namespace g2o
{
    EdgeQL::EdgeQL() : BaseMultiEdge<3, yd_fusion_localization::QLocalizerData>()
    {
        resize(1);
    }
    void EdgeQL::computeError()
    {
        Eigen::Quaterniond Qi = (dynamic_cast<VertexQ *>(_vertices[0]))->estimate();

        _error = 2 * (_measurement.q.inverse() * _measurement.local_to_map.unit_quaternion().inverse() * Qi * _measurement.sensor_to_robot.unit_quaternion()).vec();
    }
    void EdgeQL::linearizeOplus()
    {
        Eigen::Quaterniond Qi = (dynamic_cast<VertexQ *>(_vertices[0]))->estimate();

        _jacobianOplus[0] = ((yd_fusion_localization::Qleft(_measurement.q.inverse() * _measurement.local_to_map.unit_quaternion().inverse() * Qi)) * yd_fusion_localization::Qright(_measurement.sensor_to_robot.unit_quaternion())).bottomRightCorner<3, 3>();
    }
    void EdgeQL::setMeasurement(const yd_fusion_localization::QLocalizerData &m)
    {
        _measurement = m;
        _information = Eigen::Matrix3d::Zero();
        for (int i = 0; i < 3; i++)
        {
            _information(i, i) = 1.0 / _measurement.noise(i) / _measurement.noise(i);
            assert(!std::isnan(_information(i, i)));
            assert(!std::isinf(_information(i, i)));
        }
    }
} // namespace g2o