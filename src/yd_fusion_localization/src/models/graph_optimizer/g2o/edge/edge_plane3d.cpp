#include "yd_fusion_localization/models/graph_optimizer/g2o/edge/edge_plane3d.hpp"

namespace g2o
{
    EdgePlane3d::EdgePlane3d() : BaseMultiEdge<3, yd_fusion_localization::PlaneLandmarkData>()
    {
        resize(3);
    }
    void EdgePlane3d::computeError()
    {
        yd_fusion_localization::Plane3D plane = (dynamic_cast<VertexPlane3d *>(_vertices[0]))->estimate();
        Eigen::Vector3d Pi = (dynamic_cast<VertexVec *>(_vertices[1]))->estimate();
        Eigen::Quaterniond Qi = (dynamic_cast<VertexQ *>(_vertices[2]))->estimate();
        Sophus::SE3d t;
        t.translation() = Pi;
        t.setQuaternion(Qi);
        _error = (_measurement.sensor_to_robot.inverse() * t.inverse() * plane).ominus(yd_fusion_localization::Plane3D(_measurement.coeff));
    }
    // void EdgePlane3d::linearizeOplus()
    // {

    // }
    void EdgePlane3d::setMeasurement(const yd_fusion_localization::PlaneLandmarkData &m)
    {
        _measurement = m;
        _information = Eigen::Matrix3d::Zero();
        for (int i = 0; i < 3; i++)
        {
            _information(i, i) = 1.0 / _measurement.noise(i) / _measurement.noise(i);
            assert(!std::isnan(_information(i, i)));
            assert(!std::isinf(_information(i, i)));
        }
    }
}