#include "yd_fusion_localization/models/graph_optimizer/g2o/edge/edge_posel.hpp"

namespace g2o
{
    EdgePoseL::EdgePoseL() : BaseMultiEdge<6, yd_fusion_localization::PoseLocalizerData>()
    {
        resize(2);
    }
    void EdgePoseL::computeError()
    {
        Eigen::Vector3d Pi = (dynamic_cast<VertexVec *>(_vertices[0]))->estimate();
        Eigen::Quaterniond Qi = (dynamic_cast<VertexQ *>(_vertices[1]))->estimate();

        Sophus::SE3d t1 = _measurement.local_to_map.inverse();
        _error.head(3) = t1.translation() + t1.unit_quaternion() * Pi + t1.unit_quaternion() * Qi * _measurement.sensor_to_robot.translation() - _measurement.pose.translation();
        _error.tail(3) = 2 * (_measurement.pose.unit_quaternion().inverse() * t1.unit_quaternion() * Qi * _measurement.sensor_to_robot.unit_quaternion()).vec();
    }
    void EdgePoseL::linearizeOplus()
    {
        Eigen::Vector3d Pi = (dynamic_cast<VertexVec *>(_vertices[0]))->estimate();
        Eigen::Quaterniond Qi = (dynamic_cast<VertexQ *>(_vertices[1]))->estimate();
        Eigen::Quaterniond q1 = _measurement.local_to_map.unit_quaternion().inverse();

        Eigen::Matrix<double, 6, 6> jacs;
        jacs.setZero();
        jacs.block<3, 3>(0, 0) = q1.toRotationMatrix();
        jacs.block<3, 3>(0, 3) =  -(q1 * Qi).toRotationMatrix() * yd_fusion_localization::skewSymmetric(_measurement.sensor_to_robot.translation());
        jacs.block<3, 3>(3, 3) = ((yd_fusion_localization::Qleft(_measurement.pose.unit_quaternion().inverse() * q1 * Qi)) * yd_fusion_localization::Qright(_measurement.sensor_to_robot.unit_quaternion())).bottomRightCorner<3, 3>();

        _jacobianOplus[0] = jacs.block<6, 3>(0, 0);
        _jacobianOplus[1] = jacs.block<6, 3>(0, 3);
    }
    void EdgePoseL::setMeasurement(const yd_fusion_localization::PoseLocalizerData &m)
    {
        _measurement = m;
        _information = Eigen::Matrix<double, 6, 6>::Zero();
        for (int i = 0; i < 6; i++)
        {
            _information(i, i) = 1.0 / _measurement.noise(i) / _measurement.noise(i);
            assert(!std::isnan(_information(i, i)));
            assert(!std::isinf(_information(i, i)));
        }
    }
} // namespace g2o