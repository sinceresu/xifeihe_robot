/**
 * @file graph_optimizer_interface.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-1-4
 * @brief graph optimizer interface
 */

#include "yd_fusion_localization/models/graph_optimizer/graph_optimizer_interface.hpp"

namespace yd_fusion_localization
{
    GraphOptimizerInterface::GraphOptimizerInterface(const YAML::Node &yaml_node)
    {
        std::string atype = yaml_node["state_type"].as<std::string>();
        if (atype == "Basic")
        {
            state_type_ = StateType::Basic;
            state_num_ = 2;
        }
        else if (atype == "Vel")
        {
            state_type_ = StateType::Vel;
            state_num_ = 3;
        }
        else if (atype == "Bg")
        {
            state_type_ = StateType::Bg;
            state_num_ = 3;
        }
        else if (atype == "VBaBg")
        {
            state_type_ = StateType::VBaBg;
            state_num_ = 5;
        }
        else
        {
            LOG(ERROR) << "没有找到与 " << atype << " 对应的状态量类型，请检查配置文件";
        }
        LOG(INFO) << "融合状态量类型为：" << atype << std::endl;

        window_size_ = yaml_node["window_size"].as<int>();
        max_state_num_ = yaml_node["max_state_num"].as<int>();
        max_node_num_ = max_state_num_ * state_num_;
        max_edge_num_ = max_node_num_ * 3;
        max_iterations_num_ = yaml_node["max_iterations_num"].as<int>();
        check_chi2_ = yaml_node["check_chi2"].as<double>();
        check_predictor_ = yaml_node["check_predictor"].as<std::vector<double>>();
        check_localizer_ = yaml_node["check_localizer"].as<std::vector<double>>();
        predictor_outlier_chi2_ = yaml_node["predictor_outlier_chi2"].as<double>();
        localizer_outlier_chi2_ = yaml_node["localizer_outlier_chi2"].as<double>();

        keys_.clear();
        edge_id_ = 0;

        init_window_size_ = yaml_node["init_window_size"].as<int>();
        if (yaml_node["marge_info"].IsDefined())
        {
            marge_info_ = yaml_node["marge_info"].as<bool>();
        }
        else
        {
            marge_info_ = false;
        }
        if (yaml_node["compute_marginals"].IsDefined())
        {
            compute_marginals_ = yaml_node["compute_marginals"].as<bool>();
        }
        else
        {
            compute_marginals_ = false;
        }
    }

    bool GraphOptimizerInterface::WindowInited() const
    {
        if (keys_.size() < init_window_size_)
        {
            return false;
        }
        return true;
    }
} // namespace yd_fusion_localization