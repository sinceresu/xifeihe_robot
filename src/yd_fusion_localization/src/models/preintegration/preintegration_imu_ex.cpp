/**
 * @file preintegration_imu_ex.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-2-3
 * @brief preintegration imu ex
 */

#include "yd_fusion_localization/models/preintegration/preintegration_imu_ex.hpp"

namespace yd_fusion_localization
{
    PreIntegrationImuEx::PreIntegrationImuEx(const ImuData &imu_data, const Eigen::Vector3d &linearized_ba, const Eigen::Vector3d &linearized_bg)
        : data_(imu_data),
          s2r_(imu_data.sensor_to_robot)
    {
        sumdt_ = data_.stamps.back() - data_.stamps.front();
        assert(sumdt_ > 0);
        gravity_ = imu_data.gravity;
        noise_.setZero();
        for (int i = 0; i < 3; ++i)
        {
            noise_(i, i) = data_.noise(i) * data_.noise(i);
            noise_(i + 3, i + 3) = noise_(i, i);

            noise_(i + 6, i + 6) = data_.noise(i + 3) * data_.noise(i + 3);
            noise_(i + 9, i + 9) = noise_(i + 6, i + 6);

            noise_(i + 12, i + 12) = data_.noise(i + 6) * data_.noise(i + 6);

            noise_(i + 15, i + 15) = data_.noise(i + 9) * data_.noise(i + 9);
        }
        linearized_ba_ = linearized_ba;
        linearized_bg_ = linearized_bg;
        alpha_.setZero();
        gamma_.setIdentity();
        beta_.setZero();
        jacobian_ = Eigen::Matrix<double, 15, 15>::Identity();
        covariance_.setZero();
        Propagate();
    }
    Eigen::Matrix<double, 15, 1> PreIntegrationImuEx::Error(const Eigen::Vector3d &Pi,
                                                           const Eigen::Quaterniond &Qi,
                                                           const Eigen::Vector3d &Vi,
                                                           const Eigen::Vector3d &Bai,
                                                           const Eigen::Vector3d &Bgi,
                                                           const Eigen::Vector3d &Pj,
                                                           const Eigen::Quaterniond &Qj,
                                                           const Eigen::Vector3d &Vj,
                                                           const Eigen::Vector3d &Baj,
                                                           const Eigen::Vector3d &Bgj)
    {
        Eigen::Matrix<double, 15, 1> residuals;
        Eigen::Vector3d dba = Bai - linearized_ba_;
        Eigen::Vector3d dbg = Bgi - linearized_bg_;

        Eigen::Matrix3d dalpha_dba = jacobian_.block<3, 3>(0, 9);
        // Eigen::Matrix3d dtheta_dba = jacobian_.block<3, 3>(3, 9);
        Eigen::Matrix3d dbeta_dba = jacobian_.block<3, 3>(6, 9);
        Eigen::Matrix3d dalpha_dbg = jacobian_.block<3, 3>(0, 12);
        Eigen::Matrix3d dtheta_dbg = jacobian_.block<3, 3>(3, 12);
        Eigen::Matrix3d dbeta_dbg = jacobian_.block<3, 3>(6, 12);

        Eigen::Vector3d corrected_alpha = alpha_ + dalpha_dba * dba + dalpha_dbg * dbg;
        Eigen::Quaterniond corrected_gamma = gamma_ * deltaQ(dtheta_dbg * dbg);
        Eigen::Vector3d corrected_beta = beta_ + dbeta_dba * dba + dbeta_dbg * dbg;

        residuals.block<3, 1>(0, 0) = Qi.inverse() * (Pj - Pi - Vi * sumdt_ - 0.5 * sumdt_ * sumdt_ * gravity_) - corrected_alpha;
        residuals.block<3, 1>(3, 0) = 2 * (corrected_gamma.inverse() * (Qi.inverse() * Qj)).vec();
        residuals.block<3, 1>(6, 0) = Qi.inverse() * (Vj - Vi - gravity_ * sumdt_) - corrected_beta;
        residuals.block<3, 1>(9, 0) = Baj - Bai;
        residuals.block<3, 1>(12, 0) = Bgj - Bgi;
        return residuals;
    }
    Eigen::Matrix<double, 15, 30> PreIntegrationImuEx::Jacobian(const Eigen::Vector3d &Pi,
                                                               const Eigen::Quaterniond &Qi,
                                                               const Eigen::Vector3d &Vi,
                                                               const Eigen::Vector3d &Bai,
                                                               const Eigen::Vector3d &Bgi,
                                                               const Eigen::Vector3d &Pj,
                                                               const Eigen::Quaterniond &Qj,
                                                               const Eigen::Vector3d &Vj,
                                                               const Eigen::Vector3d &Baj,
                                                               const Eigen::Vector3d &Bgj)
    {
        Eigen::Matrix<double, 15, 30> jacobians;
        jacobians.setZero();
        // Eigen::Vector3d dba = Bai - linearized_ba_;
        Eigen::Vector3d dbg = Bgi - linearized_bg_;
        Eigen::Matrix3d dalpha_dba = jacobian_.block<3, 3>(0, 9);
        // Eigen::Matrix3d dtheta_dba = jacobian_.block<3, 3>(3, 9);
        Eigen::Matrix3d dbeta_dba = jacobian_.block<3, 3>(6, 9);
        Eigen::Matrix3d dalpha_dbg = jacobian_.block<3, 3>(0, 12);
        Eigen::Matrix3d dtheta_dbg = jacobian_.block<3, 3>(3, 12);
        Eigen::Matrix3d dbeta_dbg = jacobian_.block<3, 3>(6, 12);
        Eigen::Quaterniond corrected_gamma = gamma_ * deltaQ(dtheta_dbg * dbg);
        // Eigen::Vector3d corrected_alpha = alpha_ + dalpha_dba * dba + dalpha_dbg * dbg;
        // Eigen::Vector3d corrected_beta = beta_ + dbeta_dba * dba + dbeta_dbg * dbg;

        jacobians.block<3, 3>(0, 0) = -Qi.inverse().toRotationMatrix();
        jacobians.block<3, 3>(0, 3) = skewSymmetric(Qi.inverse() * (Pj - Pi - Vi * sumdt_ - 0.5 * sumdt_ * sumdt_ * gravity_));
        jacobians.block<3, 3>(0, 6) = -sumdt_ * Qi.inverse().toRotationMatrix();
        jacobians.block<3, 3>(0, 9) = -dalpha_dba;
        jacobians.block<3, 3>(0, 12) = -dalpha_dbg;

        jacobians.block<3, 3>(0, 15) = Qi.inverse().toRotationMatrix();

        jacobians.block<3, 3>(3, 3) = -(Qleft(Qj.inverse() * Qi) * Qright(corrected_gamma)).bottomRightCorner<3, 3>();
        jacobians.block<3, 3>(3, 12) = -(Qleft(Qj.inverse() * Qi * gamma_)).bottomRightCorner<3, 3>() * dtheta_dbg;

        jacobians.block<3, 3>(3, 18) = (Qleft(corrected_gamma.inverse() * Qi.inverse() * Qj)).bottomRightCorner<3, 3>();

        jacobians.block<3, 3>(6, 3) = skewSymmetric(Qi.inverse() * (Vj - Vi - gravity_ * sumdt_));
        jacobians.block<3, 3>(6, 6) = -Qi.inverse().toRotationMatrix();
        jacobians.block<3, 3>(6, 9) = -dbeta_dba;
        jacobians.block<3, 3>(6, 12) = -dbeta_dbg;

        jacobians.block<3, 3>(6, 21) = Qi.inverse().toRotationMatrix();

        jacobians.block<3, 3>(9, 9) = -Eigen::Matrix3d::Identity();

        jacobians.block<3, 3>(9, 24) = Eigen::Matrix3d::Identity();

        jacobians.block<3, 3>(12, 12) = -Eigen::Matrix3d::Identity();

        jacobians.block<3, 3>(12, 27) = Eigen::Matrix3d::Identity();

        return jacobians;
    }
    State PreIntegrationImuEx::DeadReckoning(const State &state,
                                             const ImuData &imu_data)
    {
        Sophus::SE3d s2r(imu_data.sensor_to_robot);
        Eigen::Vector3d gravity(imu_data.gravity);
        Eigen::Matrix<double, 18, 18> noise;
        noise.setZero();
        for (int i = 0; i < 3; ++i)
        {
            noise(i, i) = imu_data.noise(i) * imu_data.noise(i);
            noise(i + 3, i + 3) = noise(i, i);

            noise(i + 6, i + 6) = imu_data.noise(i + 3) * imu_data.noise(i + 3);
            noise(i + 9, i + 9) = noise(i + 6, i + 6);

            noise(i + 12, i + 12) = imu_data.noise(i + 6) * imu_data.noise(i + 6);

            noise(i + 15, i + 15) = imu_data.noise(i + 9) * imu_data.noise(i + 9);
        }
        Eigen::Vector3d p(state.p);
        Eigen::Quaterniond q(state.q);
        Eigen::Vector3d v(state.v);
        Eigen::Vector3d linearized_ba(state.ba);
        Eigen::Vector3d linearized_bg(state.bg);
        Eigen::Matrix<double, 15, 15> covariance(state.covariance.block(0, 0, 15, 15));
        Eigen::Matrix<double, 15, 15> jacobian;
        for (int i = 1; i < static_cast<int>(imu_data.stamps.size()); ++i)
        {
            double dt = imu_data.stamps[i] - imu_data.stamps[i - 1];
            Eigen::Vector3d acc0 = imu_data.acc[i - 1];
            Eigen::Vector3d acc1 = imu_data.acc[i];
            Eigen::Vector3d gyr0 = imu_data.gyro[i - 1];
            Eigen::Vector3d gyr1 = imu_data.gyro[i];

            Eigen::Vector3d result_p;
            Eigen::Quaterniond result_q;
            Eigen::Vector3d result_v;
            Eigen::Matrix<double, 15, 15> result_covariance;
            MidPointIntegration(dt, s2r, gravity, noise, acc0, acc1, gyr0, gyr1, p, q, v, linearized_ba, linearized_bg, jacobian, covariance, result_p, result_q, result_v, jacobian, result_covariance, true, false);

            p = result_p;
            q = result_q;
            v = result_v;
            covariance = result_covariance;
        }
        State result_state;
        result_state.stamp = imu_data.stamps.back();
        result_state.index = -1;
        result_state.type = StateType::VBaBg;
        result_state.p = p;
        result_state.q = q;
        result_state.v = v;
        result_state.ba = linearized_ba;
        result_state.bg = linearized_bg;
        result_state.covariance = covariance;
        return result_state;
    }
    void PreIntegrationImuEx::MidPointIntegration(double dt,
                                                  const Sophus::SE3d &s2r,
                                                  const Eigen::Vector3d &gravity,
                                                  const Eigen::Matrix<double, 18, 18> noise,
                                                  const Eigen::Vector3d &acc0,
                                                  const Eigen::Vector3d &acc1,
                                                  const Eigen::Vector3d &gyr0,
                                                  const Eigen::Vector3d &gyr1,
                                                  const Eigen::Vector3d &p,
                                                  const Eigen::Quaterniond &q,
                                                  const Eigen::Vector3d &v,
                                                  const Eigen::Vector3d &linearized_ba,
                                                  const Eigen::Vector3d &linearized_bg,
                                                  const Eigen::Matrix<double, 15, 15> &jacobian,
                                                  const Eigen::Matrix<double, 15, 15> &covariance,
                                                  Eigen::Vector3d &result_p,
                                                  Eigen::Quaterniond &result_q,
                                                  Eigen::Vector3d &result_v,
                                                  Eigen::Matrix<double, 15, 15> &result_jacobian,
                                                  Eigen::Matrix<double, 15, 15> &result_covariance,
                                                  bool gravity_included,
                                                  bool update_jacobian)
    {
        assert(dt > 0);
        Eigen::Quaterniond qex(s2r.unit_quaternion());
        Eigen::Vector3d pex(s2r.translation());
        Eigen::Matrix3d Rex = qex.toRotationMatrix();
        pex = -(qex.inverse() * pex);
        Eigen::Vector3d gyro_av = 0.5 * (gyr0 + gyr1) - linearized_bg;
        gyro_av = qex * gyro_av;
        Eigen::Quaterniond temp = Eigen::Quaterniond(1, gyro_av(0) * dt / 2, gyro_av(1) * dt / 2, gyro_av(2) * dt / 2);
        temp.normalize();
        result_q = q * temp;
        result_q.normalize();

        Eigen::Vector3d omega0 = gyr0 - linearized_bg;
        Eigen::Vector3d omega1 = gyr1 - linearized_bg;
        Eigen::Vector3d a0 = acc0 - linearized_ba;
        Eigen::Vector3d a1 = acc1 - linearized_ba;

        a0 = Rex * (omega0.cross(omega0.cross(pex)) + a0);
        a1 = Rex * (omega1.cross(omega1.cross(pex)) + a1);

        Eigen::Vector3d deltav = 0.5 * dt * (q * a0 + result_q * a1);
        result_v = v + deltav;
        if (gravity_included)
        {
            result_v += gravity * dt;
        }
        result_p = p + 0.5 * dt * (v + result_v);

        Eigen::Matrix3d R_w_x = skewSymmetric(gyro_av);
        Eigen::Matrix3d R_a0_x = skewSymmetric(a0);
        Eigen::Matrix3d R_a1_x = skewSymmetric(a1);
        Eigen::Matrix3d R_op0_x = Rex * (skewSymmetric(omega0) * skewSymmetric(pex) + skewSymmetric(omega0.cross(pex)));
        Eigen::Matrix3d R_op1_x = Rex * (skewSymmetric(omega1) * skewSymmetric(pex) + skewSymmetric(omega1.cross(pex)));

        Eigen::Matrix<double, 15, 15> F = Eigen::Matrix<double, 15, 15>::Zero();

        F.block<3, 3>(9, 9) = Eigen::Matrix3d::Identity();

        F.block<3, 3>(12, 12) = Eigen::Matrix3d::Identity();

        F.block<3, 3>(3, 3) = Eigen::Matrix3d::Identity() - R_w_x * dt;
        F.block<3, 3>(3, 12) = -1.0 * dt * Rex;

        F.block<3, 3>(6, 3) = -0.5 * dt * (q.toRotationMatrix() * R_a0_x + result_q.toRotationMatrix() * R_a1_x * (Eigen::Matrix3d::Identity() - R_w_x * dt));
        F.block<3, 3>(6, 6) = Eigen::Matrix3d::Identity();
        F.block<3, 3>(6, 9) = -0.5 * dt * ((q.toRotationMatrix() + result_q.toRotationMatrix()) * Rex);
        F.block<3, 3>(6, 12) = 0.5 * dt * dt * (result_q.toRotationMatrix() * R_a1_x * Rex) + 0.5 * dt * (q.toRotationMatrix() * R_op0_x + result_q.toRotationMatrix() * R_op1_x);

        F.block<3, 3>(0, 0) = Eigen::Matrix3d::Identity();
        F.block<3, 3>(0, 6) = dt * Eigen::Matrix3d::Identity();
        F.block<3, 3>(0, 3) = 0.5 * dt * F.block<3, 3>(6, 3);
        F.block<3, 3>(0, 9) = 0.5 * dt * F.block<3, 3>(6, 9);
        F.block<3, 3>(0, 12) = 0.5 * dt * F.block<3, 3>(6, 12);

        Eigen::Matrix<double, 15, 18> G = Eigen::Matrix<double, 15, 18>::Zero();

        G.block<3, 3>(9, 12) = Eigen::Matrix3d::Identity() * dt;

        G.block<3, 3>(12, 15) = Eigen::Matrix3d::Identity() * dt;

        G.block<3, 3>(3, 6) = -0.5 * dt * Rex;
        G.block<3, 3>(3, 9) = G.block<3, 3>(3, 6);
        G.block<3, 3>(3, 15) = dt * G.block<3, 3>(3, 6);

        G.block<3, 3>(6, 0) = -0.5 * dt * (q.toRotationMatrix() * Rex);
        G.block<3, 3>(6, 3) = -0.5 * dt * (result_q.toRotationMatrix() * Rex);
        G.block<3, 3>(6, 6) = 0.25 * dt * dt * (result_q.toRotationMatrix() * R_a1_x * Rex);
        G.block<3, 3>(6, 9) = G.block<3, 3>(6, 6);
        G.block<3, 3>(6, 6) = G.block<3, 3>(6, 6) + 0.5 * dt * (q.toRotationMatrix() * R_op0_x);
        G.block<3, 3>(6, 9) = G.block<3, 3>(6, 9) + 0.5 * dt * (result_q.toRotationMatrix() * R_op1_x);
        G.block<3, 3>(6, 12) = dt * G.block<3, 3>(6, 3);
        G.block<3, 3>(6, 15) = dt * G.block<3, 3>(6, 9);

        G.block<3, 3>(0, 0) = 0.5 * dt * G.block<3, 3>(6, 0);
        G.block<3, 3>(0, 3) = 0.5 * dt * G.block<3, 3>(6, 3);
        G.block<3, 3>(0, 6) = 0.5 * dt * G.block<3, 3>(6, 6);
        G.block<3, 3>(0, 9) = 0.5 * dt * G.block<3, 3>(6, 9);
        G.block<3, 3>(0, 12) = 0.5 * dt * G.block<3, 3>(6, 12);
        G.block<3, 3>(0, 15) = 0.5 * dt * G.block<3, 3>(6, 15);

        result_covariance = F * covariance * F.transpose() + G * noise * G.transpose();
        if (update_jacobian)
        {
            result_jacobian = F * jacobian;
        }
    }

    bool PreIntegrationImuEx::RePropagate(const Eigen::Vector3d &Bai, const Eigen::Vector3d &Bgi)
    {
        if ((Bai - linearized_ba_).norm() < 0.10 && (Bgi - linearized_bg_).norm() < 0.01)
        {
            return false;
        }
        linearized_ba_ = Bai;
        linearized_bg_ = Bgi;
        alpha_.setZero();
        gamma_.setIdentity();
        beta_.setZero();
        jacobian_ = Eigen::Matrix<double, 15, 15>::Identity();
        covariance_.setZero();
        Propagate();
        return true;
    }

    void PreIntegrationImuEx::Propagate()
    {
        for (int i = 1; i < static_cast<int>(data_.stamps.size()); ++i)
        {
            double dt = data_.stamps[i] - data_.stamps[i - 1];
            Eigen::Vector3d acc0 = data_.acc[i - 1];
            Eigen::Vector3d acc1 = data_.acc[i];
            Eigen::Vector3d gyr0 = data_.gyro[i - 1];
            Eigen::Vector3d gyr1 = data_.gyro[i];

            Eigen::Vector3d result_alpha;
            Eigen::Quaterniond result_gamma;
            Eigen::Vector3d result_beta;
            Eigen::Matrix<double, 15, 15> result_jacobian;
            Eigen::Matrix<double, 15, 15> result_covariance;
            MidPointIntegration(dt, s2r_, gravity_, noise_, acc0, acc1, gyr0, gyr1, alpha_, gamma_, beta_, linearized_ba_, linearized_bg_, jacobian_, covariance_, result_alpha, result_gamma, result_beta, result_jacobian, result_covariance, false, true);

            alpha_ = result_alpha;
            gamma_ = result_gamma;
            beta_ = result_beta;
            jacobian_ = result_jacobian;
            covariance_ = result_covariance;
        }
        information_ = covariance_.inverse();
        assert(!std::isnan(information_(0, 0)));
        assert(!std::isinf(information_(0, 0)));
        // Eigen::SelfAdjointEigenSolver<Eigen::Matrix<double, 15, 15>> saes(covariance_);
        // std::cout << "cov " << covariance_ << std::endl;
        // std::cout << "info " << information_ << std::endl;
        // std::cout << "eigen " << saes.eigenvalues().transpose() << std::endl;
        // double eps = 1e-8;
        // information_ = saes.eigenvectors() * Eigen::VectorXd((saes.eigenvalues().array() > eps).select(saes.eigenvalues().array().inverse(), 0)).asDiagonal() * saes.eigenvectors().transpose();
    }
} // namespace yd_fusion_localization