/**
 * @file preintegration_twist_ex.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-2-3
 * @brief preintegration twist ex
 */

#include "yd_fusion_localization/models/preintegration/preintegration_twist_ex.hpp"

namespace yd_fusion_localization
{
    PreIntegrationTwistEx::PreIntegrationTwistEx(const TwistData &twist_data)
        : data_(twist_data),
          s2r_(twist_data.sensor_to_robot)
    {
        sumdt_ = data_.stamps.back() - data_.stamps.front();
        assert(sumdt_ > 0);
        noise_.setZero();
        for (int i = 0; i < 3; ++i)
        {
            noise_(i, i) = data_.noise(i) * data_.noise(i);
            noise_(i + 3, i + 3) = noise_(i, i);

            noise_(i + 6, i + 6) = data_.noise(i + 3) * data_.noise(i + 3);
            noise_(i + 9, i + 9) = noise_(i + 6, i + 6);
        }
        alpha_.setZero();
        gamma_.setIdentity();
        covariance_.setZero();
        Propagate();
    }
    Eigen::Matrix<double, 6, 1> PreIntegrationTwistEx::Error(const Eigen::Vector3d &Pi,
                                                             const Eigen::Quaterniond &Qi,
                                                             const Eigen::Vector3d &Pj,
                                                             const Eigen::Quaterniond &Qj)
    {
        Eigen::Matrix<double, 6, 1> residuals;
        residuals.block<3, 1>(0, 0) = Qi.inverse() * (Pj - Pi) - alpha_;
        residuals.block<3, 1>(3, 0) = 2 * (gamma_.inverse() * (Qi.inverse() * Qj)).vec();
        return residuals;
    }
    Eigen::Matrix<double, 6, 12> PreIntegrationTwistEx::Jacobian(const Eigen::Vector3d &Pi,
                                                                 const Eigen::Quaterniond &Qi,
                                                                 const Eigen::Vector3d &Pj,
                                                                 const Eigen::Quaterniond &Qj)
    {
        Eigen::Matrix<double, 6, 12> jacobians;
        jacobians.setZero();

        jacobians.block<3, 3>(0, 0) = -Qi.inverse().toRotationMatrix();
        jacobians.block<3, 3>(0, 3) = skewSymmetric(Qi.inverse() * (Pj - Pi));
        jacobians.block<3, 3>(0, 6) = Qi.inverse().toRotationMatrix();

        jacobians.block<3, 3>(3, 3) = -(Qleft(Qj.inverse() * Qi) * Qright(gamma_)).bottomRightCorner<3, 3>();
        jacobians.block<3, 3>(3, 9) = (Qleft(gamma_.inverse() * Qi.inverse() * Qj)).bottomRightCorner<3, 3>();
        return jacobians;
    }
    State PreIntegrationTwistEx::DeadReckoning(const State &state,
                                               const TwistData &twist_data)
    {
        Sophus::SE3d s2r(twist_data.sensor_to_robot);
        Eigen::Matrix<double, 12, 12> noise;
        noise.setZero();
        for (int i = 0; i < 3; ++i)
        {
            noise(i, i) = twist_data.noise(i) * twist_data.noise(i);
            noise(i + 3, i + 3) = noise(i, i);

            noise(i + 6, i + 6) = twist_data.noise(i + 3) * twist_data.noise(i + 3);
            noise(i + 9, i + 9) = noise(i + 6, i + 6);
        }
        Eigen::Vector3d p(state.p);
        Eigen::Quaterniond q(state.q);
        Eigen::Matrix<double, 6, 6> covariance(state.covariance.block(0, 0, 6, 6));
        for (int i = 1; i < static_cast<int>(twist_data.stamps.size()); ++i)
        {
            double dt = twist_data.stamps[i] - twist_data.stamps[i - 1];
            Eigen::Vector3d vel0 = twist_data.vel[i - 1];
            Eigen::Vector3d vel1 = twist_data.vel[i];
            Eigen::Vector3d gyr0 = twist_data.gyro[i - 1];
            Eigen::Vector3d gyr1 = twist_data.gyro[i];

            Eigen::Vector3d result_p;
            Eigen::Quaterniond result_q;
            Eigen::Matrix<double, 6, 6> result_covariance;
            MidPointIntegration(dt, s2r, noise, vel0, vel1, gyr0, gyr1, p, q, covariance, result_p, result_q, result_covariance);

            p = result_p;
            q = result_q;
            covariance = result_covariance;
        }
        State result_state;
        result_state.stamp = twist_data.stamps.back();
        result_state.index = -1;
        result_state.type = StateType::Basic;
        result_state.p = p;
        result_state.q = q;
        result_state.covariance = covariance;
        return result_state;
    }
    void PreIntegrationTwistEx::MidPointIntegration(double dt,
                                                    const Sophus::SE3d &s2r,
                                                    const Eigen::Matrix<double, 12, 12> noise,
                                                    const Eigen::Vector3d &vel0,
                                                    const Eigen::Vector3d &vel1,
                                                    const Eigen::Vector3d &gyr0,
                                                    const Eigen::Vector3d &gyr1,
                                                    const Eigen::Vector3d &p,
                                                    const Eigen::Quaterniond &q,
                                                    const Eigen::Matrix<double, 6, 6> &covariance,
                                                    Eigen::Vector3d &result_p,
                                                    Eigen::Quaterniond &result_q,
                                                    Eigen::Matrix<double, 6, 6> &result_covariance)
    {
        assert(dt > 0);
        Eigen::Quaterniond qex(s2r.unit_quaternion());
        Eigen::Matrix3d Rex(qex.toRotationMatrix());
        Eigen::Vector3d pex(s2r.translation());
        Eigen::Vector3d gyro_av = 0.5 * (gyr0 + gyr1);
        gyro_av = Rex * gyro_av;
        Eigen::Quaterniond temp = Eigen::Quaterniond(1, gyro_av(0) * dt / 2, gyro_av(1) * dt / 2, gyro_av(2) * dt / 2);
        temp.normalize();
        result_q = q * temp;
        result_q.normalize();

        Eigen::Vector3d v0 = Rex * vel0;
        Eigen::Vector3d v1 = Rex * vel1;
        Eigen::Vector3d omega0 = Rex * gyr0;
        Eigen::Vector3d omega1 = Rex * gyr1;

        v0 = v0 - omega0.cross(pex);
        v1 = v1 - omega1.cross(pex);

        Eigen::Vector3d deltap = 0.5 * dt * (q * v0 + result_q * v1);
        result_p = p + deltap;

        Eigen::Matrix3d R_w_x, R_v0_x, R_v1_x, R_pex_x;

        R_w_x << 0, -gyro_av(2), gyro_av(1),
            gyro_av(2), 0, -gyro_av(0),
            -gyro_av(1), gyro_av(0), 0;
        R_v0_x << 0, -v0(2), v0(1),
            v0(2), 0, -v0(0),
            -v0(1), v0(0), 0;
        R_v1_x << 0, -v1(2), v1(1),
            v1(2), 0, -v1(0),
            -v1(1), v1(0), 0;
        R_pex_x << 0, -pex(2), pex(1),
            pex(2), 0, -pex(0),
            -pex(1), pex(0), 0;

        Eigen::Matrix<double, 6, 6> F = Eigen::Matrix<double, 6, 6>::Zero();

        F.block<3, 3>(3, 3) = Eigen::Matrix3d::Identity() - R_w_x * dt;

        F.block<3, 3>(0, 0) = Eigen::Matrix3d::Identity();
        F.block<3, 3>(0, 3) = -0.5 * dt * (q.toRotationMatrix() * R_v0_x + result_q.toRotationMatrix() * R_v1_x * (Eigen::Matrix3d::Identity() - R_w_x * dt));

        Eigen::Matrix<double, 6, 12> G = Eigen::Matrix<double, 6, 12>::Zero();

        G.block<3, 3>(3, 6) = -0.5 * dt * Rex;
        G.block<3, 3>(3, 9) = G.block<3, 3>(3, 6);

        G.block<3, 3>(0, 0) = -0.5 * dt * (q.toRotationMatrix() * Rex);
        G.block<3, 3>(0, 3) = -0.5 * dt * (result_q.toRotationMatrix() * Rex);
        G.block<3, 3>(0, 6) = 0.25 * dt * dt * (result_q.toRotationMatrix() * R_v1_x * Rex);
        G.block<3, 3>(0, 9) = G.block<3, 3>(0, 6);
        G.block<3, 3>(0, 6) = G.block<3, 3>(0, 6) - 0.5 * dt * (q.toRotationMatrix() * R_pex_x * Rex);
        G.block<3, 3>(0, 9) = G.block<3, 3>(0, 9) - 0.5 * dt * (result_q.toRotationMatrix() * R_pex_x * Rex);
        result_covariance = F * covariance * F.transpose() + G * noise * G.transpose();
    }

    void PreIntegrationTwistEx::Propagate()
    {
        for (int i = 1; i < static_cast<int>(data_.stamps.size()); ++i)
        {
            double dt = data_.stamps[i] - data_.stamps[i - 1];
            Eigen::Vector3d vel0 = data_.vel[i - 1];
            Eigen::Vector3d vel1 = data_.vel[i];
            Eigen::Vector3d gyr0 = data_.gyro[i - 1];
            Eigen::Vector3d gyr1 = data_.gyro[i];

            Eigen::Vector3d result_alpha;
            Eigen::Quaterniond result_gamma;
            Eigen::Matrix<double, 6, 6> result_covariance;
            MidPointIntegration(dt, s2r_, noise_, vel0, vel1, gyr0, gyr1, alpha_, gamma_, covariance_, result_alpha, result_gamma, result_covariance);

            alpha_ = result_alpha;
            gamma_ = result_gamma;
            covariance_ = result_covariance;
        }
        information_ = covariance_.inverse();
        assert(!std::isnan(information_(0, 0)));
        assert(!std::isinf(information_(0, 0)));
    }
} // namespace yd_fusion_localization