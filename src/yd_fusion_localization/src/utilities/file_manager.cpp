/**
 * @file file_manager.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-3-30
 * @brief file manager
 */

#include "yd_fusion_localization/utilities/file_manager.hpp"

namespace yd_fusion_localization
{
    bool FileManager::CreateFile(const std::string &path, std::ofstream &ofs)
    {
        ofs.close();
        boost::filesystem::remove(path.c_str());

        ofs.open(path.c_str(), std::ios::out);
        if (!ofs)
        {
            LOG(WARNING) << "无法生成文件: " << path;
            return false;
        }

        return true;
    }

    bool FileManager::InitDirectory(const std::string &path)
    {
        if (boost::filesystem::is_directory(path))
        {
            boost::filesystem::remove_all(path);
        }

        return CreateDirectory(path);
    }

    bool FileManager::CreateDirectory(const std::string &path)
    {
        if (!boost::filesystem::is_directory(path))
        {
            boost::filesystem::create_directories(path);
        }

        if (!boost::filesystem::is_directory(path))
        {
            LOG(WARNING) << "无法创建文件夹: " << path;
            return false;
        }
        return true;
    }
}