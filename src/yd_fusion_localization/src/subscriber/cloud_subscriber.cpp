/**
 * @file cloud_subscriber.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-15
 * @brief subscribe cloud data
 */

#include "yd_fusion_localization/subscriber/cloud_subscriber.hpp"

namespace yd_fusion_localization
{
    CloudSubscriber::CloudSubscriber(ros::NodeHandle &nh,
                                     const YAML::Node &yaml_node)
        : MeasureSubscriber(yaml_node)
    {
        data_buffer_.clear();
        if (yaml_node["cloud_fields"].IsDefined())
        {
            cloud_fields_ = yaml_node["cloud_fields"].as<std::string>();
        }
        else
        {
            cloud_fields_ = "XYZ";
        }
        GetTransform(yaml_node["lidar_map_transform"], lidar_map_transform_);
        // subscriber_ = nh.subscribe<sensor_msgs::PointCloud2>(topic_, buffer_size_, boost::bind(&Subscriber::msg_callback<sensor_msgs::PointCloud2::ConstPtr>, this, boost::placeholders::_1, data_buffer_));
        subscriber_ = nh.subscribe<sensor_msgs::PointCloud2>(topic_, buffer_size_, [this](const sensor_msgs::PointCloud2::ConstPtr &msg_ptr) { Subscriber::msg_callback(msg_ptr, data_buffer_); });
    }

    bool CloudSubscriber::HandleMessage(const MessagePointer &msg_ptr, const std::string &topic)
    {
        if (topic == topic_)
        {
            Subscriber::msg_callback(msg_ptr.cloud_ptr, data_buffer_);
            return true;
        }
        return false;
    }

    void CloudSubscriber::ClearData()
    {
        Subscriber::ClearData(data_buffer_);
    }

    void CloudSubscriber::ClearAllData()
    {
        Subscriber::ClearAllData(data_buffer_);
    }

    double CloudSubscriber::GetEarliestStamp()
    {
        return Subscriber::GetEarliestStamp(data_buffer_);
    }

    bool CloudSubscriber::HasData(double start_stamp, double end_stamp, double &stamp)
    {
        return MeasureSubscriber::HasData(start_stamp, end_stamp, stamp, data_buffer_);
    }

    bool CloudSubscriber::ValidData(double stamp, MeasureDataPtr &data)
    {
        return MeasureSubscriber::ValidData<sensor_msgs::PointCloud2::ConstPtr>(stamp, data, data_buffer_, boost::bind(&CloudSubscriber::ParseData, this, _1));
    }

    bool CloudSubscriber::GetLatestData(MeasureDataPtr &data)
    {
        return MeasureSubscriber::GetLatestData<sensor_msgs::PointCloud2::ConstPtr>(data, data_buffer_, boost::bind(&CloudSubscriber::ParseData, this, _1));
    }

    bool CloudSubscriber::GetAllData(std::deque<MeasureDataPtr> &data)
    {
        return MeasureSubscriber::GetAllData<sensor_msgs::PointCloud2::ConstPtr>(data, data_buffer_, boost::bind(&CloudSubscriber::ParseData, this, _1));
    }

    void CloudSubscriber::ParseData(MeasureDataPtr &data)
    {
        if (data == nullptr)
        {
            data = std::make_shared<CloudData>();
        }
        CloudDataPtr cloud_data = std::dynamic_pointer_cast<CloudData>(data);
        cloud_data->sensor_to_robot = sensor_to_robot_;
        cloud_data->noise = noise_;
        cloud_data->stamp = stamp_;
        if (PointCloud2HasField(*(data_buffer_[index_]), "intensity") && PointCloud2HasField(*(data_buffer_[index_]), "ring") && PointCloud2HasField(*(data_buffer_[index_]), "time") && cloud_fields_ == "XYZIRT")
        {
            cloud_data->point_type = CloudPointType::XYZIRT;
            cloud_data->xyz_ptr = nullptr;
            cloud_data->xyzi_ptr = nullptr;
            cloud_data->xyzt_ptr = nullptr;
            cloud_data->xyzirt_ptr = boost::make_shared<pcl::PointCloud<PointXYZIRT>>();
            pcl::fromROSMsg(*(data_buffer_[index_]), *(cloud_data->xyzirt_ptr));
        }
        else if (PointCloud2HasField(*(data_buffer_[index_]), "time") && cloud_fields_ == "XYZT")
        {
            cloud_data->point_type = CloudPointType::XYZT;
            cloud_data->xyz_ptr = nullptr;
            cloud_data->xyzi_ptr = nullptr;
            cloud_data->xyzirt_ptr = nullptr;
            cloud_data->xyzt_ptr = boost::make_shared<pcl::PointCloud<PointXYZT>>();
            pcl::fromROSMsg(*(data_buffer_[index_]), *(cloud_data->xyzt_ptr));
        }
        else
        {
            cloud_data->point_type = CloudPointType::XYZ;
            cloud_data->xyzi_ptr = nullptr;
            cloud_data->xyzt_ptr = nullptr;
            cloud_data->xyzirt_ptr = nullptr;
            cloud_data->xyz_ptr = boost::make_shared<pcl::PointCloud<pcl::PointXYZ>>();
            pcl::fromROSMsg(*(data_buffer_[index_]), *(cloud_data->xyz_ptr));
            cloud_data->xyzt_ptr = nullptr;
        }
        if (cloud_data->xyzt_ptr)
        {
            Eigen::Matrix4f lidar_inversion_matrix;
            lidar_inversion_matrix.setIdentity();
            lidar_inversion_matrix(1,1) = -1.0;
            lidar_inversion_matrix(2,2) = -1.0;
            pcl::transformPointCloud(*cloud_data->xyzt_ptr, *cloud_data->xyzt_ptr, lidar_map_transform_.matrix());
        }
    }

    bool CloudSubscriber::PointCloud2HasField(const sensor_msgs::PointCloud2 &pc2,
                                              const std::string &field_name)
    {
        for (const auto &field : pc2.fields)
        {
            if (field.name == field_name)
            {
                return true;
            }
        }
        return false;
    }
} // namespace yd_fusion_localization