/**
 * @file measure_subscriber.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-6-19
 * @brief subscribe measure data
 */

#include "yd_fusion_localization/subscriber/measure_subscriber.hpp"

namespace yd_fusion_localization
{
    MeasureSubscriber::MeasureSubscriber(const YAML::Node &yaml_node)
        : Subscriber(yaml_node),
          stamp_(-1.0),
          index_(-1)
    {
    }
} // namespace yd_fusion_localization