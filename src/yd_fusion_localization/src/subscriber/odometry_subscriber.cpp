/**
 * @file odometry_subscriber.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-15
 * @brief subscribe odometry data
 */

#include "yd_fusion_localization/subscriber/odometry_subscriber.hpp"

namespace yd_fusion_localization
{
    OdometrySubscriber::OdometrySubscriber(ros::NodeHandle &nh,
                                           const YAML::Node &yaml_node)
        : PredictSubscriber(yaml_node)
    {
        max_vel_ = yaml_node["max_vel"].as<double>();
        max_gyro_ = yaml_node["max_gyro"].as<double>();
        if (yaml_node["time_relation"].IsDefined())
        {
            time_relation_ = yaml_node["time_relation"].as<bool>();
        }
        else
        {
            time_relation_ = false;
        }
        
        data_buffer_.clear();
        // subscriber_ = nh.subscribe<nav_msgs::Odometry>(topic_, buffer_size_, boost::bind(&Subscriber::msg_callback<nav_msgs::OdometryConstPtr>, this, _1, data_buffer_));
        subscriber_ = nh.subscribe<nav_msgs::Odometry>(topic_, buffer_size_, [this](const nav_msgs::OdometryConstPtr &msg_ptr) { Subscriber::msg_callback(msg_ptr, data_buffer_); });
    }

    bool OdometrySubscriber::HandleMessage(const MessagePointer &msg_ptr, const std::string &topic)
    {
        if (topic == topic_)
        {
            Subscriber::msg_callback(msg_ptr.odometry_ptr, data_buffer_);
            return true;
        }
        return false;
    }

    void OdometrySubscriber::ClearData()
    {
        Subscriber::ClearData(data_buffer_);
    }

    void OdometrySubscriber::ClearAllData()
    {
        Subscriber::ClearAllData(data_buffer_);
    }

    double OdometrySubscriber::GetEarliestStamp()
    {
        return Subscriber::GetEarliestStamp(data_buffer_);
    }

    bool OdometrySubscriber::ValidData(double start_stamp, double end_stamp, PredictDataPtr &data)
    {
        return PredictSubscriber::ValidData<nav_msgs::OdometryConstPtr>(start_stamp, end_stamp, data, data_buffer_, boost::bind(&OdometrySubscriber::ParseData, this, _1));
    }

    double OdometrySubscriber::GetLatestData(double start_stamp, PredictDataPtr &data)
    {
        return PredictSubscriber::GetLatestData<nav_msgs::OdometryConstPtr>(start_stamp, data, data_buffer_, boost::bind(&OdometrySubscriber::ValidData, this, _1, _2, _3));
    }

    bool OdometrySubscriber::CheckData(const PredictDataPtr &data) const
    {
        assert(data->stamps.size() > 1);
        OdometryDataPtr odom_data = std::dynamic_pointer_cast<OdometryData>(data);
        Sophus::SE3d rpose = odom_data->pose.front().inverse() * odom_data->pose.back();
        double sumdt = odom_data->stamps.back() - odom_data->stamps.front();
        assert(sumdt > 0);
        if (rpose.translation().norm() > sumdt * max_vel_)
        {
            return false;
        }
        if (rpose.so3().log().norm() > sumdt * max_gyro_)
        {
            return false;
        }
        return true;
    }

    bool OdometrySubscriber::CheckDRData(const PredictDataPtr &data) const
    {
        return true;
    }

    void OdometrySubscriber::ParseData(PredictDataPtr &data)
    {
        if (data == nullptr)
        {
            data = std::make_shared<OdometryData>();
        }
        assert(data_buffer_.size() > 1);
        OdometryDataPtr odom_data = std::dynamic_pointer_cast<OdometryData>(data);
        odom_data->Clear();
        odom_data->sensor_to_robot = sensor_to_robot_;
        
        if (time_relation_)
        {
            odom_data->noise = noise_ * (end_stamp_ - start_stamp_);
        }
        else
        {
            odom_data->noise = noise_;
        }
        
        odom_data->stamps.push_back(start_stamp_);
        Sophus::SE3d start_pose;
        Sophus::SE3d end_pose;
        TransformDataType(data_buffer_[start_index_]->pose.pose, start_pose);
        TransformDataType(data_buffer_[start_index_ + 1]->pose.pose, end_pose);
        odom_data->pose.push_back(Interpolate(start_stamp_, data_buffer_[start_index_]->header.stamp.toSec(),
                                              data_buffer_[start_index_ + 1]->header.stamp.toSec(),
                                              start_pose, end_pose));

        double prev_stamp = start_stamp_;
        for (int i = start_index_ + 1; i < end_index_; ++i)
        {
            double stamp = data_buffer_[i]->header.stamp.toSec();
            if (stamp - prev_stamp > 1e-3)
            {
                Sophus::SE3d temp_pose;
                odom_data->stamps.push_back(stamp);
                TransformDataType(data_buffer_[i]->pose.pose, temp_pose);
                odom_data->pose.push_back(temp_pose);
                prev_stamp = stamp;
            }
        }
        if (end_stamp_ - prev_stamp < 1e-3 && odom_data->stamps.size() > 1)
        {
            odom_data->stamps.pop_back();
            odom_data->pose.pop_back();
        }
        odom_data->stamps.push_back(end_stamp_);
        TransformDataType(data_buffer_[end_index_ - 1]->pose.pose, start_pose);
        TransformDataType(data_buffer_[end_index_]->pose.pose, end_pose);
        odom_data->pose.push_back(Interpolate(end_stamp_, data_buffer_[end_index_ - 1]->header.stamp.toSec(),
                                              data_buffer_[end_index_]->header.stamp.toSec(),
                                              start_pose, end_pose));
    }
} // namespace yd_fusion_localization