/**
 * @file subscriber.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-6-19
 * @brief subscribe data
 */

#include "yd_fusion_localization/subscriber/subscriber.hpp"

namespace yd_fusion_localization
{
    Subscriber::Subscriber(const YAML::Node &yaml_node)
    {
        GetTransform(yaml_node["sensor_to_robot"], sensor_to_robot_);
        if (yaml_node["sensor_frame"].IsDefined())
        {
            sensor_frame_ = yaml_node["sensor_frame"].as<std::string>();
        }
        else
        {
            sensor_frame_ = "";
        }
        topic_ = yaml_node["topic"].as<std::string>();
        buffer_size_ = yaml_node["buffer_size"].as<int>();
        buffer_time_ = yaml_node["buffer_time"].as<double>();
        if (yaml_node["noise"].IsDefined())
        {
            std::vector<double> noise = yaml_node["noise"].as<std::vector<double>>();
            noise_.resize(noise.size());
            for (int j = 0; j < noise.size(); ++j)
            {
                noise_[j] = noise[j];
            }
        }
        else
        {
            noise_.resize(0);
        }
    }
} // namespace yd_fusion_localization