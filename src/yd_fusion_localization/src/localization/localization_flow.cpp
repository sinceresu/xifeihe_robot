/**
 * @file localization_flow.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-1-14
 * @brief localization flow
 */

#include "yd_fusion_localization/localization/localization_flow.hpp"
#include "yd_fusion_localization/global_defination/global_defination.h"

namespace yd_fusion_localization
{
    LocalizationFlow::LocalizationFlow(ros::NodeHandle &nh)
    {
        Configure(nh);
        RunBackEndThread();
    }

    void LocalizationFlow::HandleMessage(const MessagePointer &msg_ptr, const std::string &topic)
    {
        frontend_->HandleMessage(msg_ptr, topic);
        backend_->HandleMessage(msg_ptr, topic);
    }

    void LocalizationFlow::Run(bool single_thread)
    {
        double ros_stamp = ros::Time::now().toSec();
        static double stamp = ros_stamp;
        if (ros_stamp + 5e-2 < stamp)
        {
            stamp = ros_stamp;
            frontend_->PublishStatus(DiagnosticType::JumpBackInTime);
            LOG(WARNING) << "JumpBackInTime";
            backend_->SetUninited();
            return;
        }
        else
        {
            stamp = std::max(ros_stamp, stamp);
        }
        ClearData();
        if (frontend_->IfStop())
        {
            backend_->SetUninited();
            return;
        }
        if (frontend_->IfReceivedInitialpose())
        {
            backend_->SetUninited();
            backend_->Reset();
            backend_->Init(true);
            return;
        }
        if (frontend_->IfRelocalization())
        {
            backend_->SetUninited();
            return;
        }

        if (!frontend_->Status())
        {
            frontend_->PublishStatus(DiagnosticType::FrontEndStatusFalse);
            LOG(INFO) << "FrontEndStatusFalse";
            if (!backend_->IfInited())
            {
                frontend_->PublishStatus(DiagnosticType::BackEndIfInitedFalse);
                LOG(INFO) << "BackEndIfInitedFalse";
                backend_->Reset();
                backend_->Init(false);
                return;
            }
            frontend_->PublishStatus(DiagnosticType::BackEndIfInitedTrue);
            LOG(INFO) << "BackEndIfInitedTrue";
            backend_->InitWindow();
            return;
        }
        if (!frontend_->Update())
        {
            frontend_->PublishStatus(DiagnosticType::FrontEndUpdateFalse);
            LOG(INFO) << "FrontEndUpdateFalse";
            backend_->SetUninited();
            return;
        }
        backend_->Notify(frontend_->GetUpdateState());
        if (single_thread)
        {
            backend_->Wait();
        }
    }

    void LocalizationFlow::Finish()
    {
        frontend_->PublishStatus(DiagnosticType::Finish);
        LOG(INFO) << "Finish";
        backend_->Finish();
        frontend_->PublishStatus(DiagnosticType::Finished);
        LOG(INFO) << "Finished";
    }

    void LocalizationFlow::ClearData()
    {
        frontend_->ClearData();
        backend_->ClearData();
    }

    void LocalizationFlow::Configure(ros::NodeHandle &nh)
    {
        std::string yaml_path = WORK_SPACE_PATH + "/config/localization/localization.yaml";
        YAML::Node yaml_node = YAML::LoadFile(yaml_path);

        std::string map_frame = yaml_node["map_frame"].as<std::string>();
        std::string robot_frame = yaml_node["robot_frame"].as<std::string>();

        frontend_ = std::make_shared<FrontEnd>(nh, yaml_node["frontend"], map_frame, robot_frame);
        backend_ = std::make_shared<BackEnd>(nh, yaml_node["backend"], map_frame, robot_frame, frontend_);
    }

    void LocalizationFlow::RunBackEndThread()
    {
        std::thread *t = new std::thread(&BackEnd::Run, backend_.get());
    }
} // namespace yd_fusion_localization