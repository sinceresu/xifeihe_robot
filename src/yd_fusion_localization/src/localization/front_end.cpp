#include "yd_fusion_localization/localization/front_end.hpp"
#include "yd_fusion_localization/models/preintegration/preintegration_imu_ex.hpp"
#include "yd_fusion_localization/models/preintegration/preintegration_odom_ex.hpp"
#include "yd_fusion_localization/models/preintegration/preintegration_twist_ex.hpp"
#include "yd_fusion_localization/models/preintegration/preintegration_vg_ex.hpp"

namespace yd_fusion_localization
{
    FrontEnd::FrontEnd(ros::NodeHandle &nh, const YAML::Node &yaml_node, const std::string &map_frame, const std::string &robot_frame)
    {
        update_interval_time_ = yaml_node["update_interval_time"].as<double>();
        update_delay_time_ = yaml_node["update_delay_time"].as<double>();
        max_vel_ = yaml_node["max_vel"].as<double>();
        max_gyro_ = yaml_node["max_gyro"].as<double>();
        max_covariance_ = yaml_node["max_std_deviation"].as<std::vector<double>>();
        if (yaml_node["inlier_check"].IsDefined())
        {
            inlier_check_ = yaml_node["inlier_check"].as<bool>();
        }
        else
        {
            inlier_check_ = false;
        }
        for (auto &it : max_covariance_)
        {
            it = it * it;
        }

        initialpose_subscriber_ = std::make_shared<InitialposeSubscriber>(nh, yaml_node["initialpose"]);
        historypose_ = std::make_shared<Historypose>(yaml_node["historypose"]);
        ConfigurePredictors(nh, yaml_node["predictors"], robot_frame);

        ros::param::set("/stop_localization", false);
        ros::param::set("/relocalization", false);

        ConfigureDiagnosticPublisher(nh, yaml_node["diagnostic_publisher"]);
        ConfigurePublisher(nh, yaml_node["pose_publisher"], map_frame, pose_publisher_);
        // if (ConfigurePublisher(nh, yaml_node["lidar_pose_publisher"], map_frame, lidar_pose_publisher_))
        // {
        //     LOG(ERROR) << "lidar pose publisher not init " << std::endl;
        // }
        tf_broadcaster_ = std::make_shared<TFBroadCaster>(map_frame, robot_frame);
        
        Reset();
    }
    bool FrontEnd::HandleMessage(const MessagePointer &msg_ptr, const std::string &topic)
    {
        if (initialpose_subscriber_->HandleMessage(msg_ptr, topic))
        {
            return true;
        }
        for (int i = 0; i < predictors_.size(); ++i)
        {
            if (predictors_[i]->HandleMessage(msg_ptr, topic))
            {
                return true;
            }
        }
        return false;
    }
    void FrontEnd::ClearData()
    {
        initialpose_subscriber_->ClearData();
        for (int i = 0; i < predictors_.size(); ++i)
        {
            predictors_[i]->ClearData();
        }
    }
    void FrontEnd::Reset()
    {
        mutex_state_.lock();
        state_.stamp = -1.0;
        mutex_state_.unlock();

        update_state_.stamp = -1.0;

        mutex_inliers_.lock();
        if (inlier_check_)
        {
            inliers_.clear();
        }
        else
        {
            inliers_.resize(predictors_.size());
            for (int i = 0; i < inliers_.size(); ++i)
            {
                inliers_[i] = i;
            }
        }
        mutex_inliers_.unlock();
    }

    bool FrontEnd::IfStop()
    {
        bool stop_localization;
        ros::param::get("/stop_localization", stop_localization);
        if (stop_localization)
        {
            PublishStatus(DiagnosticType::Stop);
            LOG(INFO) << "Stop";
            return true;
        }
        return false;
    }

    bool FrontEnd::IfRelocalization()
    {
        bool relocalization;
        ros::param::get("/relocalization", relocalization);
        if (relocalization)
        {
            PublishStatus(DiagnosticType::Relocalization);
            LOG(INFO) << "Relocalization";
            ros::param::set("/relocalization", false);
            return true;
        }

        return false;
    }

    bool FrontEnd::IfReceivedInitialpose()
    {
        if (initialpose_subscriber_->ValidData(initpose_))
        {
            PublishStatus(DiagnosticType::ReceivedInitialpose);
            LOG(INFO) << "ReceivedInitialpose!";
            LOG(INFO) << "initialpose is " << initpose_.translation()[0]
                      << " " << initpose_.translation()[1] << " "
                      << " " << initpose_.translation()[2] << " "
                      << " " << initpose_.unit_quaternion().w() << " "
                      << " " << initpose_.unit_quaternion().x() << " "
                      << " " << initpose_.unit_quaternion().y() << " "
                      << " " << initpose_.unit_quaternion().z();
            return true;
        }
        return false;
    }
    void FrontEnd::GetInitialpose(std::deque<Sophus::SE3d> &poses)
    {
        poses.clear();
        poses.push_back(initpose_);
    }
    void FrontEnd::GetHistorypose(std::deque<Sophus::SE3d> &poses)
    {
        historypose_->LoopDetection(poses);
    }
    void FrontEnd::PublishStatus(DiagnosticType type)
    {
        mutex_diag_.lock();
        diagnostic_publisher_->Publish(type);
        mutex_diag_.unlock();
    }

    void FrontEnd::SetStatus(bool status)
    {
        std::lock_guard<std::mutex> lock(mutex_status_);
        localization_status_ = status;
        ros::param::set("/localization_status", localization_status_);
    }
    bool FrontEnd::Status()
    {
        std::lock_guard<std::mutex> lock(mutex_status_);
        return localization_status_;
    }
    void FrontEnd::SetState(const State &state)
    {
        std::lock_guard<std::mutex> lock(mutex_state_);
        state_ = state;
    }
    State FrontEnd::GetState()
    {
        std::lock_guard<std::mutex> lock(mutex_state_);
        return state_;
    }

    void FrontEnd::SetUpdateState(const State &state)
    {
        update_state_ = state;
        historypose_->Save(state);
        Publish(state);
        Publish(state.stamp);
    }

    State FrontEnd::GetUpdateState() const
    {
        return update_state_;
    }

    void FrontEnd::SetInliers(const std::vector<int> &inliers)
    {
        mutex_inliers_.lock();
        if (inlier_check_)
        {
            inliers_ = inliers;
        }
        mutex_inliers_.unlock();
    }

    std::vector<int> FrontEnd::GetInliers()
    {
        std::lock_guard<std::mutex> lock(mutex_inliers_);
        return inliers_;
    }

    bool FrontEnd::HasData(double start_stamp, double end_stamp, std::deque<std::pair<bool, PredictDataPtr>> &datas)
    {
        bool res = true;
        std::vector<int> indices;
        for (int i = 0; i < predictors_.size(); ++i)
        {
            if (!datas[i].second->stamps.empty())
            {
                if (std::fabs(datas[i].second->stamps.front() - start_stamp) < 1e-5 && std::fabs(datas[i].second->stamps.back() - end_stamp) < 1e-5)
                {
                    datas[i].first = true;
                    indices.push_back(i);
                    continue;
                }
            }
            if (predictors_[i]->HasData(start_stamp, end_stamp, datas[i].second))
            {
                if (predictors_[i]->CheckData(datas[i].second))
                {
                    datas[i].first = true;
                    indices.push_back(i);
                }
                else
                {
                    datas[i].first = false;
                }
            }
            else
            {
                datas[i].first = false;
                res = false;
            }
        }
        if (res && indices.size() == 1)
        {
            if (!predictors_[indices.front()]->CheckDRData(datas[indices.front()].second))
            {
                return false;
            }
        }
        return res;
    }

    std::deque<std::pair<bool, PredictDataPtr>> FrontEnd::NewData() const
    {
        std::deque<std::pair<bool, PredictDataPtr>> pdata;
        for (int i = 0; i < predictors_.size(); ++i)
        {
            pdata.push_back(std::make_pair(false, predictors_[i]->NewData()));
        }
        return pdata;
    }
    bool FrontEnd::GetVelocity(double stamp, Eigen::Vector3d &linear_velocity, Eigen::Vector3d &angular_velocity)
    {
        linear_velocity = Eigen::Vector3d::Zero();
        angular_velocity = Eigen::Vector3d::Zero();
        return false;
    }
    bool FrontEnd::GetVelocity(const State &state, Eigen::Vector3d &linear_velocity, Eigen::Vector3d &angular_velocity)
    {
        linear_velocity = Eigen::Vector3d::Zero();
        angular_velocity = Eigen::Vector3d::Zero();
        if (!Status())
        {
            return false;
        }
        std::vector<int> inliers = GetInliers();
        bool res = false;
        PredictDataPtr data;
        auto fi = std::find_if(inliers.begin(), inliers.end(), [this](const int &it) { return predictors_[it]->GetType() == PredictType::Imu; });
        if (fi != inliers.end())
        {
            data = predictors_[*fi]->NewData();
            if (predictors_[*fi]->HasData(state.stamp - 0.1, state.stamp, data))
            {
                ImuDataPtr imudata = std::dynamic_pointer_cast<ImuData>(data);
                double sumdt = imudata->stamps.back() - imudata->stamps.front();
                for (int i = 1; i < imudata->stamps.size(); ++i)
                {
                    double half_dt = 0.5 * (imudata->stamps[i] - imudata->stamps[i - 1]);
                    angular_velocity += half_dt * (imudata->gyro[i - 1] + imudata->gyro[i]);
                }
                angular_velocity = angular_velocity / sumdt;
                angular_velocity = angular_velocity - state.bg;
                angular_velocity = imudata->sensor_to_robot.unit_quaternion() * angular_velocity;
                res = true;
            }
        }

        fi = std::find_if(inliers.begin(), inliers.end(), [this](const int &it) { return predictors_[it]->GetType() == PredictType::Vg; });
        if (fi != inliers.end())
        {
            PredictDataPtr data = predictors_[*fi]->NewData();
            if (predictors_[*fi]->HasData(state.stamp - 0.1, state.stamp, data))
            {
                VgDataPtr vgdata = std::dynamic_pointer_cast<VgData>(data);
                for (int i = 1; i < vgdata->stamps.size(); ++i)
                {
                    double half_dt = 0.5 * (vgdata->stamps[i] - vgdata->stamps[i - 1]);
                    linear_velocity += half_dt * (vgdata->vel[i - 1] + vgdata->vel[i]);
                    if (!res)
                    {
                        angular_velocity += half_dt * (vgdata->gyro[i - 1] + vgdata->gyro[i]);
                    }
                }
                double sumdt = vgdata->stamps.back() - vgdata->stamps.front();
                if (!res)
                {
                    angular_velocity = angular_velocity / sumdt;
                    angular_velocity = angular_velocity - state.bg;
                    angular_velocity = vgdata->sensor_to_robot_gyro.unit_quaternion() * angular_velocity;
                }
                linear_velocity = linear_velocity / sumdt;
                linear_velocity = vgdata->sensor_to_robot.unit_quaternion() * linear_velocity;
                linear_velocity = linear_velocity - angular_velocity.cross(vgdata->sensor_to_robot.translation());
                return true;
            }
        }
        fi = std::find_if(inliers.begin(), inliers.end(), [this](const int &it) { return predictors_[it]->GetType() == PredictType::Twist; });
        if (fi != inliers.end())
        {
            PredictDataPtr data = predictors_[*fi]->NewData();
            if (predictors_[*fi]->HasData(state.stamp - 0.1, state.stamp, data))
            {
                TwistDataPtr twistdata = std::dynamic_pointer_cast<TwistData>(data);
                for (int i = 1; i < twistdata->stamps.size(); ++i)
                {
                    double half_dt = 0.5 * (twistdata->stamps[i] - twistdata->stamps[i - 1]);
                    linear_velocity += half_dt * (twistdata->vel[i - 1] + twistdata->vel[i]);
                    if (!res)
                    {
                        angular_velocity += half_dt * (twistdata->gyro[i - 1] + twistdata->gyro[i]);
                    }
                }
                double sumdt = twistdata->stamps.back() - twistdata->stamps.front();
                if (!res)
                {
                    angular_velocity = angular_velocity / sumdt;
                    angular_velocity = twistdata->sensor_to_robot.unit_quaternion() * angular_velocity;
                }
                linear_velocity = linear_velocity / sumdt;
                linear_velocity = twistdata->sensor_to_robot.unit_quaternion() * linear_velocity;
                linear_velocity = linear_velocity - angular_velocity.cross(twistdata->sensor_to_robot.translation());
                return true;
            }
        }
        fi = std::find_if(inliers.begin(), inliers.end(), [this](const int &it) { return predictors_[it]->GetType() == PredictType::Odometry; });
        if (fi != inliers.end())
        {
            PredictDataPtr data = predictors_[*fi]->NewData();
            if (predictors_[*fi]->HasData(state.stamp - 0.1, state.stamp, data))
            {
                OdometryDataPtr odomdata = std::dynamic_pointer_cast<OdometryData>(data);
                Sophus::SE3d rpose = odomdata->pose.front().inverse() * odomdata->pose.back();
                double sumdt = odomdata->stamps.back() - odomdata->stamps.front();
                if (!res)
                {
                    angular_velocity = rpose.so3().log() / sumdt;
                    angular_velocity = odomdata->sensor_to_robot.unit_quaternion() * angular_velocity;
                }
                linear_velocity = rpose.translation() / sumdt;
                linear_velocity = odomdata->sensor_to_robot.unit_quaternion() * linear_velocity;
                linear_velocity = linear_velocity - angular_velocity.cross(odomdata->sensor_to_robot.translation());
                return true;
            }
        }
        return false;
    }
    bool FrontEnd::Update()
    {
        double ros_stamp = ros::Time::now().toSec();
        State ustate(GetUpdateState());
        if (ros_stamp + 5e-2 < ustate.stamp)
        {
            PublishStatus(DiagnosticType::JumpBackInTime);
            LOG(WARNING) << "JumpBackInTimeF";
            return false;
        }
        if (ros_stamp - (ustate.stamp + update_interval_time_) > update_delay_time_)
        {
            PublishStatus(DiagnosticType::UpdateDelayed);
            LOG(WARNING) << "UpdateDelayed " << ros_stamp - ustate.stamp << " second!!!";
            return false;
        }
        if (ros_stamp - ustate.stamp > update_interval_time_)
        {
            State state(GetState());
            State result_state;
            int res = DeadReckoning(state, ustate.stamp, result_state);
            if(res == 0)
            {
                PublishStatus(DiagnosticType::UpdateOk);
                SetUpdateState(result_state);
                LOG(INFO) << "DeadReckoning Succeeded";
                return true;
            }
            else if (res == -1)
            {
                PublishStatus(DiagnosticType::DeadReckoningFailed);
                LOG(WARNING) << "DeadReckoningFailed";
                return false;
            }
            else
            {
                PublishStatus(DiagnosticType::WaitingForData);
                LOG(WARNING) << "WaitingForData! Delayed time is " << ros_stamp - ustate.stamp << " second!";
                return true;
            }
        }
        return true;
    }

    void FrontEnd::Publish(const State &state)
    {
        Sophus::SE3d pose(state.q, state.p);
        tf_broadcaster_->SendTransform(state.stamp, pose);
        
        if(pose_publisher_.first)
        {
            if (pose_publisher_.first->HasSubscribers())
            {
                if (pose_publisher_.second->GetType() == PublishType::PoseP)
                {
                    PosePublishDataPtr pose_pub = std::dynamic_pointer_cast<PosePublishData>(pose_publisher_.second);
                    pose_pub->stamp = state.stamp;
                    pose_pub->pose = pose;
                    pose_pub->covariance = state.covariance.block<6, 6>(0, 0);
                    pose_publisher_.first->Publish(pose_publisher_.second);
                }
                else if (pose_publisher_.second->GetType() == PublishType::OdometryP)
                {
                    OdometryPublishDataPtr pose_pub = std::dynamic_pointer_cast<OdometryPublishData>(pose_publisher_.second);
                    pose_pub->stamp = state.stamp;
                    pose_pub->pose = pose;
                    pose_pub->posecov = state.covariance.block<6, 6>(0, 0);
                    
                    Eigen::Vector3d lv, av;
                    GetVelocity(state, lv, av);
                    pose_pub->twist.block<3, 1>(0, 0) = lv;
                    pose_pub->twist.block<3, 1>(3, 0) = av;
                    
   
                    pose_publisher_.first->Publish(pose_publisher_.second);
                }
                else
                {
                    return;
                }
            }
        }else{
            LOG(ERROR) << "please check yaml, there is no define of pose_publisher";
        }

    }

    void FrontEnd::Publish(double stamp)
    {
        for (auto &it : predictors_)
        {
            it->Publish(stamp);
        }
    }

    int FrontEnd::DeadReckoning(const State &state, double ustamp, State &result_state)
    {
        std::vector<int> inliers = GetInliers();
        if (state.type == StateType::VBaBg)
        {
            int index_twist = -1;
            int index_imu = -1;
            for (int i = 0; i < inliers.size(); ++i)
            {
                if (pdata_[inliers[i]]->GetType() == PredictType::Twist)
                {
                    index_twist = i;
                }
                else if (pdata_[inliers[i]]->GetType() == PredictType::Imu)
                {
                    index_imu = i;
                }
                if (index_twist != -1 && index_imu != -1)
                {
                    break;
                }
            }
            if (index_twist != -1 && index_imu != -1)
            {
                double stamp_twist = predictors_[inliers[index_twist]]->GetLatestData(state.stamp, pdata_[inliers[index_twist]]);
                double stamp_imu = predictors_[inliers[index_imu]]->GetLatestData(state.stamp, pdata_[inliers[index_imu]]);
                if (stamp_twist > ustamp + 1e-4 && stamp_imu > ustamp + 1e-4)
                {
                    double stamp = -1.0;
                    if (stamp_twist < stamp_imu)
                    {
                        if (predictors_[inliers[index_imu]]->HasData(state.stamp, stamp_twist, pdata_[inliers[index_imu]]))
                        {
                            stamp = stamp_twist;
                        }
                    }
                    else
                    {
                        if (predictors_[inliers[index_twist]]->HasData(state.stamp, stamp_imu, pdata_[inliers[index_twist]]))
                        {
                            stamp = stamp_imu;
                        }
                    }
                    if (stamp > ustamp + 1e-4)
                    {
                        if (predictors_[inliers[index_twist]]->CheckData(pdata_[inliers[index_twist]]))
                        {
                            if (predictors_[inliers[index_imu]]->CheckData(pdata_[inliers[index_imu]]))
                            {
                                TwistDataPtr twist_data = std::dynamic_pointer_cast<TwistData>(pdata_[inliers[index_twist]]);
                                ImuDataPtr imu_data = std::dynamic_pointer_cast<ImuData>(pdata_[inliers[index_imu]]);
                                VgDataPtr vg_data = std::make_shared<VgData>();
                                VgSubscriber::ParseData(twist_data, imu_data, vg_data);
                                if (DeadReckoning(state, std::dynamic_pointer_cast<PredictData>(vg_data), result_state))
                                {
                                    double dt = result_state.stamp - state.stamp;
                                    if ((state.p - result_state.p).norm() < max_vel_ * dt && std::acos(std::fabs((state.q.inverse() * result_state.q).w())) * 2 < max_gyro_ * dt)
                                    {
                                        return 0;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        int index = -1;
        for (int i = 0; i < inliers.size(); ++i)
        {
            if (state.type == StateType::VBaBg)
            {
                assert((int)(state.covariance.cols()) == 15);
                /// nothing
            }
            else if (state.type == StateType::Bg)
            {
                assert((int)(state.covariance.cols()) == 9);
                if (pdata_[inliers[i]]->GetType() == PredictType::Imu)
                {
                    continue;
                }
            }
            else if (state.type == StateType::Vel)
            {
                assert((int)(state.covariance.cols()) == 9);
                if (pdata_[inliers[i]]->GetType() == PredictType::Imu || pdata_[inliers[i]]->GetType() == PredictType::Vg)
                {
                    continue;
                }
            }
            else if (state.type == StateType::Basic)
            {
                assert((int)(state.covariance.cols()) == 6);
                if (pdata_[inliers[i]]->GetType() == PredictType::Imu || pdata_[inliers[i]]->GetType() == PredictType::Vg)
                {
                    continue;
                }
            }
            else
            {
                return -1;
            }

            double stamp = predictors_[inliers[i]]->GetLatestData(state.stamp, pdata_[inliers[i]]);
            if (stamp > ustamp + 1e-4)
            {
                if (predictors_[inliers[i]]->CheckData(pdata_[inliers[i]]) && predictors_[inliers[i]]->CheckDRData(pdata_[inliers[i]]))
                {
                    if (DeadReckoning(state, pdata_[inliers[i]], result_state))
                    {
                        double dt = result_state.stamp - state.stamp;
                        if ((state.p - result_state.p).norm() > max_vel_ * dt || std::acos(std::fabs((state.q.inverse() * result_state.q).w())) * 2 > max_gyro_ * dt)
                        {
                            continue;
                        }
                        return 0;
                    }
                    else
                    {
                        if (i + 1 == inliers.size())
                        {
                            return -1;
                        }
                    }
                }
            }
        }
        return 1;
    }

    bool FrontEnd::DeadReckoning(const State &state, const PredictDataPtr &data, State &result_state)
    {
        if (data->GetType() == PredictType::Vg)
        {
            result_state = PreIntegrationVgEx::DeadReckoning(state, *(std::dynamic_pointer_cast<VgData>(data)));
            LOG(INFO) << "DeadReckoning Vg";
        }
        else if (data->GetType() == PredictType::Twist)
        {
            result_state = PreIntegrationTwistEx::DeadReckoning(state, *(std::dynamic_pointer_cast<TwistData>(data)));
            LOG(INFO) << "DeadReckoning Twist";
        }
        else if (data->GetType() == PredictType::Imu)
        {
            result_state = PreIntegrationImuEx::DeadReckoning(state, *(std::dynamic_pointer_cast<ImuData>(data)));
            LOG(INFO) << "DeadReckoning Imu";
        }
        else if (data->GetType() == PredictType::Odometry)
        {
            result_state = PreIntegrationOdomEx::DeadReckoning(state, *(std::dynamic_pointer_cast<OdometryData>(data)));
            LOG(INFO) << "DeadReckoning Odometry";
        }
        else
        {
            return false;
        }
        // std::cout << "FrontEnd::DeadReckoning covariance " << result_state.covariance << std::endl;
        return CheckCovariance(result_state.covariance);
    }

    bool FrontEnd::CheckCovariance(const Eigen::MatrixXd &covariance)
    {
        for (int i = 0; i < 6; ++i)
        {
            if (covariance(i, i) > max_covariance_[i])
            {
                return false;
            }
        }

        return true;
    }
    void FrontEnd::ConfigurePredictors(ros::NodeHandle &nh, const YAML::Node &yaml_node, const std::string &robot_frame)
    {
        predictors_.clear();
        int num = yaml_node["number"].as<int>();
        for (int i = 0; i < num; ++i)
        {
            std::string index("no_");
            index = index + std::to_string(i + 1);
            std::shared_ptr<Predictor> predictor = std::make_shared<Predictor>(nh, yaml_node[index], robot_frame);
            predictors_.push_back(predictor);
            pdata_.push_back(predictor->NewData());
        }
    }
    void FrontEnd::ConfigureDiagnosticPublisher(ros::NodeHandle &nh, const YAML::Node &yaml_node)
    {
        std::string topic = yaml_node["topic"].as<std::string>();
        std::string hardware_id = yaml_node["hardware_id"].as<std::string>();
        std::string node_id = yaml_node["node_id"].as<std::string>();
        int buffer_size = yaml_node["buffer_size"].as<int>();
        diagnostic_publisher_ = std::make_shared<DiagnosticPublisher>(nh, topic, hardware_id, node_id, buffer_size, false);
    }
} // namespace yd_fusion_localization
