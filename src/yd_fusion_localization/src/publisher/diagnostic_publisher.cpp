/**
 * @file diagnostic_publisher.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-10-13
 * @brief diagnostic publisher
 */

#include "yd_fusion_localization/publisher/diagnostic_publisher.hpp"

namespace yd_fusion_localization
{
    DiagnosticPublisher::DiagnosticPublisher(ros::NodeHandle &nh,
                                             const std::string &topic_name,
                                             const std::string &hardware_id,
                                             const std::string &node_id,
                                             int buff_size,
                                             bool latch)
    {
        node_name_ = ros::this_node::getName();
        hardware_id_ = hardware_id;
        node_id_ = node_id;
        publisher_ = nh.advertise<diagnostic_msgs::DiagnosticArray>(topic_name, buff_size, latch);
    }

    void DiagnosticPublisher::Publish(const DiagnosticType &data)
    {
        diagnostic_msgs::DiagnosticArray log;
        log.header.stamp = ros::Time::now();
        diagnostic_msgs::DiagnosticStatus s;
        s.name = node_name_;
        s.hardware_id = hardware_id_;
        diagnostic_msgs::KeyValue kv;
        kv.key = "error_code";
        if (data == DiagnosticType::UpdateOk)
        {
            s.level = 0;
            s.message = "ok";
            kv.value = hardware_id_ + node_id_ + "00";
        }
        else if (data == DiagnosticType::Stop)
        {
            s.level = 1;
            s.message = "Stop";
            kv.value = hardware_id_ + node_id_ + "01";
        }
        else if (data == DiagnosticType::ReceivedInitialpose)
        {
            s.level = 1;
            s.message = "ReceivedInitialpose";
            kv.value = hardware_id_ + node_id_ + "02";
        }
        else if (data == DiagnosticType::Relocalization)
        {
            s.level = 1;
            s.message = "Relocalization";
            kv.value = hardware_id_ + node_id_ + "03";
        }
        else if (data == DiagnosticType::JumpBackInTime)
        {
            s.level = 2;
            s.message = "JumpBackInTime";
            kv.value = hardware_id_ + node_id_ + "04";
        }
        else if (data == DiagnosticType::UpdateDelayed)
        {
            s.level = 1;
            s.message = "UpdateDelayed";
            kv.value = hardware_id_ + node_id_ + "05";
        }
        else if (data == DiagnosticType::WaitingForData)
        {
            s.level = 2;
            s.message = "WaitingForData";
            kv.value = hardware_id_ + node_id_ + "06";
        }
        else if (data == DiagnosticType::DeadReckoningFailed)
        {
            s.level = 2;
            s.message = "DeadReckoningFailed";
            kv.value = hardware_id_ + node_id_ + "07";
        }
        else if (data == DiagnosticType::InitWithLoopDetection)
        {
            s.level = 0;
            s.message = "InitWithLoopDetection";
            kv.value = hardware_id_ + node_id_ + "08";
        }
        else if (data == DiagnosticType::InitWithHistorypose)
        {
            s.level = 0;
            s.message = "InitWithHistorypose";
            kv.value = hardware_id_ + node_id_ + "09";
        }
        else if (data == DiagnosticType::LoopFailed)
        {
            s.level = 1;
            s.message = "LoopFailed";
            kv.value = hardware_id_ + node_id_ + "10";
        }
        else if (data == DiagnosticType::LoopDetected)
        {
            s.level = 1;
            s.message = "LoopDetected";
            kv.value = hardware_id_ + node_id_ + "11";
        }
        else if (data == DiagnosticType::InitializeFailed1)
        {
            s.level = 1;
            s.message = "InitializeFailed1";
            kv.value = hardware_id_ + node_id_ + "12";
        }
        else if (data == DiagnosticType::InitializeFailed2)
        {
            s.level = 1;
            s.message = "InitializeFailed2";
            kv.value = hardware_id_ + node_id_ + "13";
        }
        else if (data == DiagnosticType::InitializeSucceeded1)
        {
            s.level = 1;
            s.message = "InitializeSucceeded1";
            kv.value = hardware_id_ + node_id_ + "14";
        }
        else if (data == DiagnosticType::InitializeSucceeded2)
        {
            s.level = 0;
            s.message = "InitializeSucceeded2";
            kv.value = hardware_id_ + node_id_ + "15";
        }
        else if (data == DiagnosticType::InitWindowDelayed)
        {
            s.level = 2;
            s.message = "InitWindowDelayed";
            kv.value = hardware_id_ + node_id_ + "16";
        }
        else if (data == DiagnosticType::InitWindowCheckPositionFailed)
        {
            s.level = 2;
            s.message = "InitWindowCheckPositionFailed";
            kv.value = hardware_id_ + node_id_ + "17";
        }
        else if (data == DiagnosticType::InitWindowAddStateFailed1)
        {
            s.level = 2;
            s.message = "InitWindowAddStateFailed1";
            kv.value = hardware_id_ + node_id_ + "18";
        }
        else if (data == DiagnosticType::InitWindowSucceeded)
        {
            s.level = 0;
            s.message = "InitWindowSucceeded";
            kv.value = hardware_id_ + node_id_ + "19";
        }
        else if (data == DiagnosticType::InitWindowFailed)
        {
            s.level = 2;
            s.message = "InitWindowFailed";
            kv.value = hardware_id_ + node_id_ + "20";
        }
        else if (data == DiagnosticType::BackEndFinishedIfFinishTrue)
        {
            s.level = 2;
            s.message = "BackEndFinishedIfFinishTrue";
            kv.value = hardware_id_ + node_id_ + "21";
        }
        else if (data == DiagnosticType::BackEndFinished)
        {
            s.level = 2;
            s.message = "BackEndFinished";
            kv.value = hardware_id_ + node_id_ + "22";
        }
        else if (data == DiagnosticType::BackEndRunLDelayed)
        {
            s.level = 2;
            s.message = "BackEndRunLDelayed";
            kv.value = hardware_id_ + node_id_ + "23";
        }
        else if (data == DiagnosticType::BackEndRunPDelayed)
        {
            s.level = 2;
            s.message = "BackEndRunPDelayed";
            kv.value = hardware_id_ + node_id_ + "24";
        }
        else if (data == DiagnosticType::BackEndIfMeasureTrue)
        {
            s.level = 2;
            s.message = "BackEndIfMeasureTrue";
            kv.value = hardware_id_ + node_id_ + "25";
        }
        else if (data == DiagnosticType::GetNStateNegative)
        {
            s.level = 2;
            s.message = "GetNStateNegative";
            kv.value = hardware_id_ + node_id_ + "26";
        }
        else if (data == DiagnosticType::BackEndMeasureFusionAddStateFalse)
        {
            s.level = 2;
            s.message = "BackEndMeasureFusionAddStateFalse";
            kv.value = hardware_id_ + node_id_ + "27";
        }
        else if (data == DiagnosticType::BackEndMeasureFusionOptimizeFalse)
        {
            s.level = 2;
            s.message = "BackEndMeasureFusionOptimizeFalse";
            kv.value = hardware_id_ + node_id_ + "28";
        }
        else if (data == DiagnosticType::BackEndMeasureFusionSucceeded)
        {
            s.level = 0;
            s.message = "BackEndMeasureFusionSucceeded";
            kv.value = hardware_id_ + node_id_ + "29";
        }
        else if (data == DiagnosticType::BackEndIfPredictTrue)
        {
            s.level = 2;
            s.message = "BackEndIfPredictTrue";
            kv.value = hardware_id_ + node_id_ + "30";
        }
        else if (data == DiagnosticType::BackEndPredictFusionAddStateFalse)
        {
            s.level = 2;
            s.message = "BackEndPredictFusionAddStateFalse";
            kv.value = hardware_id_ + node_id_ + "31";
        }
        else if (data == DiagnosticType::BackEndPredictFusionOptimizeFalse)
        {
            s.level = 2;
            s.message = "BackEndPredictFusionOptimizeFalse";
            kv.value = hardware_id_ + node_id_ + "32";
        }
        else if (data == DiagnosticType::BackEndPredictFusionSucceeded)
        {
            s.level = 0;
            s.message = "BackEndPredictFusionSucceeded";
            kv.value = hardware_id_ + node_id_ + "33";
        }
        else if (data == DiagnosticType::FrontEndStatusFalse)
        {
            s.level = 2;
            s.message = "FrontEndStatusFalse";
            kv.value = hardware_id_ + node_id_ + "34";
        }
        else if (data == DiagnosticType::BackEndIfInitedFalse)
        {
            s.level = 2;
            s.message = "BackEndIfInitedFalse";
            kv.value = hardware_id_ + node_id_ + "35";
        }
        else if (data == DiagnosticType::BackEndIfInitedTrue)
        {
            s.level = 2;
            s.message = "BackEndIfInitedTrue";
            kv.value = hardware_id_ + node_id_ + "36";
        }
        else if (data == DiagnosticType::FrontEndUpdateFalse)
        {
            s.level = 2;
            s.message = "FrontEndUpdateFalse";
            kv.value = hardware_id_ + node_id_ + "37";
        }
        else if (data == DiagnosticType::Finish)
        {
            s.level = 2;
            s.message = "Finish";
            kv.value = hardware_id_ + node_id_ + "38";
        }
        else if (data == DiagnosticType::Finished)
        {
            s.level = 2;
            s.message = "Finished";
            kv.value = hardware_id_ + node_id_ + "39";
        }
        else if (data == DiagnosticType::InitWindowAddStateSucceeded)
        {
            s.level = 0;
            s.message = "InitWindowAddStateSucceeded";
            kv.value = hardware_id_ + node_id_ + "40";
        }
        else if (data == DiagnosticType::WindowInited)
        {
            s.level = 2;
            s.message = "WindowInited";
            kv.value = hardware_id_ + node_id_ + "41";
        }
        else if (data == InitializeFailed1Succeeded1)
        {
            s.level = 0;
            s.message = "InitializeFailed1Succeeded1";
            kv.value = hardware_id_ + node_id_ + "42";
        }
        else if (data == InitializeFailed1Failed1)
        {
            s.level = 2;
            s.message = "InitializeFailed1Failed1";
            kv.value = hardware_id_ + node_id_ + "43";
        }
        else if (data == InitWindowFailedInitIndexError)
        {
            s.level = 2;
            s.message = "InitWindowFailedInitIndexError";
            kv.value = hardware_id_ + node_id_ + "44";
        }
        else if (data == BackEndIfMeasureGetNStateFalse1)
        {
            s.level = 2;
            s.message = "BackEndIfMeasureGetNStateFalse1";
            kv.value = hardware_id_ + node_id_ + "45";
        }
        else if (data == BackEndIfMeasureGetMStampFalse)
        {
            s.level = 2;
            s.message = "BackEndIfMeasureGetMStampFalse";
            kv.value = hardware_id_ + node_id_ + "46";
        }
        else if (data == BackEndIfMeasureGetNStateFalse2)
        {
            s.level = 2;
            s.message = "BackEndIfMeasureGetNStateFalse2";
            kv.value = hardware_id_ + node_id_ + "47";
        }
        else if (data == BackEndIfMeasureGetMStateFalseWait)
        {
            s.level = 2;
            s.message = "BackEndIfMeasureGetMStateFalseWait";
            kv.value = hardware_id_ + node_id_ + "48";
        }
        else if (data == BackEndIfMeasureGetMStateFalseSetZero)
        {
            s.level = 2;
            s.message = "BackEndIfMeasureGetMStateFalseSetZero";
            kv.value = hardware_id_ + node_id_ + "49";
        }
        else if (data == BackEndMeasureNoLData)
        {
            s.level = 2;
            s.message = "BackEndMeasureNoLData";
            kv.value = hardware_id_ + node_id_ + "50";
        }
        else if (data == BackEndMeasureNoPData)
        {
            s.level = 2;
            s.message = "BackEndMeasureNoPData";
            kv.value = hardware_id_ + node_id_ + "51";
        }
        else if (data == BackEndIfPredictGetNStateFalse)
        {
            s.level = 2;
            s.message = "BackEndIfPredictGetNStateFalse";
            kv.value = hardware_id_ + node_id_ + "52";
        }
        else if (data == BackEndIfPredictNoData)
        {
            s.level = 2;
            s.message = "BackEndIfPredictNoData";
            kv.value = hardware_id_ + node_id_ + "53";
        }
        else if (data == InitializeNoMeasure1)
        {
            s.level = 2;
            s.message = "InitializeNoMeasure1";
            kv.value = hardware_id_ + node_id_ + "54";
        }
        else if (data == InitializeNoMeasure2)
        {
            s.level = 2;
            s.message = "InitializeNoMeasure2";
            kv.value = hardware_id_ + node_id_ + "55";
        }
        else if (data == InitWindowNoMData1)
        {
            s.level = 2;
            s.message = "InitWindowNoMData1";
            kv.value = hardware_id_ + node_id_ + "56";
        }
        else if (data == InitWindowNoLData1)
        {
            s.level = 2;
            s.message = "InitWindowNoLData1";
            kv.value = hardware_id_ + node_id_ + "57";
        }
        else if (data == InitWindowNoPData1)
        {
            s.level = 2;
            s.message = "InitWindowNoPData1";
            kv.value = hardware_id_ + node_id_ + "58";
        }
        else if (data == InitWindowNoMData2)
        {
            s.level = 2;
            s.message = "InitWindowNoMData2";
            kv.value = hardware_id_ + node_id_ + "59";
        }
        else if (data == InitWindowNoLData2)
        {
            s.level = 2;
            s.message = "InitWindowNoLData2";
            kv.value = hardware_id_ + node_id_ + "60";
        }
        else if (data == InitWindowNoPData2)
        {
            s.level = 2;
            s.message = "InitWindowNoPData2";
            kv.value = hardware_id_ + node_id_ + "61";
        }
        else if (data == InitWindowAddStateFailed2)
        {
            s.level = 2;
            s.message = "InitWindowAddStateFailed2";
            kv.value = hardware_id_ + node_id_ + "62";
        }
        s.values.push_back(kv);
        log.status.push_back(s);
        publisher_.publish(log);
    }
} // namespace yd_fusion_localization