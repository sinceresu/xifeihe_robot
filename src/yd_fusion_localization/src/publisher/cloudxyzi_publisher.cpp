/**
 * @file cloudxyzi_publisher.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-7-13
 * @brief cloudxyzi publisher
 */

#include "yd_fusion_localization/publisher/cloudxyzi_publisher.hpp"

namespace yd_fusion_localization
{
    CloudxyziPublisher::CloudxyziPublisher(ros::NodeHandle &nh,
                                 const std::string &topic_name,
                                 const std::string &frame_id,
                                 int buff_size,
                                 bool latch) : Publisher(frame_id)
    {
        publisher_ = nh.advertise<sensor_msgs::PointCloud2>(topic_name, buff_size, latch);
    }

    void CloudxyziPublisher::Publish(const PublishDataPtr &data)
    {
        CloudXYZIPublishDataPtr cloudxyzi_data = std::dynamic_pointer_cast<CloudXYZIPublishData>(data);
        sensor_msgs::PointCloud2Ptr cloudxyzi_publish(new sensor_msgs::PointCloud2());
        pcl::toROSMsg(*(cloudxyzi_data->xyzi_ptr), *cloudxyzi_publish);

        cloudxyzi_publish->header.stamp = ros::Time(cloudxyzi_data->stamp);
        cloudxyzi_publish->header.frame_id = frame_id_;

        publisher_.publish(*cloudxyzi_publish);
    }
} // namespace yd_fusion_localization