/**
 * @file cloud_publisher.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-29
 * @brief cloud publisher
 */

#include "yd_fusion_localization/publisher/cloud_publisher.hpp"

namespace yd_fusion_localization
{
    CloudPublisher::CloudPublisher(ros::NodeHandle &nh,
                                 const std::string &topic_name,
                                 const std::string &frame_id,
                                 int buff_size,
                                 bool latch) : Publisher(frame_id)
    {
        publisher_ = nh.advertise<sensor_msgs::PointCloud2>(topic_name, buff_size, latch);
    }

    void CloudPublisher::Publish(const PublishDataPtr &data)
    {
        CloudPublishDataPtr cloud_data = std::dynamic_pointer_cast<CloudPublishData>(data);
        sensor_msgs::PointCloud2Ptr cloud_publish(new sensor_msgs::PointCloud2());
        pcl::toROSMsg(*(cloud_data->cloud_ptr), *cloud_publish);

        cloud_publish->header.stamp = ros::Time(cloud_data->stamp);
        cloud_publish->header.frame_id = frame_id_;

        publisher_.publish(*cloud_publish);
    }
} // namespace yd_fusion_localization