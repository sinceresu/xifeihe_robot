/**
 * @file odometry_publisher.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-9-2
 * @brief odometry publisher
 */

#include "yd_fusion_localization/publisher/odometry_publisher.hpp"

namespace yd_fusion_localization
{
    OdometryPublisher::OdometryPublisher(ros::NodeHandle &nh,
                                 const std::string &topic_name,
                                 const std::string &frame_id,
                                 int buff_size,
                                 bool latch) : Publisher(frame_id)
    {
        publisher_ = nh.advertise<nav_msgs::Odometry>(topic_name, buff_size, latch);
    }

    void OdometryPublisher::Publish(const PublishDataPtr &data)
    {
        OdometryPublishDataPtr odom_data = std::dynamic_pointer_cast<OdometryPublishData>(data);
        nav_msgs::Odometry odometry_publish;

        odometry_publish.header.stamp = ros::Time(odom_data->stamp);
        odometry_publish.header.frame_id = frame_id_;

        TransformDataType(odom_data->pose, odometry_publish.pose.pose);

        CopyCovariance(odom_data->posecov, &(odometry_publish.pose.covariance[0]));

        auto  linear_velocity =  odom_data->twist.block<3, 1>(0, 0);
        decltype(odometry_publish.twist.twist.linear) linear;
        linear.x = linear_velocity(0, 0),  linear.y = linear_velocity(1, 0),  linear.z = linear_velocity(2, 0);
        odometry_publish.twist.twist.linear =linear;

        auto  augular_velocity =  odom_data->twist.block<3, 1>(3, 0);
        decltype(odometry_publish.twist.twist.angular) angular;
        angular.x = augular_velocity(0, 0),  angular.y = augular_velocity(1, 0),  angular.z = augular_velocity(2, 0);
        odometry_publish.twist.twist.angular =angular;

        publisher_.publish(odometry_publish);
    }
} // namespace yd_fusion_localization
