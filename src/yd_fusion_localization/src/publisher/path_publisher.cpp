/**
 * @file path_publisher.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-7-12
 * @brief path publisher
 */

#include "yd_fusion_localization/publisher/path_publisher.hpp"

namespace yd_fusion_localization
{
    PathPublisher::PathPublisher(ros::NodeHandle &nh,
                                 const std::string &topic_name,
                                 const std::string &frame_id,
                                 int buff_size,
                                 bool latch) : Publisher(frame_id)
    {
        publisher_ = nh.advertise<nav_msgs::Path>(topic_name, buff_size, latch);
    }

    void PathPublisher::Publish(const PublishDataPtr &data)
    {
        PathPublishDataPtr path_data = std::dynamic_pointer_cast<PathPublishData>(data);
        nav_msgs::Path path_publish;

        path_publish.header.stamp = ros::Time(path_data->stamp);
        path_publish.header.frame_id = frame_id_;

        for (auto &pose : path_data->poses)
        {
            geometry_msgs::PoseStamped pp;
            TransformDataType(pose.second, pp.pose);
            path_publish.poses.emplace_back(pp);
        }

        publisher_.publish(path_publish);
    }
} // namespace yd_fusion_localization