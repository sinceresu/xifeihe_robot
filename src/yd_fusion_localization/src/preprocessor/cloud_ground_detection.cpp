/**
 * @file cloud_ground_detection.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-6-2
 * @brief cloud ground detection
 */

#include "yd_fusion_localization/preprocessor/cloud_ground_detection.hpp"

namespace yd_fusion_localization
{
    CloudGroundDetection::CloudGroundDetection(const YAML::Node &yaml_node) : Preprocessor(yaml_node)
    {
        sensor_height_ = yaml_node["sensor_height"].as<float>();
        height_clip_range_ = yaml_node["height_clip_range"].as<float>();
        normal_filter_thresh_ = yaml_node["normal_filter_thresh"].as<float>();
        ground_points_thresh_ = yaml_node["ground_points_thresh"].as<float>();
        ground_normal_thresh_ = yaml_node["ground_normal_thresh"].as<float>();
        extracted_ = boost::make_shared<pcl::PointCloud<pcl::PointXYZI>>();
        extracted2_ = boost::make_shared<pcl::PointCloud<pcl::PointXYZI>>();
    }

    CloudGroundDetection::~CloudGroundDetection()
    {
    }

    bool CloudGroundDetection::Preprocess(const MeasureDataPtr &data, const BState &bstate, PreprocessedDataPtr &preprocessed_data)
    {
        return false;
    }

    bool CloudGroundDetection::Preprocess(PreprocessedDataPtr &data1, PreprocessedDataPtr &data2)
    {
        if (data1->GetType() != PreprocessedType::CloudImage)
        {
            return false;
        }
        if (data2 == nullptr)
        {
            data2 = std::make_shared<CloudPlaneData>();
        }
        if (data2->GetType() != PreprocessedType::CloudPlane)
        {
            return false;
        }
        CloudImageDataPtr cloud_image = std::dynamic_pointer_cast<CloudImageData>(data1);
        CloudPlaneDataPtr cloud_ground = std::dynamic_pointer_cast<CloudPlaneData>(data2);
        cloud_ground->stamp = cloud_image->stamp;
        cloud_ground->sensor_to_robot = cloud_image->sensor_to_robot;
        cloud_ground->noise = cloud_image->noise;
        cloud_ground->xyzi_ptr.reset(new pcl::PointCloud<pcl::PointXYZI>());

        CloudImageExtraction(cloud_image);

        Eigen::Matrix4f matrix = (cloud_ground->sensor_to_robot).matrix().cast<float>();
        pcl::transformPointCloud(*extracted_, *extracted_, matrix);

        bool res;
        res = PlaneClip<pcl::PointXYZI>(extracted_, extracted2_, Eigen::Vector4f(0.0f, 0.0f, 1.0f, sensor_height_ + height_clip_range_), false);
        if (!res)
        {
            return false;
        }

        res = PlaneClip<pcl::PointXYZI>(extracted2_, extracted_, Eigen::Vector4f(0.0f, 0.0f, 1.0f, sensor_height_ - height_clip_range_), true);
        if (!res)
        {
            return false;
        }

        res = NormalFilter<pcl::PointXYZI>(extracted_, extracted2_);
        if (!res)
        {
            return false;
        }

        matrix = (cloud_ground->sensor_to_robot.inverse()).matrix().cast<float>();
        pcl::transformPointCloud(*extracted2_, *extracted2_, matrix);

        Eigen::Vector3f reference = matrix.block<3, 3>(0, 0) * Eigen::Vector3f::UnitZ();
        res = Ransac<pcl::PointXYZI>(extracted2_, reference, cloud_ground->xyzi_ptr, cloud_ground->coeffs);
        return res;
    }

    void CloudGroundDetection::CloudImageExtraction(const CloudImageDataPtr &cloud_image)
    {
        extracted_->clear();
        // extract image cloud
        for (int i = 0; i < cloud_image->rows; ++i)
        {
            for (int j = 0; j < cloud_image->cols; ++j)
            {
                if (cloud_image->range_matrix.at<float>(i, j) != FLT_MAX)
                {
                    // save extracted cloud
                    extracted_->points.push_back(cloud_image->xyzi_ptr->points[j + i * cloud_image->cols]);
                }
            }
        }
    }

    template <typename PointType>
    bool CloudGroundDetection::PlaneClip(const typename pcl::PointCloud<PointType>::Ptr &src_cloud, typename pcl::PointCloud<PointType>::Ptr &dst_cloud, const Eigen::Vector4f &plane, bool negative)
    {
        pcl::PlaneClipper3D<PointType> clipper(plane);
        pcl::PointIndices::Ptr indices(new pcl::PointIndices);

        clipper.clipPointCloud3D(*src_cloud, indices->indices);

        pcl::ExtractIndices<PointType> extract;
        extract.setInputCloud(src_cloud);
        extract.setIndices(indices);
        extract.setNegative(negative);
        dst_cloud->clear();
        extract.filter(*dst_cloud);
        if (dst_cloud->size() < ground_points_thresh_)
        {
            return false;
        }
        return true;
    }

    template <typename PointType>
    bool CloudGroundDetection::NormalFilter(const typename pcl::PointCloud<PointType>::Ptr &src_cloud, typename pcl::PointCloud<PointType>::Ptr &dst_cloud)
    {
        typename pcl::NormalEstimation<PointType, pcl::Normal> ne;
        ne.setInputCloud(src_cloud);

        typename pcl::search::KdTree<PointType>::Ptr tree(new pcl::search::KdTree<PointType>);
        ne.setSearchMethod(tree);

        pcl::PointCloud<pcl::Normal>::Ptr normals(new pcl::PointCloud<pcl::Normal>);
        ne.setKSearch(10);
        ne.setViewPoint(0.0f, 0.0f, sensor_height_);
        ne.compute(*normals);

        dst_cloud->clear();
        for (int i = 0; i < src_cloud->size(); i++)
        {
            float dot = normals->at(i).getNormalVector3fMap().normalized().dot(Eigen::Vector3f::UnitZ());
            if (std::abs(dot) > std::cos(normal_filter_thresh_ * M_PI / 180.0))
            {
                dst_cloud->push_back(src_cloud->at(i));
            }
        }

        dst_cloud->width = dst_cloud->size();
        dst_cloud->height = 1;
        dst_cloud->is_dense = true;
        if (dst_cloud->size() < ground_points_thresh_)
        {
            return false;
        }
        return true;
    }

    template <typename PointType>
    bool CloudGroundDetection::Ransac(const typename pcl::PointCloud<PointType>::Ptr &src_cloud, const Eigen::Vector3f reference, typename pcl::PointCloud<PointType>::Ptr &dst_cloud, Eigen::Vector4f &coeffs)
    {
        typename pcl::SampleConsensusModelPlane<PointType>::Ptr model_p(new pcl::SampleConsensusModelPlane<PointType>(src_cloud));
        pcl::RandomSampleConsensus<PointType> ransac(model_p);
        ransac.setDistanceThreshold(0.1);
        ransac.computeModel();

        pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
        ransac.getInliers(inliers->indices);

        // too few inliers
        if (inliers->indices.size() < ground_points_thresh_)
        {
            return false;
        }
        Eigen::VectorXf temp;
        ransac.getModelCoefficients(temp);
        coeffs = temp;

        // verticality check of the detected floor's normal
        double dot = coeffs.head<3>().dot(reference);
        if (std::abs(dot) < std::cos(ground_normal_thresh_ * M_PI / 180.0))
        {
            // the normal is not vertical
            return false;
        }

        // make the normal upward
        if (coeffs.head<3>().dot(Eigen::Vector3f::UnitZ()) < 0.0f)
        {
            coeffs *= -1.0f;
        }

        dst_cloud->clear();
        pcl::ExtractIndices<PointType> extract;
        extract.setInputCloud(src_cloud);
        extract.setIndices(inliers);
        extract.filter(*dst_cloud);
        return true;
    }
}