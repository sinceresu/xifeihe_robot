/**
 * @file cloud_feature_extraction.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-6-2
 * @brief cloud feature extraction
 */

#include "yd_fusion_localization/preprocessor/cloud_feature_extraction.hpp"

namespace yd_fusion_localization
{
    CloudFeatureExtraction::CloudFeatureExtraction(const YAML::Node &yaml_node) : Preprocessor(yaml_node)
    {
        scan_number_ = yaml_node["scan_number"].as<int>();
        horizon_scan_ = yaml_node["horizon_scan"].as<int>();
        edge_threshold_ = yaml_node["edge_threshold"].as<float>();
        surf_threshold_ = yaml_node["surf_threshold"].as<float>();

        cloud_curvature_ = new float[scan_number_ * horizon_scan_];
        cloud_neighbor_picked_ = new int[scan_number_ * horizon_scan_];
        cloud_label_ = new int[scan_number_ * horizon_scan_];
        cloud_smoothness_.resize(scan_number_ * horizon_scan_);
        surface_filter_ = std::make_shared<VoxelFilter<pcl::PointXYZI>>(yaml_node["suface_filter"]);
    }

    CloudFeatureExtraction::~CloudFeatureExtraction()
    {
        delete[] cloud_curvature_;
        cloud_curvature_ = nullptr;
        delete[] cloud_neighbor_picked_;
        cloud_neighbor_picked_ = nullptr;
        delete[] cloud_label_;
        cloud_label_ = nullptr;
    }

    bool CloudFeatureExtraction::Preprocess(const MeasureDataPtr &data, const BState &bstate, PreprocessedDataPtr &preprocessed_data)
    {
        return false;
    }

    bool CloudFeatureExtraction::Preprocess(PreprocessedDataPtr &data1, PreprocessedDataPtr &data2)
    {
        if (data1->GetType() != PreprocessedType::CloudImage)
        {
            return false;
        }
        if (data2 == nullptr)
        {
            data2 = std::make_shared<CloudFeatureData>();
        }
        if (data2->GetType() != PreprocessedType::CloudFeature)
        {
            return false;
        }
        CloudImageDataPtr cloud_image = std::dynamic_pointer_cast<CloudImageData>(data1);
        CloudFeatureDataPtr cloud_feature = std::dynamic_pointer_cast<CloudFeatureData>(data2);
        cloud_feature->stamp = cloud_image->stamp;
        cloud_feature->sensor_to_robot = cloud_image->sensor_to_robot;
        cloud_feature->noise = cloud_image->noise;
        CloudImageExtraction(cloud_image, cloud_feature);
        CalculateSmoothness(cloud_feature);
        MarkOccludedPoints(cloud_feature);
        ExtractFeatures(cloud_feature);
        return true;
    }

    void CloudFeatureExtraction::CloudImageExtraction(const CloudImageDataPtr &cloud_image, CloudFeatureDataPtr &cloud_feature)
    {
        cloud_feature->startRingIndex.clear();
        cloud_feature->endRingIndex.clear();
        cloud_feature->pointColInd.clear();
        cloud_feature->pointRange.clear();
        cloud_feature->xyzi_ptr.reset(new pcl::PointCloud<pcl::PointXYZI>());
        cloud_feature->xyzi_ptr->clear();
        int count = 0;
        // extract segmented cloud for lidar odometry
        for (int i = 0; i < cloud_image->rows; ++i)
        {
            cloud_feature->startRingIndex.push_back(count + 5);
            for (int j = 0; j < cloud_image->cols; ++j)
            {
                if (cloud_image->range_matrix.at<float>(i, j) != FLT_MAX)
                {
                    // mark the points' column index for marking occlusion later
                    cloud_feature->pointColInd.push_back(j);
                    // save range info
                    cloud_feature->pointRange.push_back(cloud_image->range_matrix.at<float>(i, j));
                    // save extracted cloud
                    cloud_feature->xyzi_ptr->points.push_back(cloud_image->xyzi_ptr->points[j + i * cloud_image->cols]);
                    // size of extracted cloud
                    ++count;
                }
            }
            cloud_feature->endRingIndex.push_back(count - 1 - 5);
        }
    }

    void CloudFeatureExtraction::CalculateSmoothness(CloudFeatureDataPtr &cloud_feature)
    {
        int cloudSize = cloud_feature->xyzi_ptr->points.size();
        for (int i = 5; i < cloudSize - 5; i++)
        {
            float diffRange = cloud_feature->pointRange[i - 5] + cloud_feature->pointRange[i - 4] + cloud_feature->pointRange[i - 3] + cloud_feature->pointRange[i - 2] + cloud_feature->pointRange[i - 1] - cloud_feature->pointRange[i] * 10 + cloud_feature->pointRange[i + 1] + cloud_feature->pointRange[i + 2] + cloud_feature->pointRange[i + 3] + cloud_feature->pointRange[i + 4] + cloud_feature->pointRange[i + 5];

            assert(i < scan_number_ * horizon_scan_);
            assert(i >= 0);
            cloud_curvature_[i] = diffRange * diffRange; //diffX * diffX + diffY * diffY + diffZ * diffZ;

            cloud_neighbor_picked_[i] = 0;
            cloud_label_[i] = 0;
            // cloud_smoothness_ for sorting
            cloud_smoothness_[i].value = cloud_curvature_[i];
            cloud_smoothness_[i].ind = i;
        }
    }
    void CloudFeatureExtraction::MarkOccludedPoints(CloudFeatureDataPtr &cloud_feature)
    {
        int cloudSize = cloud_feature->xyzi_ptr->points.size();
        // mark occluded points and parallel beam points
        for (int i = 5; i < cloudSize - 6; ++i)
        {
            // occluded points
            float depth1 = cloud_feature->pointRange[i];
            float depth2 = cloud_feature->pointRange[i + 1];
            int columnDiff = std::abs(int(cloud_feature->pointColInd[i + 1] - cloud_feature->pointColInd[i]));
            assert(i < scan_number_ * horizon_scan_ - 6);
            assert(i >= 5);

            if (columnDiff < 10)
            {
                // 10 pixel diff in range image
                if (depth1 - depth2 > 0.3)
                {
                    cloud_neighbor_picked_[i - 5] = 1;
                    cloud_neighbor_picked_[i - 4] = 1;
                    cloud_neighbor_picked_[i - 3] = 1;
                    cloud_neighbor_picked_[i - 2] = 1;
                    cloud_neighbor_picked_[i - 1] = 1;
                    cloud_neighbor_picked_[i] = 1;
                }
                else if (depth2 - depth1 > 0.3)
                {
                    cloud_neighbor_picked_[i + 1] = 1;
                    cloud_neighbor_picked_[i + 2] = 1;
                    cloud_neighbor_picked_[i + 3] = 1;
                    cloud_neighbor_picked_[i + 4] = 1;
                    cloud_neighbor_picked_[i + 5] = 1;
                    cloud_neighbor_picked_[i + 6] = 1;
                }
            }
            // parallel beam
            float diff1 = std::abs(float(cloud_feature->pointRange[i - 1] - cloud_feature->pointRange[i]));
            float diff2 = std::abs(float(cloud_feature->pointRange[i + 1] - cloud_feature->pointRange[i]));

            if (diff1 > 0.02 * cloud_feature->pointRange[i] && diff2 > 0.02 * cloud_feature->pointRange[i])
                cloud_neighbor_picked_[i] = 1;
        }
    }
    void CloudFeatureExtraction::ExtractFeatures(CloudFeatureDataPtr &cloud_feature)
    {
        cloud_feature->corner_ptr.reset(new pcl::PointCloud<pcl::PointXYZI>());
        cloud_feature->surface_ptr.reset(new pcl::PointCloud<pcl::PointXYZI>());
        cloud_feature->corner_ptr->clear();
        cloud_feature->surface_ptr->clear();

        pcl::PointCloud<pcl::PointXYZI>::Ptr surfaceCloudScan(new pcl::PointCloud<pcl::PointXYZI>());
        pcl::PointCloud<pcl::PointXYZI>::Ptr surfaceCloudScanDS(new pcl::PointCloud<pcl::PointXYZI>());

        for (int i = 0; i < cloud_feature->startRingIndex.size(); i++)
        {
            surfaceCloudScan->clear();

            for (int j = 0; j < 6; j++)
            {
                int sp = (cloud_feature->startRingIndex[i] * (6 - j) + cloud_feature->endRingIndex[i] * j) / 6;
                int ep = (cloud_feature->startRingIndex[i] * (5 - j) + cloud_feature->endRingIndex[i] * (j + 1)) / 6 - 1;

                if (sp >= ep)
                    continue;

                std::sort(cloud_smoothness_.begin() + sp, cloud_smoothness_.begin() + ep, by_value());

                int largestPickedNum = 0;
                for (int k = ep; k >= sp; k--)
                {
                    int ind = cloud_smoothness_[k].ind;
                    assert(ind < scan_number_ * horizon_scan_);
                    assert(ind >= 0);
                    if (cloud_neighbor_picked_[ind] == 0 && cloud_curvature_[ind] > edge_threshold_)
                    {
                        largestPickedNum++;
                        if (largestPickedNum <= 20)
                        {
                            cloud_label_[ind] = 1;
                            cloud_feature->corner_ptr->push_back(cloud_feature->xyzi_ptr->points[ind]);
                        }
                        else
                        {
                            break;
                        }

                        cloud_neighbor_picked_[ind] = 1;
                        assert(ind + 5 < scan_number_ * horizon_scan_);
                        assert(ind - 5 >= 0);
                        for (int l = 1; l <= 5; l++)
                        {
                            int columnDiff = std::abs(int(cloud_feature->pointColInd[ind + l] - cloud_feature->pointColInd[ind + l - 1]));
                            if (columnDiff > 10)
                                break;
                            cloud_neighbor_picked_[ind + l] = 1;
                        }
                        for (int l = -1; l >= -5; l--)
                        {
                            int columnDiff = std::abs(int(cloud_feature->pointColInd[ind + l] - cloud_feature->pointColInd[ind + l + 1]));
                            if (columnDiff > 10)
                                break;
                            cloud_neighbor_picked_[ind + l] = 1;
                        }
                    }
                }

                for (int k = sp; k <= ep; k++)
                {
                    int ind = cloud_smoothness_[k].ind;
                    assert(ind < scan_number_ * horizon_scan_);
                    assert(ind >= 0);
                    if (cloud_neighbor_picked_[ind] == 0 && cloud_curvature_[ind] < surf_threshold_)
                    {
                        cloud_label_[ind] = -1;
                        cloud_neighbor_picked_[ind] = 1;

                        assert(ind + 5 < scan_number_ * horizon_scan_);
                        assert(ind - 5 >= 0);
                        for (int l = 1; l <= 5; l++)
                        {
                            int columnDiff = std::abs(int(cloud_feature->pointColInd[ind + l] - cloud_feature->pointColInd[ind + l - 1]));
                            if (columnDiff > 10)
                                break;

                            cloud_neighbor_picked_[ind + l] = 1;
                        }
                        for (int l = -1; l >= -5; l--)
                        {
                            int columnDiff = std::abs(int(cloud_feature->pointColInd[ind + l] - cloud_feature->pointColInd[ind + l + 1]));
                            if (columnDiff > 10)
                                break;

                            cloud_neighbor_picked_[ind + l] = 1;
                        }
                    }
                }

                for (int k = sp; k <= ep; k++)
                {
                    assert(k < scan_number_ * horizon_scan_);
                    assert(k >= 0);
                    if (cloud_label_[k] <= 0)
                    {
                        surfaceCloudScan->push_back(cloud_feature->xyzi_ptr->points[k]);
                    }
                }
            }

            surfaceCloudScanDS->clear();
            surface_filter_->Filter(surfaceCloudScan, surfaceCloudScanDS);

            *(cloud_feature->surface_ptr) += *surfaceCloudScanDS;
        }
    }
}