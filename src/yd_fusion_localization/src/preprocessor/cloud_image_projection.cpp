/**
 * @file cloud_image_projection.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-6-2
 * @brief cloud image projection
 */

#include "yd_fusion_localization/preprocessor/cloud_image_projection.hpp"

namespace yd_fusion_localization
{
    CloudImageProjection::CloudImageProjection(const YAML::Node &yaml_node) : Preprocessor(yaml_node)
    {
        frame_distance_threshold_ = yaml_node["frame_distance_threshold"].as<std::vector<float>>();
        scan_number_ = yaml_node["scan_number"].as<int>();
        horizon_scan_ = yaml_node["horizon_scan"].as<int>();
        scan_period_ = yaml_node["scan_period"].as<double>();
    }

    bool CloudImageProjection::Preprocess(const MeasureDataPtr &data, const BState &bstate, PreprocessedDataPtr &preprocessed_data)
    {
        if (data->GetType() != MeasureType::CloudM)
        {
            return false;
        }
        if (preprocessed_data == nullptr)
        {
            preprocessed_data = std::make_shared<CloudImageData>();
        }
        if (preprocessed_data->GetType() != PreprocessedType::CloudImage)
        {
            return false;
        }
        CloudDataPtr cloud_data = std::dynamic_pointer_cast<CloudData>(data);
        if (!AddIRTtoPointCloud(cloud_data))
        {
            return false;
        }
        CloudImageDataPtr cloud_preprocessed = std::dynamic_pointer_cast<CloudImageData>(preprocessed_data);
        cloud_preprocessed->stamp = cloud_data->stamp;
        cloud_preprocessed->sensor_to_robot = cloud_data->sensor_to_robot;
        cloud_preprocessed->noise = cloud_data->noise;
        BState new_bstate(bstate);
        TransformRM2SL(cloud_data->sensor_to_robot, Sophus::SE3d(Eigen::Quaterniond::Identity(), Eigen::Vector3d::Zero()), new_bstate);
        ProjectPointCloud(cloud_data, new_bstate, cloud_preprocessed);
        return true;
    }

    bool CloudImageProjection::Preprocess(PreprocessedDataPtr &data1, PreprocessedDataPtr &data2)
    {
        return false;
    }

    bool CloudImageProjection::AddIRTtoPointCloud(const CloudDataPtr &cloud_data)
    {
        if (cloud_data->point_type != CloudPointType::XYZIRT)
        {
            cloud_data->xyzirt_ptr.reset(new pcl::PointCloud<PointXYZIRT>());
        }
        if (cloud_data->point_type == CloudPointType::XYZ)
        {
            RemovePointCloud(*(cloud_data->xyz_ptr), *(cloud_data->xyz_ptr), frame_distance_threshold_[0], frame_distance_threshold_[1]);
            return AddIRTtoPointCloud(CloudPointType::XYZ, *(cloud_data->xyz_ptr), *(cloud_data->xyzirt_ptr));
        }
        else if (cloud_data->point_type == CloudPointType::XYZI)
        {
            RemovePointCloud(*(cloud_data->xyzi_ptr), *(cloud_data->xyzi_ptr), frame_distance_threshold_[0], frame_distance_threshold_[1]);
            return AddIRTtoPointCloud(CloudPointType::XYZI, *(cloud_data->xyz_ptr), *(cloud_data->xyzirt_ptr));
        }
        else if (cloud_data->point_type == CloudPointType::XYZT)
        {
            RemovePointCloud(*(cloud_data->xyzt_ptr), *(cloud_data->xyzt_ptr), frame_distance_threshold_[0], frame_distance_threshold_[1]);
            return AddIRTtoPointCloud(CloudPointType::XYZT, *(cloud_data->xyz_ptr), *(cloud_data->xyzirt_ptr));
        }
        else if (cloud_data->point_type == CloudPointType::XYZIRT)
        {
            RemovePointCloud(*(cloud_data->xyzirt_ptr), *(cloud_data->xyzirt_ptr), frame_distance_threshold_[0], frame_distance_threshold_[1]);
            return true;
        }
        else
        {
            return false;
        }
    }

    void CloudImageProjection::ProjectPointCloud(const CloudDataPtr &cloud_data, const BState &bstate, CloudImageDataPtr &cloud_image)
    {
        cloud_image->rows = scan_number_;
        cloud_image->cols = horizon_scan_;
        cloud_image->range_matrix = cv::Mat(cloud_image->rows, cloud_image->cols, CV_32F, cv::Scalar::all(FLT_MAX));
        cloud_image->xyzi_ptr.reset(new pcl::PointCloud<pcl::PointXYZI>());
        cloud_image->xyzi_ptr->points.resize(cloud_image->rows * cloud_image->cols);
        cloud_image->ang_res_x = 360.0 / float(cloud_image->cols);
        cloud_image->ang_res_y = 2.0;
        pcl::PointCloud<PointXYZIRT>::Ptr cloud_in = cloud_data->xyzirt_ptr;
        int cloudSize = (int)cloud_in->points.size();
        // range image projection
        for (int i = 0; i < cloudSize; ++i)
        {
            pcl::PointXYZI thisPoint;
            thisPoint.x = cloud_in->points[i].x;
            thisPoint.y = cloud_in->points[i].y;
            thisPoint.z = cloud_in->points[i].z;
            thisPoint.intensity = cloud_in->points[i].intensity;

            int rowIdn = cloud_in->points[i].ring;
            if (rowIdn < 0 || rowIdn >= cloud_image->rows)
                continue;

            float horizonAngle = std::atan2(thisPoint.x, thisPoint.y) * 180 / M_PI;

            int columnIdn = -std::round((horizonAngle - 90.0) / cloud_image->ang_res_x) + cloud_image->cols / 2;
            if (columnIdn >= cloud_image->cols)
                columnIdn -= cloud_image->cols;

            if (columnIdn < 0 || columnIdn >= cloud_image->cols)
                continue;

            float range = cloud_in->points[i].x * cloud_in->points[i].x + cloud_in->points[i].y * cloud_in->points[i].y + cloud_in->points[i].z * cloud_in->points[i].z;
            range = std::sqrt(range);

            if (cloud_image->range_matrix.at<float>(rowIdn, columnIdn) != FLT_MAX)
                continue;

            cloud_image->range_matrix.at<float>(rowIdn, columnIdn) = range;

            DeskewPoint(cloud_in->points[i], thisPoint, bstate);

            int index = columnIdn + rowIdn * cloud_image->cols;
            cloud_image->xyzi_ptr->points[index] = thisPoint;
        }
    }

    template <typename PointT>
    bool CloudImageProjection::RemovePointCloud(const pcl::PointCloud<PointT> &cloud_in, pcl::PointCloud<PointT> &cloud_out, float lower_bound, float upper_bound)
    {
        if (&cloud_in != &cloud_out)
        {
            cloud_out.header = cloud_in.header;
            cloud_out.points.resize(cloud_in.points.size());
        }

        size_t j = 0;
        float lb2 = lower_bound * lower_bound;
        float ub2 = upper_bound * upper_bound;
        for (size_t i = 0; i < cloud_in.points.size(); ++i)
        {
            if (!std::isfinite(cloud_in.points[i].x) ||
                !std::isfinite(cloud_in.points[i].y) ||
                !std::isfinite(cloud_in.points[i].z))
                continue;
            float dis = cloud_in.points[i].x * cloud_in.points[i].x + cloud_in.points[i].y * cloud_in.points[i].y + cloud_in.points[i].z * cloud_in.points[i].z;
            if (dis < lb2 || dis > ub2)
                continue;
            cloud_out.points[j] = cloud_in.points[i];
            j++;
        }
        if (j != cloud_in.points.size())
        {
            cloud_out.points.resize(j);
        }

        cloud_out.height = 1;
        cloud_out.width = static_cast<uint32_t>(j);
        cloud_out.is_dense = true;
        return true;
    }

    template <typename PointT>
    bool CloudImageProjection::AddIRTtoPointCloud(CloudPointType type, const pcl::PointCloud<PointT> &cloud_in, pcl::PointCloud<PointXYZIRT> &cloud_out)
    {
        if (type == CloudPointType::XYZIRT)
        {
            return true;
        }
        cloud_out.clear();
        int cloudSize = cloud_in.points.size();
        float startOri = -std::atan2(cloud_in.points[0].y, cloud_in.points[0].x);
        float endOri = -std::atan2(cloud_in.points[cloudSize - 1].y,
                                   cloud_in.points[cloudSize - 1].x) +
                       2 * M_PI;

        if (endOri - startOri > 3 * M_PI)
        {
            endOri -= 2 * M_PI;
        }
        else if (endOri - startOri < M_PI)
        {
            endOri += 2 * M_PI;
        }

        bool halfPassed = false;
        int count = cloudSize;
        PointXYZIRT point;
        for (int i = 0; i < cloudSize; i++)
        {
            // xyz
            point = cloud_in.points[i];

            // intensity
            // ring
            float angle = std::atan(point.z / std::sqrt(point.x * point.x + point.y * point.y)) * 180 / M_PI;
            int scanID = 0;

            if (scan_number_ == 16)
            {
                scanID = int((angle + 15) / 2 + 0.5);
                if (scanID > (scan_number_ - 1) || scanID < 0)
                {
                    count--;
                    continue;
                }
            }
            else if (scan_number_ == 32)
            {
                scanID = int((angle + 92.0 / 3.0) * 3.0 / 4.0);
                if (scanID > (scan_number_ - 1) || scanID < 0)
                {
                    count--;
                    continue;
                }
            }
            else if (scan_number_ == 64)
            {
                if (angle >= -8.83)
                    scanID = int((2 - angle) * 3.0 + 0.5);
                else
                    scanID = scan_number_ / 2 + int((-8.83 - angle) * 2.0 + 0.5);

                // use [0 50]  > 50 remove outlies
                if (angle > 2 || angle < -24.33 || scanID > 50 || scanID < 0)
                {
                    count--;
                    continue;
                }
            }
            else
            {
                LOG(ERROR) << "wrong scan number";
                return false;
            }
            point.ring = scanID;

            // time
            if (type != CloudPointType::XYZT)
            {
                float ori = -std::atan2(point.y, point.x);
                if (!halfPassed)
                {
                    if (ori < startOri - M_PI / 2)
                    {
                        ori += 2 * M_PI;
                    }
                    else if (ori > startOri + M_PI * 3 / 2)
                    {
                        ori -= 2 * M_PI;
                    }

                    if (ori - startOri > M_PI)
                    {
                        halfPassed = true;
                    }
                }
                else
                {
                    ori += 2 * M_PI;
                    if (ori < endOri - M_PI * 3 / 2)
                    {
                        ori += 2 * M_PI;
                    }
                    else if (ori > endOri + M_PI / 2)
                    {
                        ori -= 2 * M_PI;
                    }
                }

                float relTime = (ori - endOri) / (endOri - startOri);
                point.time = scan_period_ * relTime;
            }

            cloud_out.push_back(point);
        }
    }

    void CloudImageProjection::DeskewPoint(const PointXYZIRT &pi, pcl::PointXYZI &po, const BState &bstate)
    {
        Eigen::Vector3f angle = bstate.angular_velocity * pi.time;
        Eigen::AngleAxisf t_Vz(angle(2), Eigen::Vector3f::UnitZ());
        Eigen::AngleAxisf t_Vy(angle(1), Eigen::Vector3f::UnitY());
        Eigen::AngleAxisf t_Vx(angle(0), Eigen::Vector3f::UnitX());
        Eigen::AngleAxisf t_V;
        t_V = t_Vz * t_Vy * t_Vx;
        Eigen::Vector3f point(pi.x, pi.y, pi.z);
        Eigen::Vector3f adjusted_point = t_V.matrix() * point + bstate.linear_velocity * pi.time;
        po.x = adjusted_point[0];
        po.y = adjusted_point[1];
        po.z = adjusted_point[2];
    }
}