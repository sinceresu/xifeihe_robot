/**
 * @file distance_loop.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-6-17
 * @brief distance loop
 */

#include "yd_fusion_localization/loop/distance_loop.hpp"

namespace yd_fusion_localization
{
    DistanceLoop::DistanceLoop(const YAML::Node &yaml_node)
        : Loop(yaml_node)
    {
        neglect_z_ = yaml_node["neglect_z"].as<bool>();
        search_radius_ = yaml_node["search_radius"].as<float>();
        std::vector<double> noise = yaml_node["noise"].as<std::vector<double>>();
        noise_.resize(6);
        for (int i = 0; i < 6; ++i)
        {
            noise_[i] = noise[i];
        }
        time_interval_thresh_ = yaml_node["time_interval_thresh"].as<float>();
        history_num_ = yaml_node["history_num"].as<int>();
        history_xyz_.reset(new pcl::PointCloud<PointPQIndT>());
        history_kdtree_.reset(new pcl::KdTreeFLANN<PointPQIndT>());
    }

    void DistanceLoop::AddFrame(FrameDataPtr &frame)
    {
    }
    bool DistanceLoop::Detect(FrameDataPtr &frame, LoopDataPtr &loop_data)
    {
        if (history_xyz_->empty())
            return false;
        if (loop_data == nullptr)
        {
            loop_data = std::make_shared<LoopData>();
        }
        PointPQIndT pn;
        pn.x = frame->odometry.translation().x();
        pn.y = frame->odometry.translation().y();
        pn.z = frame->odometry.translation().z();
        if (neglect_z_)
        {
            pn.z = 0;
        }
        std::vector<int> pointSearchIndLoop;
        std::vector<float> pointSearchSqDisLoop;
        history_kdtree_->radiusSearch(pn, search_radius_, pointSearchIndLoop, pointSearchSqDisLoop, 0);

        if (pointSearchIndLoop.empty())
        {
            return false;
        }
        PointPQIndT pr = history_xyz_->points[pointSearchIndLoop[0]];
        loop_data->sensor_to_robot = frame->sensor_to_robot;
        loop_data->noise = noise_;
        loop_data->stamp_prev = pr.time;
        loop_data->stamp_next = frame->stamp;
        loop_data->index_prev = pr.index;
        loop_data->index_next = frame->index;
        Sophus::SE3d sprev(Eigen::Quaterniond(pr.qw, pr.qx, pr.qy, pr.qz).normalized(), Eigen::Vector3d(pr.x, pr.y, pr.z));
        Sophus::SE3d snext(frame->odometry.unit_quaternion(), Eigen::Vector3d(pn.x, pn.y, pn.z));
        loop_data->relative_pose = sprev.inverse() * snext;
        return true;
    }
    bool DistanceLoop::Detect(const std::deque<FrameDataPtr> &history, FrameDataPtr &frame, LoopDataPtr &loop_data)
    {
        history_xyz_->clear();
        for (int i = 0; i < history.size(); ++i)
        {
            double dt = frame->stamp - history[i]->stamp;
            if (dt < time_interval_thresh_)
            {
                break;
            }
            PointPQIndT p;
            p.x = history[i]->odometry.translation().x();
            p.y = history[i]->odometry.translation().y();
            p.z = history[i]->odometry.translation().z();
            if (neglect_z_)
            {
                p.z = 0;
            }
            p.qw = history[i]->odometry.unit_quaternion().w();
            p.qx = history[i]->odometry.unit_quaternion().x();
            p.qy = history[i]->odometry.unit_quaternion().y();
            p.qz = history[i]->odometry.unit_quaternion().z();
            p.index = history[i]->index;
            p.time = history[i]->stamp;
            history_xyz_->points.emplace_back(p);
        }
        if (history_xyz_->size() < history_num_)
        {
            return false;
        }
        history_kdtree_->setInputCloud(history_xyz_);
        return Detect(frame, loop_data);
    }
}