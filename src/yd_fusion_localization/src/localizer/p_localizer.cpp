/**
 * @file p_localizer.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-3-19
 * @brief p localizer
 */

#include "yd_fusion_localization/localizer/p_localizer.hpp"

namespace yd_fusion_localization
{
    PLocalizer::PLocalizer(const YAML::Node &yaml_node) : Localizer(yaml_node)
    {
        if (yaml_node["invalid_ranges"].IsDefined())
        {
            std::vector<double> inr = yaml_node["invalid_ranges"].as<std::vector<double>>();
            for (int i = 0; i < 6; ++i)
            {
                invalid_ranges_(i) = inr[i];
            }
        }
        else
        {
            for (int i = 0; i < 6; ++i)
            {
                invalid_ranges_(i) = 0;
            }
        }
        distance_threshold_ = yaml_node["distance_threshold"].as<double>();
    }

    bool PLocalizer::CheckPosition(const Sophus::SE3d &pose)
    {
        Eigen::Vector3d p = pose.translation();
        if (p[0] > invalid_ranges_[0] && p[0] < invalid_ranges_[1] &&
            p[1] > invalid_ranges_[2] && p[1] < invalid_ranges_[3] &&
            p[2] > invalid_ranges_[4] && p[2] < invalid_ranges_[5])
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    bool PLocalizer::LoopDetection(const MeasureDataPtr &data, std::deque<Sophus::SE3d> &pose)
    {
        Eigen::Vector3d result_p;
        if (!GetPose(data, result_p))
        {
            return false;
        }
        pose.push_back(Sophus::SE3d(Eigen::Quaterniond(1, 0, 0, 0), result_p));
        return true;
    }

    bool PLocalizer::Initialize(const MeasureDataPtr &data, const MState &mstate, LocalizerDataPtr &localizer_data)
    {
        return false;
    }

    bool PLocalizer::GetPose(const MeasureDataPtr &data, const MState &guess, LocalizerDataPtr &localizer_data)
    {
        Eigen::Vector3d result_p;
        if (!GetPose(data, result_p))
        {
            return false;
        }

        if ((guess.pose.translation() - result_p).norm() > distance_threshold_)
        {
            return false;
        }

        PLocalizerDataPtr pl_data = std::dynamic_pointer_cast<PLocalizerData>(localizer_data);
        pl_data->sensor_to_robot = data->sensor_to_robot;
        pl_data->local_to_map = local_to_map_;
        pl_data->noise = data->noise;
        pl_data->stamp = data->stamp;
        pl_data->p = result_p;
        return true;
    }
    bool PLocalizer::GetPose(const MeasureDataPtr &data, LocalizerDataPtr &localizer_data)
    {
        Eigen::Vector3d result_p;
        if (!GetPose(data, result_p))
        {
            return false;
        }

        PLocalizerDataPtr pl_data = std::dynamic_pointer_cast<PLocalizerData>(localizer_data);
        pl_data->sensor_to_robot = data->sensor_to_robot;
        pl_data->local_to_map = local_to_map_;
        pl_data->noise = data->noise;
        pl_data->stamp = data->stamp;
        pl_data->p = result_p;
        return true;
    }
    bool PLocalizer::Save(const MState &mstate, const MeasureDataPtr &data)
    {
        return false;
    }
    bool PLocalizer::Save()
    {
        return false;
    }
    bool PLocalizer::GetPose(const MeasureDataPtr &data, Eigen::Vector3d &result_p)
    {
        PDataPtr p_data = std::dynamic_pointer_cast<PData>(data);
        result_p = p_data->p;
        return true;
    }
} // namespace yd_fusion_localization