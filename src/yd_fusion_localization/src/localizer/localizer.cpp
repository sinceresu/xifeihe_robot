/**
 * @file localizer.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-28
 * @brief localizer
 */

#include "yd_fusion_localization/localizer/localizer.hpp"

namespace yd_fusion_localization
{
    Localizer::Localizer(const YAML::Node &yaml_node) : local_to_map_(Sophus::SE3d())
    {
        GetTransform(yaml_node["local_to_map"], local_to_map_);
        if (yaml_node["local_frame"].IsDefined())
        {
            local_frame_ = yaml_node["local_frame"].as<std::string>();
        }
        else
        {
            local_frame_ = "";
        }
    }

}