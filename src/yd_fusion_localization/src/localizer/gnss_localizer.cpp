/**
 * @file gnss_localizer.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-26
 * @brief gnss localizer
 */

#include "yd_fusion_localization/localizer/gnss_localizer.hpp"
#include "yd_fusion_localization/global_defination/global_defination.h"

namespace yd_fusion_localization
{
    GNSSLocalizer::GNSSLocalizer(const YAML::Node &yaml_node) : Localizer(yaml_node)
    {
        if (yaml_node["height"].IsDefined())
        {
            height_ = yaml_node["height"].as<double>();
        }
        else
        {
            height_ = -10000;
        }
        if (yaml_node["invalid_ranges"].IsDefined())
        {
            std::vector<double> inr = yaml_node["invalid_ranges"].as<std::vector<double>>();
            for (int i = 0; i < 6; ++i)
            {
                invalid_ranges_(i) = inr[i];
            }
        }
        else
        {
            for (int i = 0; i < 6; ++i)
            {
                invalid_ranges_(i) = 0;
            }
        }
        distance_threshold_ = yaml_node["distance_threshold"].as<double>();

        if (yaml_node["origin"].IsDefined())
        {
            double origin_latitude = yaml_node["latitude"].as<double>();
            double origin_longitude = yaml_node["longitude"].as<double>();
            double origin_altitude = yaml_node["altitude"].as<double>();
            geo_converter_.Reset(origin_latitude, origin_longitude, origin_altitude);
            geo_inited_ = true;
        }
        else
        {
            geo_inited_ = false;
        }
    }

    bool GNSSLocalizer::CheckPosition(const Sophus::SE3d &pose)
    {
        Eigen::Vector3d p = pose.translation();
        if (p[0] > invalid_ranges_[0] && p[0] < invalid_ranges_[1] &&
            p[1] > invalid_ranges_[2] && p[1] < invalid_ranges_[3] &&
            p[2] > invalid_ranges_[4] && p[2] < invalid_ranges_[5])
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    bool GNSSLocalizer::LoopDetection(const MeasureDataPtr &data, std::deque<Sophus::SE3d> &pose)
    {
        Eigen::Vector3d result_p;
        if (!GetPose(data, result_p))
        {
            return false;
        }
        Sophus::SE3d pos;
        pos.translation() = result_p;
        Eigen::Quaterniond q(1, 0, 0, 0);
        pos.so3().setQuaternion(q);
        pose.push_back(pos);
        return true;
    }

    bool GNSSLocalizer::Initialize(const MeasureDataPtr &data, const MState &mstate, LocalizerDataPtr &localizer_data)
    {
        return false;
    }

    bool GNSSLocalizer::GetPose(const MeasureDataPtr &data, const MState &guess, LocalizerDataPtr &localizer_data)
    {
        Eigen::Vector3d result_p;
        if (!GetPose(data, result_p))
        {
            return false;
        }

        if ((guess.pose.translation() - result_p).norm() > distance_threshold_)
        {
            return false;
        }
        PLocalizerDataPtr pl_data = std::dynamic_pointer_cast<PLocalizerData>(localizer_data);

        pl_data->sensor_to_robot = data->sensor_to_robot;
        pl_data->local_to_map = local_to_map_;
        pl_data->noise = data->noise;
        pl_data->stamp = data->stamp;
        pl_data->p = result_p;
        return true;
    }
    bool GNSSLocalizer::GetPose(const MeasureDataPtr &data, LocalizerDataPtr &localizer_data)
    {
        Eigen::Vector3d result_p;
        if (!GetPose(data, result_p))
        {
            return false;
        }

        PLocalizerDataPtr pl_data = std::dynamic_pointer_cast<PLocalizerData>(localizer_data);

        pl_data->sensor_to_robot = data->sensor_to_robot;
        pl_data->local_to_map = local_to_map_;
        pl_data->noise = data->noise;
        pl_data->stamp = data->stamp;
        pl_data->p = result_p;
        return true;
    }
    bool GNSSLocalizer::Save(const MState &mstate, const MeasureDataPtr &data)
    {
        return false;
    }
    bool GNSSLocalizer::Save()
    {
        return false;
    }
    bool GNSSLocalizer::GetPose(const MeasureDataPtr &data, Eigen::Vector3d &result_p)
    {
        GNSSDataPtr gnss_data = std::dynamic_pointer_cast<GNSSData>(data);
        if (!geo_inited_)
        {
            geo_inited_ = true;
            geo_converter_.Reset(gnss_data->latitude, gnss_data->longitude, gnss_data->altitude);

            LOG(INFO) << "origin lla: " << std::setprecision(12) << gnss_data->latitude << " " << gnss_data->longitude << " " << gnss_data->altitude;

            std::ofstream fout(WORK_SPACE_PATH + "/config/localization/origin_lla.yaml");
            fout << std::setprecision(12) << "latitude: " << gnss_data->latitude << std::endl
                 << "longitude: " << gnss_data->longitude << std::endl
                 << "altitude: " << gnss_data->altitude;
            fout.close();
        }

        result_p = LlaToLocal(gnss_data->latitude, gnss_data->longitude, gnss_data->altitude);
        ;
        if (height_ != -10000)
        {
            result_p = local_to_map_ * result_p;
            result_p[2] = height_;
            result_p = local_to_map_.inverse() * result_p;
        }
        return true;
    }

    Eigen::Vector3d GNSSLocalizer::LatLongAltToEcef(const double &latitude, const double &longitude,
                                                    const double &altitude)
    {
        constexpr double a = 6378137.; ///< semi-major axis, equator to center.
        constexpr double f = 1. / 298.257223563;
        constexpr double b = a * (1. - f); ///< semi-minor axis, pole to center.
        constexpr double a_squared = a * a;
        constexpr double b_squared = b * b;
        constexpr double e_squared = (a_squared - b_squared) / a_squared;
        const double sin_phi = std::sin(DegToRad(latitude));
        const double cos_phi = std::cos(DegToRad(latitude));
        const double sin_lambda = std::sin(DegToRad(longitude));
        const double cos_lambda = std::cos(DegToRad(longitude));
        const double N = a / std::sqrt(1 - e_squared * sin_phi * sin_phi);
        const double x = (N + altitude) * cos_phi * cos_lambda;
        const double y = (N + altitude) * cos_phi * sin_lambda;
        const double z = (b_squared / a_squared * N + altitude) * sin_phi;

        return Eigen::Vector3d(x, y, z);
    }

    Eigen::Vector3d GNSSLocalizer::LlaToLocal(const double &latitude, const double &longitude,
                                              const double &altitude)
    {
        double x, y, z;
        geo_converter_.Forward(latitude, longitude,
                               altitude, x, y, z);
        return Eigen::Vector3d(x, y, z);
    }

    Eigen::Vector3d GNSSLocalizer::LocalToLla(const Eigen::Vector3d &xyz)
    {
        double latitude, longitude, altitude;
        geo_converter_.Reverse(xyz[0], xyz[1], xyz[2], latitude, longitude, altitude);
        return Eigen::Vector3d(latitude, longitude, altitude);
    }
} // namespace yd_fusion_localization