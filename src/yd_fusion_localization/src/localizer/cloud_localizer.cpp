/**
 * @file cloud_localizer.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-22
 * @brief cloud localizer
 */

#include "yd_fusion_localization/localizer/cloud_localizer.hpp"
#include "yd_fusion_localization/global_defination/global_defination.h"
#include "yd_fusion_localization/models/registration/ndt_registration.hpp"
#include "yd_fusion_localization/models/registration/ndt_omp_registration.hpp"
#include "yd_fusion_localization/models/registration/http_registration.hpp"
#include "yd_fusion_localization/models/cloud_filter/voxel_filter.hpp"
#include "yd_fusion_localization/models/cloud_filter/no_filter.hpp"
#include "yd_fusion_localization/models/cloud_filter/approximate_voxel_filter.hpp"
// #include <pcl/common/transforms.h>
#include <pcl/io/pcd_io.h>
#include "yd_fusion_localization/utilities/file_manager.hpp"

namespace yd_fusion_localization
{
    void ReadState(std::ifstream &in, State &state)
    {
        in.read((char *)(&state.index), sizeof(int));
        in.read((char *)(&state.stamp), sizeof(double));
        in.read((char *)(&state.p[0]), sizeof(double));
        in.read((char *)(&state.p[1]), sizeof(double));
        in.read((char *)(&state.p[2]), sizeof(double));
        double w, x, y, z;
        in.read((char *)(&w), sizeof(double));
        in.read((char *)(&x), sizeof(double));
        in.read((char *)(&y), sizeof(double));
        in.read((char *)(&z), sizeof(double));
        state.q = Eigen::Quaterniond(w, x, y, z);
    }
    template <class Matrix>
    void ReadMatrix(std::ifstream &in, Matrix &matrix)
    {
        typename Matrix::Index rows = 0, cols = 0;
        in.read((char *)(&rows), sizeof(typename Matrix::Index));
        in.read((char *)(&cols), sizeof(typename Matrix::Index));
        matrix.resize(rows, cols);
        in.read((char *)matrix.data(), rows * cols * sizeof(typename Matrix::Scalar));
    }
    template <class datatype>
    bool ReadVector(std::ifstream &in, std::vector<datatype> &vec)
    {
        int num;
        in.read((char *)(&num), sizeof(int));
        vec.resize(num);
        in.read((char *)vec.data(), num * sizeof(datatype));
    }
    void WriteState(std::ofstream &out, const State &state)
    {
        out.write((char *)(&state.index), sizeof(int));
        out.write((char *)(&state.stamp), sizeof(double));
        out.write((char *)(&state.p[0]), sizeof(double));
        out.write((char *)(&state.p[1]), sizeof(double));
        out.write((char *)(&state.p[2]), sizeof(double));
        double w = state.q.w();
        double x = state.q.x();
        double y = state.q.y();
        double z = state.q.z();
        out.write((char *)(&w), sizeof(double));
        out.write((char *)(&x), sizeof(double));
        out.write((char *)(&y), sizeof(double));
        out.write((char *)(&z), sizeof(double));
    }
    template <class Matrix>
    void WriteMatrix(std::ofstream &out, const Matrix &matrix)
    {
        typename Matrix::Index rows = matrix.rows(), cols = matrix.cols();
        out.write((char *)(&rows), sizeof(typename Matrix::Index));
        out.write((char *)(&cols), sizeof(typename Matrix::Index));
        out.write((char *)matrix.data(), rows * cols * sizeof(typename Matrix::Scalar));
    }
    template <class datatype>
    void WriteVector(std::ofstream &out, const std::vector<datatype> &vec)
    {
        int num = vec.size();
        out.write((char *)(&num), sizeof(int));
        out.write((char *)vec.data(), num * sizeof(datatype));
    }
    CloudLocalizer::CloudLocalizer(
        const YAML::Node &yaml_node) : Localizer(yaml_node),
                                       global_map_ptr_(new pcl::PointCloud<pcl::PointXYZ>()),
                                       local_map_ptr_(new pcl::PointCloud<pcl::PointXYZ>()),
                                       current_scan_ptr_(new pcl::PointCloud<pcl::PointXYZ>())
    {
        InitWithConfig(yaml_node);
    }

    bool CloudLocalizer::CheckPosition(const Sophus::SE3d &pose)
    {
        Eigen::Vector3d p = pose.translation();
        if (p[0] > invalid_ranges_[0] && p[0] < invalid_ranges_[1] &&
            p[1] > invalid_ranges_[2] && p[1] < invalid_ranges_[3] &&
            p[2] > invalid_ranges_[4] && p[2] < invalid_ranges_[5])
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    bool CloudLocalizer::LoopDetection(const MeasureDataPtr &data, std::deque<Sophus::SE3d> &pose)
    {
        if (!sc_loop_ || sc_state_.empty())
        {
            return false;
        }
        CloudDataPtr cloud_data = std::dynamic_pointer_cast<CloudData>(data);
        std::vector<int> indices;
        if (cloud_data->point_type == CloudPointType::XYZ)
        {
            pcl::removeNaNFromPointCloud(*(cloud_data->xyz_ptr), *(cloud_data->xyz_ptr), indices);
            RemovePointCloud(*(cloud_data->xyz_ptr), *(cloud_data->xyz_ptr), frame_distance_threshold_[0], frame_distance_threshold_[1]);
        }
        else if (cloud_data->point_type == CloudPointType::XYZT)
        {
            pcl::removeNaNFromPointCloud(*(cloud_data->xyzt_ptr), *(cloud_data->xyzt_ptr), indices);
            RemovePointCloud(*(cloud_data->xyzt_ptr), *(cloud_data->xyzt_ptr), frame_distance_threshold_[0], frame_distance_threshold_[1]);
            cloud_data->xyz_ptr = boost::make_shared<pcl::PointCloud<pcl::PointXYZ>>();
            Undistort(*(cloud_data->xyzt_ptr), *(cloud_data->xyz_ptr), Eigen::Vector3f::Zero(), Eigen::Vector3f::Zero());
        }
        else
        {
            return false;
        }
        std::pair<int, float> index = scmanager_->relocalize(*(cloud_data->xyz_ptr));
        if (index.first == -1)
        {
            return false;
        }
        std::vector<int> inds;
        for (int i = 0; i <= loop_key_size_ + 2; ++i)
        {
            if (i == 0)
            {
                inds.emplace_back(index.first);
                if (inds.size() >= loop_key_size_)
                {
                    break;
                }
                continue;
            }
            int ind = index.first - i;
            if (ind >= 0)
            {
                if ((sc_state_[ind].p - sc_state_[index.first].p).norm() < loop_key_size_ * sc_dis_thresh_ && std::acos(std::fabs((sc_state_[ind].q.inverse() * sc_state_[index.first].q).w())) * 2.0 < loop_key_size_ * sc_angle_thresh_)
                {
                    inds.emplace_back(ind);
                }
            }
            ind = index.first + i;
            if (ind < sc_state_.size())
            {
                if ((sc_state_[ind].p - sc_state_[index.first].p).norm() < loop_key_size_ * sc_dis_thresh_ && std::acos(std::fabs((sc_state_[ind].q.inverse() * sc_state_[index.first].q).w())) * 2.0 < loop_key_size_ * sc_angle_thresh_)
                {
                    inds.emplace_back(ind);
                }
            }
            if (inds.size() >= loop_key_size_)
            {
                break;
            }
        }
        if (inds.size() < loop_key_size_ - 2)
        {
            for (int i = 0; i < sc_state_.size(); ++i)
            {
                if (std::abs(i - index.first) <= loop_key_size_)
                {
                    continue;
                }
                if ((sc_state_[i].p - sc_state_[index.first].p).norm() < loop_key_size_ * sc_dis_thresh_ && std::acos(std::fabs((sc_state_[i].q.inverse() * sc_state_[index.first].q).w())) * 2.0 < loop_key_size_ * sc_angle_thresh_)
                {
                    inds.emplace_back(i);
                }
                if (inds.size() >= loop_key_size_)
                {
                    break;
                }
            }
        }
        pcl::PointCloud<pcl::PointXYZ>::Ptr local_cloud_ptr = boost::make_shared<pcl::PointCloud<pcl::PointXYZ>>();
        for (int i = 0; i < inds.size(); ++i)
        {
            std::string file_path;
            if (i < old_key_size_)
            {
                file_path = sc_loop_path_ + "/pointcloud_" + std::to_string(sc_state_[inds[i]].index) + ".pcd";
            }
            else
            {
                file_path = sc_save_path_ + "/pointcloud_" + std::to_string(sc_state_[inds[i]].index) + ".pcd";
            }
            pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_ptr = boost::make_shared<pcl::PointCloud<pcl::PointXYZ>>();
            pcl::io::loadPCDFile(file_path, *cloud_ptr);
            Eigen::Matrix4f cloud_pose = (Sophus::SE3d(sc_state_[inds[i]].q, sc_state_[inds[i]].p)).matrix().cast<float>();
            pcl::transformPointCloud(*cloud_ptr, *cloud_ptr, cloud_pose);
            *local_cloud_ptr += *cloud_ptr;
        }

        sc_registration_ptr_->SetInputTarget(local_cloud_ptr);
        Sophus::SE3d gpose(sc_state_[index.first].q * Eigen::Quaterniond(Eigen::AngleAxisd(index.second, Eigen::Vector3d::UnitZ())), sc_state_[index.first].p);
        Eigen::Matrix4f guess_pose = gpose.matrix().cast<float>();
        // 匹配
        Eigen::Matrix4f match_pose = Eigen::Matrix4f::Identity();
        pcl::PointCloud<pcl::PointXYZ>::Ptr result_cloud_ptr = boost::make_shared<pcl::PointCloud<pcl::PointXYZ>>();
        sc_registration_ptr_->ScanMatch(-1.0, cloud_data->xyz_ptr, result_cloud_ptr, guess_pose, match_pose);
        Sophus::SE3d result_pose;
        TransformDataType(match_pose.cast<double>(), result_pose);
        // 判断是否有效
        double dp = (result_pose.translation() - gpose.translation()).norm();
        double dtheta = std::acos(std::fabs((gpose.unit_quaternion().inverse() * result_pose.unit_quaternion()).w())) * 2.0;
        if (sc_registration_ptr_->GetFitnessScore() > sc_match_threshold_ || dp > sc_match_distance_threshold_ || dtheta > sc_match_angle_threshold_)
        {
            std::cout << "LoopDetection Fail! key frame size: " << inds.size()
                      << ", fitness score: " << sc_registration_ptr_->GetFitnessScore() << ", dp: " << dp << ", dtheta: " << dtheta << std::endl;
            return false;
        }
        else
        {
            pose.emplace_back(result_pose);
            std::cout << "LoopDetection Succeed! key frame size: " << inds.size()
                      << ", fitness score: " << sc_registration_ptr_->GetFitnessScore() << ", dp: " << dp << ", dtheta: " << dtheta << std::endl;
            return true;
        }
    }

    bool CloudLocalizer::Initialize(const MeasureDataPtr &data,
                                    const MState &mstate,
                                    LocalizerDataPtr &localizer_data)
    {
        MState gmstate(mstate);
        Sophus::SE3d result_pose;
        int count = 0;
        float match_score_prev = init_match_threshold_ * 10;
        while (GetPose(data, gmstate, result_pose) && count < 5)
        {
            if (match_score_ / match_score_prev < 0.9)
            {
                match_score_prev = match_score_;
                gmstate.pose = result_pose;
                count++;
            }
            else
            {
                break;
            }
        }
        if (match_score_ < init_match_threshold_)
        {
            PoseLocalizerDataPtr posel_data = std::dynamic_pointer_cast<PoseLocalizerData>(localizer_data);
            posel_data->sensor_to_robot = data->sensor_to_robot;
            posel_data->local_to_map = local_to_map_;
            posel_data->noise = data->noise;
            posel_data->stamp = data->stamp;
            posel_data->pose = result_pose;
            return true;
        }
        return false;
    }

    bool CloudLocalizer::GetPose(const MeasureDataPtr &data,
                                 const MState &guess,
                                 LocalizerDataPtr &localizer_data)
    {
        Sophus::SE3d result_pose;
        if (!GetPose(data, guess, result_pose))
        {
            return false;
        }

        if (match_score_ > match_threshold_ || match_score_ <= 1e-9)
        {
            return false;
        }
        if ((guess.pose.translation() - result_pose.translation()).norm() > distance_threshold_ || std::acos(std::fabs((guess.pose.unit_quaternion().inverse() * result_pose.unit_quaternion()).w())) * 2 > angle_threshold_)
        {
            LOG(WARNING) << "CloudLocalizer::GetPose threshold, p: " << guess.pose.translation().transpose() << " vs " << result_pose.translation().transpose() << "; q: " << guess.pose.unit_quaternion().coeffs().transpose() << " vs " << result_pose.unit_quaternion().coeffs().transpose();
            return false;
        }

        PoseLocalizerDataPtr posel_data = std::dynamic_pointer_cast<PoseLocalizerData>(localizer_data);
        posel_data->sensor_to_robot = data->sensor_to_robot;
        posel_data->local_to_map = local_to_map_;
        posel_data->noise = data->noise;
        posel_data->stamp = data->stamp;
        posel_data->pose = result_pose;
        return true;
    }
    bool CloudLocalizer::Save(const MState &mstate, const MeasureDataPtr &data)
    {
        if (sc_save_)
        {
            int index = 0;
            if (sc_replace_old_ == 0 || sc_replace_old_ == 2)
            {
                index = old_key_size_;
            }
            auto iter = std::find_if(sc_state_.begin() + index, sc_state_.end(), [&](const State &it) -> bool { return (it.p - mstate.pose.translation()).norm() < sc_dis_thresh_ && std::acos(std::fabs((it.q.inverse() * mstate.pose.unit_quaternion()).w())) * 2.0 < sc_angle_thresh_; });
            if (iter != sc_state_.end())
            {
                return false;
            }
            CloudDataPtr cloud_data = std::dynamic_pointer_cast<CloudData>(data);
            std::vector<int> indices;
            if (cloud_data->point_type == CloudPointType::XYZ)
            {
                pcl::removeNaNFromPointCloud(*(cloud_data->xyz_ptr), *(cloud_data->xyz_ptr), indices);
                RemovePointCloud(*(cloud_data->xyz_ptr), *(cloud_data->xyz_ptr), frame_distance_threshold_[0], frame_distance_threshold_[1]);
                if (undistort_)
                {
                    cloud_data->xyzt_ptr = boost::make_shared<pcl::PointCloud<PointXYZT>>();
                    AddTimeFieldtoPointCloud(*(cloud_data->xyz_ptr), *(cloud_data->xyzt_ptr));
                    Undistort(*(cloud_data->xyzt_ptr), *(cloud_data->xyz_ptr), mstate.linear_velocity.cast<float>(), mstate.angular_velocity.cast<float>());
                }
            }
            else if (cloud_data->point_type == CloudPointType::XYZT)
            {
                pcl::removeNaNFromPointCloud(*(cloud_data->xyzt_ptr), *(cloud_data->xyzt_ptr), indices);
                RemovePointCloud(*(cloud_data->xyzt_ptr), *(cloud_data->xyzt_ptr), frame_distance_threshold_[0], frame_distance_threshold_[1]);
                cloud_data->xyz_ptr = boost::make_shared<pcl::PointCloud<pcl::PointXYZ>>();
                Undistort(*(cloud_data->xyzt_ptr), *(cloud_data->xyz_ptr), mstate.linear_velocity.cast<float>(), mstate.angular_velocity.cast<float>());
            }
            else
            {
                return false;
            }
            if (sc_replace_old_ == 0)
            {
                for (int i = 0; i < old_key_size_; ++i)
                {
                    if ((sc_state_[i].p - mstate.pose.translation()).norm() < sc_dis_thresh_ && std::acos(std::fabs((sc_state_[i].q.inverse() * mstate.pose.unit_quaternion()).w())) * 2.0 < sc_angle_thresh_)
                    {
                        if (std::find(sc_old_to_delete_.begin(), sc_old_to_delete_.end(), i) == sc_old_to_delete_.end())
                        {
                            sc_old_to_delete_.emplace_back(i);
                        }
                    }
                }
                DeleteOldKeys();
            }
            State save_state;
            if (sc_state_.empty())
            {
                save_state.index = 0;
            }
            else
            {
                save_state.index = sc_state_.back().index + 1;
            }
            save_state.stamp = mstate.stamp;
            save_state.p = mstate.pose.translation();
            save_state.q = mstate.pose.unit_quaternion();
            pcl::PointCloud<pcl::PointXYZ>::Ptr filtered = boost::make_shared<pcl::PointCloud<pcl::PointXYZ>>();
            frame_filter_ptr_->Filter(cloud_data->xyz_ptr, filtered);
            std::string file_path = sc_save_path_ + "/pointcloud_" + std::to_string(save_state.index) + ".pcd";
            pcl::io::savePCDFileBinary(file_path, *filtered);
            sc_state_.push_back(save_state);
            scmanager_->makeAndSaveScancontextAndKeys(*filtered);
            return true;
        }
        return false;
    }
    bool CloudLocalizer::Save()
    {
        if (!sc_save_)
        {
            return false;
        }
        std::ofstream out(sc_save_path_ + "/scancontext", std::ios::out | std::ios::binary | std::ios::trunc);
        if (!out.good())
        {
            return false;
        }
        pcl::PointCloud<pcl::PointXYZ>::Ptr map_ptr = boost::make_shared<pcl::PointCloud<pcl::PointXYZ>>();
        int num = sc_state_.size();
        out.write((char *)(&num), sizeof(int));
        for (int i = 0; i < sc_state_.size(); ++i)
        {
            assert(sc_state_[i].index >= i);
            std::string file_path;
            if (i < old_key_size_)
            {
                file_path = sc_loop_path_ + "/pointcloud_" + std::to_string(sc_state_[i].index) + ".pcd";
            }
            else
            {
                file_path = sc_save_path_ + "/pointcloud_" + std::to_string(sc_state_[i].index) + ".pcd";
            }
            pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_ptr = boost::make_shared<pcl::PointCloud<pcl::PointXYZ>>();
            pcl::io::loadPCDFile(file_path, *cloud_ptr);
            if (sc_save_path_ == sc_loop_path_)
            {
                boost::filesystem::remove(file_path);
            }
            file_path = sc_save_path_ + "/pointcloud_" + std::to_string(i) + ".pcd";
            pcl::io::savePCDFileBinary(file_path, *cloud_ptr);
            sc_state_[i].index = i;
            WriteState(out, sc_state_[i]);
            WriteMatrix(out, scmanager_->polarcontexts_[i]);
            WriteMatrix(out, scmanager_->polarcontext_invkeys_[i]);
            WriteMatrix(out, scmanager_->polarcontext_vkeys_[i]);
            WriteVector(out, scmanager_->polarcontext_invkeys_mat_[i]);
            Eigen::Matrix4f cloud_pose = (Sophus::SE3d(sc_state_[i].q, sc_state_[i].p)).matrix().cast<float>();
            pcl::transformPointCloud(*cloud_ptr, *cloud_ptr, cloud_pose);
            *map_ptr += *cloud_ptr;
        }
        out.close();
        global_map_filter_ptr_->Filter(map_ptr, map_ptr);
        pcl::io::savePCDFileBinary(sc_save_path_ + "/map.pcd", *map_ptr);
        LOG(INFO) << "map save true!";
        return true;
    }
    pcl::PointCloud<pcl::PointXYZ>::Ptr &CloudLocalizer::GetGlobalMap()
    {
        return global_map_ptr_;
    }
    pcl::PointCloud<pcl::PointXYZ>::Ptr &CloudLocalizer::GetLocalMap()
    {
        return local_map_ptr_;
    }
    pcl::PointCloud<pcl::PointXYZ>::Ptr &CloudLocalizer::GetCurrentScan()
    {
        return current_scan_ptr_;
    }

    bool CloudLocalizer::GetPose(const MeasureDataPtr &data, const MState &guess, Sophus::SE3d &result_pose)
    {
        CloudDataPtr cloud_data = std::dynamic_pointer_cast<CloudData>(data);
        Eigen::Matrix4f guess_pose(guess.pose.matrix().cast<float>());
        // 匹配之前判断是否需要更新局部地图
        if (local_map_use_box_)
        {
            std::vector<float> edge = std::dynamic_pointer_cast<BoxFilter<pcl::PointXYZ>>(local_map_filter_ptr_)->GetEdge();
            for (int i = 0; i < 3; i++)
            {
                if (guess_pose(i, 3) - edge.at(2 * i) > local_map_edge_distance_[i] &&
                    edge.at(2 * i + 1) - guess_pose(i, 3) > local_map_edge_distance_[i])
                    continue;
                ResetLocalMap(guess_pose(0, 3), guess_pose(1, 3), guess_pose(2, 3));
                registration_ptr_->SetInputTarget(local_map_ptr_);
                break;
            }
        }

        std::vector<int> indices;
        if (cloud_data->point_type == CloudPointType::XYZ)
        {
            pcl::removeNaNFromPointCloud(*(cloud_data->xyz_ptr), *(cloud_data->xyz_ptr), indices);
            RemovePointCloud(*(cloud_data->xyz_ptr), *(cloud_data->xyz_ptr), frame_distance_threshold_[0], frame_distance_threshold_[1]);
            if (undistort_)
            {
                cloud_data->xyzt_ptr = boost::make_shared<pcl::PointCloud<PointXYZT>>();
                AddTimeFieldtoPointCloud(*(cloud_data->xyz_ptr), *(cloud_data->xyzt_ptr));
                Undistort(*(cloud_data->xyzt_ptr), *(cloud_data->xyz_ptr), guess.linear_velocity.cast<float>(), guess.angular_velocity.cast<float>());
            }
        }
        else if (cloud_data->point_type == CloudPointType::XYZT)
        {
            pcl::removeNaNFromPointCloud(*(cloud_data->xyzt_ptr), *(cloud_data->xyzt_ptr), indices);
            RemovePointCloud(*(cloud_data->xyzt_ptr), *(cloud_data->xyzt_ptr), frame_distance_threshold_[0], frame_distance_threshold_[1]);
            cloud_data->xyz_ptr = boost::make_shared<pcl::PointCloud<pcl::PointXYZ>>();
            Undistort(*(cloud_data->xyzt_ptr), *(cloud_data->xyz_ptr), guess.linear_velocity.cast<float>(), guess.angular_velocity.cast<float>());
        }
        else
        {
            return false;
        }
        current_scan_ptr_.reset(new pcl::PointCloud<pcl::PointXYZ>());
        frame_filter_ptr_->Filter(cloud_data->xyz_ptr, current_scan_ptr_);

        // 与地图匹配
        Eigen::Matrix4f cloud_pose;
        pcl::PointCloud<pcl::PointXYZ>::Ptr result_cloud_ptr(new pcl::PointCloud<pcl::PointXYZ>());
        registration_ptr_->ScanMatch(cloud_data->stamp, current_scan_ptr_, result_cloud_ptr, guess_pose, cloud_pose);
        TransformDataType(cloud_pose.cast<double>(), result_pose);
        match_score_ = registration_ptr_->GetFitnessScore();
        return true;
    }

    bool CloudLocalizer::GetPose(const MeasureDataPtr &data, LocalizerDataPtr &localizer_data)
    {
        return false;
    }

    bool CloudLocalizer::InitWithConfig(const YAML::Node &yaml_node)
    {
        std::cout << "-----------------地图定位初始化-------------------" << std::endl;
        InitGlobalMap(yaml_node["global_map"]);
        InitLocalMap(yaml_node["local_map"]);
        InitFrame(yaml_node["frame"]);
        InitRegistration(yaml_node["registration"]);
        InitSC(yaml_node["scan_context"]);
        return true;
    }
    bool CloudLocalizer::InitGlobalMap(const YAML::Node &yaml_node)
    {
        InitDataPath(yaml_node["map_path"]);
        InitFilter("global_map", global_map_filter_ptr_, yaml_node["filter"]);

        pcl::io::loadPCDFile(map_path_, *global_map_ptr_);
        LOG(INFO) << "load global map size: " << global_map_ptr_->points.size();
        global_map_filter_ptr_->Filter(global_map_ptr_, global_map_ptr_);
        LOG(INFO) << "filtered global map size: " << global_map_ptr_->points.size();
    }
    bool CloudLocalizer::InitLocalMap(const YAML::Node &yaml_node)
    {
        if (yaml_node["use_box"].IsDefined())
        {
            local_map_use_box_ = yaml_node["use_box"].as<bool>();
        }
        else
        {
            local_map_use_box_ = false;
        }
        if (local_map_use_box_)
        {
            local_map_filter_ptr_ = std::make_shared<BoxFilter<pcl::PointXYZ>>(yaml_node["box_filter"]);
            local_map_edge_distance_.resize(3);
            for (size_t i = 0; i < local_map_edge_distance_.size(); i++)
            {
                local_map_edge_distance_.at(i) = yaml_node["edge_distance"][i].as<float>();
            }
            double x, y, z;
            x = yaml_node["box_filter"]["origin"][0].as<float>();
            y = yaml_node["box_filter"]["origin"][1].as<float>();
            z = yaml_node["box_filter"]["origin"][2].as<float>();
            ResetLocalMap(x, y, z);
        }
        else
        {
            local_map_filter_ptr_ = nullptr;
            local_map_ptr_ = global_map_ptr_;
            // local_map_ptr_.reset(new pcl::PointCloud<pcl::PointXYZ>(*global_map_ptr_));
        }
    }
    bool CloudLocalizer::InitFrame(const YAML::Node &yaml_node)
    {
        InitFilter("frame", frame_filter_ptr_, yaml_node["filter"]);
        frame_distance_threshold_.resize(2);
        for (size_t i = 0; i < frame_distance_threshold_.size(); i++)
        {
            frame_distance_threshold_.at(i) = yaml_node["distance_threshold"][i].as<float>();
        }
        if (yaml_node["scan_number"].IsDefined())
        {
            scan_number_ = yaml_node["scan_number"].as<int>();
        }
        else
        {
            scan_number_ = 16;
        }
        if (yaml_node["scan_period"].IsDefined())
        {
            scan_period_ = yaml_node["scan_period"].as<double>();
        }
        else
        {
            scan_period_ = 0.1;
        }
    }
    bool CloudLocalizer::InitRegistration(const YAML::Node &yaml_node)
    {
        std::string registration_method = yaml_node["method"].as<std::string>();
        std::cout << "地图匹配选择的点云匹配方式为：" << registration_method << std::endl;
        if (registration_method == "NDT")
        {
            registration_ptr_ = std::make_shared<NDTRegistration<pcl::PointXYZ>>(yaml_node);
        }
        else if (registration_method == "NDTOMP")
        {
            registration_ptr_ = std::make_shared<NDTOMPRegistration<pcl::PointXYZ>>(yaml_node);
        }
        else if (registration_method == "HTTP")
        {
            registration_ptr_ = std::make_shared<HttpRegistration<pcl::PointXYZ>>(yaml_node);
        }
        else
        {
            LOG(ERROR) << "没找到与 " << registration_method << " 相对应的点云匹配方式!";
            return false;
        }
        registration_ptr_->SetInputTarget(local_map_ptr_);
        match_threshold_ = yaml_node["match_threshold"].as<float>();
        std::cout << "匹配阈值为：" << match_threshold_ << std::endl;
        init_match_threshold_ = yaml_node["init_match_threshold"].as<float>();
        if (yaml_node["invalid_ranges"].IsDefined())
        {
            std::vector<double> inr = yaml_node["invalid_ranges"].as<std::vector<double>>();
            for (int i = 0; i < 6; ++i)
            {
                invalid_ranges_(i) = inr[i];
            }
        }
        else
        {
            for (int i = 0; i < 6; ++i)
            {
                invalid_ranges_(i) = 0;
            }
        }
        distance_threshold_ = yaml_node["distance_threshold"].as<double>();
        angle_threshold_ = yaml_node["angle_threshold"].as<double>();
        if (yaml_node["undistort"].IsDefined())
        {
            undistort_ = yaml_node["undistort"].as<bool>();
        }
        else
        {
            undistort_ = false;
        }
        return true;
    }
    bool CloudLocalizer::InitDataPath(const YAML::Node &yaml_node)
    {
        map_path_ =  std::string(std::getenv("HOME"))  + "/" + yaml_node.as<std::string>();
        return true;
    }
    bool CloudLocalizer::InitFilter(std::string filter_user, std::shared_ptr<CloudFilter<pcl::PointXYZ>> &filter_ptr, const YAML::Node &yaml_node)
    {
        std::string filter_method = yaml_node["method"].as<std::string>();
        std::cout << filter_user << "选择的滤波方法为：" << filter_method << std::endl;

        if (filter_method == "voxel_filter")
        {
            filter_ptr = std::make_shared<VoxelFilter<pcl::PointXYZ>>(yaml_node);
        }
        else if (filter_method == "no_filter")
        {
            filter_ptr = std::make_shared<NoFilter<pcl::PointXYZ>>();
        }
        else if (filter_method == "approximate_voxel_filter")
        {
            filter_ptr = std::make_shared<ApproximateVoxelFilter<pcl::PointXYZ>>(yaml_node);
        }
        else
        {
            LOG(ERROR) << "没有为 " << filter_user << " 找到与 " << filter_method << " 相对应的滤波方法!";
            return false;
        }

        return true;
    }
    bool CloudLocalizer::InitSC(const YAML::Node &yaml_node)
    {
        sc_loop_ = yaml_node["sc_loop"].as<bool>();
        sc_save_ = yaml_node["sc_save"].as<bool>();
        sc_replace_old_ = yaml_node["sc_replace_old"].as<int>();
        sc_dis_thresh_ = yaml_node["sc_dis_thresh"].as<double>();
        sc_angle_thresh_ = yaml_node["sc_angle_thresh"].as<double>();
        loop_key_size_ = yaml_node["loop_key_size"].as<int>();
        InitSCRegistration(yaml_node["sc_registration"]);
        sc_loop_path_ = WORK_SPACE_PATH + "/data/localization/" + yaml_node["sc_loop_path"].as<std::string>();
        if (!FileManager::CreateDirectory(sc_loop_path_))
        {
            sc_loop_ = false;
            sc_save_ = false;
            return false;
        }
        sc_save_path_ = WORK_SPACE_PATH + "/data/localization/" + yaml_node["sc_save_path"].as<std::string>();
        if (!FileManager::CreateDirectory(sc_save_path_))
        {
            sc_loop_ = false;
            sc_save_ = false;
            return false;
        }
        ReadKeys(yaml_node["params"]);
        return true;
    }
    bool CloudLocalizer::InitSCRegistration(const YAML::Node &yaml_node)
    {
        std::string registration_method = yaml_node["method"].as<std::string>();
        std::cout << "InitSCRegistration: " << registration_method << std::endl;
        if (registration_method == "NDT")
        {
            sc_registration_ptr_ = std::make_shared<NDTRegistration<pcl::PointXYZ>>(yaml_node);
        }
        else if (registration_method == "NDTOMP")
        {
            sc_registration_ptr_ = std::make_shared<NDTOMPRegistration<pcl::PointXYZ>>(yaml_node);
        }
        else if (registration_method == "HTTP")
        {
            sc_registration_ptr_ = std::make_shared<HttpRegistration<pcl::PointXYZ>>(yaml_node);
        }
        else
        {
            LOG(ERROR) << "没找到与 " << registration_method << " 相对应的点云匹配方式!";
            return false;
        }
        sc_match_threshold_ = yaml_node["match_threshold"].as<float>();
        std::cout << "sc_match_threshold: " << sc_match_threshold_ << std::endl;
        sc_match_distance_threshold_ = yaml_node["match_distance_threshold"].as<double>();
        sc_match_angle_threshold_ = yaml_node["match_angle_threshold"].as<double>();
        return true;
    }
    bool CloudLocalizer::ResetLocalMap(float x, float y, float z)
    {
        std::vector<float> origin = {x, y, z};
        std::dynamic_pointer_cast<BoxFilter<pcl::PointXYZ>>(local_map_filter_ptr_)->SetOrigin(origin);
        std::dynamic_pointer_cast<BoxFilter<pcl::PointXYZ>>(local_map_filter_ptr_)->Filter(global_map_ptr_, local_map_ptr_);

        std::vector<float> edge = std::dynamic_pointer_cast<BoxFilter<pcl::PointXYZ>>(local_map_filter_ptr_)->GetEdge();
        LOG(INFO) << "new local map:" << edge.at(0) << ","
                  << edge.at(1) << ","
                  << edge.at(2) << ","
                  << edge.at(3) << ","
                  << edge.at(4) << ","
                  << edge.at(5) << std::endl
                  << std::endl;

        return true;
    }
    template <typename PointT>
    bool CloudLocalizer::RemovePointCloud(const pcl::PointCloud<PointT> &cloud_in,
                                          pcl::PointCloud<PointT> &cloud_out, float lower_bound, float upper_bound)
    {
        if (&cloud_in != &cloud_out)
        {
            cloud_out.header = cloud_in.header;
            cloud_out.points.resize(cloud_in.points.size());
        }

        size_t j = 0;
        float lb2 = lower_bound * lower_bound;
        float ub2 = upper_bound * upper_bound;
        for (size_t i = 0; i < cloud_in.points.size(); ++i)
        {
            float dis = cloud_in.points[i].x * cloud_in.points[i].x + cloud_in.points[i].y * cloud_in.points[i].y + cloud_in.points[i].z * cloud_in.points[i].z;
            if (dis < lb2 || dis > ub2)
                continue;
            cloud_out.points[j] = cloud_in.points[i];
            j++;
        }
        if (j != cloud_in.points.size())
        {
            cloud_out.points.resize(j);
        }

        cloud_out.height = 1;
        cloud_out.width = static_cast<uint32_t>(j);
        cloud_out.is_dense = true;
    }
    // template bool CloudLocalizer::RemovePointCloud<pcl::PointXYZ>(const pcl::PointCloud<pcl::PointXYZ> &cloud_in, cl::PointCloud<pcl::PointXYZ> &cloud_out, float lower_bound, float upper_bound);
    // template bool CloudLocalizer::RemovePointCloud<PointXYZT>(const pcl::PointCloud<PointXYZT> &cloud_in, cl::PointCloud<PointXYZT> &cloud_out, float lower_bound, float upper_bound);

    void CloudLocalizer::AddTimeFieldtoPointCloud(const pcl::PointCloud<pcl::PointXYZ> &cloud_in, pcl::PointCloud<PointXYZT> &cloud_out)
    {
        cloud_out.clear();
        int cloudSize = cloud_in.points.size();
        if (cloudSize == 0)
        {
            LOG(ERROR) << "cloud is empty !!!";
            return;
        }
        float startOri = -std::atan2(cloud_in.points[0].y, cloud_in.points[0].x);
        float endOri = -std::atan2(cloud_in.points[cloudSize - 1].y,
                                   cloud_in.points[cloudSize - 1].x) +
                       2 * M_PI;

        if (endOri - startOri > 3 * M_PI)
        {
            endOri -= 2 * M_PI;
        }
        else if (endOri - startOri < M_PI)
        {
            endOri += 2 * M_PI;
        }

        bool halfPassed = false;
        int count = cloudSize;
        PointXYZT point;
        // std::vector<pcl::PointCloud<PointXYZT>> laserCloudScans(scan_number_);
        for (int i = 0; i < cloudSize; i++)
        {
            point.x = cloud_in.points[i].x;
            point.y = cloud_in.points[i].y;
            point.z = cloud_in.points[i].z;

            float angle = std::atan(point.z / sqrt(point.x * point.x + point.y * point.y)) * 180 / M_PI;
            int scanID = 0;

            if (scan_number_ == 16)
            {
                scanID = int((angle + 15) / 2 + 0.5);
                if (scanID > (scan_number_ - 1) || scanID < 0)
                {
                    count--;
                    continue;
                }
            }
            else if (scan_number_ == 32)
            {
                scanID = int((angle + 92.0 / 3.0) * 3.0 / 4.0);
                if (scanID > (scan_number_ - 1) || scanID < 0)
                {
                    count--;
                    continue;
                }
            }
            else if (scan_number_ == 64)
            {
                if (angle >= -8.83)
                    scanID = int((2 - angle) * 3.0 + 0.5);
                else
                    scanID = scan_number_ / 2 + int((-8.83 - angle) * 2.0 + 0.5);

                // use [0 50]  > 50 remove outlies
                if (angle > 2 || angle < -24.33 || scanID > 50 || scanID < 0)
                {
                    count--;
                    continue;
                }
            }
            else
            {
                LOG(ERROR) << "wrong scan number";
                return;
            }

            float ori = -std::atan2(point.y, point.x);
            if (!halfPassed)
            {
                if (ori < startOri - M_PI / 2)
                {
                    ori += 2 * M_PI;
                }
                else if (ori > startOri + M_PI * 3 / 2)
                {
                    ori -= 2 * M_PI;
                }

                if (ori - startOri > M_PI)
                {
                    halfPassed = true;
                }
            }
            else
            {
                ori += 2 * M_PI;
                if (ori < endOri - M_PI * 3 / 2)
                {
                    ori += 2 * M_PI;
                }
                else if (ori > endOri + M_PI / 2)
                {
                    ori -= 2 * M_PI;
                }
            }

            float relTime = (ori - endOri) / (endOri - startOri);
            point.time = scan_period_ * relTime;
            // laserCloudScans[scanID].push_back(point);
            cloud_out.push_back(point);
        }

        // for (int i = 0; i < scan_number_; i++)
        // {
        //     cloud_out += laserCloudScans[i];
        // }
    }

    void CloudLocalizer::Undistort(const pcl::PointCloud<PointXYZT> &cloud_in,
                                   pcl::PointCloud<pcl::PointXYZ> &cloud_out,
                                   const Eigen::Vector3f &linear_velocity,
                                   const Eigen::Vector3f &angular_velocity)
    {
        cloud_out.resize(cloud_in.size());
        for (int i = 0; i < cloud_in.points.size(); i++)
        {
            Undistort(&cloud_in.points[i], &cloud_out.points[i], linear_velocity, angular_velocity);
        }
    }

    void CloudLocalizer::Undistort(PointXYZT const *const pi, pcl::PointXYZ *const po, const Eigen::Vector3f &linear_velocity, const Eigen::Vector3f &angular_velocity)
    {
        Eigen::Vector3f angle = angular_velocity * pi->time;
        Eigen::AngleAxisf t_Vz(angle(2), Eigen::Vector3f::UnitZ());
        Eigen::AngleAxisf t_Vy(angle(1), Eigen::Vector3f::UnitY());
        Eigen::AngleAxisf t_Vx(angle(0), Eigen::Vector3f::UnitX());
        Eigen::AngleAxisf t_V;
        t_V = t_Vz * t_Vy * t_Vx;
        Eigen::Vector3f point(pi->x, pi->y, pi->z);
        Eigen::Vector3f adjusted_point = t_V.matrix() * point + linear_velocity * pi->time;
        po->x = adjusted_point[0];
        po->y = adjusted_point[1];
        po->z = adjusted_point[2];
    }

    void CloudLocalizer::DeleteOldKeys()
    {
        bool res = !sc_old_to_delete_.empty();
        old_key_size_ -= sc_old_to_delete_.size();
        while (!sc_old_to_delete_.empty())
        {
            int index = sc_old_to_delete_.back();
            std::string path = sc_loop_path_ + "/pointcloud_" + std::to_string(sc_state_[index].index) + ".pcd";
            boost::filesystem::remove(path.c_str());
            scmanager_->deleteOldSC(index);
            sc_state_.erase(sc_state_.begin() + index);
            sc_old_to_delete_.pop_back();
        }
        if (res)
        {
            scmanager_->treereconstruction();
        }
    }
    void CloudLocalizer::ReadKeys(const YAML::Node &yaml_node)
    {
        sc_state_.clear();
        old_key_size_ = 0;
        scmanager_ = std::make_shared<SCManager>();
        scmanager_->LIDAR_HEIGHT = yaml_node["LIDAR_HEIGHT"].as<double>();   // lidar height : add this for simply directly using lidar scan in the lidar local coord (not robot base coord) / if you use robot-coord-transformed lidar scans, just set this as 0.
        scmanager_->PC_NUM_RING = yaml_node["PC_NUM_RING"].as<int>();        // 20 in the original paper (IROS 18)
        scmanager_->PC_NUM_SECTOR = yaml_node["PC_NUM_SECTOR"].as<int>();    // 60 in the original paper (IROS 18)
        scmanager_->PC_MAX_RADIUS = yaml_node["PC_MAX_RADIUS"].as<double>(); // 80 meter max in the original paper (IROS 18)
        scmanager_->PC_UNIT_SECTORANGLE = 360.0 / double(scmanager_->PC_NUM_SECTOR);
        scmanager_->PC_UNIT_RINGGAP = scmanager_->PC_MAX_RADIUS / double(scmanager_->PC_NUM_RING);
        scmanager_->NUM_EXCLUDE_RECENT = yaml_node["NUM_EXCLUDE_RECENT"].as<int>();             // simply just keyframe gap, but node position distance-based exclusion is ok.
        scmanager_->NUM_CANDIDATES_FROM_TREE = yaml_node["NUM_CANDIDATES_FROM_TREE"].as<int>(); // 10 is enough. (refer the IROS 18 paper)
        scmanager_->SEARCH_RATIO = yaml_node["SEARCH_RATIO"].as<double>();                      // for fast comparison, no Brute-force, but search 10 % is okay. // not was in the original conf paper, but improved ver.
        scmanager_->SC_DIST_THRES = yaml_node["SC_DIST_THRES"].as<double>();                    // 0.4-0.6 is good choice for using with robust kernel (e.g., Cauchy, DCS) + icp fitness threshold / if not, recommend 0.1-0.15
        scmanager_->TREE_MAKING_PERIOD_ = yaml_node["TREE_MAKING_PERIOD"].as<int>();            // i.e., remaking tree frequency, to avoid non-mandatory every remaking, to save time cost / in the LeGO-LOAM integration, it is synchronized with the loop detection callback (which is 1Hz) so it means the tree is updated evrey 10 sec. But you can use the smaller value because it is enough fast ~ 5-50ms wrt N.
        std::ifstream in(sc_loop_path_ + "/scancontext", std::ios::in | std::ios::binary);
        if (!in.good())
        {
            return;
        }
        int num;
        in.read((char *)(&num), sizeof(int));
        old_key_size_ = num;
        for (int i = 0; i < num; ++i)
        {
            State state;
            ReadState(in, state);
            sc_state_.emplace_back(state);
            Eigen::MatrixXd matrix;
            ReadMatrix(in, matrix);
            scmanager_->polarcontexts_.emplace_back(matrix);
            ReadMatrix(in, matrix);
            scmanager_->polarcontext_invkeys_.emplace_back(matrix);
            ReadMatrix(in, matrix);
            scmanager_->polarcontext_vkeys_.emplace_back(matrix);
            std::vector<float> vt;
            ReadVector(in, vt);
            scmanager_->polarcontext_invkeys_mat_.emplace_back(vt);
        }
        in.close();
        scmanager_->treereconstruction();
    }
} // namespace yd_fusion_localization
