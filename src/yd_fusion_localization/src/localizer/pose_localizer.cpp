/**
 * @file pose_localizer.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-26
 * @brief pose localizer
 */

#include "yd_fusion_localization/localizer/pose_localizer.hpp"

namespace yd_fusion_localization
{
    PoseLocalizer::PoseLocalizer(const YAML::Node &yaml_node) : Localizer(yaml_node)
    {
        if (yaml_node["invalid_ranges"].IsDefined())
        {
            std::vector<double> inr = yaml_node["invalid_ranges"].as<std::vector<double>>();
            for (int i = 0; i < 6; ++i)
            {
                invalid_ranges_(i) = inr[i];
            }
        }
        else
        {
            for (int i = 0; i < 6; ++i)
            {
                invalid_ranges_(i) = 0;
            }
        }
        distance_threshold_ = yaml_node["distance_threshold"].as<double>();
        angle_threshold_ = yaml_node["angle_threshold"].as<double>();
    }

    bool PoseLocalizer::CheckPosition(const Sophus::SE3d &pose)
    {
        Eigen::Vector3d p = pose.translation();
        if (p[0] > invalid_ranges_[0] && p[0] < invalid_ranges_[1] &&
            p[1] > invalid_ranges_[2] && p[1] < invalid_ranges_[3] &&
            p[2] > invalid_ranges_[4] && p[2] < invalid_ranges_[5])
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    bool PoseLocalizer::LoopDetection(const MeasureDataPtr &data, std::deque<Sophus::SE3d> &pose)
    {
        Sophus::SE3d result_pose;
        if (!GetPose(data, result_pose))
        {
            return false;
        }
        pose.push_back(result_pose);
        return true;
    }

    bool PoseLocalizer::Initialize(const MeasureDataPtr &data, const MState &mstate, LocalizerDataPtr &localizer_data)
    {
        return GetPose(data, mstate, localizer_data);
    }

    bool PoseLocalizer::GetPose(const MeasureDataPtr &data, const MState &guess, LocalizerDataPtr &localizer_data)
    {
        Sophus::SE3d result_pose;
        if (!GetPose(data, result_pose))
        {
            return false;
        }

        if ((guess.pose.translation() - result_pose.translation()).norm() > distance_threshold_ || std::acos(std::fabs((guess.pose.unit_quaternion().inverse() * result_pose.unit_quaternion()).w())) * 2 > angle_threshold_)
        {
            return false;
        }

        PoseLocalizerDataPtr posel_data = std::dynamic_pointer_cast<PoseLocalizerData>(localizer_data);
        posel_data->sensor_to_robot = data->sensor_to_robot;
        posel_data->local_to_map = local_to_map_;
        posel_data->noise = data->noise;
        posel_data->stamp = data->stamp;
        posel_data->pose = result_pose;
        return true;
    }
    bool PoseLocalizer::GetPose(const MeasureDataPtr &data, LocalizerDataPtr &localizer_data)
    {
        Sophus::SE3d result_pose;
        if (!GetPose(data, result_pose))
        {
            return false;
        }

        PoseLocalizerDataPtr posel_data = std::dynamic_pointer_cast<PoseLocalizerData>(localizer_data);
        posel_data->sensor_to_robot = data->sensor_to_robot;
        posel_data->local_to_map = local_to_map_;
        posel_data->noise = data->noise;
        posel_data->stamp = data->stamp;
        posel_data->pose = result_pose;
        return true;
    }
    bool PoseLocalizer::Save(const MState &mstate, const MeasureDataPtr &data)
    {
        return false;
    }
    bool PoseLocalizer::Save()
    {
        return false;
    }
    bool PoseLocalizer::GetPose(const MeasureDataPtr &data, Sophus::SE3d &result_pose)
    {
        PoseDataPtr pose_data = std::dynamic_pointer_cast<PoseData>(data);
        result_pose = pose_data->pose;
        return true;
    }
} // namespace yd_fusion_localization