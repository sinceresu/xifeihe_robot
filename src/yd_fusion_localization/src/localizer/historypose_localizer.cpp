/**
 * @file historypose_localizer.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-26
 * @brief historypose localizer
 */

#include "yd_fusion_localization/localizer/historypose_localizer.hpp"
#include "yd_fusion_localization/global_defination/global_defination.h"

namespace yd_fusion_localization
{
    HistoryposeLocalizer::HistoryposeLocalizer(const YAML::Node &yaml_node)
    {
        std::string path;
        if (yaml_node["save_path"].IsDefined())
        {
            path = WORK_SPACE_PATH + yaml_node["save_path"].as<std::string>();
        }
        else
        {
            path = WORK_SPACE_PATH + "/config/localization/pose_history";
        }
        if (yaml_node["save_num"].IsDefined())
        {
            num_ = yaml_node["save_num"].as<int>();
        }
        else
        {
            num_ = 2;
        }
        
        path_.clear();
        for (int i = 0; i < num_; ++i)
        {
            path_.push_back(path + "/pose" + std::to_string(i) + ".yaml");
        }
        path_.push_back(path + "/doghouse.yaml");
        count_ = 0;
    }

    HistoryposeLocalizer::~HistoryposeLocalizer()
    {
    }

    bool HistoryposeLocalizer::LoopDetection(std::deque<Sophus::SE3d> &data)
    {
        for (int i = 0; i < path_.size(); ++i)
        {
            std::ifstream fin(path_[i]);
            if (fin)
            {
                std::vector<double> fs;
                std::string strs;
                while (std::getline(fin, strs))
                {
                    fs.push_back(std::atof(strs.c_str()));
                }
                if (fs.size() == 7)
                {
                    Sophus::SE3d s;
                    if (GetTransform(fs, s))
                    {
                        data.push_back(s);
                    }
                }
            }
            fin.close();
        }
        return true;
    }

    void HistoryposeLocalizer::SavePose(const Sophus::SE3d &data)
    {
        // return;
        count_ = count_ % num_;
        std::ofstream fout(path_[count_]);
        if (fout)
        {
            fout << std::setprecision(12) << data.translation()[0] << std::endl
                 << data.translation()[1] << std::endl
                 << data.translation()[2] << std::endl
                 << data.unit_quaternion().w() << std::endl
                 << data.unit_quaternion().x() << std::endl
                 << data.unit_quaternion().y() << std::endl
                 << data.unit_quaternion().z();
        }
        fout.close();
        count_++;
    }
} // namespace yd_fusion_localization
