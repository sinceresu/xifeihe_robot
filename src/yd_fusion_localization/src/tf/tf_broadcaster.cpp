/**
 * @file tf_broadcaster.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-15
 * @brief tf broadcaster
 */

#include "yd_fusion_localization/tf/tf_broadcaster.hpp"

namespace yd_fusion_localization
{
    TFBroadCaster::TFBroadCaster(std::string frame_id, std::string child_frame_id) {
        transform_.frame_id_ = frame_id;
        transform_.child_frame_id_ = child_frame_id;
    }

    void TFBroadCaster::SendTransform(double stamp, const Sophus::SE3d& pose) {
        transform_.stamp_ = ros::Time(stamp);
        TransformDataType(pose, transform_);
        broadcaster_.sendTransform(transform_);
    }
}