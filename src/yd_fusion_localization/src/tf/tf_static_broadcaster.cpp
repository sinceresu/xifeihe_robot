/**
 * @file tf_broadcaster.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-15
 * @brief tf broadcaster
 */

#include "yd_fusion_localization/tf/tf_static_broadcaster.hpp"

namespace yd_fusion_localization
{
    TFStaticBroadCaster::TFStaticBroadCaster(std::string frame_id, std::string child_frame_id) :
        tf_static_published_(false) {
        transform_.header.frame_id = frame_id;
        transform_.child_frame_id = child_frame_id;
    }

    void TFStaticBroadCaster::SendTransform(double stamp, const Sophus::SE3d& pose) {
        if (tf_static_published_) 
            return;

        auto transform = Eigen::Isometry3d(pose.matrix());
        auto translation = transform.translation();
        auto rotation = Eigen::Quaterniond(transform.linear());
        transform_.transform.translation.x = translation[0];
        transform_.transform.translation.y = translation[1];
        transform_.transform.translation.z = translation[2];
        transform_.transform.rotation.x = rotation.x();
        transform_.transform.rotation.y = rotation.y();
        transform_.transform.rotation.z = rotation.z();
        transform_.transform.rotation.w = rotation.w();
         transform_.header.stamp = ros::Time(stamp);
        broadcaster_.sendTransform(transform_);
        tf_static_published_ = true;
    }
}