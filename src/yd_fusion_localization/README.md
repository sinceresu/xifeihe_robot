# yd_fusion_localization (巡检车融合定位)
提供巡检机器人的定位功能

## 安装编译环境

1 源码安装方式：
第三方库中的g2o.tar.gz和ceres-solver-1.14.0.tar.gz
解压缩后分别执行：

````shell
mkdir build
cd build
cmake ..
make -j3
sudo make install
````
2 安装包安装方式：

````shell
sudo apt install libceres-dev
sudo apt install ros-melodic-libg2o
````
##  订阅话题
```json
/AuxLocator/laser_odom_to_init [nav_msgs/Odometry] #雷达里程计
/imu/data [sensor_msgs/Imu] #imu数据
/initialpose [unknown type] #初始化数据
/velodyne_points [sensor_msgs/PointCloud2] #雷达数据
/yida/wheel/odometer [geometry_msgs/TwistStamped] #轮速计数据
```

## 发布话题
```json
/current_scan [sensor_msgs/PointCloud2] #转换到地图坐标系的当前帧扫描
/global_map [sensor_msgs/PointCloud2] #地图点云
/local_map [sensor_msgs/PointCloud2] #局部地图点云
/robot_pose [nav_msgs/Odometry] #巡检车车身中心定位
/yd/heartbeat [diagnostic_msgs/DiagnosticArray] #程序运行状态
```
## 发布坐标变换(TF)
|frame_id|child_frame_id|描述
|--------------------- |-----------|----------------------|
|map|robot_pose|从机器人中心坐标到地图坐标的转换
|robot_pose|lidar_pose|从激光雷达坐标到机器人中心坐标的转换

## 多传感器融合配置参数
|名称|类型|描述|缺省
|--------------------- |-----------|----------------------|------------------|
|predictors：number|int|融合预测传感器的数量，排序没有先后之分，但是id需要按照顺序且不可重复|Odometry为雷达里程计，Twist为轮速计，Imu为imu传感器，主要用于配置多传感器融合入
|measurers:number|int|融合观测传感器器的数量，排序没有先后之分，但是id需要按照顺序且不可重复|Cloud:雷达传感器，Image：相机传感器，主要用于配置视觉回狗窝。
|state_type|string|两种配置方案，Vbabg和Basic，融合雷达imu轮速计为Vbabg，融合雷达和轮速计以及只使用雷达均为Basic|实际映射为代码中每一个状态量的vertex数量

## 视觉回狗窝配置参数
位置: backend : measurers 
|名称|类型|描述|缺省
|--------------------- |-----------|----------------------|------------------|
|number|int|1为不使用视觉回狗窝， 2为使用视觉回狗窝|需要保证no_1的type为Cloud，no_2的type为Image
|sensor_to_robot|xyz rpy|该传感器到车体中心的外参|标定获得
|topic|string|订阅该传感器的topic名称|
|lidar_map_transform|xyz wxyz|用于轮渡项目雷达倒置安装|巡检车配置[0, 0, 0, 1, 0, 0, 0]，轮渡配置[0, 0, 0, 0, 1, 0, 0]
|map_path|string|点云地图路径|默认home路径下，需更改为地图到home的相对路径
|local_to_map|xyz wxyz|狗窝的二维码在世界坐标系下的坐标|需要其他程序标定所得
|tag_size|double|狗窝二维码尺寸大小|测量获得
|intrinsic_matrix|double|相机内参|标定获得
|valid_ranges|min_x max_x min_y max_y min_z max_z|当定位处在设置的范围内时，使用视觉定位|根据标定的到的local_to_map，和巡检车进狗窝时定位变化趋势，估计充电桩之前约2米的定位范围。




## 参数
|名称|类型|描述|缺省
|--------------------- |-----------|----------------------|------------------|
|map_frame|string|世界坐标系（主要用于发布tf）|
|robot_frame|string|机器人坐标系（主要用于发布tf）|
|update_interval_time|double|定位更新时间间隔|
|update_delay_time|double|更新延时阈值,若大于该值则说明定位失败|
|max_vel|double|最大线速度，若定位推算出的线速度大于该值,则定位失败|
|max_gyro|double|最大角速度|
|max_std_deviation|double|前端航迹推算的标准差阈值,若大于该值,则定位失败,主要用于考虑航迹推算的累积误差|
|inlier_check|bool|后端优化是否返回的内感受型传感器的异常情况,如果是,则会在航迹推算时只采用非异常的传感器|
|initialpose|double|拉坐标时的地图坐标系 z 坐标|
|interval_time|double|后端优化时间间隔阈值|
|interval_distance|double|后端优化的平移间隔阈值|
|interval_angle|double 弧度|后端优化的角度间隔阈值|
|interval_predict_time|double|后端只融合内感受型传感器的时间间隔阈值|
|init_interval_time|double|初始化窗口时 2 个关键帧的时间间隔|
|init_delay_ltime|double|初始化时的全局定位数据延时阈值,若大于该值则初始化失败|
|predictor_delay_time|double|后端预测延时阈值,若大于该值,则定位失败|
|save|bool|是否保存关键帧数据|
|history_loop|bool|是否采用历史定位数据进行重定位|
|localizer_thresh|(时间/位移/角度)|全局定位的间隔阈值,若大于该值,则定位失败|
|std_deviation_thresh|double|最新帧的标准差阈值,若大于该值,则定位失败|
|distance_threshold|double|点云中有效点距离范围|
|undistort|bool|是否去畸变|
|sc_replace_old|int|保存关键帧的策略|
|match_threshold|double|重定位时匹配得分阈值|

## 启动

### 在线启动
````shell
roslaunch yd_fusion_localization online.launch
````

### 离线启动
````shell
roslaunch yd_fusion_localization param.launch
roslaunch yd_fusion_localization online.launch
rosbag play --clock xxx.bag
````
