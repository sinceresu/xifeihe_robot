/**
 * @file online_node.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-1-21
 * @brief online node
 */

#include "yd_fusion_localization/localization/localization_flow.hpp"
#include <rosbag/view.h>
#include <rosgraph_msgs/Clock.h>

namespace yd_fusion_localization {
std::string WORK_SPACE_PATH;
}
using namespace yd_fusion_localization;

int main(int argc, char *argv[])
{
    google::InitGoogleLogging(argv[0]);
    google::ParseCommandLineFlags(&argc, &argv, true);
    FLAGS_log_dir =  "/home/Log";
    FLAGS_stop_logging_if_full_disk = true;
    FLAGS_logtostderr = true;
    // FLAGS_minloglevel = 1;

    yd_fusion_localization::WORK_SPACE_PATH = ros::package::getPath("yd_fusion_localization");

    ros::init(argc, argv, "online_node");

    ros::NodeHandle nh;
    std::shared_ptr<LocalizationFlow> localization_flow_ptr = std::make_shared<LocalizationFlow>(nh);

    ros::Rate rate(20);
    while (true)
    {
        if (!ros::ok())
        {
            localization_flow_ptr->Finish();
            break;
        }
        ros::spinOnce();
        localization_flow_ptr->Run(false);

        rate.sleep();
    }

    return 0;
}
