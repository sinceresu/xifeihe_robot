/**
 * @file mapping_offline_node.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-7-20
 * @brief mapping offline node
 */

#include "yd_fusion_localization/mapping/lidar_mapping_flow.hpp"
#include <boost/filesystem.hpp>

#include <rosgraph_msgs/Clock.h>
#include <rosbag/view.h>

#include "yd_fusion_localization/global_defination/global_defination.h"

using namespace yd_fusion_localization;
namespace yd_fusion_localization {
std::string WORK_SPACE_PATH;
}
int main(int argc, char *argv[])
{
    google::InitGoogleLogging(argv[0]);
    yd_fusion_localization::WORK_SPACE_PATH = ros::package::getPath("yd_fusion_localization");
    FLAGS_log_dir = WORK_SPACE_PATH + "/Log";
    FLAGS_stop_logging_if_full_disk = true;
    FLAGS_logtostderr = true;
    if (argc > 2)
    {
        int tostderr = std::atoi(argv[2]);
        if (tostderr == 1)
        {
            FLAGS_logtostderr = false;
        }
        else if (tostderr == 2)
        {
            FLAGS_logtostderr = false;
            FLAGS_alsologtostderr = true;
        }
    }
    ros::init(argc, argv, "mapping_offline_node");
    ros::NodeHandle nh;

    std::vector<std::string> topics;
    std::vector<std::string> types;
    std::string yaml_path = WORK_SPACE_PATH + "/config/mapping/lidar_mapping.yaml";
    YAML::Node yaml_node = YAML::LoadFile(yaml_path);

    types.push_back("Cloud");
    std::string topic = yaml_node["cloud_subscriber"]["topic"].as<std::string>();
    topics.push_back(topic);
    int num = yaml_node["predictors"]["number"].as<int>();
    for (int i = 0; i < num; ++i)
    {
        std::string index("no_");
        index = index + std::to_string(i + 1);
        std::string type = yaml_node["predictors"][index]["type"].as<std::string>();
        if (type == "Vg")
        {
            types.push_back("Twist");
            types.push_back("Imu");
            std::string topic = yaml_node["predictors"][index]["twist"]["topic"].as<std::string>();
            topics.push_back(topic);
            topic = yaml_node["predictors"][index]["imu"]["topic"].as<std::string>();
            topics.push_back(topic);
        }
        else
        {
            types.push_back(type);
            std::string topic = yaml_node["predictors"][index]["topic"].as<std::string>();
            topics.push_back(topic);
        }
    }
    num = yaml_node["measurers"]["number"].as<int>();
    for (int i = 0; i < num; ++i)
    {
        std::string index("no_");
        index = index + std::to_string(i + 1);
        std::string topic = yaml_node["measurers"][index]["subscriber"]["topic"].as<std::string>();
        topics.push_back(topic);
        std::string type = yaml_node["measurers"][index]["subscriber"]["type"].as<std::string>();
        types.push_back(type);
    }

    std::vector<ros::Publisher> pubs(topics.size());
    for (int i = 0; i < topics.size(); ++i)
    {
        if (types[i] == "Odometry")
        {
            pubs[i] = nh.advertise<nav_msgs::Odometry>(topics[i], 100, false);
            ;
        }
        else if (types[i] == "Twist")
        {
            pubs[i] = nh.advertise<geometry_msgs::TwistStamped>(topics[i], 100, false);
        }
        else if (types[i] == "Imu")
        {
            pubs[i] = nh.advertise<sensor_msgs::Imu>(topics[i], 100, false);
        }
        else if (types[i] == "GNSS")
        {
            pubs[i] = nh.advertise<sensor_msgs::NavSatFix>(topics[i], 100, false);
        }
        else if (types[i] == "Cloud")
        {
            pubs[i] = nh.advertise<sensor_msgs::PointCloud2>(topics[i], 100, false);
        }
        else if (types[i] == "Pose")
        {
            pubs[i] = nh.advertise<geometry_msgs::PoseWithCovarianceStamped>(topics[i], 100, false);
        }
        else if (types[i] == "Image")
        {
            pubs[i] = nh.advertise<sensor_msgs::Image>(topics[i], 100, false);
        }
        else if (types[i] == "P")
        {
            pubs[i] = nh.advertise<geometry_msgs::PoseStamped>(topics[i], 100, false);
        }
        else if (types[i] == "Q")
        {
            pubs[i] = nh.advertise<sensor_msgs::Imu>(topics[i], 100, false);
        }
    }

    ros::param::set("/use_sim_time", true);
    ros::Publisher clock_publisher = nh.advertise<rosgraph_msgs::Clock>("/clock", 100);
    rosgraph_msgs::Clock clock;

    rosbag::Bag bag;
    bag.open(argv[1], rosbag::bagmode::Read);
    rosbag::View view(bag, rosbag::TopicQuery(topics));

    const std::string    map_path_ =std::string(std::getenv("HOME"))  + "/" +    yaml_node["lidar_mapping"]["map_path"].as<std::string>() + "/";
    std::shared_ptr<LidarMappingFlow> mapping_flow_ptr = std::make_shared<LidarMappingFlow>(nh, map_path_);
    BOOST_FOREACH (rosbag::MessageInstance const m, view)
    {
        if (!ros::ok())
        {
            mapping_flow_ptr->Finish();
            break;
        }

        clock.clock = m.getTime();
        clock_publisher.publish(clock);

        MessagePointer msg_ptr;
        for (int i = 0; i < topics.size(); ++i)
        {
            if (topics[i] == m.getTopic())
            {
                if (types[i] == "Odometry")
                {
                    msg_ptr.odometry_ptr = m.instantiate<nav_msgs::Odometry>();
                    pubs[i].publish(msg_ptr.odometry_ptr);
                }
                else if (types[i] == "Twist")
                {
                    msg_ptr.twist_ptr = m.instantiate<geometry_msgs::TwistStamped>();
                    pubs[i].publish(msg_ptr.twist_ptr);
                }
                else if (types[i] == "Imu")
                {
                    msg_ptr.imu_ptr = m.instantiate<sensor_msgs::Imu>();
                    pubs[i].publish(msg_ptr.imu_ptr);
                }
                else if (types[i] == "GNSS")
                {
                    msg_ptr.gnss_ptr = m.instantiate<sensor_msgs::NavSatFix>();
                    pubs[i].publish(msg_ptr.gnss_ptr);
                }
                else if (types[i] == "Cloud")
                {
                    msg_ptr.cloud_ptr = m.instantiate<sensor_msgs::PointCloud2>();
                    pubs[i].publish(msg_ptr.cloud_ptr);
                }
                else if (types[i] == "Pose")
                {
                    msg_ptr.pose_ptr = m.instantiate<geometry_msgs::PoseWithCovarianceStamped>();
                    pubs[i].publish(msg_ptr.pose_ptr);
                }
                else if (types[i] == "Image")
                {
                    msg_ptr.image_ptr = m.instantiate<sensor_msgs::Image>();
                    pubs[i].publish(msg_ptr.image_ptr);
                }
                else if (types[i] == "P")
                {
                    msg_ptr.p_ptr = m.instantiate<geometry_msgs::PoseStamped>();
                    pubs[i].publish(msg_ptr.p_ptr);
                }
                else if (types[i] == "Q")
                {
                    msg_ptr.q_ptr = m.instantiate<sensor_msgs::Imu>();
                    pubs[i].publish(msg_ptr.q_ptr);
                }
            }
        }

        mapping_flow_ptr->HandleMessage(msg_ptr, m.getTopic());

        mapping_flow_ptr->Run();
    }

    mapping_flow_ptr->Finish();
    std::cout << "mapping_offline_node OK!!!" << std::endl;

    return 0;
}
