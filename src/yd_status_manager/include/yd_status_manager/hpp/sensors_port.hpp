#ifndef SENSORS_PORT_HPP
#define SENSORS_PORT_HPP

#include <iostream>

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>

#include "ros/ros.h"

#include <netinet/in.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "../utils.cpp"

using namespace std;

class SensorsPort
{
    public:
        int open_result;
        string port_name;

        struct termios newtio, oldtio;

        // Constructor & Destructor
        SensorsPort(string name);
        ~SensorsPort();

        bool open_port();
        void read_port(int *read_nothing_count,

                       float *roll_angle,
                       float *pitch_angle,
                       float *heading_angle,

                       int *obstacle_dis,
                       int *left_fall_dis,
                       int *right_fall_dis);
        void close_port();
};

// Constructor
SensorsPort::SensorsPort(string name)
{
    port_name = name;
}
// Destructor
SensorsPort::~SensorsPort()
{
    close_port();
}

// 打开串口
bool SensorsPort::open_port()
{
    open_result = open(port_name.c_str(), O_RDWR | O_NOCTTY | O_NDELAY | O_NONBLOCK);

    if(open_result < 0)
    {
        ROS_INFO("OPEN PORT [%s] FAILED!", port_name.c_str());
        return false;
    }
    else
    {
        ROS_INFO("OPEN PORT [%s] SUCCESS!", port_name.c_str());

        if(tcgetattr(open_result, &oldtio) != 0)
        {
            ROS_INFO("TERMIOS GET I/O ATTR FAILED!");
        }

        bzero(&newtio, sizeof(newtio));
        newtio.c_cflag |= CLOCAL | CREAD;
        newtio.c_cflag &= ~CSIZE;

        /// 数据位 停止位 波特率
        newtio.c_cflag |= CS8;
        cfsetispeed(&newtio, B115200);
        cfsetospeed(&newtio, B115200);

        newtio.c_cflag &= ~CSTOPB;

        newtio.c_cc[VTIME] = 0;
        newtio.c_cc[VMIN]  = 0;
        tcflush(open_result, TCIFLUSH);

        if(tcsetattr(open_result, TCSANOW, &newtio) != 0)
        {
            ROS_INFO("PORT SET ERROR!");
            return false;
        }
        else
        {
            ROS_INFO("PORT SET DONE!");
            return true;
        }
    }
}
// 读取串口
void SensorsPort::read_port(int *read_nothing_count,

                            float *roll_angle,
                            float *pitch_angle,
                            float *heading_angle,

                            int *obstacle_dis,
                            int *left_fall_dis,
                            int *right_fall_dis)
{
    int res, read_len = 0;
    char buf[256];
    memset(buf, 0, sizeof(buf));

    res = 0;
    do
    {
        read_len =  read(open_result, buf, sizeof(buf));
        res += read_len;
    }
    while(read_len > 0);

    if(res < 0)
    {
        ROS_INFO("PORT READ ERROR!");
    }
    else if(res == 0)
    {
        *read_nothing_count++;
    }
    else
    {
        string s(buf);
        if(s.length() != 0)
        {
            //printf("********************\n");
            //printf("data(%lu): %s\n", s.length(), s.c_str());

            vector<string> str_arr = split(s, ";");
            // RPH、DY7、TF6、TF8
            //printf("%s\n", str_arr[0].c_str());
            //printf("%s\n", str_arr[1].c_str());
            //printf("%s\n", str_arr[2].c_str());
            //printf("%s\n", str_arr[3].c_str());

            //printf("------------\n");

            // 因为原始数据%8.3lf的格式设置，导致转化成string时出现“空格补位”的情况
            string rph_str  = replace(str_arr[0], " ", "#");
            string rph_str1 = replace(rph_str, "####", "#");
            string rph_str2 = replace(rph_str1, "###", "#");
            string rph_str3 = replace(rph_str2, "##", "#");
            //printf("%s\n", rph_str3.c_str());

            vector<string> rph_arr = split(rph_str3, "#");
            //printf("R=%s\n", rph_arr[1].c_str());
            //printf("P=%s\n", rph_arr[2].c_str());
            //printf("H=%s\n", rph_arr[3].c_str());

            *roll_angle    = atof(rph_arr[1].c_str());
            *pitch_angle   = atof(rph_arr[2].c_str());
            *heading_angle = atof(rph_arr[3].c_str());

            //printf("\n");

            // 距离cm
            vector<string> dy7_arr = split(str_arr[1], " ");
            //printf("DY7=%s\n", dy7_arr[1].c_str());
            *obstacle_dis = atoi(dy7_arr[1].c_str());

            //printf("\n");

            vector<string> tf6_arr = split(str_arr[2], " ");
            // 距离cm、强度(0~65535)、温度(℃  = value / 8 - 256)
            //printf("tf6(1)=%s\n", tf6_arr[1].c_str());
            //printf("tf6(2)=%s\n", tf6_arr[2].c_str());
            //printf("tf6(3)=%s\n", tf6_arr[3].c_str());

            *left_fall_dis = atoi(tf6_arr[1].c_str());

            //printf("\n");

            vector<string> tf8_arr = split(str_arr[3], " ");
            //printf("tf8(1)=%s\n", tf8_arr[1].c_str());
            //printf("tf8(2)=%s\n", tf8_arr[2].c_str());
            //printf("tf8(3)=%s\n", tf8_arr[3].c_str());

            *right_fall_dis = atoi(tf8_arr[1].c_str());

            //printf("------------\n");
            //printf("********************\n");
        }

        // Clear
        res      = 0;
        read_len = 0;
        memset(buf, 0, sizeof(buf));
    }
}
// 关闭串口
void SensorsPort::close_port()
{
    fcntl(open_result, F_SETFL, 0);
    tcflush(open_result, TCIOFLUSH);
    tcsetattr(open_result, TCSANOW, &oldtio);
    close(open_result);
}

#endif
