#ifndef YD_STATUS_MANAGER_HPP
#define YD_STATUS_MANAGER_HPP

#include "ros/ros.h"

#include <string>
#include <vector>

#include <string.h>

// 消息
#include <std_msgs/Float32.h>
#include <geometry_msgs/TwistStamped.h>

#include <diagnostic_msgs/DiagnosticArray.h>
#include <diagnostic_msgs/DiagnosticStatus.h>
#include <diagnostic_msgs/KeyValue.h>
#include "../utils.cpp"

using namespace std;

class YD_STATUS_MANAGER
{
    public:
        // variable
        ros::Time robot_sensors_next_time;
        // Topic
        // Sensors
        string lifts_height_sub_topic_name;
        string odometer_sub_topic_name;
        string robot_sensors_sub_topic_name;

        // Nodes
        string status_manager_heart_beat_pub_topic_name;
        string status_manager_robot_control_pub_topic_name;
        string status_manager_xavier_sleep_pub_topic_name;

        string status_manager_devices_status_pub_topic_name;

        string camera_manager_status_sub_topic_name;
        string home_manager_status_sub_topic_name;

        string fuzzy_controller_status_sub_topic_name;
        string platform_controller_status_sub_topic_name;

        /// Values
        // 云台升降杆高度
        float LIFT_HEIGHT;
        // 里程计
        float ROBOT_LINEAR_SPEED;
        float ROBOT_ANGULAR_SPEED;

        // 可见光、红外相机状态码
        int VISUAL_CAMERA_STATUS_CODE  = -1;
        int THERMAL_CAMERA_STATUS_CODE = -1;

        // 充电桩、自动门状态码
        string CHARGE_STAKE_STATUS_CODE;
        string AUTO_DOOR_STATUS_CODE;

        // 测距超声波
        string ULTRASONIC_DIS_LEFT_FRONT_VALUE;
        string ULTRASONIC_DIS_RIGHT_FRONT_VALUE;
        string ULTRASONIC_DIS_LEFT_BACK_VALUE;
        string ULTRASONIC_DIS_RIGHT_BACK_VALUE;
        // 防跌落超声波
        string ULTRASONIC_FALL_LEFT_FRONT_VALUE;
        string ULTRASONIC_FALL_RIGHT_FRONT_VALUE;
        string ULTRASONIC_FALL_LEFT_BACK_VALUE;
        string ULTRASONIC_FALL_RIGHT_BACK_VALUE;
        // 前后防撞条
        string ANTI_COLLISION_BAR_FRONT_STATUS_CODE;
        string ANTI_COLLISION_BAR_BACK_STATUS_CODE;
        // 温度传感器
        string TEMPERATURE_SENSOR_1_VALUE;
        string TEMPERATURE_SENSOR_2_VALUE;
        string TEMPERATURE_SENSOR_3_VALUE;
        string TEMPERATURE_SENSOR_4_VALUE;

        // 电机 压、流、can通信
        string ROBOT_MOTOR_1_VOLTAGE_VALUE;
        string ROBOT_MOTOR_1_ELECTRICITY_VALUE;
        string ROBOT_MOTOR_1_CAN_SIGNAL_STATUS_CODE;

        string ROBOT_MOTOR_2_VOLTAGE_VALUE;
        string ROBOT_MOTOR_2_ELECTRICITY_VALUE;
        string ROBOT_MOTOR_2_CAN_SIGNAL_STATUS_CODE;

        string ROBOT_MOTOR_3_VOLTAGE_VALUE;
        string ROBOT_MOTOR_3_ELECTRICITY_VALUE;
        string ROBOT_MOTOR_3_CAN_SIGNAL_STATUS_CODE;

        string ROBOT_MOTOR_4_VOLTAGE_VALUE;
        string ROBOT_MOTOR_4_ELECTRICITY_VALUE;
        string ROBOT_MOTOR_4_CAN_SIGNAL_STATUS_CODE;

        string ROBOT_MOTOR_5_VOLTAGE_VALUE;
        string ROBOT_MOTOR_5_ELECTRICITY_VALUE;
        string ROBOT_MOTOR_5_CAN_SIGNAL_STATUS_CODE;

        string ROBOT_MOTOR_6_VOLTAGE_VALUE;
        string ROBOT_MOTOR_6_ELECTRICITY_VALUE;
        string ROBOT_MOTOR_6_CAN_SIGNAL_STATUS_CODE;

        string ROBOT_MOTOR_7_VOLTAGE_VALUE;
        string ROBOT_MOTOR_7_ELECTRICITY_VALUE;
        string ROBOT_MOTOR_7_CAN_SIGNAL_STATUS_CODE;

        string ROBOT_MOTOR_8_VOLTAGE_VALUE;
        string ROBOT_MOTOR_8_ELECTRICITY_VALUE;
        string ROBOT_MOTOR_8_CAN_SIGNAL_STATUS_CODE;

        // 电池(电压、电流、电量、温度等)
        string BATTERY_PART_1_VOLTAGE_VALUE;
        string BATTERY_PART_2_VOLTAGE_VALUE;
        string BATTERY_PART_3_VOLTAGE_VALUE;
        string BATTERY_PART_4_VOLTAGE_VALUE;
        string BATTERY_PART_5_VOLTAGE_VALUE;
        string BATTERY_PART_6_VOLTAGE_VALUE;
        string BATTERY_PART_7_VOLTAGE_VALUE;
        string BATTERY_PART_8_VOLTAGE_VALUE;
        string BATTERY_PART_9_VOLTAGE_VALUE;
        string BATTERY_PART_10_VOLTAGE_VALUE;
        string BATTERY_PART_11_VOLTAGE_VALUE;
        string BATTERY_PART_12_VOLTAGE_VALUE;
        string BATTERY_PART_13_VOLTAGE_VALUE;

        string BATTERY_TEMPERATURE_1_VALUE;
        string BATTERY_TEMPERATURE_2_VALUE;
        string BATTERY_TEMPERATURE_3_VALUE;
        string BATTERY_TEMPERATURE_4_VALUE;
        string BATTERY_TEMPERATURE_5_VALUE;

        string BATTERY_HIGH_VOLTAGE_ALARM_CODE;
        string BATTERY_LOW_VOLTAGE_ALARM_CODE;
        string BATTERY_OVER_CURRENT_ALARM_CODE;
        string BATTERY_HIGH_TEMPERATURE_ALARM_CODE;

        string BATTERY_CHARGE_DISCHARGE_ELECTRICITY_VALUE;
        string BATTERY_VOLUME_VALUE;
        string BATTERY_RECYCLE_NUM_VALUE;
        string BATTERY_VOLUME_RATIO;

        string WIRELESS_CHARGE_ALARM_CODE;

        // 充、放电 MOS
        string BATTERY_CHARGE_MOS_STATUS_CODE;
        string BATTERY_DISCHARGE_MOS_STATUS_CODE;
        // ACD电压检测
        string ADC1_VOLTAGE_VALUE;
        string ADC2_VOLTAGE_VALUE;
        string ADC3_VOLTAGE_VALUE;
        string ADC4_VOLTAGE_VALUE;

        // 车子RPY
        string ROBOT_ROLL_ANGLE_VALUE;
        string ROBOT_PITCH_ANGLE_VALUE;
        string ROBOT_YAW_ANGLE_VALUE;
        //机器时间
        string ROBOT_START_TIME;

        // 云台水平、垂直角度 及 升降台高度
        string PLATFORM_HORIZONTAL_ANGLE;
        string PLATFORM_VERTICAL_ANGLE;

        /// Publisher、Subscriber
        // Pub
        ros::Publisher statusManagerPub;
        // Sub
        // 传感器
        ros::Subscriber liftHeightSub;
        ros::Subscriber odometerSub;
        ros::Subscriber robotSensorsSub;
        // 监听节点
        ros::Subscriber cameraManagerStatusSub;
        ros::Subscriber homeManagerStatusSub;

        ros::Subscriber fuzzyControllerStatusSub;
        ros::Subscriber platformControllerStatusSub;

        // Constructor & Destructor
        YD_STATUS_MANAGER(NodeHandle n);
        ~YD_STATUS_MANAGER();

        // 回调
        void liftHeightMessagesCallback(const std_msgs::Float32::ConstPtr& msg);
        void odometerMessagesCallback(const geometry_msgs::TwistStamped::ConstPtr& msg);
        void robotSensorsMessagesCallback(const diagnostic_msgs::DiagnosticStatus::ConstPtr& msg);

        void cameraManagerStatusMessagesCallback(const diagnostic_msgs::DiagnosticArray::ConstPtr& msg);
        void homeManagerStatusMessagesCallback(const diagnostic_msgs::DiagnosticArray::ConstPtr& msg);

        void fuzzyControllerStatusMessagesCallback(const diagnostic_msgs::DiagnosticArray::ConstPtr& msg);
        void platformControllerStatusMessagesCallback(const diagnostic_msgs::DiagnosticArray::ConstPtr& msg);

        void sendStatusData(nav_msgs::Odometry robot_pose, int xavier_level_value);
};

// Constructor
YD_STATUS_MANAGER::YD_STATUS_MANAGER(NodeHandle n)
{
    ROS_INFO("YD_STATUS_MANAGER() Constructor");

    ros::param::get("/yd_status_manager/lifts_height_sub_topic_name", lifts_height_sub_topic_name);
    ros::param::get("/yd_status_manager/odometer_sub_topic_name", odometer_sub_topic_name);
    ros::param::get("/yd_status_manager/robot_sensors_sub_topic_name", robot_sensors_sub_topic_name);

    ros::param::get("/yd_status_manager/status_manager_heart_beat_pub_topic_name", status_manager_heart_beat_pub_topic_name);
    ros::param::get("/yd_status_manager/status_manager_robot_control_pub_topic_name", status_manager_robot_control_pub_topic_name);
    ros::param::get("/yd_status_manager/status_manager_xavier_sleep_pub_topic_name", status_manager_xavier_sleep_pub_topic_name);

    ros::param::get("/yd_status_manager/status_manager_devices_status_pub_topic_name", status_manager_devices_status_pub_topic_name);

    ros::param::get("/yd_status_manager/camera_manager_status_sub_topic_name", camera_manager_status_sub_topic_name);
    ros::param::get("/yd_status_manager/home_manager_status_sub_topic_name", home_manager_status_sub_topic_name);

    ros::param::get("/yd_status_manager/fuzzy_controller_status_sub_topic_name", fuzzy_controller_status_sub_topic_name);
    ros::param::get("/yd_status_manager/platform_controller_status_sub_topic_name", platform_controller_status_sub_topic_name);

    robot_sensors_next_time = ros::Time::now();

    statusManagerPub = n.advertise<diagnostic_msgs::DiagnosticArray>(status_manager_devices_status_pub_topic_name, 1);

    liftHeightSub   = n.subscribe<std_msgs::Float32>(lifts_height_sub_topic_name, 1, &YD_STATUS_MANAGER::liftHeightMessagesCallback, this);
    odometerSub     = n.subscribe<geometry_msgs::TwistStamped>(odometer_sub_topic_name, 1, &YD_STATUS_MANAGER::odometerMessagesCallback, this);
    robotSensorsSub = n.subscribe<diagnostic_msgs::DiagnosticStatus>(robot_sensors_sub_topic_name, 1, &YD_STATUS_MANAGER::robotSensorsMessagesCallback, this);

    cameraManagerStatusSub = n.subscribe<diagnostic_msgs::DiagnosticArray>(camera_manager_status_sub_topic_name, 1, &YD_STATUS_MANAGER::cameraManagerStatusMessagesCallback, this);
    homeManagerStatusSub   = n.subscribe<diagnostic_msgs::DiagnosticArray>(home_manager_status_sub_topic_name, 1, &YD_STATUS_MANAGER::homeManagerStatusMessagesCallback, this);

    fuzzyControllerStatusSub    = n.subscribe<diagnostic_msgs::DiagnosticArray>(fuzzy_controller_status_sub_topic_name, 1, &YD_STATUS_MANAGER::fuzzyControllerStatusMessagesCallback, this);
    platformControllerStatusSub = n.subscribe<diagnostic_msgs::DiagnosticArray>(platform_controller_status_sub_topic_name, 1, &YD_STATUS_MANAGER::platformControllerStatusMessagesCallback, this);
}

// CameraManagerStatusMessagesCallback
void YD_STATUS_MANAGER::cameraManagerStatusMessagesCallback(const diagnostic_msgs::DiagnosticArray::ConstPtr& msg)
{
    vector<diagnostic_msgs::DiagnosticStatus>::const_iterator it;
    for(it  = msg->status.begin(); it != msg->status.end(); ++it)
    {
        if(msg->header.frame_id == "visual")
        {
            VISUAL_CAMERA_STATUS_CODE = it->level;
        }
        if(msg->header.frame_id == "thermal")
        {
            THERMAL_CAMERA_STATUS_CODE = it->level;
        }
    }
}
// HomeManagerStatusMessagesCallback
void YD_STATUS_MANAGER::homeManagerStatusMessagesCallback(const diagnostic_msgs::DiagnosticArray::ConstPtr& msg)
{
    vector<diagnostic_msgs::DiagnosticStatus>::const_iterator it;
    for(it = msg->status.begin(); it != msg->status.end(); ++it)
    {
        ROS_INFO("level: %i", it->level);
        if(msg->header.frame_id == "charge_stake")
        {
            CHARGE_STAKE_STATUS_CODE = it->level;
        }
        if(msg->header.frame_id == "door")
        {
            AUTO_DOOR_STATUS_CODE = it->level;
        }
    }
}

// FuzzyControllerStatusMessagesCallback
void YD_STATUS_MANAGER::fuzzyControllerStatusMessagesCallback(const diagnostic_msgs::DiagnosticArray::ConstPtr& msg)
{
    // to do sth.
}
// PlatformControllerStatusMessagesCallback
void YD_STATUS_MANAGER::platformControllerStatusMessagesCallback(const diagnostic_msgs::DiagnosticArray::ConstPtr& msg)
{
    vector<diagnostic_msgs::DiagnosticStatus>::const_iterator it;
    for(it = msg->status.begin(); it != msg->status.end(); ++it)
    {
        string value = it->message;
        std::vector<std::string> v_arr = split(value, " ");
        PLATFORM_HORIZONTAL_ANGLE = v_arr[0];
        PLATFORM_VERTICAL_ANGLE   = v_arr[1];
    }
}

// PlatformLiftHeightMessagesCallback
void YD_STATUS_MANAGER::liftHeightMessagesCallback(const std_msgs::Float32::ConstPtr& msg)
{
    LIFT_HEIGHT = msg->data;
}
// OdometerMessagesCallback
void YD_STATUS_MANAGER::odometerMessagesCallback(const geometry_msgs::TwistStamped::ConstPtr& msg) {
    ROBOT_LINEAR_SPEED  = msg->twist.linear.x;
    ROBOT_ANGULAR_SPEED = msg->twist.angular.z;
}
// RobotSensorsMessagesCallback
void YD_STATUS_MANAGER::robotSensorsMessagesCallback(const diagnostic_msgs::DiagnosticStatus::ConstPtr& msg)
{
    ros::Time now_time = ros::Time::now();
    if (now_time < robot_sensors_next_time)
        return;
    robot_sensors_next_time = now_time + ros::Duration(0.5);

      for(int i = 0; i < msg->values.size(); i++)
      {
          // 防跌落传感器
          /// 左前(数据-1、传输-2、线路-3 异常)
          if(msg->values[i].key == "sensor6") { ULTRASONIC_FALL_LEFT_FRONT_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor67_5") { if(msg->values[i].value == "1") { ULTRASONIC_FALL_LEFT_FRONT_VALUE = "-3"; } }
          if(msg->values[i].key == "sensor67_6") { if(msg->values[i].value == "1") { ULTRASONIC_FALL_LEFT_FRONT_VALUE = "-2"; } }
          if(msg->values[i].key == "sensor67_7") { if(msg->values[i].value == "1") { ULTRASONIC_FALL_LEFT_FRONT_VALUE = "-1"; } }
          /// 右前
          if(msg->values[i].key == "sensor8") { ULTRASONIC_FALL_RIGHT_FRONT_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor67_2") { if(msg->values[i].value == "1") { ULTRASONIC_FALL_RIGHT_FRONT_VALUE = "-3"; } }
          if(msg->values[i].key == "sensor67_3") { if(msg->values[i].value == "1") { ULTRASONIC_FALL_RIGHT_FRONT_VALUE = "-2"; } }
          if(msg->values[i].key == "sensor67_4") { if(msg->values[i].value == "1") { ULTRASONIC_FALL_RIGHT_FRONT_VALUE = "-1"; } }
          /// 左后
          if(msg->values[i].key == "sensor10") { ULTRASONIC_FALL_LEFT_BACK_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor68_7") { if(msg->values[i].value == "1") { ULTRASONIC_FALL_LEFT_BACK_VALUE = "-3"; } }
          if(msg->values[i].key == "sensor67_0") { if(msg->values[i].value == "1") { ULTRASONIC_FALL_LEFT_BACK_VALUE = "-2"; } }
          if(msg->values[i].key == "sensor67_1") { if(msg->values[i].value == "1") { ULTRASONIC_FALL_LEFT_BACK_VALUE = "-1"; } }
          /// 右后
          if(msg->values[i].key == "sensor12") { ULTRASONIC_FALL_RIGHT_BACK_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor68_4") { if(msg->values[i].value == "1") { ULTRASONIC_FALL_RIGHT_BACK_VALUE = "-3"; } }
          if(msg->values[i].key == "sensor68_5") { if(msg->values[i].value == "1") { ULTRASONIC_FALL_RIGHT_BACK_VALUE = "-2"; } }
          if(msg->values[i].key == "sensor68_6") { if(msg->values[i].value == "1") { ULTRASONIC_FALL_RIGHT_BACK_VALUE = "-1"; } }

          // 超声波传感器
          if(msg->values[i].key == "sensor14") { ULTRASONIC_DIS_LEFT_FRONT_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor68_1") { if(msg->values[i].value == "1") { ULTRASONIC_DIS_LEFT_FRONT_VALUE = "-3"; } }
          if(msg->values[i].key == "sensor68_2") { if(msg->values[i].value == "1") { ULTRASONIC_DIS_LEFT_FRONT_VALUE = "-2"; } }
          if(msg->values[i].key == "sensor68_3") { if(msg->values[i].value == "1") { ULTRASONIC_DIS_LEFT_FRONT_VALUE = "-1"; } }

          if(msg->values[i].key == "sensor15") { ULTRASONIC_DIS_RIGHT_FRONT_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor69_6") { if(msg->values[i].value == "1") { ULTRASONIC_DIS_RIGHT_FRONT_VALUE = "-3"; } }
          if(msg->values[i].key == "sensor69_7") { if(msg->values[i].value == "1") { ULTRASONIC_DIS_RIGHT_FRONT_VALUE = "-2"; } }
          if(msg->values[i].key == "sensor68_0") { if(msg->values[i].value == "1") { ULTRASONIC_DIS_RIGHT_FRONT_VALUE = "-1"; } }

          if(msg->values[i].key == "sensor16") { ULTRASONIC_DIS_LEFT_BACK_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor71_5") { if(msg->values[i].value == "1") { ULTRASONIC_DIS_LEFT_BACK_VALUE = "-3"; } }
          if(msg->values[i].key == "sensor71_6") { if(msg->values[i].value == "1") { ULTRASONIC_DIS_LEFT_BACK_VALUE = "-2"; } }
          if(msg->values[i].key == "sensor71_7") { if(msg->values[i].value == "1") { ULTRASONIC_DIS_LEFT_BACK_VALUE = "-1"; } }

          if(msg->values[i].key == "sensor17") { ULTRASONIC_DIS_RIGHT_BACK_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor71_2") { if(msg->values[i].value == "1") { ULTRASONIC_DIS_RIGHT_BACK_VALUE = "-3"; } }
          if(msg->values[i].key == "sensor71_3") { if(msg->values[i].value == "1") { ULTRASONIC_DIS_RIGHT_BACK_VALUE = "-2"; } }
          if(msg->values[i].key == "sensor71_4") { if(msg->values[i].value == "1") { ULTRASONIC_DIS_RIGHT_BACK_VALUE = "-1"; } }

          // 防碰撞条传感器
          if(msg->values[i].key == "sensor18_7") { ANTI_COLLISION_BAR_FRONT_STATUS_CODE = msg->values[i].value; }
          if(msg->values[i].key == "sensor69_5") { if(msg->values[i].value == "1") { ANTI_COLLISION_BAR_FRONT_STATUS_CODE = "-3"; } }

          if(msg->values[i].key == "sensor18_6") { ANTI_COLLISION_BAR_BACK_STATUS_CODE = msg->values[i].value; }
          if(msg->values[i].key == "sensor69_4") { if(msg->values[i].value == "1") { ANTI_COLLISION_BAR_BACK_STATUS_CODE = "-3"; } }

          // 温度传感器
          if(msg->values[i].key == "sensor19") { TEMPERATURE_SENSOR_1_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor69_1") { if(msg->values[i].value == "1") { TEMPERATURE_SENSOR_1_VALUE = "-3"; } }
          if(msg->values[i].key == "sensor69_2") { if(msg->values[i].value == "1") { TEMPERATURE_SENSOR_1_VALUE = "-2"; } }
          if(msg->values[i].key == "sensor69_3") { if(msg->values[i].value == "1") { TEMPERATURE_SENSOR_1_VALUE = "-1"; } }

          if(msg->values[i].key == "sensor21") { TEMPERATURE_SENSOR_2_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor70_6") { if(msg->values[i].value == "1") { TEMPERATURE_SENSOR_2_VALUE = "-3"; } }
          if(msg->values[i].key == "sensor70_7") { if(msg->values[i].value == "1") { TEMPERATURE_SENSOR_2_VALUE = "-2"; } }
          if(msg->values[i].key == "sensor69_0") { if(msg->values[i].value == "1") { TEMPERATURE_SENSOR_2_VALUE = "-1"; } }

          if(msg->values[i].key == "sensor23") { TEMPERATURE_SENSOR_3_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor70_3") { if(msg->values[i].value == "1") { TEMPERATURE_SENSOR_3_VALUE = "-3"; } }
          if(msg->values[i].key == "sensor70_4") { if(msg->values[i].value == "1") { TEMPERATURE_SENSOR_3_VALUE = "-2"; } }
          if(msg->values[i].key == "sensor70_5") { if(msg->values[i].value == "1") { TEMPERATURE_SENSOR_3_VALUE = "-1"; } }

          if(msg->values[i].key == "sensor25") { TEMPERATURE_SENSOR_4_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor70_0") { if(msg->values[i].value == "1") { TEMPERATURE_SENSOR_4_VALUE = "-3"; } }
          if(msg->values[i].key == "sensor70_1") { if(msg->values[i].value == "1") { TEMPERATURE_SENSOR_4_VALUE = "-2"; } }
          if(msg->values[i].key == "sensor70_2") { if(msg->values[i].value == "1") { TEMPERATURE_SENSOR_4_VALUE = "-1"; } }

          // 电机
          if(msg->values[i].key == "sensor27") { ROBOT_MOTOR_1_VOLTAGE_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor29") { ROBOT_MOTOR_1_ELECTRICITY_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor31") { ROBOT_MOTOR_1_CAN_SIGNAL_STATUS_CODE = msg->values[i].value; }

          if(msg->values[i].key == "sensor32") { ROBOT_MOTOR_2_VOLTAGE_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor34") { ROBOT_MOTOR_2_ELECTRICITY_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor36") { ROBOT_MOTOR_2_CAN_SIGNAL_STATUS_CODE = msg->values[i].value; }

          if(msg->values[i].key == "sensor37") { ROBOT_MOTOR_3_VOLTAGE_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor39") { ROBOT_MOTOR_3_ELECTRICITY_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor41") { ROBOT_MOTOR_3_CAN_SIGNAL_STATUS_CODE = msg->values[i].value; }

          if(msg->values[i].key == "sensor42") { ROBOT_MOTOR_4_VOLTAGE_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor44") { ROBOT_MOTOR_4_ELECTRICITY_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor46") { ROBOT_MOTOR_4_CAN_SIGNAL_STATUS_CODE = msg->values[i].value; }

          if(msg->values[i].key == "sensor47") { ROBOT_MOTOR_5_VOLTAGE_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor49") { ROBOT_MOTOR_5_ELECTRICITY_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor51") { ROBOT_MOTOR_5_CAN_SIGNAL_STATUS_CODE = msg->values[i].value; }

          if(msg->values[i].key == "sensor52") { ROBOT_MOTOR_6_VOLTAGE_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor54") { ROBOT_MOTOR_6_ELECTRICITY_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor56") { ROBOT_MOTOR_6_CAN_SIGNAL_STATUS_CODE = msg->values[i].value; }

          if(msg->values[i].key == "sensor57") { ROBOT_MOTOR_7_VOLTAGE_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor59") { ROBOT_MOTOR_7_ELECTRICITY_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor61") { ROBOT_MOTOR_7_CAN_SIGNAL_STATUS_CODE = msg->values[i].value; }

          if(msg->values[i].key == "sensor62") { ROBOT_MOTOR_8_VOLTAGE_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor64") { ROBOT_MOTOR_8_ELECTRICITY_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor66") { ROBOT_MOTOR_8_CAN_SIGNAL_STATUS_CODE = msg->values[i].value; }

          // 电池(节)
          if(msg->values[i].key == "sensor78") { BATTERY_PART_1_VOLTAGE_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor80") { BATTERY_PART_2_VOLTAGE_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor82") { BATTERY_PART_3_VOLTAGE_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor84") { BATTERY_PART_4_VOLTAGE_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor86") { BATTERY_PART_5_VOLTAGE_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor88") { BATTERY_PART_6_VOLTAGE_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor90") { BATTERY_PART_7_VOLTAGE_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor92") { BATTERY_PART_8_VOLTAGE_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor94") { BATTERY_PART_9_VOLTAGE_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor96") { BATTERY_PART_10_VOLTAGE_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor98") { BATTERY_PART_11_VOLTAGE_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor100") { BATTERY_PART_12_VOLTAGE_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor102") { BATTERY_PART_13_VOLTAGE_VALUE = msg->values[i].value; }

          // 电池温度
          if(msg->values[i].key == "sensor104") { BATTERY_TEMPERATURE_1_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor105") { BATTERY_TEMPERATURE_2_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor106") { BATTERY_TEMPERATURE_3_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor107") { BATTERY_TEMPERATURE_4_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor108") { BATTERY_TEMPERATURE_5_VALUE = msg->values[i].value; }
          // 电池报警相关
          if(msg->values[i].key == "sensor109_7") { BATTERY_HIGH_VOLTAGE_ALARM_CODE = msg->values[i].value; }
          if(msg->values[i].key == "sensor109_6") { BATTERY_LOW_VOLTAGE_ALARM_CODE = msg->values[i].value; }
          if(msg->values[i].key == "sensor109_5") { BATTERY_OVER_CURRENT_ALARM_CODE = msg->values[i].value; }
          if(msg->values[i].key == "sensor109_4") { BATTERY_HIGH_TEMPERATURE_ALARM_CODE = msg->values[i].value; }

          // 无线充电故障信息
          if(msg->values[i].key == "sensor71_1") { WIRELESS_CHARGE_ALARM_CODE = msg->values[i].value; }
          // 充、放 电流
          if(msg->values[i].key == "sensor72") { BATTERY_CHARGE_DISCHARGE_ELECTRICITY_VALUE = msg->values[i].value; }
          // 电池剩余容量
          if(msg->values[i].key == "sensor74") { BATTERY_VOLUME_VALUE = msg->values[i].value; }
          // 循环次数
          if(msg->values[i].key == "sensor76") { BATTERY_RECYCLE_NUM_VALUE = msg->values[i].value; }
          // 电量百分比
          if(msg->values[i].key == "sensor162") { BATTERY_VOLUME_RATIO = msg->values[i].value; }

          // 充、放电 MOS
          if(msg->values[i].key == "sensor109_3") { BATTERY_CHARGE_MOS_STATUS_CODE = msg->values[i].value; }
          if(msg->values[i].key == "sensor109_2") { BATTERY_DISCHARGE_MOS_STATUS_CODE = msg->values[i].value; }
          // ADC电压检测
          if(msg->values[i].key == "sensor110") { ADC1_VOLTAGE_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor112") { ADC2_VOLTAGE_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor114") { ADC3_VOLTAGE_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor116") { ADC4_VOLTAGE_VALUE = msg->values[i].value; }

          // 机器人横滚角、俯仰角、偏航角
          if(msg->values[i].key == "sensor118") { ROBOT_ROLL_ANGLE_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor120") { ROBOT_PITCH_ANGLE_VALUE = msg->values[i].value; }
          if(msg->values[i].key == "sensor122") { ROBOT_YAW_ANGLE_VALUE = msg->values[i].value; }
          // 机器时间
          if(msg->values[i].key == "sensor124") { ROBOT_START_TIME = msg->values[i].value; }
      }
}

// 汇总发送各个节点状态
void YD_STATUS_MANAGER::sendStatusData(nav_msgs::Odometry robot_pose, int xavier_level_value)
{
    diagnostic_msgs::DiagnosticArray statusMsg;
    statusMsg.header.stamp = ros::Time::now();

    diagnostic_msgs::DiagnosticStatus sta;
    sta.name     = "StatusManager";
    sta.message  = "DevicesStatus";

    /// 定位节点
    // 车体位置
    string robotPositionValue = std::to_string(robot_pose.pose.pose.position.x) + "," +
                                std::to_string(robot_pose.pose.pose.position.y) + "," +
                                std::to_string(robot_pose.pose.pose.position.z);

    string robotOrientationValue = std::to_string(robot_pose.pose.pose.orientation.x) + "," +
                                   std::to_string(robot_pose.pose.pose.orientation.y) + "," +
                                   std::to_string(robot_pose.pose.pose.orientation.z) + "," +
                                   std::to_string(robot_pose.pose.pose.orientation.w);

    diagnostic_msgs::KeyValue robotPoseValue;
    robotPoseValue.key   = "Robot Pose Status";
    robotPoseValue.value = robotPositionValue + " " + robotOrientationValue;

    // XAvier Buffer Size
    diagnostic_msgs::KeyValue xavierStatusValue;
    xavierStatusValue.key   = "XAvier Status";
    xavierStatusValue.value = std::to_string(xavier_level_value);

    // 可见光相机
    diagnostic_msgs::KeyValue visualCameraStatusValue;
    visualCameraStatusValue.key   = "Visual Camera Status";
    visualCameraStatusValue.value = std::to_string(VISUAL_CAMERA_STATUS_CODE);
    // 红外相机
    diagnostic_msgs::KeyValue thermalCameraStatusValue;
    thermalCameraStatusValue.key   = "Thermal Camera Status";
    thermalCameraStatusValue.value = std::to_string(THERMAL_CAMERA_STATUS_CODE);

    // 充电桩
    diagnostic_msgs::KeyValue chargeStakeStatusValue;
    chargeStakeStatusValue.key   = "Charge Stake Status";
    chargeStakeStatusValue.value = CHARGE_STAKE_STATUS_CODE;
    // 自动门
    diagnostic_msgs::KeyValue doorStatusValue;
    doorStatusValue.key   = "Door Status";
    doorStatusValue.value = AUTO_DOOR_STATUS_CODE;

    // 云台
    diagnostic_msgs::KeyValue platformStatusValue;
    platformStatusValue.key   = "Platform Status";
    platformStatusValue.value = PLATFORM_HORIZONTAL_ANGLE + " " + PLATFORM_VERTICAL_ANGLE;

    // 测距超声波
    diagnostic_msgs::KeyValue ultrasonicDisLeftFrontStatusValue;
    ultrasonicDisLeftFrontStatusValue.key   = "Ultrasonic Dis Left Front Status";
    ultrasonicDisLeftFrontStatusValue.value = ULTRASONIC_DIS_LEFT_FRONT_VALUE;

    diagnostic_msgs::KeyValue ultrasonicDisRightFrontStatusValue;
    ultrasonicDisRightFrontStatusValue.key   = "Ultrasonic Dis Right Front Status";
    ultrasonicDisRightFrontStatusValue.value = ULTRASONIC_DIS_RIGHT_FRONT_VALUE;

    diagnostic_msgs::KeyValue ultrasonicDisLeftBackStatusValue;
    ultrasonicDisLeftBackStatusValue.key   = "Ultrasonic Dis Left Back Status";
    ultrasonicDisLeftBackStatusValue.value = ULTRASONIC_DIS_LEFT_BACK_VALUE;

    diagnostic_msgs::KeyValue ultrasonicDisRightBackStatusValue;
    ultrasonicDisRightBackStatusValue.key   = "Ultrasonic Dis Right Back Status";
    ultrasonicDisRightBackStatusValue.value = ULTRASONIC_DIS_RIGHT_BACK_VALUE;

    // 防跌落超声波
    diagnostic_msgs::KeyValue ultrasonicFallLeftFrontStatusValue;
    ultrasonicFallLeftFrontStatusValue.key   = "Ultrasonic Fall Left Front Status";
    ultrasonicFallLeftFrontStatusValue.value = ULTRASONIC_FALL_LEFT_FRONT_VALUE;

    diagnostic_msgs::KeyValue ultrasonicFallRightFrontStatusValue;
    ultrasonicFallRightFrontStatusValue.key   = "Ultrasonic Fall Right Front Status";
    ultrasonicFallRightFrontStatusValue.value = ULTRASONIC_FALL_RIGHT_FRONT_VALUE;

    diagnostic_msgs::KeyValue ultrasonicFallLeftBackStatusValue;
    ultrasonicFallLeftBackStatusValue.key   = "Ultrasonic Fall Left Back Status";
    ultrasonicFallLeftBackStatusValue.value = ULTRASONIC_FALL_LEFT_BACK_VALUE;

    diagnostic_msgs::KeyValue ultrasonicFallRightBackStatusValue;
    ultrasonicFallRightBackStatusValue.key   = "Ultrasonic Fall Right Back Status";
    ultrasonicFallRightBackStatusValue.value = ULTRASONIC_FALL_RIGHT_BACK_VALUE;

    // 防撞条
    diagnostic_msgs::KeyValue antiCollisionBarFrontStatusValue;
    antiCollisionBarFrontStatusValue.key   = "Anti Collision Bar Front Status";
    antiCollisionBarFrontStatusValue.value = ANTI_COLLISION_BAR_FRONT_STATUS_CODE;

    diagnostic_msgs::KeyValue antiCollisionBarBackStatusValue;
    antiCollisionBarBackStatusValue.key   = "Anti Collision Bar Back Status";
    antiCollisionBarBackStatusValue.value = ANTI_COLLISION_BAR_BACK_STATUS_CODE;

    // 升降台高度
    diagnostic_msgs::KeyValue liftHeightValue;
    liftHeightValue.key   = "Lift Height Value";
    liftHeightValue.value = std::to_string(LIFT_HEIGHT);

    // 里程计
    diagnostic_msgs::KeyValue odometerValue;
    odometerValue.key   = "Odometer Value";
    odometerValue.value = std::to_string(ROBOT_LINEAR_SPEED) + "," + std::to_string(ROBOT_ANGULAR_SPEED);

    // 温度
    diagnostic_msgs::KeyValue temperatureSensor1value;
    temperatureSensor1value.key   = "Temperature Sensor 1 Status";
    temperatureSensor1value.value = TEMPERATURE_SENSOR_1_VALUE;

    diagnostic_msgs::KeyValue temperatureSensor2value;
    temperatureSensor2value.key   = "Temperature Sensor 2 Status";
    temperatureSensor2value.value = TEMPERATURE_SENSOR_2_VALUE;

    diagnostic_msgs::KeyValue temperatureSensor3value;
    temperatureSensor3value.key   = "Temperature Sensor 3 Status";
    temperatureSensor3value.value = TEMPERATURE_SENSOR_3_VALUE;

    diagnostic_msgs::KeyValue temperatureSensor4value;
    temperatureSensor4value.key   = "Temperature Sensor 4 Status";
    temperatureSensor4value.value = TEMPERATURE_SENSOR_4_VALUE;

    // 电机 压、流、can通信
    // 1
    diagnostic_msgs::KeyValue robotMotor1VoltageStatusValue;
    robotMotor1VoltageStatusValue.key   = "Robot Motor 1 Voltage Status";
    robotMotor1VoltageStatusValue.value = ROBOT_MOTOR_1_VOLTAGE_VALUE;

    diagnostic_msgs::KeyValue robotMotor1ElectricityStatusValue;
    robotMotor1ElectricityStatusValue.key   = "Robot Motor 1 Electricity Status";
    robotMotor1ElectricityStatusValue.value = ROBOT_MOTOR_1_ELECTRICITY_VALUE;

    diagnostic_msgs::KeyValue robotMotor1CanSignalStatusValue;
    robotMotor1CanSignalStatusValue.key   = "Robot Motor 1 Can Signal Status";
    robotMotor1CanSignalStatusValue.value = ROBOT_MOTOR_1_CAN_SIGNAL_STATUS_CODE;
    // 2
    diagnostic_msgs::KeyValue robotMotor2VoltageStatusValue;
    robotMotor2VoltageStatusValue.key   = "Robot Motor 2 Voltage Status";
    robotMotor2VoltageStatusValue.value = ROBOT_MOTOR_2_VOLTAGE_VALUE;

    diagnostic_msgs::KeyValue robotMotor2ElectricityStatusValue;
    robotMotor2ElectricityStatusValue.key   = "Robot Motor 2 Electricity Status";
    robotMotor2ElectricityStatusValue.value = ROBOT_MOTOR_2_ELECTRICITY_VALUE;

    diagnostic_msgs::KeyValue robotMotor2CanSignalStatusValue;
    robotMotor2CanSignalStatusValue.key   = "Robot Motor 2 Can Signal Status";
    robotMotor2CanSignalStatusValue.value = ROBOT_MOTOR_2_CAN_SIGNAL_STATUS_CODE;
    // 3
    diagnostic_msgs::KeyValue robotMotor3VoltageStatusValue;
    robotMotor3VoltageStatusValue.key   = "Robot Motor 3 Voltage Status";
    robotMotor3VoltageStatusValue.value = ROBOT_MOTOR_3_VOLTAGE_VALUE;

    diagnostic_msgs::KeyValue robotMotor3ElectricityStatusValue;
    robotMotor3ElectricityStatusValue.key   = "Robot Motor 3 Electricity Status";
    robotMotor3ElectricityStatusValue.value = ROBOT_MOTOR_3_ELECTRICITY_VALUE;

    diagnostic_msgs::KeyValue robotMotor3CanSignalStatusValue;
    robotMotor3CanSignalStatusValue.key   = "Robot Motor 3 Can Signal Status";
    robotMotor3CanSignalStatusValue.value = ROBOT_MOTOR_3_CAN_SIGNAL_STATUS_CODE;
    // 4
    diagnostic_msgs::KeyValue robotMotor4VoltageStatusValue;
    robotMotor4VoltageStatusValue.key   = "Robot Motor 4 Voltage Status";
    robotMotor4VoltageStatusValue.value = ROBOT_MOTOR_4_VOLTAGE_VALUE;

    diagnostic_msgs::KeyValue robotMotor4ElectricityStatusValue;
    robotMotor4ElectricityStatusValue.key   = "Robot Motor 4 Electricity Status";
    robotMotor4ElectricityStatusValue.value = ROBOT_MOTOR_4_ELECTRICITY_VALUE;

    diagnostic_msgs::KeyValue robotMotor4CanSignalStatusValue;
    robotMotor4CanSignalStatusValue.key   = "Robot Motor 4 Can Signal Status";
    robotMotor4CanSignalStatusValue.value = ROBOT_MOTOR_4_CAN_SIGNAL_STATUS_CODE;
    // 5
    diagnostic_msgs::KeyValue robotMotor5VoltageStatusValue;
    robotMotor5VoltageStatusValue.key   = "Robot Motor 5 Voltage Status";
    robotMotor5VoltageStatusValue.value = ROBOT_MOTOR_5_VOLTAGE_VALUE;

    diagnostic_msgs::KeyValue robotMotor5ElectricityStatusValue;
    robotMotor5ElectricityStatusValue.key   = "Robot Motor 5 Electricity Status";
    robotMotor5ElectricityStatusValue.value = ROBOT_MOTOR_5_ELECTRICITY_VALUE;

    diagnostic_msgs::KeyValue robotMotor5CanSignalStatusValue;
    robotMotor5CanSignalStatusValue.key   = "Robot Motor 5 Can Signal Status";
    robotMotor5CanSignalStatusValue.value = ROBOT_MOTOR_5_CAN_SIGNAL_STATUS_CODE;
    // 6
    diagnostic_msgs::KeyValue robotMotor6VoltageStatusValue;
    robotMotor6VoltageStatusValue.key   = "Robot Motor 6 Voltage Status";
    robotMotor6VoltageStatusValue.value = ROBOT_MOTOR_6_VOLTAGE_VALUE;

    diagnostic_msgs::KeyValue robotMotor6ElectricityStatusValue;
    robotMotor6ElectricityStatusValue.key   = "Robot Motor 6 Electricity Status";
    robotMotor6ElectricityStatusValue.value = ROBOT_MOTOR_6_ELECTRICITY_VALUE;

    diagnostic_msgs::KeyValue robotMotor6CanSignalStatusValue;
    robotMotor6CanSignalStatusValue.key   = "Robot Motor 6 Can Signal Status";
    robotMotor6CanSignalStatusValue.value = ROBOT_MOTOR_6_CAN_SIGNAL_STATUS_CODE;
    // 7
    diagnostic_msgs::KeyValue robotMotor7VoltageStatusValue;
    robotMotor7VoltageStatusValue.key   = "Robot Motor 7 Voltage Status";
    robotMotor7VoltageStatusValue.value = ROBOT_MOTOR_7_VOLTAGE_VALUE;

    diagnostic_msgs::KeyValue robotMotor7ElectricityStatusValue;
    robotMotor7ElectricityStatusValue.key   = "Robot Motor 7 Electricity Status";
    robotMotor7ElectricityStatusValue.value = ROBOT_MOTOR_7_ELECTRICITY_VALUE;

    diagnostic_msgs::KeyValue robotMotor7CanSignalStatusValue;
    robotMotor7CanSignalStatusValue.key   = "Robot Motor 7 Can Signal Status";
    robotMotor7CanSignalStatusValue.value = ROBOT_MOTOR_7_CAN_SIGNAL_STATUS_CODE;
    // 8
    diagnostic_msgs::KeyValue robotMotor8VoltageStatusValue;
    robotMotor8VoltageStatusValue.key   = "Robot Motor 8 Voltage Status";
    robotMotor8VoltageStatusValue.value = ROBOT_MOTOR_8_VOLTAGE_VALUE;

    diagnostic_msgs::KeyValue robotMotor8ElectricityStatusValue;
    robotMotor8ElectricityStatusValue.key   = "Robot Motor 8 Electricity Status";
    robotMotor8ElectricityStatusValue.value = ROBOT_MOTOR_8_ELECTRICITY_VALUE;

    diagnostic_msgs::KeyValue robotMotor8CanSignalStatusValue;
    robotMotor8CanSignalStatusValue.key   = "Robot Motor 8 Can Signal Status";
    robotMotor8CanSignalStatusValue.value = ROBOT_MOTOR_8_CAN_SIGNAL_STATUS_CODE;

    // 电池电压(节)
    // 1
    diagnostic_msgs::KeyValue batteryPart1VoltageStatusValue;
    batteryPart1VoltageStatusValue.key   = "Battery Part 1 Voltage";
    batteryPart1VoltageStatusValue.value = BATTERY_PART_1_VOLTAGE_VALUE;
    // 2
    diagnostic_msgs::KeyValue batteryPart2VoltageStatusValue;
    batteryPart2VoltageStatusValue.key   = "Battery Part 2 Voltage";
    batteryPart2VoltageStatusValue.value = BATTERY_PART_2_VOLTAGE_VALUE;
    // 3
    diagnostic_msgs::KeyValue batteryPart3VoltageStatusValue;
    batteryPart3VoltageStatusValue.key   = "Battery Part 3 Voltage";
    batteryPart3VoltageStatusValue.value = BATTERY_PART_3_VOLTAGE_VALUE;
    // 4
    diagnostic_msgs::KeyValue batteryPart4VoltageStatusValue;
    batteryPart4VoltageStatusValue.key   = "Battery Part 4 Voltage";
    batteryPart4VoltageStatusValue.value = BATTERY_PART_4_VOLTAGE_VALUE;
    // 5
    diagnostic_msgs::KeyValue batteryPart5VoltageStatusValue;
    batteryPart5VoltageStatusValue.key   = "Battery Part 5 Voltage";
    batteryPart5VoltageStatusValue.value = BATTERY_PART_5_VOLTAGE_VALUE;
    // 6
    diagnostic_msgs::KeyValue batteryPart6VoltageStatusValue;
    batteryPart6VoltageStatusValue.key   = "Battery Part 6 Voltage";
    batteryPart6VoltageStatusValue.value = BATTERY_PART_6_VOLTAGE_VALUE;
    // 7
    diagnostic_msgs::KeyValue batteryPart7VoltageStatusValue;
    batteryPart7VoltageStatusValue.key   = "Battery Part 7 Voltage";
    batteryPart7VoltageStatusValue.value = BATTERY_PART_7_VOLTAGE_VALUE;
    // 8
    diagnostic_msgs::KeyValue batteryPart8VoltageStatusValue;
    batteryPart8VoltageStatusValue.key   = "Battery Part 8 Voltage";
    batteryPart8VoltageStatusValue.value = BATTERY_PART_8_VOLTAGE_VALUE;
    // 9
    diagnostic_msgs::KeyValue batteryPart9VoltageStatusValue;
    batteryPart9VoltageStatusValue.key   = "Battery Part 9 Voltage";
    batteryPart9VoltageStatusValue.value = BATTERY_PART_9_VOLTAGE_VALUE;
    // 10
    diagnostic_msgs::KeyValue batteryPart10VoltageStatusValue;
    batteryPart10VoltageStatusValue.key   = "Battery Part 10 Voltage";
    batteryPart10VoltageStatusValue.value = BATTERY_PART_10_VOLTAGE_VALUE;
    // 11
    diagnostic_msgs::KeyValue batteryPart11VoltageStatusValue;
    batteryPart11VoltageStatusValue.key   = "Battery Part 11 Voltage";
    batteryPart11VoltageStatusValue.value = BATTERY_PART_11_VOLTAGE_VALUE;
    // 12
    diagnostic_msgs::KeyValue batteryPart12VoltageStatusValue;
    batteryPart12VoltageStatusValue.key   = "Battery Part 12 Voltage";
    batteryPart12VoltageStatusValue.value = BATTERY_PART_12_VOLTAGE_VALUE;
    // 13
    diagnostic_msgs::KeyValue batteryPart13VoltageStatusValue;
    batteryPart13VoltageStatusValue.key   = "Battery Part 13 Voltage";
    batteryPart13VoltageStatusValue.value = BATTERY_PART_13_VOLTAGE_VALUE;
    // 电池温度
    // 1
    diagnostic_msgs::KeyValue batteryTemperature1StatusValue;
    batteryTemperature1StatusValue.key   = "Battery Temperature 1 Value";
    batteryTemperature1StatusValue.value = BATTERY_TEMPERATURE_1_VALUE;
    // 2
    diagnostic_msgs::KeyValue batteryTemperature2StatusValue;
    batteryTemperature2StatusValue.key   = "Battery Temperature 2 Value";
    batteryTemperature2StatusValue.value = BATTERY_TEMPERATURE_2_VALUE;
    // 3
    diagnostic_msgs::KeyValue batteryTemperature3StatusValue;
    batteryTemperature3StatusValue.key   = "Battery Temperature 3 Value";
    batteryTemperature3StatusValue.value = BATTERY_TEMPERATURE_3_VALUE;
    // 4
    diagnostic_msgs::KeyValue batteryTemperature4StatusValue;
    batteryTemperature4StatusValue.key   = "Battery Temperature 4 Value";
    batteryTemperature4StatusValue.value = BATTERY_TEMPERATURE_4_VALUE;
    // 5
    diagnostic_msgs::KeyValue batteryTemperature5StatusValue;
    batteryTemperature5StatusValue.key   = "Battery Temperature 5 Value";
    batteryTemperature5StatusValue.value = BATTERY_TEMPERATURE_5_VALUE;
    // 电池报警相关
    // 过压
    diagnostic_msgs::KeyValue batteryHighVoltageAlarmCodeValue;
    batteryHighVoltageAlarmCodeValue.key   = "Battery High Voltage Alarm Code";
    batteryHighVoltageAlarmCodeValue.value = BATTERY_HIGH_VOLTAGE_ALARM_CODE;
    // 欠压
    diagnostic_msgs::KeyValue batteryLowVoltageAlarmCodeValue;
    batteryLowVoltageAlarmCodeValue.key   = "Battery Low Voltage Alarm Code";
    batteryLowVoltageAlarmCodeValue.value = BATTERY_LOW_VOLTAGE_ALARM_CODE;
    // 过流
    diagnostic_msgs::KeyValue batteryOverCurrentAlarmCodeValue;
    batteryOverCurrentAlarmCodeValue.key   = "Battery Over Current Alarm Code";
    batteryOverCurrentAlarmCodeValue.value = BATTERY_OVER_CURRENT_ALARM_CODE;
    // 高温
    diagnostic_msgs::KeyValue batteryHighTemperatureAlarmCodeValue;
    batteryHighTemperatureAlarmCodeValue.key   = "Battery High Temperature Alarm Code";
    batteryHighTemperatureAlarmCodeValue.value = BATTERY_HIGH_TEMPERATURE_ALARM_CODE;

    // 电池 充、放 电流
    diagnostic_msgs::KeyValue batteryChargeDischargeElectricityStatusValue;
    batteryChargeDischargeElectricityStatusValue.key   = "Battery Charge Discharge Electricity";
    batteryChargeDischargeElectricityStatusValue.value = BATTERY_CHARGE_DISCHARGE_ELECTRICITY_VALUE;
    // 电量
    diagnostic_msgs::KeyValue batteryVolumeStatusValue;
    batteryVolumeStatusValue.key   = "Battery Volume";
    batteryVolumeStatusValue.value = BATTERY_VOLUME_VALUE;
    // 电池循环次数
    diagnostic_msgs::KeyValue batteryRecycleNumStatusValue;
    batteryRecycleNumStatusValue.key   = "Battery Recycle Num";
    batteryRecycleNumStatusValue.value = BATTERY_RECYCLE_NUM_VALUE;
    // 电量百分比
    diagnostic_msgs::KeyValue batteryVolumeRatioValue;
    batteryVolumeRatioValue.key   = "Battery Ratio";
    batteryVolumeRatioValue.value = BATTERY_VOLUME_RATIO;
    // 无线充电故障信息
    diagnostic_msgs::KeyValue wirelessChargeAlarmCode;
    wirelessChargeAlarmCode.key   = "Wireless Charge Alarm Code";
    wirelessChargeAlarmCode.value = WIRELESS_CHARGE_ALARM_CODE;
    // 充、放电 MOS
    // 充电
    diagnostic_msgs::KeyValue batteryChargeMosStatusValue;
    batteryChargeMosStatusValue.key   = "Battery Charge Mos Status";
    batteryChargeMosStatusValue.value = BATTERY_CHARGE_MOS_STATUS_CODE;
    // 放电
    diagnostic_msgs::KeyValue batteryDischargeMosStatusValue;
    batteryDischargeMosStatusValue.key   = "Battery Discharge Mos Status";
    batteryDischargeMosStatusValue.value = BATTERY_DISCHARGE_MOS_STATUS_CODE;
    // ADC电压检测
    // 1
    diagnostic_msgs::KeyValue adc1VoltageStatusValue;
    adc1VoltageStatusValue.key   = "ADC1 Voltage Status";
    adc1VoltageStatusValue.value = ADC1_VOLTAGE_VALUE;
    // 2
    diagnostic_msgs::KeyValue adc2VoltageStatusValue;
    adc2VoltageStatusValue.key   = "ADC2 Voltage Status";
    adc2VoltageStatusValue.value = ADC2_VOLTAGE_VALUE;
    // 3
    diagnostic_msgs::KeyValue adc3VoltageStatusValue;
    adc3VoltageStatusValue.key   = "ADC3 Voltage Status";
    adc3VoltageStatusValue.value = ADC3_VOLTAGE_VALUE;
    // 4
    diagnostic_msgs::KeyValue adc4VoltageStatusValue;
    adc4VoltageStatusValue.key   = "ADC4 Voltage Status";
    adc4VoltageStatusValue.value = ADC4_VOLTAGE_VALUE;

    // 车子RPY
    // R
    diagnostic_msgs::KeyValue robotRollAngleStatusValue;
    robotRollAngleStatusValue.key   = "Robot Roll Angle";
    robotRollAngleStatusValue.value = ROBOT_ROLL_ANGLE_VALUE;
    // P
    diagnostic_msgs::KeyValue robotPitchAngleStatusValue;
    robotPitchAngleStatusValue.key   = "Robot Pitch Angle";
    robotPitchAngleStatusValue.value = ROBOT_PITCH_ANGLE_VALUE;
    // Y
    diagnostic_msgs::KeyValue robotYawAngleStatusValue;
    robotYawAngleStatusValue.key   = "Robot Yaw Angle";
    robotYawAngleStatusValue.value = ROBOT_YAW_ANGLE_VALUE;
    // Time
    diagnostic_msgs::KeyValue robotTimeValue;
    robotTimeValue.key   = "Robot Start Time";
    robotTimeValue.value = ROBOT_START_TIME;

    // Publish
    sta.values.push_back(robotPoseValue);
    sta.values.push_back(xavierStatusValue);

    sta.values.push_back(visualCameraStatusValue);
    sta.values.push_back(thermalCameraStatusValue);

    sta.values.push_back(chargeStakeStatusValue);
    sta.values.push_back(doorStatusValue);

    sta.values.push_back(platformStatusValue);

    sta.values.push_back(ultrasonicDisLeftFrontStatusValue);
    sta.values.push_back(ultrasonicDisRightFrontStatusValue);
    sta.values.push_back(ultrasonicDisLeftBackStatusValue);
    sta.values.push_back(ultrasonicDisRightBackStatusValue);

    sta.values.push_back(ultrasonicFallLeftFrontStatusValue);
    sta.values.push_back(ultrasonicFallRightFrontStatusValue);
    sta.values.push_back(ultrasonicFallLeftBackStatusValue);
    sta.values.push_back(ultrasonicFallRightBackStatusValue);

    sta.values.push_back(antiCollisionBarFrontStatusValue);
    sta.values.push_back(antiCollisionBarBackStatusValue);

    sta.values.push_back(liftHeightValue);
    sta.values.push_back(odometerValue);

    sta.values.push_back(temperatureSensor1value);
    sta.values.push_back(temperatureSensor2value);
    sta.values.push_back(temperatureSensor3value);
    sta.values.push_back(temperatureSensor4value);

    sta.values.push_back(robotMotor1VoltageStatusValue);
    sta.values.push_back(robotMotor1ElectricityStatusValue);
    sta.values.push_back(robotMotor1CanSignalStatusValue);
    sta.values.push_back(robotMotor2VoltageStatusValue);
    sta.values.push_back(robotMotor2ElectricityStatusValue);
    sta.values.push_back(robotMotor2CanSignalStatusValue);
    sta.values.push_back(robotMotor3VoltageStatusValue);
    sta.values.push_back(robotMotor3ElectricityStatusValue);
    sta.values.push_back(robotMotor3CanSignalStatusValue);
    sta.values.push_back(robotMotor4VoltageStatusValue);
    sta.values.push_back(robotMotor4ElectricityStatusValue);
    sta.values.push_back(robotMotor4CanSignalStatusValue);
    sta.values.push_back(robotMotor5VoltageStatusValue);
    sta.values.push_back(robotMotor5ElectricityStatusValue);
    sta.values.push_back(robotMotor5CanSignalStatusValue);
    sta.values.push_back(robotMotor6VoltageStatusValue);
    sta.values.push_back(robotMotor6ElectricityStatusValue);
    sta.values.push_back(robotMotor6CanSignalStatusValue);
    sta.values.push_back(robotMotor7VoltageStatusValue);
    sta.values.push_back(robotMotor7ElectricityStatusValue);
    sta.values.push_back(robotMotor7CanSignalStatusValue);
    sta.values.push_back(robotMotor8VoltageStatusValue);
    sta.values.push_back(robotMotor8ElectricityStatusValue);
    sta.values.push_back(robotMotor8CanSignalStatusValue);

    sta.values.push_back(batteryPart1VoltageStatusValue);
    sta.values.push_back(batteryPart2VoltageStatusValue);
    sta.values.push_back(batteryPart3VoltageStatusValue);
    sta.values.push_back(batteryPart4VoltageStatusValue);
    sta.values.push_back(batteryPart5VoltageStatusValue);
    sta.values.push_back(batteryPart6VoltageStatusValue);
    sta.values.push_back(batteryPart7VoltageStatusValue);
    sta.values.push_back(batteryPart8VoltageStatusValue);
    sta.values.push_back(batteryPart9VoltageStatusValue);
    sta.values.push_back(batteryPart10VoltageStatusValue);
    sta.values.push_back(batteryPart11VoltageStatusValue);
    sta.values.push_back(batteryPart12VoltageStatusValue);
    sta.values.push_back(batteryPart13VoltageStatusValue);

    sta.values.push_back(batteryTemperature1StatusValue);
    sta.values.push_back(batteryTemperature2StatusValue);
    sta.values.push_back(batteryTemperature3StatusValue);
    sta.values.push_back(batteryTemperature4StatusValue);
    sta.values.push_back(batteryTemperature5StatusValue);

    sta.values.push_back(batteryHighVoltageAlarmCodeValue);
    sta.values.push_back(batteryLowVoltageAlarmCodeValue);
    sta.values.push_back(batteryOverCurrentAlarmCodeValue);
    sta.values.push_back(batteryHighTemperatureAlarmCodeValue);

    sta.values.push_back(batteryChargeDischargeElectricityStatusValue);
    sta.values.push_back(batteryVolumeStatusValue);
    sta.values.push_back(batteryRecycleNumStatusValue);
    sta.values.push_back(batteryVolumeRatioValue);

    sta.values.push_back(wirelessChargeAlarmCode);

    sta.values.push_back(batteryChargeMosStatusValue);
    sta.values.push_back(batteryDischargeMosStatusValue);

    sta.values.push_back(adc1VoltageStatusValue);
    sta.values.push_back(adc2VoltageStatusValue);
    sta.values.push_back(adc3VoltageStatusValue);
    sta.values.push_back(adc4VoltageStatusValue);

    sta.values.push_back(robotRollAngleStatusValue);
    sta.values.push_back(robotPitchAngleStatusValue);
    sta.values.push_back(robotYawAngleStatusValue);
    sta.values.push_back(robotTimeValue);

    statusMsg.status.push_back(sta);
    statusManagerPub.publish(statusMsg);
}

// Destructor
YD_STATUS_MANAGER::~YD_STATUS_MANAGER()
{
    // to do sth.
}

#endif
