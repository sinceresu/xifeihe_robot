#ifndef POINT_CLOUD_MESSAGE_FILTERS_HPP
#define POINT_CLOUD_MESSAGE_FILTERS_HPP

#include "ros/ros.h"

#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

/// 消息类
#include "nav_msgs/Odometry.h"
#include "sensor_msgs/Image.h"
#include "sensor_msgs/CompressedImage.h"
#include "param_server/server.h"
#include "yidamsg/pointcloud_color.h"

using namespace ros;
using namespace std;
using namespace boost;
using namespace message_filters;

class MessageFilters
{
public:
    param_server::Server* server;
    int device_id=1,version=1;
    int frequency;
    ros::Time last;
    // Topic Name
    string message_sync_data_pub_topic_name;

    string robot_pose_sub_topic_name;
    string platform_pose_sub_topic_name;

    string visible_image_sub_topic_name;
    string infrared_image_sub_topic_name;

    string temperature_sub_topic_name;

    // Publisher & Subscriber
    Publisher pointCloudSyncDataPub;

    // 点云着色同步
    message_filters::Subscriber<nav_msgs::Odometry> *synPoseSub;
    message_filters::Subscriber<nav_msgs::Odometry> *platformPoseSub;

    message_filters::Subscriber<sensor_msgs::Image> *visibleImageSub;
    message_filters::Subscriber<sensor_msgs::Image> *infraredImageSub;

    message_filters::Subscriber<sensor_msgs::Image> *temperatureSub;

    // SynPolicy
    typedef sync_policies::ApproximateTime<nav_msgs::Odometry,
                                           nav_msgs::Odometry,
                                           sensor_msgs::Image>
        pointCloudDataSyncPolicy;
    Synchronizer<pointCloudDataSyncPolicy> *messageSync;

    // 点云着色同步2
    message_filters::Subscriber<nav_msgs::Odometry> *_platformPoseSub;

    message_filters::Subscriber<sensor_msgs::Image> *_visibleImageSub;
    message_filters::Subscriber<sensor_msgs::Image> *_infraredImageSub;

    message_filters::Subscriber<sensor_msgs::Image> *_temperatureSub;

    typedef sync_policies::ApproximateTime<nav_msgs::Odometry,
                                           sensor_msgs::Image>
        _pointCloudDataSyncPolicy;
    Synchronizer<_pointCloudDataSyncPolicy> *_messageSync;

    // Constructor & Destructor
    MessageFilters(NodeHandle n);
    ~MessageFilters();
    void configCallback(param_server::SimpleType &config)
    {
        readConfig();
    }
    void readConfig(){
        if(server->exist("version")){
            server->get("version",version);
        }
        if(server->exist("device_id")){
            server->get("device_id",device_id);
        }
        if(server->exist("frequency")){
            server->get("frequency",frequency);
        }
        ROS_INFO("version:%i device_id:%i  frequency:%i",version,device_id,frequency);
    }

    static void messageSyncPolicyCallback(const nav_msgs::Odometry::ConstPtr &poseMsg,
                                          const nav_msgs::Odometry::ConstPtr &platformPoseMsg,
                                          const sensor_msgs::ImageConstPtr &temperatureMsg);

    static void _messageSyncPolicyCallback(const nav_msgs::Odometry::ConstPtr &platformPoseMsg,
                                           const sensor_msgs::ImageConstPtr &temperatureMsg);
};

// Constructor
MessageFilters::MessageFilters(NodeHandle n)
{
    ROS_INFO("MessageFilters() Constructor");
    //package config
    server = new param_server::Server("yd_status_manager","cfg/robot.yml");
    param_server::CallbackType f = boost::bind(&MessageFilters::configCallback,this,_1); //绑定回调函数
    server->setCallback(f);
    readConfig();
    ros::param::get("/yd_status_manager/message_sync_data_pub_topic_name", message_sync_data_pub_topic_name);

    ros::param::get("/yd_status_manager/robot_pose_sub_topic_name", robot_pose_sub_topic_name);
    ros::param::get("/yd_status_manager/platform_pose_sub_topic_name", platform_pose_sub_topic_name);

    ros::param::get("/yd_status_manager/visible_image_sub_topic_name", visible_image_sub_topic_name);
    ros::param::get("/yd_status_manager/infrared_image_sub_topic_name", infrared_image_sub_topic_name);

    ros::param::get("/yd_status_manager/temperature_sub_topic_name", temperature_sub_topic_name);

    pointCloudSyncDataPub = n.advertise<yidamsg::pointcloud_color>(message_sync_data_pub_topic_name, 1);

    synPoseSub = new message_filters::Subscriber<nav_msgs::Odometry>(n, robot_pose_sub_topic_name, 1);
    platformPoseSub = new message_filters::Subscriber<nav_msgs::Odometry>(n, platform_pose_sub_topic_name, 1);

    // visibleImageSub = new message_filters::Subscriber<sensor_msgs::Image>(n, visible_image_sub_topic_name, 1);
    // infraredImageSub = new message_filters::Subscriber<sensor_msgs::Image>(n, infrared_image_sub_topic_name, 1);

    temperatureSub = new message_filters::Subscriber<sensor_msgs::Image>(n, temperature_sub_topic_name, 1);

    messageSync = new Synchronizer<pointCloudDataSyncPolicy>(pointCloudDataSyncPolicy(10),
                                                             *synPoseSub,
                                                             *platformPoseSub,
                                                             *temperatureSub);
    messageSync->registerCallback(bind(&messageSyncPolicyCallback, _1, _2, _3));

    // 点云着色同步2
    /* _platformPoseSub = new message_filters::Subscriber<nav_msgs::Odometry>(n, platform_pose_sub_topic_name, 1);

    _visibleImageSub = new message_filters::Subscriber<sensor_msgs::Image>(n, visible_image_sub_topic_name, 1);
    _infraredImageSub = new message_filters::Subscriber<sensor_msgs::Image>(n, infrared_image_sub_topic_name, 1);

    _temperatureSub = new message_filters::Subscriber<sensor_msgs::Image>(n, temperature_sub_topic_name, 1);

    _messageSync = new Synchronizer<_pointCloudDataSyncPolicy>(_pointCloudDataSyncPolicy(20),
                                                              *_platformPoseSub,
                                                              *_temperatureSub);
    _messageSync->registerCallback(bind(&_messageSyncPolicyCallback, _1, _2)); */
};

// Destructor
MessageFilters::~MessageFilters()
{
    // to do sth.
}

#endif
