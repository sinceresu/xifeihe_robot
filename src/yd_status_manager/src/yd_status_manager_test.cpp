/// 工具类
#include "boost/date_time/posix_time/posix_time.hpp"

#include "ros/ros.h"
#include "stdio.h"
#include "time.h"

#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <termios.h>

#include <thread>

#include <fstream>
#include <iostream>
#include <sstream>
#include <cstring>

#include <chrono>
#include <ratio>
#include <vector>

/// Boost
#include <boost/regex.hpp>
#include <boost/filesystem.hpp>
#include "boost/bind.hpp"
#include "boost/thread/mutex.hpp"
#include "boost/thread/thread.hpp"

/// OpenCV
#include <opencv2/opencv.hpp>
#include <cv_bridge/cv_bridge.h>

// 消息类
/// Msg
#include "std_msgs/Bool.h"
#include "std_msgs/UInt8.h"
#include "std_msgs/Int32.h"
#include "std_msgs/String.h"

#include "nav_msgs/Odometry.h"
#include "sensor_msgs/Image.h"
#include "sensor_msgs/CompressedImage.h"

#include <diagnostic_msgs/DiagnosticArray.h>
#include <diagnostic_msgs/DiagnosticStatus.h>
#include <diagnostic_msgs/KeyValue.h>

using namespace cv;
using namespace std;

/// Variables
char STRING_CHARS[50];
typedef unsigned char byte;

/// Publisher
// 节点状态
ros::Publisher cameraManagerStatusPub;
ros::Publisher homeManagerStatusPub;
ros::Publisher localizationManagerStatusPub;

ros::Publisher fuzzyControllerStatusPub;
ros::Publisher platformControllerStatusPub;
// 点云着色同步
ros::Publisher robotPosePub;
ros::Publisher platformPosePub;
ros::Publisher visualImagePub;
ros::Publisher thermalImagePub;

// 发送各个节点状态测试数据消息
void sendNodeStatusTestDatas()
{
    /// 相机节点
    // 可见光相机
    diagnostic_msgs::DiagnosticArray v_camera_sta_msg;
    v_camera_sta_msg.header.stamp    = ros::Time::now();
    v_camera_sta_msg.header.frame_id = "visual";

    diagnostic_msgs::DiagnosticStatus v_camera_sta;
    v_camera_sta.level       = diagnostic_msgs::DiagnosticStatus::OK;
    v_camera_sta.name        = "192.168.0.108";
    v_camera_sta.message     = "正常";
    v_camera_sta.hardware_id = "1";

    diagnostic_msgs::KeyValue v_camera_values;
    v_camera_values.key   = "Visual Camera Values";
    v_camera_values.value = "xxx(焦距)/xxx(光圈)/xxx(分辨率)";

    v_camera_sta.values.push_back(v_camera_values);
    v_camera_sta_msg.status.push_back(v_camera_sta);

    // 红外相机
    diagnostic_msgs::DiagnosticArray t_camera_sta_msg;
    t_camera_sta_msg.header.stamp    = ros::Time::now();
    t_camera_sta_msg.header.frame_id = "thermal";

    diagnostic_msgs::DiagnosticStatus t_camera_sta;
    t_camera_sta.level       = diagnostic_msgs::DiagnosticStatus::WARN;
    t_camera_sta.name        = "192.168.0.110";
    t_camera_sta.message     = "延迟";
    t_camera_sta.hardware_id = "2";

    diagnostic_msgs::KeyValue t_camera_values;
    t_camera_values.key   = "Thermal Camera Values";
    t_camera_values.value = "xxx(焦距)/xxx(光圈)/xxx(分辨率)";

    t_camera_sta.values.push_back(t_camera_values);
    t_camera_sta_msg.status.push_back(t_camera_sta);

    /// 狗窝节点
    // 充电桩
    diagnostic_msgs::DiagnosticArray charge_stake_sta_msg;
    charge_stake_sta_msg.header.stamp    = ros::Time::now();
    charge_stake_sta_msg.header.frame_id = "charge_stake";

    diagnostic_msgs::DiagnosticStatus charge_stake_sta;
    charge_stake_sta.level       = diagnostic_msgs::DiagnosticStatus::OK;
    charge_stake_sta.name        = "charge_stake_state";
    charge_stake_sta.message     = "1";
    charge_stake_sta.hardware_id = "328565";

    charge_stake_sta_msg.status.push_back(charge_stake_sta);

    // 自动门
    diagnostic_msgs::DiagnosticArray door_sta_msg;
    door_sta_msg.header.stamp    = ros::Time::now();
    door_sta_msg.header.frame_id = "door";

    diagnostic_msgs::DiagnosticStatus door_sta;
    door_sta.level       = diagnostic_msgs::DiagnosticStatus::OK;
    door_sta.name        = "door_state";
    door_sta.message     = "0";
    door_sta.hardware_id = "DORMA ES200E";

    door_sta_msg.status.push_back(door_sta);

    /// 运动控制节点
    diagnostic_msgs::DiagnosticArray fuzzy_controller_sta_msg;
    fuzzy_controller_sta_msg.header.stamp = ros::Time::now();

    diagnostic_msgs::DiagnosticStatus fuzzy_controller_sta;
    fuzzy_controller_sta.level       = diagnostic_msgs::DiagnosticStatus::OK;
    fuzzy_controller_sta.name        = "robot_sensors_chassis_info";
    fuzzy_controller_sta.hardware_id = "1";

    // 传感器
    // 测距超声波
    diagnostic_msgs::KeyValue ultrasonic_dis_values;
    ultrasonic_dis_values.key   = "ultrasonic_dis";
    ultrasonic_dis_values.value = "测距值";
    // (左/右)防跌落超声波
    diagnostic_msgs::KeyValue ultrasonic_fall_left_values;
    ultrasonic_fall_left_values.key   = "ultrasonic_fall_left";
    ultrasonic_fall_left_values.value = "测距值";

    diagnostic_msgs::KeyValue ultrasonic_fall_right_values;
    ultrasonic_fall_right_values.key   = "ultrasonic_fall_right";
    ultrasonic_fall_right_values.value = "测距值";
    // 防撞条
    diagnostic_msgs::KeyValue anti_collision_bar_values;
    anti_collision_bar_values.key   = "anti_collision_bar";
    anti_collision_bar_values.value = "0";

    // 底盘
    // 电压
    diagnostic_msgs::KeyValue battery_voltage_values;
    battery_voltage_values.key   = "battery_voltage";
    battery_voltage_values.value = "31.3903";
    // 电流
    diagnostic_msgs::KeyValue battery_electricity_values;
    battery_electricity_values.key   = "battery_electricity";
    battery_electricity_values.value = "3.2";
    // 电量
    diagnostic_msgs::KeyValue battery_volume_values;
    battery_volume_values.key   = "battery_volume";
    battery_volume_values.value = "40.4";
    // 电量
    diagnostic_msgs::KeyValue robot_motor_rev_values;
    robot_motor_rev_values.key   = "robot_motor_rev";
    robot_motor_rev_values.value = "500";

    fuzzy_controller_sta.values.push_back(ultrasonic_dis_values);
    fuzzy_controller_sta.values.push_back(ultrasonic_fall_left_values);
    fuzzy_controller_sta.values.push_back(ultrasonic_fall_right_values);
    fuzzy_controller_sta.values.push_back(anti_collision_bar_values);

    fuzzy_controller_sta.values.push_back(battery_voltage_values);
    fuzzy_controller_sta.values.push_back(battery_electricity_values);
    fuzzy_controller_sta.values.push_back(battery_volume_values);
    fuzzy_controller_sta.values.push_back(robot_motor_rev_values);

    fuzzy_controller_sta_msg.status.push_back(fuzzy_controller_sta);

    /// 云台控制节点
    diagnostic_msgs::DiagnosticArray platform_controller_sta_msg;
    platform_controller_sta_msg.header.stamp = ros::Time::now();

    diagnostic_msgs::DiagnosticStatus platform_controller_sta;
    platform_controller_sta.level       = diagnostic_msgs::DiagnosticStatus::OK;
    platform_controller_sta.name        = "platform_anglej";
    platform_controller_sta.message     = "";
    platform_controller_sta.hardware_id = "YZ-BY002W";

    platform_controller_sta_msg.status.push_back(platform_controller_sta);

    /// Publish
    cameraManagerStatusPub.publish(v_camera_sta_msg);
    cameraManagerStatusPub.publish(t_camera_sta_msg);

    homeManagerStatusPub.publish(charge_stake_sta_msg);
    homeManagerStatusPub.publish(door_sta_msg);

    fuzzyControllerStatusPub.publish(fuzzy_controller_sta_msg);
    platformControllerStatusPub.publish(platform_controller_sta_msg);
}

// 发送点云数据同步着色测试数据消息
void sendMessageSyncTestDatas()
{
    /// 车体位置、位姿
    nav_msgs::Odometry robotPoseMsg;
    robotPoseMsg.pose.pose.position.x = 1.0;
    robotPoseMsg.pose.pose.position.y = 2.0;
    robotPoseMsg.pose.pose.position.z = 3.0;

    robotPoseMsg.pose.pose.orientation.x = 1.0;
    robotPoseMsg.pose.pose.orientation.y = 2.0;
    robotPoseMsg.pose.pose.orientation.z = 3.0;
    robotPoseMsg.pose.pose.orientation.w = 4.0;

    /// 点云位置
    nav_msgs::Odometry platformPoseMsg;
    platformPoseMsg.pose.pose.position.x = 1.0;
    platformPoseMsg.pose.pose.position.y = 2.0;

    /// 可见光、红外图片
    // 可见光
    Mat v_image_m = imread("../include/yd_status_manager/images/v.jpg", 1);
    sensor_msgs::ImagePtr v_image_msg_ptr = cv_bridge::CvImage(std_msgs::Header(), "bgr8", v_image_m).toImageMsg();
    // 红外
    Mat t_image_m = imread("../include/yd_status_manager/images/t.jpg", 1);
}

// StatusManagerTest主线程
void statusManagerTest()
{
		ROS_INFO(">>>>>: statusManagerTest()");

    time_t last_t = time(NULL);
    time_t cur_t  = time(NULL);

    while(true)
		{
        if(cur_t - last_t == 1)
				{
            last_t = time(NULL);

            string time = boost::posix_time::to_iso_extended_string(ros::Time::now().toBoost());
            strcpy(STRING_CHARS, time.c_str());
            ROS_INFO("Time: %s", STRING_CHARS);
            // clear
            memset(STRING_CHARS, 0, sizeof(STRING_CHARS));

            // 模拟1f/s发送各节点测试数据消息
            sendNodeStatusTestDatas();
				}
        else if(cur_t - last_t == 100)
        {
            // 模拟10f/s发送点云着色测试数据消息
            sendMessageSyncTestDatas();
        }
				else
				{
						cur_t = time(NULL);
				}
		}
}

/// 主函数
int main(int argc, char** argv)
{
    ROS_INFO("Status Manager Main");

    ros::init(argc, argv, "StatusManagerTest");
	  ros::NodeHandle n;

    /// 节点
    cameraManagerStatusPub       = n.advertise<diagnostic_msgs::DiagnosticArray>("/yida/internal/camera_status", 1);
    homeManagerStatusPub         = n.advertise<diagnostic_msgs::DiagnosticArray>("/yida/internal/charge_status", 1);
    localizationManagerStatusPub = n.advertise<diagnostic_msgs::DiagnosticArray>("/heartbeat", 1);

    fuzzyControllerStatusPub    = n.advertise<diagnostic_msgs::DiagnosticArray>("/yida/internal/fuzzy_status", 1);
    platformControllerStatusPub = n.advertise<diagnostic_msgs::DiagnosticArray>("/yida/internal/platform_status", 1);

    /// 点云着色
    robotPosePub    = n.advertise<nav_msgs::Odometry>("/robot_pose", 1);
    platformPosePub = n.advertise<nav_msgs::Odometry>("/yida/yuntai/position", 1);
    visualImagePub  = n.advertise<sensor_msgs::Image>("/thermal/image_raw", 1);
    thermalImagePub = n.advertise<sensor_msgs::CompressedImage>("thermal/image_proc/compressed", 1);

		// StatusManagerTest Thread
    thread statusManagerTestThread(statusManagerTest);
    statusManagerTestThread.join();

    return 0;
}
