#include <ros/ros.h>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <message_filters/sync_policies/exact_time.h>
#include "nav_msgs/Odometry.h"
#include "sensor_msgs/Image.h"
#include "sensor_msgs/CompressedImage.h"

#include "yidamsg/pointcloud_color.h"
#include <iostream>
#include "lz4/lz4.c"
#include <ctime>
#include "param_server/server.h"

using namespace std;

class MessageSync3{
private:
    param_server::Server* server;
    void readConfig(){
        if(server->exist("pos_x")){
            server->get("pos_x",pos_x);
        }
        if(server->exist("pos_y")){
            server->get("pos_y",pos_y);
        }
        if(server->exist("pos_z")){
            server->get("pos_z",pos_z);
        }
        if(server->exist("qua_x")){
            server->get("qua_x",qua_x);
        }
        if(server->exist("qua_y")){
            server->get("qua_y",qua_y);
        }
        if(server->exist("qua_z")){
            server->get("qua_z",qua_z);
        }
        if(server->exist("qua_w")){
            server->get("qua_w",qua_w);
        }
        if(server->exist("version")){
            server->get("version",version);
        }
        if(server->exist("device_id")){
            server->get("device_id",device_id);
        }
        ROS_INFO("pos_x:%f pos_y:%f pos_z:%f",pos_x,pos_y,pos_z);
        ROS_INFO("qua_x:%f qua_y:%f qua_z:%f qua_w:%f",qua_x,qua_y,qua_z,qua_w);
        ROS_INFO("version:%i device_id:%i",version,device_id);
    }
public:
    static LZ4_stream_t* compressor()
    {
        static LZ4_stream_t* _compressor = LZ4_createStream();
        return _compressor;
    }

    static LZ4_streamDecode_t* decompressor()
    {
        static LZ4_streamDecode_t* _decompressor = LZ4_createStreamDecode();
        return _decompressor;
    }
    static string get_system_cur_time()
    {
        time_t raw_time;
        struct tm * time_info;
        time(&raw_time);
        time_info = localtime(&raw_time);

        char buffer[50];
        strftime(buffer, sizeof(buffer), "%Y-%m-%d %H:%M:%S", time_info);
        string time_str(buffer);
        memset(buffer, 0, sizeof(buffer));
        return time_str;
    }

    ros::NodeHandle nh;
    ros::Publisher pub;
    std::string platform_topic,temp_topic,image_topic,pub_topic;
    typedef message_filters::sync_policies::ApproximateTime<nav_msgs::Odometry,nav_msgs::Odometry,sensor_msgs::Image> mySyncPolicy;
    message_filters::Subscriber<nav_msgs::Odometry> *plat_sub;
    message_filters::Subscriber<nav_msgs::Odometry> *temp_sub;
    message_filters::Subscriber<sensor_msgs::Image> *image_sub;
    message_filters::Synchronizer<mySyncPolicy> *sync;
    int device_id,version;
    float pos_x,pos_y,pos_z,qua_w,qua_x,qua_y,qua_z;

    MessageSync3(const std::string packageName,const ros::NodeHandle &nh_ = ros::NodeHandle("~")):nh(nh_){
        ros::param::get("~/platform_topic", platform_topic);
        ros::param::get("~/image_topic", image_topic);
        ros::param::get("~/temp_topic", temp_topic);
        ros::param::get("~/pub_topic", pub_topic);
        std::string configName;
        ros::param::get("~/config", configName);
        ROS_INFO("config:%s",configName.c_str());
        //package config
        server = new param_server::Server(packageName,configName);
        param_server::CallbackType f = boost::bind(&MessageSync3::configCallback,this,_1); //绑定回调函数
        server->setCallback(f);
        readConfig();
        ROS_INFO("platform_topic:%s image_topic:%s",platform_topic.c_str(),image_topic.c_str());
        pub = nh.advertise<yidamsg::pointcloud_color>(pub_topic, 1);
        plat_sub = new message_filters::Subscriber<nav_msgs::Odometry>(nh, platform_topic, 1);
        temp_sub = new message_filters::Subscriber<nav_msgs::Odometry>(nh, temp_topic, 1);
        image_sub = new message_filters::Subscriber<sensor_msgs::Image>(nh, image_topic, 1);
        sync = new message_filters::Synchronizer<mySyncPolicy>(mySyncPolicy(10), *plat_sub,*temp_sub, *image_sub);
        sync->registerCallback(boost::bind(&MessageSync3::callback, this, _1, _2, _3));
        // exactSync = new message_filters::Synchronizer<exactPolicy>(exactPolicy(20), *plat_sub, *image_sub);
        // exactSync->registerCallback(boost::bind(&MessageSync3::callback, this, _1, _2));
    }

    ~MessageSync3(){};
    void configCallback(param_server::SimpleType &config)
    {
        // for (auto &kv : config) {
        //     ROS_INFO("callback key:%s value:%s",kv.first.c_str(),kv.second.c_str());
        // }
        readConfig();
    }
    void callback(const nav_msgs::Odometry::ConstPtr &platformMsg,
                const nav_msgs::Odometry::ConstPtr &tempPoint,
                const sensor_msgs::Image::ConstPtr &temperatureMsg){
    clock_t start = clock();
    //cout << " receive Time：" << start << endl;
    nav_msgs::Odometry syn_platform_pose = *platformMsg;
    nav_msgs::Odometry temp_point = *tempPoint;
    sensor_msgs::Image syn_temperature   = *temperatureMsg;

    yidamsg::pointcloud_color pointCloudSyncData;
    if(version==1){
        /// Temperarture
        std::vector<unsigned char>  oriVect = syn_temperature.data;
        if(oriVect.size()<4) return;
        std::vector<unsigned char>  dealVect;
        //deal float -> 2byte
        float f = 0;
        //char buffer[4];
        unsigned char inn = 0,dcc = 0;
        int in = 0,ff = 0;
    for (int i = 0; i < oriVect.size(); )
    {
        try
        {
            float* t = (float*)&oriVect[i];
            f = *t;
        }
        catch(const std::exception& e)
        {
            std::cerr << e.what() << '\n';
        }
        
        //deal
        f = f + 100;
        in = (int)f;
        inn = in > 255 ? 255:in;
        ff =(f-in)*100;
        dcc = (unsigned char)ff;
        //printf("f=%d inn=%x dcc=%x\n",f,inn,dcc);
        dealVect.push_back(inn);
        dealVect.push_back(dcc);
        i+=4;
        f = 0;
    }
    clock_t end1   = clock();
    //cout << "花费了" << (double)(end1 - start)/CLOCKS_PER_SEC << endl;
    int length = dealVect.size();
    //ROS_INFO("start compress :%i",length);
    std::vector<unsigned char> compressedBuf(LZ4_COMPRESSBOUND(length));
    LZ4_resetStream_fast(MessageSync3::compressor());
    const int compressedSize = LZ4_compress_fast_continue(
                 MessageSync3::compressor(), (char*)&dealVect[0], (char*)&compressedBuf[0],
                 length, compressedBuf.size(), 1);
    //ROS_INFO("compressed size : %i",compressedSize);
    std::vector<unsigned char> decodeData;
    decodeData.assign(compressedBuf.begin(), compressedBuf.begin() + compressedSize);

    pointCloudSyncData.temperature = decodeData;

    }else if(version==2){
        std::vector<unsigned char> decodeData;
        decodeData.assign(syn_temperature.data.begin(), syn_temperature.data.begin() + syn_temperature.data.size());
        pointCloudSyncData.temperature = decodeData;
    }else if(version==3){
        std::vector<unsigned char> decodeData;
        decodeData.assign(syn_temperature.data.begin(), syn_temperature.data.begin() + syn_temperature.data.size());
        decodeData.push_back(temp_point.pose.pose.position.x);
        decodeData.push_back(temp_point.pose.pose.position.y);
        decodeData.push_back(temp_point.pose.pose.position.z);
        decodeData.push_back(temp_point.pose.pose.orientation.x);
        pointCloudSyncData.temperature = decodeData;
    }
    //Pose
    pointCloudSyncData.pos_x = pos_x;
    pointCloudSyncData.pos_y = pos_y;
    pointCloudSyncData.pos_z = pos_z;

    pointCloudSyncData.qua_x = qua_x;
    pointCloudSyncData.qua_y = qua_y;
    pointCloudSyncData.qua_z = qua_z;
    pointCloudSyncData.qua_w = qua_w;
    /// Platform Pose Process
    pointCloudSyncData.horizontal = syn_platform_pose.pose.pose.position.x;
    pointCloudSyncData.vertical   = syn_platform_pose.pose.pose.position.z;

    /// Stamp
    pointCloudSyncData.stamp = MessageSync3::get_system_cur_time();
    pointCloudSyncData.version = version;
    pointCloudSyncData.device_id = device_id;
    clock_t end2   = clock();
    cout << "fuction deal time:" << (double)(end2 - start)/CLOCKS_PER_SEC << endl;
    pub.publish(pointCloudSyncData);
    }
};