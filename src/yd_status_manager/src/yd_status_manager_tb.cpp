// 工具类
#include "boost/date_time/posix_time/posix_time.hpp"
#include "boost/date_time/posix_time/posix_time_io.hpp"

#include "ros/ros.h"
#include "stdio.h"
#include "time.h"

#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include <chrono>
#include <cstring>
#include <cstddef>
#include <fstream>
#include <iostream>
#include <locale>
#include <ratio>
#include <sstream>
#include <string>
#include <thread>
#include <vector>

/// Boost
#include "boost/bind.hpp"
#include "boost/thread/mutex.hpp"
#include "boost/thread/thread.hpp"
#include <boost/regex.hpp>
#include <boost/filesystem.hpp>

/// OpenCV
#include <opencv2/opencv.hpp>
#include <cv_bridge/cv_bridge.h>

// 消息类
/// Msg
#include "std_msgs/Bool.h"
#include "std_msgs/UInt8.h"
#include "std_msgs/Int32.h"
#include "std_msgs/String.h"

#include "nav_msgs/Odometry.h"

#include <diagnostic_msgs/DiagnosticArray.h>
#include <diagnostic_msgs/DiagnosticStatus.h>
#include <diagnostic_msgs/KeyValue.h>

/// Srv
//#include "yd_framework/Settings.h"

/// hpp
#include "../include/yd_status_manager/hpp/point_cloud_message_filters.hpp"
#include "../include/yd_status_manager/hpp/yd_status_manager.hpp"

#include "lz4/lz4.c"
#include <ctime>

using namespace cv;
using namespace std;
using namespace message_filters;

/// Variables
int XAVIER_LEVEL_VALUE;

bool is_stop_receive_laser = false;
nav_msgs::Odometry statusManagerPoseMsg;
nav_msgs::Odometry lastSynPoseMsg;

/// XAvier Buffer Size
bool is_robot_stop = false;
//bool is_xavier_sleeping = false;
//int XAVIER_SLEEPING_TIME             = 0;
//int XAVIER_BUFFER_SIZE_MAXIMUM_COUNT = 0;

/// Publisher-Subscriber
// Publisher
ros::Publisher statusManagerHeartbeatPub;
ros::Publisher robotControlPub;
//ros::Publisher xavierSleepPub;
// Subscriber
ros::Subscriber statusManagerPoseSub;
ros::Subscriber isStopReceiveLaserSub;
ros::Subscriber localizationStatusSub;

/// Client
//ros::ServiceClient statusManagerSettingsClient;
//ros::ServiceClient statusManagerTasksClient;
// Srv消息
//yd_framework::Settings settingsSrv;

/// 节点监听类
YD_STATUS_MANAGER *YDStatusManager = NULL;
/// 点云着色数据同步类
MessageFilters *mf = NULL;

static LZ4_stream_t* compressor()
{
        static LZ4_stream_t* _compressor = LZ4_createStream();
        return _compressor;
}

static LZ4_streamDecode_t* decompressor()
    {
        static LZ4_streamDecode_t* _decompressor = LZ4_createStreamDecode();
        return _decompressor;
    }

/*===================================回调消息代码块===================================*/
// StatusManagerPoseMessagesCallback
void statusManagerPoseMessagesCallback(const nav_msgs::Odometry::ConstPtr& msg)
{
    statusManagerPoseMsg = *msg;
}
// IsStopReceiveLaserMessagesCallback
void isStopReceiveLaserMessagesCallback(const std_msgs::Bool::ConstPtr& msg)
{
    is_stop_receive_laser = msg->data;
}

/// 定位节点相关
// 车体停止、继续移动
void robotControlFun(bool b)
{
    std_msgs::Int32 msg;
    msg.data = b ? 0 : 1;
    robotControlPub.publish(msg);
}
/* Xavier休眠、唤醒
void xavierControlFun(bool b)
{
    std_msgs::Bool msg;
    msg.data = b;
    xavierSleepPub.publish(msg);
}
*/
// LocalizationStatusMessagesCallback
void localizationStatusMessagesCallback(const diagnostic_msgs::DiagnosticArray::ConstPtr& msg)
{
    vector<diagnostic_msgs::DiagnosticStatus>::const_iterator it;
    for(it = msg->status.begin(); it != msg->status.end(); ++it)
    {
        // 过滤筛选出定位节点状态
        if(it->name == "/Localization_node")
        {
            // OK
            if(it->level == diagnostic_msgs::DiagnosticStatus::OK)
            {
                XAVIER_LEVEL_VALUE = 0;

                // 车体恢复移动
                if(is_robot_stop)
                {
                    ROS_INFO("ROBOT CONTINUE MOVE!");

                    is_robot_stop = false;
                    robotControlFun(is_robot_stop);
                }
            }
            // WARN
            else if(it->level == diagnostic_msgs::DiagnosticStatus::WARN)
            {
                XAVIER_LEVEL_VALUE = 1;
                ROS_INFO("XAvier Bad Matching.");
            }
            // ERROR
            else if(it->level == diagnostic_msgs::DiagnosticStatus::ERROR)
            {
                XAVIER_LEVEL_VALUE = 2;
                ROS_INFO("XAvier Bad Situation(Buffer Size > 5).");
            }
            // STALE(TODO: 逻辑基于休眠消息发送后，/heartbeat停止发送)
            else if(it->level == diagnostic_msgs::DiagnosticStatus::STALE)
            {
                XAVIER_LEVEL_VALUE = 3;
                ROS_INFO("XAvier Error, Stop Robot and XAvier Need Sleep(Buffer Size > 20).");

                /*
                // 停止车体
                is_robot_stop = true;
                robotControlFun(is_robot_stop);

                *
                XAVIER_BUFFER_SIZE_MAXIMUM_COUNT++;

                // 停止车体
                if(!is_robot_stop)
                {
                    is_robot_stop = true;
                    robotControlFun(is_robot_stop);
                }
                // XAvier休眠1m
                if(XAVIER_BUFFER_SIZE_MAXIMUM_COUNT == 3)
                {
                    XAVIER_BUFFER_SIZE_MAXIMUM_COUNT = 0;

                    is_xavier_sleeping = true;
                    xavierControlFun(is_xavier_sleeping);
                }
                */
            }
        }
    }
}

// MessageSyncPolicyCallback
void MessageFilters::messageSyncPolicyCallback(const nav_msgs::Odometry::ConstPtr& poseMsg,
                                               const nav_msgs::Odometry::ConstPtr& platformPoseMsg,
                                               const sensor_msgs::ImageConstPtr& temperatureMsg)
{
    clock_t start = clock();
    cout << " receive Time：" << start << endl;
    nav_msgs::Odometry syn_pose          = *poseMsg;
    nav_msgs::Odometry syn_platform_pose = *platformPoseMsg;
    sensor_msgs::Image syn_temperature   = *temperatureMsg;

    yidamsg::pointcloud_color pointCloudSyncData;

    // Temperarture
    std::vector<unsigned char> decodeData;
    decodeData.assign(syn_temperature.data.begin(), syn_temperature.data.begin() + syn_temperature.data.size());
    /*
    ROS_INFO("start compress :%i",length);
    std::vector<unsigned char> compressedBuf(LZ4_COMPRESSBOUND(length));
    LZ4_resetStream_fast(compressor());
    const int compressedSize = LZ4_compress_fast_continue(
                 compressor(), (char*)&pointCloudSyncData.temperature[0], (char*)&compressedBuf[0],
                 length, compressedBuf.size(), 1);
    ROS_INFO("compressed size : %i",compressedSize);
    std::vector<unsigned char> decodeData;
    decodeData.assign(compressedBuf.begin(), compressedBuf.begin() + compressedSize);
    
    ROS_INFO("[ROBOT MOVING] x=%f y=%f z=%f", syn_pose.pose.pose.position.x, syn_pose.pose.pose.position.y, syn_pose.pose.pose.position.z);

    ROS_INFO("decoded ...");
    static std::vector<unsigned char> decodedBuf(length);
            const int decodedSize = LZ4_decompress_safe_continue(
                decompressor(), (char*)&decodeData[0], (char*)&decodedBuf[0],
                decodeData.size(), decodedBuf.size());
    ROS_INFO("decoded size : %i",decodedSize);
    */
    pointCloudSyncData.temperature = decodeData;

    pointCloudSyncData.pos_x = syn_pose.pose.pose.position.x;
    pointCloudSyncData.pos_y = syn_pose.pose.pose.position.y;
    pointCloudSyncData.pos_z = syn_pose.pose.pose.position.z;

    pointCloudSyncData.qua_x = syn_pose.pose.pose.orientation.x;
    pointCloudSyncData.qua_y = syn_pose.pose.pose.orientation.y;
    pointCloudSyncData.qua_z = syn_pose.pose.pose.orientation.z;
    pointCloudSyncData.qua_w = syn_pose.pose.pose.orientation.w;

    /// Platform Pose Process
    pointCloudSyncData.horizontal = syn_platform_pose.pose.pose.position.x;
    pointCloudSyncData.vertical   = syn_platform_pose.pose.pose.position.z;

    /// Stamp
    pointCloudSyncData.stamp = get_system_cur_time();
    pointCloudSyncData.version = 2;
    pointCloudSyncData.device_id = 1;
    /// Publish
    clock_t end2   = clock();
    cout << "total:" << (double)(end2 - start)/CLOCKS_PER_SEC << endl;
    //ROS_INFO(">>>>> [MOVE]Point Cloud Data Sync Pub Time:[%s]", get_system_cur_time().c_str());
    mf->pointCloudSyncDataPub.publish(pointCloudSyncData);
}
// _MessageSyncPolicyCallback
void MessageFilters::_messageSyncPolicyCallback(const nav_msgs::Odometry::ConstPtr& platformPoseMsg,
                                                const sensor_msgs::ImageConstPtr& temperatureMsg)
{
    if(is_stop_receive_laser)
    {
        nav_msgs::Odometry syn_platform_pose = *platformPoseMsg;
        sensor_msgs::Image syn_temperature   = *temperatureMsg;

        yidamsg::pointcloud_color pointCloudSyncData;

        /// Image Process

        /// Temperarture
        pointCloudSyncData.temperature = syn_temperature.data;

        //ROS_INFO("[ROBOT STOP] x=%f y=%f z=%f", lastSynPoseMsg.pose.pose.position.x, lastSynPoseMsg.pose.pose.position.y, lastSynPoseMsg.pose.pose.position.z);

        pointCloudSyncData.pos_x = lastSynPoseMsg.pose.pose.position.x;
        pointCloudSyncData.pos_y = lastSynPoseMsg.pose.pose.position.y;
        pointCloudSyncData.pos_z = lastSynPoseMsg.pose.pose.position.z;

        pointCloudSyncData.qua_x = lastSynPoseMsg.pose.pose.orientation.x;
        pointCloudSyncData.qua_y = lastSynPoseMsg.pose.pose.orientation.y;
        pointCloudSyncData.qua_z = lastSynPoseMsg.pose.pose.orientation.z;
        pointCloudSyncData.qua_w = lastSynPoseMsg.pose.pose.orientation.w;

        /// Platform Pose Process
        pointCloudSyncData.horizontal = syn_platform_pose.pose.pose.position.x;
        pointCloudSyncData.vertical   = syn_platform_pose.pose.pose.position.y;

        /// Stamp
        pointCloudSyncData.stamp = get_system_cur_time();

        /// Publish
        sleep(0.45);
        ROS_INFO(">>>>> [STOP]Point Cloud Data Sync Pub Time:[%s]", get_system_cur_time().c_str());
        mf->pointCloudSyncDataPub.publish(pointCloudSyncData);
    }
}
/*====================================================================================*/

// 节点心跳
void nodeHeartBeat()
{
    diagnostic_msgs::DiagnosticArray heartbeatMsg;
    heartbeatMsg.header.stamp = ros::Time::now();

    diagnostic_msgs::DiagnosticStatus heartbeatStatus;
    heartbeatStatus.level = 0;
    heartbeatStatus.name  = "/yd_status_manager";
    heartbeatStatus.hardware_id = "1";
    heartbeatStatus.message = "ok";  
    diagnostic_msgs::KeyValue dv;
    dv.key = "error_code";
    dv.value = "10500";
    heartbeatStatus.values.push_back(dv);
    heartbeatMsg.status.push_back(heartbeatStatus);
    statusManagerHeartbeatPub.publish(heartbeatMsg);
}

// StatusManager主线程
void statusManager()
{
		ROS_INFO("statusManager()");

    time_t last_t = time(NULL);
    time_t cur_t  = time(NULL);

    while(ros::ok())
		{
        if(cur_t - last_t == 1)
				{
            last_t = time(NULL);

            // time note
            string cur_time = get_system_cur_time();
            ROS_INFO(">>>>> TIME[%s]", cur_time.c_str());

            // 每秒发送设备状态节点信息
            YDStatusManager->sendStatusData(statusManagerPoseMsg, XAVIER_LEVEL_VALUE);
            // 向 监控中心 发送 节点心跳
            nodeHeartBeat();

            /* XAvier休眠提示
            if(is_xavier_sleeping)
            {
                XAVIER_SLEEPING_TIME++;
                ROS_INFO("XAvier Bad Situation, Sleeping 1m(%ds)", XAVIER_SLEEPING_TIME);

                // XAvier休眠结束
                if(XAVIER_SLEEPING_TIME == 60)
                {
                    XAVIER_SLEEPING_TIME = 0;
                    is_xavier_sleeping   = false;
                    xavierControlFun(is_xavier_sleeping);

                    // 车子恢复移动
                    is_robot_stop = false;
                    robotControlFun(is_robot_stop);
                }
            }
            */
				}
				else
				{
						cur_t = time(NULL);
				}

        sleep(0.4);
		}
}

// ros初始化线程
void *rosInit(void *arg)
{
    ROS_INFO("rosInit()");

    ros::NodeHandle n = *((ros::NodeHandle*) arg);

		// Subscriber
    statusManagerPoseSub  = n.subscribe<nav_msgs::Odometry>("/robot_pose", 1, statusManagerPoseMessagesCallback);
    isStopReceiveLaserSub = n.subscribe<std_msgs::Bool>("/stop_receive_laser", 1, isStopReceiveLaserMessagesCallback);
    localizationStatusSub = n.subscribe<diagnostic_msgs::DiagnosticArray>("/yd/heartbeat", 1, localizationStatusMessagesCallback);

    // 点云着色数据同步
    mf = new MessageFilters(n);

    ros::spin();
}

// 主函数
int main(int argc, char** argv)
{
    ros::init(argc, argv, "StatusManager");
	ros::NodeHandle n;

    YDStatusManager = new YD_STATUS_MANAGER(n);

    // Pub
	statusManagerHeartbeatPub = n.advertise<diagnostic_msgs::DiagnosticArray>(YDStatusManager->status_manager_heart_beat_pub_topic_name, 1);
    robotControlPub           = n.advertise<std_msgs::Int32>(YDStatusManager->status_manager_robot_control_pub_topic_name, 1);
    //xavierSleepPub            = n.advertise<std_msgs::Bool>(YDStatusManager->status_manager_xavier_sleep_pub_topic_name, 1);

    // Ros Init Thread
    pthread_t rosInitThread = 0;
    pthread_create(&rosInitThread, NULL, rosInit, &n);

		// StatusManager Thread
    std::thread statusManagerThread(statusManager);
    statusManagerThread.join();

    return 0;
}
