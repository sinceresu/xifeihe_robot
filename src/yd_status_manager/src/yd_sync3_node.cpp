/*
 * @Descripttion: 
 * @version: 
 * @Author: li
 * @Date: 2021-02-22 11:12:47
 * @LastEditors: li
 * @LastEditTime: 2021-02-22 11:13:06
 */
#include "ros/ros.h"
#include "messagesync3.hpp"

#define __app_name__ "sync_node"

int main(int argc, char **argv)
{
    ros::init(argc, argv, __app_name__);
    ROS_INFO("%s node start ...",__app_name__);
    MessageSync3 node("yd_status_manager");
    
    ros::Rate rate(1);

    while (ros::ok())
    {
        ros::spinOnce();
        rate.sleep();
    }

    return 0;
}