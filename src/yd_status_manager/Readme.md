# **设备节点相关消息参数说明(监听部分)**

## 相机管理节点

> 通过 ping 可见光/红外相机的 ip 的状态，来获取相机设备当前的状态。
三种状态：

* 不可ping * 正常 * 延迟

### 消息

> topic: /yida/internal/camera_status

> message: diagnostic_msgs/DiagnosticArray.msg

### 说明：

* Header 部分

1. stamp: 当前时间戳
2. frame_id: visual/thermal 分别对应“可见光相机/红外相机”

* DiagnosticStatus 部分

1. level: ERROR/OK/WARN 分别对应“三种状态”
2. name: 可见光相机 ip/红外相机 ip
3. message: 不可ping/正常/延迟 三种状态描述
4. hardware_id: 可填相机设备 id，无则不填
5. values: 可填当前相机的焦距、光圈大小等相关设备参数，无则不填

## 运动控制节点

> 暂不使用

## 传感器节点

> 传感器相关和底盘相关部分数据以byte序列方式从串口获取并发出，详细说明见协议说明文档。

### 传感器相关

1. 前后测距超声波传感器

2. 左右防跌落超声波传感器

3. 防撞条传感器

4. 温度传感器

5. 升降台

6. 里程计

### 底盘相关：

> 电压、电流、电量、电池温度、电池循环次数、电池充/放电MOS状态以及电机等等。

### 消息

① 升降台高度

> topic: /yida/lifts/height

> message: std_msgs::Float32

② 里程计

> topic: /yida/wheel/odometer

> message: geometry_msgs::TwistStamped

#### 说明

[1].车体线速度：twist.linear.x

[2].车轮角速度：twist.angular.z

③ 传感器及底盘

> topic: /yida/robot/sensor

> message: diagnostic::DiagnosticArray

## 云台控制节点

> 通过实时获取云台当前的“水平/垂直角度”来判断云台的工作状态。

### 消息

> topic: /yida/internal/platform_status

> message: diagnostic_msgs/DiagnosticArray.msg

### 说明

* Header 部分

stamp: 当前时间戳

* DiagnosticStatus 部分

1. level: ERROR/OK/WARN 目前暂不填值
2. name: platform_angle
3. message: 云台水平、垂直角度的拼接字符串，以空格隔开
4. hardware_id: 云台设备 id，无则不填
5. values: 暂不填

## 狗窝管理节点
> 充电桩：实时限位开关状态

> 自动门：实时开、关状态

### 消息
> topic: /yida/internal/charge_status

> message: diagnostic_msgs/DiagnosticArray.msg

### 说明

* Header 部分
1. stamp: 当前时间戳
2. frame_id: charge_stake/door 分别对应“充电桩/自动门”

* DiagnosticStatus 部分
1. level: ERROR/OK/WARN 目前暂不填值
2. name: charge_stake_state/door_state
3. message: 充电桩充电与否状态 0(无)/1(充电) 或 自动门开关状态 0(关)/1(开)
4. hardware_id: 设备 id，无则不填
5. values: 暂不填

## 定位节点
> 通过定位节点的/heartbeat心跳消息，监听当前XAvier Buffer-size的变化，以判定其设备工作状态

### 消息
> topic: /heartbeat

> message: diagnostic_msgs/DiagnosticArray.msg

### 说明

* Header 部分

stamp: 当前时间戳

* DiagnosticStatus 部分

level: OK/WARN/ERROR/STALE

① OK: 设备工作状态正常

② WARN: 设备工作状态开始出现异常，坐标matching匹配不稳定，可能需要重启定位

③ ERROR: 设备工作状态出现问题，需要发送车体停止消息，待稳定时继续车体行走

④ STALE: 设备工作状态错误，需要发送车体停止消息，后续可能需要再发送XAvier休眠消息

# **设备节点相关消息参数说明(发送部分)**
> StatusManager以每秒1帧的频率发送所有设备当前状态信息，供资源中心ResourceCenter使用(PS: 资源中心解析数据时，请按照所给的**key**值进行解析)

## 消息
> topic: /yida/status_updates

> message: diagnostic_msgs/DiagnosticArray.msg

### 说明

* Header 部分

stamp: 当前时间戳

* DiagnosticStatus 部分
1. name: StatusManager
2. message: DevicesStatus
3. values:

    ① 定位管理节点状态

      <1>.车体位置信息robotPoseValue

          `key   = "Robot Pose Status"`
          `value = "车体位置x" + "," + "车体位置y" + "," + "车体位置z" + " " + "车体位姿x" + "," + "车体位姿y" + "," + "车体位姿z" + "车体位姿w"`

      <2>.XAvier信息xavierStatusValue

          `key   = "XAvier Status"`
          `value = "0(OK)/1(WARN)/2(ERROR)/3(STALE)"`

    ② 相机管理节点状态

      <1>.可见光相机信息visualCameraStatusValue

          `key   = "Visual Camera Status"`
          `value = "0(OK)/1(WARN)/2(ERROR)"`

      <2>.红外相机信息thermalCameraStatusValue

          `key   = "Thermal Camera Status"`
          `value = "0(OK)/1(WARN)/2(ERROR)"`

    ③ 狗窝管理节点状态

      <1>.充电桩信息chargeStakeStatusValue

          `key   = "Charge Stake Status"`
          `value = "0(未充电)/1(充电)"`

      <2>.自动门信息doorStatusValue

          `key   = "Door Status"`
          `value = "0(关)/1(开)"`

    ④ 云台控制节点状态platformStatusValue

      `key   = "Platform Status"`
      `value = "水平角度" + " " + "垂直角度"`

    ⑤ 传感器及底盘节点状态

      PS: 当传感器出现异常时，相对应的value值会分别赋为"-1"、"-2"、"-3"；分别代表“数据异常”、“传输异常”和“线路异常”。因为目前无法判断，当其中某个异常出现时是否会引起其他异常并发(如：**线路异常**时，此时数据与传输是否都会发生异常？)，所以这里暂不考虑异常并发的情况，即：当数据异常与传输异常并发时，只会按照判断的先后逻辑顺序使后面的传输异常覆盖掉前面的数据异常，而只看到后面的传输异常。

      <1>.传感器信息(说明举例)

          [1].测距超声波ultrasonicDis(Left/Right Front/Back)StatusValue

             `// 左前`
             `key   = "Ultrasonic Dis Left Front Status"`
             `value = "测距值(或异常code[-1/-2/-3])"`
             `// 右后`
             `key   = "Ultrasonic Dis Right Back Status"`
             `value = "同上"`

          [2].防跌落超声波ultrasonicFall(Left/Right Front/Back)StatusValue

             `// 左前`
             `key   = "Ultrasonic Fall Left Front Status"`
             `value = "测距值(或异常code[-1/-2/-3])"`
             `// 右后`
             `key   = "Ultrasonic Fall Right Back Status"`
             `value = "同上"`

          [3].前后防撞条antiCollisionBar(Front/Back)StatusValue

             `// 前(PS: 防撞条传感器异常时，只有**线路异常**)`
             `key   = "Anti Collision Bar Front Status"`
             `value = "0(无)/1(碰撞)(或异常code[-3])"`

          [4].升降台liftHeightValue

             `// 单位: 毫米(mm)`
             `key   = "Lift Height Value"`
             `value = "高度值"`

          [5].里程计

             `key   = "Odometer Value"`
             `value = "线速度" + "," + "角速度"`

          [6].温度传感器(1/2/3/4不知分别为什么设备的温度)

             `key   = "Temperature Sensor 1 Status"`
             `value = "温度值"`

          [7].车子RPY(横滚角、俯仰角、偏航角)

             `// R`
             `key   = "Robot Roll Angle"`
             `value = "角度值"`

      <2>.底盘信息

          [1].电机 压、流、can通信故障信息(1/2/3/4/5/6/7/8不知分别为什么设备的电机)

             `// 1`
             `key   = "Robot Motor 1 Voltage Status"`
             `value = "电压值"`
             `key   = "Robot Motor 1 Electricity Status"`
             `value = "电流值"`
             `key   = "Robot Motor 1 Can Signal Status"`
             `value = "0(无故障)/1(有故障)"`

          [2].10节电池电压batteryPart(1/2/3/4/5/6/7/8/9/10)VoltageStatusValue

             `// 1`
             `key   = "Battery Part 1 Voltage"`
             `value = "电压值"`

          [3].电池温度(1/2)

             `// 1`
             `key   = "Battery Temperature 1 Value"`
             `value = "温度值"`

          [3].电池 充、放 电流batteryChargeDischargeElectricityStatusValue

             `key   = "Battery Charge Discharge Electricity"`
             `value = "电流值"`

          [4].电量batteryVolumeStatusValue

             `key   = "Battery Volume"`
             `value = "电量值"`

          [5].电池循环次数batteryRecycleNumStatusValue

             `key   = "Battery Recycle Num"`
             `value = "次数"`

          [6].无线充电故障信息

             `key   = "Wireless Charge Alarm Code"`
             `value = "0(无故障)/1(有故障)"`

          [7].电池 充、放 电MOS状态battery(Charge/Discharge)MosStatusValue

             `// 充电`
             `key   = "Battery Charge Mos Status"`
             `value = "0(关)/1(开)"`

          [8].电池报警(过压/欠压/过流/高温)battery(High/Low Voltage、Over Curent、High Temperature)AlarmCodeValue

             `// 过压`
             `key   = "Battery High Voltage Alarm Code"`
             `value = "0(无)/1(有)"`

          [9].ADC电压检测(1/2/3/4)

             `// 1`
             `key   = "ADC1 Voltage Status"`
             `value = "电压值"`

> StatusManager同步着色数据(最新说明待更新)

同步机器人位置、云台位置、可见光图片以及红外图片数据，用以点云着色使用。

# 同步话题topic：

1. robot位置：/robot_pose
2. 云台位置：/yida/yuntai/position
3. flirone可见光图片：/thermal/image_raw
4. flirone红外图片：/thermal/image_proc/compressed

## 同步数据结构说明

### ① 消息：yidamsg/pointcloud_color

<1>.v_format:可见光图片数据格式

<2>.v_data:可见光图片数据(string字符串拼接，暂不用；其作用被gray_data代替)

<3>.t_format:红外图片数据格式

<4>.t_data:红外图片数据(string字符串拼接)

<5>.pos_x、pos_y、pos_z:robot位置x、y、z

<6>.quz_x、qua_y、qua_z、qua_w:robot位姿x、y、z、w

<7>.horizontal、vertical:云台水平、垂直角度位置

<8>.gray_data:因为可见光图片数据为uint16灰度数据，而采取转化成uint8的特殊处理

### ② 因为/stop_receive_laser的逻辑，导致robot有时会停止发送车体位置信息，此时同步数据模块会以发送车体停止前最后一帧的位置数据处理。

# 发送话题topic：/yd/pointcloud/vt，消息帧率保持在5~8hz左右。

> StatusManager节点启动方式：`rossun yd_status_manager yd_status_manager`

roslaunch启动方式：`roslaunch yd_status_manager yd_status_manager.launch`

# launch文件说明

① 传感器与底盘信息节点topic

② 节点心跳、车体控制topic

③ 节点状态监听及其状态消息发布topic

④ 点云着色数据同步topic
