


#include "timestamped_transform.h"
#include <glog/logging.h>


TimestampedTransform Interpolate(const TimestampedTransform& start,
                                 const TimestampedTransform& end,
                                 double time) {
  CHECK_LE(start.time, time);
  CHECK_GE(end.time, time);

  const double duration = end.time - start.time;
  const double factor = (time - start.time) / duration;
  const Eigen::Vector3d origin =
      start.transform.translation() +
      (end.transform.translation() - start.transform.translation()) * factor;
  const Eigen::Quaterniond rotation =
      Eigen::Quaterniond(start.transform.linear())
          .slerp(factor, Eigen::Quaterniond(end.transform.linear()));
  Eigen::Affine3d interploted_transform;
  interploted_transform.translation() = origin;
  interploted_transform.linear() = rotation.matrix();

  return TimestampedTransform{time, interploted_transform};
}
