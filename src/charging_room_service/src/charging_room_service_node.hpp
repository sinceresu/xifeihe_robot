#ifndef charging_room_service_node_HPP
#define charging_room_service_node_HPP

#pragma once
#include <queue>
#include <mutex>
#include "ros/ros.h"
#include "ros/package.h"
#include "std_msgs/String.h"
#include "actionlib/server/simple_action_server.h"
#include "diagnostic_msgs/DiagnosticArray.h"
#include "diagnostic_msgs/DiagnosticStatus.h"
#include "diagnostic_msgs/KeyValue.h"
#include "charging_room_service_msgs/ChargingPileData.h"
#include "charging_room_service_msgs/ChargingPileDataStamped.h"
#include "charging_room_service_msgs/KeyValue.h"
#include "charging_room_service_msgs/StatusValueArray.h"
#include "charging_room_service_msgs/DoorControl.h"
#include "charging_room_service_msgs/DoorAction.h"
#include "charging_room_service.hpp"

namespace charging_room_service
{
    class CChargingRoomServiceNode
    {
    public:
        typedef struct _NodeOptions
        {
            int robot_id;
        } NodeOptions;

    public:
        CChargingRoomServiceNode();
        ~CChargingRoomServiceNode();

    private:
        NodeOptions node_options_;

    private:
        ros::NodeHandle nh_;
        ros::NodeHandle private_nh_;

        void LaunchParam();
        void LaunchPublishers();
        void LaunchService();
        void LaunchActions();

        ros::Publisher charging_pile_data_publisher_;
        ros::Publisher status_value_array_publisher_;

        ros::ServiceServer door_control_service;
        bool door_control_service_Callback(charging_room_service_msgs::DoorControl::Request &req,
                                           charging_room_service_msgs::DoorControl::Response &res);

        typedef actionlib::SimpleActionServer<charging_room_service_msgs::DoorAction> DoorServer;
        DoorServer door_action;
        void door_executeCB(const charging_room_service_msgs::DoorGoalConstPtr &goal);

    public:
        void Run();

    private:
        CChargingRoomService charging_room_service;
        CChargingRoomService::ParamOptions_t charging_room_param;
        void data_handle(void *p);
    };
}

#endif
