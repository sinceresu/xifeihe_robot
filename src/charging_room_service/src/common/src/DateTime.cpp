#include "DateTime.hpp"

namespace utils_common_libs
{

    /**
     * Description:      获取当前日期
     * Function:         GetCurrentDate
     * Parameters:       _format:世界格式
     * Return:           std::string:返回字符串时间
     * Author:
     * Date:
     */
    std::string GetCurrentDate(const std::string &_format)
    {
        time_t nowtime;
        nowtime = time(NULL);
        char tmp[64];
#ifdef __linux__
        strftime(tmp, sizeof(tmp), _format.c_str(), localtime(&nowtime));
#elif _WIN32
        struct tm t;
        errno_t err = localtime_s(&t, &nowtime);
        if (err != 0)
        {
            return "";
        }
        strftime(tmp, sizeof(tmp), _format.c_str(), &t);
#elif __ANDROID__
#else
#endif

        return tmp;
    }

    /**
     * Description:      获取当前时间辍
     * Function:         GetTimeStamp
     * Parameters:
     * Return:           time_t:返回时间辍
     * Author:
     * Date:
     */
    time_t GetTimeStamp()
    {
        /* std::chrono::milliseconds ms = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch());
        std::time_t timeStamp = ms.count(); */
        //
        std::chrono::time_point<std::chrono::system_clock, std::chrono::milliseconds> tp = std::chrono::time_point_cast<std::chrono::milliseconds>(std::chrono::system_clock::now());
        time_t timeStamp = tp.time_since_epoch().count();
        return timeStamp;
    }

    /**
     * Description:      字符串转时间
     * Function:         StrToTime_t
     * Parameters:       _time:字符串时间
     * Parameters:       _format:时间格式
     * Return:           time_t:返回时间
     * Author:
     * Date:
     */
    time_t StrToTime_t(const std::string &_time, const std::string &_format)
    {
        try
        {
            tm tm_time;
            int year, month, day, hour, minute, second;
            sscanf(_time.c_str(), _format.c_str(), &year, &month, &day, &hour, &minute, &second);
            tm_time.tm_year = year - 1900;
            tm_time.tm_mon = month - 1;
            tm_time.tm_mday = day;
            tm_time.tm_hour = hour;
            tm_time.tm_min = minute;
            tm_time.tm_sec = second;
            tm_time.tm_isdst = 0;
            time_t time_ret = mktime(&tm_time);
            return time_ret;
        }
        catch (const std::exception &e)
        {
            std::cerr << e.what() << '\n';
            //
            return 0;
        }
    }

}