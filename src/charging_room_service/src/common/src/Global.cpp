#include "Global.hpp"
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <openssl/md5.h>

namespace utils_common_libs
{

    bool CreateDirs(const std::string &dirPath)
    {
        uint32_t beginCmpPath = 0;
        uint32_t endCmpPath = 0;
        std::string fullPath = "";

        if ('/' != dirPath[0])
        {
            // 当前工作目录的绝对路径
            fullPath = getcwd(nullptr, 0);
            beginCmpPath = fullPath.size();
            fullPath = fullPath + "/" + dirPath;
        }
        else
        {
            fullPath = dirPath;
            beginCmpPath = 1;
        }

        if (fullPath[fullPath.size() - 1] != '/')
        {
            fullPath += "/";
        }

        endCmpPath = fullPath.size();
        for (uint32_t i = beginCmpPath; i < endCmpPath; i++)
        {
            if ('/' == fullPath[i])
            {
                std::string curPath = fullPath.substr(0, i);
                // 判断目标文件夹是否存在
                if (access(curPath.c_str(), F_OK) != 0)
                {
                    if (mkdir(curPath.c_str(), S_IRUSR | S_IRGRP | S_IROTH | S_IWUSR | S_IWGRP | S_IWOTH) == -1)
                    {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    int md5_encoded(const char *input, char *output)
    {
        int nRet = 0;
        //
        unsigned char md[MD5_DIGEST_LENGTH];
        memset(md, 0, MD5_DIGEST_LENGTH);
        char buf[MD5_DIGEST_LENGTH * 2 + 1];
        memset(buf, 0, MD5_DIGEST_LENGTH * 2 + 1);
        //
        MD5_CTX ctx;
        // 1. 初始化
        if (1 != MD5_Init(&ctx))
        {
            nRet = -1;
            printf("MD5_Init failed ... \r\n");
            return -1;
        }
        // 2. 添加数据
        if (1 != MD5_Update(&ctx, (const void *)input, strlen((char *)input)))
        {
            nRet = -1;
            printf("MD5_Update failed ... \r\n");
            return -1;
        }
        // 3. 计算结果
        if (1 != MD5_Final(md, &ctx))
        {
            nRet = -1;
            printf("MD5_Final failed ... \r\n");
            return -1;
        }
        // 4. 输出结果
        for (int i = 0; i < MD5_DIGEST_LENGTH; i++)
        {
            sprintf(&buf[i * 2], "%02x", md[i]);
            // sprintf(&buf[i * 2], "%02X", md[i]);
        }
        if (output == nullptr)
        {
            nRet = -1;
            printf("output nullptr ... \r\n");
            return -1;
        }
        strcpy(output, buf);
        //
        // MD5((unsigned char *)input, strlen((char *)input), md);
        // for (int i = 0; i < MD5_DIGEST_LENGTH; i++)
        // {
        //   sprintf(&buf[i * 2], "%02x", md[i]);
        //   // sprintf(&buf[i * 2], "%02X", md[i]);
        // }
        // strcpy(output, buf);

        return nRet;
    }

    char *md5_hex2c(unsigned char *in_md5_c, char *out_md5_c)
    {
        for (int i = 0; i < MD5_DIGEST_LENGTH; ++i)
        {
            // 小写
            sprintf(out_md5_c + i * 2, "%02x", in_md5_c[i]);
            // 大写
            // sprintf(out_md5_c + i * 2, "%02X", in_md5_c[i]);
        }
        out_md5_c[MD5_DIGEST_LENGTH * 2] = '\0';

        return out_md5_c;
    }

    //
    int string_match_len(const char *pattern, int patternLen,
                         const char *string, int stringLen, int nocase)
    {
        while (patternLen)
        {
            switch (pattern[0])
            {
            case '*':
                while (pattern[1] == '*')
                {
                    pattern++;
                    patternLen--;
                }
                if (patternLen == 1)
                    return 1; /** match */
                while (stringLen)
                {
                    if (string_match_len(pattern + 1, patternLen - 1,
                                         string, stringLen, nocase))
                        return 1; /** match */
                    string++;
                    stringLen--;
                }
                return 0; /** no match */
                break;
            case '?':
                if (stringLen == 0)
                    return 0; /** no match */
                string++;
                stringLen--;
                break;
            case '[':
            {
                int inot, match;

                pattern++;
                patternLen--;
                inot = pattern[0] == '^';
                if (inot)
                {
                    pattern++;
                    patternLen--;
                }
                match = 0;
                while (1)
                {
                    if (pattern[0] == '\\')
                    {
                        pattern++;
                        patternLen--;
                        if (pattern[0] == string[0])
                            match = 1;
                    }
                    else if (pattern[0] == ']')
                    {
                        break;
                    }
                    else if (patternLen == 0)
                    {
                        pattern--;
                        patternLen++;
                        break;
                    }
                    else if (pattern[1] == '-' && patternLen >= 3)
                    {
                        int start = pattern[0];
                        int end = pattern[2];
                        int c = string[0];
                        if (start > end)
                        {
                            int t = start;
                            start = end;
                            end = t;
                        }
                        if (nocase)
                        {
                            start = tolower(start);
                            end = tolower(end);
                            c = tolower(c);
                        }
                        pattern += 2;
                        patternLen -= 2;
                        if (c >= start && c <= end)
                            match = 1;
                    }
                    else
                    {
                        if (!nocase)
                        {
                            if (pattern[0] == string[0])
                                match = 1;
                        }
                        else
                        {
                            if (tolower((int)pattern[0]) == tolower((int)string[0]))
                                match = 1;
                        }
                    }
                    pattern++;
                    patternLen--;
                }
                if (inot)
                    match = !match;
                if (!match)
                    return 0; /** no match */
                string++;
                stringLen--;
                break;
            }
            case '\\':
                if (patternLen >= 2)
                {
                    pattern++;
                    patternLen--;
                }
                /** fall through */
            default:
                if (!nocase)
                {
                    if (pattern[0] != string[0])
                        return 0; /** no match */
                }
                else
                {
                    if (tolower((int)pattern[0]) != tolower((int)string[0]))
                        return 0; /** no match */
                }
                string++;
                stringLen--;
                break;
            }
            pattern++;
            patternLen--;
            if (stringLen == 0)
            {
                while (*pattern == '*')
                {
                    pattern++;
                    patternLen--;
                }
                break;
            }
        }
        if (patternLen == 0 && stringLen == 0)
            return 1;
        return 0;
    }

    int string_match(const char *pattern, const char *string, int nocase)
    {
        return string_match_len(pattern, strlen(pattern), string, strlen(string), nocase);
    }

    //
    int match_string(std::string w_str, std::string m_str)
    {
        int w_len = w_str.size();
        int m_len = m_str.size();
        std::vector<std::vector<int>> b_dp(w_len + 1, std::vector<int>(m_len + 1, 0));
        b_dp[0][0] = 1;
        for (int i = 1; i <= w_len; i++)
        {
            char ch = w_str[i - 1];
            b_dp[i][0] = b_dp[i - 1][0] && (ch == '*');
            for (int j = 1; j <= m_len; j++)
            {
                char ch2 = m_str[j - 1];
                if (ch == '*')
                    b_dp[i][j] = b_dp[i - 1][j] || b_dp[i][j - 1];
                else
                    b_dp[i][j] = b_dp[i - 1][j - 1] && (ch == '?' || ch2 == ch);
            }
        }
        return b_dp[w_len][m_len];
    }

}