#ifndef charging_room_service_HPP
#define charging_room_service_HPP

#pragma once
#include <iostream>
#include <vector>
#include <map>
#include <queue>
#include <thread>
#include <functional>
#include <math.h>
#include <mutex>
#include "common/src/Common.hpp"
#include "tcp/socket_client.hpp"

namespace charging_room_service
{

    enum struct EDoorStatus
    {
        Unknown = 0,
        Open,
        Close,
        Half
    };

    enum struct ETouchStatus
    {
        Unknown = 0,
        ON,
        OFF,
        Left,
        Right
    };

    typedef struct _Key_Value_t
    {
        std::string key;
        std::string value;
        _Key_Value_t(std::string _key, std::string _value) : key(_key), value(_value) {}
    } Key_Value_t;

    class CChargingRoomService
    {
    public:
        typedef struct _ParamOptions_t
        {
            std::string service_ip;
            int service_port;
            int device_slave;
            int door_timeout;
        } ParamOptions_t;

    public:
        CChargingRoomService();
        ~CChargingRoomService();

    private:
        ParamOptions_t param_;
        std::shared_ptr<socket_client::CSocketClient> modbus_ptr_;
        int slave_;

        EDoorStatus door_status;
        ETouchStatus touch_stauts;

        std::mutex cmd_mtx;

    public:
        void SetParam(ParamOptions_t _param);
        int Connect(std::string _ip, int _port, int _slave);
        int Disconnect();
        void Run();
        bool QueryData(std::vector<Key_Value_t> &_data);
        bool OpenDoor();
        bool CloseDoor();
        EDoorStatus GetDoorStatus();
        ETouchStatus GetTouchStatus();
    };
}

#endif