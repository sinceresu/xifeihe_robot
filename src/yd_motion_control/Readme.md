# yd_motion_control

任务执行管理,机器人控制节点


## Start Run

roslaunch yd_motion_control sport_control.launch



**roslaunch 参数**
<!-- 机器id -->
<param name="RobotId" type="int" value="1"/>
<!-- 狗窝坐标 -->
<param name="HomeX" type="double" value="43.469"/>
<param name="HomeY" type="double" value="0"/>
<param name="HomeZ" type="double" value="7.93"/>
<!-- back点坐标 -->
<param name="BackX" type="double" value="43.469"/>
<param name="BackY" type="double" value="0"/>
<param name="BackZ" type="double" value="5.735"/>
<!-- 控制模式超时时间 -->
<param name="SetModeTimeOut" type="double" value="600"/>
<!-- 底盘串口协议版本 -->
<param name="serial_version" value="v2201"/>
<!-- go_home_type 1: 前进到home点 -->
<param name="go_home_type" type="int" value="1"/>


**cfg/config 参数**
car-speed: 0.2
<!-- 最大运行速度 -->
car_speed_max: 0.6
<!-- 防跌落开关 -->
sensor-fall-back: false
sensor-fall-front: false
<!-- 防跌落限位值 -->
sensor-fall-value: 35
<!-- 超声波开关 -->
sensor-ultrasound-back: true
sensor-ultrasound-front: true
<!-- 超声波限位值 -->
sensor-ultrasound-value: 20
<!-- 防碰撞开关 -->
sensor-collision-back: true
sensor-collision-front: true
<!-- 充电限位值 -->
battery-charge-value: 95
battery-nocharge-value: 98
<!-- 底盘串口号 -->
serial-name: /dev/car
