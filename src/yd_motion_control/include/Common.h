#ifndef _ROBOT_MESSAGE_H_
#define _ROBOT_MESSAGE_H_

#include <string>
#include <vector>
// #include "json/json.h"
#ifdef ARM_BUILD
#include "json_arm/json.h"
#else
#include "json/json.h"
#endif

const float matrixArray[3][2] = {{0.4390, -1.6866}, {-6.9193, -22.3334}, {-1.3194, 0.9025}};

const float testdata[3][3] = {{-7.44504, -1.54022, -9.40118}, {-3.05995, -1.5171, -0.633805}, {-13.1692, -1.41655, -4.48325}};

#define CAR_BODY_LEN 1000 // mm
#define LASER_HEIGHT 265  // mm

#define MDL_OFFLINE_TIME_MAX 5 //ĳ��ģ��5��û����Ϣ��������Ϊ�˳�
#define MDL_DADA_QUEUE_MAX 5   //ÿ��ģ������ݶ��еı������ֵ

const float home_x = 0.0;
const float home_y = 0.0;
const float home_z = 0.0;
const float start_x = 0;
const float start_y = 0;
const float start_z = 0;

typedef enum _module_type_
{
	MDL_TYPE_PROGRAM = 0, //������

	MDL_TYPE_LOCATION = 1,		  //��λģ��
	MDL_TYPE_COMPASS = 2,		  //��������
	MDL_TYPE_LIDARSCANNING = 3,	  //�����·ɨ��
	MDL_TYPE_OBSTACLE = 4,		  //�ϰ�ɨ��
	MDL_TYPE_ROADRECOGNITION = 5, //��·ʶ��
	MDL_TYPE_FUZZYCONTROL = 6,	  //ģ������
	MDL_TYPE_PLANNING = 7,		  //·���滮
	MDL_TYPE_MOTOR = 8,			  //�����˵���
	MDL_TYPE_MAX = 32
} MODULE_TYPE;

typedef enum __message_type__
{
	MSG_TYPE_EXIT = 0, // ϵͳ�쳣������ֹͣ

	MSG_TYPE_WORK_STATE = 1,		//����״̬
	MSG_TYPE_BATTERY_EXCEPTION = 2, //����쳣
	MSG_TYPE_GOHOME_TASK = 3,		//����ԭ��
	MSG_TYPE_LOCATION = 4,			//	std::vector<std::strin	std::vector<std::string> cameraPose1;
	MSG_TYPE_ROAD_PLAN_TASK = 5,	//·���滮
	MSG_TYPE_ROAD_VIDEODECT = 6,	//�Ӿ���·ɨ��
	MSG_TYPE_LOG = 7,				//��־
	MSG_TYPE_RESTART = 8,			//ģ������

	MSG_TYPE_MAX = 254
} MSG_TYPE;

typedef enum __task_type__
{
	TASK_TYPE_START = 1,
	TASK_TYPE_TURN = 2,
	TASK_TYPE_END = 3,
	TASK_TYPE_MONITOR = 4,
	TASK_TYPE_TRANSFER = 5,
	TASK_TYPE_BACK = 6,
	TASK_TYPE_UNKNOWN = 0
} TASK_TYPE;

typedef struct _Point
{
	float x;
	float y;
	float z;
} SPoint;

typedef struct _road_plan
{
	std::string road;
	float LocX;
	float LocY;
	float LocZ;
	int fLocWidth;
	int nType;
	float fTurnAngle;
	std::string transfer_id;
	bool ongoing;
	bool end;
	bool finish;
	int align;
	std::vector<std::string> cameraPose;
	_road_plan() : finish(false) {}
} ROAD_PLAN;

typedef struct _robot_param
{
	float compass_angle;

	float lidar_front_left_dis;
	float lidar_front_right_dis;
	float lidar_back_left_dis;
	float lidar_back_right_dis;
	float lidar_angle;

	double video_roaddect;
} ROBOT_PARAM;
std::vector<std::string> split_yd(std::string str, std::string pattern);

#endif
