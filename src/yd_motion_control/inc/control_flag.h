
#ifndef KOBUKI_INERTIA_DATA_HPP__
#define KOBUKI_INERTIA_DATA_HPP__



enum motor_control_flag{
	ROBOT_STOP = 0,
	GO_STRAIGHT = 1,
	TURN_LEFT =2,
	TURN_RIGHT =3,
	ROBOT_TURN = 5,
	WATCH=6,
	BACK=7

};



#endif
