#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
import sys
import time
import thread
import threading
import rospy
import serial
import cv2
import math
import tf
import numpy as np
from std_msgs.msg import String
from sensor_msgs.msg import Image
#from cloud_platform.srv import *
from yidamsg.msg import Yida_pose
from yidamsg.msg import transfer
#from yidamsg.msg import InspectedResult
#from yidamsg.msg import Detect_Result

def callback_pose(data):
	#print('------------callback_pose-------------')
	carpose_msg = Yida_pose()
	trans = [0,0,0]
	try:
		(trans, rot) = listener.lookupTransform('/map', '/car_link', rospy.Time(0))
	except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
		pass
		print('************get trans data error!************')

	carpose_msg.x = trans[0]
	carpose_msg.z = trans[1]
	carpose_msg.y = trans[2]
	carpose_msg.anglez = data.anglez
	carpose_pub.publish(carpose_msg)

if __name__ == '__main__':
	rospy.init_node('car_point_node', anonymous=True)

	listener = tf.TransformListener()

	rospy.Subscriber("/robot_pose", Yida_pose, callback_pose, queue_size=1)
	carpose_pub = rospy.Publisher("/car_pose", Yida_pose, queue_size=1)

	rospy.spin()


