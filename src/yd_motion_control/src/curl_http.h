#ifndef __curl_http_H__
#define __curl_http_H__

#include <string>
#include "curl/curl.h"
#include "boost/filesystem/path.hpp"

bool uploadFile(std::string url, std::string file, std::string auth)
{
	boost::filesystem::path p(file);
	std::string license = "auth: " + license;

	CURL *curl;
	CURLcode res;

	curl_global_init(CURL_GLOBAL_ALL);

	struct curl_httppost *formpost = NULL;
	struct curl_httppost *lastptr = NULL;
	struct curl_slist *headerlist = NULL;

	headerlist = curl_slist_append(headerlist, license.data());
	const char buf[] = "Expect:";

	/* Fill in the file upload field */
	curl_formadd(&formpost,
				 &lastptr,
				 CURLFORM_COPYNAME, "file",
				 CURLFORM_FILE, file.data(),
				 CURLFORM_END);

	/* Fill in the filename field */
	curl_formadd(&formpost,
				 &lastptr,
				 CURLFORM_COPYNAME, "filename",
				 CURLFORM_COPYCONTENTS, p.filename().string().data(),
				 CURLFORM_END);

	/* Fill in the submit field too, even if this is rarely needed */
	curl_formadd(&formpost,
				 &lastptr,
				 CURLFORM_COPYNAME, "submit",
				 CURLFORM_COPYCONTENTS, "Submit",
				 CURLFORM_END);

	curl = curl_easy_init();
	/* initalize custom header list (stating that Expect: 100-continue is not wanted */
	headerlist = curl_slist_append(headerlist, buf);
	bool ret = false;
	if (curl)
	{
		curl_easy_setopt(curl, CURLOPT_URL, url.data());
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);
		curl_easy_setopt(curl, CURLOPT_HTTPPOST, formpost);

		/* Perform the request, res will get the return code */
		res = curl_easy_perform(curl);
		/* Check for errors */
		if (res != CURLE_OK)
		{
			fprintf(stderr, "curl_easy_perform() failed: %s\n",
					curl_easy_strerror(res));
		}
		else
		{
			long http_code = 0;
			curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &http_code);
			if (http_code == 200 && res != CURLE_ABORTED_BY_CALLBACK)
			{
				ret = true;
			}
			else
			{
				ret = false;
			}
		}

		/* always cleanup */
		curl_easy_cleanup(curl);

		/* then cleanup the formpost chain */
		curl_formfree(formpost);
		/* free slist */
		curl_slist_free_all(headerlist);
	}
	return ret;
}

#endif