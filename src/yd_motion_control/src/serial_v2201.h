﻿/*
 * @Author: your name
 * @Date: 2019-12-10 17:29:06
 * @LastEditTime: 2020-07-13 10:15:44
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /motor_shenzhen/src/serialcomm_lp_float.h
 */
#ifndef __SERIAL_V2201_H__
#define __SERIAL_V2201_H__
#include "serialcomm.h"

class Serial_v2201 : public SerialComm
{
public:
    Serial_v2201(int _robot_id);
    ~Serial_v2201();

public:
    void *comThreadIml();
    void sendSpeedToMcu(char smooth_type, float V, float W);
    void sendSpeedToWCXD(char smooth_type, float V, float W);
    void sendSpeedToDrive(float V, float W);
    void sendLifts(float h);
    void sendFan(bool status);
    void sendTest();
    void sendChargeStatus(int status);
    void sendMotorPower(int status);
    void sendMotorEnable(int status);
    void sendArmReset(int status);
    void sendArmHeight(int vlaue);
    void sendMinArmAction(int status);
    void sendPDStatus(int status);
    void sendLEDStatus(char status, char r, char g, char b, char s, char e, char t);
};

#endif //__SERIAL_V2201_H__
