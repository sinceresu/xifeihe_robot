#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/UInt8.h"
#include "std_msgs/Int16.h"
#include "std_msgs/Int32.h"
#include "std_msgs/Int64.h"
#include "std_msgs/Float32.h"
#include "std_msgs/Bool.h"
#include "geometry_msgs/Twist.h"
#include "geometry_msgs/Pose.h"
#include <diagnostic_msgs/DiagnosticArray.h>
#include <diagnostic_msgs/DiagnosticStatus.h>
#include <diagnostic_msgs/KeyValue.h>
#include <nav_msgs/Odometry.h>
#include "tf/transform_datatypes.h"
#include <pthread.h>
#include <thread>
#include <mutex>
// #include "../include/json/json.h"
// #ifdef ARM_BUILD
// #include "../include/json_arm/json.h"
// #else
// #include "../include/json/json.h"
// #endif
#include <time.h>
#include "yidamsg/TaskControl.h"
#include <iostream>
#include "../include/Common.h"
#include "../inc/control_flag.h"
#include <math.h>
#include "yidamsg/task_status.h"
#include "yidamsg/TaskList.h"
#include "yidamsg/fuzzy_task.h"
#include "yidamsg/Log.h"
#include "yidamsg/transfer.h"
#include "yidamsg/motor_control.h"
#include "yidamsg/ControlMode.h"
#include "yidamsg/car_status.h"
#include "yidamsg/Yida_pose.h"
#include "yidamsg/taskPlanStatus.h"
#include "yidamsg/TaskExecuteStatus.h"
#include "yidamsg/routeStatus.h"
#include "yidamsg/InspectedResult.h"
#include "yidamsg/manualControlParameters.h"
#include "yidamsg/taskControlParameters.h"
#include "yidamsg/taskJsonStatus.h"
#include "yidamsg/weather.h"
#include "yidamsg/ArmControl.h"
#include <string.h>
#include "yidamsg/run_status.h"
#include "yidamsg/task_pose.h"
#include "utilssport.h"
#include "serialcomm.h"
#include "serial_v2020.h"
#include "serial_v2021.h"
#include "serial_v2103.h"
#include "serial_v2201.h"

#include "base_deal.h"
#include "deal_v2020.h"
#include "deal_v2021.h"
#include "deal_v2103.h"
#include "deal_v2201.h"

#include "curl_http.h"
#include "sensor_msgs/Joy.h"
#include "../include/Common.h"

#include <syslog.h>
#include <signal.h>
#include <unistd.h>
#include <stdio.h>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <stdlib.h>
#include <sys/time.h>
#include <termios.h>
#include <semaphore.h>

#include "tf2_ros/transform_listener.h"
#include "tf2_ros/message_filter.h"
#include "message_filters/subscriber.h"
#include "tf2_geometry_msgs/tf2_geometry_msgs.h"
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Geometry>
#include <tf2/utils.h>
#include <geometry_msgs/TwistStamped.h>
#include "actionlib/client/simple_action_client.h"
#include "chargeroom.h"
#include <csignal>

#include <log4cxx/logger.h>
#include <log4cxx/propertyconfigurator.h>
#include <log4cxx/helpers/exception.h>

#include "param_server/server.h"
#include "charging_room_service_msgs/DoorControl.h"
#include "charging_room_service_msgs/DoorAction.h"
#include "charging_room_service_msgs/ChargingPileData.h"
#include "charging_room_service_msgs/ChargingPileDataStamped.h"
#include "yd_cloudplatform/CloudPlatControl.h"

#include "undistort_service_msgs/StartUndistort.h"
#include "undistort_service_msgs/StopUndistort.h"
#include "streaming_service_msgs/start_streaming.h"
#include "streaming_service_msgs/stop_streaming.h"
#include "streaming_service_msgs/ColorizeInfo.h"
#include "audio_capture_msgs/CaptureResult.h"
#include "audio_capture_msgs/AudioCapture.h"

#include "nav_msgs/Path.h"

#include <map>
#include <rapidjson/istreamwrapper.h>
#include <rapidjson/encodedstream.h>
#include <rapidjson/document.h>
#include <rapidjson/rapidjson.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>

void signalHandler(int signum)
{
	std::cout << "Interrupt signal (" << signum << ") received.\n";
	// 清理并关闭
	// 终止程序
	exit(signum);
}
using namespace std;
using namespace log4cxx;
using namespace log4cxx::helpers;
using namespace rapidjson;

static const string CONF_LOG_FILE = "/home/yd/workspace/src/yd_motion_control/log.properties";
LoggerPtr logger(Logger::getRootLogger());

time_t GetTimeStamp()
{
	/* std::chrono::milliseconds ms = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch());
	std::time_t timeStamp = ms.count(); */
	//
	std::chrono::time_point<std::chrono::system_clock, std::chrono::milliseconds> tp = std::chrono::time_point_cast<std::chrono::milliseconds>(std::chrono::system_clock::now());
	time_t timeStamp = tp.time_since_epoch().count();
	return timeStamp;
}

std::string GetCurrentDate(const std::string &_format)
{
	time_t nowtime;
	nowtime = time(NULL);
	char tmp[64];
#ifdef __linux__
	strftime(tmp, sizeof(tmp), _format.c_str(), localtime(&nowtime));
#elif _WIN32
	struct tm t;
	errno_t err = localtime_s(&t, &nowtime);
	if (err != 0)
	{
		return "";
	}
	strftime(tmp, sizeof(tmp), _format.c_str(), &t);
#elif __ANDROID__
#else
#endif

	return tmp;
}

#define PLAT_TYPE 1
// #define ROBOT_ID 1
int ROBOT_ID = 0;
string serial_version;
int door_control_type = 1;
int go_home_type = 1;
std::string audio_dir;

#define CAR_FORWARD 1
#define CAR_BACKWARD 2
#define CAR_TURN 3

#define JSON_STATUS_FINISH 1
#define JSON_STATUS_DOING 2

#define TASK_NO_EXECUTE 4	   // 任务未执行
#define TASK_DOING 3		   // 正在执行
#define TASK_PRE_DOING 6	   // 欲执行
#define TASK_HAS_EXECUTED 0	   // 已完成
#define TASK_STOP 1			   // 停止
#define TASK_OVER_PERIOD 5	   // 任务超期
#define TASK_PAUSE 2		   // 任务暂停
#define TASK_OVER_TIME 7	   // 任务超时
#define TASK_PAUSE_RESTORE 10  // 任务暂停恢复
#define TASK_INTERRUPT_PAUSE 8 // 任务事件打断

param_server::Server *param_server_;
std::mutex serial_mtx;
std::shared_ptr<SerialComm> serial_comm;
std::shared_ptr<BaseDeal> sensor_deal;

ros::Publisher heart_pub;
ros::Publisher motor_pub;
ros::Publisher transfer_pub;
ros::Publisher work_status_pub;
ros::Publisher task_execute_status_pub;
ros::Publisher pose_stop_pub;
ros::Publisher onebuttontask_pub;
ros::Publisher control_mode_pub; // 用于1S发布一次
ros::Publisher task_pub;
ros::Publisher route_status_pub;
ros::Publisher start_end_road_pub;
ros::Publisher control_mode_change_pub;
ros::Publisher tasks_json_status_pub;
ros::Publisher weather_pub;
ros::Publisher restart_pub;
ros::Publisher detect_result_pub;
ros::Publisher ftp_upload_pub;
ros::Publisher append_data_pub;
ros::Publisher path_pub;

ros::ServiceClient doorControl_client;
ros::ServiceClient GTaskStatusManagerClientToRobot;
ros::ServiceClient cloudplatform_client;
ros::ServiceClient undistort_start_client;
ros::ServiceClient undistort_stop_client;
ros::ServiceClient stream_start_client;
ros::ServiceClient stream_stop_client;
ros::ServiceClient audio_capture_client;

tf2_ros::Buffer buffer_;
sem_t charge_sem;
bool bStart(true);
bool bStartSendSubTask(false);
EMotionStep step = EMotionStep::None;
EActionStatus action = EActionStatus::None;
bool bObstacle(false);
bool bPositionError(false);
int work_status = 0;
bool goHome_doing = false;
// vector<ROAD_PLAN *> vePlan;
vector<ROAD_PLAN> vePlan;
bool pose_flag = false;
bool clear_task_flg = false;
bool task_doing = false;
int transfer_flag = 0;
int biao_flag = 0;
int task_id = 0;
std::string recon_type = "";
unsigned char task_execute_status = 255;
unsigned char pointWatchNum = 0;
char location_transfer = 0;
int g_mode = 0;
int ultrasound_counter_b = 0;
int ultrasound_counter_f = 0;
yidamsg::Yida_pose GCurrentPose;
int g_location_falut_code = 0;
int g_motion_falut_code = 0;
int g_sensor_falut_code = 0;
// int g_connect_num = 0;
char g_car_action_status = CAR_FORWARD;
bool go_home_stop = false;

#define CLOSE_DOOR 0
#define OPEN_DOOR 1

enum struct EDoorStatus
{
	Unknown = 0,
	Open,
	Close,
	Half
};

enum struct ETouchStatus
{
	Unknown = 0,
	ON,
	OFF,
	Left,
	Right
};

typedef struct _Charging_Pile_t
{
	char door_status;
	char touch_status;
	struct timespec time_stamp;
	_Charging_Pile_t() : door_status(0), touch_status(0) {}
} Charging_Pile_t;

Charging_Pile_t charging_pile_data;

float car_speed;
std::string serial_name;

typedef actionlib::SimpleActionClient<charging_room_service_msgs::DoorAction> DoorActionClient;

bool start_undistort()
{
	undistort_service_msgs::StartUndistort cmd;
	if (undistort_start_client.call(cmd))
	{
		if (cmd.response.status.code == 0)
		{
			return true;
		}
	}
	return false;
}
bool stop_undistort()
{
	undistort_service_msgs::StopUndistort cmd;
	if (undistort_stop_client.call(cmd))
	{
		if (cmd.response.status.code == 0)
		{
			return true;
		}
	}
	return false;
}
bool b_colorize_stream_status = false;
bool start_stream()
{
	Document obj_doc;
	Document::AllocatorType &allocator = obj_doc.GetAllocator();
	obj_doc.SetObject();
	Value robotId_obj(kStringType);
	robotId_obj.SetString(std::to_string(ROBOT_ID).c_str(), allocator);
	obj_doc.AddMember("device_id", robotId_obj, allocator);
	Value taskId_obj(kStringType);
	taskId_obj.SetString(std::to_string(task_id).c_str(), allocator);
	obj_doc.AddMember("task_id", taskId_obj, allocator);
	rapidjson::StringBuffer task_buf;
	rapidjson::Writer<rapidjson::StringBuffer> writer_task(task_buf);
	obj_doc.Accept(writer_task);
	std::string task_data = std::string(task_buf.GetString());

	b_colorize_stream_status = false;
	streaming_service_msgs::start_streaming cmd;
	cmd.request.camera_id = to_string(ROBOT_ID);
	cmd.request.task_data = task_data;
	if (stream_start_client.call(cmd))
	{
		if (cmd.response.status.code == 0)
		{
			return true;
		}
	}
	return false;
}
bool stop_stream()
{
	b_colorize_stream_status = false;
	streaming_service_msgs::stop_streaming cmd;
	cmd.request.camera_id = to_string(ROBOT_ID);
	if (stream_stop_client.call(cmd))
	{
		return true;
	}
	return false;
}

bool audio_capture_op(int type, std::string format, std::string file, int time)
{
	audio_capture_msgs::AudioCapture cmd;
	cmd.request.type = type;
	cmd.request.format = format;
	cmd.request.file = file;
	cmd.request.time = time;
	if (audio_capture_client.call(cmd))
	{
		if (cmd.response.result == 0)
		{
			return true;
		}
	}
	return false;
}

void readConfig()
{
	if (param_server_->exist("car-speed"))
	{
		param_server_->get("car-speed", car_speed);
	}
	if (param_server_->exist("car_speed_max"))
	{
		param_server_->get("car_speed_max", utilssport::car_speed_max);
	}
	if (param_server_->exist("sensor-fall-back"))
	{
		bool result = false;
		param_server_->get("sensor-fall-back", sensor_deal->sensor_fall_back);
		// sensor_deal->sensor_fall_back = result;
		ROS_INFO("sensor_fall_back:%i", sensor_deal->sensor_fall_back);
	}
	if (param_server_->exist("sensor-fall-front"))
	{
		param_server_->get("sensor-fall-front", sensor_deal->sensor_fall_front);
	}
	if (param_server_->exist("sensor-fall-value"))
	{
		param_server_->get("sensor-fall-value", sensor_deal->sensor_fall_value);
	}
	if (param_server_->exist("sensor-ultrasound-back"))
	{
		param_server_->get("sensor-ultrasound-back", sensor_deal->sensor_ultrasound_back);
	}
	if (param_server_->exist("sensor-ultrasound-front"))
	{
		param_server_->get("sensor-ultrasound-front", sensor_deal->sensor_ultrasound_front);
	}
	if (param_server_->exist("sensor-ultrasound-value"))
	{
		param_server_->get("sensor-ultrasound-value", sensor_deal->sensor_ultrasound_value);
	}
	if (param_server_->exist("sensor-collision-back"))
	{
		param_server_->get("sensor-collision-back", sensor_deal->sensor_collision_back);
	}
	if (param_server_->exist("sensor-collision-front"))
	{
		param_server_->get("sensor-collision-front", sensor_deal->sensor_collision_front);
	}
	if (param_server_->exist("battery-charge-value"))
	{
		param_server_->get("battery-charge-value", sensor_deal->battery_charge_value);
	}
	if (param_server_->exist("battery-nocharge-value"))
	{
		param_server_->get("battery-nocharge-value", sensor_deal->battery_nocharge_value);
	}
	if (param_server_->exist("serial-name"))
	{
		param_server_->get("serial-name", serial_name);
	}
	std::cout << "car_speed:" << car_speed << std::endl;
	std::cout << "sensor_fall_back:" << sensor_deal->sensor_fall_back << std::endl;
	std::cout << "sensor_fall_front:" << sensor_deal->sensor_fall_front << std::endl;
	std::cout << "sensor_fall_value:" << sensor_deal->sensor_fall_value << std::endl;
	std::cout << "sensor_ultrasound_back:" << sensor_deal->sensor_ultrasound_back << std::endl;
	std::cout << "sensor_ultrasound_front:" << sensor_deal->sensor_ultrasound_front << std::endl;
	std::cout << "sensor_ultrasound_value:" << sensor_deal->sensor_ultrasound_value << std::endl;
	std::cout << "sensor_collision_back:" << sensor_deal->sensor_collision_back << std::endl;
	std::cout << "sensor_collision_front:" << sensor_deal->sensor_collision_front << std::endl;
	std::cout << "battery_charge_value:" << sensor_deal->battery_charge_value << std::endl;
	std::cout << "battery_nocharge_value:" << sensor_deal->battery_nocharge_value << std::endl;
	std::cout << "serial_name:" << serial_name << std::endl;
}

void callback(param_server::SimpleType &config)
{
	for (auto &kv : config)
	{
		ROS_INFO("callback key:%s value:%s", kv.first.c_str(), kv.second.c_str());
	}
	readConfig();
}

void stopHomeCallback(const std_msgs::String msg)
{
	go_home_stop = !go_home_stop;
	std::cout << "go_home_stop:" << go_home_stop << std::endl;
}

void initLogSys()
{
	try
	{
		PropertyConfigurator::configure(CONF_LOG_FILE);
		LOG4CXX_INFO(logger, "Init() Log success.");
	}
	catch (Exception &)
	{
	}
}

std::vector<std::string> splitYd(std::string str, std::string pattern)
{
	std::string::size_type pos;
	std::vector<std::string> result;
	str += pattern;
	int size = str.size();

	for (int i = 0; i < size; i++)
	{
		pos = str.find(pattern, i);
		if (pos < size)
		{
			std::string s = str.substr(i, pos - i);
			result.push_back(s);
			i = pos + pattern.size() - 1;
		}
	}
	return result;
}

bool linPptimalAngle(float currentAngel, std::string road, float &angel)
{
	std::string road_line = road;
	std::vector<std::string> vePoint = splitYd(road_line, "/");
	if (vePoint.size() != 2)
	{
		return false;
	}
	std::string road_start = vePoint[0];
	std::vector<std::string> ve_sPoint = splitYd(road_start, ",");
	if (ve_sPoint.size() != 3)
	{
		return false;
	}
	SPoint s_point;
	s_point.x = atof(ve_sPoint[0].c_str());
	s_point.y = atof(ve_sPoint[1].c_str());
	s_point.z = atof(ve_sPoint[2].c_str());
	std::string road_end = vePoint[1];
	std::vector<std::string> ve_ePoint = splitYd(road_end, ",");
	if (ve_ePoint.size() != 3)
	{
		return false;
	}
	SPoint e_point;
	e_point.x = atof(ve_ePoint[0].c_str());
	e_point.y = atof(ve_ePoint[1].c_str());
	e_point.z = atof(ve_ePoint[2].c_str());
	float line_angle1 = calcLineAngle(s_point.x, s_point.y, e_point.x, e_point.y);
	float lDiff = 0.0;
	float rDiff = 0.0;
	if (line_angle1 == currentAngel)
	{
		lDiff = 0.0;
		rDiff = 0.0;
	}
	else if (line_angle1 > currentAngel)
	{
		lDiff = line_angle1 - currentAngel;
		rDiff = 360.0 - (line_angle1 - currentAngel);
	}
	else
	{
		lDiff = 360.0 - (currentAngel - line_angle1);
		rDiff = currentAngel - line_angle1;
	}
	float diff1 = lDiff >= rDiff ? rDiff : lDiff;
	//
	float line_angle2 = calcLineAngle(e_point.x, e_point.y, s_point.x, s_point.y);
	lDiff = 0.0;
	rDiff = 0.0;
	if (line_angle2 == currentAngel)
	{
		lDiff = 0.0;
		rDiff = 0.0;
	}
	else if (line_angle2 > currentAngel)
	{
		lDiff = line_angle2 - currentAngel;
		rDiff = 360.0 - (line_angle2 - currentAngel);
	}
	else
	{
		lDiff = 360.0 - (currentAngel - line_angle2);
		rDiff = currentAngel - line_angle2;
	}
	float diff2 = lDiff >= rDiff ? rDiff : lDiff;
	if (diff1 > diff2)
	{
		angel = line_angle2;
	}
	else
	{
		angel = line_angle1;
	}
	return true;
}

int door_control(void *args, char flag)
{
	ros::NodeHandle ros_nh = *((ros::NodeHandle *)args);

	float home_x, home_y, home_z, back_x, back_y, back_z;
	ros_nh.getParam("HomeX", home_x);
	ros_nh.getParam("HomeY", home_y);
	ros_nh.getParam("HomeZ", home_z);
	ros_nh.getParam("BackX", back_x);
	ros_nh.getParam("BackY", back_y);
	ros_nh.getParam("BackZ", back_z);

	if (calcToLineDistance(GCurrentPose.x, GCurrentPose.y, back_x, back_y, home_x, home_y) > 0.3)
	{
		return -1;
	}

	int nRet = -1;

	DoorActionClient door("/charging/room/door", true);
	// wait for the action server to start
	bool start_before_timeout = door.waitForServer(ros::Duration(30.0));
	if (!start_before_timeout)
	{
		return -1;
	}

	charging_room_service_msgs::DoorGoal goal;
	goal.status = flag;
	door.sendGoal(goal);

	// wait for the action to return
	bool finished_before_timeout = door.waitForResult(ros::Duration(180.0));
	if (finished_before_timeout)
	{
		actionlib::SimpleClientGoalState state = door.getState();
		ROS_INFO("Action finished: %s", state.toString().c_str());
		if (state == state.SUCCEEDED)
		{
			nRet = 0;
		}
	}
	else
		ROS_INFO("Action did not finish before the time out.");

	return nRet;
}

void taskStatusPub(float dis, float alldis, int task_type, float start_x, float start_y, float end_x, float end_y)
{
	static int i = 0;
	i++;
	if (i % 8 == 0)
	{
		yidamsg::task_status msg;
		msg.alldis = alldis;
		msg.dis = dis;
		msg.start_x = start_x;
		msg.start_y = start_y;
		msg.end_x = end_x;
		msg.end_y = end_y;
		msg.direction = (task_type == TASK_TYPE_BACK) ? 0 : 1;
		task_pub.publish(msg);
	}
}
void taskJsonStatusPub(int index, char status)
{
	yidamsg::taskJsonStatus msg;
	msg.robot_id = ROBOT_ID;
	msg.task_history_id = task_id;
	msg.index = index;
	msg.status = status;
	tasks_json_status_pub.publish(msg);
}
void backToHomeRoadPub(char start_end_flg)
{
	yidamsg::ControlMode msg;
	msg.robot_id = ROBOT_ID;
	msg.mode = start_end_flg;
	start_end_road_pub.publish(msg);
}

void TaskStatusPub(float dis, float alldis, int task_type, float start_x, float start_y, float end_x, float end_y)
{
	static int i = 0;
	i++;
	if (i % 8 == 0)
	{
		yidamsg::task_status msg;
		msg.alldis = alldis;
		msg.dis = dis;
		msg.start_x = start_x;
		msg.start_y = start_y;
		msg.end_x = end_x;
		msg.end_y = end_y;
		msg.direction = (task_type == TASK_TYPE_BACK) ? 0 : 1;
		task_pub.publish(msg);
	}
}
void transfrom(geometry_msgs::PoseStamped pose, geometry_msgs::TransformStamped transform)
{
	// 1.p1 world position
	double p1x = pose.pose.position.x;
	double p1y = pose.pose.position.y;
	Eigen::Vector3d t1 = Eigen::Vector3d(p1x, p1y, 0);
	Eigen::Quaterniond q1(pose.pose.orientation.w, pose.pose.orientation.x, pose.pose.orientation.y, pose.pose.orientation.z);

	// 2. t12 value
	double t12x = transform.transform.translation.x;
	double t12y = transform.transform.translation.y;
	Eigen::Vector3d t12 = Eigen::Vector3d(t12x, t12y, 0);
	Eigen::Quaterniond q12(transform.transform.rotation.w, transform.transform.rotation.x, transform.transform.rotation.y, transform.transform.rotation.z);

	// calc p2
	Eigen::Quaterniond q2 = q1 * q12;
	Eigen::Vector3d t2 = t1 + q1.toRotationMatrix() * t12;

	geometry_msgs::PoseStamped center_pose;
	center_pose.pose.position.x = t2(0);
	center_pose.pose.position.y = t2(1);
	center_pose.pose.position.z = pose.pose.position.z;

	center_pose.pose.orientation.x = q2.x();
	center_pose.pose.orientation.y = q2.y();
	center_pose.pose.orientation.z = q2.z();
	center_pose.pose.orientation.w = q2.w();

	// double roll, pitch, yaw;
	// GCurrentPose.x = center_pose.pose.position.x;
	// GCurrentPose.y = center_pose.pose.position.y;
	// GCurrentPose.z = center_pose.pose.position.z;
	// tf::Quaternion quat;
	// tf::quaternionMsgToTF(center_pose.pose.orientation, quat);
	// tf::Matrix3x3(quat).getRPY(roll, pitch, yaw); //进行转换
	// GCurrentPose.anglez = yaw;

	double roll, pitch, yaw;
	GCurrentPose.x = pose.pose.position.x;
	GCurrentPose.y = pose.pose.position.y;
	GCurrentPose.z = pose.pose.position.z;
	tf::Quaternion quat;
	tf::quaternionMsgToTF(pose.pose.orientation, quat);
	tf::Matrix3x3(quat).getRPY(roll, pitch, yaw); // 进行转换
	GCurrentPose.anglez = yaw;

	// std::cout << "center_pose:" << center_pose << std::endl;
}

void poseCallback(const nav_msgs::OdometryConstPtr &pose_msg)
{
	geometry_msgs::PoseStamped lidar_pose;
	lidar_pose.header.frame_id = "lidar_pose";
	ros::Time current_time = ros::Time::now();
	lidar_pose.header.stamp = current_time;
	lidar_pose.pose.position.x = pose_msg->pose.pose.position.x;
	lidar_pose.pose.position.y = pose_msg->pose.pose.position.y;
	lidar_pose.pose.position.z = pose_msg->pose.pose.position.z;
	lidar_pose.pose.orientation.x = pose_msg->pose.pose.orientation.x;
	lidar_pose.pose.orientation.y = pose_msg->pose.pose.orientation.y;
	lidar_pose.pose.orientation.z = pose_msg->pose.pose.orientation.z;
	lidar_pose.pose.orientation.w = pose_msg->pose.pose.orientation.w;

	geometry_msgs::TransformStamped transform;
	try
	{
		transform = buffer_.lookupTransform("lidar_pose", "robot_pose",
											ros::Time(0));
		transfrom(lidar_pose, transform);
	}
	catch (const std::exception &e)
	{
		std::cerr << e.what() << '\n';
	}
	pose_flag = true;
}
void poseStopPub(bool boolValue)
{
	std_msgs::Bool msg_cp;
	msg_cp.data = boolValue;
	pose_stop_pub.publish(msg_cp);
}

std::vector<std::string> transfer_data;
// time,stop_id,point_id,point_type,point_param,[object_id],robot_id,task_id

void meterCallback(const std_msgs::String msg)
{
	ROS_INFO("meterCallback start");
	string cam_now_status = msg.data;
	std::vector<std::string> lists = splitYd(cam_now_status, "/");
	ROS_INFO("receive meter_flag data:%s", cam_now_status.c_str());
	biao_flag = 1;
	poseStopPub(false);
	ROS_INFO("meterCallback end");
}

void localization_heartbeat_callback(const diagnostic_msgs::DiagnosticArray msg)
{
	static int no_imu_count = 0;
	static int no_twist_count = 0;
	static int no_odometry_count = 0;
	char fault_status = 0x00;
	std::vector<diagnostic_msgs::DiagnosticStatus>::const_iterator it;
	for (it = msg.status.begin(); it != msg.status.end(); ++it)
	{
		if (it->name == "/online_node")
		{
			if (it->message == "NoIMUData")
			{
				no_imu_count++;
				if (no_imu_count >= 3)
				{
					fault_status = fault_status | (1 << 0);
					ROS_WARN("Location falut; fault_code:%d, message:%s", (1 << 0), it->message.c_str());
				}
			}
			if (it->message == "NOTwistData")
			{
				no_twist_count++;
				if (no_twist_count >= 3)
				{
					fault_status = fault_status | (1 << 1);
					ROS_WARN("Location falut; fault_code:%d, message:%s", (1 << 1), it->message.c_str());
				}
			}
			if (it->message == "NoOdometryData")
			{
				no_odometry_count++;
				if (no_odometry_count >= 3)
				{
					no_odometry_count = 3;
					fault_status = fault_status | (1 << 2);
					ROS_WARN("Location falut; fault_code:%d, message:%s", (1 << 2), it->message.c_str());
				}
			}
		}
	}
	if (((fault_status >> 0) & 0x01) != 1)
	{
		no_imu_count = 0;
	}
	if (((fault_status >> 1) & 0x01) != 1)
	{
		no_twist_count = 0;
	}
	if (((fault_status >> 2) & 0x01) != 1)
	{
		no_odometry_count = 0;
	}

	g_location_falut_code = fault_status;
}

bool b_partial_discharge_status = false;
int n_b_partial_discharge_count = 0;
std::vector<diagnostic_msgs::KeyValue> pd_datas;
void partial_discharge_callback(const diagnostic_msgs::DiagnosticStatus msg)
{
	ROS_INFO("partial_discharge_callback");
	if (b_partial_discharge_status)
	{
		pd_datas = msg.values;
		if (n_b_partial_discharge_count == 1)
		{
			// std::string pd_value = "";
			// for (vector<diagnostic_msgs::KeyValue>::iterator v_item = pd_datas.begin(); v_item != pd_datas.end(); v_item++)
			// {
			// 	pd_value += v_item->value;
			// 	pd_value += ",";
			// }
			// //
			// int t_size = transfer_data.size();
			// yidamsg::InspectedResult inspected_msg;
			// inspected_msg.camid = ROBOT_ID;
			// inspected_msg.equipid = transfer_data[2] + "/" + transfer_data[t_size - 2] + "/" + transfer_data[t_size - 1];
			// inspected_msg.result = pd_value;
			// inspected_msg.success = true;
			// detect_result_pub.publish(inspected_msg);
		}
		n_b_partial_discharge_count++;

		// biao_flag = 1;
		// poseStopPub(false);
	}
}

bool b_audio_capture_status = false;
int n_audio_capture_result = 0;
void audio_capture_callback(const audio_capture_msgs::CaptureResult msg)
{
	if (b_audio_capture_status)
	{
		if (msg.result == 0)
		{
			n_audio_capture_result = 1;
		}
		else
		{
			n_audio_capture_result = -1;
		}
	}
}

bool b_ftp_upload_status = false;
int n_ftp_upload_result = 0;
void ftp_upload_result_callback(const std_msgs::String msg)
{
	if (b_ftp_upload_status)
	{
		std::vector<std::string> vePoint = splitYd(msg.data, ":");
		if (vePoint.size() >= 2)
		{
			if (vePoint[0] == "true")
			{
				n_ftp_upload_result = 1;
			}
			else
			{
				n_ftp_upload_result = -1;
			}
		}
		else
		{
			n_ftp_upload_result = -1;
		}
	}
}

void pd_collection_fun(std::vector<std::string> data)
{
	if (data.size() >= 5)
	{
		/* code */
	}
	else
	{
		biao_flag = 0;
		return;
	}
	std::string device_id = data[2];
	std::vector<std::string> device_point = splitYd(data[4], ",");
	if (device_point.size() >= 3)
	{
		/* code */
	}
	else
	{
		biao_flag = 0;
		return;
	}
	float x_device = atof(device_point[0].c_str());
	float y_device = atof(device_point[1].c_str());
	float z_device = atof(device_point[2].c_str());

	float max_arm_height = GCurrentPose.z + 0.180;
	int goal_height = (z_device - max_arm_height) * 1000;
	goal_height = goal_height < 0 ? 0 : goal_height;
	goal_height = goal_height > 1000 ? 1000 : goal_height;
	//
	bool b_achieve_height = false;
	bool b_achieve_touch = false;
	bool b_data_collection = false;
	int n_time_out = 0;
	ros::Rate arm_rate(1);
	//
	{
		if (serial_comm)
		{
			serial_comm->sendMinArmAction(1);
			serial_comm->sendArmHeight(goal_height);
		}
		n_time_out = 0;
		while (ros::ok && task_doing)
		{
			// if (abs(sensor_deal->max_arm_height - goal_height) < 10 && sensor_deal->min_arm_origin_status == 1)
			if (abs(sensor_deal->max_arm_height - goal_height) < 15 && sensor_deal->min_arm_length < 15)
			{
				ROS_INFO("arm height done.");
				b_achieve_height = true;
				break;
			}
			n_time_out++;
			if (n_time_out >= 30)
			{
				ROS_WARN("arm height timeout.");
				break;
			}
			ros::spinOnce();
			arm_rate.sleep();
		}
	}
	//
	{
		if (b_achieve_height && task_doing)
		{
			if (serial_comm)
			{
				serial_comm->sendMinArmAction(0);
			}
			n_time_out = 0;
			while (ros::ok && task_doing)
			{
				if (sensor_deal->min_arm_touch_status == 1)
				{
					ROS_INFO("arm touch done.");
					b_achieve_touch = true;
					break;
				}
				if (sensor_deal->min_arm_end_status == 1)
				{
					ROS_WARN("min arm max.");
					break;
				}
				n_time_out++;
				if (n_time_out >= 30)
				{
					ROS_WARN("arm touch timeout.");
					break;
				}
				ros::spinOnce();
				arm_rate.sleep();
			}
		}
	}
	//
	{
		if (b_achieve_touch && task_doing)
		{
			if (serial_comm)
			{
				// open
				b_partial_discharge_status = true;
				n_b_partial_discharge_count = 0;
				pd_datas.clear();
				serial_comm->sendPDStatus(0);
			}
			n_time_out = 0;
			while (ros::ok && task_doing)
			{
				n_time_out++;
				if (n_time_out >= 60)
				{
					if (b_partial_discharge_status)
					{
						if (n_b_partial_discharge_count > 0)
						{
							if (pd_datas.size() > 0)
							{
								std::string pd_value = "";
								for (vector<diagnostic_msgs::KeyValue>::iterator v_item = pd_datas.begin(); v_item != pd_datas.end(); v_item++)
								{
									pd_value += v_item->value;
									pd_value += ",";
								}
								//
								int t_size = transfer_data.size();
								yidamsg::InspectedResult inspected_msg;
								inspected_msg.camid = ROBOT_ID;
								inspected_msg.equipid = transfer_data[2] + "/" + transfer_data[t_size - 2] + "/" + transfer_data[t_size - 1];
								inspected_msg.result = pd_value;
								inspected_msg.success = true;
								detect_result_pub.publish(inspected_msg);
								ROS_INFO("pd collection done.");
								b_data_collection = true;
							}
						}
					}
					if (!b_data_collection)
					{
						int t_size = transfer_data.size();
						yidamsg::InspectedResult inspected_msg;
						inspected_msg.camid = ROBOT_ID;
						inspected_msg.equipid = transfer_data[2] + "/" + transfer_data[t_size - 2] + "/" + transfer_data[t_size - 1];
						inspected_msg.result = "10003";
						inspected_msg.success = false;
						detect_result_pub.publish(inspected_msg);
						ROS_WARN("pd collection failed.");
					}
					break;
				}
				ros::spinOnce();
				arm_rate.sleep();
			}
			if (serial_comm)
			{
				// close
				b_partial_discharge_status = false;
				n_b_partial_discharge_count = 0;
				pd_datas.clear();
				serial_comm->sendPDStatus(1);
			}
		}
	}

__end:
	// if (serial_comm)
	// {
	// 	serial_comm->sendMinArmAction(1);
	// 	serial_comm->sendArmReset(0);
	// }
	// int arm_reset_time = 0;
	// ros::Rate arm_reset_rate(1);
	// while (ros::ok)
	// {
	// 	// if (sensor_deal->max_arm_height < 10 && sensor_deal->min_arm_origin_status == 1)
	// 	if (sensor_deal->max_arm_height < 10 && sensor_deal->min_arm_length < 10)
	// 	{
	// 		ROS_INFO("task arm reset finish.");
	// 		break;
	// 	}
	// 	arm_reset_time++;
	// 	if (arm_reset_time >= 15)
	// 	{
	// 		ROS_WARN("arm reset timeout.");
	// 		break;
	// 	}
	// 	ros::spinOnce();
	// 	arm_reset_rate.sleep();
	// }
	if (serial_comm)
	{
		serial_comm->sendMinArmAction(1);
		int arm_reset_time = 0;
		ros::Rate arm_reset_rate(1);
		while (ros::ok)
		{
			if (sensor_deal->min_arm_length < 15)
			{
				ROS_INFO("task min arm reset finish.");
				break;
			}
			arm_reset_time++;
			if (arm_reset_time >= 15)
			{
				ROS_WARN("min arm reset timeout.");
				break;
			}
			ros::spinOnce();
			arm_reset_rate.sleep();
		}
	}
	if (serial_comm)
	{
		serial_comm->sendArmReset(0);
		int arm_reset_time = 0;
		ros::Rate arm_reset_rate(1);
		while (ros::ok)
		{
			if (sensor_deal->max_arm_height < 15)
			{
				ROS_INFO("task max arm reset finish.");
				break;
			}
			arm_reset_time++;
			if (arm_reset_time >= 15)
			{
				ROS_WARN("max arm reset timeout.");
				break;
			}
			ros::spinOnce();
			arm_reset_rate.sleep();
		}
	}
	//
	biao_flag = 1;
	poseStopPub(false);
}

// audio
void audio_capture_fun(std::vector<std::string> data)
{
	int data_time = 30;
	int t_size = data.size();
	std::string name = data[2] + "_" + data[t_size - 2] + "_" + std::to_string(GetTimeStamp()) + ".wav";
	std::string audio_file = audio_dir + "/" + name;
	// 年 / 月 / 日 / 巡视任务编码 / Audio / 巡检点位ID_机器人编码_时间.jpg
	std::string local_file = audio_file;
	std::string area_path = GetCurrentDate("%Y") + "/" + GetCurrentDate("%m") + "/" + GetCurrentDate("%d") + "/" + transfer_data[t_size - 1] + "/Audio/";
	std::string target_file = area_path + "/" + name;
	if (audio_capture_op(1, "wav", audio_file + "", data_time))
	{
		b_audio_capture_status = true;
		n_audio_capture_result = 0;
	}
	ros::Rate audio_rate(1);
	int audio_time = 0;
	while (ros::ok && task_doing)
	{
		if (b_audio_capture_status)
		{
			if (n_audio_capture_result == 1)
			{
				b_ftp_upload_status = true;
				n_ftp_upload_result = 0;

				std::string data = local_file + ";" + target_file;
				std_msgs::String ftp_msg;
				ftp_msg.data = data;
				ftp_upload_pub.publish(ftp_msg);

				break;
			}
			if (n_audio_capture_result == -1)
			{
				break;
			}
			audio_time++;
			if (audio_time >= (data_time + 10))
			{
				ROS_WARN("audio timeout.");
				break;
			}
		}
		ros::spinOnce();
		audio_rate.sleep();
	}
	b_audio_capture_status = false;
	n_audio_capture_result = 0;
	//
	bool audio_result = false;
	int upload_time = 0;
	while (ros::ok && task_doing)
	{
		if (b_ftp_upload_status)
		{
			if (n_ftp_upload_result == 1)
			{
				audio_result = true;
				break;
			}
			if (n_ftp_upload_result == -1)
			{
				break;
			}
			upload_time++;
			if (upload_time >= 30)
			{
				ROS_WARN("upload timeout.");
				break;
			}
		}
		ros::spinOnce();
		audio_rate.sleep();
	}
	b_ftp_upload_status = false;
	n_ftp_upload_result = 0;
	yidamsg::InspectedResult inspected_msg;
	inspected_msg.camid = ROBOT_ID;
	inspected_msg.equipid = data[2] + "/" + data[t_size - 2] + "/" + data[t_size - 1];
	// inspected_msg.result;
	inspected_msg.address = "";
	inspected_msg.success = false;
	if (audio_result)
	{
		inspected_msg.address = target_file;
		inspected_msg.success = true;
	}
	if (task_doing)
	{
		detect_result_pub.publish(inspected_msg);
	}
	//
	biao_flag = 1;
	poseStopPub(false);
}

void transfer_handle_fun(std::string data, void *p)
{
	std::vector<std::string> transfer_msg = splitYd(data, "/");
	if (transfer_msg.size() >= 4)
	{
		/* code */
	}
	else
	{
		biao_flag = 0;
		return;
	}

	transfer_data = transfer_msg;

	// 局放
	if (transfer_msg[3] == "7")
	{
		pd_collection_fun(transfer_msg);
	}
	// 音频
	if (transfer_msg[3] == "8")
	{
		audio_capture_fun(transfer_msg);
	}
}

void transfer_callback(const yidamsg::transfer &msg)
{
	if (msg.flag == 0)
	{
		std::thread pd_Thread = std::thread(transfer_handle_fun, msg.data, nullptr);
		pd_Thread.detach();
	}
	else if (msg.flag == 1)
	{
	}
	else
	{
	}
}

void stream_result_callback(const streaming_service_msgs::ColorizeInfo &msg)
{
	if (msg.type == 1)
	{
		b_colorize_stream_status = true;
	}
	else
	{
		b_colorize_stream_status = false;
	}
}

void planning_move_callback(const geometry_msgs::TwistStamped &msg)
{
	if (serial_comm)
	{
		// 线速度指令
		// msg.twist.linear.x
		// 角速度指令
		// msg.twist.angular.z

		if (msg.twist.linear.x > 1.0 || msg.twist.angular.z > 0.5)
		{
			ROS_WARN("Speed exceeds upper limit. vx: %f, wz: %f", msg.twist.linear.x, msg.twist.angular.z);
			return;
		}
		if (sensor_deal && sensor_deal->b_motor_power == 0 && (msg.twist.linear.x != 0 || msg.twist.angular.z != 0))
		{
			serial_comm->sendMotorPower(1);
			return;
		}
		serial_comm->sendSpeedToWCXD(IS_SMOOTH, msg.twist.linear.x, msg.twist.angular.z);
	}
}

void charging_pile_callback(const charging_room_service_msgs::ChargingPileDataStamped &msg)
{
	charging_pile_data.door_status = msg.charging_pile_data.door_status;
	charging_pile_data.touch_status = msg.charging_pile_data.touch_status;
	charging_pile_data.time_stamp.tv_sec = msg.header.stamp.toSec();
	charging_pile_data.time_stamp.tv_nsec = msg.header.stamp.toNSec();
}

void path_goal_status_callback(const std_msgs::UInt8 &msg)
{
	if (msg.data == 1)
	{
		step = EMotionStep::Wait;
	}
	else
	{
		step = EMotionStep::Fail;
	}
}

int resolve_json(rapidjson::Document &doc)
{
	const rapidjson::Value &tasks = doc["Tasks"];
	if (!tasks.IsArray())
	{
		ROS_WARN("Invalid 'Tasks' field");
		return -1;
	}
	const rapidjson::Value &iid = doc["InspectId"];
	if (iid.IsString())
	{
		std::cout << "InspectId:" << iid.GetString() << std::endl;
		task_id = atoi(iid.GetString());
	}
	const rapidjson::Value &rid = doc["RobotId"];
	if (rid.IsNumber())
	{
		std::cout << "RobotId:" << rid.GetInt() << std::endl;
	}
	const rapidjson::Value &thid = doc["TaskHistoryId"];
	if (thid.IsNumber())
	{
		std::cout << "TaskHistoryId:" << thid.GetInt() << std::endl;
	}
	const rapidjson::Value &tid = doc["TaskId"];
	if (tid.IsNumber())
	{
		std::cout << "TaskId:" << tid.GetInt() << std::endl;
	}
	const rapidjson::Value &recon = doc["ReconType"];
	if (recon.IsNumber())
	{
		std::cout << "ReconType:" << recon.GetString() << std::endl;
		recon_type = recon.GetString();
	}
	static const char *kTypeNames[] = {"Null", "False", "True", "Object", "Array", "String", "Number"};
	for (SizeType i = 0; i < tasks.Size(); i++) // 使用 SizeType 而不是 size_t
	{
		// std::shared_ptr<ROAD_PLAN> task(new ROAD_PLAN());
		ROAD_PLAN task;
		const rapidjson::Value &obj = tasks[i];
		for (Value::ConstMemberIterator itr = obj.MemberBegin(); itr != obj.MemberEnd(); ++itr)
		{
			std::string type = kTypeNames[itr->value.GetType()];
			std::string key = itr->name.GetString();
			// std::cout << "" << itr->name.GetString() << " " << type  << std::endl;
			if (key == "Align")
			{
				std::cout << "Align:" << itr->value.GetString() << std::endl;
				task.road = itr->value.GetString();
			}
			else if (key == "CameraPose")
			{
				std::cout << "CameraPose:" << itr->value.GetString() << std::endl;
				std::string camera_pose = itr->value.GetString();
				std::vector<std::string> cam_pose = splitYd(camera_pose, ";");
				task.cameraPose.assign(cam_pose.begin(), cam_pose.end());
			}
			else if (key == "Id")
			{
				std::cout << "Id:" << itr->value.GetString() << std::endl;
				task.transfer_id = itr->value.GetString();
			}
			else if (key == "TurnAngle")
			{
				std::cout << "TurnAngle:" << itr->value.GetString() << std::endl;
				std::string turn_angle = itr->value.GetString();
				task.fTurnAngle = atof(turn_angle.c_str());
			}
			else if (key == "TLine")
			{
				std::cout << "TLine:" << itr->value.GetString() << std::endl;
				task.road = itr->value.GetString();
			}
			else if (key == "TLoc")
			{
				std::cout << "TLoc:" << itr->value.GetString() << std::endl;
				std::string sloc = itr->value.GetString();
				std::vector<std::string> veLoc = splitYd(sloc, ";");
				task.LocX = atof(veLoc[0].c_str());
				task.LocY = atof(veLoc[1].c_str());
				task.LocZ = atof(veLoc[2].c_str());
			}
			else if (key == "TLocType")
			{
				std::string sType = itr->value.GetString();
				if (!strcmp(sType.c_str(), "start")) // 与transfer相同
					task.nType = TASK_TYPE_START;
				else if (!strcmp(sType.c_str(), "turn"))
					task.nType = TASK_TYPE_TURN;
				else if (!strcmp(sType.c_str(), "monitor"))
					task.nType = TASK_TYPE_MONITOR;
				else if (!strcmp(sType.c_str(), "transfer"))
					task.nType = TASK_TYPE_TRANSFER;
				else if (!strcmp(sType.c_str(), "back"))
					task.nType = TASK_TYPE_BACK;
				else if (!strcmp(sType.c_str(), "end"))
					task.nType = TASK_TYPE_END;
				else
					task.nType = TASK_TYPE_UNKNOWN;
			}
			else if (key == "TLocWidth")
			{
				if (itr->value.IsNumber())
				{
					std::cout << "TLocWidth:" << itr->value.GetFloat() << std::endl;
					float sLocWidth = itr->value.GetFloat();
					task.fLocWidth = sLocWidth;
				}
			}
		}
		vePlan.push_back(task);
	}
	return 1;
}

// 定位状态正误
void positionStatusCallback(const std_msgs::Int32 msg)
{
	if (msg.data == 0)
	{
		bPositionError = true;
	}
	else
	{
		bPositionError = false;
	}
}

void taskExecuteStatusPub(int task_id, int task_status)
{
	yidamsg::TaskExecuteStatus task_msg;
	task_msg.task_history_id = task_id;
	task_msg.task_status = task_status;
	task_execute_status_pub.publish(task_msg);
}

void workStatusPub()
{
	int pointId = 0;

	work_status = 0;
	if (task_doing)
	{
		// work_status = 1;
		work_status = work_status | 1 << 0;
		if (bStartSendSubTask)
		{
			task_execute_status = TASK_DOING;
		}
		else
		{
			task_execute_status = TASK_PAUSE;
		}
		for (size_t i = 0; i < vePlan.size(); i++)
		{
			if (vePlan[i].nType == TASK_TYPE_TRANSFER && !vePlan[i].finish)
			{
				string transfer_str = vePlan[i].cameraPose[pointWatchNum];
				std::vector<std::string> transfer_param = splitYd(vePlan[i].cameraPose[pointWatchNum], "/");
				if (transfer_param.size() >= 1)
				{
					std::vector<std::string> point_param = splitYd(transfer_param[0], ":");
					if (point_param.size() >= 1)
					{
						pointId = atoi(point_param[0].c_str());
						break;
					}
				}
			}
		}
	}
	else
	{
		task_id = 0;
		pointId = 0;
		recon_type = "";
		task_execute_status = 255;
	}
	if (goHome_doing)
	{
		// work_status = 2;
		work_status = work_status | 1 << 1;
	}
	char charging_pile_touch_status = charging_pile_data.touch_status;
	ros::Time data_time(charging_pile_data.time_stamp.tv_sec, charging_pile_data.time_stamp.tv_nsec);
	ros::Time now_time = ros::Time::now();
	ros::Time attemp_end = data_time + ros::Duration(2);
	if (now_time > attemp_end)
	{
		charging_pile_touch_status = 0;
	}

	// 电机电源 电机使能
	if (charging_pile_touch_status == (char)ETouchStatus::ON)
	{
		g_motion_falut_code = g_motion_falut_code & 0xFFFFFFBF;
		g_motion_falut_code = g_motion_falut_code & 0xFFFFFF7F;
	}
	if (!task_doing)
	{
		g_motion_falut_code = g_motion_falut_code & 0xFFFFFFFD;
	}

	// std::cout << "workStatusPub ..." << std::endl;
	Document doc;
	Document::AllocatorType &allocator = doc.GetAllocator();

	Value task(kObjectType);
	task.AddMember("task_history_id", task_id, allocator);
	task.AddMember("task_status", task_execute_status, allocator);
	task.AddMember("point_id", pointId, allocator);
	Value data(kObjectType);
	data.AddMember("task", task, allocator);
	doc.SetObject();
	doc.AddMember("robot_id", ROBOT_ID, allocator);
	doc.AddMember("robot_status", g_motion_falut_code, allocator);
	doc.AddMember("sensor_status", g_sensor_falut_code, allocator);
	doc.AddMember("touch_status", charging_pile_touch_status, allocator);
	doc.AddMember("work_status", work_status, allocator);
	doc.AddMember("data", data, allocator);

	rapidjson::StringBuffer s;
	rapidjson::Writer<rapidjson::StringBuffer> writer(s);
	doc.Accept(writer);
	std::string json_str = std::string(s.GetString());
	// std::cout << "json_str:" << json_str << std::endl;
	std_msgs::String msg;
	msg.data = json_str;
	work_status_pub.publish(msg);
}

void start_task_handle_fun(std::string data, void *p)
{
	bool b_colorize_point = false;
	if (data != "")
	{
		std::vector<std::string> recon_list = splitYd(data, ",");
		for (std::vector<std::string>::iterator recon = recon_list.begin(); recon != recon_list.end(); recon++)
		{
			if (*recon == "5" || *recon == "6" || *recon == "10")
			{
				b_colorize_point = true;
				break;
			}
		}
	}

	if (b_colorize_point)
	{
		Document obj_doc;
		Document::AllocatorType &allocator = obj_doc.GetAllocator();
		obj_doc.SetObject();
		Value motionState_obj(kNumberType);
		motionState_obj.SetString(std::to_string(1).c_str(), allocator);
		obj_doc.AddMember("motion_state", motionState_obj, allocator);
		rapidjson::StringBuffer append_buf;
		rapidjson::Writer<rapidjson::StringBuffer> writer_append(append_buf);
		obj_doc.Accept(writer_append);

		std::string append_data = std::string(append_buf.GetString());
		std_msgs::String append_data_msg;
		append_data_msg.data = append_data;
		append_data_pub.publish(append_data_msg);

		// if (start_undistort())
		// {
		// 	//
		// }
		if (start_stream())
		{
			//
		}
		int wait_count = 0;
		ros::Rate wait_rate(1);
		while (ros::ok)
		{
			wait_count++;
			if (b_colorize_stream_status || wait_count > 30)
			{
				break;
			}
			ros::spinOnce();
			wait_rate.sleep();
		}
		sleep(6);
	}

	poseStopPub(false);
	bStartSendSubTask = true;
	taskExecuteStatusPub(task_id, TASK_DOING);
	task_execute_status = TASK_DOING;
}

void clearTaskForVariable()
{
	if (stop_stream())
	{
		//
	}
	clear_task_flg = false;
	bStart = true;
	bStartSendSubTask = false;
	// vector<ROAD_PLAN *>().swap(vePlan);
	vector<ROAD_PLAN>().swap(vePlan);
	pose_flag = false;
	task_doing = false;
	biao_flag = 0;
	ROS_INFO("clear task");
	// if (serial_comm)
	// {
	// 	serial_comm->sendMinArmAction(1);
	// 	serial_comm->sendArmReset(0);
	// }
	if (serial_comm)
	{
		serial_comm->sendMinArmAction(1);
		int arm_reset_time = 0;
		ros::Rate arm_reset_rate(1);
		while (ros::ok)
		{
			if (sensor_deal->min_arm_length < 15)
			{
				ROS_INFO("task min arm reset finish.");
				break;
			}
			arm_reset_time++;
			if (arm_reset_time >= 15)
			{
				ROS_WARN("min arm reset timeout.");
				break;
			}
			ros::spinOnce();
			arm_reset_rate.sleep();
		}
	}
	if (serial_comm)
	{
		serial_comm->sendArmReset(0);
		int arm_reset_time = 0;
		ros::Rate arm_reset_rate(1);
		while (ros::ok)
		{
			if (sensor_deal->max_arm_height < 15)
			{
				ROS_INFO("task max arm reset finish.");
				break;
			}
			arm_reset_time++;
			if (arm_reset_time >= 15)
			{
				ROS_WARN("max arm reset timeout.");
				break;
			}
			ros::spinOnce();
			arm_reset_rate.sleep();
		}
	}
}

bool receive_task_clear(int16_t flag)
{
	// res.success = 0;
	// if (req.flag == 2) //清除当前任务
	if (flag == 2) // 清除当前任务
	{
		ROS_INFO("clear task ***");
		clear_task_flg = true;
		// res.success = 1;
	}
	// else if (req.flag == 1) //暂停当前任务
	else if (flag == 1) // 暂停当前任务
	{
		ROS_INFO("pause task *** ");
		bStartSendSubTask = false;
		// res.success = 1;
	}
	// else if (req.flag == 3) //继续当前任务
	else if (flag == 3) // 继续当前任务
	{
		ROS_INFO("continue task ***");
		bStartSendSubTask = true;
		// res.success = 1;
	}
	else
	{
		return false;
	}

	return true;
}
void TaskClearCallback(const std_msgs::Int16 msg)
{
	receive_task_clear(msg.data);
}
bool srvTaskClear(yidamsg::TaskControl::Request &req, yidamsg::TaskControl::Response &res)
{
	if (receive_task_clear(req.flag))
	{
		res.success = 1;
	}
	else
	{
		res.success = 0;
	}

	return true;
}

bool receive_task_list(std::string plan)
{
	std::cout << "receive_task_list start" << std::endl;
	clear_task_flg = true;
	while (clear_task_flg == true)
	{
		usleep(100000);
	}
	if (bStartSendSubTask == false) // 所有任务执行完毕状态
	{
		std::string list = plan;
		std::cout << plan << std::endl;
		const char *json = list.c_str();
		StringStream ss(json);
		rapidjson::Document doc;
		if (doc.ParseStream(ss).HasParseError())
		{
			ROS_ERROR("Failed to parse json ");
			return false;
		}
		if (!doc.IsObject())
		{
			ROS_WARN("json is not object");
			return false;
		}
		if (!doc.HasMember("Tasks"))
		{
			ROS_WARN("No 'Tasks' field");
			return false;
		}
		const rapidjson::Value &iid = doc["InspectId"];
		std::string inspid = "";
		if (iid.IsString())
		{
			inspid = iid.GetString();
			std::cout << "InspectId:" << iid.GetString() << std::endl;
			task_id = atoi(iid.GetString());
		}
		task_doing = true;
		if (inspid == "safe")
		{
			ROS_INFO("start safe task");
		}
		else
		{
			ROS_INFO("start robot task");
		}
		const rapidjson::Value &recon = doc["ReconType"];
		if (recon.IsString())
		{
			std::cout << "ReconType:" << recon.GetString() << std::endl;
			recon_type = recon.GetString();
		}
		int r = resolve_json(doc);
		if (r < 0)
		{
			ROS_ERROR("resolve json error");
			return false;
		}
		// bStartSendSubTask = true;
		if (task_id == -1) // 让李浩要求 该id 为一键返航任务
		{
			bStartSendSubTask = true;
			yidamsg::taskPlanStatus task_msg;
			task_msg.robotId = 1;
			task_msg.status = 1;
			onebuttontask_pub.publish(task_msg);
		}
		else
		{
			std::thread s_Thread = std::thread(start_task_handle_fun, recon_type, nullptr);
			s_Thread.detach();
			// bStartSendSubTask = true;
			// // if (start_undistort())
			// // {
			// // 	//
			// // }
			// if (start_stream())
			// {
			// 	//
			// }
			// poseStopPub(false);
			// taskExecuteStatusPub(task_id, TASK_DOING);
			// task_execute_status = TASK_DOING;
		}

		string s_time;
		double secs = ros::Time::now().toSec();
		time_t t1 = int(secs);
		char now[64];
		struct tm *ttime;
		ttime = localtime(&t1);
		strftime(now, 64, "%Y-%m-%d %H:%M:%S", ttime);
		s_time = now;
		cout << "the now time is " << s_time << endl;
		poseStopPub(false);
		// res.status = true;
	}
	else
	{
		// res.status = false;
		return false;
	}

	return true;
}

// bool receive_task_list(std::string plan)
// {
// 	clear_task_flg = true;
// 	while (clear_task_flg == true)
// 	{
// 		usleep(100000);
// 	}
// 	if (bStartSendSubTask == false) //所有任务执行完毕状态
// 	{
// 		// std::string list = req.plan;
// 		std::string list = plan;
// 		std::cout << plan << std::endl;
// 		Json::Reader reader;
// 		Json::Value value;
// 		std::stringstream ss1;
// 		if (reader.parse(list, value))
// 		{
// 			Json::Value arrayObj = value["Tasks"];
// 			string inspid = value["InspectId"].asString();
// 			task_id = atoi(inspid.c_str());
// 			task_doing = true;
// 			if (inspid == "safe")
// 			{
// 				ROS_INFO("start safe task");
// 			}
// 			else
// 			{
// 				ROS_INFO("start robot task");
// 			}
// 			resloveTaskFromJson(arrayObj);
// 			bStartSendSubTask = true;
// 			// res.status = true;
// 			if (task_id == -1) //让李浩要求 该id 为一键返航任务
// 			{
// 				yidamsg::taskPlanStatus task_msg;
// 				task_msg.robotId = 1;
// 				task_msg.status = 1;
// 				onebuttontask_pub.publish(task_msg);
// 			}
// 			else
// 			{
// 				poseStopPub(false);
// 				taskExecuteStatusPub(task_id, TASK_DOING);
// 				task_execute_status = TASK_DOING;
// 			}
// 			string s_time;
// 			double secs = ros::Time::now().toSec();
// 			time_t t1 = int(secs);
// 			char now[64];
// 			struct tm *ttime;
// 			ttime = localtime(&t1);
// 			strftime(now, 64, "%Y-%m-%d %H:%M:%S", ttime);
// 			s_time = now;
// 			cout << "the now time is " << s_time << endl;
// 			poseStopPub(false);
// 		}
// 		// res.status = true;
// 	}
// 	else
// 	{
// 		// res.status = false;
// 		return false;
// 	}

// 	return true;
// }

void TaskListCallback(const std_msgs::String msg)
{
	receive_task_list(msg.data);
}

bool srvTaskList(yidamsg::TaskList::Request &req, yidamsg::TaskList::Response &res)
{
	if (receive_task_list(req.plan))
	{
		res.status = true;
	}
	else
	{
		res.status = false;
	}

	return true;
}

// 模式切换
void modeCallback(const yidamsg::ControlMode msg)
{
	if (msg.mode == HANDLE_CONTROL)
	{
		g_mode = HANDLE_CONTROL;
		clear_task_flg = true;
		ROS_INFO("to handle control");
	}
	else if (msg.mode == PC_CONTROL)
	{
		g_mode = PC_CONTROL;
		clear_task_flg = true;
		ROS_INFO("to pc control");
	}
	else if (msg.mode == TASK_CONTROL)
	{
		g_mode = TASK_CONTROL;
		clear_task_flg = true;
		ROS_INFO("to task control");
	}
	else if (msg.mode == URGENCY_CONTROL)
	{
		g_mode = URGENCY_CONTROL;
		clear_task_flg = true;
		ROS_INFO("to urgency control");
	}
	else if (msg.mode == OBSTACLE_CONTROL)
	{
		g_mode = OBSTACLE_CONTROL;
		clear_task_flg = true;
	}
	else
	{
		g_mode = TASK_CONTROL;
	}
}

void manualControlCallback(const yidamsg::manualControlParameters msg)
{
	if (msg.control_type == 0 && g_mode == PC_CONTROL)
	{
		if (PLAT_TYPE == 0)
		{
			if (serial_comm)
			{
				serial_comm->sendSpeedToMcu(IS_SMOOTH, msg.linear_speed, msg.angular_speed);
			}
		}
		else
		{
			if (serial_comm)
			{
				if (sensor_deal && sensor_deal->b_motor_power == 0 && (msg.linear_speed != 0 || msg.angular_speed != 0))
				{
					serial_comm->sendMotorPower(1);
					return;
				}
				serial_comm->sendSpeedToWCXD(IS_SMOOTH, msg.linear_speed, msg.angular_speed);
			}
		}
	}
}

void speedCallback(const yidamsg::motor_control msg)
{
	if ((g_mode == TASK_CONTROL || g_mode == URGENCY_CONTROL) &&
		(msg.control_mode == TASK_CONTROL))
	{
		if (PLAT_TYPE == 0)
		{
			if (serial_comm)
			{
				serial_comm->sendSpeedToMcu(IS_SMOOTH, msg.speed.linear.x, msg.speed.angular.z);
			}
		}
		else
		{
			if (serial_comm)
			{
				if (sensor_deal && sensor_deal->b_motor_power == 0 && (msg.speed.linear.x != 0 || msg.speed.angular.z != 0))
				{
					serial_comm->sendMotorPower(1);
					return;
				}
				serial_comm->sendSpeedToWCXD(IS_SMOOTH, msg.speed.linear.x, msg.speed.angular.z);
			}
		}
	}
	else if (g_mode == OBSTACLE_CONTROL && msg.control_mode == OBSTACLE_CONTROL)
	{
		if (PLAT_TYPE == 0)
		{
			if (serial_comm)
			{
				serial_comm->sendSpeedToMcu(NO_SMOOTH, msg.speed.linear.x, msg.speed.angular.z);
			}
		}
		else
		{
			if (serial_comm)
			{
				if (sensor_deal && sensor_deal->b_motor_power == 0 && (msg.speed.linear.x != 0 || msg.speed.angular.z != 0))
				{
					serial_comm->sendMotorPower(1);
					return;
				}
				serial_comm->sendSpeedToWCXD(NO_SMOOTH, msg.speed.linear.x, msg.speed.angular.z);
			}
		}
	}
	else
	{
		/* code */
	}
}

void cmd_vel_Callback(const geometry_msgs::Twist msg)
{
	if (serial_comm)
	{
		if (sensor_deal && sensor_deal->b_motor_power == 0 && (msg.linear.x != 0 || msg.angular.z != 0))
		{
			serial_comm->sendMotorPower(1);
			return;
		}
		serial_comm->sendSpeedToWCXD(IS_SMOOTH, msg.linear.x, msg.angular.z);
	}
}

void obstacleAvoidResultCallback(const std_msgs::Int32 msg)
{
	if (msg.data != 1) // 失败  清楚当前任务，通知重新规划路径
	{
		yidamsg::taskControlParameters task_server;
		task_server.request.control_type = 2;
		task_server.request.task_history_id = task_id;
		task_server.request.flag = 2;
		if (GTaskStatusManagerClientToRobot.call(task_server))
		{
			if (task_server.response.status == 0)
			{
				yidamsg::routeStatus route_status;
				route_status.x = GCurrentPose.x;
				route_status.y = GCurrentPose.y;
				route_status.z = GCurrentPose.z;
				route_status_pub.publish(route_status);
			}
		}
	}
}
void deepCameraDistanceCallback(const std_msgs::Float32 msg)
{
	// if( (msg.data < 0.5) &&
	//     (msg.data > 0.00001) )
	if (msg.data > 0.00001)
	{
		bObstacle = true;
	}
	else
	{
		bObstacle = false;
	}
}

void joyCallback(const sensor_msgs::Joy msg)
{
	static unsigned int mode_num_flg = 0;
	static unsigned char pluse_bit = 0;

	pluse_bit = pluse_bit << 1;
	pluse_bit = pluse_bit & 0x03;
	if (int(msg.buttons[7] == 1))
	{
		pluse_bit = pluse_bit | (0x01);
	}
	else
	{
		pluse_bit = pluse_bit | (0x00);
	}
	if ((pluse_bit & 0x03) == 0x01)
	{
		mode_num_flg++;
		g_mode = (mode_num_flg % 2) == HANDLE_CONTROL ? HANDLE_CONTROL : TASK_CONTROL;
		clear_task_flg = (g_mode == TASK_CONTROL) ? true : false;
		poseStopPub(false);
		{
			yidamsg::ControlMode controlMode;
			controlMode.robot_id = ROBOT_ID;
			controlMode.mode = g_mode;
			control_mode_change_pub.publish(controlMode);
		}
	}
	if (msg.buttons[5] == 1 && g_mode == HANDLE_CONTROL) // 遥控控制
	{
		float vspeed = msg.axes[1];
		float wspeed = msg.axes[0];
		if (vspeed == 0)
		{
			wspeed = wspeed >= 0.2 ? 0.2 : wspeed;
			wspeed = wspeed <= -0.2 ? -0.2 : wspeed;
		}
		wspeed = vspeed >= 0 ? wspeed : (-wspeed);
		vspeed = vspeed > 0.4 ? 0.4 : vspeed;
		if (PLAT_TYPE == 0)
		{
			if (serial_comm)
			{
				serial_comm->sendSpeedToMcu(IS_SMOOTH, vspeed, wspeed);
			}
		}
		else
		{
			if (serial_comm)
			{
				if (sensor_deal && sensor_deal->b_motor_power == 0 && (vspeed != 0 || wspeed != 0))
				{
					serial_comm->sendMotorPower(1);
					return;
				}
				serial_comm->sendSpeedToWCXD(IS_SMOOTH, vspeed, wspeed);
			}
		}
	}
}

void bgtaskCallback(const std_msgs::String msg)
{
	clear_task_flg = true;
}
void motorSpeedPub(int controlMode, float linearVelocity, float angularVelocity)
{
	if (linearVelocity != 0 || angularVelocity != 0)
	{
		// if (sensor_deal->max_arm_height > 10 || sensor_deal->min_arm_origin_status != 1)
		if (sensor_deal->max_arm_height > 15 || sensor_deal->min_arm_length > 15)
		{
			linearVelocity = 0;
			angularVelocity = 0;
			ROS_WARN("Extension state of manipulator.");
		}
	}

	yidamsg::motor_control ctl;
	ctl.control_mode = controlMode;
	ctl.speed.linear.x = linearVelocity;
	ctl.speed.angular.z = angularVelocity;
	motor_pub.publish(ctl);
}
void *timePollThread(void *args)
{
	yidamsg::ControlMode msg;
	ros::NodeHandle n = *((ros::NodeHandle *)args);
	float home_x, home_y, home_z, back_x, back_y, back_z;
	int set_mode_timeout = 60 * 10;
	static char step_num = 0;
	static int mode_time = 0;
	float home_dis = 0.1;
	float home_dis_pre = 0.1;
	float back_dis = 0.1;
	float back_dis_pre = 0.1;

	n.getParam("HomeX", home_x);
	n.getParam("HomeY", home_y);
	n.getParam("HomeZ", home_z);
	n.getParam("BackX", back_x);
	n.getParam("BackY", back_y);
	n.getParam("BackZ", back_z);
	n.getParam("SetModeTimeOut", set_mode_timeout);
	float all_dis = calcDestDistace(home_x, home_y, back_x, back_y);

	ros::Rate loop_rate(1);
	// while (true)
	while (ros::ok())
	{
		step_num++;
		if (sensor_deal)
			sensor_deal->g_connect_num++;
		sleep(1);

		// 通信链接是否正常
		if (sensor_deal && sensor_deal->g_connect_num > 3)
		{
			g_motion_falut_code = g_motion_falut_code | 0x01;
		}
		else
		{
			g_motion_falut_code = g_motion_falut_code & 0xFFFFFFFE;
		}
		// 电机电源
		if (sensor_deal && sensor_deal->b_motor_power == 0)
		{
			g_motion_falut_code = g_motion_falut_code | 0x40;
		}
		else
		{
			g_motion_falut_code = g_motion_falut_code & 0xFFFFFFBF;
		}
		// 电机使能
		if (sensor_deal && sensor_deal->b_motor_enable == 0)
		{
			g_motion_falut_code = g_motion_falut_code | 0x80;
		}
		else
		{
			g_motion_falut_code = g_motion_falut_code & 0xFFFFFF7F;
		}

		mode_time = (g_mode != TASK_CONTROL) ? mode_time++ : 0;
		if (mode_time >= set_mode_timeout)
		{
			mode_time = 0;
			g_mode = TASK_CONTROL;
		}
		msg.robot_id = ROBOT_ID;
		msg.mode = g_mode;
		control_mode_pub.publish(msg);

		diagnostic_msgs::DiagnosticArray dia_array;
		dia_array.header.stamp = ros::Time::now();
		diagnostic_msgs::DiagnosticStatus robot_status;
		robot_status.name = "/yd_motion_control"; // 这里写节点的名字
		robot_status.message = "";				  // 问题描述
		robot_status.hardware_id = "";			  // 硬件信息，根据需要填写
		if (g_motion_falut_code == 0)
		{
			robot_status.level = diagnostic_msgs::DiagnosticStatus::OK;
		} // 0 = OK, 1 = Warn, 2 = Error
		else
		{
			robot_status.level = diagnostic_msgs::DiagnosticStatus::WARN;
		}

		diagnostic_msgs::KeyValue motion_fault_code;
		motion_fault_code.key = "motion_node_fault_code";
		motion_fault_code.value = to_string(g_motion_falut_code);
		robot_status.values.push_back(motion_fault_code);
		diagnostic_msgs::KeyValue kv;
		kv.key = "error_code";
		char buffer[8];
		sprintf(buffer, "104%02d", g_motion_falut_code);
		std::string error_code = buffer;
		kv.value = error_code;
		robot_status.values.push_back(kv);
		dia_array.status.push_back(robot_status); // 有更多信息可以继续在这里添加
		heart_pub.publish(dia_array);
		//
		workStatusPub();
		//
		bool b_home_point = false;
		float distanceToHome = calcDestDistace(GCurrentPose.x, GCurrentPose.y, home_x, home_y);
		if (distanceToHome < 0.3)
		{
			b_home_point = true;
		}
		else
		{
			b_home_point = false;
		}
		char charging_pile_touch_status = charging_pile_data.touch_status;
		ros::Time data_time(charging_pile_data.time_stamp.tv_sec, charging_pile_data.time_stamp.tv_nsec);
		ros::Time now_time = ros::Time::now();
		ros::Time attemp_end = data_time + ros::Duration(2);
		if (now_time > attemp_end)
		{
			charging_pile_touch_status = 0;
		}
		// if (sensor_deal->battery_info <= sensor_deal->battery_charge_value && (chargeRoom.charge_switch == true && b_home_point == true) && task_doing == false)
		if (sensor_deal->battery_info <= sensor_deal->battery_charge_value && (charging_pile_touch_status == (char)ETouchStatus::ON && b_home_point == true) && task_doing == false)
		{
			if (serial_comm)
			{
				serial_comm->charge_flg = true;
			}

			static int i_charge_on = 0;
			i_charge_on++;
			if (i_charge_on % 10 == 0)
			{
				// motorSpeedPub(0, 0, 0);
				//
				if (sensor_deal->b_motor_power == 1)
				{
					serial_comm->sendMotorPower(0);
				}
				usleep(1000 * 200);
				if (sensor_deal->b_battery_charge == 0 || sensor_deal->battery_electric_current <= 0)
				{
					serial_comm->sendChargeStatus(1);
				}
				ROS_INFO("charge ok");
			}
		}
		// else if (sensor_deal->battery_info >= sensor_deal->battery_nocharge_value || (chargeRoom.charge_switch == false || b_home_point == false))
		else if (sensor_deal->battery_info >= sensor_deal->battery_nocharge_value || (charging_pile_touch_status == (char)ETouchStatus::OFF || b_home_point == false))
		{
			if (serial_comm)
			{
				serial_comm->charge_flg = false;
			}

			static int i_charge_off = 0;
			i_charge_off++;
			if (i_charge_off % 10 == 0)
			{
				// motorSpeedPub(0, 0, 0);
				//
				if (sensor_deal->b_battery_charge == 1 || sensor_deal->battery_electric_current > 0)
				{
					serial_comm->sendChargeStatus(0);
				}
				ROS_INFO("charge no");
			}
		}
		else
		{
		}
		//
		{
			static int ii = 0;
			ii++;
			if (ii % 30 == 0)
			{
				ii = 0;
				ROS_INFO("door status: %d, touch status: %d", charging_pile_data.door_status, charging_pile_data.touch_status);
			}
		}

		// ros::spinOnce();
		// loop_rate.sleep();
	}
	return nullptr;
}

bool arm_callback(yidamsg::ArmControl::Request &req, yidamsg::ArmControl::Response &res)
{
	if (!serial_comm)
	{
		return false;
	}

	if (req.type == 1)
	{
		if (req.action == 0)
		{
			serial_comm->sendArmReset(0);
		}
		if (req.action == 1)
		{
			serial_comm->sendArmHeight(req.values[0]);
		}
	}
	if (req.type == 2)
	{
		if (req.action == 0)
		{
			serial_comm->sendMinArmAction(1);
		}
		if (req.action == 1)
		{
			serial_comm->sendMinArmAction(0);
		}
	}
	if (req.type == 3)
	{
		if (req.action == 0)
		{
			serial_comm->sendPDStatus(1);
		}
		if (req.action == 1)
		{
			serial_comm->sendPDStatus(0);
		}
	}
	res.result = 0;
	res.message = "";

	return true;
}

void *subRun(void *args)
{
	ros::NodeHandle n = *((ros::NodeHandle *)args);

	std::string localization_heartbeat_msg = "/yd/heartbeat_mc";
	n.getParam("localization_heartbeat", localization_heartbeat_msg);

	ros::Subscriber cmd_vel_sub = n.subscribe("/cmd_vel", 1, cmd_vel_Callback);
	ros::Subscriber sub2 = n.subscribe("/yida/obstacle_avoid/distance", 1, deepCameraDistanceCallback);
	ros::Subscriber sub3 = n.subscribe("robot_pose", 1, poseCallback);	// pose
	ros::Subscriber sub4 = n.subscribe("meter_flag", 1, meterCallback); // meter index
	ros::Subscriber sub5 = n.subscribe("/yida/obstacle_avoid/result", 1, obstacleAvoidResultCallback);
	ros::Subscriber sub6 = n.subscribe("robot_position_status", 1, positionStatusCallback); // pose error
	ros::Subscriber sub7 = n.subscribe("/ydserver/manual_control", 1, manualControlCallback);
	ros::Subscriber sub8 = n.subscribe("joy", 1, joyCallback);
	ros::Subscriber sub9 = n.subscribe("/task_list", 1, TaskListCallback);
	ros::Subscriber sub10 = n.subscribe("/task_clear", 1, TaskClearCallback);
	ros::Subscriber sub_ctlSpeed = n.subscribe("/yida/robot/motor_control", 1, speedCallback);
	ros::Subscriber sub_ctlmode = n.subscribe("/yida/robot/control_mode", 1, modeCallback);
	ros::Subscriber sub_restart = n.subscribe("/yida/bgtask/restart", 1, bgtaskCallback);
	ros::Subscriber sub_gohome_stop = n.subscribe("/yida/gohome/stop", 1, stopHomeCallback);
	ros::Subscriber localization_heartbeat_sub = n.subscribe(localization_heartbeat_msg, 1, localization_heartbeat_callback);
	ros::Subscriber partial_discharge_sub = n.subscribe("/yida/sensor/partial/discharge", 1, partial_discharge_callback);
	ros::Subscriber audio_capture_sub = n.subscribe("/audio_capture/audio/capture/result", 1, audio_capture_callback);
	ros::Subscriber upload_result_sub = n.subscribe("/ftp/upload/result", 1, ftp_upload_result_callback);
	ros::Subscriber stream_result_sub = n.subscribe("/streaming_service/colorize_info", 1, stream_result_callback);
	ros::Subscriber transfer_sub = n.subscribe("/transfer_pub", 1, transfer_callback);
	ros::Subscriber planning_move_sub = n.subscribe("/yida/patrol_car_speed_cmd", 1, planning_move_callback);
	ros::Subscriber charging_pile_sub = n.subscribe("/charging/pile/data", 1, charging_pile_callback);				 // charging_pile_data
	ros::Subscriber path_goal_status = n.subscribe("/task_goals_adaptor_node/status", 1, path_goal_status_callback); // charging_pile_data

	ros::ServiceServer task_server = n.advertiseService("tasklist", srvTaskList); // 获取上位机下发的任务列表
	ros::ServiceServer tasklist_clear = n.advertiseService("/taskclear", srvTaskClear);
	ros::ServiceServer arm_action = n.advertiseService("/yida/robot/arm", arm_callback);

	ros::spin();
	return nullptr;
}

char locationFault()
{
	char pos_status = 0x00;

	// 坐标是否正常接收判断
	static char pose_num = 0;
	if (pose_flag == true)
	{
		pose_flag = false;
		pose_num = 0;
	}
	else
	{
		if (location_transfer == 0)
		{
			pose_num++;
		}
		if (pose_num >= 18)
		{
			pose_num = 18;
			pos_status = pos_status | (1 << 0);
			ROS_WARN("Abnormal receiving of positioning coordinates; pos_status:%d", (1 << 0));
		}
	}
	if (g_location_falut_code != 0)
	{
		pos_status = pos_status | (1 << 1);
	}

	return pos_status;
}

char someFaultProcess()
{
	char fault_status = 0x00;
	/*
	if(bPositionError == true)
	{
		ROS_INFO("定位状态错误！！！");
		fault_status = 1;
	}else{}
	*/
	if (bObstacle == true)
	{
		fault_status = fault_status | (1 << 0);
		ROS_WARN("Obstacles detected; fault_code:%d", (1 << 0));
	}
	// other sensor
	if (g_car_action_status == CAR_FORWARD)
	{
		if (sensor_deal->sensor_fall_f_flag)
		{
			fault_status = fault_status | (1 << 1);
			ROS_WARN("CAR_FORWARD, Front anti drop sensor triggered; fault_code:%d", (1 << 1));
		}
		if (sensor_deal->sensor_ultrasound_f_flag)
		{
			fault_status = fault_status | (1 << 2);
			ROS_WARN("CAR_FORWARD, Front ultrasonic sensor triggered; fault_code:%d", (1 << 2));
		}
		if (sensor_deal->sensor_collision_f_flag)
		{
			fault_status = fault_status | (1 << 3);
			ROS_WARN("CAR_FORWARD, Front anti collision sensor triggered; fault_code:%d", (1 << 3));
		}
	}
	else if (g_car_action_status == CAR_BACKWARD)
	{
		if (sensor_deal->sensor_fall_b_flag)
		{
			fault_status = fault_status | (1 << 4);
			ROS_WARN("CAR_BACKWARD, Rear fall sensor triggered; fault_code:%d", (1 << 4));
		}
		if (sensor_deal->sensor_ultrasound_b_flag)
		{
			fault_status = fault_status | (1 << 5);
			ROS_WARN("CAR_BACKWARD, Rear ultrasonic sensor triggered; fault_code:%d", (1 << 5));
		}
		if (sensor_deal->sensor_collision_b_flag)
		{
			fault_status = fault_status | (1 << 6);
			ROS_WARN("CAR_BACKWARD, Rear anti collision sensor triggered; fault_code:%d", (1 << 6));
		}
	}
	else if (g_car_action_status == CAR_TURN)
	{
		if (sensor_deal->sensor_ultrasound_b_flag)
		{
			fault_status = fault_status | (1 << 5);
			ROS_WARN("CAR_TURN, Rear ultrasonic sensor triggered; fault_code:%d", (1 << 5));
		}
		if (sensor_deal->sensor_collision_b_flag)
		{
			fault_status = fault_status | (1 << 6);
			ROS_WARN("CAR_TURN, Rear anti collision sensor triggered; fault_code:%d", (1 << 6));
		}
		if (sensor_deal->sensor_ultrasound_f_flag)
		{
			fault_status = fault_status | (1 << 2);
			ROS_WARN("CAR_TURN, Front ultrasonic sensor triggered; fault_code:%d", (1 << 2));
		}
		if (sensor_deal->sensor_collision_f_flag)
		{
			fault_status = fault_status | (1 << 3);
			ROS_WARN("CAR_TURN, Front anti collision sensor triggered; fault_code:%d", (1 << 3));
		}
	}
	else
	{
	}

	g_sensor_falut_code = fault_status;
	return fault_status;
}

int getUltrasonicDistance(int actionType)
{
	int ultrasonicDistance = 0;
	if (actionType == CAR_FORWARD)
	{
		if (sensor_deal->sensor_ultrasound_14 != 0)
		{
			ultrasonicDistance = sensor_deal->sensor_ultrasound_14;
		}
		if (sensor_deal->sensor_ultrasound_15 != 0)
		{
			ultrasonicDistance = ultrasonicDistance <= sensor_deal->sensor_ultrasound_15 ? ultrasonicDistance : sensor_deal->sensor_ultrasound_15;
		}
	}
	else if (actionType == CAR_BACKWARD)
	{
		if (sensor_deal->sensor_ultrasound_16 != 0)
		{
			ultrasonicDistance = sensor_deal->sensor_ultrasound_16;
		}
		if (sensor_deal->sensor_ultrasound_17 != 0)
		{
			ultrasonicDistance = ultrasonicDistance <= sensor_deal->sensor_ultrasound_17 ? ultrasonicDistance : sensor_deal->sensor_ultrasound_17;
		}
	}
	else if (g_car_action_status == CAR_TURN)
	{
	}
	else
	{
	}
	return ultrasonicDistance;
}

char transferPub(vector<string> lists, vector<string> task_camera, string trans_id, char flg)
{
	ROS_INFO("transferPub begin ...");
	string s_time;
	double secs = ros::Time::now().toSec();
	time_t t1 = int(secs);
	char now[64];
	struct tm *ttime;
	ttime = localtime(&t1);
	strftime(now, 64, "%Y-%m-%d %H:%M:%S", ttime);
	s_time = now;
	//
	yidamsg::transfer data;
	data.flag = flg;
	if (flg == 0)
	{
		transfer_flag = 1;
		data.data = s_time + "/";
		data.data += trans_id;
		data.data += "/";
		std::ostringstream s;
		if (task_camera.size() >= 2)
		{
			if (lists.size() == 4)																									//
				s << data.data << task_camera[0] << "/" << task_camera[1] << "/" << lists[1] << "/" << lists[2] << "/" << lists[3]; // 0：station
			else if (lists.size() == 5)
				s << data.data << task_camera[0] << "/" << task_camera[1] << "/" << lists[1] << "/" << lists[2] << "/" << lists[3] << "/" << lists[4]; // 0：station
			else
			{
				ROS_WARN("lists info error");
			}
			data.data = s.str();
		}
	}
	else
	{
		yd_cloudplatform::CloudPlatControl cloudplat_control;
		cloudplat_control.request.id = 1;
		cloudplat_control.request.action = 1;
		cloudplat_control.request.type = 4;
		cloudplat_control.request.allvalue.push_back(0);
		cloudplat_control.request.allvalue.push_back(0);
		cloudplat_control.request.allvalue.push_back(0);
		if (cloudplatform_client.call(cloudplat_control))
			std::cout << "set xyz 0,0,0 degree success!" << std::endl;
		else
			std::cout << "set xyz 0,0,0 degree failed!" << std::endl;

		data.data = s_time;
	}
	ROS_INFO("transferPub publish data:%s", data.data.c_str());
	sleep(1);
	transfer_pub.publish(data);

	return 1;
}

void *chargeThread(void *args)
{
	ros::NodeHandle n = *((ros::NodeHandle *)args);
	float home_x, home_y, home_z, back_x, back_y, back_z;
	n.getParam("HomeX", home_x);
	n.getParam("HomeY", home_y);
	n.getParam("HomeZ", home_z);
	n.getParam("BackX", back_x);
	n.getParam("BackY", back_y);
	n.getParam("BackZ", back_z);
	while (ros::ok())
	{
		int interval_print_i = 0;
		goHome_doing = false;
		backToHomeRoadPub(0);
		sem_wait(&charge_sem);
		// 此路段拒绝接受下发任务
		goHome_doing = true;
		backToHomeRoadPub(1);
		sleep(1);

		yd_cloudplatform::CloudPlatControl cloudplat_control;
		cloudplat_control.request.id = 1;
		cloudplat_control.request.action = 1;
		// cloudplat_control.request.type = 0;
		// cloudplat_control.request.value = go_home_type == 1 ? 0 : 18000;
		cloudplat_control.request.type = 4;
		cloudplat_control.request.allvalue.push_back(go_home_type == 1 ? 0 : 18000);
		cloudplat_control.request.allvalue.push_back(go_home_type == 1 ? 0 : 0);
		cloudplat_control.request.allvalue.push_back(go_home_type == 1 ? 0 : 0);
		if (cloudplatform_client.call(cloudplat_control))
			std::cout << "set xy degree success!" << std::endl;
		else
			std::cout << "set xy degree failed!" << std::endl;

		char control_status = 0;
		if (door_control_type == 1) // need open door
		{
			control_status = -1;
			control_status = door_control((void *)&n, OPEN_DOOR);
			if (control_status != 0)
			{
				ROS_WARN("open time out...");
				goHome_doing = false;
				backToHomeRoadPub(0);
				continue;
			}
			if (control_status == 0)
			{
				ROS_INFO("door open ok");
			}
		}
		double wait_time = 0;
		EActionStatus current_action = EActionStatus::Orientate;
		int in_position = 0;
		float linearVelocity = 0, angularVelocity = 0;
		g_car_action_status = CAR_BACKWARD;
		while (ros::ok() && control_status == 0)
		{
			interval_print_i++;
			float currentRobotYaw = GCurrentPose.anglez * 180 / PII; // 当前姿态角
			// float nextRoadAngle = calcLineAngle(home_x, home_y, back_x, back_y);
			float nextRoadAngle = go_home_type == 1 ? calcLineAngle(back_x, back_y, home_x, home_y) : calcLineAngle(home_x, home_y, back_x, back_y);
			float runDistance = calcDestDistace(GCurrentPose.x, GCurrentPose.y, back_x, back_y);
			float allDistance = calcDestDistace(back_x, back_y, home_x, home_y);
			float turnAngleToLine = calcTurnAngleToLine(back_x, back_y, home_x, home_y, GCurrentPose.anglez);
			float fVerticalDis = calcToLineDistance(GCurrentPose.x, GCurrentPose.y, home_x, home_y, back_x, back_y);
			float distanceToNextPoint = calcDestDistace(GCurrentPose.x, GCurrentPose.y, home_x, home_y);

			// 综合处理一些故障检测
			char deal_status = 0;
			if ((sensor_deal && sensor_deal->g_connect_num > 3) || !sensor_deal)
			{
				deal_status = 1;
			}
			char pos_status = locationFault();
			if (pos_status != 0)
			{
				g_motion_falut_code = g_motion_falut_code | 0x10;
			}
			else
			{
				g_motion_falut_code = g_motion_falut_code & 0xFFFFFFEF;
			}
			char fault_status = someFaultProcess();
			if (fault_status != 0x00)
			{
				g_motion_falut_code = g_motion_falut_code | 0x20;
			}
			else
			{
				g_motion_falut_code = g_motion_falut_code & 0xFFFFFFDF;
			}
			float over_dis = 0;
			char rect_status = calcToLimitStatus(GCurrentPose.x, GCurrentPose.y, back_x, back_y, home_x, home_y, 100, EActionStatus::None, over_dis);
			if (rect_status != 0x00)
			{
				g_motion_falut_code = g_motion_falut_code | 0x02;
			}
			else
			{
				g_motion_falut_code = g_motion_falut_code & 0xFFFFFFFD;
			}
			// process
			fault_status = fault_status & 0x9F;
			usleep(20000);
			if (wait_time > 20000 * 5000)
			{
				current_action = EActionStatus::Orientate;
				goHome_doing = false;
				backToHomeRoadPub(0);
				ROS_INFO("charge timeout");
				g_car_action_status = CAR_FORWARD;
				break;
			}
			if (deal_status != 0 || pos_status != 0 || rect_status != 0 || fault_status != 0x00)
			{
				wait_time += 20000;
				if (interval_print_i % 50 == 0)
					ROS_WARN("have some fault deal_status:%x --  pos_status:%x -- rect_status:%x -- over_dis:%f -- fault_status:%x",
							 deal_status, pos_status, rect_status, over_dis, fault_status);
				motorSpeedPub(0, 0, 0);
				continue;
			}
			wait_time = 0;
			//
			bool b_home_point = false;
			float distanceToHome = calcDestDistace(GCurrentPose.x, GCurrentPose.y, home_x, home_y);
			if (distanceToHome < 0.02)
			{
				b_home_point = true;
			}
			else
			{
				b_home_point = false;
			}
			//
			switch (current_action)
			{
				static int timenum = 0;

			case EActionStatus::Orientate: // 对朝向  home --> back
			{
				if (interval_print_i % 50 == 0)
					ROS_INFO("face direction");
				turnAngle(90, currentRobotYaw, nextRoadAngle, linearVelocity, angularVelocity);
				if (isDestination(DEST_ANGLE, runDistance - allDistance, currentRobotYaw, nextRoadAngle, linearVelocity, angularVelocity))
				{
					in_position++;
				}
				if (in_position > 3)
				{
					current_action = EActionStatus::Forward;
					// current_action = EActionStatus::Backward;
				}
			}
			break;
			case EActionStatus::Forward:  // go home
			case EActionStatus::Backward: // go home
			{
				if (interval_print_i % 50 == 0)
					ROS_INFO("to home point");
				if (!go_home_stop)
				{
					if (go_home_type == 1)
					{
						forwardGoHome(fVerticalDis, turnAngleToLine, distanceToNextPoint, allDistance, linearVelocity, angularVelocity);
					}
					else
					{
						backwardGoHome(fVerticalDis, turnAngleToLine, distanceToNextPoint, allDistance, linearVelocity, angularVelocity);
					}
				}
				else
				{
					linearVelocity = 0;
					angularVelocity = 0;
				}
				if (isDestination(DEST_POINT, runDistance - allDistance, 0, 0, linearVelocity, angularVelocity) == true)
				{
					current_action = EActionStatus::Touch;
				}
				char charging_pile_touch_status = charging_pile_data.touch_status;
				ros::Time data_time(charging_pile_data.time_stamp.tv_sec, charging_pile_data.time_stamp.tv_nsec);
				ros::Time now_time = ros::Time::now();
				ros::Time attemp_end = data_time + ros::Duration(2);
				if (now_time > attemp_end)
				{
					charging_pile_touch_status = 0;
				}
				// if (chargeRoom.charge_switch == true)
				if (charging_pile_touch_status == (char)ETouchStatus::ON) // touch
				{
					current_action = EActionStatus::Close;
					ROS_INFO("Touch the contact - current_action = EActionStatus::Forward,Backward");
				}
				//
				if (b_home_point == true)
				{
					current_action = EActionStatus::Close;
					ROS_INFO("Reach charging position - current_action = EActionStatus::Forward,Backward");
				}
			}
			break;
			case EActionStatus::Touch: // 找触点
			{
				timenum++;
				if (timenum > 50 * 60)
				{
					timenum = 0;
					linearVelocity = 0;
					angularVelocity = 0;
					current_action = EActionStatus::Done;
					break;
				}

				if (interval_print_i % 50 == 0)
					ROS_INFO("find point");

				// linearVelocity = -0.05;
				linearVelocity = go_home_type == 1 ? 0.03 : -0.03;
				angularVelocity = 0;

				char charging_pile_touch_status = charging_pile_data.touch_status;
				ros::Time data_time(charging_pile_data.time_stamp.tv_sec, charging_pile_data.time_stamp.tv_nsec);
				ros::Time now_time = ros::Time::now();
				ros::Time attemp_end = data_time + ros::Duration(2);
				if (now_time > attemp_end)
				{
					charging_pile_touch_status = 0;
				}
				// if (chargeRoom.charge_switch == true)
				if (charging_pile_touch_status == (char)ETouchStatus::ON) // touch
				{
					current_action = EActionStatus::Close;
					ROS_INFO("Touch the contact - current_action = EActionStatus::Touch");
				}
				//
				if (b_home_point == true)
				{
					current_action = EActionStatus::Close;
					ROS_INFO("Reach charging position - current_action = EActionStatus::Touch");
				}
			}
			break;
			case EActionStatus::Close: // 关门
			{
				timenum = 0;
				linearVelocity = 0;
				angularVelocity = 0;
				motorSpeedPub(TASK_CONTROL, linearVelocity, angularVelocity);
				if (door_control_type == 1) // need close door
				{
					if (door_control((void *)&n, CLOSE_DOOR) != 0)
					{
						ROS_INFO("door close fault");
						// continue;要求不要尝试
					}
					else
					{
						ROS_INFO("door close ok");
					}
				}
				current_action = EActionStatus::Done;
			}
			break;
			default:
				break;
			}
			// usleep(20000);
			motorSpeedPub(TASK_CONTROL, linearVelocity, angularVelocity);
			if (current_action == EActionStatus::Done)
			{
				sleep(6);
				current_action = EActionStatus::None;
				// 该路完成，允许下发任务
				goHome_doing = false;
				backToHomeRoadPub(0);
				ROS_INFO("charge finish");
				g_car_action_status = CAR_FORWARD;
				break;
			}
		}
	}
	return nullptr;
}

void *serialCheckRun(void *args)
{
	sleep(10);
	while (ros::ok())
	{
		if (serial_comm && sensor_deal && sensor_deal->g_connect_num > 15)
		{
			if (serial_comm->b_open || serial_comm->b_run)
			{
				ROS_INFO("disconnect tty name:%s", serial_name.c_str());
				serial_comm->disconnect();
				sleep(6);
			}
		}
		if (serial_comm && !(serial_comm->b_open) && !(serial_comm->b_run))
		{
			sleep(6);
			ROS_INFO("connect tty name:%s", serial_name.c_str());
			serial_comm->connect(serial_name);
		}

		sleep(10);
	}
	return nullptr;
}

int main(int argc, char **argv)
{
	signal(SIGINT, signalHandler);

	initLogSys();

	ros::init(argc, argv, "sport_control");
	// set param
	param_server_ = new param_server::Server("yd_motion_control", "cfg/config.yml");
	param_server::CallbackType f = boost::bind(&callback, _1); // 绑定回调函数
	param_server_->setCallback(f);

	float home_x, home_y, home_z, back_x, back_y, back_z;
	//
	ros::NodeHandle n;
	pthread_t ros_thread = 0;
	pthread_t ros_mode_thread = 0;
	pthread_t wheel_odom_thread = 0;
	pthread_t control_thread_t;
	pthread_t charge_thread_t;
	pthread_t home_thread_t;
	pthread_t serial_check_thread = 0;
	float angularVelocity = 0;
	float linearVelocity = 0;

	tf2_ros::TransformListener tf2_(buffer_);
	n.getParam("HomeX", home_x);
	n.getParam("HomeY", home_y);
	n.getParam("HomeZ", home_z);
	n.getParam("BackX", back_x);
	n.getParam("BackY", back_y);
	n.getParam("BackZ", back_z);

	n.param<int>("RobotId", ROBOT_ID, 0);
	n.param<std::string>("serial_version", serial_version, "v2201");
	n.param<int>("door_control_type", door_control_type, 1);
	n.param<int>("go_home_type", go_home_type, 1);
	n.param<std::string>("audio_dir", audio_dir, "");
	ROS_INFO("serial_version:%s", serial_version.c_str());

	sem_init(&charge_sem, 0, 0);
	control_mode_pub = n.advertise<yidamsg::ControlMode>("/control_mode", 1);
	transfer_pub = n.advertise<yidamsg::transfer>("transfer_pub", 1); // watch point
	motor_pub = n.advertise<yidamsg::motor_control>("/yida/robot/motor_control", 1);
	work_status_pub = n.advertise<std_msgs::String>("/yida/robot/work_status", 1);
	task_execute_status_pub = n.advertise<yidamsg::TaskExecuteStatus>("/task_execute_status", 1);
	onebuttontask_pub = n.advertise<yidamsg::taskPlanStatus>("/onebuttontask_status", 1);
	pose_stop_pub = n.advertise<std_msgs::Bool>("/stop_receive_laser", 1);
	heart_pub = n.advertise<diagnostic_msgs::DiagnosticArray>("/yd/heartbeat", 1);
	task_pub = n.advertise<yidamsg::task_status>("task_status", 1);
	route_status_pub = n.advertise<yidamsg::routeStatus>("/yida/routeStatus", 1);
	start_end_road_pub = n.advertise<yidamsg::ControlMode>("/charge_status", 1);
	control_mode_change_pub = n.advertise<yidamsg::ControlMode>("/yida/robot/control_mode", 1);
	tasks_json_status_pub = n.advertise<yidamsg::taskJsonStatus>("yida/task_json_status", 1);
	weather_pub = n.advertise<yidamsg::weather>("/weather_info", 1);
	restart_pub = n.advertise<std_msgs::String>("/yida/robot/restart", 1);
	detect_result_pub = n.advertise<yidamsg::InspectedResult>("/detect_result", 1);
	ftp_upload_pub = n.advertise<std_msgs::String>("/ftp/upload/file", 1);
	append_data_pub = n.advertise<std_msgs::String>("/streaming_service/append_data", 1);
	path_pub = n.advertise<nav_msgs::Path>("/mission_path_send_node/mission_path", 1);

	doorControl_client = n.serviceClient<charging_room_service_msgs::DoorControl>("/charging/room/door/service");
	cloudplatform_client = n.serviceClient<yd_cloudplatform::CloudPlatControl>("/yida/internal/platform_cmd");
	undistort_start_client = n.serviceClient<undistort_service_msgs::StartUndistort>("/undistort_service/start_undistort");
	undistort_stop_client = n.serviceClient<undistort_service_msgs::StopUndistort>("/undistort_service/stop_undistort");
	stream_start_client = n.serviceClient<streaming_service_msgs::start_streaming>("/streaming_service/start_streaming");
	stream_stop_client = n.serviceClient<streaming_service_msgs::stop_streaming>("/streaming_service/stop_streaming");
	audio_capture_client = n.serviceClient<audio_capture_msgs::AudioCapture>("/audio_capture/audio/capture");
	GTaskStatusManagerClientToRobot = n.serviceClient<yidamsg::taskControlParameters>("/yida/robot/task_control");
	//
	serial_name = "/dev/ttyUSB0";
	if (strcmp(serial_version.c_str(), "v2020") == 0)
	{
		ROS_INFO("bind serial version v2020");
		serial_comm = std::make_shared<Serial_v2020>(ROBOT_ID);
		sensor_deal = std::make_shared<dealV2020>(n, ROBOT_ID);
		serial_comm->data_size = 122;
	}
	else if (strcmp(serial_version.c_str(), "v2021") == 0)
	{
		ROS_INFO("bind serial version v2021");
		serial_comm = std::make_shared<Serial_v2021>(ROBOT_ID);
		sensor_deal = std::make_shared<dealV2021>(n, ROBOT_ID);
		serial_comm->data_size = 131;
	}
	else if (strcmp(serial_version.c_str(), "v2103") == 0)
	{
		ROS_INFO("bind serial version v2103");
		serial_comm = std::make_shared<Serial_v2103>(ROBOT_ID);
		sensor_deal = std::make_shared<dealV2103>(n, ROBOT_ID);
		serial_comm->data_size = 165;
	}
	if (strcmp(serial_version.c_str(), "v2201") == 0)
	{
		ROS_INFO("bind serial version v2201");
		serial_comm = std::make_shared<Serial_v2201>(ROBOT_ID);
		sensor_deal = std::make_shared<dealV2201>(n, ROBOT_ID);
		serial_comm->data_size = 182;
	}
	ROS_INFO("serial init ok");
	if (!serial_comm)
	{
		ROS_ERROR("serial_comm is null");
		return 1;
	}
	if (!sensor_deal)
	{
		ROS_ERROR("sensor_deal is null");
		return 1;
	}
	//
	pthread_create(&ros_thread, NULL, subRun, (void *)&n);
	pthread_create(&ros_mode_thread, NULL, timePollThread, (void *)&n);
	pthread_create(&charge_thread_t, NULL, chargeThread, (void *)&n);
	//
	ROS_INFO("sensor_fall_f_flag:%d, sensor_fall_b_flag:%d, sensor_collision_f_flag:%d, sensor_collision_b_flag:%d, sensor_ultrasound_f_flag:%d, sensor_ultrasound_b_flag:%d",
			 sensor_deal->sensor_fall_f_flag, sensor_deal->sensor_fall_b_flag, sensor_deal->sensor_collision_f_flag,
			 sensor_deal->sensor_collision_b_flag, sensor_deal->sensor_ultrasound_f_flag, sensor_deal->sensor_ultrasound_b_flag);
	//
	auto fun = std::bind(&BaseDeal::pubWheel, sensor_deal, std::placeholders::_1);
	serial_comm->dealFunct = fun;
	// read for sensor_deal
	readConfig();
	ROS_INFO("tty name:%s", serial_name.c_str());
	serial_comm->connect(serial_name);
	//
	pthread_create(&serial_check_thread, NULL, serialCheckRun, (void *)&n);
	//
	usleep(1000 * 100);
	serial_comm->sendPDStatus(1);
	serial_comm->sendMinArmAction(1);
	serial_comm->sendArmReset(0);
	int arm_reset_time = 0;
	ros::Rate arm_reset_rate(1);
	while (ros::ok)
	{
		// if (sensor_deal->max_arm_height <= 10 && sensor_deal->min_arm_origin_status == 1)
		if (sensor_deal->max_arm_height < 15 && sensor_deal->min_arm_length < 15)
		{
			ROS_INFO("arm reset finish.");
			break;
		}
		arm_reset_time++;
		if (arm_reset_time >= 10)
		{
			ROS_WARN("arm reset timeout.");
			break;
		}
		ros::spinOnce();
		arm_reset_rate.sleep();
	}
	//
	bStartSendSubTask = false;
	ROS_INFO("sport control start!!!");
	{
		std_msgs::String msg;
		msg.data = "robot_restart";
		restart_pub.publish(msg);
	}
	// ****** ****** ****** ****** ****** ****** ****** ****** ****** ******
	// begin
	while (ros::ok())
	{
		int interval_print_i = 0;
		if (clear_task_flg == true) // 清除当前任务
		{
			clearTaskForVariable();
			// stop car
			/* code */
		}
		if (bStartSendSubTask == true) // 运行任务
		{
			if (task_doing == true)
			{
				if (serial_comm)
				{
					serial_comm->charge_flg = false;
					//
					if (sensor_deal->b_battery_charge == 1)
					{
						serial_comm->sendChargeStatus(0);
					}
				}
			}
			//
			if (calcDestDistace(GCurrentPose.x, GCurrentPose.y, home_x, home_y) < 0.5) // home点
			{
				ROS_INFO("home point start task, ready open door");
				if (door_control_type == 1) // need open door
				{
					if (door_control((void *)&n, OPEN_DOOR) != 0)
					{
						ROS_WARN("home point, open the door fail.");
						yidamsg::taskControlParameters task_server;
						task_server.request.control_type = 2;
						task_server.request.task_history_id = task_id;
						task_server.request.flag = 2;
						if (GTaskStatusManagerClientToRobot.call(task_server))
						{
							//
						}
						clear_task_flg = true;
						continue;
					}
				}
			}
			//
			static bool motion_stop = false;
			// 循环任务
			for (size_t index = 0; index < vePlan.size(); index++)
			{
				if (clear_task_flg == true) // 清除当前任务
				{
					clearTaskForVariable();
					// smooth stop car
					/* code */
					break;
				}

				ROS_INFO("vePlan index = %d, type: %d", (int)index, vePlan[index].nType);
				int planType = vePlan[index].nType;
				step = EMotionStep::None;
				action = EActionStatus::None;
				pointWatchNum = 0;
				int vePlanSize = vePlan.size();

				while (ros::ok())
				{
					usleep(20000); // 设理论执行周期20ms

					if (clear_task_flg == true) // 清除当前任务
					{
						clearTaskForVariable();
						// smooth stop car
						/* code */
						break;
					}

					float currentRobotYaw = 0;
					float distanceToNextPoint = 0;
					float runDistance = 0;
					float allDistance = 0;
					float turnAngleToLine = 0;
					float fVerticalDis = 0;
					bool currentIsSamePoint = false;
					float currentRoadAngle = 0;
					bool nextIsSamePoint = false;
					float nextRoadAngle = 0;
					// 位置姿态计算
					{
						currentRobotYaw = GCurrentPose.anglez * 180 / PII;																										   // 当前姿态角
						distanceToNextPoint = calcDestDistace(GCurrentPose.x, GCurrentPose.y, vePlan[index].LocX, vePlan[index].LocZ);											   // 当前点距目标点距离
						runDistance = calcDestDistace(GCurrentPose.x, GCurrentPose.y, vePlan[index - 1].LocX, vePlan[index - 1].LocZ);											   // 当前已运行距离
						allDistance = calcDestDistace(vePlan[index - 1].LocX, vePlan[index - 1].LocZ, vePlan[index].LocX, vePlan[index].LocZ);									   // 当前任务总距离
						turnAngleToLine = calcTurnAngleToLine(vePlan[index - 1].LocX, vePlan[index - 1].LocZ, vePlan[index].LocX, vePlan[index].LocZ, GCurrentPose.anglez);		   // 计算当前夹角
						fVerticalDis = calcToLineDistance(GCurrentPose.x, GCurrentPose.y, vePlan[index].LocX, vePlan[index].LocZ, vePlan[index - 1].LocX, vePlan[index - 1].LocZ); // 左右距离
						if (index >= 1)
						{
							currentIsSamePoint = isSamePoint(vePlan[index - 1].LocX, vePlan[index - 1].LocZ, vePlan[index].LocX, vePlan[index].LocZ);
							currentRoadAngle = calcLineAngle(vePlan[index - 1].LocX, vePlan[index - 1].LocZ, vePlan[index].LocX, vePlan[index].LocZ); // 运行点与上个点的的世界坐标系角
						}
						else
						{
							currentIsSamePoint = false;
							currentRoadAngle = 0;
						}
						if (index >= vePlanSize - 1) // 结束点
						{
							nextIsSamePoint = false;
							nextRoadAngle = 0;
							TaskStatusPub(runDistance, allDistance, planType, 0, 0, 0, 0);
						}
						else
						{
							nextIsSamePoint = isSamePoint(vePlan[index].LocX, vePlan[index].LocZ, vePlan[index + 1].LocX, vePlan[index + 1].LocZ);
							nextRoadAngle = calcLineAngle(vePlan[index].LocX, vePlan[index].LocZ, vePlan[index + 1].LocX, vePlan[index + 1].LocZ); // 运行点与下个点的的世界坐标系角
							if (vePlan[index].LocX == vePlan[index + 1].LocX && vePlan[index].LocZ == vePlan[index + 1].LocZ)
							{
								float intersectAngle = vePlan[index].fTurnAngle == 180 ? 0.0 : vePlan[index].fTurnAngle;
								if (intersectAngle > 0)
								{
									nextRoadAngle = (intersectAngle - 180.0) + currentRoadAngle;
								}
								else
								{
									nextRoadAngle = (intersectAngle + 180.0) + currentRoadAngle;
								}
								nextRoadAngle = nextRoadAngle > 180.0 ? (nextRoadAngle - 360.0) : nextRoadAngle;
								nextRoadAngle = nextRoadAngle < -180.0 ? (nextRoadAngle + 360.0) : nextRoadAngle;
							}
							TaskStatusPub(runDistance, allDistance, planType, vePlan[index].LocX, vePlan[index].LocZ, vePlan[index + 1].LocX, vePlan[index + 1].LocZ);
						}
					}
					// 综合处理一些故障检测
					{
						char deal_status = 0;
						if ((sensor_deal && sensor_deal->g_connect_num > 3) || !sensor_deal)
						{
							deal_status = 1;
						}
						char pos_status = locationFault();
						if (pos_status != 0)
						{
							g_motion_falut_code = g_motion_falut_code | 0x10;
						}
						else
						{
							g_motion_falut_code = g_motion_falut_code & 0xFFFFFFDF;
						}
						char fault_status = someFaultProcess();
						if (fault_status != 0x00)
						{
							g_motion_falut_code = g_motion_falut_code | 0x20;
						}
						else
						{
							g_motion_falut_code = g_motion_falut_code & 0xFFFFFFEF;
						}
						float over_dis = 0;
						char rect_status = calcToLimitStatus(GCurrentPose.x, GCurrentPose.y, vePlan[index].LocX, vePlan[index].LocZ, vePlan[index - 1].LocX, vePlan[index - 1].LocZ, vePlan[index].nType, action, over_dis);
						if (rect_status != 0x00)
						{
							g_motion_falut_code = g_motion_falut_code | 0x02;
						}
						else
						{
							g_motion_falut_code = g_motion_falut_code & 0xFFFFFFFD;
						}
						if (deal_status != 0 || pos_status != 0 || rect_status != 0 || fault_status != 0x00)
						{
							if (deal_status != 0 || pos_status != 0 || rect_status != 0 || fault_status != 0x00)
							{
								// stop car
								/* code */
							}
							else
							{
								// smooth stop car
								/* code */
							}
							interval_print_i++;
							if (interval_print_i % 100 == 0)
							{
								ROS_WARN("have some fault - deal_status:%x; pos_status:%x; rect_status:%x,over_dis:%f; fault_status:%x",
										 deal_status, pos_status, rect_status, over_dis, fault_status);
							}
							continue;
						}
						if (bStartSendSubTask == false)
						{
							if (!motion_stop)
							{
								motion_stop = true;
								// stop car
								/* code */
							}
							continue;
						}
					}
					//
					{
						if (planType != TASK_TYPE_END && calcDestDistace(GCurrentPose.x, GCurrentPose.y, back_x, back_y) < 0.3)
						{
							char charging_pile_door_status = charging_pile_data.door_status;
							ros::Time data_time(charging_pile_data.time_stamp.tv_sec, charging_pile_data.time_stamp.tv_nsec);
							ros::Time now_time = ros::Time::now();
							ros::Time attemp_end = data_time + ros::Duration(2);
							if (now_time > attemp_end)
							{
								charging_pile_door_status = 0;
							}
							if (charging_pile_door_status == (char)EDoorStatus::Open) // door open
							{
								if (door_control_type == 1) // need close door
								{
									ROS_INFO("open to close");
									door_control((void *)&n, CLOSE_DOOR);
								}
							}
						}
					}
					// process
					{
						float dis_home_back = calcDestDistace(home_x, home_y, (back_x + home_x) / 2, (home_y + back_y) / 2);
						if (fabs((calcDestDistace(GCurrentPose.x, GCurrentPose.y, home_x, home_y) + calcDestDistace(GCurrentPose.x, GCurrentPose.y, (back_x + home_x) / 2, (home_y + back_y) / 2)) - dis_home_back) < 0.3)
						{
							float linearVelocity = 0;
							float angularVelocity = 0;
							if (go_home_type == 1)
							{
								backwardGo(fVerticalDis, turnAngleToLine, distanceToNextPoint, allDistance, linearVelocity, angularVelocity);
							}
							else
							{
								forwardGo(fVerticalDis, turnAngleToLine, distanceToNextPoint, allDistance, getUltrasonicDistance(CAR_FORWARD), linearVelocity, angularVelocity);
							}
							motorSpeedPub(0, linearVelocity, 0);
							continue;
						}
						//
						float x = vePlan[index].LocX;
						float y = vePlan[index].LocY;
						float z = vePlan[index].LocZ;
						float yaw = 0.0;
						float pitch = 0.0;
						float roll = 0.0;
						switch (planType)
						{
						case TASK_TYPE_TURN: // 转弯点
						{
							yaw = currentRoadAngle / 180 * PII;
						}
						break;
						case TASK_TYPE_TRANSFER: // 观测点
						{
							yaw = currentRoadAngle / 180 * PII;
						}
						break;
						case TASK_TYPE_BACK: // 后退点
						{
							float road_angle = 0.0;
							if (currentRoadAngle > 0)
							{
								road_angle = currentRoadAngle - 180.0;
							}
							else
							{
								road_angle = currentRoadAngle + 180.0;
							}
							yaw = road_angle / 180 * PII;
						}
						break;
						case TASK_TYPE_END: // 所有任务结束点
						{
							yaw = currentRoadAngle / 180 * PII;
						}
						break;
						default:
							break;
						}
						//
						if (step == EMotionStep::None)
						{
							if (index == 0)
							{
								float fDisStart = calcDestDistace(GCurrentPose.x, GCurrentPose.y, vePlan[index].LocX, vePlan[index].LocZ);
								if ((fDisStart < 0.5))
								{
									step = EMotionStep::Start;
								}
							}
							else
							{
								float fDisStart = calcDestDistace(GCurrentPose.x, GCurrentPose.y, vePlan[index - 1].LocX, vePlan[index - 1].LocZ);
								if ((fDisStart < 0.5))
								{
									step = EMotionStep::Start;
								}
							}
						}
						else if (step == EMotionStep::Start)
						{
							taskJsonStatusPub(index, JSON_STATUS_DOING);

							if (index == 0)
							{
								step = EMotionStep::Done;
							}
							else
							{
								if (currentIsSamePoint)
								{
									step = EMotionStep::Wait;
								}
								else
								{
									step = EMotionStep::Obstacle;

									nav_msgs::Path plan_msg;
									plan_msg.header.stamp = ros::Time::now();
									plan_msg.header.frame_id = "map";
									geometry_msgs::PoseStamped pose_msg;
									pose_msg.header.stamp = ros::Time::now();
									pose_msg.header.frame_id = "map";
									pose_msg.pose.position.x = x;
									pose_msg.pose.position.y = z;
									pose_msg.pose.position.z = y;
									pose_msg.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(roll, pitch, yaw);
									plan_msg.poses.push_back(pose_msg);
									path_pub.publish(plan_msg);
								}
							}
						}
						else if (step == EMotionStep::Direct)
						{
							/* code */
						}
						else if (step == EMotionStep::Obstacle)
						{
							if (motion_stop)
							{
								motion_stop = false;

								nav_msgs::Path plan_msg;
								plan_msg.header.stamp = ros::Time::now();
								plan_msg.header.frame_id = "map";
								geometry_msgs::PoseStamped pose_msg;
								pose_msg.header.stamp = ros::Time::now();
								pose_msg.header.frame_id = "map";
								pose_msg.pose.position.x = x;
								pose_msg.pose.position.y = z;
								pose_msg.pose.position.z = y;
								pose_msg.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(roll, pitch, yaw);
								plan_msg.poses.push_back(pose_msg);
								path_pub.publish(plan_msg);
							}

							ros::Time now_time = ros::Time::now();
							static ros::Time begin_time = now_time;
							if (now_time > begin_time + ros::Duration(600.0))
							{
								// taskExecuteStatusPub(task_id, TASK_STOP);

								// clear_task_flg = true;

								// break;

								ROS_WARN("timeout 600");
							}
						}
						else if (step == EMotionStep::Wait)
						{
							switch (planType)
							{
							case TASK_TYPE_TURN: // 转弯点
							{
								step = EMotionStep::Done;
							}
							break;
							case TASK_TYPE_TRANSFER: // 观测点
							{
								unsigned char pointType = 0;
								std::vector<std::string> lists;
								std::vector<std::string> task_camera;
								std::string trans_id;

								if (action == EActionStatus::None)
								{
									action = EActionStatus::Detect;
								}
								else if (action == EActionStatus::Detect)
								{
									if (pointWatchNum > vePlan[index].cameraPose.size())
									{
										action = EActionStatus::Wait;
										break;
									}
									location_transfer = 1;
									poseStopPub(true);
									lists = splitYd(vePlan[index].cameraPose[pointWatchNum], "/");
									task_camera = splitYd(lists[0], ":");
									trans_id = vePlan[index].transfer_id;
									pointType = stoi(task_camera[1]);
									// 如果是着色点
									if (pointType == 5)
									{
										yidamsg::InspectedResult point_result;
										std::ostringstream s;
										s << task_camera[0] << "/" << lists[2] << "/" << lists[3];
										point_result.equipid = s.str(); // point_id/robot_id/task_id
										point_result.result = "-999999";
										point_result.success = true;
										//	point_result_pub.publish(point_result);
									}
									sleep(2);
									ROS_INFO("TRANSFER pub ...");
									transferPub(lists, task_camera, trans_id, 0);

									action = EActionStatus::Wait;
								}
								else if (action == EActionStatus::Wait)
								{
									static int transferOverTime = 0;
									// 该任务完成
									if (biao_flag == 1 || pointType == 5)
									{
										biao_flag = 0;
										transferOverTime = 0;
										pointWatchNum++;
										if (pointWatchNum >= vePlan[index].cameraPose.size())
										{
											action = EActionStatus::Done;
											pointWatchNum = 0;
											location_transfer = 0;
											poseStopPub(false);
											sleep(2);
											transferPub(lists, task_camera, trans_id, 1);
											if (serial_comm)
											{
												serial_comm->sendMinArmAction(1);
												int arm_reset_time = 0;
												ros::Rate arm_reset_rate(1);
												while (ros::ok)
												{
													if (sensor_deal->min_arm_length < 15)
													{
														ROS_INFO("task min arm reset finish.");
														break;
													}
													arm_reset_time++;
													if (arm_reset_time >= 15)
													{
														ROS_WARN("min arm reset timeout.");
														break;
													}
													ros::spinOnce();
													arm_reset_rate.sleep();
												}
											}
											if (serial_comm)
											{
												serial_comm->sendArmReset(0);
												int arm_reset_time = 0;
												ros::Rate arm_reset_rate(1);
												while (ros::ok)
												{
													if (sensor_deal->max_arm_height < 15)
													{
														ROS_INFO("task max arm reset finish.");
														break;
													}
													arm_reset_time++;
													if (arm_reset_time >= 15)
													{
														ROS_WARN("max arm reset timeout.");
														break;
													}
													ros::spinOnce();
													arm_reset_rate.sleep();
												}
											}
										}
										else
										{
											action = EActionStatus::Detect; // 继续该停靠点的下一个观测点
										}
									}
									else
									{
										if ((transferOverTime++) > 10000)
										{
											transferOverTime = 0;
											pointWatchNum++;
											if (pointWatchNum >= vePlan[index].cameraPose.size())
											{
												action = EActionStatus::Done;
												pointWatchNum = 0;
												location_transfer = 0;
												poseStopPub(false);
												sleep(2);
												transferPub(lists, task_camera, trans_id, 1);
												if (serial_comm)
												{
													serial_comm->sendMinArmAction(1);
													int arm_reset_time = 0;
													ros::Rate arm_reset_rate(1);
													while (ros::ok)
													{
														if (sensor_deal->min_arm_length < 15)
														{
															ROS_INFO("task min arm reset finish.");
															break;
														}
														arm_reset_time++;
														if (arm_reset_time >= 15)
														{
															ROS_WARN("min arm reset timeout.");
															break;
														}
														ros::spinOnce();
														arm_reset_rate.sleep();
													}
												}
												if (serial_comm)
												{
													serial_comm->sendArmReset(0);
													int arm_reset_time = 0;
													ros::Rate arm_reset_rate(1);
													while (ros::ok)
													{
														if (sensor_deal->max_arm_height < 15)
														{
															ROS_INFO("task max arm reset finish.");
															break;
														}
														arm_reset_time++;
														if (arm_reset_time >= 15)
														{
															ROS_WARN("max arm reset timeout.");
															break;
														}
														ros::spinOnce();
														arm_reset_rate.sleep();
													}
												}
											}
											else
											{
												action = EActionStatus::Detect; // 继续该停靠点的下一个观测点
											}
										}
									}
								}
								else if (action == EActionStatus::Done)
								{
									static int armResetTime = 0;
									// if ((sensor_deal->max_arm_height < 10 && sensor_deal->min_arm_origin_status == 1) || ((armResetTime++) > 1000))
									if ((sensor_deal->max_arm_height < 15 && sensor_deal->min_arm_length < 15) || ((armResetTime++) > 1000))
									{
										action = EActionStatus::None;
										//
										step = EMotionStep::Done;
									}
								}
								else
								{
									step = EMotionStep::Done;
								}
							}
							break;
							case TASK_TYPE_BACK: // 后退点
							{
								step = EMotionStep::Done;
							}
							break;
							case TASK_TYPE_END: // 所有任务结束点
							{
								step = EMotionStep::Done;
							}
							break;
							default:
								break;
							}
						}
						else if (step == EMotionStep::Done)
						{
							taskJsonStatusPub(index, JSON_STATUS_FINISH);

							vePlan[index].finish = true;

							if (planType == TASK_TYPE_END)
							{
								float fDisEnd = calcDestDistace(GCurrentPose.x, GCurrentPose.y, back_x, back_y);
								if (fDisEnd < 0.5)
								{
									ROS_INFO("ready into door");
									poseStopPub(false);
									sem_post(&charge_sem);
									sleep(6);
								}
								if (task_id == -1)
								{
									yidamsg::taskPlanStatus task_msg;
									task_msg.robotId = 1;
									task_msg.status = 0;
									onebuttontask_pub.publish(task_msg);
								}
								else
								{
									poseStopPub(true);
									taskExecuteStatusPub(task_id, TASK_HAS_EXECUTED);
									task_execute_status = TASK_HAS_EXECUTED;
									// if (stop_undistort())
									// {
									// 	//
									// }
									if (stop_stream())
									{
										//
									}
								}
								bStartSendSubTask = false;
								task_doing = false;
							}
							if (index == vePlanSize - 1)
							{
								vector<ROAD_PLAN>().swap(vePlan);
								ROS_INFO("Task Finish %d", task_id);
							}

							break;
						}
						else if (step == EMotionStep::Fail)
						{
							taskExecuteStatusPub(task_id, TASK_STOP);

							clear_task_flg = true;

							break;
						}
						else
						{
							/* code */
						}
					}
				}
			}
		}
		usleep(60000);
	}
	// end

	pthread_detach(ros_thread);
	pthread_detach(ros_mode_thread);
	pthread_detach(wheel_odom_thread);
	pthread_detach(control_thread_t);
	pthread_detach(charge_thread_t);
	pthread_detach(home_thread_t);
	pthread_detach(serial_check_thread);

	return 0;
}
