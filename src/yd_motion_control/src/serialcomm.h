﻿/*
 * @Author: your name
 * @Date: 2019-12-10 17:29:06
 * @LastEditTime: 2020-07-13 10:15:44
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /motor_shenzhen/src/serialcomm_lp_float.h
 */
#ifndef __SERIAL_COMM_H__
#define __SERIAL_COMM_H__
#include <string>
#include <cstddef>
#include <unistd.h>
#include "ros/ros.h"
#include <linux/serial.h>
#include <stdio.h>	 /* Standard input/output definitions */
#include <string.h>	 /* String function definitions */
#include <unistd.h>	 /* UNIX standard function definitions */
#include <fcntl.h>	 /* File control definitions */
#include <errno.h>	 /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include <assert.h>
#include <math.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <iostream>
#include <pthread.h>
#include <sys/epoll.h>
#include <vector>
#include <mutex>

#define RX_BUFFER_SIZE 4096
#define DEFAULT_BAUD_RATE 115200
#define IS_SMOOTH 0
#define NO_SMOOTH 1
using namespace std;

typedef void (*dealFcn)(const vector<unsigned char> &);
typedef std::function<void(const vector<unsigned char> &)> dealCB;

class SerialComm
{
public:
	struct epoll_event event;
	struct epoll_event events[6]; //事件集合
	int epid;
	int m_fd;
	int max_receive_num = 4096;
	int data_size = 122;
	short lifts_height;
	bool fan_status;
	bool charge_flg;
	unsigned char sensorBuff[4096];
	vector<unsigned char> waitBuff;
	pthread_t com_thread_t, deal_thread_t;
	// dealFcn dealFunct;
	dealCB dealFunct;
	std::mutex send_mtx;
	struct timeval last_time;
	bool b_open;
	bool b_run;

public:
	SerialComm(std::string _version, int _robot_id);
	~SerialComm();
	std::string get_version();
	static void *comThread(void *param);
	void startComThread(void);
	int connect(const std::string &deviceName, unsigned int baudRate = DEFAULT_BAUD_RATE);
	int disconnect();
	int epollInit(int cfd);
	void carCmdPacket(unsigned char *sendBuf);
	//
	virtual void *comThreadIml();
	virtual void sendSpeedToMcu(char smooth_type, float V, float W);
	virtual void sendSpeedToWCXD(char smooth_type, float V, float W);
	virtual void sendSpeedToDrive(float V, float W);
	virtual void sendLifts(float h);
	virtual void sendFan(bool status);
	virtual void sendTest();
	//
	virtual void sendChargeStatus(int status);
	virtual void sendMotorPower(int status);
	virtual void sendMotorEnable(int status);
	//
	virtual void sendArmReset(int status);
	virtual void sendArmHeight(int vlaue);
	virtual void sendMinArmAction(int status);
	//
	virtual void sendPDStatus(int status);
	virtual void sendLEDStatus(char status, char r, char g, char b, char s, char e, char t);

protected:
	std::string serial_version;
	int robot_id;
	int setBaudRate(int baudRate);
	void setFlags();

protected:
	unsigned char m_rxBuffer[RX_BUFFER_SIZE];
	int m_rxCount;
	float *m_ranges;
	unsigned int m_rangesCount;
	struct termios m_oldopt;
};

#endif //__SERIAL_COMM_H__
