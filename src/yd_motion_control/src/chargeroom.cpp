#include "chargeroom.h"

#define SERVPORT 502

static unsigned short g_transaction_index = 0;

ChargeRoom::ChargeRoom()
{
	temp = -999;
	humd = -999;
	gasp = -999;
	winds = -999;
	windd = -999;
	rainfall = -999;
	bus_fault = false;
	socket_fault = false;
	bus_flg = true;
	door_status = DOOR_STATUS_IDLE;
	charge_room_action = DOOR_GET_WEATHER;
	sock_fd = initModbusTcp();
	if (sock_fd == -1)
	{
		socket_fault = true;
	}
	else
	{
		socket_fault = false;
	}
	startSocketThread();
}

ChargeRoom::~ChargeRoom()
{
	pthread_detach(socket_thread_t);
	::close(sock_fd);
}

//读取所有输入
char ChargeRoom::readInputCoils(int sock_fd)
{
	unsigned char buff[12];
	// ROS_INFO("read inputcoils");
	buff[0] = g_transaction_index / 256;
	buff[1] = g_transaction_index % 256;
	buff[2] = 0;
	buff[3] = 0;
	buff[4] = 0;
	buff[5] = 0x06;
	buff[6] = 1;
	buff[7] = 0x02;
	buff[8] = 0;
	buff[9] = 0;
	buff[10] = 0;
	buff[11] = 5;

	if ((send(sock_fd, buff, 12, 0)) < 10)
	{
		socket_fault = true;
		return -1;
	}

	int i = 0;
	char encrypt_end[30];
	for (i = 0; i < 30; i++)
	{
		encrypt_end[i] = 0x00;
	}
	for (i = 0; i < 12; i++)
	{
		{
			sprintf(&encrypt_end[i * 2], "%02x", buff[i] & 0x0ff);
		}
	}
	//
	static char send_num = 0;
	if (send_num++ > 10)
	{
		send_num = 0;
		ROS_INFO("send input cmd :%s", encrypt_end);
	}

	g_transaction_index++;

	return 0;
}

//读输出线圈的状态
char ChargeRoom::readOutpusCoils(int sock_fd)
{
	unsigned char buff[12];

	buff[0] = g_transaction_index / 256;
	buff[1] = g_transaction_index % 256;
	buff[2] = 0;
	buff[3] = 0;
	buff[4] = 0;
	buff[5] = 0x06;
	buff[6] = 1;
	buff[7] = 0x01;
	buff[8] = 0;
	buff[9] = 0;
	buff[10] = 0;
	buff[11] = 5;

	if ((send(sock_fd, buff, 12, 0)) < 10)
	{
		socket_fault = true;
		return -1;
	}
	g_transaction_index++;

	return 0;
}

//
char ChargeRoom::readMobusRtu(int sock_fd, char salve_addr)
{
	unsigned char buff[12];

	buff[0] = g_transaction_index / 256;
	buff[1] = g_transaction_index % 256;
	buff[2] = 0;
	buff[3] = 0;
	buff[4] = 0;
	buff[5] = 0x06;
	buff[6] = salve_addr; //salve address
	buff[7] = 0x03;
	buff[8] = 0;
	buff[9] = 0x09;
	buff[10] = 0;
	buff[11] = 6;

	if ((send(sock_fd, buff, 12, 0)) < 10)
	{
		socket_fault = true;
		return -1;
	}
	g_transaction_index++;

	return 0;
}

char ChargeRoom::writeOutPutCoils(int sock_fd, short addr, bool value)
{
	unsigned char buff[12];
	buff[0] = g_transaction_index / 256;
	buff[1] = g_transaction_index % 256;
	buff[2] = 0;
	buff[3] = 0;
	buff[4] = 0;
	buff[5] = 0x06;
	buff[6] = 1;
	buff[7] = 0x05;
	buff[8] = (addr >> 8) & 0x00ff;
	buff[9] = addr & 0x00ff;
	if (value)
	{
		buff[10] = 0xFF;
		buff[11] = 0x00;
	}
	else
	{
		buff[10] = 0x00;
		buff[11] = 0x00;
	}
	// if(value)
	// 	ROS_INFO("send cmd open---");
	// else
	// 	ROS_INFO("send cmd close---");
	if ((send(sock_fd, buff, 12, 10)) < 0)
	{
		socket_fault = true;
		return -1;
	}
	int i = 0;
	char encrypt_end[30];
	for (i = 0; i < 30; i++)
	{
		encrypt_end[i] = 0x00;
	}
	for (i = 0; i < 12; i++)
	{
		{
			sprintf(&encrypt_end[i * 2], "%02x", buff[i] & 0x0ff);
		}
	}
	ROS_INFO("send output cmd :%s", encrypt_end);
	g_transaction_index++;
	//

	return 0;
}

int ChargeRoom::initModbusTcp()
{
	int sockfd, sendbytes;
	int flags = 0;
	struct sockaddr_in serv_addr;
	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	{
		return -1;
	}

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(SERVPORT);
	serv_addr.sin_addr.s_addr = inet_addr("192.168.1.174");
	if ((connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(struct sockaddr))) < 0)
	{
		close(sockfd);
		return -1;
	}
	flags = fcntl(sockfd, F_GETFL, 0);
	fcntl(sockfd, F_SETFL, flags | O_NONBLOCK);
	ROS_INFO("SOCKET OK");

	return sockfd;
}

int ChargeRoom::disconnect()
{
	::close(sock_fd);

	return 0;
}

void *ChargeRoom::socketThreadIml()
{
	int read_len = 0;
	char i = 0;
	while (true)
	{
		if (socket_fault == true)
		{
			disconnect();
			sleep(10);
			sock_fd = initModbusTcp();
			if (sock_fd == -1)
			{
				ROS_WARN("try door server fault !!!");
				socket_fault = true;
				continue;
			}
			else
			{
				socket_fault = false;
			}
		}
		switch (charge_room_action)
		{
		case DOOR_SET_CLOSE:
		{
			ROS_INFO("DOOR_SET_CLOSE");
			// writeOutPutCoils(sock_fd,1,false);
			// usleep(500000);
			writeOutPutCoils(sock_fd, 0, true);
			bus_flg = false;
		}
		break;
		case DOOR_SET_OPEN:
		{
			ROS_INFO("DOOR_SET_OPEN");
			// writeOutPutCoils(sock_fd,0,false);
			// usleep(500000);
			writeOutPutCoils(sock_fd, 1, true);
			bus_flg = false;
		}
		break;
		case DOOR_CLEAR:
		{
			writeOutPutCoils(sock_fd, 0, false);
			usleep(500000);
			writeOutPutCoils(sock_fd, 1, false);
			bus_flg = false;
		}
		break;
		case DOOR_GET_INPUT:
		{
			readInputCoils(sock_fd);
			bus_flg = false;
			// ROS_INFO("read input");
		}
		break;
		case DOOR_GET_WEATHER:
		{
			bus_flg = true;
			readMobusRtu(sock_fd, SLAVE_ADDR);
			// ROS_INFO("read weather:");
		}
		break;
		default:
			break;
		}
		while (true)
		{
			//static char try_num = 0;
			usleep(500000);
			read_len = recv(sock_fd, (char *)bus_buff, 1024, 0);
			if (read_len <= 0)
			{
				// ROS_INFO("read data timeout");
				if (bus_flg == true)
				{
					bus_fault = true;
				}
				else
				{
					bus_fault = false;
				}
				readInputCoils(sock_fd);
				usleep(500000);
				read_len = recv(sock_fd, (char *)bus_buff, 1024, 0);
				if (read_len <= 0)
				{
					// ROS_INFO("time out too");
					socket_fault = true;
				}
				break;
				// if(try_num++ > 1)
				// {
				// 	try_num = 0;
				// 	bus_fault = true;
				// 	ROS_INFO("read data fail");
				// 	break;
				// }
				// continue;
			}
			if (bus_flg == true)
			{
				bus_fault = false;
			}
			if (read_len >= 10)
			{
				char encrypt_end[30];
				for (i = 0; i < 30; i++)
				{
					encrypt_end[i] = 0x00;
				}
				for (i = 0; i < 12; i++)
				{
					{
						sprintf(&encrypt_end[i * 2], "%02x", bus_buff[i] & 0x0ff);
					}
				}
				// ROS_INFO("read data from chargeRoom :%s", encrypt_end);
				switch (bus_buff[CMD_INDEX])
				{
				case 0x02: //读取输入
				{
					bool sw1, sw2;
					charge_switch = ((bus_buff[DATA_INDEX] & 0x08) == 0x08) ? true : false;
					sw1 = ((bus_buff[DATA_INDEX] & 0x02) == 0x02) ? true : false;
					sw2 = ((bus_buff[DATA_INDEX] & 0x04) == 0x04) ? true : false;
					if (sw1 == false && sw2 == false)
					{
						door_open_switch = true;
						door_status = DOOR_STATUS_OPEN;
					}
					else
					{
						door_open_switch = false;
					}
					if (sw1 == true && sw2 == true)
					{
						door_close_switch = true;
						door_status = DOOR_STATUS_CLOSE;
					}
					else
					{
						door_close_switch = false;
					}
				}
				break;
				case 0x05: //读取输出
				{
				}
				break;
				case 0x03:
				{
					if (bus_buff[LENGTH_INDEX] == 12)
					{
						temp = bus_buff[DATA_INDEX] * 256 + bus_buff[DATA_INDEX + 1];
						humd = bus_buff[DATA_INDEX + 2] * 256 + bus_buff[DATA_INDEX + 3];
						gasp = bus_buff[DATA_INDEX + 4] * 256 + bus_buff[DATA_INDEX + 5];
						winds = bus_buff[DATA_INDEX + 6] * 256 + bus_buff[DATA_INDEX + 7];
						windd = bus_buff[DATA_INDEX + 8] * 256 + bus_buff[DATA_INDEX + 9];
						rainfall = bus_buff[DATA_INDEX + 10] * 256 + bus_buff[DATA_INDEX + 11];
					}
				}
				break;
				default:
					break;
				}
				bzero(bus_buff, 1024);
				break;
			}
		}
	}

	return nullptr;
}

void ChargeRoom::startSocketThread(void)
{
	int result = pthread_create(&socket_thread_t, NULL, socketThread, (void *)this);
}

void *ChargeRoom::socketThread(void *param)
{
	ChargeRoom *pThis = (ChargeRoom *)param;
	pThis->socketThreadIml();

	return nullptr;
}
