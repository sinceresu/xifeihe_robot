﻿/*
 * @Author: your name
 * @Date: 2019-12-10 17:29:06
 * @LastEditTime: 2020-08-13 11:18:19
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /motor_shenzhen/src/serialcomm_lp_float.h
 */
#ifndef __CHARGEROOM_H__
#define __CHARGEROOM_H__
#include <string>
#include <cstddef>
#include <unistd.h>
#include "ros/ros.h"
#include <stdio.h>	/* Standard input/output definitions */
#include <string.h> /* String function definitions */
#include <unistd.h> /* UNIX standard function definitions */
#include <fcntl.h>	/* File control definitions */
#include <errno.h>	/* Error number definitions */
#include <assert.h>
#include <math.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <iostream>
#include <pthread.h>
#include <sys/epoll.h>
#include <vector>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>

#define SLAVE_ADDR 0X02
#define DOOR_SET_OPEN 0
#define DOOR_SET_CLOSE 1
#define DOOR_SET_IDLE 2
#define DOOR_GET_INPUT 3
#define DOOR_GET_WEATHER 4
#define DOOR_CLEAR 5
#define CHARGE_SWITCH_OPEN 1
#define CHARGE_SWITCH_CLOSE 0
#define CMD_INDEX 7
#define LENGTH_INDEX 8
#define DATA_INDEX 9
#define DOOR_STATUS_IDLE 0
#define DOOR_STATUS_OPEN 1
#define DOOR_STATUS_CLOSE 2

using namespace std;

class ChargeRoom
{
public:
	int sock_fd;
	char charge_room_action;
	bool bus_fault;
	bool bus_flg;
	bool socket_fault;
	bool charge_switch;
	bool door_open_switch;
	bool door_close_switch;
	char door_status;
	unsigned char bus_buff[1024];
	float temp;
	float humd;
	float gasp;
	float winds;
	float windd;
	float rainfall;
	pthread_t socket_thread_t;
	static void *socketThread(void *param);
	void *socketThreadIml();
	void startSocketThread(void);
	char readInputCoils(int sock_fd);
	char readOutpusCoils(int sock_fd);
	char readMobusRtu(int sock_fd, char salve_addr);
	char writeOutPutCoils(int sock_fd, short addr, bool value);
	int initModbusTcp();
	int disconnect();
	ChargeRoom();
	~ChargeRoom();

protected:
	unsigned char m_rxBuffer[1024];
};

#endif //
