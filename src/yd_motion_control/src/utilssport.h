/*
 * @Author: your name
 * @Date: 2019-11-19 17:13:01
 * @LastEditTime: 2020-08-31 10:08:58
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /wali/media/root/MiD/workspace/src/taskmanager_robot/src/task_fuzzy_lp.cpp
 */
// #include "geometry_msgs/Twist.h"
// #include "std_msgs/String.h"
// #include "yidamsg/fuzzy_task.h"
// #include "yidamsg/motor_control.h"
// #include "yidamsg/Yida_pose.h"
#include <stdio.h>
#include <memory>
// #include <map>
// #include <sys/time.h>
// #include <unistd.h>
// #include "ros/ros.h"
// #include "std_msgs/String.h"
// #include "std_msgs/Bool.h"
// #include <sys/wait.h>
// #include <algorithm>
// #include <fcntl.h>
// #include <fstream>
// #include <iostream>
// #include <sys/types.h>
// #include "sensor_msgs/Joy.h"
// #include "../include/Common.h"
// #include "../inc/control_flag.h"
// #include <syslog.h>
// #include <math.h>

#include "stdio.h"
#include <math.h>
#include <sys/wait.h>
#include <algorithm>
#include <fcntl.h>
#include <fstream>
#include <iostream>

#define PII 3.1415926
#define DIS_NEAR 0.5
#define OFFSET_ERROR 0.15
#define VERTICLE_DIS 0.35
// #define MAX_LINE_SPEED 0.99
#define DEFAULT_POSE 9999.9
#define VERTICLE_RATIO 50
#define ANGULAR_ERROR 2.5
#define FIX_ANGULAR_SPEED_MIN 0.08
#define FIX_ANGULAR_SPEED_MAX 0.4
#define FIX_LINEAR_SPEED 0.2
#define DEST_ANGLE 0
#define DEST_POINT 1
#define TASK_CONTROL 0
#define HANDLE_CONTROL 1
#define PC_CONTROL 2
#define URGENCY_CONTROL 3
#define OBSTACLE_CONTROL 4

#define LINEARK 0.8
#define LINEARB 0.06
#define ANGLEK 0.03

namespace utilssport
{
	double car_speed_max = 0.6;
	double min_angle_speed = 1;
	double max_angle_speed = 18;
	double sensorLimitSpeed_distance = 120;
	double car_reduction_ratio = 36;
}

typedef struct
{
	float Kp;
	float Ki;
	float Kd;
	float Ek;
	float Ek1;
	float Ek2;
	float LocSum;
} PID_LocTypeDef;
typedef struct
{
	float Kp;
	float Ki;
	float Kd;
	float Ek;
	float Ek1;
	float Ek2;
} PID_IncTypeDef;

enum struct EMotionStep
{
	None = 0,
	Start,
	Direct,
	Obstacle,
	Wait,
	Fail,
	Done
};

enum struct EActionStatus
{
	None = 0,
	Orientate,
	Forward,
	Backward,
	Turn,
	Reach,
	Detect,
	Touch,
	Open,
	Close,
	Wait,
	Done
};

float pidLoc(float SetValue, float ActualValue, PID_LocTypeDef *PID);
float pidInc(float SetValue, float ActualValue, PID_IncTypeDef *PID);
//
bool isSamePoint(float a_pos_x, float a_pos_z, float b_pos_x, float b_pos_z);
//计算应该旋转的角度
float calcTurnAngleToLine(float a_pos_x, float a_pos_z, float b_pos_x, float b_pos_z, float anglez);
//计算直线角度
float calcLineAngle(float a_pos_x, float a_pos_z, float b_pos_x, float b_pos_z);
float calcDestDistace(float beginX, float beginZ, float endX, float endZ); //计算两点间的距离
/*
 * return : 大于0，右侧，小于0 在左侧
 */
float calcToLineDistance(float thisPosX, float thisPosZ, float thisTaskX, float thisTaskZ, float lastTaskX, float lastTaskZ); // (当前点， 终点，起点) 计算当前距左右的距离
char calcToLimitStatus(float thisPosX, float thisPosZ, float thisTaskX, float thisTaskZ, float lastTaskX, float lastTaskZ, int type, EActionStatus actinonStatus, float &over_dis);
float distanceToVelocity(float len, float FStart, float FStop, float flexible, int index);
// char poseFaultCheck(yidamsg::Yida_pose GCurrentPose,PoseCheck &checkPose)
char forwardGo(float fVerticalDis, float calcTurnAngleToLine, float distanceToNextPoint, float allDistance, float sensorDistance, float &currentLinearVelocity, float &angularVelocity);
void turnAngle(float orientation, float currentAngel, float destAngle, float &linearVelocity, float &angularVelocity);
char backwardGo(float fVerticalDis, float calcTurnAngleToLine, float distanceToNextPoint, float allDistance, float &currentLinearVelocity, float &angularVelocity);
char forwardGoHome(float fVerticalDis, float calcTurnAngleToLine, float distanceToNextPoint, float allDistance, float &currentLinearVelocity, float &angularVelocity);
char backwardGoHome(float fVerticalDis, float calcTurnAngleToLine, float distanceToNextPoint, float allDistance, float &currentLinearVelocity, float &angularVelocity);
// bool haveArrivePoint(float distanceToNextPoint,float &linearVelocity,float &angularVelocity);
// bool haveArriveAngle(float currentAngle,float destAngle,float &linearVelocity,float &angularVelocity);
bool isDestination(char destType, float distanceError, float currentAngle, float destAngle, float &linearVelocity, float &angularVelocity);
// bool isDestination(char destType,float distanceToNextPoint,float currentAngle,float destAngle,float &linearVelocity,float &angularVelocity );
bool SmoothStop(float &linearVelocity, float &angularVelocity);
bool sensorLimitSpeed(float sensorDistance, float c_LinearVelocity, float c_AngularVelocity,
					  float i_LinearVelocity, float i_AngularVelocity, float &r_LinearVelocity, float &r_AngularVelocity);
