#ifndef __DEAL_V2201_H__
#define __DEAL_V2201_H__
#include <float.h>
#include <vector>
#include "ros/ros.h"
#include <sensor_msgs/BatteryState.h>
#include <geometry_msgs/TwistStamped.h>
#include <diagnostic_msgs/DiagnosticStatus.h>
#include "yidamsg/ArmDataStamped.h"

class dealV2201 : public BaseDeal
{
public:
	dealV2201(const ros::NodeHandle &nh, int _robot_id) : BaseDeal(nh, _robot_id)
	{
		partial_discharge_pub = nh_.advertise<diagnostic_msgs::DiagnosticStatus>("/yida/sensor/partial/discharge", 1);
		arm_data_pub = nh_.advertise<yidamsg::ArmDataStamped>("/yida/robot/arm/data", 1);
	}

private:
	ros::Publisher partial_discharge_pub;
	ros::Publisher arm_data_pub;

public:
	void pubWheel(const vector<unsigned char> &data)
	{
		int length = data.size();
		if (length == 182)
		{
			g_connect_num = 0;
			geometry_msgs::TwistStamped wheel_odom;
			int speedH = data[2];
			int speedL = data[3];
			int speed = speedH * 256 + speedL;
			// if (speedH > 7)
			if (speedH > 0x50)
			{
				speed = speed - 65536;
			}
			int angleH = data[4];
			int angleL = data[5];
			int angle = angleH * 256 + angleL;
			// if (angleH > 7)
			if (angleH > 0x50)
			{
				angle = angle - 65536;
			}
			// wheel_odom.twist.linear.x = speed / 1000.0;
			// wheel_odom.twist.angular.z = angle / 1000.0;
			wheel_odom.twist.linear.x = speed / 10000.0;
			wheel_odom.twist.angular.z = angle / 10000.0;
			wheel_odom.header.stamp = ros::Time::now();
			wheel_pub.publish(wheel_odom);
			//防跌落传感器1-4
			// topic:/yida/robot/sensor type:diagnostic_msgs/DiagnosticStatus
			diagnostic_msgs::DiagnosticStatus robot_sensor;
			int sensor6 = data[6] * 256 + data[7];
			setValue(&robot_sensor, "sensor6", sensor6);
			int sensor8 = data[8] * 256 + data[9];
			setValue(&robot_sensor, "sensor8", sensor8);
			int sensor10 = data[10] * 256 + data[11];
			setValue(&robot_sensor, "sensor10", sensor10);
			int sensor12 = data[12] * 256 + data[13];
			setValue(&robot_sensor, "sensor12", sensor12);
			ROS_DEBUG("fall: %i,%i,%i,%i", sensor6, sensor8, sensor10, sensor12);
			if (sensor_fall_front)
			{
				if (sensor8 > sensor_fall_value || sensor6 > sensor_fall_value)
				{
					sensor_fall_f_flag = true;
				}
				else
				{
					sensor_fall_f_flag = false;
				}
			}
			if (sensor_fall_back)
			{
				if (sensor10 > sensor_fall_value || sensor12 > sensor_fall_value)
				{
					sensor_fall_b_flag = true;
				}
				else
				{
					sensor_fall_b_flag = false;
				}
			}

			//超声波测距1-4
			int sensor14 = data[14];
			sensor_ultrasound_14 = data[14];
			setValue(&robot_sensor, "sensor14", sensor14);
			int sensor15 = data[15];
			sensor_ultrasound_15 = data[15];
			setValue(&robot_sensor, "sensor15", sensor15);
			int sensor16 = data[16];
			sensor_ultrasound_16 = data[16];
			setValue(&robot_sensor, "sensor16", sensor16);
			int sensor17 = data[17];
			sensor_ultrasound_17 = data[17];
			setValue(&robot_sensor, "sensor17", sensor17);
			ROS_DEBUG("ultrasound: %i,%i,%i,%i", sensor14, sensor15, sensor16, sensor17);
			if (sensor_ultrasound_back)
			{
				if ((sensor16 > 0 && sensor16 < sensor_ultrasound_value) && sensor16 != 25)
				{
					// ROS_INFO("sensor16 %i",sensor16);

					sensor_ultrasound_b_flag = true;
					ultrasound_counter_b = 20;
				}
				else
				{
					ultrasound_counter_b--;
					if (ultrasound_counter_b <= 0)
					{
						sensor_ultrasound_b_flag = false;
						ultrasound_counter_b = 0;
					}
				}
			}
			if (sensor_ultrasound_front)
			{
				if (((sensor14 > 0 && sensor14 < sensor_ultrasound_value) && sensor14 != 25) || ((sensor15 > 0 && sensor15 < sensor_ultrasound_value) && sensor15 != 25))
				{
					// ROS_INFO("sensor14 %i",sensor16);
					sensor_ultrasound_f_flag = true;
					ultrasound_counter_f = 20;
				}
				else
				{
					ultrasound_counter_f--;
					if (ultrasound_counter_f <= 0)
					{
						sensor_ultrasound_f_flag = false;
						ultrasound_counter_f = 0;
					}
				}
			}

			// if((sensor18>0 && sensor18 < 400)){
			// 	ROS_INFO("sensor18 %i",sensor18);
			// 	sensor_ultrasound_flag = true;
			// }

			//前后碰撞开关信息
			int sensor18_7 = data[18] / 128;
			setValue(&robot_sensor, "sensor18_7", sensor18_7);
			int sensor18_6 = data[18] % 128 / 64;
			setValue(&robot_sensor, "sensor18_6", sensor18_6);
			int sensor18_5 = data[18] % 128 % 64 / 32;
			setValue(&robot_sensor, "sensor18_5", sensor18_5);
			ROS_DEBUG("sensor18_7 %i sensor18_6 %i sensor18_5 %i", sensor18_7, sensor18_6, sensor18_5);

			if (sensor18_6 == 1 && sensor_collision_back)
			{
				// ROS_INFO("sensor18_6 %i",sensor18_6);
				sensor_collision_b_flag = true;
			}
			else
			{
				sensor_collision_b_flag = false;
			}
			if (sensor18_7 == 1 && sensor_collision_front)
			{
				// ROS_INFO("sensor18_7 %i",sensor18_7);
				sensor_collision_f_flag = true;
			}
			else
			{
				sensor_collision_f_flag = false;
			}
			remote_control_flag = sensor18_5 == 1 ? true : false;

			//温度传感器1-4
			signed short sensor19 = data[19] * 256 + data[20];
			signed short sensor21 = data[21] * 256 + data[22];
			signed short sensor23 = data[23] * 256 + data[24];
			signed short sensor25 = data[25] * 256 + data[26];
			setValue(&robot_sensor, "sensor19", sensor19);
			setValue(&robot_sensor, "sensor21", sensor21);
			setValue(&robot_sensor, "sensor23", sensor23);
			setValue(&robot_sensor, "sensor25", sensor25);

			// 1号电机电压/电流/温度
			int sensor27 = data[27] * 256 + data[28];
			int sensor29 = data[29] * 256 + data[30];
			int sensor31 = data[31];
			setValue(&robot_sensor, "sensor27", sensor27);
			setValue(&robot_sensor, "sensor29", sensor29);
			setValue(&robot_sensor, "sensor31", sensor31);
			// 2号电机电压/电流/温度
			int sensor32 = data[32] * 256 + data[33];
			int sensor34 = data[34] * 256 + data[35];
			int sensor36 = data[36];
			setValue(&robot_sensor, "sensor32", sensor32);
			setValue(&robot_sensor, "sensor34", sensor34);
			setValue(&robot_sensor, "sensor36", sensor36);
			// 3号电机电压/电流/温度
			int sensor37 = data[37] * 256 + data[38];
			int sensor39 = data[39] * 256 + data[40];
			int sensor41 = data[41];
			setValue(&robot_sensor, "sensor37", sensor37);
			setValue(&robot_sensor, "sensor39", sensor39);
			setValue(&robot_sensor, "sensor41", sensor41);
			// 4号电机电压/电流/温度
			int sensor42 = data[42] * 256 + data[43];
			int sensor44 = data[44] * 256 + data[45];
			int sensor46 = data[46];
			setValue(&robot_sensor, "sensor42", sensor42);
			setValue(&robot_sensor, "sensor44", sensor44);
			setValue(&robot_sensor, "sensor46", sensor46);
			// 5号电机电压/电流/温度
			int sensor47 = data[47] * 256 + data[48];
			int sensor49 = data[49] * 256 + data[50];
			int sensor51 = data[51];
			setValue(&robot_sensor, "sensor47", sensor47);
			setValue(&robot_sensor, "sensor49", sensor49);
			setValue(&robot_sensor, "sensor51", sensor51);
			// 6号电机电压/电流/温度
			int sensor52 = data[52] * 256 + data[53];
			int sensor54 = data[54] * 256 + data[55];
			int sensor56 = data[56];
			setValue(&robot_sensor, "sensor52", sensor52);
			setValue(&robot_sensor, "sensor54", sensor54);
			setValue(&robot_sensor, "sensor56", sensor56);
			// 7号电机电压/电流/温度
			int sensor57 = data[57] * 256 + data[58];
			int sensor59 = data[59] * 256 + data[60];
			int sensor61 = data[61];
			setValue(&robot_sensor, "sensor57", sensor57);
			setValue(&robot_sensor, "sensor59", sensor59);
			setValue(&robot_sensor, "sensor61", sensor61);
			// 8号电机电压/电流/温度
			int sensor62 = data[62] * 256 + data[63];
			int sensor64 = data[64] * 256 + data[65];
			int sensor66 = data[66];
			setValue(&robot_sensor, "sensor62", sensor62);
			setValue(&robot_sensor, "sensor64", sensor64);
			setValue(&robot_sensor, "sensor66", sensor66);

			//故障信息(数据异常/传输异常/线路异常)
			// byte 67
			int remainder67 = 0;
			int sensor67_7 = data[67] / 128;
			remainder67 = data[67] % 128;
			int sensor67_6 = remainder67 / 64;
			remainder67 = remainder67 % 64;
			int sensor67_5 = remainder67 / 32;
			remainder67 = remainder67 % 32;
			int sensor67_4 = remainder67 / 16;
			remainder67 = remainder67 % 16;
			int sensor67_3 = remainder67 / 8;
			remainder67 = remainder67 % 8;
			int sensor67_2 = remainder67 / 4;
			remainder67 = remainder67 % 4;
			int sensor67_1 = remainder67 / 2;
			remainder67 = remainder67 % 2;
			int sensor67_0 = remainder67 / 1;
			setValue(&robot_sensor, "sensor67_7", sensor67_7);
			setValue(&robot_sensor, "sensor67_6", sensor67_6);
			setValue(&robot_sensor, "sensor67_5", sensor67_5);
			setValue(&robot_sensor, "sensor67_4", sensor67_4);
			setValue(&robot_sensor, "sensor67_3", sensor67_3);
			setValue(&robot_sensor, "sensor67_2", sensor67_2);
			setValue(&robot_sensor, "sensor67_1", sensor67_1);
			setValue(&robot_sensor, "sensor67_0", sensor67_0);
			// byte 68
			int remainder68 = 0;
			int sensor68_7 = data[68] / 128;
			remainder68 = data[68] % 128;
			int sensor68_6 = remainder68 / 64;
			remainder68 = remainder68 % 64;
			int sensor68_5 = remainder68 / 32;
			remainder68 = remainder68 % 32;
			int sensor68_4 = remainder68 / 16;
			remainder68 = remainder68 % 16;
			int sensor68_3 = remainder68 / 8;
			remainder68 = remainder68 % 8;
			int sensor68_2 = remainder68 / 4;
			remainder68 = remainder68 % 4;
			int sensor68_1 = remainder68 / 2;
			remainder68 = remainder68 % 2;
			int sensor68_0 = remainder68 / 1;
			setValue(&robot_sensor, "sensor68_7", sensor68_7);
			setValue(&robot_sensor, "sensor68_6", sensor68_6);
			setValue(&robot_sensor, "sensor68_5", sensor68_5);
			setValue(&robot_sensor, "sensor68_4", sensor68_4);
			setValue(&robot_sensor, "sensor68_3", sensor68_3);
			setValue(&robot_sensor, "sensor68_2", sensor68_2);
			setValue(&robot_sensor, "sensor68_1", sensor68_1);
			setValue(&robot_sensor, "sensor68_0", sensor68_0);
			// byte 69
			int remainder69 = 0;
			int sensor69_7 = data[69] / 128;
			remainder69 = data[69] % 128;
			int sensor69_6 = remainder69 / 64;
			remainder69 = remainder69 % 64;
			int sensor69_5 = remainder69 / 32;
			remainder69 = remainder69 % 32;
			int sensor69_4 = remainder69 / 16;
			remainder69 = remainder69 % 16;
			int sensor69_3 = remainder69 / 8;
			remainder69 = remainder69 % 8;
			int sensor69_2 = remainder69 / 4;
			remainder69 = remainder69 % 4;
			int sensor69_1 = remainder69 / 2;
			remainder69 = remainder69 % 2;
			int sensor69_0 = remainder69 / 1;
			setValue(&robot_sensor, "sensor69_7", sensor69_7);
			setValue(&robot_sensor, "sensor69_6", sensor69_6);
			setValue(&robot_sensor, "sensor69_5", sensor69_5);
			setValue(&robot_sensor, "sensor69_4", sensor69_4);
			setValue(&robot_sensor, "sensor69_3", sensor69_3);
			setValue(&robot_sensor, "sensor69_2", sensor69_2);
			setValue(&robot_sensor, "sensor69_1", sensor69_1);
			setValue(&robot_sensor, "sensor69_0", sensor69_0);
			// byte 70
			int remainder70 = 0;
			int sensor70_7 = data[70] / 128;
			remainder70 = data[70] % 128;
			int sensor70_6 = remainder70 / 64;
			remainder70 = remainder70 % 64;
			int sensor70_5 = remainder70 / 32;
			remainder70 = remainder70 % 32;
			int sensor70_4 = remainder70 / 16;
			remainder70 = remainder70 % 16;
			int sensor70_3 = remainder70 / 8;
			remainder70 = remainder70 % 8;
			int sensor70_2 = remainder70 / 4;
			remainder70 = remainder70 % 4;
			int sensor70_1 = remainder70 / 2;
			remainder70 = remainder70 % 2;
			int sensor70_0 = remainder70 / 1;
			setValue(&robot_sensor, "sensor70_7", sensor70_7);
			setValue(&robot_sensor, "sensor70_6", sensor70_6);
			setValue(&robot_sensor, "sensor70_5", sensor70_5);
			setValue(&robot_sensor, "sensor70_4", sensor70_4);
			setValue(&robot_sensor, "sensor70_3", sensor70_3);
			setValue(&robot_sensor, "sensor70_2", sensor70_2);
			setValue(&robot_sensor, "sensor70_1", sensor70_1);
			setValue(&robot_sensor, "sensor70_0", sensor70_0);
			// byte 71
			int remainder71 = 0;
			int sensor71_7 = data[71] / 128;
			remainder71 = data[71] % 128;
			int sensor71_6 = remainder71 / 64;
			remainder71 = remainder71 % 64;
			int sensor71_5 = remainder71 / 32;
			remainder71 = remainder71 % 32;
			int sensor71_4 = remainder71 / 16;
			remainder71 = remainder71 % 16;
			int sensor71_3 = remainder71 / 8;
			remainder71 = remainder71 % 8;
			int sensor71_2 = remainder71 / 4;
			remainder71 = remainder71 % 4;
			int sensor71_1 = remainder71 / 2;
			remainder71 = remainder71 % 2;
			int sensor71_0 = remainder71 / 1;
			setValue(&robot_sensor, "sensor71_7", sensor71_7);
			setValue(&robot_sensor, "sensor71_6", sensor71_6);
			setValue(&robot_sensor, "sensor71_5", sensor71_5);
			setValue(&robot_sensor, "sensor71_4", sensor71_4);
			setValue(&robot_sensor, "sensor71_3", sensor71_3);
			setValue(&robot_sensor, "sensor71_2", sensor71_2);
			setValue(&robot_sensor, "sensor71_1", sensor71_1);
			setValue(&robot_sensor, "sensor71_0", sensor71_0);

			//充放电电流
			signed short sensor72 = data[72] * 256 + data[73];
			battery_electric_current = sensor72;
			setValue(&robot_sensor, "sensor72", sensor72);
			//电池剩余容量
			int sensor74 = data[74] * 256 + data[75];
			setValue(&robot_sensor, "sensor74", sensor74);
			// battery_info = ((float(sensor74) * 1.0) / 4000) * 100;
			//循环次数
			int sensor76 = data[76] * 256 + data[77];
			setValue(&robot_sensor, "sensor76", sensor76);
			// 1-13节电池电压
			int sensor78 = data[78] * 256 + data[79];
			int sensor80 = data[80] * 256 + data[81];
			int sensor82 = data[82] * 256 + data[83];
			int sensor84 = data[84] * 256 + data[85];
			int sensor86 = data[86] * 256 + data[87];
			int sensor88 = data[88] * 256 + data[89];
			int sensor90 = data[90] * 256 + data[91];
			int sensor92 = data[92] * 256 + data[93];
			int sensor94 = data[94] * 256 + data[95];
			int sensor96 = data[96] * 256 + data[97];
			int sensor98 = data[98] * 256 + data[99];
			int sensor100 = data[100] * 256 + data[101];
			int sensor102 = data[102] * 256 + data[103];
			setValue(&robot_sensor, "sensor78", sensor78);
			setValue(&robot_sensor, "sensor80", sensor80);
			setValue(&robot_sensor, "sensor82", sensor82);
			setValue(&robot_sensor, "sensor84", sensor84);
			setValue(&robot_sensor, "sensor86", sensor86);
			setValue(&robot_sensor, "sensor88", sensor88);
			setValue(&robot_sensor, "sensor90", sensor90);
			setValue(&robot_sensor, "sensor92", sensor92);
			setValue(&robot_sensor, "sensor94", sensor94);
			setValue(&robot_sensor, "sensor96", sensor96);
			setValue(&robot_sensor, "sensor98", sensor98);
			setValue(&robot_sensor, "sensor100", sensor100);
			setValue(&robot_sensor, "sensor102", sensor102);
			//电池温度 1-5
			signed char sensor104 = data[104];
			setValue(&robot_sensor, "sensor104", sensor104);
			signed char sensor105 = data[105];
			setValue(&robot_sensor, "sensor105", sensor105);
			signed char sensor106 = data[106];
			setValue(&robot_sensor, "sensor106", sensor106);
			signed char sensor107 = data[107];
			setValue(&robot_sensor, "sensor107", sensor107);
			signed char sensor108 = data[108];
			setValue(&robot_sensor, "sensor108", sensor108);
			//电池报警
			int remainder109 = 0;
			int sensor109_7 = data[109] / 128;
			remainder109 = data[109] % 128;
			int sensor109_6 = remainder109 / 64;
			remainder109 = remainder109 % 64;
			int sensor109_5 = remainder109 / 32;
			remainder109 = remainder109 % 32;
			int sensor109_4 = remainder109 / 16;
			remainder109 = remainder109 % 16;
			int sensor109_3 = remainder109 / 8;
			remainder109 = remainder109 % 8;
			int sensor109_2 = remainder109 / 4;

			setValue(&robot_sensor, "sensor109_7", sensor109_7);
			setValue(&robot_sensor, "sensor109_6", sensor109_6);
			setValue(&robot_sensor, "sensor109_5", sensor109_5);
			setValue(&robot_sensor, "sensor109_4", sensor109_4);
			setValue(&robot_sensor, "sensor109_3", sensor109_3);
			setValue(&robot_sensor, "sensor109_2", sensor109_2);
			// ADC1电压检测 1-4
			int sensor110 = data[110] * 256 + data[111];
			setValue(&robot_sensor, "sensor110", sensor110);
			int sensor112 = data[112] * 256 + data[113];
			setValue(&robot_sensor, "sensor112", sensor112);
			// int sensor114 = data[114] * 256 + data[115];
			// setValue(&robot_sensor, "sensor114", sensor114);
			// int sensor116 = data[116] * 256 + data[117];
			// setValue(&robot_sensor, "sensor116", sensor116);

			// arm
			int min_arm01 = data[114];
			setValue(&robot_sensor, "sensor114", min_arm01);
			int min_arm02 = data[115];
			setValue(&robot_sensor, "sensor115", min_arm02);
			int min_arm03 = data[116];
			setValue(&robot_sensor, "sensor116", min_arm03);
			int min_armL = data[117];
			setValue(&robot_sensor, "sensor117", min_armL);
			min_arm_length = min_armL * 10;
			int max_arm01 = data[118];
			setValue(&robot_sensor, "sensor118", max_arm01);
			int max_arm02 = data[119];
			setValue(&robot_sensor, "sensor119", max_arm02);
			int max_arm03 = data[120];
			setValue(&robot_sensor, "sensor120", max_arm03);
			int max_armH = data[121] * 256 + data[122];
			setValue(&robot_sensor, "sensor121", max_armH);
			max_arm_height = max_armH;
			int armStatus = data[123];
			int sensor123_2 = (armStatus >> 2) & 0x01;
			setValue(&robot_sensor, "sensor123_2", sensor123_2);
			int sensor123_1 = (armStatus >> 1) & 0x01;
			setValue(&robot_sensor, "sensor123_1", sensor123_1);
			// int sensor123_0 = armStatus & 0x01;
			// setValue(&robot_sensor, "sensor123_0", sensor123_0);
			// min_arm_origin_status = sensor123_0;
			min_arm_touch_status = sensor123_1;
			min_arm_end_status = sensor123_2;

			yidamsg::ArmDataStamped arm_msg;
			arm_msg.header.stamp = ros::Time::now();
			arm_msg.arm.robot_id = robot_id;
			arm_msg.arm.min_motor_angle1 = max_arm01;
			arm_msg.arm.min_motor_angle2 = max_arm02;
			arm_msg.arm.min_motor_angle3 = max_arm03;
			arm_msg.arm.min_arm_length = min_armL * 10;
			arm_msg.arm.max_motor_angle1 = max_arm01;
			arm_msg.arm.max_motor_angle2 = max_arm02;
			arm_msg.arm.max_motor_angle3 = max_arm03;
			arm_msg.arm.max_arm_height = max_armH;
			// arm_msg.arm.min_arm_origin = sensor123_0;
			arm_msg.arm.min_arm_touch = sensor123_1;
			arm_msg.arm.min_arm_end = sensor123_2;
			arm_data_pub.publish(arm_msg);

			// time
			int hour = data[124];
			int minute = data[125];
			int second = data[126];
			int millisecondL = data[127];
			int millisecondH = data[128];
			int rosSec = hour * 3600 + minute * 60 + second;
			int rosNsec = millisecondH * 256 + millisecondL;
			long time = rosSec * 1000 + rosNsec;
			setValue(&robot_sensor, "sensor124", time);
			// ROS_DEBUG("hour:%x minute:%x second:%x", hour, minute, second);
			// ROS_DEBUG("millisecondL:%x millisecondH:%x ", millisecondL, millisecondH);
			// ROS_DEBUG("==second:%i millisecond:%i", rosSec, rosNsec);

			//电源模块控制信息
			int remainder129 = 0;
			int sensor129_7 = data[129] / 128;
			remainder129 = data[129] % 128;
			int sensor129_6 = remainder129 / 64;
			b_motor_power = sensor129_6;
			remainder129 = remainder129 % 64;
			int sensor129_5 = remainder129 / 32;
			b_battery_charge = sensor129_5;
			remainder129 = remainder129 % 32;
			int sensor129_4 = remainder129 / 16;
			remainder129 = remainder129 % 16;
			int sensor129_3 = remainder129 / 8;
			remainder129 = remainder129 % 8;
			int sensor129_2 = remainder129 / 4;
			remainder129 = remainder129 % 4;
			int sensor129_1 = remainder129 / 2;
			remainder129 = remainder129 % 2;
			int sensor129_0 = remainder129 / 1;
			b_motor_enable = sensor129_0;

			setValue(&robot_sensor, "sensor129_6", sensor129_6);
			setValue(&robot_sensor, "sensor129_5", sensor129_5);
			setValue(&robot_sensor, "sensor129_4", sensor129_4);
			setValue(&robot_sensor, "sensor129_3", sensor129_3);
			setValue(&robot_sensor, "sensor129_2", sensor129_2);
			setValue(&robot_sensor, "sensor129_1", sensor129_1);
			setValue(&robot_sensor, "sensor129_0", sensor129_0);

			std::vector<unsigned char> temp;
			temp.push_back(data[133]);
			temp.push_back(data[132]);
			temp.push_back(data[131]);
			temp.push_back(data[130]);
			signed long int *sensor130 = (signed long int *)&temp[0];
			// printf("134|%x:%x:%x:%x = %i\n",data[130],data[131],data[132],data[133],*sensor130);
			setValue(&robot_sensor, "sensor130", *sensor130);
			temp.clear();
			temp.push_back(data[137]);
			temp.push_back(data[136]);
			temp.push_back(data[135]);
			temp.push_back(data[134]);
			signed long int *sensor134 = (signed long int *)&temp[0];
			setValue(&robot_sensor, "sensor134", *sensor134);
			temp.clear();
			temp.push_back(data[141]);
			temp.push_back(data[140]);
			temp.push_back(data[139]);
			temp.push_back(data[138]);
			signed long int *sensor138 = (signed long int *)&temp[0];
			setValue(&robot_sensor, "sensor138", *sensor138);
			temp.clear();
			temp.push_back(data[145]);
			temp.push_back(data[144]);
			temp.push_back(data[143]);
			temp.push_back(data[142]);
			signed long int *sensor142 = (signed long int *)&temp[0];
			setValue(&robot_sensor, "sensor142", *sensor142);
			temp.clear();
			temp.push_back(data[149]);
			temp.push_back(data[148]);
			temp.push_back(data[147]);
			temp.push_back(data[146]);
			signed long int *sensor146 = (signed long int *)&temp[0];
			setValue(&robot_sensor, "sensor146", *sensor146);
			temp.clear();
			temp.push_back(data[153]);
			temp.push_back(data[152]);
			temp.push_back(data[151]);
			temp.push_back(data[150]);
			signed long int *sensor150 = (signed long int *)&temp[0];
			setValue(&robot_sensor, "sensor150", *sensor150);
			temp.clear();
			temp.push_back(data[157]);
			temp.push_back(data[156]);
			temp.push_back(data[155]);
			temp.push_back(data[154]);
			signed long int *sensor154 = (signed long int *)&temp[0];
			setValue(&robot_sensor, "sensor154", *sensor154);
			temp.clear();
			temp.push_back(data[161]);
			temp.push_back(data[160]);
			temp.push_back(data[159]);
			temp.push_back(data[158]);
			signed long int *sensor158 = (signed long int *)&temp[0];
			setValue(&robot_sensor, "sensor158", *sensor158);
			// printf("158|%x:%x:%x:%x = %i\n",data[158],data[159],data[160],data[161],*sensor158);

			int sensor162 = data[162];
			setValue(&robot_sensor, "sensor162", sensor162);
			battery_info = sensor162;

			static unsigned int b_pub_num_flg = 0;
			b_pub_num_flg++;
			if (b_pub_num_flg > 50)
			{
				b_pub_num_flg = 0;
				// battery_state_msg
				sensor_msgs::BatteryState battery_state_msg;
				battery_state_msg.header.stamp = ros::Time::now();
				battery_state_msg.header.frame_id = std::to_string(robot_id);
				battery_state_msg.voltage = 48.0;
				float b_temperature = FLT_MIN;
				b_temperature = b_temperature > sensor104 ? b_temperature : sensor104;
				b_temperature = b_temperature > sensor105 ? b_temperature : sensor105;
				b_temperature = b_temperature > sensor106 ? b_temperature : sensor106;
				b_temperature = b_temperature > sensor107 ? b_temperature : sensor107;
				b_temperature = b_temperature > sensor108 ? b_temperature : sensor108;
				// battery_state_msg.temperature = b_temperature;
				battery_state_msg.current = sensor72;
				battery_state_msg.charge = sensor74;
				// battery_state_msg.capacity;
				// battery_state_msg.design_capacity;
				battery_state_msg.percentage = sensor162 * 0.01;
				battery_state_msg.power_supply_status = 0;
				if (sensor72 > 0)
				{
					battery_state_msg.power_supply_status = 1;
				}
				else if (sensor72 < 0)
				{
					battery_state_msg.power_supply_status = 2;
				}
				else
				{
					battery_state_msg.power_supply_status = 3;
					if ((sensor162 * 0.01) >= 1)
					{
						battery_state_msg.power_supply_status = 4;
					}
				}
				battery_state_msg.power_supply_health = 0;
				if (sensor109_7 || sensor109_6 || sensor109_5 || sensor109_4 || sensor109_3 || sensor109_2)
				{
					if (sensor109_7)
					{
						battery_state_msg.power_supply_health = 4;
					}
					if (sensor109_4)
					{
						battery_state_msg.power_supply_health = 2;
					}
					if (sensor109_6 || sensor109_5 || sensor109_3 || sensor109_2)
					{
						battery_state_msg.power_supply_health = 5;
					}
				}
				else
				{
					battery_state_msg.power_supply_health = 1;
				}
				battery_state_msg.power_supply_technology = 2;
				battery_state_msg.present = true;
				battery_state_msg.cell_voltage.push_back(sensor78);
				battery_state_msg.cell_voltage.push_back(sensor80);
				battery_state_msg.cell_voltage.push_back(sensor82);
				battery_state_msg.cell_voltage.push_back(sensor84);
				battery_state_msg.cell_voltage.push_back(sensor86);
				battery_state_msg.cell_voltage.push_back(sensor88);
				battery_state_msg.cell_voltage.push_back(sensor90);
				battery_state_msg.cell_voltage.push_back(sensor92);
				battery_state_msg.cell_voltage.push_back(sensor94);
				battery_state_msg.cell_voltage.push_back(sensor96);
				battery_state_msg.cell_voltage.push_back(sensor98);
				battery_state_msg.cell_voltage.push_back(sensor100);
				battery_state_msg.cell_voltage.push_back(sensor102);
				// battery_state_msg.cell_temperature.push_back(sensor104);
				// battery_state_msg.cell_temperature.push_back(sensor105);
				// battery_state_msg.cell_temperature.push_back(sensor106);
				// battery_state_msg.cell_temperature.push_back(sensor107);
				// battery_state_msg.cell_temperature.push_back(sensor108);
				// battery_state_msg.location;
				// battery_state_msg.serial_number;
				battery_state_pub.publish(battery_state_msg);
			}

			other_sensor_pub.publish(robot_sensor);

			if (data[165] == 187 && data[166] == 85)
			{
				diagnostic_msgs::DiagnosticStatus partial_discharge_data;
				int ae_num = data[167] * 256 + data[168];
				setValue(&partial_discharge_data, "ae_num", ae_num);
				int ae_amplitude = data[169] * 256 + data[170];
				setValue(&partial_discharge_data, "ae_amplitude", ae_amplitude);
				int ae_average = data[171] * 256 + data[172];
				setValue(&partial_discharge_data, "ae_average", ae_average);
				int tev_num = data[173] * 256 + data[174];
				setValue(&partial_discharge_data, "tev_num", tev_num);
				int tev_amplitude = data[175] * 256 + data[176];
				setValue(&partial_discharge_data, "tev_amplitude", tev_amplitude);
				int tev_average = data[177] * 256 + data[178];
				setValue(&partial_discharge_data, "tev_average", tev_average);

				partial_discharge_pub.publish(partial_discharge_data);
			}
		}
		// else if (length == 17)
		// {
		// 	diagnostic_msgs::DiagnosticStatus partial_discharge_data;
		// 	int ae_num = data[2] * 256 + data[3];
		// 	setValue(&partial_discharge_data, "ae_num", ae_num);
		// 	int ae_amplitude = data[4] * 256 + data[5];
		// 	setValue(&partial_discharge_data, "ae_amplitude", ae_amplitude);
		// 	int ae_average = data[6] * 256 + data[7];
		// 	setValue(&partial_discharge_data, "ae_average", ae_average);
		// 	int tev_num = data[8] * 256 + data[9];
		// 	setValue(&partial_discharge_data, "tev_num", tev_num);
		// 	int tev_amplitude = data[10] * 256 + data[11];
		// 	setValue(&partial_discharge_data, "tev_amplitude", tev_amplitude);
		// 	int tev_average = data[12] * 256 + data[13];
		// 	setValue(&partial_discharge_data, "tev_average", tev_average);

		// 	partial_discharge_pub.publish(partial_discharge_data);
		// }
	}
};

#endif
