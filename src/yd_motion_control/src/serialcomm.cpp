﻿#include "serialcomm.h"

using namespace std;

SerialComm::SerialComm(std::string _version, int _robot_id)
    : serial_version(_version), robot_id(_robot_id), b_open(false), b_run(false)
{
    charge_flg = false;
}

SerialComm::~SerialComm()
{
}

std::string SerialComm::get_version()
{
    return serial_version;
}

void *SerialComm::comThread(void *param)
{
    SerialComm *pThis = (SerialComm *)param;
    pThis->comThreadIml();
    return nullptr;
}

void SerialComm::startComThread(void)
{
    int result = pthread_create(&com_thread_t, NULL, comThread, (void *)this);
}

int SerialComm::connect(const std::string &deviceName, unsigned int baudRate)
{
    m_fd = open(deviceName.c_str(), O_RDWR | O_NOCTTY | O_NDELAY | O_NONBLOCK);
    if (m_fd < 0)
    {
        ROS_INFO("SerialComm: unable to open serial port %s", deviceName.c_str());
        return -1;
    }
    // struct timeval tv;
    // tv.tv_sec = 3000 / 1000;
    // tv.tv_usec = 3000 / 1000;
    // fd_set readfds;
    // FD_ZERO(&readfds);
    // FD_SET(m_fd, &readfds);
    // if (select(m_fd + 1, &readfds, NULL, NULL, &tv) <= 0)
    // {
    //     return -1;
    // }
    setFlags();
    if (epollInit(m_fd) <= 0)
    {
        return -1;
    }
    b_open = true;
    startComThread();

    return 0;
}

int SerialComm::disconnect()
{
    // fcntl(m_fd, F_SETFL, 0);
    // tcflush(m_fd, TCIOFLUSH);
    // tcsetattr(m_fd, TCSANOW, &m_oldopt);
    // ::close(m_fd);

    b_open = false;
    if (m_fd >= 0)
    {
        fcntl(m_fd, F_SETFL, 0);
        tcflush(m_fd, TCIOFLUSH);
        tcsetattr(m_fd, TCSANOW, &m_oldopt);
        if (epid >= 0)
        {
            ::close(epid);
            epid = -1;
        }
        ::close(m_fd);
        m_fd = -1;
    }

    return 0;
}

int SerialComm::epollInit(int cfd)
{
    epid = epoll_create(6); //放在初始化
    event.events = EPOLLET | EPOLLIN;
    event.data.fd = cfd;
    if (epoll_ctl(epid, EPOLL_CTL_ADD, cfd, &event) != 0)
    {
        ROS_INFO("set epoll error!");
        return 0;
    }
    tcflush(cfd, TCIOFLUSH); //清空串口输入输出缓存

    return 1;
}

int SerialComm::setBaudRate(int baudRate)
{
    return 0;
}

void SerialComm::setFlags()
{
    int nSpeed = 115200; // DEFAULT_BAUD_RATE
    int nBits = 8;
    char nEvent = 'N';
    int nStop = 1;
    struct termios newtio, oldtio;
    if (tcgetattr(m_fd, &oldtio) != 0)
    {
        perror("SetupSerial 1");
    }
    bzero(&newtio, sizeof(newtio));
    newtio.c_cflag |= CLOCAL | CREAD;
    newtio.c_cflag &= ~CSIZE;

    switch (nBits)
    {
    case 7:
        newtio.c_cflag |= CS7;
        break;
    case 8:
        newtio.c_cflag |= CS8;
        break;
    }

    switch (nEvent)
    {
    case 'O': //奇校验
        newtio.c_cflag |= PARENB;
        newtio.c_cflag |= PARODD;
        newtio.c_iflag |= (INPCK | ISTRIP);
        break;
    case 'E': //偶校验
        newtio.c_iflag |= (INPCK | ISTRIP);
        newtio.c_cflag |= PARENB;
        newtio.c_cflag &= ~PARODD;
        break;
    case 'N': //无校验
        newtio.c_cflag &= ~PARENB;
        break;
    }

    switch (nSpeed)
    {
    case 2400:
        cfsetispeed(&newtio, B2400);
        cfsetospeed(&newtio, B2400);
        break;
    case 4800:
        cfsetispeed(&newtio, B4800);
        cfsetospeed(&newtio, B4800);
        break;
    case 9600:
        cfsetispeed(&newtio, B9600);
        cfsetospeed(&newtio, B9600);
        break;
    case 115200:
        cfsetispeed(&newtio, B115200);
        cfsetospeed(&newtio, B115200);
        break;
    default:
        cfsetispeed(&newtio, B9600);
        cfsetospeed(&newtio, B9600);
        break;
    }
    if (nStop == 1)
    {
        newtio.c_cflag &= ~CSTOPB;
    }
    else if (nStop == 2)
    {
        newtio.c_cflag |= CSTOPB;
    }
    newtio.c_cc[VTIME] = 0;
    newtio.c_cc[VMIN] = 0;
    tcflush(m_fd, TCIFLUSH);
    if ((tcsetattr(m_fd, TCSANOW, &newtio)) != 0)
    {
        perror("com set error");
    }
}

void SerialComm::carCmdPacket(unsigned char *sendBuf)
{
    unsigned short checkSum = 0x00;
    unsigned char i = 0;

    for (i = 0; i < 11; i++)
    {
        checkSum += sendBuf[i];
    }
    sendBuf[11] = (checkSum >> 8) & 0x00FF;
    sendBuf[12] = checkSum & 0x00FF;
}

void SerialComm::sendSpeedToMcu(char smooth_type, float V, float W)
{
    string sendBuf = "s" + to_string(smooth_type) + to_string(V) + ":" + to_string(W) + "e";
    write(m_fd, sendBuf.c_str(), strlen(sendBuf.c_str()));
}

void SerialComm::sendSpeedToWCXD(char smooth_type, float V, float W)
{
    if (V < 0)
        W = (-1) * W;
    short linear_v = V * 1000;
    short angular_v = W * 1000;

    unsigned char send_buff[13];
    char i = 0;
    send_buff[0] = 0xAA;
    send_buff[1] = 0x55;
    send_buff[2] = 0x0D;
    send_buff[3] = 0x01;
    send_buff[4] = linear_v >> 8;
    send_buff[5] = linear_v & 0x00FF;

    send_buff[6] = angular_v >> 8;
    send_buff[7] = angular_v & 0x00FF;

    send_buff[8] = 0;
    send_buff[9] = 0;
    // if(fan_status){
    // 	send_buff[10] = 0x01;
    // }else{
    // 	send_buff[10] = 0x00;
    // }
    send_buff[10] = 0xFF;
    if (charge_flg)
    {
        // send_buff[10] = send_buff[10] | 0x02;
        send_buff[10] = 0xBF;
    }
    else
    {
        // send_buff[10] = send_buff[10] & 0xFD;
        send_buff[10] = 0xFD;
    }

    carCmdPacket(send_buff);

    if (charge_flg)
    {
        printf("send cmd to robot plate:");
        for (i = 0; i < 13; i++)
        {
            printf(" %x ", send_buff[i]);
        }
        printf("\r\n");
    }

    int send_size = write(m_fd, send_buff, 13);
    // printf("\r\nsend cmd to robot plate:");
    // for (i = 0; i < 13; i++)
    // {
    // 	printf(" %x", send_buff[i]);
    // }
    // printf("\r\n");

    //	printf( "send_size of %d  sendBuff[10]:%d linearSpeed: %x  %x ---angularSpeed : %x  %x",send_size,send_buff[10],send_buff[4],send_buff[5],send_buff[6],send_buff[7]);
}

void SerialComm::sendSpeedToDrive(float V, float W)
{
}

void SerialComm::sendLifts(float h_)
{
    short h = h_ * 1000;
    if (h >= 300)
    {
        /* code */
        h = 300;
    }
    lifts_height = h;
    /*
    unsigned char send_buff[10];
    send_buff[0] = 0xAA;
    send_buff[1] = 0x55;
    send_buff[2] = 0x0D;
    send_buff[3] = 0x01;
    send_buff[4] = 0x00;
    send_buff[5] = 0x00;

    send_buff[6] = 0x00;
    send_buff[7] = 0x00;

    send_buff[8] = lifts_height >> 8;
    send_buff[9] = lifts_height & 0x00FF;
    send_buff[10] = 0x00;

    carCmdPacket(send_buff);
    write(m_fd,send_buff, 13);
    */
}

void SerialComm::sendFan(bool status)
{
    fan_status = status;
}

void SerialComm::sendTest()
{
    unsigned char send_buff[2] = {0x55, 0xAA};
    int send_size = write(m_fd, send_buff, 2);
    printf("size of: %d", send_size);
}

void SerialComm::sendChargeStatus(int status)
{
    unsigned char send_buff[13] = {0xAA, 0x55, 0x0D, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x0D};
    send_buff[10] = 0xFF;
    if (status == 0)
    {
        send_buff[10] = 0xFD;
    }
    else
    {
        send_buff[10] = 0xFE;
    }
    carCmdPacket(send_buff);

    int send_size = write(m_fd, send_buff, 13);

    printf("sendChargeStatus %s: ", status == 1 ? "ON" : "OFF");
    for (int i = 0; i < 13; i++)
    {
        printf("%02x ", send_buff[i]);
    }
    printf("\r\n");
}

void SerialComm::sendMotorPower(int status)
{
    unsigned char send_buff[13] = {0xAA, 0x55, 0x0D, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x0D};
    send_buff[10] = 0xFF;
    if (status == 0)
    {
        if (charge_flg)
        {
            send_buff[10] = 0xFD;
        }
        else
        {
            send_buff[10] = 0xFC;
        }
    }
    else
    {
        if (charge_flg)
        {
            send_buff[10] = 0xFF;
        }
        else
        {
            send_buff[10] = 0xFE;
        }
    }
    carCmdPacket(send_buff);

    int send_size = write(m_fd, send_buff, 13);
}

void SerialComm::sendMotorEnable(int status)
{
    unsigned char send_buff[13] = {0xAA, 0x55, 0x0D, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x0D};
    send_buff[10] = 0xFF;
    if (status == 0)
    {
        if (charge_flg)
        {
            send_buff[10] = 0xFD;
        }
        else
        {
            send_buff[10] = 0xFC;
        }
    }
    else
    {
        if (charge_flg)
        {
            send_buff[10] = 0xFF;
        }
        else
        {
            send_buff[10] = 0xFE;
        }
    }
    carCmdPacket(send_buff);

    int send_size = write(m_fd, send_buff, 13);
}

void SerialComm::sendArmReset(int status)
{
}

void SerialComm::sendArmHeight(int vlaue)
{
}

void SerialComm::sendMinArmAction(int status)
{
}

void SerialComm::sendPDStatus(int status)
{
}

void SerialComm::sendLEDStatus(char status, char r, char g, char b, char s, char e, char t)
{
}

void *SerialComm::comThreadIml()
{
    gettimeofday(&last_time, NULL);
    b_run = true;
    int read_len = 0;
    int len = 0;
    int i = 0, witeNum = 0;
    vector<unsigned char> comData;
    bool is_complete = true;
    unsigned short receiveSum = 0;
    char sErr[2048], sRcv[4098];
    // while (1)
    while (b_open && b_run)
    {
        struct timeval now_time;
        gettimeofday(&now_time, NULL);
        if ((now_time.tv_sec - last_time.tv_sec) > 15)
        {
            ROS_DEBUG("timeout\n");
            b_run = false;
            disconnect();
            return nullptr;
        }

        witeNum = epoll_wait(epid, events, 1, 20);
        if (witeNum == 0)
        {
            ROS_DEBUG("wite num is 0");
            continue;
        }
        for (i = 0; i < witeNum; i++)
        {
            if ((events[i].events & EPOLLERR) || (events[i].events & EPOLLHUP) || (!(events[i].events & EPOLLIN)))
            {
                ROS_DEBUG("events com read error");
                break;
            }
            else if (events[i].events & EPOLLIN)
            {
                struct timeval tv;
                tv.tv_sec = 3;
                tv.tv_usec = 0;
                fd_set readfds;
                FD_ZERO(&readfds);
                FD_SET(events[i].data.fd, &readfds);
                if (select(events[i].data.fd + 1, &readfds, NULL, NULL, &tv) <= 0)
                {
                    ROS_DEBUG("select error");
                    break;
                }

                len = read(events[i].data.fd, sensorBuff, max_receive_num);
                // tcdrain(m_fd);
                // tcflush(m_fd, TCIFLUSH);
                if (len >= 0)
                {
                    //	ROS_DEBUG("com read size:%d", len);
                    // waitBuff.push_back(sensorBuff);
                    memset(sRcv, 0, sizeof(sRcv));
                    for (int i = 0; i < len; i++)
                    {
                        // ROS_DEBUG("value:%x", sensorBuff[i]);
                        sprintf(&sRcv[i * 3], "%02X ", sensorBuff[i]);
                        waitBuff.push_back(sensorBuff[i]);
                    }
                    //	ROS_INFO("com read size:%d,%d,%s", len,witeNum,sRcv);
                    // ROS_DEBUG("add finish size %i", waitBuff.size());
                    // deal data
                    int length = waitBuff.size();
                    int deleteLength = 0;
                    int not_com_num = 0;
                    //判断数据完整性
                    //以 aa 55 开始和结尾 并保证长度
                    is_complete = true;
                    for (int index = 0; index < length - 1;)
                    {
                        ROS_DEBUG("size:%i index:%i value:%x", length, index, waitBuff[index]);
                        unsigned char flag1 = waitBuff[index];
                        unsigned char flag2 = waitBuff[index + 1];
                        if (flag1 == 170 && flag2 == 85)
                        {
                            int endPos = index + data_size;
                            if (endPos <= length)
                            {
                                // check length
                                memset(sErr, 0, sizeof(sErr));
                                for (int j = index + 2; j < endPos - 2; j++)
                                {
                                    flag1 = waitBuff[j];
                                    flag2 = waitBuff[j + 1];
                                    sprintf(&sErr[(j - index - 2) * 3], "%02x ", waitBuff[j]);
                                    if (flag1 == 170 && flag2 == 85)
                                    {
                                        // not com
                                        is_complete = false;
                                        // ROS_DEBUG("j %i,index %i",j,index);
                                        not_com_num = j - index;
                                        deleteLength += not_com_num;
                                        ROS_INFO("not_com_num %i,deleteLength %i %s", not_com_num, deleteLength, sErr);
                                        index += length;
                                        j += data_size;
                                        continue;
                                    }
                                }
                                if (is_complete)
                                {
                                    // copy
                                    comData.clear();
                                    receiveSum = 0;
                                    ROS_DEBUG("complete data:");
                                    for (int j = index; j < endPos; j++)
                                    {
                                        /* code */
                                        if (j - index < data_size - 2)
                                        {
                                            receiveSum += waitBuff[j];
                                        }
                                        comData.push_back(waitBuff[j]);
                                        // ROS_DEBUG("%i %x", index,waitBuff[j]);
                                    }
                                    int serialSum = comData[data_size - 2] * 256 + comData[data_size - 1];
                                    // ROS_DEBUG("serialSum %i,receiveSum %i",serialSum,receiveSum);
                                    if (receiveSum == serialSum)
                                    {
                                        gettimeofday(&last_time, NULL);
                                    }
                                    if (receiveSum == serialSum && dealFunct != NULL)
                                    {
                                        dealFunct(comData);
                                        // ROS_INFO("deal function");
                                    }
                                    index += data_size;
                                    deleteLength += data_size;
                                    continue;
                                }
                            }
                            else
                            {
                                // ROS_DEBUG("break for loop");
                                index += data_size;
                            }
                        }
                        else
                        {
                            index++;
                            deleteLength++;
                        }
                    }
                    // delete incomplete data
                    // ROS_DEBUG("delete element size %i", deleteLength);
                    if (deleteLength > 0)
                        waitBuff.erase(std::begin(waitBuff), std::begin(waitBuff) + deleteLength);
                    // ROS_DEBUG("delete finish size %i", waitBuff.size());
                }
                else
                {
                    ROS_DEBUG("read error");
                }
                // ROS_INFO("capacity:%i size:%i max_size:%i", waitBuff.capacity(), waitBuff.size(), waitBuff.max_size());
                bzero(sensorBuff, max_receive_num);
            }
        }
    }
    b_run = false;
    disconnect();
    return nullptr;
}
