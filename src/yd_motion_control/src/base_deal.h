#ifndef __BASE_DEAL_H__
#define __BASE_DEAL_H__
#include <float.h>
#include <vector>
#include "ros/ros.h"
#include <sensor_msgs/BatteryState.h>
#include <geometry_msgs/TwistStamped.h>
#include <diagnostic_msgs/DiagnosticStatus.h>

class BaseDeal
{
public:
    BaseDeal(const ros::NodeHandle &nh = ros::NodeHandle("~"), int _robot_id = 0) : nh_(nh), robot_id(_robot_id)
    {
        wheel_pub = nh_.advertise<geometry_msgs::TwistStamped>("/yida/wheel/odometer", 1);
        other_sensor_pub = nh_.advertise<diagnostic_msgs::DiagnosticStatus>("/yida/robot/sensor", 1);
        battery_state_pub = nh_.advertise<sensor_msgs::BatteryState>("/yida/robot/battery", 1);
    };
    ~BaseDeal(){};
    //
    int robot_id;
    //
    int b_battery_charge = 0;
    int b_motor_power = 0;
    int b_motor_enable = 0;
    // sensor data
    int battery_info = 100;
    signed short battery_electric_current = 0;
    int sensor_ultrasound_14 = 0, sensor_ultrasound_15 = 0, sensor_ultrasound_16 = 0, sensor_ultrasound_17 = 0;
    int g_connect_num;
    // config
    bool sensor_ultrasound_front, sensor_ultrasound_back, sensor_fall_front, sensor_fall_back, sensor_collision_front, sensor_collision_back;
    int sensor_ultrasound_value = 50, sensor_fall_value = 36;
    int battery_charge_value = 95, battery_nocharge_value = 98;
    // sensor status
    bool sensor_fall_f_flag = false, sensor_fall_b_flag = false;
    bool sensor_collision_f_flag = false, sensor_collision_b_flag = false;
    bool sensor_ultrasound_f_flag = false, sensor_ultrasound_b_flag = false;
    bool remote_control_flag = false;
    // arm
    int min_arm_length = 0;
    int max_arm_height = 0;
    // int min_arm_origin_status = 0;
    int min_arm_touch_status = 0;
    int min_arm_end_status = 0;
    //
    virtual void pubWheel(const vector<unsigned char> &data){};

protected:
    ros::NodeHandle nh_;
    ros::Publisher wheel_pub, other_sensor_pub, battery_state_pub;
    int ultrasound_counter_b = 0, ultrasound_counter_f = 0;
    void setValue(diagnostic_msgs::DiagnosticStatus *status, string key, int value)
    {
        diagnostic_msgs::KeyValue sensor8_kv;
        sensor8_kv.key = key;
        sensor8_kv.value = std::to_string(value);
        status->values.push_back(sensor8_kv);
    }
};
#endif
