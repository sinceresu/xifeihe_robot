﻿
#include "serial_v2103.h"

Serial_v2103::Serial_v2103(int _robot_id) : SerialComm("v2103", _robot_id)
{
}

Serial_v2103::~Serial_v2103()
{
}

void Serial_v2103::sendSpeedToMcu(char smooth_type, float V, float W)
{
    string sendBuf = "s" + to_string(smooth_type) + to_string(V) + ":" + to_string(W) + "e";
    write(m_fd, sendBuf.c_str(), strlen(sendBuf.c_str()));
}

void Serial_v2103::sendSpeedToWCXD(char smooth_type, float V, float W)
{
    if (V < 0)
        W = (-1) * W;
    short linear_v = V * 1000;
    short angular_v = W * 1000;

    unsigned char send_buff[13];
    send_buff[0] = 0xAA;
    send_buff[1] = 0x55;
    send_buff[2] = 0x0D;
    send_buff[3] = 0x01;

    send_buff[4] = linear_v >> 8;
    send_buff[5] = linear_v & 0x00FF;

    send_buff[6] = angular_v >> 8;
    send_buff[7] = angular_v & 0x00FF;

    send_buff[8] = 0x00;
    send_buff[9] = 0x00;
    send_buff[10] = 0x00;

    carCmdPacket(send_buff);

    int send_size = write(m_fd, send_buff, 13);
}

void Serial_v2103::sendSpeedToDrive(float V, float W)
{
}

void Serial_v2103::sendLifts(float h_)
{
    short h = h_ * 1000;
    if (h >= 300)
    {
        /* code */
        h = 300;
    }
    lifts_height = h;
    /*
    unsigned char send_buff[10];
    send_buff[0] = 0xAA;
    send_buff[1] = 0x55;
    send_buff[2] = 0x0D;
    send_buff[3] = 0x01;
    send_buff[4] = 0x00;
    send_buff[5] = 0x00;


    send_buff[6] = 0x00;
    send_buff[7] = 0x00;

    send_buff[8] = lifts_height >> 8;
    send_buff[9] = lifts_height & 0x00FF;
    send_buff[10] = 0x00;

    carCmdPacket(send_buff);
    write(m_fd,send_buff, 13);
    */
}

void Serial_v2103::sendFan(bool status)
{
    fan_status = status;
}

void Serial_v2103::sendTest()
{
    unsigned char send_buff[2] = {0x55, 0xAA};
    int send_size = write(m_fd, send_buff, 2);
    printf("size of: %d", send_size);
}

void Serial_v2103::sendChargeStatus(int status)
{
    unsigned char send_buff[13] = {0xAA, 0x55, 0x0D, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x0D};
    if (status == 0)
    {
        send_buff[10] = 0x02;
    }
    else
    {
        send_buff[10] = 0x82;
    }
    carCmdPacket(send_buff);

    int send_size = write(m_fd, send_buff, 13);

    printf("sendChargeStatus %s: ", status == 1 ? "ON" : "OFF");
    for (int i = 0; i < 13; i++)
    {
        printf("%02x ", send_buff[i]);
    }
    printf("\r\n");
}

void Serial_v2103::sendMotorPower(int status)
{
    unsigned char send_buff[13] = {0xAA, 0x55, 0x0D, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x0D};
    if (status == 0)
    {
        send_buff[10] = 0x01;
    }
    else
    {
        send_buff[10] = 0x81;
    }
    carCmdPacket(send_buff);

    int send_size = write(m_fd, send_buff, 13);
}

void Serial_v2103::sendMotorEnable(int status)
{
    unsigned char send_buff[13] = {0xAA, 0x55, 0x0D, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x0D};
    if (status == 0)
    {
        send_buff[10] = 0x07;
    }
    else
    {
        send_buff[10] = 0x87;
    }
    carCmdPacket(send_buff);

    int send_size = write(m_fd, send_buff, 13);
}

void *Serial_v2103::comThreadIml()
{
    int read_len = 0;
    int len = 0;
    int i = 0, witeNum = 0;
    vector<unsigned char> comData;
    bool is_complete = true;
    unsigned short receiveSum = 0;
    char sErr[2048], sRcv[4098];
    while (1)
    {
        witeNum = epoll_wait(epid, events, 1, 20);
        if (witeNum == 0)
        {
            ROS_DEBUG("wite num is 0");
            continue;
        }
        for (i = 0; i < witeNum; i++)
        {
            if ((events[i].events & EPOLLERR) || (events[i].events & EPOLLHUP) || (!(events[i].events & EPOLLIN)))
            {
                ROS_DEBUG("com read error");
                break;
            }
            else if (events[i].events & EPOLLIN)
            {
                len = read(events[i].data.fd, sensorBuff, max_receive_num);
                // tcdrain(m_fd);
                // tcflush(m_fd, TCIFLUSH);
                if (len >= 0)
                {
                    //	ROS_DEBUG("com read size:%d", len);
                    // waitBuff.push_back(sensorBuff);
                    memset(sRcv, 0, sizeof(sRcv));
                    for (int i = 0; i < len; i++)
                    {
                        // ROS_DEBUG("value:%x", sensorBuff[i]);
                        sprintf(&sRcv[i * 3], "%02X ", sensorBuff[i]);
                        waitBuff.push_back(sensorBuff[i]);
                    }
                    //	ROS_INFO("com read size:%d,%d,%s", len,witeNum,sRcv);
                    // ROS_DEBUG("add finish size %i", waitBuff.size());
                    // deal data
                    int length = waitBuff.size();
                    int deleteLength = 0;
                    int not_com_num = 0;
                    //判断数据完整性
                    //以 aa 55 开始和结尾 并保证长度
                    is_complete = true;
                    for (int index = 0; index < length - 1;)
                    {
                        ROS_DEBUG("size:%i index:%i value:%x", length, index, waitBuff[index]);
                        unsigned char flag1 = waitBuff[index];
                        unsigned char flag2 = waitBuff[index + 1];
                        if (flag1 == 170 && flag2 == 85)
                        {
                            int endPos = index + data_size;
                            if (endPos <= length)
                            {
                                // check length
                                memset(sErr, 0, sizeof(sErr));
                                for (int j = index + 2; j < endPos - 2; j++)
                                {
                                    flag1 = waitBuff[j];
                                    flag2 = waitBuff[j + 1];
                                    sprintf(&sErr[(j - index - 2) * 3], "%02x ", waitBuff[j]);
                                    if (flag1 == 170 && flag2 == 85)
                                    {
                                        // not com
                                        is_complete = false;
                                        // ROS_DEBUG("j %i,index %i",j,index);
                                        not_com_num = j - index;
                                        deleteLength += not_com_num;
                                        ROS_INFO("not_com_num %i,deleteLength %i %s", not_com_num, deleteLength, sErr);
                                        index += length;
                                        j += data_size;
                                        continue;
                                    }
                                }
                                if (is_complete)
                                {
                                    // copy
                                    comData.clear();
                                    receiveSum = 0;
                                    ROS_DEBUG("complete data:");
                                    for (int j = index; j < endPos; j++)
                                    {
                                        /* code */
                                        if (j - index < data_size - 2)
                                        {
                                            receiveSum += waitBuff[j];
                                        }
                                        comData.push_back(waitBuff[j]);
                                        // ROS_DEBUG("%i %x", index,waitBuff[j]);
                                    }
                                    int serialSum = comData[data_size - 2] * 256 + comData[data_size - 1];
                                    // ROS_DEBUG("serialSum %i,receiveSum %i",serialSum,receiveSum);
                                    if (receiveSum == serialSum && dealFunct != NULL)
                                    {
                                        dealFunct(comData);
                                        // ROS_INFO("deal function");
                                    }
                                    index += data_size;
                                    deleteLength += data_size;
                                    continue;
                                }
                            }
                            else
                            {
                                // ROS_DEBUG("break for loop");
                                index += data_size;
                            }
                        }
                        else
                        {
                            index++;
                            deleteLength++;
                        }
                    }
                    // delete incomplete data
                    // ROS_DEBUG("delete element size %i", deleteLength);
                    if (deleteLength > 0)
                        waitBuff.erase(std::begin(waitBuff), std::begin(waitBuff) + deleteLength);
                    // ROS_DEBUG("delete finish size %i", waitBuff.size());
                }
                // ROS_INFO("capacity:%i size:%i max_size:%i", waitBuff.capacity(), waitBuff.size(), waitBuff.max_size());
                bzero(sensorBuff, max_receive_num);
            }
        }
    }
}