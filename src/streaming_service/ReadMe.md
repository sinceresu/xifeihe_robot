# Streaming service（流媒体服务）

## 安装编译环境:

### 升级ffmpeg
````shell
sudo add-apt-repository -y ppa:jonathonf/ffmpeg-4
sudo apt update
sudo apt install libx264-dev
sudo apt install libavcodec-dev libavformat-dev  libavutil-dev  libavfilter-dev  libavdevice-dev
````
### 降级ffmpeg

如果出现冲突，可以通过如下指令回退ffmpeg版本。
````shell
sudo apt install ppa-purge && sudo ppa-purge ppa:jonathonf/ffmpeg-4
````

### 安装libdrm
### 非必要
````shell
git clone https://github.com/freedreno/libdrm.git
cd libdrm
meson builddir/ --prefix=/usr
sudo ninja -C builddir/ install

sudo apt-get install libdrm-dev
````

### 安装gstreamer 流媒体服务器
````shell
sudo apt-get install libgstreamer-plugins-base1.0-dev libgstreamer-plugins-good1.0-dev libgstreamer-plugins-bad1.0-dev libgstrtspserver-1.0-dev gstreamer1.0-plugins-ugly gstreamer1.0-plugins-bad
````



## 编译ZLMediakit:

````shell
cd third_party/ZLMediaKit
mkdir build
cd build
cmake ..
make -j
````

##  订阅话题
###  位姿对齐温度图片
Topic: /infrared/streaminged 

Type: streaming_service_msgs::PosedImage
streaming_service_msgs::PosedImage定义：
```json
sensor_msgs/Image image   #温度图片 
geometry_msgs/Pose pose   # 当前位姿 
sensor_msgs/CameraInfo camera_info  #相机参数
```

##  发布话题


##  服务
###  开始发流任务
Service Name: streaming_service/start_streaming 

Type: streaming_service_msgs::start_streaming

```json
string camera_id
---
streaming_service_msgs/StatusResponse status
```

camera_id做为rtsp流的地址后缀，比如 rtsp://192.168.1.100:8554/camera_id
###  停止发流任务
Service Name: streaming_service/stop_streaming  

Type: streaming_service_msgs::stop_streaming 

streaming_service_msgs::stop_streaming定义：
```json
---
streaming_service_msgs/StatusResponse status
```
## 参数:

| 名称                        | 类型        | 描述                        | 缺省值            |
|--------------------- |-----------|----------------------|------------------|
| undistort_topic| string     | 对齐图片消息的topic  |/infrared/undistorted|
| streaming_port| int           | rtsp端口号       | 8554|
| min_celcius        | double  | 红外温度下限         | 0.0|
| max_celcius        | double | 红外温度上限        | 800.0|
| key_frame_interval        | int           | 关键帧间隔       | 10|
| constant_rate_factor     | int           | 恒定质量因子   | 10|


## 启动:
###  启动ZLMediaKit 流媒体服务器

````shell
ZLMediaKit/release/linux/Debug/MediaServer
````
###  启动服务

````shell
roslaunch streaming_service streaming_service.launch

````
