
#include <stdio.h>
#include <vector>
#include <random>
#include <memory>
#include <thread>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <opencv2/opencv.hpp>

#include "ros/ros.h"
#include <sensor_msgs/Image.h>
#include <cv_bridge/cv_bridge.h>


#include "gflags/gflags.h"
#include "glog/logging.h"




using namespace std;
using namespace cv;

namespace streaming_service{
namespace {
void GenStripImage(Mat& raw_image, float top_strip_value, float bottom_strip_value) {
  for (size_t row= 0; row < raw_image.rows/2; row++) {
    for (size_t col= 0; col < raw_image.cols; col++) {
      raw_image.at<float>(row, col) = top_strip_value;
    }
  }
  for (size_t row= raw_image.rows/2; row < raw_image.rows; row++) {
    for (size_t col= 0; col < raw_image.cols; col++) {
      raw_image.at<float>(row, col) = bottom_strip_value;
    }
  }
}

int kbhit(void)
{
  struct termios oldt, newt;
  int ch;
  int oldf;

  tcgetattr(STDIN_FILENO, &oldt);
  newt = oldt;
  newt.c_lflag &= ~(ICANON | ECHO);
  tcsetattr(STDIN_FILENO, TCSANOW, &newt);
  oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
  fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);

  ch = getchar();

  tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
  fcntl(STDIN_FILENO, F_SETFL, oldf);

  if (ch != EOF) {
    ungetc(ch, stdin);
    return 1;
  }

  return 0;
}
}
void Run() {
  ros::NodeHandle nh;
  ros::Publisher temp_pub =  nh.advertise<sensor_msgs::Image>(
        "/yida/internal/temperature", 1);

  Mat raw_image(512, 640, CV_32FC1);
  const float raw_min = 10.f;
  const float raw_max = 30;

  while (ros::ok()) {

    sensor_msgs::Image msg;

    GenStripImage(raw_image, raw_max, raw_max);
    std::this_thread::sleep_for(std::chrono::milliseconds(200));

    ros::WallTime wt = ros::WallTime::now();
    msg.header.stamp = ros::Time(wt.sec, wt.nsec);
    msg.header.frame_id = "temprature";

    cv_bridge::CvImage heat_cvimg(msg.header,
                            sensor_msgs::image_encodings::TYPE_32FC1, raw_image);
    heat_cvimg.toImageMsg(msg);

   wt = ros::WallTime::now();
    msg.header.stamp = ros::Time(wt.sec, wt.nsec);
    msg.header.frame_id = "temprature";

    // LOG(INFO) << raw_image.at<float>(0, 0) << " time: " << wt.toSec();

    temp_pub.publish(msg);


    GenStripImage(raw_image, raw_min, raw_min);
    std::this_thread::sleep_for(std::chrono::milliseconds(200));

     wt = ros::WallTime::now();
    msg.header.stamp = ros::Time(wt.sec, wt.nsec);
    msg.header.frame_id ="temprature";

    cv_bridge::CvImage temp_cvimg(msg.header,
                            sensor_msgs::image_encodings::TYPE_32FC1, raw_image);
    temp_cvimg.toImageMsg(msg);
     wt = ros::WallTime::now();
    msg.header.stamp = ros::Time(wt.sec, wt.nsec);

    msg.header.frame_id = "temprature";
    // LOG(INFO) << raw_image.at<float>(0, 0) << " time: " << wt.toSec();;

    temp_pub.publish(msg);

  }
}

void TestTransode(int argc, char** argv) {
  // google::InitGoogleLogging(argv[0]);
  // google::ParseCommandLineFlags(&argc, &argv, true);
  FLAGS_logtostderr = true;
  FLAGS_colorlogtostderr = true;

  ::ros::init(argc, argv, " test_transcode");

   auto workthread = thread(Run );

  
  ros::spin();



}

}


int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);
  FLAGS_logtostderr = true;
  LOG(INFO) << "start test streamer .";

  streaming_service::TestTransode(argc, argv);

  getchar();
 
}

