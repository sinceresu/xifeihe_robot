
#include <stdio.h>
#include <vector>
#include <thread>

#include <opencv2/opencv.hpp>


#include "gflags/gflags.h"
#include "glog/logging.h"

#include "ros/ros.h"
#include "rosbag/bag.h"
#include "rosbag/view.h"
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>
#include "tf2_eigen/tf2_eigen.h"

#include "undistort_service_msgs/PosedImage.h"

#include "../src/streamer.h"


DEFINE_string(streaming_url, "",
              "First directory in which configuration files are searched, "
              "second is always the Cartographer installation to allow "
              "including files from there.");
DEFINE_string(bag_filepath, "",
              "First directory in which configuration files are searched, "
              "second is always the Cartographer installation to allow "
              "including files from there.");

DEFINE_double(min_celcius, 0., "minimum temprature.");
DEFINE_double(max_celcius, 100., "Bag to streaming.");


using namespace std;
using namespace cv;

namespace streaming_service{
namespace {
  void TempToJet(const cv::Mat &temp, float celsius_min, float celsius_max,
                               cv::Mat &color) {

  const double alpha = 255.0 / (celsius_max - celsius_min);
  const double beta = -alpha * celsius_min;

  temp.convertTo(color, CV_8UC1, alpha, beta);
  cv::applyColorMap(color, color, cv::COLORMAP_JET);
}
void GenStripImage(Mat& raw_image, float min_value, float max_value) {
  for (size_t row= 0; row < raw_image.rows/2; row++) {
    for (size_t col= 0; col < raw_image.cols; col++) {
      raw_image.at<float>(row, col) = min_value;
    }
  }
  for (size_t row= raw_image.rows/2; row < raw_image.rows; row++) {
    for (size_t col= 0; col < raw_image.cols; col++) {
      raw_image.at<float>(row, col) = max_value;
    }
  }
}

void QuantizeTempertureImage( const cv::Mat& temperture_image, cv::Mat& quantized_img, double min_celcius, double max_celcius) {
  const double alpha = std::numeric_limits<uint16_t>::max() / (max_celcius - min_celcius);
  const double beta = -alpha * min_celcius;
  temperture_image.convertTo(quantized_img, CV_16UC1, alpha, beta);
    
}
void SplitUint16(uint16_t input, uint8_t &even_byte, uint8_t &odd_byte) {
  even_byte = 0u;
  odd_byte = 0u;
  for (size_t i = 0; i < 8; i++) {
    uint8_t even_bit =((1u << (i * 2)) & input ) != 0u ? 1u : 0u;
    uint8_t odd_bit = ((1u << (i * 2 + 1)) & input )  != 0u ? 1u : 0u;
    even_byte |= even_bit << i;
    odd_byte |= odd_bit << i;
  }
}

void ConvertQuantizedImageToYuv422(const cv::Mat& quantized_img, vector<cv::Mat> & yuv422_img) {
  cv::Mat& y_img = yuv422_img[0];
  cv::Mat& u_img = yuv422_img[1];
  cv::Mat& v_img = yuv422_img[2];

  for (size_t row= 0; row < quantized_img.rows; row++) {
    for (size_t col= 0; col < quantized_img.cols; col += 2) {

      uint16_t value1 = quantized_img.at<uint16_t>(row, col);
      uint8_t even_byte;
      uint8_t odd_byte;
      SplitUint16(value1, even_byte, odd_byte);
      y_img.at<uint8_t>(row, col) = even_byte;
      u_img.at<uint8_t>(row, col/2) = odd_byte;

      uint16_t value2 = quantized_img.at<uint16_t>(row, col + 1);
      SplitUint16(value2, even_byte, odd_byte);
      y_img.at<uint8_t>(row, col + 1) = even_byte;
      v_img.at<uint8_t>(row, col/2) = odd_byte;
    }
  }
}

static const uint8_t UV_OFFSET = 128u;
void ConvertQuantizedImageToYuv444(const cv::Mat& quantized_img, vector<cv::Mat> & yuv422_img) {
  cv::Mat& y_img = yuv422_img[0];
  cv::Mat& u_img = yuv422_img[1];
  cv::Mat& v_img = yuv422_img[2];

  for (size_t row= 0; row < quantized_img.rows; row++) {
    for (size_t col= 0; col < quantized_img.cols; col++) {

      uint16_t value1 = quantized_img.at<uint16_t>(row, col);
      uint8_t even_byte;
      uint8_t odd_byte;
      SplitUint16(value1, even_byte, odd_byte);

      y_img.at<uint8_t>(row, col) = even_byte;

      // uint8_t even_half_byte;
      // uint8_t odd_half_byte;
      // SplitUint8(odd_byte, even_half_byte, odd_half_byte);

      u_img.at<uint8_t>(row, col) = odd_byte;
      v_img.at<uint8_t>(row, col) =  UV_OFFSET;
    }
  }
}

void ConvertQuantizedImageToYUV422P10(const cv::Mat& quantized_img, vector<cv::Mat> & yuv422_img) {
  cv::Mat& y_img = yuv422_img[0];
    for (size_t row= 0; row < quantized_img.rows; row++) {
      for (size_t col= 0; col < quantized_img.cols; col++) { 
        uint16_t value = quantized_img.at<uint16_t>(row, col);
        y_img.at<uint16_t>(row, col) = value >> 6;

      }
    }

}

void ConvertQuantizedImageToYUV444P10(const cv::Mat& quantized_img, vector<cv::Mat> & yuv422_img) {
  cv::Mat& y_img = yuv422_img[0];
    for (size_t row= 0; row < quantized_img.rows; row++) {
      for (size_t col= 0; col < quantized_img.cols; col++) { 
        uint16_t value = quantized_img.at<uint16_t>(row, col);
        y_img.at<uint16_t>(row, col) = (value + 32u) >> 6;

      }
    }

}
}

void TestStreaming(int argc, char** argv) {

  std::vector<cv::Mat> yuv422_img(3);


  Streamer streamer;
  streamer.SetEncodeParam(5, 10);
  uint64_t start_timestamp;
  uint64_t last_timestamp;
  int64_t timestamp;
  bool initialized = false;
  int i = 0;

  while(1) {
    rosbag::Bag bag;
    bag.open(FLAGS_bag_filepath, rosbag::bagmode::Read);
    rosbag::View view(bag);

      
    for (const rosbag::MessageInstance& message : view) {
      if (!::ros::ok()) {
        break;
      }
      if(!message.isType<undistort_service_msgs::PosedImage>()) 
        continue;

        auto posed_img_msg = message.instantiate<undistort_service_msgs::PosedImage>();
        cv_bridge::CvImageConstPtr cv_ptr= cv_bridge::toCvCopy(posed_img_msg->image, sensor_msgs::image_encodings::TYPE_32FC1);
        cv::Mat temp_img = cv_ptr->image.clone();
        // uint64_t timestamp = posed_img_msg->image.header.stamp.toNSec() / 1000000;

        Eigen::Affine3d camera_to_map;
        Eigen::Affine3d robot_to_map;
        Eigen::Vector2i ptz(posed_img_msg->ptz.horizontal, posed_img_msg->ptz.vertical);
        Eigen::fromMsg(posed_img_msg->camera, camera_to_map);
        Eigen::fromMsg(posed_img_msg->robot, robot_to_map);

        LOG(INFO) <<  "sending : " << i <<  "th frame" ;

      if (!initialized) {
        const auto& input_image = cv_ptr->image;
        if (0 != streamer.Initialize(input_image.cols, input_image.rows, FLAGS_streaming_url, 5, timestamp)) {
          LOG(ERROR) << "Failed to initialize!";
          break;
        }
        yuv422_img[0]  = Mat(input_image.rows, input_image.cols, CV_16UC1, Scalar(0));
        yuv422_img[1]  = Mat(input_image.rows, input_image.cols, CV_16UC1, Scalar(512));
        yuv422_img[2]  = Mat(input_image.rows, input_image.cols, CV_16UC1, Scalar(512));
        start_timestamp = timestamp;
        last_timestamp = timestamp;
        initialized = true;
      }

      // cv::Mat raw_image(cv_ptr->image.rows, cv_ptr->image.cols, CV_32FC1);
      // if (i%2)
      //   GenStripImage(raw_image, 0, 800);
      // else
      //   GenStripImage(raw_image, 800, 0);

      cv::Mat raw_image = cv_ptr->image.clone();
      cv::Mat jet_image;
      TempToJet(temp_img, 0, 30, jet_image);
      std::string jet_file = "/home/sujin/output/tio/jet_output_" + std::to_string(i) + ".png";
      cv::imwrite(jet_file, jet_image);
      
      cv::Mat quantized_img;
      QuantizeTempertureImage(raw_image, quantized_img, FLAGS_min_celcius, FLAGS_max_celcius);
      // QuantizeTempertureImage(raw_image, quantized_img, FLAGS_min_celcius, FLAGS_max_celcius);
      ConvertQuantizedImageToYUV444P10(quantized_img, yuv422_img);

      // std::this_thread::sleep_for(std::chrono::milliseconds(timestamp - last_timestamp));
      std::this_thread::sleep_for(std::chrono::milliseconds(150));

      streamer.Send(yuv422_img, camera_to_map, robot_to_map, ptz, "", "", timestamp);

      last_timestamp = timestamp;
      timestamp += 200;
      i++;
    }
  }

  streamer.Release();

}

}


int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);
  FLAGS_logtostderr = true;
  LOG(INFO) << "start test streamer .";

  ros::init(argc, argv, "rosbag_publisher");
  ros::start();
  streaming_service::TestStreaming(argc, argv);

  getchar();
 
}

