
#include <stdio.h>
#include <vector>
#include <random>
#include <memory>
#include <thread>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <opencv2/opencv.hpp>


#include "gflags/gflags.h"
#include "glog/logging.h"

#include <sensor_msgs/Image.h>
#include "../src/streamer.h"


DEFINE_string(streaming_url, "",

              "First directory in which configuration files are searched, "
              "second is always the Cartographer installation to allow "
              "including files from there.");



using namespace std;
using namespace cv;

namespace streaming_service{
namespace {
void GenStripImage(Mat& raw_image, float min_value, float max_value) {
  for (size_t row= 0; row < raw_image.rows/2; row++) {
    for (size_t col= 0; col < raw_image.cols; col++) {
      raw_image.at<float>(row, col) = min_value;
    }
  }
  for (size_t row= raw_image.rows/2; row < raw_image.rows; row++) {
    for (size_t col= 0; col < raw_image.cols; col++) {
      raw_image.at<float>(row, col) = max_value;
    }
  }
}

int kbhit(void)
{
  struct termios oldt, newt;
  int ch;
  int oldf;

  tcgetattr(STDIN_FILENO, &oldt);
  newt = oldt;
  newt.c_lflag &= ~(ICANON | ECHO);
  tcsetattr(STDIN_FILENO, TCSANOW, &newt);
  oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
  fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);

  ch = getchar();

  tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
  fcntl(STDIN_FILENO, F_SETFL, oldf);

  if (ch != EOF) {
    ungetc(ch, stdin);
    return 1;
  }

  return 0;
}
}
void TestStreamer(int argc, char** argv) {

  Mat raw_image(512, 640, CV_32FC1);
  const float raw_min = 0.f;
  const float raw_max = 800;

  const float alpha = std::numeric_limits<uint8_t>::max() / (raw_max - raw_min);
  const float beta = -alpha * raw_min;

  Streamer streamer;
  if (0 != streamer.Initialize(raw_image.cols, raw_image.rows, FLAGS_streaming_url, 5, 0)) {
    LOG(ERROR) << "Failed to initialize!";
  }
  
  int64_t timestamp = 0;
  std::vector<Mat> yuv422_image(3);
  yuv422_image[1]  = Mat(raw_image.rows,  raw_image.cols, CV_8UC1, Scalar(128));
  yuv422_image[2]  = Mat(raw_image.rows,  raw_image.cols, CV_8UC1, Scalar(128));

  while(1) {
    GenStripImage(raw_image, raw_min, raw_max);
    raw_image.convertTo(yuv422_image[0], CV_8UC1, alpha, beta);
    streamer.Send(yuv422_image, Eigen::Affine3d::Identity(), Eigen::Affine3d::Identity(), Eigen::Vector2i::Identity(), "", "", timestamp);
    std::this_thread::sleep_for(std::chrono::milliseconds(200));
    timestamp +=  200;

    GenStripImage(raw_image, raw_max, raw_min);
    raw_image.convertTo(yuv422_image[0], CV_8UC1, alpha, beta);
    streamer.Send(yuv422_image, Eigen::Affine3d::Identity(), Eigen::Affine3d::Identity(), Eigen::Vector2i::Identity(), "", "", timestamp);
    std::this_thread::sleep_for(std::chrono::milliseconds(200));
    timestamp +=  200;
    if (kbhit() != 0) {
      int ch = getchar();
      if (ch == 's') {
        getchar();
      }
      else
        break;
    }

  }
  streamer.Release();

}

}


int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);
  FLAGS_logtostderr = true;
  LOG(INFO) << "start test streamer .";

  streaming_service::TestStreamer(argc, argv);

  getchar();
 
}

