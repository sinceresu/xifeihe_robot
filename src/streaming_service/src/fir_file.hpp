#ifndef fir_file_HPP
#define fir_file_HPP

#pragma once
#include <stdlib.h>
#include <string>
#include <vector>
#include <string.h>
#include <iostream>
#include <fstream>

class CFIR
{
public:
    CFIR();
    ~CFIR();

public:
    int Read(std::string _file, unsigned short &_width, unsigned short &_height, std::vector<float> &_temps);
    int Write(std::string _file, unsigned short _width, unsigned short _height, std::vector<float> _temps);
};

#endif