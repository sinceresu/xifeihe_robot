#include <algorithm>
#include <fstream>
#include <iostream>

#include "gflags/gflags.h"
#include "glog/logging.h"

#include "streaming_node.h"
#include "transcode_node.h"

using namespace std;

namespace streaming_service {
void Run(int argc, char** argv) {
  LOG(INFO) << "start streaming service.";

  StreamingNode  streaming_node;
  TranscodeNode  transcode_node;

  ::ros::spin();

  LOG(INFO) << "finished  streaming service.";

}

}


int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);
  FLAGS_logtostderr = true;
  FLAGS_colorlogtostderr = true;

  ::ros::init(argc, argv, " streaming_service_node");
  
  streaming_service::Run(argc, argv);

}


