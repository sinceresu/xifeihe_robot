#pragma once

#include <string>
#include <memory>
#include <vector>
#include <mutex>
#include <thread>


#include <opencv2/opencv.hpp>
#include <Eigen/Core>
#include <Eigen/Geometry>

#include "ros/ros.h"
#include <std_msgs/String.h>
#include <sensor_msgs/Image.h>

#include "streaming_service_msgs/start_streaming.h"
#include "streaming_service_msgs/stop_streaming.h"
#include "streaming_service_msgs/ColorizeInfo.h"
#include "undistort_service_msgs/PosedImage.h"

#include "jsoncpp/json/json.h"
#include "jsoncpp/json/value.h"
#include "jsoncpp/json/writer.h"
#include "fir_file.hpp"


namespace  streaming_service{
class Streamer;

class StreamingNode {
 public:
    enum StreamingResult {
        SUCCESS = 0,
        IO_ERROR = -2,
        INVALID_PARAM = -4,
        ERROR = -5
    };
  enum StreamingAction {
        START = 0,
        STOP = 1,
  };
  StreamingNode();
  ~StreamingNode();

  StreamingNode(const StreamingNode&) = delete;
  StreamingNode& operator=(const StreamingNode&) = delete;

  private:
    enum StreamingState {
        IDLE = 0,
        STREAMINGING = 1,
    };

    void GetParameters();
    bool HandleStartStreaming(
        streaming_service_msgs::start_streaming::Request& request,
        streaming_service_msgs::start_streaming::Response& response);
        
    bool HandleStopStreaming(
        streaming_service_msgs::stop_streaming::Request& request,
        streaming_service_msgs::stop_streaming::Response& response);
     
    void OnPosedImage(const std::string& topic, const undistort_service_msgs::PosedImage::ConstPtr& image_msg);

    void OnAppendData(const std::string& topic, const std_msgs::String::ConstPtr& data_msg);

    void PublishColorizeInfo(std::string camera_id, int type);
    
    void SendPosedImage(uint64_t timestamp_ms);
    
    void InitializeImages(const cv::Size& image_size);
    int Initialize(const cv::Size& image_size, uint64_t start_time_ms);   

    void Run();

    int get_local_ip(const char *eth_inf, char *ip);

    ::ros::NodeHandle node_handle_;

    std::vector<::ros::ServiceServer> service_servers_;

    ::ros::Publisher colorize_info_publisher_;

    ::ros::Subscriber infrared_temp_subscriber_;

    ::ros::Subscriber posed_image_subscriber_;

    ::ros::Subscriber append_data_subscriber_;

    int state_ = StreamingState::IDLE;

    std::string temperature_topic__;
    std::string undistort_topic_;

    std::string streaming_topic_;

    std::string image_rtsp_;
    std::string file_path_;
  
    std::string camera_id_;
    int streaming_port_;
    float min_celcius_;
    float max_celcius_;

    std::unique_ptr<Streamer> streamer_;
    cv::Mat quantized_img_;
    std::vector<cv::Mat> yuv_img_;
    uint64_t start_timestamp_;
    int key_frame_interval_;
    int constant_rate_factor_;

    int save_test_data_;

    bool initialized_;

    bool stop_flag_;

    std::mutex mutex_;
    bool input_ready_;

    std::string task_data_;
    std::string append_data_;

    cv::Mat input_frame_;
    Eigen::Affine3d camera_pose_;
    Eigen::Affine3d robot_pose_;
    Eigen::Vector2i ptz_angle_;
    std::thread work_thread_;
  };
  
}
