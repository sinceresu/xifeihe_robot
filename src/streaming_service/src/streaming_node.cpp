#include "streaming_node.h"
#include <limits>
#include <iostream>
#include <fstream>
#include <thread>
#include <chrono>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <net/if.h>
#include <arpa/inet.h>

#include <boost/filesystem.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/imgcodecs.hpp>

#include "gflags/gflags.h"
#include "glog/logging.h"

#include "ros/package.h"
#include <cv_bridge/cv_bridge.h>
#include "tf2_eigen/tf2_eigen.h"
#include <sensor_msgs/Image.h>
#include "streaming_service/node_constants.h"

#include "streamer.h"
#include "common/err_code.h"

using namespace std;
using namespace cv;

namespace streaming_service
{
  constexpr double kTfBufferCacheTimeInSeconds = 10.;
  const int kDefaultFrameRate = 5;
  namespace
  {
    constexpr int kLatestOnlyPublisherQueueSize = 1;

    // Subscribes to the 'topic' for 'trajectory_id' using the 'node_handle' and
    // calls 'handler' on the 'node' to handle messages. Returns the subscriber.
    template <typename MessageType>
    ::ros::Subscriber SubscribeWithHandler(
        void (StreamingNode::*handler)(const std::string &,
                                       const typename MessageType::ConstPtr &),
        const std::string &topic,
        ::ros::NodeHandle *const node_handle, StreamingNode *const node)
    {
      return node_handle->subscribe<MessageType>(
          topic, kLatestOnlyPublisherQueueSize,
          boost::function<void(const typename MessageType::ConstPtr &)>(
              [node, handler,
               topic](const typename MessageType::ConstPtr &msg)
              {
                (node->*handler)(topic, msg);
              }));
    }

    void QuantizeTempertureImage(const cv::Mat &temperture_image, cv::Mat &quantized_img, float min_celcius, float max_celcius)
    {
      const float alpha = std::numeric_limits<uint16_t>::max() / (max_celcius - min_celcius);
      const float beta = -alpha * min_celcius;
      temperture_image.convertTo(quantized_img, CV_16UC1, alpha, beta);
    }

    // void ConvertQuantizedImageToYuv422(const cv::Mat& quantized_img, vector<cv::Mat> & yuv422_img) {
    //   cv::Mat& y_img = yuv422_img[0];
    //   cv::Mat& u_img = yuv422_img[1];
    //   cv::Mat& v_img = yuv422_img[2];

    //   for (size_t row= 0; row < quantized_img.rows; row++) {
    //     for (size_t col= 0; col < quantized_img.cols; col += 2) {
    //       uint16_t value1 = quantized_img.at<uint16_t>(row, col);
    //       y_img.at<uint8_t>(row, col) = static_cast<uint8_t>(value1 & 0xff);
    //       u_img.at<uint8_t>(row, col/2) = static_cast<uint8_t>((value1 >>8) & 0xff);
    //       uint16_t value2 = quantized_img.at<uint16_t>(row, col + 1);
    //       y_img.at<uint8_t>(row, col + 1) = static_cast<uint8_t>(value2 & 0xff);
    //       v_img.at<uint8_t>(row, col/2) = static_cast<uint8_t>((value2 >>8) & 0xff);
    //     }
    //   }
    // }
    void SplitUint16(uint16_t input, uint8_t &even_byte, uint8_t &odd_byte)
    {
      even_byte = 0u;
      odd_byte = 0u;
      for (size_t i = 0; i < 8; i++)
      {
        uint8_t even_bit = ((1u << (i * 2)) & input) != 0u ? 1u : 0u;
        uint8_t odd_bit = ((1u << (i * 2 + 1)) & input) != 0u ? 1u : 0u;
        even_byte |= even_bit << i;
        odd_byte |= odd_bit << i;
      }
    }

    void SplitUint8(uint8_t input, uint8_t &even_byte, uint8_t &odd_byte)
    {
      even_byte = 0u;
      odd_byte = 0u;
      for (size_t i = 0; i < 4; i++)
      {
        uint8_t even_bit = ((1u << (i * 2)) & input) != 0u ? 1u : 0u;
        uint8_t odd_bit = ((1u << (i * 2 + 1)) & input) != 0u ? 1u : 0u;
        even_byte |= even_bit << i;
        odd_byte |= odd_bit << i;
      }
    }

    void ConvertQuantizedImageToYuv422(const cv::Mat &quantized_img, vector<cv::Mat> &yuv422_img)
    {
      cv::Mat &y_img = yuv422_img[0];
      cv::Mat &u_img = yuv422_img[1];
      cv::Mat &v_img = yuv422_img[2];

      for (size_t row = 0; row < quantized_img.rows; row++)
      {
        for (size_t col = 0; col < quantized_img.cols; col += 2)
        {

          uint16_t value1 = quantized_img.at<uint16_t>(row, col);
          uint8_t even_byte;
          uint8_t odd_byte;
          SplitUint16(value1, even_byte, odd_byte);
          y_img.at<uint8_t>(row, col) = even_byte;
          u_img.at<uint8_t>(row, col / 2) = odd_byte;

          uint16_t value2 = quantized_img.at<uint16_t>(row, col + 1);
          SplitUint16(value2, even_byte, odd_byte);
          y_img.at<uint8_t>(row, col + 1) = even_byte;
          v_img.at<uint8_t>(row, col / 2) = odd_byte;
        }
      }
    }

    static const uint8_t UV_OFFSET = 128u;
    void ConvertQuantizedImageToYuv444(const cv::Mat &quantized_img, vector<cv::Mat> &yuv422_img)
    {
      cv::Mat &y_img = yuv422_img[0];
      cv::Mat &u_img = yuv422_img[1];
      cv::Mat &v_img = yuv422_img[2];

      for (size_t row = 0; row < quantized_img.rows; row++)
      {
        for (size_t col = 0; col < quantized_img.cols; col++)
        {

          uint16_t value1 = quantized_img.at<uint16_t>(row, col);
          uint8_t even_byte;
          uint8_t odd_byte;
          SplitUint16(value1, even_byte, odd_byte);

          y_img.at<uint8_t>(row, col) = even_byte;

          // uint8_t even_half_byte;
          // uint8_t odd_half_byte;
          // SplitUint8(odd_byte, even_half_byte, odd_half_byte);

          u_img.at<uint8_t>(row, col) = even_byte;
          v_img.at<uint8_t>(row, col) = UV_OFFSET;
        }
      }
    }

    void ConvertQuantizedImageToYUV444P10(const cv::Mat &quantized_img, vector<cv::Mat> &yuv422_img)
    {
      cv::Mat &y_img = yuv422_img[0];
      for (size_t row = 0; row < quantized_img.rows; row++)
      {
        for (size_t col = 0; col < quantized_img.cols; col++)
        {
          uint16_t value = quantized_img.at<uint16_t>(row, col);
          y_img.at<uint16_t>(row, col) = (value + 32u) >> 6;
        }
      }
    }
  }

  StreamingNode::StreamingNode()
      : task_data_(""), append_data_("")
  {
    GetParameters();

    service_servers_.push_back(node_handle_.advertiseService(
        kStartStreamingServiceName, &StreamingNode::HandleStartStreaming, this));

    service_servers_.push_back(node_handle_.advertiseService(
        kStopStreamingServiceName, &StreamingNode::HandleStopStreaming, this));

    colorize_info_publisher_ = node_handle_.advertise<streaming_service_msgs::ColorizeInfo>(
        kColorizeInfoTopicName, 1);

    append_data_subscriber_ = SubscribeWithHandler<std_msgs::String>(
        &StreamingNode::OnAppendData, kAppendDataTopicName, &node_handle_, this);
  }

  StreamingNode::~StreamingNode()
  {
  }

  void StreamingNode::GetParameters()
  {

    ::ros::NodeHandle private_handle("~");

    private_handle.param<std::string>("/streaming_service/temperature_topic_", temperature_topic__, "/yida/internal/temperature");
    private_handle.param<std::string>("/streaming_service/undistort_topic", undistort_topic_, "/infrared/undistorted");

    private_handle.param<int>("/streaming_service/streaming_port", streaming_port_, 554);

    private_handle.param<std::string>("/streaming_service/image_rtsp", image_rtsp_, "");

    private_handle.param<float>("/streaming_service/min_celcius", min_celcius_, -20.0f);
    private_handle.param<float>("/streaming_service/max_celcius", max_celcius_, 600.0f);

    private_handle.param<int>("/streaming_service/key_frame_interval", key_frame_interval_, 5);
    private_handle.param<int>("/streaming_service/constant_rate_factor", constant_rate_factor_, 0);

    private_handle.param<int>("/streaming_service/save_test_data", save_test_data_, 0);

    ros::param::get("~file_path", file_path_);
  }

  bool StreamingNode::HandleStartStreaming(
      streaming_service_msgs::start_streaming::Request &request,
      streaming_service_msgs::start_streaming::Response &response)
  {
    ROS_INFO("HandleStartStreaming");
    ROS_INFO_STREAM("HandleStartStreaming req:\n"
                    << request);
    if (state_ == StreamingState::STREAMINGING)
    {
      response.status.code = StreamingResult::SUCCESS;
      response.status.message = "streaminging.";

      PublishColorizeInfo(request.camera_id, 1);

      LOG(WARNING) << "streaminging! ";
      return true;
    }
    camera_id_ = request.camera_id;
    task_data_ = request.task_data;

    input_ready_ = false;
    stop_flag_ = false;
    initialized_ = false;

    work_thread_ = thread(bind(&StreamingNode::Run, this));

    posed_image_subscriber_ = SubscribeWithHandler<undistort_service_msgs::PosedImage>(
        &StreamingNode::OnPosedImage, undistort_topic_, &node_handle_,
        this);
    response.status.code = StreamingResult::SUCCESS;
    state_ = StreamingState::STREAMINGING;

    return true;
  }

  bool StreamingNode::HandleStopStreaming(
      streaming_service_msgs::stop_streaming::Request &request,
      streaming_service_msgs::stop_streaming::Response &response)
  {
    ROS_INFO("HandleStopStreaming");
    if (state_ == StreamingState::IDLE)
    {
      response.status.code = StreamingResult::SUCCESS;
      response.status.message = "not streaming.";

      PublishColorizeInfo(request.camera_id, 0);

      LOG(WARNING) << "not streaming! ";
      return true;
    }

    stop_flag_ = true;
    if (work_thread_.joinable())
      work_thread_.join();

    if (initialized_)
    {
      streamer_->Release();
    }

    posed_image_subscriber_.shutdown();
    std::this_thread::sleep_for(std::chrono::milliseconds(50));

    response.status.code = StreamingResult::SUCCESS;
    state_ = StreamingState::IDLE;

    PublishColorizeInfo(request.camera_id, 0);

    return true;
  }

  void StreamingNode::OnPosedImage(const std::string &topic, const undistort_service_msgs::PosedImage::ConstPtr &img_msg)
  {
    // ROS_INFO("HandleSyncMessage");

    cv_bridge::CvImageConstPtr cv_ptr = cv_bridge::toCvCopy(img_msg->image, sensor_msgs::image_encodings::TYPE_32FC1);
    Eigen::Affine3d camera;
    Eigen::Affine3d robot;
    Eigen::Vector2i ptz(img_msg->ptz.horizontal, img_msg->ptz.vertical);
    Eigen::fromMsg(img_msg->camera, camera);
    Eigen::fromMsg(img_msg->robot, robot);

    {
      unique_lock<std::mutex> lock(mutex_);
      input_frame_ = cv_ptr->image.clone();
      camera_pose_ = camera;
      robot_pose_ = robot;
      ptz_angle_ = ptz;
      input_ready_ = true;
    }

    // uint64_t timestamp = img_msg->image.header.stamp.toNSec() / 1000000;
    // SendPosedImage(timestamp);
  }

  void StreamingNode::OnAppendData(const std::string &topic, const std_msgs::String::ConstPtr &data_msg)
  {
    append_data_ = data_msg->data;
  }

  void StreamingNode::PublishColorizeInfo(std::string camera_id, int type)
  {
    char ip[16] = {0x0};
    get_local_ip("eth1", ip);
    stringstream streaming_url;
    streaming_url << "rtsp://" << ip << ":" << streaming_port_ << "/live/" << camera_id_;
    streaming_service_msgs::ColorizeInfo msg;
    msg.camera_id = camera_id;
    msg.rtsp_url = streaming_url.str();
    msg.min_celcius = min_celcius_;
    msg.max_celcius = max_celcius_;
    msg.type = type;
    colorize_info_publisher_.publish(msg);
  }

  void StreamingNode::SendPosedImage(uint64_t timestamp_ms)
  {
    cv::Mat quantized_img;
    Eigen::Affine3d camera;
    Eigen::Affine3d robot;
    Eigen::Vector2i ptz;
    {
      unique_lock<std::mutex> lock(mutex_);
      if (!input_ready_)
        return;

      if (!initialized_)
      {
        int ret = Initialize(input_frame_.size(), timestamp_ms);
        if (ERRCODE_OK != ret)
        {
          return;
        }
      }

      QuantizeTempertureImage(input_frame_, quantized_img, min_celcius_, max_celcius_);
      camera = camera_pose_;
      robot = robot_pose_;
      ptz = ptz_angle_;
      input_ready_ = false;
    }

    ConvertQuantizedImageToYUV444P10(quantized_img, yuv_img_);

    if (ERRCODE_OK != streamer_->Send(yuv_img_, camera, robot, ptz, task_data_, append_data_, timestamp_ms))
    {
      LOG(WARNING) << "Failed to streaming!";
    }

    // write file
    LOG(INFO) << task_data_;
    Json::Reader task_reader;
    Json::Value task_json;
    if (!task_reader.parse(task_data_, task_json))
    {
      // std::count << "json parse error" << std::endl;
      return;
    }
    std::string device_id = task_json["device_id"].asString();
    std::string task_id = task_json["task_id"].asString();

    LOG(INFO) << append_data_;
    Json::Reader append_reader;
    Json::Value append_json;
    if (!append_reader.parse(append_data_, append_json))
    {
      // std::count << "json parse error" << std::endl;
      return;
    }
    int motion_state = append_json["motion_state"].asString();

    std::string point_id = "0";
    std::string point_type = "0";
    std::string position = "0,0,0";
    std::string object_id = "0";
    // std::string device_id = "0";
    // std::string task_id = "0";
    if (motion_state == 0)
    {
      point_id = append_json["point_id"].asString();
      point_type = append_json["point_type"].asString();
      position = append_json["position"].asString();
      object_id = append_json["object_id"].asString();
      // device_id = append_json["device_id"].asString();
      // task_id = append_json["task_id"].asString();
    }

    std::string task_file = file_path_ + "/task/" + task_id + ".text";
    std::string fir_file = file_path_ + "/fir/" + task_id + "_" + point_id + "_" + std::to_string(timestamp_ms) + ".fir";
    std::string image_file = file_path_ + "/image/" + task_id + "_" + point_id + "_" + std::to_string(timestamp_ms) + ".jpg";
    // pose
    Eigen::Quaterniond rotation(camera.linear());
    const auto &translation = camera.translation();
    std::stringstream ir_pose_str;
    ir_pose_str << translation.x() << "," << translation.y() << "," << translation.z() << "," << rotation.x() << "," << rotation.y() << "," << rotation.z() << "," << rotation.w();

    if (save_test_data_ == 1)
    {
      // fir
      try
      {
        int dataIndex = 0;
        float fTemp = 0;
        std::vector<float> temps;
        unsigned char *p_temp_data = input_frame_.data;
        for (int height = 0; height < input_frame_.rows; height++)
        {
          for (int width = 0; width < input_frame_.cols; width++)
          {
            fTemp = *((float *)(p_temp_data + dataIndex));
            dataIndex += 4;

            int index = (height * input_frame_.cols) + width;

            temps.push_back(fTemp);
          }
        }
        CFIR fir;
        fir.Write(fir_file, input_frame_.cols, input_frame_.rows, temps);
      }
      catch (const std::exception &e)
      {
        std::cerr << e.what() << '\n';
      }
      // image
      try
      {
        bool b_done = false;
        ros::Time last_time = ros::Time::now();
        ros::Time attemp_end = last_time + ros::Duration(10.0);
        ros::Rate rate(1);
        while (ros::ok() && !b_done)
        {
          cv::VideoCapture capture;
          capture.open(image_rtsp_);
          while (capture.isOpened() && !b_done)
          {
            cv::Mat frame;
            capture >> frame;
            if (!frame.empty())
            {
              cv::imwrite(image_file, frame);

              b_done = true;
              break;
            }
            if (ros::Time::now() > attemp_end)
            {
              b_done = true;
              break;
            }
          }
          if (capture.isOpened())
          {
            capture.release();
          }
          // ------
          if (ros::Time::now() > attemp_end)
          {
            b_done = true;
            break;
          }

          ros::spinOnce();
          rate.sleep();
        }
      }
      catch (const std::exception &e)
      {
        std::cerr << e.what() << '\n';
      }
      // text
      fstream f;
      f.open(task_file, ios::out | ios::app);
      f << device_id << ";" << task_id << ";" << fir_file << ";" << image_file << ";" << ir_pose_str.str() << "\r\n";
      f.close();
    }
  }

  int StreamingNode::Initialize(const cv::Size &image_size, uint64_t start_time_ms)
  {
    InitializeImages(image_size);

    streamer_ = unique_ptr<Streamer>(new Streamer());
    streamer_->SetEncodeParam(key_frame_interval_, constant_rate_factor_);

    stringstream streaming_url;
    streaming_url << "rtsp://localhost:" << streaming_port_ << "/live/" << camera_id_;
    if (0 != streamer_->Initialize(image_size.width, image_size.height, streaming_url.str(), kDefaultFrameRate, start_time_ms))
    {
      streamer_->Release();
      LOG(WARNING) << "Failed to initialize rtsp pusher!";
      return ERRCODE_INIT_NETWORK;
    }

    start_timestamp_ = start_time_ms;
    initialized_ = true;
    return ERRCODE_OK;
  }

  void StreamingNode::InitializeImages(const cv::Size &image_size)
  {
    yuv_img_ = std::vector<cv::Mat>(3);
    yuv_img_[0] = Mat(image_size.height, image_size.width, CV_16UC1, Scalar(0));
    yuv_img_[1] = Mat(image_size.height, image_size.width, CV_16UC1, Scalar(512));
    yuv_img_[2] = Mat(image_size.height, image_size.width, CV_16UC1, Scalar(512));
  }

  void StreamingNode::Run()
  {
    // chrono::duration sleep_ms = chrono::milliseconds(1000 / kDefaultFrameRate);
    LOG(INFO) << "Enter StreamingNode work thead!";
    Eigen::Affine3d invalid_pose;
    Eigen::Vector2i invalid_angle(100000, 100000);
    invalid_pose.translation() = Eigen::Vector3d(100000.0f, 100000.0f, 100000.0f);
    while (ros::ok() && !stop_flag_)
    {
      this_thread::sleep_for(chrono::milliseconds(1000 / kDefaultFrameRate));
      uint64_t timestamp = ros::Time::now().toNSec() / 1000000;

      if (!initialized_)
      {
        boost::shared_ptr<sensor_msgs::Image const> infrared_temp_sub;
        infrared_temp_sub = ros::topic::waitForMessage<sensor_msgs::Image>(temperature_topic__, ros::Duration(1.0));
        if (infrared_temp_sub == nullptr)
        {
          continue;
        }
        cv::Size image_size(infrared_temp_sub->width, infrared_temp_sub->height);
        int ret = Initialize(image_size, timestamp);
        if (ERRCODE_OK != ret)
        {
          sleep(1);
          continue;
        }
        else
        {
          PublishColorizeInfo(camera_id_, 1);
        }
      }

      if (input_ready_)
      {
        SendPosedImage(timestamp);
        continue;
      }

      if (!initialized_)
      {
        continue;
      }

      if (ERRCODE_OK != streamer_->Send(yuv_img_, invalid_pose, invalid_pose, invalid_angle, task_data_, append_data_, timestamp))
      {
        LOG(WARNING) << "Failed to streaming!";
      }
    }

    LOG(INFO) << "Exit StreamingNode work thead!";
  }

  // 获取本机ip
  int StreamingNode::get_local_ip(const char *eth_inf, char *ip)
  {
    int sd;
    struct sockaddr_in sin;
    struct ifreq ifr;

    sd = socket(AF_INET, SOCK_DGRAM, 0);
    if (-1 == sd)
    {
      printf("socket error: %s", strerror(errno));
      return -1;
    }

    strncpy(ifr.ifr_name, eth_inf, IFNAMSIZ);
    ifr.ifr_name[IFNAMSIZ - 1] = 0;
    // if error: No such device
    if (ioctl(sd, SIOCGIFADDR, &ifr) < 0)
    {
      printf("ioctl error: %s", strerror(errno));
      close(sd);
      return -1;
    }

    memcpy(&sin, &ifr.ifr_addr, sizeof(sin));
    snprintf(ip, 16, "%s", inet_ntoa(sin.sin_addr));

    close(sd);

    return 0;
  }

}
