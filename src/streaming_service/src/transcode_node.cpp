#include "transcode_node.h"
#include <limits>
#include <iostream>
#include <fstream>
#include <thread> 
#include <chrono> 
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <net/if.h>
#include <arpa/inet.h>

#include <boost/filesystem.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/imgcodecs.hpp>

#include "gflags/gflags.h"
#include "glog/logging.h"

#include "ros/package.h"
#include <cv_bridge/cv_bridge.h>
#include "tf2_eigen/tf2_eigen.h"
#include <sensor_msgs/Image.h>
#include "streaming_service/node_constants.h"

#include "streamer.h"
#include "common/err_code.h"

using namespace std;
using namespace cv;

namespace streaming_service {
constexpr double kTfBufferCacheTimeInSeconds = 10.;
const int kDefaultFrameRate = 4;
namespace {
constexpr int kLatestOnlyPublisherQueueSize = 1;

// Subscribes to the 'topic' for 'trajectory_id' using the 'node_handle' and
// calls 'handler' on the 'node' to handle messages. Returns the subscriber.
template <typename MessageType>
::ros::Subscriber SubscribeWithHandler(
    void (TranscodeNode::*handler)(const std::string&,
                                    const typename MessageType::ConstPtr&),
      const std::string& topic,
    ::ros::NodeHandle* const node_handle, TranscodeNode* const node) {
  return node_handle->subscribe<MessageType>(
      topic, kLatestOnlyPublisherQueueSize,
      boost::function<void(const typename MessageType::ConstPtr&)>(
          [node, handler,
           topic](const typename MessageType::ConstPtr& msg) {
            (node->*handler)(topic, msg);
          }));
}

void TempToJet(const cv::Mat &temp, float celsius_min, float celsius_max,
                               cv::Mat &color) {

  const double alpha = 255.0 / (celsius_max - celsius_min);
  const double beta = -alpha * celsius_min;

  temp.convertTo(color, CV_8UC1, alpha, beta);
  cv::applyColorMap(color, color, cv::COLORMAP_JET);
}


}

TranscodeNode::TranscodeNode() {
  GetParameters();

  service_servers_.push_back(node_handle_.advertiseService(
    kStartTranscodeServiceName, &TranscodeNode::HandleStartTranscode, this));

  service_servers_.push_back(node_handle_.advertiseService(
    kStopTranscodeServiceName, &TranscodeNode::HandleStopTranscode, this));

  stop_flag_ = false;
  initialized_ = false;
  // work_thread_ = thread(bind(&TranscodeNode::Run, this));
  // state_ = TranscodeState::TRANSCODING;
}

TranscodeNode::~TranscodeNode() {
}

void TranscodeNode::GetParameters() {

  ::ros::NodeHandle private_handle("~");

  private_handle.param<std::string>("/streaming_service/camera_id", camera_id_, "4");

  private_handle.param<std::string>("/streaming_service/temperature_topic_", temperature_topic__, "/yida/internal/temperature");

  private_handle.param<int>("/streaming_service/streaming_port", streaming_port_,  554);
 
  private_handle.param<float>("/streaming_service/colorize_min_celcius", colorize_min_celcius_,  10.0f);
  private_handle.param<float>("/streaming_service/colorize_max_celcius", colorize_max_celcius_,  30.0f);

  private_handle.param<int>("/streaming_service/key_frame_interval", key_frame_interval_,  5);
  private_handle.param<int>("/streaming_service/transcode_constant_rate_factor", constant_rate_factor_,  0);
}

bool TranscodeNode::HandleStartTranscode(
        streaming_service_msgs::start_transcode::Request& request,
        streaming_service_msgs::start_transcode::Response& response) {
  ROS_INFO("HandleStartStreaming");
  ROS_INFO_STREAM("HandleStartStreaming req:\n"
                        << request);
  if (state_ == TranscodeState::TRANSCODING) {
      response.status.code = TranscodeResult::SUCCESS;
      response.status.message = "streaminging.";


      LOG(WARNING) << "streaminging! " ;
      return true;  
  }
  camera_id_ = request.camera_id;

  stop_flag_ = false;
  initialized_ = false;

  work_thread_ = thread(bind(&TranscodeNode::Run, this));

  response.status.code = TranscodeResult::SUCCESS;
  state_ = TranscodeState::TRANSCODING;

  return true;
}
bool TranscodeNode::HandleStopTranscode(
        streaming_service_msgs::stop_transcode::Request& request,
        streaming_service_msgs::stop_transcode::Response& response) {
  ROS_INFO("HandleStopStreaming");
  if (state_ == TranscodeState::IDLE) {
      response.status.code = TranscodeResult::SUCCESS;
      response.status.message = "not streaming.";


      LOG(WARNING) << "not streaming! " ;
      return true;  
  }

  stop_flag_ = true;
  if (work_thread_.joinable())
    work_thread_.join();

  if (initialized_) {
    streamer_->Release();
  }


  response.status.code = TranscodeResult::SUCCESS;
  state_ = TranscodeState::IDLE;


  return true;
}


void TranscodeNode::SendImage(uint64_t timestamp_ms) {

  if (!initialized_) {
    int ret = Initialize(input_frame_.size(), timestamp_ms);
    if (ERRCODE_OK != ret)
    {
      return;
    }
  }

  if (ERRCODE_OK != streamer_->SendBGRImage(input_frame_, timestamp_ms)) {
    LOG(WARNING) << "Failed to streaming!";
  }
}

int TranscodeNode::Initialize(const cv::Size& image_size, uint64_t start_time_ms){
  InitializeImages(image_size);

  streamer_ = unique_ptr<Streamer>(new Streamer(PIX_FMT_BGR24));
  streamer_->SetEncodeParam(key_frame_interval_, constant_rate_factor_);

  stringstream streaming_url;
  streaming_url << "rtsp://localhost:" << streaming_port_<< "/infrared/" <<  camera_id_;
  if (0 != streamer_->Initialize(image_size.width, image_size.height, streaming_url.str(), kDefaultFrameRate, start_time_ms)) {
    streamer_->Release();
    LOG(WARNING) << "Failed to initialize rtsp pusher!";
    return ERRCODE_INIT_NETWORK;
  }

  start_timestamp_ = start_time_ms;
  initialized_ = true;
  return ERRCODE_OK;
}

void TranscodeNode::InitializeImages(const cv::Size& image_size){
  bgr_img_  = Mat(image_size.height,  image_size.width, CV_8UC3,  Scalar(0, 128, 128));
}

void TranscodeNode::Run() {
  // chrono::duration sleep_ms = chrono::milliseconds(1000 / kDefaultFrameRate);
  while(ros::ok() && !stop_flag_) {
    sensor_msgs::ImageConstPtr infrared_temp_sub = ros::topic::waitForMessage<sensor_msgs::Image>(temperature_topic__, ros::Duration(1.0));
    if (infrared_temp_sub == nullptr)
    {
      continue;
    }

    uint64_t timestamp =infrared_temp_sub->header.stamp.toNSec() / 1000000 ;// in ms;
    if (!initialized_) {
      
      cv::Size image_size(infrared_temp_sub->width, infrared_temp_sub->height);
      int ret = Initialize(image_size, timestamp);
      if (ERRCODE_OK != ret)
      {
        sleep(1);
        continue;
      }

    }

    cv_bridge::CvImageConstPtr cv_ptr= cv_bridge::toCvCopy(*infrared_temp_sub, sensor_msgs::image_encodings::TYPE_32FC1);
    TempToJet(cv_ptr->image, colorize_min_celcius_, colorize_max_celcius_, input_frame_);
    
#ifndef NDEBUG
    putText(input_frame_, to_string(timestamp), Point2i(200, 200), FONT_HERSHEY_SIMPLEX, 1, Scalar(255, 255, 255) , 3);
    LOG(INFO) << "timestamp: " << timestamp;
  #endif
    SendImage(timestamp);
  }
    LOG(INFO) << "Exit TranscodeNode work thead!";

}

}

