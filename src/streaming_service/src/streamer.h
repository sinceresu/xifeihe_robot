#pragma once

#include <string>
#include <memory>
#include <vector>
#include <deque>

#include <opencv2/opencv.hpp>
#include <Eigen/Geometry>


typedef struct AVCodec AVCodec;
typedef struct AVCodecContext AVCodecContext;
typedef struct AVFrame AVFrame;
typedef struct AVFormatContext AVFormatContext;
typedef struct AVStream AVStream;
typedef struct AVPacket AVPacket;
typedef struct SwsContext SwsContext;
typedef struct AVBitStreamFilter AVBitStreamFilter;
typedef struct AVBSFContext AVBSFContext;
typedef struct SwsContext SwsContext;

namespace  streaming_service{

enum PixelFormat {
  PIX_FMT_BGR24,
  PIX_FMT_YUV444P10LE,
};

class Streamer {
 public:
  Streamer(PixelFormat pixel_format = PIX_FMT_YUV444P10LE);
  ~Streamer();

  Streamer(const Streamer&) = delete;
  Streamer& operator=(const Streamer&) = delete;
  int Initialize(int image_width, int image_heights, const std::string & streaming_url, int frame_rate, uint64_t start_time_ms);
  void Release();
  void SetEncodeParam(int key_frame_interval, int constant_rate_factor);

  // int Send(const std::vector<cv::Mat>& image_yuv, const Eigen::Affine3d& pose, int64_t timestamp_ms);
  int Send(const std::vector<cv::Mat>& image_yuv, const Eigen::Affine3d& camera, const Eigen::Affine3d& robot, const Eigen::Vector2i& ptz,
          const std::string task_data, const std::string append_data, int64_t timestamp_ms);
  int SendBGRImage(const cv::Mat& image_bgr, int64_t timestamp_ms);

  private:
    // std::string GenerateSEIHead(const Eigen::Affine3d& pose);
    std::string GenerateSEIHead(const Eigen::Affine3d& camera, const Eigen::Affine3d& robot, const Eigen::Vector2i& ptz,
                                const std::string task_data, const std::string append_data);
    const AVCodec *codec;
    AVCodecContext      *enc_ctx_;

    SwsContext *img_convert_ctx_;

    
    AVFormatContext *ofmt_ctx_;
    AVStream *video_st_;
    std::string codec_name_;
    AVFrame * frame_;
    AVFrame * bgr_frame_;

    AVPacket*  pkt_;
    std::string stream_url_;
	  const AVBitStreamFilter * bsf_;
    AVBSFContext *bsf_ctx_ = NULL;
    int key_frame_interval_;
    int constant_rate_factor_;

    std::deque< Eigen::Affine3d> camera_queue_;
    std::deque< Eigen::Affine3d> robot_queue_;
    std::deque< Eigen::Vector2i> ptz_queue_;

    PixelFormat pixel_format_;
  };
  
}
