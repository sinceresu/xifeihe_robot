/*
 * ##################################################################################################
 * ## 字节                    含义                    类型                    备注                    ##
 * ##                                                                                              ##
 * ## 1-4                    文件标记                 字 符                   固定为 "FIR\0"          ##
 * ## 5-8                    光学透过率               浮点数                   低字节在前，高字节在后    ##
 * ## 9-12                   比辐射率                 浮点数                   低字节在前，高字节在后    ##
 * ## 13-16                  距离                    浮点数                   低字节在前，高字节在后    ##
 * ## 17-20                  环境温度                 浮点数                   低字节在前，高字节在后    ##
 * ## 21-24                  环境湿度                 浮点数                   低字节在前，高字节在后    ##
 * ## 25-26                  图像高度                 短整数                   低字节在前，高字节在后    ##
 * ## 27-28                  图像宽度                 短整数                   低字节在前，高字节在后    ##
 * ## 29-64                  保留                                                                   ##
 * ###################################################################################################
 */

#include "fir_file.hpp"

CFIR::CFIR()
{
}

CFIR::~CFIR()
{
}

int CFIR::Read(std::string _file, unsigned short &_width, unsigned short &_height, std::vector<float> &_temps)
{
    FILE *fd = fopen(_file.c_str(), "r");
    if (!fd)
    {
        return -1;
    }
    // 文件标记
    char flag[4] = {0};
    fread(flag, 1, 4, fd);
    flag[3] = '\0';
    if (strcmp(flag, "FIR") != 0)
    {
        fclose(fd);
        return -1;
    }
    // 光学透过率
    float optical_transmittance;
    fread(&optical_transmittance, 1, sizeof(optical_transmittance), fd);
    // 比辐射率
    float emissivity = 0.0;
    fread(&emissivity, 1, sizeof(emissivity), fd);
    // 距离
    float distance = 0.0;
    fread(&distance, 1, sizeof(distance), fd);
    // 环境温度
    float ambient_temp = 0.0;
    fread(&ambient_temp, 1, sizeof(ambient_temp), fd);
    // 环境湿度
    float ambient_humidity = 0.0;
    fread(&ambient_humidity, 1, sizeof(ambient_humidity), fd);
    // 图像高度
    unsigned short image_height;
    fread(&image_height, 1, sizeof(image_height), fd);
    _height = image_height;
    // 图像宽度
    unsigned short image_width;
    fread(&image_width, 1, sizeof(image_width), fd);
    _width = image_width;
    // 保留
    char reserved[36] = {0};
    fread(reserved, 1, sizeof(reserved), fd);
    // 温度
    unsigned short cols = image_width;
    unsigned short rows = image_height;
    for (int row = 0; row < rows; row++)
    {
        for (int col = 0; col < cols; col++)
        {
            short temp = 0;
            fread(&temp, 1, sizeof(temp), fd);
            float fTemp = float(temp) / 10;
            // float fTemp = temp * 0.1;

            _temps.push_back(fTemp);
        }
    }
    fclose(fd);

    return 0;
}

int CFIR::Write(std::string _file, unsigned short _width, unsigned short _height, std::vector<float> _temps)
{
    FILE *fd = fopen(_file.c_str(), "w");
    if (!fd)
    {
        return -1;
    }
    // 文件标记
    char flag[4] = "FIR";
    flag[3] = '\0';
    fwrite(flag, 1, 4, fd);
    // 光学透过率
    float optical_transmittance = 0.0;
    fwrite(&optical_transmittance, 1, sizeof(optical_transmittance), fd);
    // 比辐射率
    float emissivity = 0.0;
    fwrite(&emissivity, 1, sizeof(emissivity), fd);
    // 距离
    float distance = 0.0;
    fwrite(&distance, 1, sizeof(distance), fd);
    // 环境温度
    float ambient_temp = 0.0;
    fwrite(&ambient_temp, 1, sizeof(ambient_temp), fd);
    // 环境湿度
    float ambient_humidity = 0.0;
    fwrite(&ambient_humidity, 1, sizeof(ambient_humidity), fd);
    // 图像高度
    unsigned short image_height = _height;
    fwrite(&image_height, 1, sizeof(image_height), fd);
    // 图像宽度
    unsigned short image_width = _width;
    fwrite(&image_width, 1, sizeof(image_width), fd);
    // 保留
    char reserved[36] = {0};
    fwrite(reserved, 1, sizeof(reserved), fd);
    // 温度
    for (size_t i = 0; i < _temps.size(); i++)
    {
        short value = _temps[i] * 10;
        /* code */
        fwrite(&value, 1, sizeof(value), fd);
    }
    fclose(fd);

    return 0;
}