from abc import ABCMeta
from typing import Optional, Type, Dict, Union

import inject
import paho.mqtt.client as mqtt
import rospy

from .util import lookup_object, extract_values, populate_instance

import time
from hashlib import md5
from typing import Callable
import _thread


def bridge_type(msg_type: str) -> Callable[[str], str]:
    obj_name = msg_type.split(":")[0]
    if obj_name.endswith('msg'):
        return 'msg'
    if obj_name.endswith('srv'):
        return 'srv'
    return ''


# def create_bridge(factory: Union[str, "Bridge"], msg_type: Union[str, Type[rospy.Message]], topic_from: str,
#                   topic_to: str, frequency: Optional[float] = None, **kwargs) -> "Bridge":
#     """ generate bridge instance using factory callable and arguments. if `factory` or `meg_type` is provided as string,
#      this function will convert it to a corresponding object.
#     """
#     if isinstance(factory, str):
#         factory = lookup_object(factory)
#     if not issubclass(factory, Bridge):
#         raise ValueError("factory should be Bridge subclass")
#     if isinstance(msg_type, str):
#         msg_type = lookup_object(msg_type)
#     if not issubclass(msg_type, rospy.Message):
#         raise TypeError(
#             "msg_type should be rospy.Message instance or its string"
#             "reprensentation")
#     return factory(
#         topic_from=topic_from, topic_to=topic_to, msg_type=msg_type, frequency=frequency, **kwargs)

def create_bridge(factory: Union[str, "Bridge"], msg_type: str, topic_from: str,
                  topic_to: str, service_from: Optional[str] = None, service_to: Optional[str] = None, frequency: Optional[float] = None, **kwargs) -> "Bridge":
    """ generate bridge instance using factory callable and arguments. if `factory` or `msg_type` is provided as string,
     this function will convert it to a corresponding object.
    """
    if isinstance(factory, str):
        factory = lookup_object(factory)
    if not issubclass(factory, Bridge):
        raise ValueError("factory should be Bridge subclass")
    if bridge_type(msg_type) == 'msg':
        return factory(
            topic_from=topic_from, topic_to=topic_to, msg_type=msg_type, frequency=frequency, **kwargs)
    if bridge_type(msg_type) == 'srv':
        return factory(
            service_from=service_from, service_to=service_to, topic_from=topic_from, topic_to=topic_to, msg_type=msg_type, frequency=frequency, **kwargs)


class Bridge(object, metaclass=ABCMeta):
    """ Bridge base class """
    _device_id = inject.attr('device_id')
    _mqtt_client = inject.attr(mqtt.Client)
    _serialize = inject.attr('serializer')
    _deserialize = inject.attr('deserializer')
    _extract_private_path = inject.attr('mqtt_private_path_extractor')


class RosToMqttBridge(Bridge):
    """ Bridge from ROS topic to MQTT

    bridge ROS messages on `topic_from` to MQTT topic `topic_to`. expect `msg_type` ROS message type.
    """

    # def __init__(self, topic_from: str, topic_to: str, msg_type: rospy.Message, frequency: Optional[float] = None):
    def __init__(self, topic_from: str, topic_to: str, msg_type: str, frequency: Optional[float] = None):
        self._topic_from = topic_from
        self._topic_to = self._extract_private_path(topic_to)
        # self._msg_type = msg_type
        if isinstance(msg_type, str):
            self._msg_type = lookup_object(msg_type)
        if not issubclass(self._msg_type, rospy.Message):
            raise TypeError(
                "msg_type should be rospy.Message instance or its string"
                "reprensentation")
        self._last_published = rospy.get_time()
        self._interval = 0 if frequency is None else 1.0 / frequency
        # rospy.Subscriber(topic_from, msg_type, self._callback_ros)
        self._message_ros()

    def _message_ros(self):
        rospy.Subscriber(self._topic_from, self._msg_type, self._callback_ros)

    def _message_mqtt(self):
        pass

    def _callback_ros(self, msg: rospy.Message):
        now = rospy.get_time()
        if now - self._last_published >= self._interval:
            self._publish(msg)
            self._last_published = now

    def _publish(self, msg: rospy.Message):
        # payload = self._serialize(extract_values(msg))
        t = time.time()
        Stamp = int(round(t * 1000))
        logId_md5 = md5(str(Stamp).encode('utf8')).hexdigest()
        device_id = self._device_id()
        msg_Head = {'logId': logId_md5, 'gatewayId': device_id, 'timestamp': Stamp,
                    'type': 0, 'version': 'v1.0', 'data': extract_values(msg)}
        payload = self._serialize(msg_Head)
        self._mqtt_client.publish(topic=self._topic_to, payload=payload)


class MqttToRosBridge(Bridge):
    """ Bridge from MQTT to ROS topic

    bridge MQTT messages on `topic_from` to ROS topic `topic_to`. MQTT messages will be converted to `msg_type`.
    """

    # def __init__(self, topic_from: str, topic_to: str, msg_type: Type[rospy.Message],
    #              frequency: Optional[float] = None, queue_size: int = 10):
    def __init__(self, topic_from: str, topic_to: str, msg_type: str,
                 frequency: Optional[float] = None, queue_size: int = 10):
        self._topic_from = self._extract_private_path(topic_from)
        self._topic_to = topic_to
        # self._msg_type = msg_type
        if isinstance(msg_type, str):
            self._msg_type = lookup_object(msg_type)
        if not issubclass(self._msg_type, rospy.Message):
            raise TypeError(
                "msg_type should be rospy.Message instance or its string"
                "reprensentation")
        self._queue_size = queue_size
        self._last_published = rospy.get_time()
        self._interval = None if frequency is None else 1.0 / frequency
        # # Adding the correct topic to subscribe to
        # self._mqtt_client.subscribe(self._topic_from)
        # self._mqtt_client.message_callback_add(self._topic_from, self._callback_mqtt)
        # self._publisher = rospy.Publisher(
        #     self._topic_to, self._msg_type, queue_size=self._queue_size)
        self._message_ros()

    def _message_ros(self):
        self._publisher = rospy.Publisher(
            self._topic_to, self._msg_type, queue_size=self._queue_size)

    def _message_mqtt(self):
        self._mqtt_client.subscribe(self._topic_from)
        self._mqtt_client.message_callback_add(
            self._topic_from, self._callback_mqtt)

    def _callback_mqtt(self, client: mqtt.Client, userdata: Dict, mqtt_msg: mqtt.MQTTMessage):
        """ callback from MQTT """
        rospy.logdebug("MQTT received from {}".format(mqtt_msg.topic))
        now = rospy.get_time()

        if self._interval is None or now - self._last_published >= self._interval:
            try:
                ros_msg = self._create_ros_message(mqtt_msg)
                self._publisher.publish(ros_msg)
                self._last_published = now
            except Exception as e:
                rospy.logerr(e)

    def _create_ros_message(self, mqtt_msg: mqtt.MQTTMessage) -> rospy.Message:
        """ create ROS message from MQTT payload """
        # Hack to enable both, messagepack and json deserialization.
        if self._serialize.__name__ == "packb":
            msg_dict = self._deserialize(mqtt_msg.payload, raw=False)
        else:
            msg_dict = self._deserialize(mqtt_msg.payload)
        # return populate_instance(msg_dict, self._msg_type())
        return populate_instance(msg_dict['data'], self._msg_type())


class RosServiceToMqttBridge(Bridge):
    """ Bridge from ROS service to MQTT
    """

    def __init__(self, service_from: str, service_to: str, topic_from: str, topic_to: str, msg_type: str, frequency: Optional[float] = None):
        self._request_end = False
        self._response_data = None
        self._service_from = service_from
        self._service_to = service_to
        self._topic_from = self._extract_private_path(topic_from)
        self._topic_to = self._extract_private_path(topic_to)
        # self._msg_type = msg_type
        if isinstance(msg_type, str):
            self._msg_type = lookup_object(msg_type)
        self._last_published = rospy.get_time()
        self._interval = 0 if frequency is None else 1.0 / frequency
        # self._service = rospy.Service(
        #     service_from, self._msg_type, self._callback_ros)
        self._message_ros()
        # self._mqtt_client.subscribe(self._topic_from)
        # self._mqtt_client.message_callback_add(
        #     self._topic_from, self._callback_mqtt)

    def _message_ros(self):
        self._service = rospy.Service(
            self._service_from, self._msg_type, self._callback_ros)

    def _message_mqtt(self):
        self._mqtt_client.subscribe(self._topic_from)
        self._mqtt_client.message_callback_add(
            self._topic_from, self._callback_mqtt)

    def _callback_ros(self, msg):
        rospy.logdebug("ROS received from {}".format(self._service_from))
        self._response_data = self._msg_type._response_class()
        self._request_end = False
        now = rospy.get_time()
        if now - self._last_published >= self._interval:
            self._publish(msg)
            self._last_published = now
            start_time = rospy.Time.now()
            attemp_end = start_time + rospy.Duration(60)
            rate = rospy.Rate(10)
            while not rospy.is_shutdown():
                if (self._request_end or rospy.Time.now() > attemp_end):
                    break
                rate.sleep()
        return self._response_data

    def _callback_mqtt(self, client: mqtt.Client, userdata: Dict, mqtt_msg: mqtt.MQTTMessage):
        """ callback from MQTT """
        rospy.logdebug("MQTT received from {}".format(mqtt_msg.topic))
        now = rospy.get_time()

        if self._interval is None or now - self._last_published >= self._interval:
            try:
                ros_msg = self._create_ros_message(mqtt_msg)
                self._response_data = ros_msg
                self._request_end = True
                self._last_published = now
            except Exception as e:
                rospy.logerr(e)

    def _create_ros_message(self, mqtt_msg: mqtt.MQTTMessage) -> rospy.Message:
        """ create ROS message from MQTT payload """
        # Hack to enable both, messagepack and json deserialization.
        if self._serialize.__name__ == "packb":
            msg_dict = self._deserialize(mqtt_msg.payload, raw=False)
        else:
            msg_dict = self._deserialize(mqtt_msg.payload)
        # return populate_instance(msg_dict, self._msg_type())
        return populate_instance(msg_dict['data'], self._msg_type._response_class())

    def _publish(self, msg: rospy.Message):
        # payload = self._serialize(extract_values(msg))
        t = time.time()
        Stamp = int(round(t * 1000))
        logId_md5 = md5(str(Stamp).encode('utf8')).hexdigest()
        device_id = self._device_id()
        msg_Head = {'logId': logId_md5, 'gatewayId': device_id, 'timestamp': Stamp,
                    'type': 0, 'version': 'v1.0', 'data': extract_values(msg)}
        payload = self._serialize(msg_Head)
        self._mqtt_client.publish(topic=self._topic_to, payload=payload)


class MqttToRosServiceBridge(Bridge):
    """ Bridge from MQTT to ROS service
    """

    def __init__(self, service_from: str, service_to: str, topic_from: str, topic_to: str, msg_type: str, frequency: Optional[float] = None):
        self._service_from = service_from
        self._service_to = service_to
        self._topic_from = self._extract_private_path(topic_from)
        self._topic_to = self._extract_private_path(topic_to)
        # self._msg_type = msg_type
        if isinstance(msg_type, str):
            self._msg_type = lookup_object(msg_type)
        self._last_published = rospy.get_time()
        self._interval = 0 if frequency is None else 1.0 / frequency
        self._message_ros()
        # self._mqtt_client.subscribe(self._topic_from)
        # self._mqtt_client.message_callback_add(
        #     self._topic_from, self._callback_mqtt)
        # self._message_mqtt()

    def _message_ros(self):
        pass

    def _message_mqtt(self):
        self._mqtt_client.subscribe(self._topic_from)
        self._mqtt_client.message_callback_add(
            self._topic_from, self._callback_mqtt)

    def _callback_mqtt(self, client: mqtt.Client, userdata: Dict, mqtt_msg: mqtt.MQTTMessage):
        """ callback from MQTT """
        rospy.logdebug("MQTT received from {}".format(mqtt_msg.topic))
        now = rospy.get_time()

        if self._interval is None or now - self._last_published >= self._interval:
            try:
                ros_msg = self._create_ros_message(mqtt_msg)
                try:
                    _thread.start_new_thread(self._call_service, (ros_msg, ) )
                except:
                    print ("Error: 无法启动线程")
                self._last_published = now
            except Exception as e:
                rospy.logerr(e)

    def _create_ros_message(self, mqtt_msg: mqtt.MQTTMessage) -> rospy.Message:
        """ create ROS message from MQTT payload """
        # Hack to enable both, messagepack and json deserialization.
        if self._serialize.__name__ == "packb":
            msg_dict = self._deserialize(mqtt_msg.payload, raw=False)
        else:
            msg_dict = self._deserialize(mqtt_msg.payload)
        # return populate_instance(msg_dict, self._msg_type())
        return populate_instance(msg_dict['data'], self._msg_type._request_class())

    def _publish(self, msg: rospy.Message):
        # payload = self._serialize(extract_values(msg))
        t = time.time()
        Stamp = int(round(t * 1000))
        logId_md5 = md5(str(Stamp).encode('utf8')).hexdigest()
        device_id = self._device_id()
        msg_Head = {'logId': logId_md5, 'gatewayId': device_id, 'timestamp': Stamp,
                    'type': 0, 'version': 'v1.0', 'data': extract_values(msg)}
        payload = self._serialize(msg_Head)
        self._mqtt_client.publish(topic=self._topic_to, payload=payload)

    def _call_service(self, msg):
        rospy.wait_for_service(self._service_to)
        try:
            service_call = rospy.ServiceProxy(self._service_to, self._msg_type)
            request_data = service_call(msg)
            self._publish(request_data)
        except rospy.ServiceException as e:
            print("Service call failed: %s"%e)


__all__ = ['create_bridge', 'Bridge', 'RosToMqttBridge', 'MqttToRosBridge']
