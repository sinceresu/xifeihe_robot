#!/bin/bash

# start yd_motion_control node
if [ "$1" = "rosrun" ]; then
  echo "Not Supported (rosrun start)"
elif [ "$1" = "roslaunch" ]; then
  roslaunch yd_motion_control sport_control.launch
elif [ "$1" = "rosmon" ]; then
  mon launch yd_motion_control sport_control.launch --name=rosmon_motion_control
else
  echo "Usage: yd_motion_control.sh ( commands ... )"
  echo "commands:"
  echo "  rosrun        rosrun start"
  echo "  roslaunch     roslaunch start"
  echo "  rosmon        rosmon start"
  echo "Note: Waiting for the process to end and use of the -force option require that \$CATALINA_PID is defined"
fi