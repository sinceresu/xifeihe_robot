#!/bin/bash

# start velodyne_pointcloud node
if [ "$1" = "rosrun" ]; then
  echo "Not Supported (rosrun start)"
elif [ "$1" = "roslaunch" ]; then
  roslaunch velodyne_pointcloud VLP16_points.launch
elif [ "$1" = "rosmon" ]; then
  mon launch velodyne_pointcloud VLP16_points.launch --name=mon_velodyne
else
  echo "Usage: velodyne_pointcloud.sh ( commands ... )"
  echo "commands:"
  echo "  rosrun        rosrun start"
  echo "  roslaunch     roslaunch start"
  echo "  rosmon        rosmon start"
  echo "Note: Waiting for the process to end and use of the -force option require that \$CATALINA_PID is defined"
fi