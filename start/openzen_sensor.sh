#!/bin/bash

source /home/nvidia/xifeihe_robot/devel/setup.bash

# start openzen_sensor node
if [ "$1" = "rosrun" ]; then
  rosrun openzen_sensor openzen_sensor_node _sensor_name:="IG1232000327"
elif [ "$1" = "roslaunch" ]; then
  echo "Not Supported (roslaunch start)"
elif [ "$1" = "rosmon" ]; then
  echo "Not Supported (rosmon start)"
else
  echo "Usage: openzen_sensor.sh ( commands ... )"
  echo "commands:"
  echo "  rosrun        rosrun start"
  echo "  roslaunch     roslaunch start"
  echo "  rosmon        rosmon start"
  echo "Note: Waiting for the process to end and use of the -force option require that \$CATALINA_PID is defined"
fi