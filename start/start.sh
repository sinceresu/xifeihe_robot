#/bin/bash

function mixed_run_ros_node() {
  echo "[mixed_run_ros_node] params: $*"
  # ./xxx.sh
  # sh ./xxx.sh
  # bash ./xxx.sh
  # source ./xxx.sh
  # run roscore
  echo "run roscore"
  sleep 1
  source ./roscore.sh &
  sleep 20
  # run node
  # echo "run openzen_sensor"
  # sleep 1
  # source openzen_sensor.sh 'rosrun' &
  echo "run velodyne_pointcloud"
  sleep 1
  source ./velodyne_pointcloud.sh $1 &
  echo "run yd_frontend"
  sleep 1
  source ./yd_frontend.sh $1 &
  echo "run yd_fusion_localization"
  sleep 1
  source ./yd_fusion_localization.sh $1 &
  echo "run charging_room_service"
  sleep 1
  source ./charging_room_service.sh $1 &
  echo "run weather_service"
  sleep 1
  source ./weather_service.sh $1 &
  echo "run yd_camerastream_capture"
  sleep 1
  source ./yd_camerastream_capture.sh $1 &
  echo "run yd_cloudplatform"
  sleep 1
  source ./yd_cloudplatform.sh $1 &
  echo "run yd_status_manager"
  sleep 1
  source ./yd_status_manager.sh $1 &
  echo "run undistort_service"
  sleep 1
  source ./undistort_service.sh $1 &
  echo "run streaming_service"
  sleep 1
  source ./streaming_service.sh $1 &
  echo "run ftp_image_collect"
  sleep 1
  source ./ftp_image_collect.sh $1 &
  echo "run ros_rtsp"
  sleep 1
  source ./ros_rtsp.sh $1 &
  echo "run mqtt_bridge"
  sleep 1
  source ./mqtt_bridge.sh $1 &
  echo "run yd_motion_control"
  sleep 1
  source ./yd_motion_control.sh $1 &
  echo "run openzen_sensor"
  sleep 1
  source openzen_sensor.sh 'rosrun' &
  echo "run watch_detect"
  sleep 1
  source watch_detect.sh $1 &
  echo "run watch_recon"
  sleep 1
  source watch_recon.sh $1 &
  echo "run MS"
  sleep 1
  source MS.sh 
}

function unmixed_run_ros_node() {
  echo "[unmixed_run_ros_node] params: $*"
  # run roscore
  echo "run roscore"
  sleep 1
  gnome-terminal --tab -- bash -c "bash ./roscore.sh;exec bash;"
  sleep 20
  # run node
  # echo "run openzen_sensor"
  # sleep 1
  # gnome-terminal --tab -- bash -c "bash ./openzen_sensor.sh 'rosrun';exec bash;"
  echo "run velodyne_pointcloud"
  sleep 1
  gnome-terminal --tab -- bash -c "bash ./velodyne_pointcloud.sh $1;exec bash;"
  echo "run yd_frontend"
  sleep 1
  gnome-terminal --tab -- bash -c "bash ./yd_frontend.sh $1;exec bash;"
  echo "run yd_fusion_localization"
  sleep 1
  gnome-terminal --tab -- bash -c "bash ./yd_fusion_localization.sh $1;exec bash;"
  echo "run charging_room_service"
  sleep 1
  gnome-terminal --tab -- bash -c "bash ./charging_room_service.sh $1;exec bash;"
  echo "run weather_service"
  sleep 1
  gnome-terminal --tab -- bash -c "bash ./weather_service.sh $1;exec bash;"
  echo "run yd_camerastream_capture"
  sleep 1
  gnome-terminal --tab -- bash -c "bash ./yd_camerastream_capture.sh $1;exec bash;"
  echo "run yd_cloudplatform"
  sleep 1
  gnome-terminal --tab -- bash -c "bash ./yd_cloudplatform.sh $1;exec bash;"
  echo "run yd_status_manager"
  sleep 1
  gnome-terminal --tab -- bash -c "bash ./yd_status_manager.sh $1;exec bash;"
  echo "run undistort_service"
  sleep 1
  gnome-terminal --tab -- bash -c "bash ./undistort_service.sh $1;exec bash;"
  echo "run streaming_service"
  sleep 1
  gnome-terminal --tab -- bash -c "bash ./streaming_service.sh $1;exec bash;"
  echo "run ros_rtsp"
  sleep 1
  gnome-terminal --tab -- bash -c "bash ./ros_rtsp.sh $1;exec bash;"
  echo "run ftp_image_collect"
  sleep 1
  gnome-terminal --tab -- bash -c "bash ./ftp_image_collect.sh $1;exec bash;"
  echo "run mqtt_bridge"
  sleep 1
  gnome-terminal --tab -- bash -c "bash ./mqtt_bridge.sh $1;exec bash;"
  echo "run yd_motion_control"
  sleep 1
  gnome-terminal --tab -- bash -c "bash ./yd_motion_control.sh $1;exec bash;"
  echo "run openzen_sensor"
  sleep 1
  gnome-terminal --tab -- bash -c "bash ./openzen_sensor.sh 'rosrun';exec bash;"
  echo "run watch_detect"
  sleep 1
  gnome-terminal --tab -- bash -c "bash ./watch_detect.sh $1;exec bash;"
  echo "run watch_recon"
  sleep 1
  gnome-terminal --tab -- bash -c "bash ./watch_recon.sh $1;exec bash;"
  echo "run MS"
  sleep 1
  gnome-terminal --tab -- bash -c "bash ./MS.sh $1;exec bash;"
}

function run_ros_node() {
  echo "[run_ros_node] params: $*"

  # ros env
  source ./env.sh

  if [ -z "$1" ] && [ -z "$2" ]; then
    mixed_run_ros_node "roslaunch"
  elif [ "$1" = "mixed" ] && [ -z "$2" ]; then
    mixed_run_ros_node "roslaunch"
  elif [ "$1" = "mixed" ] && [ "$2" = "rosrun" -o "$2" = "roslaunch" -o "$2" = "rosmon" ]; then
    mixed_run_ros_node $2
  elif [ "$1" = "unmixed" ] && [ -z "$2" ]; then
    unmixed_run_ros_node "roslaunch"
  elif [ "$1" = "unmixed" ] && [ "$2" = "rosrun" -o "$2" = "roslaunch" ]; then
    unmixed_run_ros_node $2
  elif [ "$1" = "rosmon" ]; then
    mixed_run_ros_node "rosmon"
  else
    echo -e "\033[31m run error\033[0m"
  fi
}

function printf_help_info() {
  echo "Usage: start.sh ( commands ... )"
  echo "commands:"
  echo "  mixed                    single mixed window (defined)"
  echo "    rosrun                   rosrun start"
  echo "    roslaunch                roslaunch start (defined)"
  echo "    rosmon                   rosmon start"
  echo "  unmixed                  multiple unmixed window"
  echo "    rosrun                   rosrun start"
  echo "    roslaunch                roslaunch start (defined)"
  echo "  rosmon                   single mixed window,rosmon start"
  echo "  help                     more information"
  echo "Note: Waiting for the process to end and use of the -force option require that \$CATALINA_PID is defined"
}

# ****** Run ******

cd /home/nvidia/xifeihe_robot/start

# params
echo "start params: $0 $*"
echo "num: $#"
echo ""

PRG="$0"
while [ -h "$PRG" ] ; do
  ls=`ls -ld "$PRG"`
  link=`expr "$ls" : '.*-> \(.*\)$'`
  if expr "$link" : '/.*' > /dev/null; then
    PRG="$link"
  else
    PRG=`dirname "$PRG"`/"$link"
  fi
done

# variable
PRGDIR=`dirname "$PRG"`
BLOOP=1

# permission
# echo 'nvidia' | sudo -S chmod 777 /dev/ttyUSB*

# exec
if [ -z "$1" ] && [ -z "$2" ]; then
  run_ros_node "mixed" "roslaunch"
elif [ "$1" = "mixed" ] && [ -z "$2" ]; then
  run_ros_node $1 "roslaunch"
elif [ "$1" = "mixed" ] && [ "$2" = "rosrun" -o "$2" = "roslaunch" -o "$2" = "rosmon" ]; then
  run_ros_node $1 $2
elif [ "$1" = "unmixed" ] && [ -z "$2" ]; then
  run_ros_node $1 "roslaunch"
elif [ "$1" = "unmixed" ] && [ "$2" = "rosrun" -o "$2" = "roslaunch" ]; then
  run_ros_node $1 $2
elif [ "$1" = "rosmon" ]; then
  run_ros_node $1
elif [ "$1" = "help" ]; then
  printf_help_info
  BLOOP=0
else
  echo "start.sh: bad params"
  echo "Try 'start.sh help' for more information."
  BLOOP=0
fi

# ctrl + c
trap "onCtrlC" INT
function onCtrlC() {
  BLOOP=0
  echo "Ctrl+C is captured"
}

# loop
while [ $BLOOP -ge 1 ] ; do
  sleep 1
done
