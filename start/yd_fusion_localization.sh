#!/bin/bash

# start yd_fusion_localization node
if [ "$1" = "rosrun" ]; then
  echo "Not Supported (rosrun start)"
elif [ "$1" = "roslaunch" ]; then
  roslaunch yd_fusion_localization online.launch
elif [ "$1" = "rosmon" ]; then
  mon launch yd_fusion_localization online.launch --name=mon_localization
else
  echo "Usage: yd_fusion_localization.sh ( commands ... )"
  echo "commands:"
  echo "  rosrun        rosrun start"
  echo "  roslaunch     roslaunch start"
  echo "  rosmon        rosmon start"
  echo "Note: Waiting for the process to end and use of the -force option require that \$CATALINA_PID is defined"
fi