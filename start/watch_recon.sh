#!/bin/bash
WORKSPACE_PATH=`echo /home/nvidia/xifeihe_robot`
source $WORKSPACE_PATH/devel/setup.bash

# start weather_service node
if [ "$1" = "rosrun" ]; then
  echo "Not Supported (rosrun start)"
elif [ "$1" = "roslaunch" ]; then
  roslaunch device_detect watch_recog.launch
elif [ "$1" = "rosmon" ]; then
  mon launch roslaunch device_detect watch_recog.launch --name=mon_watch_recog
else
  echo "Usage: weather_service.sh ( commands ... )"
  echo "commands:"
  echo "  rosrun        rosrun start"
  echo "  roslaunch     roslaunch start"
  echo "  rosmon        rosmon start"
  echo "Note: Waiting for the process to end and use of the -force option require that \$CATALINA_PID is defined"
fi
