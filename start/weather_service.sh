#!/bin/bash

# start weather_service node
if [ "$1" = "rosrun" ]; then
  echo "Not Supported (rosrun start)"
elif [ "$1" = "roslaunch" ]; then
  roslaunch weather_service weather_service.launch
elif [ "$1" = "rosmon" ]; then
  mon launch weather_service weather_service.launch --name=mon_weather_service
else
  echo "Usage: weather_service.sh ( commands ... )"
  echo "commands:"
  echo "  rosrun        rosrun start"
  echo "  roslaunch     roslaunch start"
  echo "  rosmon        rosmon start"
  echo "Note: Waiting for the process to end and use of the -force option require that \$CATALINA_PID is defined"
fi