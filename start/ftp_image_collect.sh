#!/bin/bash

# start ftp_image_collect node
if [ "$1" = "rosrun" ]; then
  echo "Not Supported (rosrun start)"
elif [ "$1" = "roslaunch" ]; then
  roslaunch ftp_image_collect ftp_image_collect.launch
elif [ "$1" = "rosmon" ]; then
  mon launch ftp_image_collect ftp_image_collect.launch --name=mon_ftp_image_collect
else
  echo "Usage: ftp_image_collect.sh ( commands ... )"
  echo "commands:"
  echo "  rosrun        rosrun start"
  echo "  roslaunch     roslaunch start"
  echo "  rosmon        rosmon start"
  echo "Note: Waiting for the process to end and use of the -force option require that \$CATALINA_PID is defined"
fi