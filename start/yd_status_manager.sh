#!/bin/bash

# start yd_status_manager node
if [ "$1" = "rosrun" ]; then
  echo "Not Supported (rosrun start)"
elif [ "$1" = "roslaunch" ]; then
  roslaunch yd_status_manager yd_status_manager.launch
elif [ "$1" = "rosmon" ]; then
  mon launch yd_status_manager yd_status_manager.launch --name=mon_status_manager
else
  echo "Usage: yd_status_manager.sh ( commands ... )"
  echo "commands:"
  echo "  rosrun        rosrun start"
  echo "  roslaunch     roslaunch start"
  echo "  rosmon        rosmon start"
  echo "Note: Waiting for the process to end and use of the -force option require that \$CATALINA_PID is defined"
fi