#!/bin/bash

# start yd_frontend node
if [ "$1" = "rosrun" ]; then
  echo "Not Supported (rosrun start)"
elif [ "$1" = "roslaunch" ]; then
  roslaunch yd_frontend yd_laser_odometry.launch
elif [ "$1" = "rosmon" ]; then
  mon launch yd_frontend yd_laser_odometry.launch --name=mon_frontend
else
  echo "Usage: yd_frontend.sh ( commands ... )"
  echo "commands:"
  echo "  rosrun        rosrun start"
  echo "  roslaunch     roslaunch start"
  echo "  rosmon        rosmon start"
  echo "Note: Waiting for the process to end and use of the -force option require that \$CATALINA_PID is defined"
fi