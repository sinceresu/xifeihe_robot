#!/bin/bash

# start charging_room_service node
if [ "$1" = "rosrun" ]; then
  echo "Not Supported (rosrun start)"
elif [ "$1" = "roslaunch" ]; then
  roslaunch charging_room_service charging_room_service.launch
elif [ "$1" = "rosmon" ]; then
  mon launch charging_room_service charging_room_service.launch --name=mon_charing_room_service
else
  echo "Usage: charging_room_service.sh ( commands ... )"
  echo "commands:"
  echo "  rosrun        rosrun start"
  echo "  roslaunch     roslaunch start"
  echo "  rosmon        rosmon start"
  echo "Note: Waiting for the process to end and use of the -force option require that \$CATALINA_PID is defined"
fi