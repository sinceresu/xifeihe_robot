#!/bin/bash

# start decoder_h264 node
if [ "$1" = "rosrun" ]; then
  echo "Not Supported (rosrun start)"
elif [ "$1" = "roslaunch" ]; then
  roslaunch decoder_h264 decoder_h264_node.launch
elif [ "$1" = "rosmon" ]; then
  mon launch decoder_h264 decoder_h264_node.launch --name=mon_decoder_h264
else
  echo "Usage: decoder_h264.sh ( commands ... )"
  echo "commands:"
  echo "  rosrun        rosrun start"
  echo "  roslaunch     roslaunch start"
  echo "  rosmon        rosmon start"
  echo "Note: Waiting for the process to end and use of the -force option require that \$CATALINA_PID is defined"
fi