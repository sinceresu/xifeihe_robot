#!/bin/bash

echo ""
echo "[YDRobot] Configuring dependencies ......"

USER_NAME=nvidia
#
echo "USER_NAME" $USER_NAME

# kinetic/melodic/noetic
ROS_SYS=melodic
LOCAL_IP=192.168.1.251
MASTER_IP=192.168.1.251
WORKSPACE_PATH=`echo /home/nvidia/xifeihe_robot`

#
echo "ROS_SYS" $ROS_SYS
echo "LOCAL_IP" $LOCAL_IP
echo "MASTER_IP" $MASTER_IP
echo "WORKSPACE_PATH" $WORKSPACE_PATH
echo ""

# env
export ROS_IP=$LOCAL_IP
export ROS_HOSTNAME=$LOCAL_IP
export ROS_MASTER_URI=http://$MASTER_IP:11311
#
source /opt/ros/$ROS_SYS/setup.bash
source $WORKSPACE_PATH/install/setup.bash
