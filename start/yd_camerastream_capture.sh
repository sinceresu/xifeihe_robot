#!/bin/bash

# start yd_camerastream_capture node
if [ "$1" = "rosrun" ]; then
  echo "Not Supported (rosrun start)"
elif [ "$1" = "roslaunch" ]; then
  roslaunch yd_camerastream_capture rtsp_to_h264_capture.launch
elif [ "$1" = "rosmon" ]; then
  mon launch yd_camerastream_capture rtsp_to_h264_capture.launch --name=mon_camerastream_capture
else
  echo "Usage: yd_camerastream_capture.sh ( commands ... )"
  echo "commands:"
  echo "  rosrun        rosrun start"
  echo "  roslaunch     roslaunch start"
  echo "  rosmon        rosmon start"
  echo "Note: Waiting for the process to end and use of the -force option require that \$CATALINA_PID is defined"
fi