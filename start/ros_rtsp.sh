#!/bin/bash

# start ros_rtsp node
if [ "$1" = "rosrun" ]; then
  echo "Not Supported (rosrun start)"
elif [ "$1" = "roslaunch" ]; then
  roslaunch ros_rtsp rtsp_streams.launch
elif [ "$1" = "rosmon" ]; then
  mon launch ros_rtsp rtsp_streams.launch --name=mon_rtsp_streams
else
  echo "Usage: ros_rtsp.sh ( commands ... )"
  echo "commands:"
  echo "  rosrun        rosrun start"
  echo "  roslaunch     roslaunch start"
  echo "  rosmon        rosmon start"
  echo "Note: Waiting for the process to end and use of the -force option require that \$CATALINA_PID is defined"
fi
